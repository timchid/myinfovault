-- Author: Pradeep Haldiya
-- Created: 2013-07-08
-- 
-- Consolidate/update/add new department
-- 1.  Two CAES departments 
--     (CAES – Human and Community Development, CAES – Environmental Design) 
--     being combined into one new department (CAES – Human Ecology)
--
-- 2. Rename MIV department 23:292 in the College of Biological Sciences (CBS) from Microbiology to 
--    Microbiology and Molecular Genetics


-- Consolidate/update/add new department to reflect CAES dept. changes 
source insertSystemDepartment.sql;

-- Moving Users and their dossiers and archive dossiers to CAES - Human Ecology Department.
Update UserAccount Set DepartmentID=246 Where SchoolID=27 And DepartmentID=54;
Update Appointment Set DepartmentID=246 Where SchoolID=27 And DepartmentID=54;
Update Dossier Set PrimaryDepartmentID=246 Where PrimarySchoolID=27 And PrimaryDepartmentID=54;
Update DossierSnapshot Set DepartmentID=246 Where SchoolID=27 And DepartmentID=54;

