-- Author: Pradeep K Haldiya
-- Created: 2012-12-03


-- to change title column length from 100 to 225 characters.
source AlterTables_MIV4665.sql;

-- remove depts that are not used.
source insertSystemDepartment.sql;


source AlterTables_MIV4669.sql;