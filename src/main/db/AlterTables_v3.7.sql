-- Author: Rick Hendricks
-- Date: 01/12/2011

-- Add/update columns for v3.7.0.

Alter table `EducationSummary` 
ADD COLUMN `StartMonthId` int(2) null default 0 after StartDate,
ADD COLUMN `EndMonthId` int(2) null default 0 after EndDate,
MODIFY COLUMN `Degree` varchar(30);

