-- Author: Rick Hendricks
-- Date: 03/17/2011

-- Table updates for v3.9.0.

Alter table `Dossier` 
ADD COLUMN `RetroactiveDate` date default null after `Effectivedate`;

source insertDossierAttributeLocation.sql;
source insertDelegationAuthorityAttributeCollection.sql;






 
