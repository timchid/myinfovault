--  Author: Stephen Paulsen
-- Created: 2011-06-24
-- Updated: 2011-06-24

-- Table and Data updates for v4.0

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


ALTER TABLE RoleAssignment
 ADD COLUMN `Scope` varchar(255) DEFAULT NULL AFTER `RoleID`;

source createSystemSchool.sql
source createSystemDepartment.sql
source insertSystemSchool.sql
source insertSystemDepartment.sql
source insertDossierView.sql

source createMivRole.sql
source insertMivRole.sql
source MivFunctions_v4.0.sql
source createAppointmentAssignmentView.sql
source createUserAssignmentView.sql
 
 -- Rebuild the "roles" view to include the Scope from RoleAssignment
DROP VIEW IF EXISTS `roles`;
-- A convenient way to look at a persons roles: select * from roles where UserID=nnnnn
CREATE VIEW `roles` AS SELECT
    `RoleAssignment`.`ID` AS `ID`,
    `RoleAssignment`.`UserID` AS `UserID`,
    `RoleAssignment`.`RoleID` AS `RoleID`,
    `MivRole`.`MivCode` AS `MivCode`,
    `Scope` AS `Scope`,
    `RoleAssignment`.`PrimaryRole` AS `PrimaryRole`
  FROM (`RoleAssignment` LEFT JOIN `MivRole` ON ((`RoleAssignment`.`RoleID` = `MivRole`.`ID`)));

-- AuditTrail implementation : Start
source createEntityTypeAndActionTypeTablesWithInitialData.sql
source createAuditTrailTable.sql
-- AuditTrail implementation : End
    
  
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
