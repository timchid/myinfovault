-- signature records for DEANS_FINAL_DECISION, DEANS_RECOMMENDATION, and JOINT_DEANS_RECOMMENDATION
-- will now use their associated department ID instead of the RAF ID
UPDATE DocumentSignature s
LEFT JOIN RAF_FormDepartments d
ON s.DocumentID=d.FormID
AND s.SchoolID=d.SchoolID
AND s.Departmentid=d.DepartmentID
SET s.DocumentID=d.ID
WHERE s.DocumentType IN(20,23,28);


-- No longer need to maintain signature ID and list of requested signatures for RAF and departments
-- each decision on a RAF (primary department, joint departments, and the RAF itself) have a unique ID
ALTER TABLE RAF_Forms DROP COLUMN SignatureID, DROP COLUMN Requested;
ALTER TABLE RAF_FormDepartments DROP COLUMN SignatureID, DROP COLUMN Requested;

-- Country, Province and City columns can be null
ALTER TABLE `Events` 
CHANGE COLUMN `Country` `Country` VARCHAR(3) NULL DEFAULT NULL  , 
CHANGE COLUMN `Province` `Province` VARCHAR(30) NULL DEFAULT NULL  , 
CHANGE COLUMN `City` `City` VARCHAR(50) NULL DEFAULT NULL;

ALTER TABLE `PublicationEvents` 
CHANGE COLUMN `Country` `Country` VARCHAR(3) NULL DEFAULT NULL  , 
CHANGE COLUMN `Province` `Province` VARCHAR(30) NULL DEFAULT NULL  , 
CHANGE COLUMN `City` `City` VARCHAR(50) NULL DEFAULT NULL;

ALTER TABLE `Reviews` 
CHANGE COLUMN `Country` `Country` VARCHAR(3) NULL DEFAULT NULL  , 
CHANGE COLUMN `Province` `Province` VARCHAR(30) NULL DEFAULT NULL  , 
CHANGE COLUMN `City` `City` VARCHAR(50) NULL DEFAULT NULL;
