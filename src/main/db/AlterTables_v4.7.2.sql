SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- MIV-5109 : Rename Department of Spanish with Department of Spanish and Portuguese
source insertSystemDepartment.sql

source createEventLog.sql;

-- MIV-5080 - Allow the attribute status of the Disclosure Certificate to be either SIGNED or SIGNEDIT 
source insertRoutingPathDefinitions.sql;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
