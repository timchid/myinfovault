-- Move the org chart just below the extramural letters. MIV-5588
source insertDossierAttributeLocation.sql

-- Change creative *works* to creative *work* in pdfs. MIV-5632
source insertSystemSectionHeader.sql

ALTER TABLE `DossierAttributes`
CHANGE COLUMN `Value`
`Value` ENUM ('ADDED','CLOSE','OPEN','COMPLETE','SIGNED','SIGNEDIT','REQUESTED',
              'REQUESTEDIT','INVALID','INVALIDEDIT','MISSING','HOLD','RELEASE') NULL;