-- Drop unused table.
DROP TABLE UserDocumentFormat;

-- Reorder Documents in "One PDF" View
source insertCollection.sql
source insertCollectionDocument.sql;
source insertDossierView.sql;

-- Make MiddleName nullable
ALTER TABLE `UserAccount` MODIFY `MiddleName` VARCHAR(50) NULL;

-- Create back-up table for dossier records to alter
CREATE TABLE DossierBackup LIKE Dossier;

-- Back-up said records
INSERT INTO DossierBackup
SELECT *
FROM Dossier
WHERE WorkflowStatus IN ('P','F') AND CompletedDate IS NULL;

-- Update Dossier to add completed date using last routed date
UPDATE Dossier
SET CompletedDate=LastRoutedDate
WHERE WorkflowStatus IN ('P','F') AND CompletedDate IS NULL;
