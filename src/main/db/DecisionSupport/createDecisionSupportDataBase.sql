-- Author: Pradeep Haldiya
-- Created: 2012-08-13
-- Creating database for DecisionSupport.

-- Login as root user
DROP SCHEMA IF EXISTS `sisdb`; 
CREATE SCHEMA IF NOT EXISTS `sisdb`
DEFAULT CHARACTER SET = utf8
DEFAULT COLLATE = utf8_unicode_ci;

