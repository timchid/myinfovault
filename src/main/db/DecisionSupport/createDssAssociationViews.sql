-- Author: Pradeep Haldiya
-- Created: 2014-02-20
-- views for creative activities associations between 
--	1. works and events
--	2. works and publication events
--	3. works and reviews
--	4. events and reviews

-- associations between works and events 
DROP VIEW IF EXISTS `DssWorksEventsAssociation`;
CREATE VIEW `DssWorksEventsAssociation` AS 
SELECT UserID, WorkID, EventID FROM `myinfovault`.`WorksEventsAssociation`;

-- associations between works and publication events
DROP VIEW IF EXISTS `DssWorksPublicationEventsAssociation`;
CREATE VIEW `DssWorksPublicationEventsAssociation` AS 
SELECT UserID, WorkID, PublicationEventID FROM `myinfovault`.`WorksPublicationEventsAssociation`;

-- associations between works and reviews
DROP VIEW IF EXISTS `DssWorksReviewsAssociation`;
CREATE VIEW `DssWorksReviewsAssociation` AS 
SELECT UserID, WorkID, ReviewID FROM `myinfovault`.`WorksReviewsAssociation`;

-- associations between events and reviews
DROP VIEW IF EXISTS `DssEventsReviewsAssociation`;
CREATE VIEW `DssEventsReviewsAssociation` AS 
SELECT UserID, EventID, ReviewID FROM `myinfovault`.`EventsReviewsAssociation`;
