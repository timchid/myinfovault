-- Author: Pradeep Haldiya
-- Created: 2014-02-20
-- views for creative activities master tables
--	1. WorkType
--	2. WorkStatus
--	3. EventType
--	4. EventStatus
--	5. PublicationStatus

DROP VIEW IF EXISTS `DssWorkType`;
CREATE VIEW `DssWorkType` AS 
SELECT ID, Description, ShortDescription FROM `myinfovault`.`WorkType`;

DROP VIEW IF EXISTS `DssWorkStatus`;
CREATE VIEW `DssWorkStatus` AS 
SELECT ID, Status, Description, Active FROM `myinfovault`.`WorkStatus`;

DROP VIEW IF EXISTS `DssEventType`;
CREATE VIEW `DssEventType` AS 
SELECT ID, Description, ShortDescription FROM `myinfovault`.`EventType`;

DROP VIEW IF EXISTS `DssEventStatus`;
CREATE VIEW `DssEventStatus` AS 
SELECT ID, Status, Description, Active FROM `myinfovault`.`EventStatus`;

DROP VIEW IF EXISTS `DssPublicationStatus`;
CREATE VIEW `DssPublicationStatus` AS 
SELECT ID, Description FROM `myinfovault`.`PublicationStatus`;
