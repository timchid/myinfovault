-- Author: Pradeep Haldiya
-- Created: 2012-08-14
-- To the list of user's access rights on schools and departments.

DROP VIEW IF EXISTS `DssAccessRights`;
CREATE VIEW `DssAccessRights` AS 
SELECT
    	CONCAT(UA.`Surname`, ', ' ,UA.`GivenName`) AS Fullname,
	UA.`Surname`,
	UA.`GivenName`,
    	UA.`PersonUUID` AS `PersonUUID`,    
    	UA.`Login` AS `LoginID`,
    	CONVERT((CASE WHEN (MR.`MivCode` = 'DEPT_STAFF' OR MR.`MivCode` = 'DEPT_ASSISTANT' OR MR.`MivCode` = 'DEPT_CHAIR') THEN 'D'
        	 WHEN (MR.`MivCode` = 'SCHOOL_STAFF' OR MR.`MivCode` = 'DEAN') THEN 'S'
         	WHEN (MR.`MivCode` = 'SYS_ADMIN' OR MR.`MivCode` = 'VICE_PROVOST' OR MR.`MivCode` = 'VICE_PROVOST_STAFF') THEN 'A'
    	ELSE null END) USING utf8) COLLATE utf8_unicode_ci AS `AccessType`,
    	GROUP_CONCAT(DISTINCT `myinfovault`.`getDepartmentIdFromScope`(RA.`Scope`) SEPARATOR ",") AS DepartmentIds,
    	GROUP_CONCAT(DISTINCT `myinfovault`.`getSchoolIdFromScope`(RA.`Scope`) SEPARATOR ",") AS SchoolIds,
    	UA.`Active` AS UserActive
FROM `myinfovault`.`RoleAssignment` RA
LEFT JOIN `myinfovault`.`UserAccount` UA ON RA.`UserID`=UA.`UserID`
LEFT JOIN `myinfovault`.`MivRole` MR ON RA.`RoleID` = MR.`ID`
WHERE MR.`MivCode` IN ('DEPT_STAFF', 'DEPT_ASSISTANT', 'DEPT_CHAIR', 'SCHOOL_STAFF', 'DEAN', 'SYS_ADMIN', 'VICE_PROVOST', 'VICE_PROVOST_STAFF')
GROUP BY Fullname;
