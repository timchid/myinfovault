-- Author: Pradeep Haldiya
-- Created: 2014-02-20
-- Updated: 2014-06-27
-- List of creative activities reviews 

DROP VIEW IF EXISTS `DssReviews`;
CREATE VIEW `DssReviews` AS 
SELECT 
	r.ID, 

	CONCAT(u.Surname, ', ', u.GivenName) AS Fullname,
	u.Surname,
	u.GivenName,
	u.PersonUUID,
	d.Description AS Department,
        d.DepartmentID AS DepartmentID,
	s.Description AS School,
        s.SchoolID AS SchoolID,

	r.CategoryID,
	CASE r.CategoryID 
		WHEN 1 THEN 'Print Review'
		WHEN 2 THEN 'Media Review'
		END AS `CategoryDescription`,
	r.ReviewDate,
	r.PrintReviewYear,
	r.Reviewer,
	r.Venue, 
	r.Description, 
	r.Title, 
	r.Publication, 
	r.Publisher,
	r.Volume,
	r.Issue,
	r.Pages,
	r.Country, 
	r.Province,
	r.City,
	r.URL, 

	CONVERT((CASE
		WHEN aa.PrimaryAppointment = true THEN 'Primary'
		WHEN aa.PrimaryAppointment = false THEN 'Joint'
		ELSE 'None' END) USING utf8) COLLATE utf8_unicode_ci as AppointmentType,
	u.Active AS UserActive

FROM `myinfovault`.`Reviews` AS r

  LEFT JOIN myinfovault.UserAccount u ON r.UserID=u.UserID
  LEFT JOIN myinfovault.Appointment aa ON aa.DepartmentID IS NOT NULL AND aa.SchoolID IS NOT NULL AND aa.UserID=u.UserID
  LEFT JOIN myinfovault.SystemDepartment d ON d.DepartmentID=ifnull(aa.DepartmentID,u.DepartmentID) AND d.SchoolID=ifnull(aa.SchoolID,u.SchoolID)
  LEFT JOIN myinfovault.SystemSchool s ON s.Demo=false AND s.SchoolID=ifnull(aa.SchoolID,u.SchoolID)

WHERE u.PersonUUID is not null;

