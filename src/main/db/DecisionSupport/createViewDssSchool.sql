-- Author: Pradeep Haldiya
-- Created: 2012-08-14
-- To the list of miv schools with All option 

-- Depends On: nothing


DROP VIEW IF EXISTS `DssSchool`;
CREATE VIEW `DssSchool` AS 
/*SELECT 
	-1 AS ID,
	'All Schools' AS Text,
	'All Schools' AS Description,
	null AS Abbreviation,
	true AS Active
FROM DUAL
UNION */
SELECT 
	SchoolID AS ID,
	CONCAT(SchoolID, '-' ,Description) AS Text,
	Description,
	IF(char_length(`Abbreviation`)>0, `Abbreviation`, null) AS Abbreviation,
	Active 
FROM `myinfovault`.`SystemSchool` 
WHERE Demo=false;
