-- Author: Pradeep Haldiya
-- Created: 2014-02-19
-- To the list of creative activities works 
--   ^^^^^^   (wtf does this mean?)

-- Depends On: buildUserFullName (createRoutineBuildUserFullName.sql)

DROP VIEW IF EXISTS `DssUserAccount`;
CREATE VIEW `DssUserAccount` AS 
SELECT
	U.UserID, -- 'MIV Internal User ID'
	buildUserFullName(U.GivenName, U.MiddleName, U.Surname) AS Fullname,
	U.Surname,
	U.GivenName,
	U.MiddleName,
	U.PersonUUID, -- 'Mothra ID'
	D.Description AS Department,
	D.DepartmentID AS DepartmentID,
	S.Description AS School,
	S.SchoolID AS SchoolID,
	U.PreferredName,
	U.Email,
	U.Active
FROM myinfovault.UserAccount U
LEFT JOIN myinfovault.Appointment AA ON AA.DepartmentID IS NOT NULL AND AA.SchoolID IS NOT NULL AND AA.UserID=U.UserID
LEFT JOIN myinfovault.SystemDepartment D ON D.DepartmentID=ifnull(AA.DepartmentID,U.DepartmentID) AND D.SchoolID=ifnull(AA.SchoolID,U.SchoolID)
LEFT JOIN myinfovault.SystemSchool S ON S.Demo=false AND S.SchoolID=ifnull(AA.SchoolID,U.SchoolID)
WHERE U.PersonUUID is not null;
