-- These sql statements are for use with an Oracle database. For use with Mysql, the statements must be modified to use the UUID()
-- function in place of the Oracle sys_guid() function. Also the nextval function used to retrieve the next record ID for an insert
-- must be replaced with a manually determined next record id since sequneces are not supported in MySql.   

-- Inserts for the Federation School Administrator responsibility

-- New Federation "School Administrator Responsibility" associated to the Review Template

INSERT INTO KRIM_RSP_T(RSP_ID, OBJ_ID, VER_NBR, RSP_TMPL_ID, NM, DESC_TXT, ACTV_IND, NMSPC_CD)
VALUES(KRIM_RSP_ID_S.nextval, sys_guid(), 1, '1', 'Federation School Administrator', 'Federation routing permission for Dossiers in all departments for the same school.', 'Y', 'UCD-MIV')
/

-- Associate the "documentTypeName" attribute (DossierDocument) for Federation School Administrator responsibility with the Kuali reviewResponsibiltyService

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
VALUES (KRIM_ATTR_DATA_ID_S.nextval,'DossierDocument',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='documentTypeName' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Federation School Administrator' and NMSPC_CD='UCD-MIV'),1)
/

-- Associate the "routeNodeName" attribute (FederationSchool) for Federation School Administrator with the Kuali reviewResponsibiltyService

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
VALUES (KRIM_ATTR_DATA_ID_S.nextval,'FederationSchool',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='routeNodeName' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Federation School Administrator' and NMSPC_CD='UCD-MIV'),1)
/

-- Associate the "required" attribute for Federation School Administrator with the Kuali reviewResponsibiltyService

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
VALUES (KRIM_ATTR_DATA_ID_S.nextval,'true',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='required' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Federation School Administrator' and NMSPC_CD='UCD-MIV'),1)
/

-- Associate the "actionDetailsAtRoleMemberLevel" attribute for Federation School Administrator with the Kuali reviewResponsibiltyService

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
VALUES (KRIM_ATTR_DATA_ID_S.nextval,'false',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='actionDetailsAtRoleMemberLevel' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Federation School Administrator' and NMSPC_CD='UCD-MIV'),1)
/

-- Associate the SCHOOL ADMINSTRATOR role to Federation School Administrator Responsibility

INSERT INTO KRIM_ROLE_RSP_T (ACTV_IND,OBJ_ID,ROLE_ID,ROLE_RSP_ID,RSP_ID,VER_NBR)
VALUES ('Y',sys_guid(),(select ROLE_ID from KRIM_ROLE_T where NMSPC_CD='UCD-MIV' and ROLE_NM='MIV-SCH'),KRIM_ROLE_RSP_ID_S.nextval,(select RSP_ID from KRIM_RSP_T where NMSPC_CD='UCD-MIV' and NM='Federation School Administrator'),1)
/

-- Associate an "Approve" action to the Federation School Administrator Responsibility

INSERT INTO KRIM_ROLE_RSP_ACTN_T (ACTN_PLCY_CD,ACTN_TYP_CD,FRC_ACTN,OBJ_ID,ROLE_MBR_ID,ROLE_RSP_ACTN_ID,ROLE_RSP_ID,VER_NBR)
VALUES ('F','A','Y',sys_guid(),'*',KRIM_ROLE_RSP_ACTN_ID_S.nextval,(select ROLE_RSP_ID from KRIM_ROLE_RSP_T where RSP_ID = (select RSP_ID from KRIM_RSP_T where NMSPC_CD='UCD-MIV' and NM='Federation School Administrator') and ROLE_ID = (select ROLE_ID from KRIM_ROLE_T where ROLE_NM='MIV-SCH' and NMSPC_CD='UCD-MIV')),1)
/

-- Inserts for the Federation Vice Provost Office responsibility

-- New "Federation Vice Provost Office" Responsibility associated to the Review Template

INSERT INTO KRIM_RSP_T(RSP_ID, OBJ_ID, VER_NBR, RSP_TMPL_ID, NM, DESC_TXT, ACTV_IND, NMSPC_CD)
VALUES(KRIM_RSP_ID_S.nextval, sys_guid(), 1, '1', 'Federation Vice Provost Office', 'Federation routing permission for all Dossiers in all schools and departments.', 'Y', 'UCD-MIV')
/

-- Associate the "documentTypeName" attribute (DossierDocument) for Federation Vice Provost Office responsibility with the Kuali reviewResponsibiltyService

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
VALUES (KRIM_ATTR_DATA_ID_S.nextval,'DossierDocument',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='documentTypeName' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Federation Vice Provost Office' and NMSPC_CD='UCD-MIV'),1)
/

-- Associate the "routeNodeName" attribute (FederationViceProvost) for Federation Vice Provost Office with the Kuali reviewResponsibiltyService

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
VALUES (KRIM_ATTR_DATA_ID_S.nextval,'FederationViceProvost',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='routeNodeName' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Federation Vice Provost Office' and NMSPC_CD='UCD-MIV'),1)
/

-- Associate the "required" attribute for Federation Vice Provost Office with the Kuali reviewResponsibiltyService

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
VALUES (KRIM_ATTR_DATA_ID_S.nextval,'true',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='required' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Federation Vice Provost Office' and NMSPC_CD='UCD-MIV'),1)
/

-- Associate the "actionDetailsAtRoleMemberLevel" attribute for Federation Vice Provost Office with the Kuali reviewResponsibiltyService

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
VALUES (KRIM_ATTR_DATA_ID_S.nextval,'false',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='actionDetailsAtRoleMemberLevel' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Federation Vice Provost Office' and NMSPC_CD='UCD-MIV'),1)
/

-- Associate the VICEPROVOST role to Federation Vice Provost Office Responsibility

INSERT INTO KRIM_ROLE_RSP_T (ACTV_IND,OBJ_ID,ROLE_ID,ROLE_RSP_ID,RSP_ID,VER_NBR)
VALUES ('Y',sys_guid(),(select ROLE_ID from KRIM_ROLE_T where NMSPC_CD='UCD-MIV' and ROLE_NM='MIV-VP'),KRIM_ROLE_RSP_ID_S.nextval,(select RSP_ID from KRIM_RSP_T where NMSPC_CD='UCD-MIV' and NM='Federation Vice Provost Office'),1)
/

-- Associate an "Approve" action to the Federation Vice Provost Office Responsibility

INSERT INTO KRIM_ROLE_RSP_ACTN_T (ACTN_PLCY_CD,ACTN_TYP_CD,FRC_ACTN,OBJ_ID,ROLE_MBR_ID,ROLE_RSP_ACTN_ID,ROLE_RSP_ID,VER_NBR)
VALUES ('F','A','Y',sys_guid(),'*',KRIM_ROLE_RSP_ACTN_ID_S.nextval,(select ROLE_RSP_ID from KRIM_ROLE_RSP_T where RSP_ID = (select RSP_ID from KRIM_RSP_T where NMSPC_CD='UCD-MIV' and NM='Federation Vice Provost Office') and ROLE_ID = (select ROLE_ID from KRIM_ROLE_T where ROLE_NM='MIV-VP' and NMSPC_CD='UCD-MIV')),1)
/
