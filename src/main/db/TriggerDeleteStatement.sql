# Author: Stephen Paulsen
# Date: 2007-02-05

# Deletes on CvStatement -- Candidate's Statement

DELIMITER |

CREATE TRIGGER DeleteCvStatement AFTER DELETE on CvStatement
FOR EACH ROW BEGIN
  IF OLD.UploadName IS NULL
  THEN
  DELETE FROM CandidateStatement
    WHERE
       OldID=OLD.STID
    ;
  ELSE
  DELETE FROM UserUploadDocument
    WHERE
       UserID=OLD.SSN AND
       DocumentID=1 AND
       UploadFile=OLD.UploadName AND
       Year=OLD.Year
    ;
  END IF;
END;
|

DELIMITER ;

