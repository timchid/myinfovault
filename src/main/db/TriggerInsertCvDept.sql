-- Author: Stephen Paulsen
-- Date: 2008-10-24
-- Purpose: Guarantee that CvDept.SchDept text matches the DeptId and SchId columns.

-- Run on Inserts to CvDept - MIV Department Table for MIV Versions prior to v3.0

DELIMITER |

CREATE TRIGGER InsertCvDept BEFORE INSERT on CvDept
FOR EACH ROW BEGIN
  DECLARE school INT;
  DECLARE dept INT;
  DECLARE combined CHAR(30);

  SET school = NEW.SchId;
  SET dept = NEW.DeptId;
  SET combined = CONCAT(school, ',', dept);

  SET NEW.SchDept = combined;
END;
|

DELIMITER ;

