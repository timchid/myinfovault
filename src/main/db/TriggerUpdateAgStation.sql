# Author: Stephen Paulsen
# Date: 2007-02-05

# Updates on CvStation -- Ag Experiment Station

DELIMITER |

CREATE TRIGGER UpdateCvStation AFTER UPDATE on CvStation
FOR EACH ROW BEGIN
  IF OLD.UploadName IS NULL
  THEN
  UPDATE ReportSummary
    SET
       UserID=NEW.SSN,
       TypeID=1,
       ReportNumber=NEW.Snum,
       ReportTitle=NEW.Title,
       InvestigatorName=NEW.InvName,
       DateSpan=NEW.Dates,
       Sequence=NEW.Seq,
       Display=(NEW.PkPrt='Y'),
       UpdateTimestamp=now(),
       UpdateUserID=NEW.SSN
    WHERE
       TypeID=1 AND
       OldID=OLD.AESID
    ;
  ELSE
  UPDATE UserUploadDocument
    SET
       UserID=NEW.SSN,
       UploadFile=NEW.UploadName,
       Year=NEW.Dates,
       Display=(NEW.PkPrt='Y'),
       UpdateTimestamp=now(),
       UpdateUserID=NEW.SSN
    WHERE
       UserID=OLD.SSN AND
       DocumentID=9 AND
       UploadFile=OLD.UploadName AND
       Year=OLD.Dates
    ;
  END IF;
END;
|

DELIMITER ;

