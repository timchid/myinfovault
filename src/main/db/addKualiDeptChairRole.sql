-- This file contains the Oracle statements required to add the Kuali MIV-DEPT-CHAIR role. Cut and paste the
-- appropriate statements into the sql client of your choice and execute in the order shown.
-- 
-- The statements my be executed as is against an Oracle database with no further changes.
-- In order to properly update a MySql database, the statements must be modified to replace sys_guid() with UUID(),
-- and sysdate with NOW(). In addition, MySql does not support sequences, therfore the nextval function must be 
-- replaced with the max ID of the appropriate table. 
--
-- Oracle Statements
--

INSERT INTO KRIM_TYP_T (ACTV_IND,KIM_TYP_ID,NM,NMSPC_CD,OBJ_ID,SRVC_NM,VER_NBR)
  VALUES ('Y',KRIM_TYP_ID_S.nextval,'Department Chair','UCD-MIV',sys_guid(),'departmentRoleTypeService',1)

-- Routing Responsibility  
INSERT INTO KRIM_RSP_T (ACTV_IND,NMSPC_CD,OBJ_ID,RSP_ID,RSP_TMPL_ID,VER_NBR,NM,DESC_TXT)
  VALUES ('Y','UCD-MIV',sys_guid(),KRIM_RSP_ID_S.nextval,(select RSP_TMPL_ID from KRIM_RSP_TMPL_T where NMSPC_CD = 'KR-WKFLW' and NM = 'Review'),1,'Department Chair','Routing permission to Dossiers in the same department.')

-- Role Definition
INSERT INTO KRIM_ROLE_T (ACTV_IND,KIM_TYP_ID,LAST_UPDT_DT,NMSPC_CD,OBJ_ID,ROLE_ID,ROLE_NM,DESC_TXT,VER_NBR)
  VALUES ('Y',(select KIM_TYP_ID from KRIM_TYP_T where NMSPC_CD='UCD-MIV' and NM='Department'),sysdate,'UCD-MIV',sys_guid(),KRIM_ROLE_ID_S.nextval,'MIV-DEPT-CHAIR','Department Chair',1)

-- KIM Role Responsibility
INSERT INTO KRIM_ROLE_RSP_T (ACTV_IND,OBJ_ID,ROLE_ID,ROLE_RSP_ID,RSP_ID,VER_NBR)
  VALUES ('Y',sys_guid(),(select ROLE_ID from KRIM_ROLE_T where NMSPC_CD='UCD-MIV' and ROLE_NM='MIV-DEPT-CHAIR'),KRIM_ROLE_RSP_ID_S.nextval,(select RSP_ID from KRIM_RSP_T where NMSPC_CD='UCD-MIV' and NM='Department Chair'),1)

-- KIM Role Responsibility Action
INSERT INTO KRIM_ROLE_RSP_ACTN_T (ACTN_PLCY_CD,ACTN_TYP_CD,FRC_ACTN,OBJ_ID,ROLE_MBR_ID,ROLE_RSP_ACTN_ID,ROLE_RSP_ID,VER_NBR)
  VALUES ('F','A','Y',sys_guid(),'*',KRIM_ROLE_RSP_ACTN_ID_S.nextval,(select ROLE_RSP_ID from KRIM_ROLE_RSP_T where RSP_ID = (select RSP_ID from KRIM_RSP_T where NMSPC_CD='UCD-MIV' and NM='Department Chair') and ROLE_ID = (select ROLE_ID from KRIM_ROLE_T where ROLE_NM='MIV-DEPT-CHAIR' and NMSPC_CD='UCD-MIV')),1)

-- KIM Role Responsibility Attribute Data for Role
INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
  VALUES (KRIM_ATTR_DATA_ID_S.nextval,'false',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='actionDetailsAtRoleMemberLevel' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Department Chair' and NMSPC_CD='UCD-MIV'),1)

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
  VALUES (KRIM_ATTR_DATA_ID_S.nextval,'DossierDocument',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='documentTypeName' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Department Chair' and NMSPC_CD='UCD-MIV'),1)

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
  VALUES (KRIM_ATTR_DATA_ID_S.nextval,'Department',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='routeNodeName' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Department Chair' and NMSPC_CD='UCD-MIV'),1)

INSERT INTO KRIM_RSP_ATTR_DATA_T (ATTR_DATA_ID,ATTR_VAL,KIM_ATTR_DEFN_ID,KIM_TYP_ID,OBJ_ID,RSP_ID,VER_NBR)
  VALUES (KRIM_ATTR_DATA_ID_S.nextval,'true',(select KIM_ATTR_DEFN_ID from KRIM_ATTR_DEFN_T where NM='required' and NMSPC_CD='KR-WKFLW'),(select KIM_TYP_ID from KRIM_TYP_T where SRVC_NM='reviewResponsibilityTypeService' and NMSPC_CD='KR-WKFLW'),sys_guid(),(select RSP_ID from KRIM_RSP_T where NM='Department Chair' and NMSPC_CD='UCD-MIV'),1)

-- KIM Blanket Approval for Role
INSERT INTO KRIM_ROLE_PERM_T (ROLE_PERM_ID,OBJ_ID,VER_NBR,ROLE_ID,PERM_ID,ACTV_IND) VALUES (krim_role_perm_id_s.nextval,sys_guid(),1,(select ROLE_ID from KRIM_ROLE_T where ROLE_NM='MIV-DEPT-CHAIR' AND NMSPC_CD='UCD-MIV' and ACTV_IND='Y'),(select PERM_ID from KRIM_PERM_T where NM='Administer Routing for Document' and NMSPC_CD='UCD-MIV' and ACTV_IND='Y'),'Y')


