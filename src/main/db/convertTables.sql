# Author: Stephen Paulsen
# Date: 2008-01-24
#
# Make changes to production MIV tables that are not being replaced.
#

-- Increase the size of several "Letter" fields that use textareas to be mediumtext
alter table CvOtherLetters
 modify ToName mediumtext,
 modify FromName mediumtext,
 modify Re mediumtext
 ;
alter table CvLetter
 modify ToDesc mediumtext,
 modify FromDesc mediumtext,
 modify Re mediumtext
 ;
alter table CvDisclose
 modify Ending mediumtext
 ;

-- Update character set to utf8 for columns to allow special character to be updated/inserted into the column without causing
# 'Data truncation: Data too long for column' error
alter table CvLetter
 modify column Remarks mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvLetter
 modify column ToDesc mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvLetter
 modify column FromDesc mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvLetter
 modify column OpenStmt mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvLetter
 modify column Teaching mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvLetter
 modify column Research mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvLetter
 modify column Competence mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvLetter
 modify column Service mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvLetter
 modify column Summary mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
 
 alter table CvLetter
 modify column Re mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;

alter table CvOtherLetters
 modify column Re mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column LetDesc varchar(50)
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column FromName mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column ToName mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column OpenStmt mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column BlankText mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column Teaching mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column Research mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column Competence mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column Service mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;
alter table CvOtherLetters
 modify column Summary mediumtext
 character set utf8 collate utf8_general_ci
 default null
 ;



-- Switch the PubMed download table from latin1 to utf8
alter table PubMed character set utf8;
alter table PubMed convert to character set utf8;

-- Make the UserLogin timestamps consistent with our new standards.
alter table UserLogin
 change column LastLoginDate LoginTimestamp datetime default NULL,
 change column LastLogoutDate LogoutTimestamp datetime default NULL,
 change column InsertDate InsertTimestamp timestamp default current_timestamp,
 change column UpdateDate UpdateTimestamp datetime default NULL
 ;

-- The "Login" column is 'text' -- way too big.
alter table UserLogin
 modify column Login varchar(50),
 -- Index the login names for faster distauth/CAS lookups.
 add index (Login)
 ;


-- Add a flag for development team members (super-admin)
alter table CvMem
 add column DevTeam boolean default false after CvLevel
 ;

