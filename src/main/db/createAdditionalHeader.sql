-- Author: Lawrence Fyfe
-- Created: 8/10/2007
-- Updated: 3/03/2010

create table AdditionalHeader
(
   ID int primary key auto_increment,
   UserID int,
   SectionName varchar(255),
   DocumentID int,
   Value varchar(100),
--   ColumnPercent tinyint(3), -- all rows are null, nothing queries it
   Sequence int,
   Display boolean NOT NULL default true,
--   OldHID2 int,         -- this is needed for data migration, but can be dropped later.
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
