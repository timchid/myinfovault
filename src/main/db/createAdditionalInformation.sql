# Author: Lawrence Fyfe
# Date: 10/20/2006

create table AdditionalInformation
(
   ID int primary key auto_increment,
   HeaderID int,
   UserID int,
   Content mediumtext,
--   ColumnOne mediumtext,     -- all rows are null, nothing queries it
--   ColumnTwo mediumtext,     -- all rows are null, nothing queries it
--   ColumnPercent tinyint(3), -- all rows are null, nothing queries it
   Sequence int,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID),
   index (HeaderID)
)
engine = InnoDB
default character set utf8;
