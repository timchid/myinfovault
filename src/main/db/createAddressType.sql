# Author: Lawrence Fyfe
# Date: 10/24/2006

create table AddressType
(
   ID int primary key auto_increment,
   Description varchar(40),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
