# Author: Venkat 
# Date: 11/26/2007

create table AdministrativeActivitySummary
(
    ID int primary key auto_increment COMMENT 'Record unique identifier',
    UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
    StartYear varchar(4) COMMENT 'Start year of activity',
    EndYear varchar(10)  COMMENT 'Last year of activity',
    Title varchar(255) COMMENT 'Administrative title and institution',
    PercentEffort varchar(4) COMMENT 'Percentage of effort of this activity',
    Sequence int COMMENT 'Relative order in which this record should appear when listed',
    UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
    UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
    index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Service: Administrative activities';
