# Author: Lawrence Fyfe
# Date: 10/12/2006

create table AdvisingSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   TypeID int COMMENT 'ID corresponding to the advising type', -- TODO: ref AdvisingType
   Year varchar(4) COMMENT 'Academic year of the advising',
   Content mediumtext COMMENT 'Description of the advising for the review period',
   AdviseeCount int COMMENT 'Number of advisees associated with the review period and description',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Teaching: Special/Student academic advising';
