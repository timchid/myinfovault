-- Author: Jacob Saporito
-- Created: 2015-03-31

-- Create the ApiEntity table
-- Dependencies:

DROP TABLE IF EXISTS `myinfovault`.`ApiEntityScope`;

CREATE TABLE `myinfovault`.`ApiEntityScope`
(
    `ID` int primary key auto_increment,
    `EntityID` int NOT NULL,
    -- Scope, -1 denotes all, 0 denotes none
    `SchoolID` int NOT NULL DEFAULT 0,
    `DepartmentID` int NOT NULL DEFAULT 0,
    `UserID` int NOT NULL DEFAULT 0,
    -- Permissions
    `Create` boolean NOT NULL DEFAULT false,
    `Read` boolean NOT NULL DEFAULT false,
    `Update` boolean NOT NULL DEFAULT false,
    `Delete` boolean NOT NULL DEFAULT false,
    -- The API resource to which this scope and these permissions apply
    `Resource` ENUM('ALL', 'ACTION', 'DOSSIER', 'GRANT', 'PACKET', 'PUBLICATION', 'USER') NOT NULL,
    `InsertTimestamp` timestamp DEFAULT CURRENT_TIMESTAMP,
    KEY `key_entityscope_entity_id` (`EntityId`),
    CONSTRAINT `fk_entityscope_entity_id`
        FOREIGN KEY (`EntityID`)
        REFERENCES `myinfovault`.`ApiEntity` (`ID`)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
)
engine = InnoDB
default character set utf8;
