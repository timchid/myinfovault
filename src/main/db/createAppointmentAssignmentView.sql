-- Author : Pradeep Kumar Haldiya
-- Created: 09/27/2011

DELIMITER $$

/*
 * View to hold Appointment Assignment info
 * Not just a convenience view - Used by UserAssignment View					
 */
DROP VIEW IF EXISTS `AppointmentAssignment`$$
CREATE VIEW `myinfovault`.`AppointmentAssignment` AS
Select  `ra`.`UserID` AS `UserID`,
        `ra`.`PrimaryRole` AS `PrimaryAppointment`,
        `getSchoolIdFromScope`(`ra`.`Scope`) AS `SchoolID`,
        `getDepartmentIdFromScope`(`ra`.`Scope`) AS `DepartmentID`,
        `ra`.`PrimaryRole` AS `PrimaryRole`,
        `ra`.`RoleID` AS `RoleID`
from `RoleAssignment` `ra`
/*union
 -- Union to Appointment table to just make sure only candidates get appointments 
Select  `a`.`UserID` AS `UserID`,
        `a`.`PrimaryAppointment` AS `PrimaryAppointment`,
        `a`.`SchoolID` AS `SchoolID`,
        `a`.`DepartmentID` AS `DepartmentID`,
        true AS `PrimaryRole`,
        '6' AS `RoleID`
from `Appointment` `a`*/
$$

DELIMITER ;
