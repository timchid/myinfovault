# Author: Lawrence Fyfe
# Date: 8/29/2006

drop table if exists CollectionDocument;
create table CollectionDocument
(
   CollectionID int,
   DocumentID int,
   Sequence int,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
