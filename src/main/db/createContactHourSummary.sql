# Author: Lawrence Fyfe
# Date: 10/16/2006

create table ContactHourSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   Term varchar(20) COMMENT 'Quarter/Year of the contact hours',
   Lecture decimal(6,2) COMMENT 'Number of hours in lecture',
   Discussion decimal(6,2) COMMENT 'Number of hours in discussion',
   Lab decimal(6,2) COMMENT 'Number of lab hours',
   Clinic decimal(6,2) COMMENT 'Number of clinic hours',
   Sequence int NOT NULL default 100 COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Teaching: Contact hours';
