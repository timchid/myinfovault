# Author: Lawrence Fyfe
# Date: 10/16/2006

create table CourseEvaluationSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   TypeID int COMMENT 'ID corresponding to the type of course evaluation', -- TODO: ref CourseEvaluationType
   Year varchar(40) COMMENT 'Quarter/Year of the course evaluation',
   Course varchar(200) COMMENT 'Course number for the evaluation',
   Description mediumtext COMMENT 'Course title',
   Enrollment INT NULL COMMENT 'Number of students enrolled',
   ResponseTotal varchar(10) COMMENT 'Total number of responses',
   InstructorScore varchar(10) COMMENT 'Instructor evaluation score',
   CourseScore varchar(10) COMMENT 'Course score',
   Link varchar(200) COMMENT 'URL to evaluation if available online',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Instructor/Course evaluatoins';
