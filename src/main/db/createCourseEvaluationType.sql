# Author: Lawrence Fyfe
# Date: 01/12/2007

create table CourseEvaluationType
(
   ID int primary key auto_increment,
   Sequence int,
   Description varchar(40),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
