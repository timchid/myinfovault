CREATE TABLE Decision (
    ID INT PRIMARY KEY auto_increment,
    Comment TEXT,
    Decision ENUM('APPROVED',
                  'DENIED',
                  'OTHER',
                  'POSITIVE_APPRAISAL',
                  'POSITIVE_GUARDED_APPRAISAL',
                  'GUARDED_APPRAISAL',
                  'GUARDED_NEGATIVE_APPRAISAL',
                  'NEGATIVE_APPRAISAL',
                  'SATISFACTORY_REVIEW_ADVANCEMENT',
                  'SATISFACTORY_REVIEW',
                  'UNSATISFACTORY_REVIEW',
                  'REAPPOINTMENT_APPROVED',
                  'REAPPOINTMENT_DENIED',
                  'NONE') NOT NULL DEFAULT 'NONE',
    ContraryToCommittee BOOLEAN DEFAULT false
) ENGINE = InnoDB DEFAULT CHARACTER SET utf8 COLLATE = utf8_unicode_ci;
