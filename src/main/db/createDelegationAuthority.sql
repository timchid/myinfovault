# Author: Rick Hendricks
# Date: 07/23/2009
# This table defines the delegation authorites for an action (Merit, Promotion, etc).

drop table if exists DelegationAuthority;

create table DelegationAuthority
(
   ID int primary key auto_increment,
   DelegationAuthority ENUM ('REDELEGATED','NON_REDELEGATED','REDELEGATED_FEDERATION') NOT null
)
engine = InnoDB
default character set utf8;
