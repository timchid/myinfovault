drop table if exists DC_Ranks;
drop table if exists DC_Materials;
drop table if exists DC_Forms;

create table DC_Forms (
        ID                  int                   primary key auto_increment,
        DossierID           int                   not null,
        SchoolID            int                   not null,
        DepartmentID        int                   not null,
  --    EnteredDate         varchar(16)           default '',
        EnteredDate         date,
        CandidateName       varchar(255), --          default '',
        SchoolName          varchar(255), --          default '',
        DepartmentName      varchar(255), --          default '',
        ActionAppraisal     boolean               not null,
        ActionAcceleration  boolean               not null,
        ActionDeferral      boolean               not null,
        ActionOther         boolean               not null,
        AdditionalInfo      text, --                  default '',
        ChangesAdditions    text, --                  default '',
        Revised             boolean,
        Archived            boolean               default false,
        
        InsertTimestamp timestamp default current_timestamp,
        InsertUserID int,
        UpdateTimestamp datetime,
        UpdateUserID int,

        constraint fk_dcforms_DossierID foreign key ind_DossierID (DossierID) references Dossier(DossierID) on delete cascade
) ENGINE = InnoDB default character set utf8 collate = utf8_general_ci;

create table DC_Materials (
        ID                  int                   primary key auto_increment,
        FormID              int                   not null,
        Material            varchar(255)          not null,
        Checked             boolean               not null,
        InsertTimestamp     timestamp             not null default current_timestamp,
        InsertUserID        int                   NOT NULL,
        UpdateTimestamp datetime,
        UpdateUserID int,
        
        constraint fk_dcformmaterials_FormID foreign key ind_FormID (FormID) references DC_Forms(ID) on delete cascade
) ENGINE = InnoDB default character set utf8 collate = utf8_general_ci;

create table DC_Ranks (
        ID                  int                   primary key auto_increment,
        FormID              int                   not null,
        PresentRankAndStep  varchar(255)          default '',
        ProposedRankAndStep varchar(255)          default '',
        InsertTimestamp     timestamp             not null default current_timestamp,
        InsertUserID        int                   NOT NULL,
        UpdateTimestamp datetime,
        UpdateUserID int,
        
        constraint fk_dcformranks_FormID foreign key ind_FormID (FormID) references DC_Forms(ID) on delete cascade
) ENGINE = InnoDB default character set utf8 collate = utf8_general_ci;
