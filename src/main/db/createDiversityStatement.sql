-- Author: Pradeep Haldiya
-- Created: 2012-06-04
-- Updated: 

-- Create the Diversity Statement
-- Dependencies: none

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS DiversityStatement;

CREATE TABLE DiversityStatement (
   ID int primary key auto_increment,
   UserID int,
   Year int,
   TeachingContent mediumtext COMMENT 'Description/Elaboration of Diversity Activities in Teaching' DEFAULT NULL,
   ServiceContent mediumtext COMMENT 'Description/Elaboration of Diversity Activities in University and Public Service' DEFAULT NULL,
   ResearchContent mediumtext COMMENT 'Description/Elaboration of Diversity Activities in Scholarly and Creative Activities' DEFAULT NULL,
   Sequence int,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
