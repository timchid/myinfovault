-- Author: Craig Gilmore
-- Created: 06/17/2009
-- Updated: 02/23/2010

-- what's this for?
-- Dependencies: none?

drop view if exists sigs;
drop view if exists sigtable;
drop table if exists DocumentSignature;

create table DocumentSignature (
    ID int primary key auto_increment,
    Signed boolean not null default false,
    UserID int not null COMMENT 'The REQUESTED Signer',
    DocumentVersion int not null default 0,
    DocumentType int not null,
    DocumentID int not null,
    SchoolID int not null,
    DepartmentID int not null,
    DossierID int,
    SignTime bigint default 0,
    DocumentHash varbinary(128),
    DigestType varchar(7),
    CompatibleVersion char(3),
    InsertTimestamp timestamp default current_timestamp,
    InsertUserID int,
    UpdateTimestamp datetime,
    UpdateUserID int COMMENT 'The ACTUAL Signer',
    unique(UserID,DocumentId,DocumentType,SchoolID,DepartmentID)
)
engine = InnoDB
default character set utf8;

-- Create a view as a convenience: the DocumentHash can have characters that affect a terminal, so hex it.
CREATE VIEW sigs AS
  SELECT ID, Signed, UserId, DocumentId AS DocId, DocumentType AS DocType, DocumentVersion AS DocRepVer, SchoolID AS SchId, DepartmentID AS DeptId, SignTime,
  left(hex(DocumentHash),64) as Signature, DigestType AS Digest,
  CompatibleVersion AS Compat, InsertUserID, InsertTimestamp, UpdateUserID AS Signer, UpdateTimestamp
 FROM DocumentSignature;

-- Create a view with more understandable names for the various User ID columns and a smaller view of the Hash.
CREATE VIEW sigtable AS
  SELECT
        ID,
        InsertUserID AS Requestor,
        UserId AS RequestedSigner,
        UpdateUserID AS Signer,
        Signed,
        SignTime,
        DocumentType AS DocType,
        DocumentId AS DocId,
        DocumentVersion AS DocRepVer,
        SchoolID AS SchId,
        DepartmentID AS DeptId,
        left(hex(DocumentHash),16) as Signature,
        DigestType AS Digest,
        CompatibleVersion AS Compat,
        InsertTimestamp,
        UpdateTimestamp
 FROM DocumentSignature
 ORDER BY ID ASC;
