# Author: Rick Hendricks
# Date: 04/06/2009
# This table defines the various dossier attribute types. The sequence defines
# the order in which the attributes should be included in the dossier.

drop table if exists DossierAttributeType;

create table DossierAttributeType
(
   ID int primary key auto_increment,
   Name varchar(60),
   Description varchar(60),
   Required boolean NOT NULL default true,
   Optional VARCHAR(255) NULL DEFAULT NULL COMMENT 'Comma-delimited list of DossierActionType.ID where the dossier attribute is optional (overriding Required)',
   Display boolean NOT NULL default true,
   ActionTypeIds varchar(255) NULL default NULL,
   Type ENUM ('DOCUMENTUPLOAD','DOCUMENT','ACTION','HEADER') NOT NULL,
   DisplayProperty varchar(50) default NULL,
   Sequence int,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
