-- Author: Rick Hendricks
-- Created:  4/03/2009
-- Updated:  2/03/2009

-- Create the ...?
-- Dependencies: Dossier and DossierAttributeType tables must exist

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

drop table if exists DossierAttributes;

-- -----------------------------------------------------
-- Table `myinfovault`.`DossierAttributes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `myinfovault`.`DossierAttributes` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `AttributeTypeID` INT NOT NULL ,
  `DossierID` INT NOT NULL ,
  `DepartmentID` INT NOT NULL ,
  `SchoolID` INT NOT NULL ,
  `Value` ENUM ('ADDED','CLOSE','OPEN','COMPLETE','SIGNED','SIGNEDIT','REQUESTED','REQUESTEDIT','INVALID','INVALIDEDIT','MISSING','HOLD','RELEASE','CANCEL') NULL,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`),
  CONSTRAINT `fk_da_DossierID` FOREIGN KEY (`DossierID`) REFERENCES `Dossier`(`DossierID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_da_AttributeType` FOREIGN KEY (`AttributeTypeID`) REFERENCES `DossierAttributeType`(`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



