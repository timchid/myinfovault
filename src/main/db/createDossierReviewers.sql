SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

drop table if exists DossierReviewers;

-- -----------------------------------------------------
-- Table `myinfovault`.`DossierReviewers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `myinfovault`.`DossierReviewers` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `EntityID` INT NOT NULL ,
  `EntityType` ENUM('GROUP', 'USER') NOT NULL default 'USER' ,
  `DossierID` INT NOT NULL ,
  `SchoolID` INT NOT NULL ,
  `DepartmentID` INT NOT NULL ,
  `ReviewLocation` VARCHAR(50) NOT NULL ,
  `AssignedByUserID` INT NOT NULL ,
  `AssignedByUserRole` VARCHAR(50) NOT NULL ,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime,
  `UpdateUserID` INT,
  PRIMARY KEY (`ID`),
  INDEX (DossierID))
    ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



