# Author: Rick Hendricks
# Date: 04/21/2009

DROP TABLE IF EXISTS DossierView;

CREATE TABLE DossierView (
	ID INT primary key auto_increment,
	RoleID SMALLINT DEFAULT 0 NOT NULL,
	CollectionID SMALLINT DEFAULT 0 NOT NULL,
	Sequence INT DEFAULT 0 NOT NULL,
	InsertTimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	InsertUserId SMALLINT DEFAULT 0 NOT NULL,
	UpdateTimestamp TIMESTAMP,
	UpdateuserId SMALLINT
) 
engine = InnoDB
default character set utf8;
