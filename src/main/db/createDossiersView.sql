-- Author : Pradeep Kumar Haldiya
-- Created: 2013-09-13

/*
 * View to hold the Dossier and AcademicAction details
 */
DROP VIEW IF EXISTS `Dossiers`;

CREATE VIEW `Dossiers` AS
SELECT  D.ID, 
        A.UserID, 
        D.AcademicActionID, 
        D.DossierID, 
        A.EffectiveDate, 
        D.RetroactiveDate, 
        D.SubmittedDate, 
        D.LastRoutedDate, 
        D.ArchivedDate, 
        D.CompletedDate, 
        D.PrimarySchoolID, 
        D.PrimaryDepartmentID, 
        D.Description, 
        DAT.ActionType,
        DA.DelegationAuthority,
        D.ReviewType, 
        D.RecommendedActionFormPresent, 
        D.WorkflowLocations, 
        D.WorkflowStatus,
        A.ActionTypeID,
        A.DelegationAuthorityID,
        D.InsertTimestamp, 
        D.InsertUserID, 
        D.UpdateTimestamp, 
        D.UpdateUserID 
FROM Dossier D, AcademicAction A, DossierActionType DAT, DelegationAuthority DA 
WHERE D.AcademicActionID = A.ID 
        AND A.ActionTypeID = DAT.ID 
        AND A.DelegationAuthorityID = DA.ID;
