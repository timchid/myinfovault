-- Author: Craig Gilmore
-- Created: 2013-07-30
-- Updated: 2013-08-23

-- Create the Log and Log Details tables for Event Logging.
-- Dependencies: none


DROP TABLE IF EXISTS EventLogDetails;
DROP TABLE IF EXISTS EventLog;

CREATE TABLE EventLog
(
    UUID BINARY(16) PRIMARY KEY,
    ParentUUID BINARY(16),
    DossierID INT,
    Posted BIGINT NOT NULL,
    Title VARCHAR(100) NOT NULL,
    Comments TEXT,
    RealUserID INT NOT NULL,
    ShadowUserID INT,

    FOREIGN KEY (DossierID) REFERENCES Dossier(DossierID) ON DELETE CASCADE
);

CREATE TABLE EventLogDetails
(
    EventLogUUID BINARY(16) NOT NULL,
    Category ENUM('DOSSIER',
                  'SIGNATURE',
                  'REQUEST',
                  'HOLD',
                  'RELEASE',
                  'DECISION',
                  'DISCLOSURE',
                  'RAF',
                  'VOTE',
                  'UPLOAD',
                  'ROUTE',
                  'REVIEW',
                  'USER',
                  'RECORDCHG',
                  'PACKET',
                  'ERROR') NOT NULL,
    Detail VARCHAR(255),
    
    FOREIGN KEY (EventLogUUID) REFERENCES EventLog(UUID) ON DELETE CASCADE
);
