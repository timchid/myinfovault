-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-02-03

-- Create the EventType Table

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS EventType;

create table EventType
(
   ID int primary key auto_increment,
   Description VARCHAR (200) NOT NULL,
   ShortDescription VARCHAR (50) NOT NULL,
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL,
   INDEX(`ID`)
)ENGINE=INNODB
AUTO_INCREMENT=501 /* Hold 500 ids for default values */
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
