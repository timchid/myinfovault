# Author: Lawrence Fyfe
# Date: 01/22/2007

create table ExtendingKnowledgeMedia
(
   ID int primary key auto_increment,
   UserID int,
   MediaTypeID int,
   Title varchar(255),
   DateSpan varchar(50),
   Publisher varchar(255),
   Sequence int,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
