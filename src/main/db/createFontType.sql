# Author: Lawrence Fyfe
# Date: 8/01/2007

create table FontType
(
   ID int primary key auto_increment,
   Name varchar(40),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
