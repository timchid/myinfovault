--  Author: Stephen Paulsen
-- Created: 2012-05-24
-- Updated: 2012-05-24

-- New geographical tables for v4.4
-- Populating the City table takes a long time.
-- This keeps the geographical tables separate from other changes
-- so these can be managed ahead of time / apart from unrelated
-- updates.


source createGeoCountry.sql;
source insertGeoCountry.sql;

source createGeoState.sql;
source insertGeoState.sql;

source createGeoCity.sql;
source insertGeoCity.sql;
