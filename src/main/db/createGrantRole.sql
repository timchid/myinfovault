# Author: Lawrence Fyfe
# Date: 9/20/2006


DROP TABLE IF EXISTS GrantRole;

create table GrantRole
(
   ID int primary key auto_increment,
   Name varchar(50),
   Sequence int,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;

