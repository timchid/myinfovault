# Author: Lawrence Fyfe
# Date: 9/20/2006

create table GrantSummary
(
    ID int primary key auto_increment COMMENT 'Record unique identifier',
    UserID int NOT NULL COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
    TypeID int NOT NULL COMMENT 'ID corresponding to the grant type (e.g. research, gift, teaching/training)', -- TODO: ref GrantType
    StatusID int NOT NULL COMMENT 'ID corresponding to the grant status (e.g. active, pending, completed, not-awarded, none )', -- TODO: ref GrantStatus
    Title varchar(255) COMMENT 'Grant/contract/gift title',
    Description mediumtext COMMENT 'Description of purpose/goal',
    Number varchar(50) COMMENT 'Grant number',
    Source varchar(255) COMMENT 'Grant funding agency or source of funds',
    Amount varchar(20) COMMENT 'Total annual ammount including interest costs',
    SubmitDate varchar(10) COMMENT 'Date the grant application was submitted',
    StartDate varchar(10) COMMENT 'Beginning date of the period covered by this grant',
    EndDate varchar(10) COMMENT 'Ending date of the period covered by this grant',
    RoleID int COMMENT 'ID corresponding to the user grant role (e.g. investigator, assistant researcher, collaborator, instructor, trainer)', -- TODO: ref GrantRole
    PrincipalInvestigator varchar(255) COMMENT 'Name of the principal investigator',
    Effort varchar(6) COMMENT 'Percent effort',
    Sequence int COMMENT 'Relative order in which this record should appear when listed',
    Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
    DisplayDescription boolean NOT NULL default true COMMENT 'Should description be displayed?',
    InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
    InsertUserID int NOT NULL COMMENT 'ID corresponding to the real user that inserted the record',
    UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
    UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
    index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Grants and contracts';
