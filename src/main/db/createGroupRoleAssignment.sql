-- Author: Rick Hendricks
-- Date: 08/08/2011
-- This table defines scoped roles for groups.

-- Create the GroupRoleScope table
-- Dependencies: Group 

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS `myinfovault`.`GroupRoleAssignment`;

CREATE TABLE GroupRoleAssignment (
    `ID` int primary key auto_increment,
    `GroupID` int NOT NULL,
    `RoleID` int NOT NULL,
    `Scope` varchar(255) DEFAULT NULL,
    `PrimaryRole` boolean NOT NULL DEFAULT false,
    `InsertTimestamp` timestamp default current_timestamp,
    `InsertUserID` int,
    CONSTRAINT
      FOREIGN KEY (`GroupID`)
      REFERENCES `Groups` (`ID`)
      ON DELETE CASCADE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

