-- Author: Rick Hendricks
-- Date: 09/01/2009
-- Updated: 2009-09-22

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

create table JointAppointments
(
  ID int primary key auto_increment,
  DossierID int NOT null,
  SchoolID int NOT null,
  DepartmentID int NOT null,
  Complete boolean default false,
  InsertTimestamp timestamp default current_timestamp,
  InsertUserID int NOT NULL,
  UpdateTimestamp datetime,
  UpdateUserID int,
  INDEX(`DossierID`),
  CONSTRAINT
    FOREIGN KEY (`SchoolID` )
    REFERENCES `myinfovault`.`SystemSchool` (`SchoolID` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT
    FOREIGN KEY (`DepartmentID` )
    REFERENCES `myinfovault`.`SystemDepartment` (`DepartmentID` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
