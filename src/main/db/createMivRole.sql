-- Author: Rick Hendricks
-- Created: 04/21/2009
-- Updated: 04/21/2009

-- Create the MIV roles table
-- Dependencies: none

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS MivRole;

CREATE TABLE MivRole (
    ID int primary key auto_increment,
    Description VARCHAR(40) DEFAULT '' NOT NULL,
    ShortDescription VARCHAR(25) DEFAULT '' NOT NULL,
    MivCode VARCHAR(20) DEFAULT '' NOT NULL,
    KimRoleId VARCHAR(25) DEFAULT '' NOT NULL,
    -- Document id's of redacted documents which this role is allowed to view. Default is NULL = none allowed
    ViewNonRedactedDocIds varchar(255) DEFAULT NULL, 
    -- Attribute id's of dossier attributes this role is limited to editing. Default is NULL = no limits
    LimitEditToDossierAttributeIds varchar(255) DEFAULT NULL,
    InsertTimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    InsertUserId SMALLINT DEFAULT 0 NOT NULL,
    UpdateTimestamp TIMESTAMP NULL,
    UpdateuserId SMALLINT
) 
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
