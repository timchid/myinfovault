# Author: Lawrence Fyfe
# Date: 10/4/2006

create table MonthName
(
   ID int primary key,
   Name varchar(40),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
