-- Author: Rick Hendricks
-- Created: 2015-03-25
-- Updated: never
--
-- Create PacketRequest records for all active dossiers at the PacketRequest location.
--
-- Required Input: None

-- Records at the PacketRequest location are identified with tje following query:
-- mysql> select * from Dossier where workflowLocations='PacketRequest' and WorkflowStatus='R';

DROP PROCEDURE IF EXISTS Gen_PacketRequests;

DELIMITER $$

CREATE PROCEDURE Gen_PacketRequests()

BEGIN
        DECLARE bDone INT DEFAULT FALSE;
        DECLARE userId int(11);
        DECLARE dossierId int(11);
        DECLARE departmentId int(11);
        DECLARE schoolId int(11);
 
        DECLARE curs CURSOR FOR SELECT aa.UserId, d.DossierId, d.PrimarySchoolId, d.PrimaryDepartmentId FROM Dossier d, AcademicAction aa  WHERE d.workflowLocations='PacketRequest' and d.WorkflowStatus='R' and d.AcademicActionId = aa.ID;
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = TRUE;
 
OPEN curs;

read_loop: LOOP
        FETCH curs INTO userId, dossierId, schoolId, departmentId;

        -- Check if we are done
        IF bDone THEN
                LEAVE read_loop;
        END IF;
        INSERT INTO PacketRequest (UserID, PacketID, DossierID, InsertUserID) VALUES (userId, 0, dossierId, 18635);
        INSERT INTO DossierAttributes (ID, AttributeTypeID, DossierID, DepartmentID, SchoolID, Value, InsertUserID) VALUES (0, 122, dossierId, departmentId, schoolId, 'OPEN', 18635);
 
END LOOP;

COMMIT;

CLOSE curs;

END $$

DELIMITER ;
