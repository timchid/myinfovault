-- Author: Stephen Paulsen
-- Created: 2014-04-11
-- Updated: 2014-07-18

-- Convert an MIV user from an Appointee to a Candidate
-- Find the first appointee RoleAssignment, which is assumed to be the Primary Appointment
--   (this may not be true for someone with multiple appointments who has also been edited)
-- Update the UserAccount record with the provided Login (Uid), PersonUUID, and Email
-- Add a UserLogin record so the new Candidate will be able to login to MIV
-- Add the EmployeeId to the UserIdentifier table.
-- Update existing appointee RoleAssignments (role 20) to be Candidate (role 6) and
-- add a new MIV_USER assignment (role 7)

DROP PROCEDURE IF EXISTS `TRANSITION_APPOINTEE`;

DELIMITER $$

CREATE PROCEDURE `TRANSITION_APPOINTEE`(
                                        IN inUpdateUser INT(11),
                                        IN inUserId INT(11),
                                        IN inUid VARCHAR(30),
                                        IN inPersonUUID VARCHAR(36),
                                        IN inEmployeeId VARCHAR(10),
                                        IN inEmail VARCHAR(100)
                                       )
BEGIN
    DECLARE nRecId INT(11);
    DECLARE sHomeScope VARCHAR(255);
    -- DECLARE affectedRows INT(11); -- used for `GET DIAGNOSTICS` which requires MySQL 5.6

    -- Magic numbers used below:
    --   20 is RoleId of APPOINTEE
    --    6 is RoleId of CANDIDATE
    --    7 is RoleId of MIV_USER


    START TRANSACTION;

    -- Maybe do the select below with "SELECT COUNT(ID) ..." to first verify there is exactly one record?
    --                                                  --
    -- Find the existing APPOINTEE role record, save the Scope to re-use, save the record ID for later update.
    SELECT ID, CONCAT_WS(', ', TRIM(SUBSTRING_INDEX(`Scope`, ',', 1)), TRIM(SUBSTRING_INDEX(`Scope`, ',', -1)))
        INTO nRecId, sHomeScope
        FROM RoleAssignment
        WHERE `UserID`=inUserId AND `RoleId`=20
      ORDER BY InsertTimestamp
      LIMIT 1
    ;

    -- debug
    /*
    SELECT CONCAT_WS('=', 'nRecId', nRecid) AS RecId,
           CONCAT_WS('=', 'sHomeScope', sHomeScope) AS NewScope;
    */

 --   SELECT inUserId AS paramUserId, inUid AS paramUid, inPersonUUID AS paramPersonUUID; -- produces output to see on page
 --   SELECT * FROM UserAccount WHERE UserID=inUserId;

    -- Update the UserAccount table, setting the LoginID, PersonUUID, and Email
    UPDATE UserAccount
        SET Login=inUid, PersonUUID=inPersonUUID, Email=inEmail, UpdateUserID=inUpdateUser, UpdateTimestamp=CURRENT_TIMESTAMP
        WHERE UserID=inUserId
    ;

    -- Insert a record in the UserLogin table with the MIV UserID and Login
    INSERT INTO UserLogin (UserID, Login, InsertUser)
        VALUES (inUserId, inUid, inUpdateUser)
    ;


    -- Insert a record in the UserIdentifier table with their Employee ID as Value using IdentifierID 1
    INSERT INTO UserIdentifier (UserID, IdentifierID, Value, InsertUserID)
        VALUES (inUserId, 1, inEmployeeId, inUpdateUser)
    ;


    -- Change all original Role records that are RoleID 20 (APOINTEE) to RoleID 6 (CANDIDATE)
    UPDATE RoleAssignment
        SET RoleID=6, UpdateUserID=inUpdateUser, UpdateTimestamp=CURRENT_TIMESTAMP
        WHERE UserID=inUserId AND RoleId=20
    ;
    -- GET DIAGNOSTICS affectedRows = ROW_COUNT; -- how many rows were changed? -- requires MySQL 5.6 ... QA is currently 5.0.95

    -- Insert another Role with RoleID 7 (MIV_USER) using the Scope copied, and with PrimaryRole set to false
    INSERT INTO RoleAssignment (UserID, RoleID, Scope, PrimaryRole, InsertUserID)
        VALUES (inUserId, 7, sHomeScope, false, inUpdateUser)
    ;

    COMMIT;
    
    SELECT * FROM roles WHERE UserID=inUserId;

END$$

DELIMITER ;

