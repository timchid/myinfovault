# Author: Lawrence Fyfe
# Date: 10/18/2006

create table ReportSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   TypeID int COMMENT 'ID corresponding to the type of report (currently only Agricultural Experiment Station)', -- TODO: ref ReportType
   ReportNumber varchar(50) COMMENT 'Report number',
   ReportTitle varchar(255) COMMENT 'Project title',
   InvestigatorName varchar(255) COMMENT 'Names of the investigators',
   DateSpan varchar(50) COMMENT 'Date range for report',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Agriculture experiment station report';
