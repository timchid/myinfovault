-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-02-03

-- Create the Reviews Table

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS Reviews;

create table Reviews
(
   ID int auto_increment,
   UserID int NOT NULL,
   CategoryID int NOT NULL,
   ReviewDate date,
   PrintReviewYear int,
   Reviewer varchar (255) NULL,
   Venue varchar (255) NULL,
   Description varchar(500) NULL,
   Title varchar (255) NULL,
   Publication varchar (255) NULL,
   Publisher varchar (255) NULL,
   Volume varchar(20) NULL,
   Issue varchar (20) NULL,
   Pages varchar (50) NULL,
   Country varchar(3) NOT NULL DEFAULT '',
   Province varchar(30) NOT NULL DEFAULT '',
   City varchar(50) NOT NULL DEFAULT '',
   URL text NULL,
   Sequence int,
   Display boolean NOT NULL default true, 
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL ,
   PRIMARY KEY (`ID`),
   INDEX(`UserID`)
)ENGINE=INNODB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
