-- Author: Stephen Paulsen
-- Created: 06/16/2009
-- Updated: 09/10/2009

-- Create the Account-to-Role relation table
-- Dependencies: UserAccount and MivRole tables must exist

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP VIEW IF EXISTS `myinfovault`.`roles`;
DROP TABLE IF EXISTS `myinfovault`.`RoleAssignment`;

CREATE TABLE RoleAssignment (
    `ID` int primary key auto_increment,
    `UserID` int NOT NULL,
    `RoleID` int NOT NULL,
    `Scope` varchar(255) DEFAULT NULL,
    `PrimaryRole` boolean NOT NULL DEFAULT false,
    `InsertTimestamp` timestamp default current_timestamp,
    `InsertUserID` int,
    `UpdateTimestamp` datetime,
    `UpdateUserID` int,
-- These next two appear redundant
    INDEX(`UserID`),
    KEY `fk_accountid` (`UserID`),
    KEY `fk_roleid` (`RoleID`),
    -- Must match a person in the account table. If the person is deleted remove their roles too.
    CONSTRAINT `fk_accountid` FOREIGN KEY (`UserID`) REFERENCES `UserAccount` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_role` FOREIGN KEY (`RoleID`) REFERENCES `MivRole` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)
ENGINE = InnoDB
DEFAULT CHARACTER SET utf8;

-- A convenient way to look at a persons roles: select * from roles where UserID=nnnnn
CREATE VIEW `roles` AS SELECT
    `RoleAssignment`.`ID` AS `ID`,
    `RoleAssignment`.`UserID` AS `UserID`,
    `RoleAssignment`.`RoleID` AS `RoleID`,
    `MivRole`.`MivCode` AS `MivCode`,
    `RoleAssignment`.`PrimaryRole` AS `PrimaryRole`
  FROM (`RoleAssignment` LEFT JOIN `MivRole` ON ((`RoleAssignment`.`RoleID` = `MivRole`.`ID`)));


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

