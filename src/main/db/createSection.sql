-- Author: Lawrence Fyfe
-- Date: 9/14/2006

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


DROP TABLE IF EXISTS `myinfovault`.`Section`;
create table Section
(
   ID int primary key auto_increment,
   Name varchar(255),
   RecordName varchar(255),
   SelectFields mediumtext,
   FromTable varchar(500),
   SectionBaseTable VARCHAR(255) NULL DEFAULT NULL,
   WhereClause varchar(255),
   SectionPrimaryKeyID varchar(500),
   OrderBy varchar(100),
   OldDset varchar(14),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS; 

