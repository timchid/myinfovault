-- Author: Rick Hendricks
-- Created: 2011-12-02
--
-- Based on article at http://www.microshell.com/database/mysql/emulating-nextval-function-to-get-sequence-in-mysql
--
-- Explanation on the table columns.
--
-- The definition of a sequence is a positive number that grows bigger.
--
-- There is flexibility of having a sequence incremented by other than 1.
-- So, for example, if you want to have a sequence with only even values or odd values,
-- you can simply start at an even number or odd number and have sequence_increment set to 2.
--
-- The last column (sequence_cycle) is a toggle for if a sequence value can roll over to minimum value
-- if it has reached the maximum value. The null sequence_cur_value is null-able because we want to have
-- the column value as NULL when the sequence has overflow and sequence cycle is set to false (to detect error condition).
-- The max value of a bigint(20) is 18446744073709551615.

CREATE TABLE `sequence_data` (
        `sequence_name` VARCHAR(100) NOT NULL,
        `sequence_increment` INT(11) UNSIGNED NOT NULL DEFAULT 1,
        `sequence_min_value` INT(11) UNSIGNED NOT NULL DEFAULT 1,
        `sequence_max_value` BIGINT(20) UNSIGNED NOT NULL DEFAULT 18446744073709551615,
        `sequence_cur_value` BIGINT(20) UNSIGNED DEFAULT 1,
        `sequence_cycle` BOOLEAN NOT NULL DEFAULT FALSE,
        PRIMARY KEY (`sequence_name`)
) ENGINE=MyISAM;
--   Why MyISAM instead of InnoDB ?
