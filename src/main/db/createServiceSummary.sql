 -- TODO: drop table; unused
# Author: Lawrence Fyfe
# Date: 10/09/2006

create table ServiceSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record',
   ServiceTypeID int,
   CommitteeTypeID int,
   Year varchar(20),
   Description mediumtext,
   PercentEffort varchar(4),
   Role varchar(255),
   Remark varchar(255),
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8;
