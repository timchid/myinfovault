--  Author: Stephen Paulsen
-- Created: 2009-03-03
-- Updated: 2011-03-18

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `myinfovault` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `myinfovault`;

-- -----------------------------------------------------
-- Table `myinfovault`.`SystemSchool`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`SystemSchool` ;

CREATE TABLE `myinfovault`.`SystemSchool` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `SchoolID` INT NOT NULL,
  `Description` VARCHAR(255) NOT NULL,
  `Abbreviation` VARCHAR(15) DEFAULT '',
  `Demo` BOOLEAN NOT NULL DEFAULT TRUE,
  `Active` BOOLEAN NOT NULL DEFAULT TRUE,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `UpdateUserID` INT DEFAULT NULL,
  PRIMARY KEY (`ID`) ,
  UNIQUE INDEX `school` (`SchoolID` ASC) )
ENGINE = InnoDB
default character set utf8
collate = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

--
-- create table SystemSchool
-- (
--     ID int NOT NULL primary key auto_increment,
--     SchoolID int NOT NULL,
--     Description varchar(255) NOT NULL,
--     InsertTimestamp timestamp NOT NULL default current_timestamp,
--     InsertUserID int NOT NULL,
--     UpdateTimestamp datetime default NULL,
--     UpdateUserID int default NULL,
-- )
-- engine = InnoDB
-- default character set utf8;
--
