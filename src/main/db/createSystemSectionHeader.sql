# Author: Stephen Paulsen
# Date: 08/15/2007

create table SystemSectionHeader
(
   ID int primary key auto_increment,
   SectionID int NOT NULL,
   Header varchar(100), -- Must allow NULL headers
   Sequence int NOT NULL default 100,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (SectionID)
)
engine = InnoDB
default character set utf8;
