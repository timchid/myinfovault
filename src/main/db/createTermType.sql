# Author: Lawrence Fyfe
# Date: 10/4/2006

create table TermType
(
   ID int primary key auto_increment,
   Code varchar(2),
   GroupCode int,   
   Description varchar(40),
   Sequence int,
   DropDownSequence int,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
