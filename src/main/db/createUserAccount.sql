--  Author: Stephen Paulsen
-- Created: 2009-03-05
-- Updated: 2009-08-05

-- Create the User Accounts table (replaces the old CvMem table)
-- Dependencies: SystemSchool table and SystemDepartment table must exist

-- possible future addition: column for KIM entity ID and/or principal ID

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `myinfovault` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `myinfovault`;

-- -----------------------------------------------------
-- Table `myinfovault`.`UserAccount`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`UserAccount`;

CREATE  TABLE IF NOT EXISTS `myinfovault`.`UserAccount` (
  `UserID` INT NOT NULL AUTO_INCREMENT,
  `Login` VARCHAR(50) NOT NULL UNIQUE,
  `PersonUUID` VARCHAR(36) COMMENT 'aka EntityID, MothraID', -- later, should be NOT NULL UNIQUE. 36 is size of Kuali OBJ_ID GUID fields.
  `SchoolID` INT NOT NULL,
  `DepartmentID` INT NOT NULL,
  `Surname` VARCHAR(50) NOT NULL,
  `GivenName` VARCHAR(50) NOT NULL,
  `MiddleName` VARCHAR(50) NULL DEFAULT NULL,
  `PreferredName` VARCHAR(255),
  `Email` VARCHAR(100) NULL DEFAULT NULL,
  `Active` BOOLEAN NOT NULL DEFAULT true,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `UpdateUserID` INT DEFAULT NULL,
  PRIMARY KEY (`UserID`),
  ADD INDEX(`PersonUUID`),-- not necessary when personuuid becomes 'unique'
  INDEX `fk_schoolid` (`SchoolID` ASC),
  INDEX `fk_departmentid` (`DepartmentID` ASC),
  CONSTRAINT `fk_schoolid`
    FOREIGN KEY (`SchoolID` )
    REFERENCES `myinfovault`.`SystemSchool` (`SchoolID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_departmentid`
    FOREIGN KEY (`DepartmentID` )
    REFERENCES `myinfovault`.`SystemDepartment` (`DepartmentID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
default character set utf8;


-- Abbreviated account table convenience view
-- DON'T write code that depends on this existing.
DROP VIEW IF EXISTS `myinfovault`.`accounts`;
CREATE  VIEW `myinfovault`.`accounts` AS
 SELECT
    UserID, Login, PersonUUID AS UUID, SchoolID AS School, DepartmentID AS Dept,
    Surname AS Surname, GivenName AS Given, MiddleName AS Middle, PreferredName AS Preferred,
    Email, Active AS Actv
 FROM UserAccount;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
