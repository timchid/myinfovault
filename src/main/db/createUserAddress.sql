# Author: Lawrence Fyfe
# Date: 10/24/2006

create table UserAddress
(
   ID int primary key auto_increment,
   UserID int,
   TypeID int,
   Street1 varchar(100),
   Street2 varchar(100),
   City varchar(50),
   State varchar(2),
   ZipCode varchar(10),
   Label varchar(50),
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
