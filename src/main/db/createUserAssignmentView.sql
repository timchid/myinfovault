-- Author : Pradeep Kumar Haldiya
-- Created: 2011-09-23
-- Updated: 2012-05-01


/*
 * View to hold user info with assigned roles info
 * Not just a convenience view - Used by MivUserReportDaoImpl
 */
DROP VIEW IF EXISTS `UserAssignment`;
CREATE VIEW `UserAssignment` AS
SELECT ua.UserID AS `UserID`
 ,ua.PersonUUID AS `PersonUUID`
 ,getUserSortNameByUserID(ua.UserID) AS `SortName`
 ,ifnull(aa.RoleID,'-1') AS `RoleID`
 ,ifnull(mr.MivCode,'INVALID_ROLE') AS `MivCode`
 ,ifnull(`aa`.`SchoolID`,getHomeSchoolIDByUserID(`ua`.`UserID`)) AS `SchoolID`
 ,CASE WHEN RoleID NOT IN (3) THEN
	ifnull(`aa`.`DepartmentID`,getHomeDepartmentIDByUserID(`ua`.`UserID`)) 
	ELSE `aa`.`DepartmentID` END
	AS `DepartmentID`
 ,aa.PrimaryRole AS `PrimaryRole`
 ,ua.Active AS `Active`
 ,ua.PreferredName AS `PreferredName`
 ,ua.Surname AS `Surname`
 ,ua.GivenName AS `GivenName`
 ,ua.MiddleName AS `MiddleName`
 ,ua.Email AS `Email`
/*FROM UserAccount ua LEFT OUTER JOIN AppointmentAssignment aa ON ua.UserID=aa.UserID
                    LEFT OUTER JOIN MivRole mr ON aa.RoleID = mr.ID$$*/
 FROM UserAccount ua, AppointmentAssignment aa, MivRole mr
 WHERE ua.UserID=aa.UserID AND aa.RoleID = mr.ID;

