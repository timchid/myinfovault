# Author: Lawrence Fyfe
# Date: 11/01/2006

create table UserEmail
(
   ID int primary key auto_increment,
   UserID int NOT NULL,
   Value varchar(255),
   Official boolean NOT NULL default false,
   Label varchar(50),
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
