# Author: Lawrence Fyfe
# Date: 01/17/2007
# Updated: 07/27/2012 by Pradeep 

create table UserFormatPattern
(
   ID int primary key auto_increment,
   UserID int NOT NULL,
   DocumentID int NOT NULL,
   FieldName varchar(255) COMMENT 'To store comma separated fields to apply same formatting options.' DEFAULT NULL,
   Pattern varchar(255),
   Bold boolean NOT NULL default false,
   Underline boolean NOT NULL default false,
   Italic boolean NOT NULL default false,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
