# Author: Lawrence Fyfe
# Date: 03/05/2007

create table UserRecordAnnotation
(
   ID int primary key auto_increment,
   UserID int,
   RecordID int,
   SectionBaseTable VARCHAR(255) NULL DEFAULT NULL, 
   Label mediumtext,
   RightJustify boolean,
   DisplayBefore boolean,
   Footnote mediumtext,
   Notation varchar(4),
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
