# Author: Stephen Paulsen
# Date: 01/09/2008

-- This table appears to be obsolete, or possibly never used at all.
-- No code references it, and there are zero records in the table in production.
-- This appears to be intended to allow custom formatting of the document and record styles,
-- which was decided to be disallowed, as the Dossier is supposed to have a standard format.
-- When confirmed:
--  * delete this file
--  * drop the table from the database

create table UserRecordFormat
(
    ID int primary key auto_increment,
    UserID int,
    RecordID int,
    SectionID int,
    DocumentID int,
    Display boolean,
    Alignment tinyint(1),
    Indentation tinyint(1),
    FontSize tinyint(2),
    Bold boolean,
    Underline boolean,
    Italic boolean,
    HeaderDisplay boolean,
    RemarkDisplay boolean,
    InsertTimestamp timestamp default current_timestamp,
    InsertUserID int,
    UpdateTimestamp datetime,
    UpdateUserID int,
    index (UserID)
)
engine = InnoDB
default character set utf8;
