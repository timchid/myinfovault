-- Author: Stephen Paulsen
-- Created: 2009-10-02
-- Updated:

-- Stored procedure used for MIV-2745

DELIMITER |

DROP PROCEDURE IF EXISTS fix_user_login|
CREATE PROCEDURE fix_user_login(IN mivid INT, IN origlogin CHAR(50), IN newlogin CHAR(50))
  MODIFIES SQL DATA
BEGIN
    DECLARE orig_count INT;
    DECLARE new_count INT;

    SET orig_count=(SELECT count(Login) FROM CvMem WHERE Uid=mivid AND Login=origlogin);
    SET new_count=(SELECT count(Login) FROM CvMem WHERE Login=newlogin);

    /* Make sure the original login is in the table */
    IF orig_count=1 AND new_count=0 THEN

        /*SELECT * FROM CvMem WHERE Uid=mivid;*/ /* debug - see what's there */

        /* fix the login in the CvMem table */
        UPDATE CvMem SET Login=newlogin WHERE Uid=mivid AND Login=origlogin;

        /*SELECT * FROM CvMem WHERE Uid=mivid;*/ /* debug - see the updated row */

        /* fix up the UserLogin table if needed */
        SET new_count=(SELECT count(Login) FROM UserLogin WHERE Login=newlogin);

        IF new_count=0 THEN /* the new login isn't in the UserLogin table yet... add it. */
            /*SELECT * FROM UserLogin WHERE UserID=mivid;*/ /* debug - what logins are there? */
            INSERT INTO UserLogin (UserID,Login,InsertUser) VALUES (mivid,newlogin,18099);
            /*SELECT * FROM UserLogin WHERE UserID=mivid;*/ /* debug - is the new login added? */
        END IF;

    /* debug - show (interactively) that it wasn't found */
    /*ELSE
        SELECT 'Not Found!';*/

    END IF;
END;
|

DELIMITER ;

