-- Author: Stephen Paulsen
-- Created: 03/29/2010
-- Updated: -

-- SQL updates for v3.2 release
-- This file can be removed after the release

source insertDocument.sql
source insertCollectionDocument.sql
source insertDossierAttributeType.sql
source insertDossierAttributeLocation.sql
source insertDelegationAuthorityAttributeCollection.sql
-- alter the DossierAttributes table; don't run createDossierAttributes.sql run this instead
source alterTablesReleaseForSignature.sql



-- List of files taken from comments for commit 6616
--   alterTablesReleaseForSignature.sql
--   insertCollectionDocument.sql
--   insertDossierAttributeType.sql
--   insertDossierAttributeLocation.sql
--   createDossierAttributes.sql
--   insertDelegationAuthorityAttributeCollection.sql
--   insertDocument.sql
