--  Author: Stephen Paulsen
-- Created: 2009-03-31
-- Updated: 2009-03-31

-- Drop old version <= 2 tables that are no longer used in MIV v3
--
-- This list is based on the V2 schema image from
--  https://confluence.ucdavis.edu/confluence/download/attachments/12977314/MIV_V2_Database_Schema.jpg?version=1
--

-- Left-to-Right, top-to-bottom order, with safest table sets first
drop table CvMedia;             -- pubs: alternative media
drop table CvBookCh;            -- pubs: book chapters
drop table CvBooks;             -- pubs: books authored
drop table CvBooksEd;           -- pubs: books edited
drop table CvEditor;            -- pubs: letters to editor
drop table CvJournals;          -- pubs: journals
drop table CvTgrants;           -- teach/train grants
drop table CvReviews;           -- pubs: (book?) reviews
drop table CvAbstracts;         -- pubs: abstracts
drop table CvLimited;           -- pubs: limited distribution
drop table CvRactgrants;        -- research grants - active
drop table CvRcomgrants;        -- research grants - completed
drop table CvRothgrants;        -- research grants - other
drop table CvRpendgrants;       -- research grants - pending
drop table CvUnawarded;         -- unawarded grants
drop table CvRgiftgrants;       -- "gift" grants
drop table CvAddDtl;            -- additional information
drop table CvEdu;               -- education & training
drop table CvTior;              -- teaching: courses (instructor of record)
drop table CvEmploy;            -- employment history
drop table CvTothers;           -- other? teaching? headers???
drop table CvLec;               -- lecture/seminar/lab/other (teaching supplement)
drop table CvTadvise;           -- teaching: advising
drop table CvTthesis;           -- teaching: thesis supervision
drop table CvEva;               -- teaching: evaluations
drop table CvHonors;            -- honors and awards
drop table CvCothers;           -- other... service?
drop table CvBoards;            -- advisory boards
drop table CvLicenses;          -- licenses & certifications
drop table CvStation;           -- ag experiment station
drop table CvComm;              -- service: committees (allegedly)
drop table CvText;              -- teaching: university extension
drop table CvEKwcpsc;           -- ext.know: workshops/conferences....
drop table CvTrainee;           -- former trainees
drop table CvHours;             -- contact hours
drop table CvInterest;          -- areas of interest
drop table CvEKbpem;            -- ext.know: broadcast,print,elec.media
drop table Tagline;             -- annotations
drop table CvAddDtl2;           -- additional information (2 col)
drop table CvPosDesc;           -- position description
drop table CvTdev;              -- teaching: development
drop table CvTspec;             -- teaching: special advising

drop table CvMaps;              -- never figured out what exactly this one does

-- NIH related tables, superseded by Biosketch
drop table NIH;
drop table NIHparms;

