-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011

DELIMITER $$

-- Function to check for Conversion Needed or Not
DROP FUNCTION IF EXISTS `isConversionNeeded`$$
CREATE FUNCTION `isConversionNeeded`(roleId INT(11)) RETURNS tinyint(1)
BEGIN
	DECLARE needConversion BOOLEAN DEFAULT FALSE;
	DECLARE SYS_ADMIN 		INT(11) DEFAULT 1; 
	DECLARE SCHOOL_STAFF 		INT(11) DEFAULT 2; 
	DECLARE DEAN 				INT(11) DEFAULT 3; 
	DECLARE DEPT_STAFF 		INT(11) DEFAULT 4; 
	DECLARE DEPT_ASSISTANT	INT(11) DEFAULT 5; 
	DECLARE CANDIDATE 		INT(11) DEFAULT 6; 
	DECLARE MIV_USER 			INT(11) DEFAULT 7; 
	DECLARE CRC_REVIEWER 		INT(11) DEFAULT 8; 
	DECLARE DEPT_REVIEWER 	INT(11) DEFAULT 9; 
	DECLARE VICE_PROVOST 		INT(11) DEFAULT 10; 
	DECLARE VICE_PROVOST_STAFF INT(11) DEFAULT 11; 
	DECLARE ARCHIVE_ADMIN 	INT(11) DEFAULT 12; 
	DECLARE SCHOOL_REVIEWER 	INT(11) DEFAULT 13; 
	DECLARE J_SCHOOL_REVIEWER 	INT(11) DEFAULT 14; 
	DECLARE CANDIDATE_SNAPSHOT INT(11) DEFAULT 15; 
	DECLARE ARCHIVE_USER 		INT(11) DEFAULT 16; 
	DECLARE DEPT_CHAIR 		INT(11) DEFAULT 17; 

	CASE roleId 
		WHEN SYS_ADMIN THEN /* 1 */
			SET needConversion = true;
		WHEN SCHOOL_STAFF THEN /* 2 */
			SET needConversion = true; /* school staff only has single appointment */
		WHEN DEAN  THEN /* 3 */
			SET needConversion = true;
		WHEN DEPT_STAFF THEN /* 4 */
			SET needConversion = true;
		WHEN DEPT_ASSISTANT THEN /* 5 */
			SET needConversion = true;
		WHEN CANDIDATE THEN /* 6 */
			SET needConversion = false;
		WHEN MIV_USER THEN /* 7 */
			SET needConversion = false;
		WHEN CRC_REVIEWER  THEN /* 8 */
			SET needConversion = false;
		WHEN DEPT_REVIEWER THEN /* 9 */
			SET needConversion = false;
		WHEN VICE_PROVOST  THEN /* 10 */
			SET needConversion = true;
		WHEN VICE_PROVOST_STAFF THEN /* 11 */
			SET needConversion = true;
		WHEN ARCHIVE_ADMIN THEN /* 12 */
			SET needConversion = true;
		WHEN SCHOOL_REVIEWER THEN /* 13 */
			SET needConversion = false;
		WHEN J_SCHOOL_REVIEWER  THEN /* 14 */
			SET needConversion = false;
		WHEN CANDIDATE_SNAPSHOT  THEN /* 15 */
			SET needConversion = false;
		WHEN ARCHIVE_USER  THEN /* 16 */
			SET needConversion = true;
		WHEN DEPT_CHAIR   THEN /* 17 */
			SET needConversion = false;
	END CASE;	

	RETURN needConversion;

END$$

DELIMITER ;
