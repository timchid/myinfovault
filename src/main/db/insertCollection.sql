# Author: Lawrence Fyfe
# Date: 8/29/2006
# Note: Replace 14021 with your internal system ID

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

truncate Collection;

insert into Collection (ID,Description,InsertUserID)
values
(1,'Packet',14021),
(2,'Curriculum Vitae',14021),
(3,'NIH Biosketch',14021),
(4,'Primary Department',18635),
(5,'Joint Department',18635),
(6,'Primary School',18635),
(7,'Joint School',18635),
(8,'CRC Reviewer',18635),
(9,'Vice Provost',18635),
(10,'School Reviewer',18635),
(11,'Joint School Reviewer',18635),
(12,'Common Department',18635),
(13,'Ad Hoc Shadow Committee',18635),
(14,'Shadow Committee',18635),
(15,'Common Appeal',18635),
(16,'School Appeal',18635),
(17,'Vice Provost Appeal',18635),
(18,'Action History Document',20508),
(19,'Primary School Review',18635),
(20,'Joint School Review',18635),
(21,'Action',21121);


COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
