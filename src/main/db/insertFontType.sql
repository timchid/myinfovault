# Author: Lawrence Fyfe
# Date: 8/01/2007

truncate FontType;

insert into FontType (ID,Name,InsertUserID)
values
(1,'Verdana',14021),
(2,'Helvetica',14021),
(3,'Times',14021),
(4,'Times New Roman',14021);
