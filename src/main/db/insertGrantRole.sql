# Author: Lawrence Fyfe
# Date: 9/19/2006

truncate GrantRole;

insert into GrantRole (ID,Name,Sequence,InsertTimestamp,InsertUserID)
values
(1,'Principal Investigator',1,current_timestamp(),14021),
(2,'Co-Investigator',3,current_timestamp(),14021),
(3,'Assistant Researcher',4,current_timestamp(),14021),
(4,'Collaborator',5,current_timestamp(),14021),
(5,'Instructor',6,current_timestamp(),14021),
(6,'Trainer',7,current_timestamp(),14021),
(7,'Co-Principal Investigator',2,current_timestamp(),14021);
