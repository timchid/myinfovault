# Author: Lawrence Fyfe
# Date: 9/19/2006

truncate GrantStatus;

insert into GrantStatus (ID,Name,Sequence,InsertUserID)
values
(1,'Active',1,14021),
(2,'Pending',2,14021),
(3,'Completed',3,14021),
(4,'Not Awarded',4,14021),
(5,'None',5,14021);
