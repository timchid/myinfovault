# Author: Lawrence Fyfe
# Date: 11/29/2006

truncate GrantType;

insert into GrantType (ID,Description,Sequence,InsertUserID)
values
(1,'Research',1,14021),
(2,'Gift',2,14021),
(3,'Teaching and Training',3,14021),
(4,'Other',4,14021);
