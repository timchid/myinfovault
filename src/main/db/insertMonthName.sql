# Author: Lawrence Fyfe
# Date: 10/4/2006

truncate MonthName;

insert into MonthName (ID,Name,InsertUserID)
values
(0,'None',14021),
(1,'January',14021),
(2,'February',14021),
(3,'March',14021),
(4,'April',14021),
(5,'May',14021),
(6,'June',14021),
(7,'July',14021),
(8,'August',14021),
(9,'September',14021),
(10,'October',14021),
(11,'November',14021),
(12,'December',14021);
