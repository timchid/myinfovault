# Author: Lawrence Fyfe
# Date: 01/23/2007

truncate Notation;

insert into Notation (ID,Symbol,Value,InsertUserID)
values
(1,'*','Included in the review period.',14021),
(2,'x','Most significant works.',14021),
(3,'+','Major mentoring role.',14021),
(4,'@','Refereed.',14021);
