--  Author: Stephen Paulsen
-- Created: 2009-03-04
-- Updated: 2011-06-24

-- Q: What is the difference between "Provost's Office" (#24) and "Offices of the Chancellor and Provost" (#41)?

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

truncate SystemSchool;

insert into SystemSchool (SchoolID, Description, Abbreviation, Demo, Active, InsertUserID)
values
( 1, 'School of Medicine', 'SOM', false, true, 18099),
(18, 'Veterinary Medicine', 'SOVM', false, true, 18099),
-- (21, 'UCSF', '', true, true, 18099),
(21, 'Sample Dean''s Office', '', true, true, 18099),
(23, 'College of Biological Sciences', 'CBS', false, true, 18099),
(24, 'Provost''s Office', '', false, true, 18099),
(27, 'College of Agricultural & Environmental Sciences', 'CAES', false, true, 18099),
-- (29, 'UCSD', '', true, false, 18099),
-- (30, 'Letters and Science', '', false, 18099),
(30, 'LS: MPS', '', false, true, 18099),
(31, 'School of Education', 'SOE', false, true, 18099),
-- (32, 'Mathematics', '', false, 18099),
-- (33, 'Sproul Social Sciences Admin', '', false, 18099), -- MIV-3844
(34, 'Engineering', 'COE', false, true, 18099),
-- (35, 'UC Irvine', '', true, true, 18099),
(36, 'Office of Research', 'OVCR', false, true, 18099), -- actually "Office of the Vice Chancellor for Research" (OVCR)
(37, 'Graduate School of Management', 'GSM', false, true, 18099),
(38, 'Information and Educational Technology', 'IET', false, true, 18099),
(39, 'University Library', '', false, true, 18099),
(40, 'Law Library', '', false, false, 18099),
(41, 'Offices of the Chancellor and Provost', '', false, true, 18099),
-- WARNING! School #42 for the Academic Senate is now a Magic Number in the code.
-- See  DepartmentListFilter.include(...) at the SENATE_STAFF case label.
(42, 'Academic Senate', '', false, true, 18099),
-- (43, 'UCR', '', true, false, 18099),
(44, 'UCLA', '', true, false, 18099),
(45, 'LS: HArCS', '', false, true, 18099),
(46, 'LS: Social Science', 'LS: DSS', false, true, 18099),
-- "LS: MPS" is School 30, above - renamed from "Letters and Science"
-- (50, 'SUNY', '', true, true, 18099),
-- (51, 'Iowa State University', '', true, true, 18099),
(52, 'School of Law', 'SOL', false, true, 18099),
(53, 'UC Davis Extension', 'UCD Extension', false, true, 18635),
(54, 'Graduate Studies', '', false, true, 18099),
(55, 'Betty Irene Moore School of Nursing', 'BIMSON', false, true, 18099)
;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
