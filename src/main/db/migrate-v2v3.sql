-- Author: Stephen Paulsen
-- Created: 06/08/2009
-- Updated: 10/05/2009

-- Run all of the alter, create, and insert SQL scripts
-- necessary to update the MIV v2.x database to v3.0

-- warnings;    -- too noisy, makes me think things are going wrong even though they're ok.

use myinfovault;

-- Remove old test and demo users as listed in MIV-2891 and
-- clean up any problem users before doing any migration.
source defineFixLogin.sql
source deleteTestUsers-v2v3.sql
source deleteDuplicateLogins-v2v3.sql
source fixActiveLogins-v2v3.sql


-- Changes to Collections, Documents, and Sections
source createCollection.sql
source insertCollection.sql

source createCollectionDocument.sql
source insertCollectionDocument.sql

source createDocument.sql
source insertDocument.sql

source createDocumentSection.sql
source insertDocumentSection.sql

source createSection.sql
source insertSection.sql


-- v2 to 3 changes for Document Uploads
source alterUserUploadDocument.sql


-- Set up the new School and Department tables
source createSystemSchool.sql
source insertSystemSchool.sql

source createSystemDepartment.sql
source insertSystemDepartment.sql

-- Changes for the "honors" section
source insertBiosketchTables.sql
-- -- Honors and Awards moved ... this part wasn't needed
-- source insertSystemSectionHeader.sql


-- Define and bootstrap the v3 roles
source createMivRole.sql
source insertMivRole.sql

-- Set up and migrate data to the new UserAccount table
drop table if exists UserAccount;   -- shouldn't already exist, but check anyway
source createUserAccount.sql

drop table if exists RoleAssignment;    -- shouldn't already exist, but check anyway
source createRoleAssignment.sql

-- Migrate Accounts - requires the new School and Department tables (above)
-- migrate from CvMem to UserAccount
source insertUserAccountMigration.sql
-- initially populate RoleAssignment based on CvLevels
source insertRoleAssignment.sql


-- Appointment and Joint Appointment storage, requires the new School and Department tables (above)
source createAppointment.sql
source createJointAppointments.sql


-- New Dossier related tables
drop table if exists DossierAttributeType;  -- shouldn't already exist, but check anyway
source createDossierAttributeType.sql;
source insertDossierAttributeType.sql;

source createDossierAttributeLocation.sql
source insertDossierAttributeLocation.sql

source createDossierAttributes.sql

source createDossierView.sql
source insertDossierView.sql

source createDossierSnapshot.sql


-- Delegation of Authority
source createDelegationAuthority.sql
source insertDelegationAuthority.sql
source createDelegationAuthorityAttributeCollection.sql
source insertDelegationAuthorityAttributeCollection.sql

source createWorkflowLocation.sql
source insertWorkflowLocation.sql

source createDossierReviewers.sql

-- Finally the Dossier itself
source createDossier.sql


-- Recommended Action Form set of tables
source createRAF.sql

-- Disclosure Certificate set of tables
source createDisclosureCertificate.sql

-- Electronic Signatures
source createDocumentSignature.sql



-- Packet Migration --

-- Migration is going to alter the uploads table. Make a backup
-- of it before we touch anything.
CREATE TABLE IF NOT EXISTS uploadbackup LIKE UserUploadDocument;
INSERT INTO uploadbackup (SELECT * FROM UserUploadDocument);
COMMIT;
