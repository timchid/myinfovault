-- v3.2 "Release for Signature"
-- Update the UploadFirst column to be false for DESII report uploads (documentID = 4)
UPDATE UserUploadDocument set UploadFirst=false where DocumentID=4;
