DELIMITER |

ALTER TABLE dossier ADD COLUMN InitiatorPrincipalName varchar (50) NOT NULL after UserId| 

DROP PROCEDURE IF EXISTS updateDossier|

CREATE PROCEDURE updateDossier()
BEGIN
  DECLARE done INT DEFAULT 0;
  DECLARE c_login CHAR(50);
  DECLARE c_id INT;
  DECLARE cur1 cursor for select u.UserId, u.Login  from  UserAccount u, Dossier d where u.UserId = d.UserId;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

  OPEN cur1;

  update_loop:  LOOP
      FETCH cur1 INTO c_id, c_login;
      IF done=1 THEN
          LEAVE update_loop;
      END IF;
  
      Update Dossier set InitiatorPrincipalName=c_login WHERE UserId=c_id;
  END LOOP update_loop;

  CLOSE cur1;
  SET done=0;

END;

|

DELIMITER ;

CALL updateDossier;

DROP PROCEDURE IF EXISTS updateDossier;
