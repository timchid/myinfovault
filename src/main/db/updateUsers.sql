# Author: Stephen Paulsen
# Date: 2008-03-17
#
# Set the devteam flag for all the development team users.
# This is a convenience to be run after a data migration,
# and should not be part of any automated process.
#

update CvMem set CvLevel='1', DevTeam=true where login in
(
'itjoycee',	# 16855
'stephenp',	# 18099
'lgjohnst',	# 18185
'rhendric',	# 18635
'bmgarg',	# 18797
'deereddy',	# 18807
'stun',		# 18816
'venkatv'	# 18824
);
