import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
//import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A test servlet to display information available through the ServletConfig and ServletContext objects. 
 * @author Stephen Paulsen
 */
public class SysInfo extends HttpServlet
{
    private static final long serialVersionUID = 200702161152L;
    private ServletConfig cfg1;
    private ServletContext ctx1;
//    private Logger syslog;
    private static final int MAXLEN = 60;

    @Override
    public void init(ServletConfig config)
    {
        // get config and context info from the config passed to us.
        this.cfg1 = config;
        this.ctx1 = cfg1.getServletContext();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
    {
        try
        {
            ServletOutputStream out = null;
            Enumeration<?> e;
            String[] columns = { "Name", "Value" };
            int colcount;

            out = res.getOutputStream();
            res.setContentType("text/html");

            prologue(out);


            /* ***** ---------- ========== ---------- ***** */

            out.println("<h3>ServletConfig Init Parameter Names</h3>\n");
            out.println("<h4>Servlet<em>Config</em>.getInitParameter(&lt;name&gt;)</h4>\n<p>");
//            colcount = beginTable(out, columns);
            colcount = beginTable(out, "Name", "Value");

            e = cfg1.getInitParameterNames();
            if (!e.hasMoreElements()) {
                out.println("    <tr><td colspan=\""+colcount+"\">nothing found</td></tr>");
            }
            else {
                while (e.hasMoreElements())
                {
                    Object o = e.nextElement();
                    String name = o.toString();
                    String val = cfg1.getInitParameter(name);
                    out.println("<tr><td>"+name+"</td><td>"+val+"</td></tr>");
                }
            }
            finishTable(out);
            out.println("</p>");



            /* ***** ---------- ========== ---------- ***** */

            out.println("<h3>ServletContext Attribute Names</h3>\n");
            out.println("<h4>ServletContext.getAttribute(&lt;name&gt;)</h4>\n<p>");
            colcount = beginTable(out, columns);

            e = ctx1.getAttributeNames();
            if (!e.hasMoreElements()) {
                out.println("    <tr><td colspan=\""+colcount+"\">nothing found</td></tr>");
            }
            else {
                while (e.hasMoreElements())
                {
                    Object o = e.nextElement();
                    String name = o.toString();
                    Object attr = ctx1.getAttribute(name);
                    String val = attr.toString();

                    // hmmmmm..... do we need this, or will the test for Iterable below do it?
                    if (val.startsWith("[L"))
                    {
                        val = Arrays.deepToString((Object[])attr);
                    }
                    if (val.length() > MAXLEN)
                    {
                        String start = val.substring(0, MAXLEN/2-1);
                        String end = val.substring(val.length()-(MAXLEN/2-1), val.length());
                        val = start + " ... " + end;
                    }
                    out.println("    <tr><td>"+name+"</td><td>"+val+"</td></tr>");
                    if (attr instanceof Iterable) {
                        for (Object atval : (Iterable<?>)attr) {
                            out.println("<tr><td>&nbsp;</td><td>"+atval.toString()+"</td></tr>");
                        }
                    }
                }
            }
            finishTable(out);
            out.println("</p>");


            /* ***** ---------- ========== ---------- ***** */

            out.println("<h3>ServletContext Init Parameter Names</h3>\n");
            out.println("<h4>Servlet<em>Context</em>.getInitParameter(&lt;name&gt;)</h4>\n<p>");
            colcount = beginTable(out, columns);

            e = ctx1.getInitParameterNames();
            if (!e.hasMoreElements()) {
                out.println("    <tr><td colspan=\""+colcount+"\">nothing found</td></tr>");
            }
            else {
                while (e.hasMoreElements())
                {
                    Object o = e.nextElement();
                    String name = o.toString();
                    String val = ctx1.getInitParameter(name);
                    out.println("    <tr><td>"+name+"</td><td>"+val+"</td></tr>");
                }
            }
            finishTable(out);
            out.println("</p>");


            /* ***** ---------- ========== ---------- ***** */

            out.println("<h3>Request Headers</h3>\n");
            out.println("<h4>Request.getHeader(&lt;name&gt;)</h4>\n<p>");
            colcount = beginTable(out, columns);

            e = req.getHeaderNames();
            if (!e.hasMoreElements()) {
                out.println("    <tr><td colspan=\""+colcount+"\">nothing found</td></tr>");
            }
            else {
                while (e.hasMoreElements())
                {
                    Object o = e.nextElement();
                    String name = o.toString();
                    String val = req.getHeader(name);
                    out.println("    <tr><td>"+name+"</td><td>"+val+"</td></tr>");
                }
            }
            finishTable(out);
            out.println("</p>");


            /* ***** ---------- ========== ---------- ***** */

            out.println("<h3>Request Attributes</h3>\n");
            out.println("<h4>Request.getAttribute(&lt;name&gt;)</h4>\n<p>");
            colcount = beginTable(out, columns);

            e = req.getAttributeNames();
            if (!e.hasMoreElements()) {
                out.println("    <tr><td colspan=\""+colcount+"\">nothing found</td></tr>");
            }
            else {
                while (e.hasMoreElements())
                {
                    Object o = e.nextElement();
                    String name = o.toString();
                    String val = req.getAttribute(name).toString();
                    out.println("    <tr><td>"+name+"</td><td>"+val+"</td></tr>");
                }
            }
            finishTable(out);
            out.println("</p>");


            epilogue(out);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    int beginTable(ServletOutputStream out, String... columns)
        throws IOException
    {
        int colcount = 0;
        out.println("\n<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\">");
        out.print  ("  <thead>\n    <tr>");
        for (String s : columns)
        {
            out.print("<th>"+s+"</th>");
            colcount++;
        }
        out.println("</tr>\n  </thead>\n  <tbody>");
        return colcount;
    }

    void finishTable(ServletOutputStream out)
        throws IOException
    {
        out.println("  </tbody>\n</table>\n");
    }

    void prologue(ServletOutputStream out)
    {
        try {
            out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            out.println("<html>\n<head>");
            out.println(" <title>Test : SysInfo</title>");
            out.println(" <link rel='stylesheet' type='text/css' href='../myinfovault.css'>");
            out.println("</head>\n<body>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    void epilogue(ServletOutputStream out)
    {
        try {
            out.println("<br>\n</body>\n</html>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
