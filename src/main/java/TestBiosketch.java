import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchType;


public class TestBiosketch
{

    public static void main(String[] args)
    {
        try
        {
            System.out.println(BiosketchType.convert("2"));
            System.out.println(BiosketchType.convert("1"));
            System.out.println(BiosketchType.convert("3"));
            System.out.println(BiosketchType.convert("10"));
            System.out.println(BiosketchType.convert("xyz"));
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }

}
