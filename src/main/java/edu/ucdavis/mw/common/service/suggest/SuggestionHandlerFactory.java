/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SuggestionHandlerFactory.java
 */


package edu.ucdavis.mw.common.service.suggest;

/**
 * @author Stephen Paulsen
 * @param <E>
 *
 */
public interface SuggestionHandlerFactory<E>
{
    /**
     * May return null if no suggestion handler could be found for the topic.
     * @param topic
     * @return a configured SuggestionHandler, a default SuggestionHandler, or <code>null</code>
     */
    public SuggestionHandler<E> getHandler(String topic);

    /**
     * Reset the factory.
     */
    public void reset();
}
