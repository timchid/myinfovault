/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AnnotationsDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.annotation;

import static edu.ucdavis.myinfovault.ConfigReloadConstants.ALL;
import static edu.ucdavis.myinfovault.ConfigReloadConstants.PROPERTIES;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.annotation.Annotation;
import edu.ucdavis.mw.myinfovault.domain.annotation.AnnotationLine;
import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.QueryTool;


/**
 * Handles retrieval of annotation records.
 * Note that old publication and presentation summary records will have a null section id
 * @author R Hendricks
 * @since MIV 4.4
 */
public class AnnotationDaoImpl extends DaoSupport implements AnnotationDao
{
    private static final Logger log = LoggerFactory.getLogger(AnnotationDaoImpl.class);

    // I intend to eventually remove this — for now this will show how many DAOs are
    // created and may inform the choice of instance vs. static for several variables.
    static long instanceNum = 0;
    {
        log.info("Creating Instance #{} of AnnotationDaoImpl", ++instanceNum);
    }

    private Map<String, Map<String, String>> sectionidByNameMap = MIVConfig.getConfig().getMap("sectionidbyname");

    private Map<String,List<String>> sectionTablesByRecordTypeMap = null;

    private static String selectAnnotationsBaseQuery;

    private static String selectAnnotationsByIdSQL;

    private static String selectAnnotationsByUserIdSQL;

    private static String selectAnnotationsByUserIdAndRecordIdSQL;

    private static String selectAnnotationsByUserIdAndRecordTypesPartialSQL;

    private static String selectAnnotationsByUserIdAndRecordTypeForDisplayPartialSQL;

    private static String selectAnnotationsByUserIdAndRecordIdAndRecordTypesSQL;

    private static String selectAnnotationLinesByAnnotationId;

    private static volatile boolean sqlLoaded = false;
    private static volatile boolean sqlLoading = false;


    public AnnotationDaoImpl()
    {
        super("annotations", true);

        if (!sqlLoaded) {
            loadSql(null);
            sqlLoaded = true;
        }

        EventDispatcher.getDispatcher().register(this);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#getAnnotation(int, long)
     */
    @Override
    public Annotation getAnnotationByPacket(int id, long packetId)
    {
        Iterator<Annotation>annotations = getJdbcTemplate().query(selectAnnotationsByIdSQL, new Object[] { id, packetId }, annotationMapper).iterator();
        return annotations.hasNext() ? annotations.next() : null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#getAnnotations(int, long)
     */
    @Override
    public Map<String, Annotation> getAnnotations(final int userId, final long packetId)
    {
        return this.getAnnotationsList(selectAnnotationsByUserIdSQL, userId, packetId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#getAnnotation(int, int, long)
     */
    @Override
    public Annotation getAnnotation(final int userId, final int recordId, final long packetId)
    {
        List<Annotation>annotations =
                getJdbcTemplate().query(selectAnnotationsByUserIdAndRecordIdSQL, new Object[] {userId, recordId, packetId}, annotationMapper);

        return annotations.isEmpty() ? null : this.resolveMutipleAnnotations(annotations, packetId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#getAnnotation(int, int, long, java.lang.String)
     */
    @Override
    public Annotation getAnnotation(final int userId, final int recordId,  final long packetId, final String recType)
    {
        final ArrayList<Object> paramList = new ArrayList<>();
        // All queries have the UserId, just add here...could also have passed to method to add
        paramList.add(userId);
        paramList.add(recordId);
        paramList.add(packetId);

        final String query = buildSectionsQuery(selectAnnotationsByUserIdAndRecordIdAndRecordTypesSQL,
                                                new String[] { recType }, paramList);

        List<Annotation>annotations =
                getJdbcTemplate().query(query, paramList.toArray(), annotationMapper);

        return annotations.isEmpty() ? null : this.resolveMutipleAnnotations(annotations, packetId);
    }


    /**
     * Resolve a single annotation record when there are multiple present pointing to the same target record.
     * The most which should ever be found is two, one master annotation and one packet specific annotation,
     * both of which point to the same target record.
     *
     * If the input packet ID is 0 (zero), it indicates a master packet.
     * If the input packet ID is > 0, it indicates a packet specific annotation. In the case of a packet
     * specific annotation, any annotation lines and notations present for a master packet will be inherited by
     * the packet annotation.
     *
     * @param annotations
     * @param packetId
     * @return Annotation
     */
    private Annotation resolveMutipleAnnotations(final List<Annotation> annotations, final long packetId)
    {
        if (annotations == null || annotations.size() == 0) {
            log.debug("There are no Annotations; returning null");
            return null;
        }
        if (annotations.size() == 1) {
            log.debug("There is only one annotation - nothing to merge - returning:\n{}", annotations.get(0));
            return annotations.get(0);
        }

        // Can potentially return more than one annotation pointing to the same target record...one master and one packet specific,
        // so return only the record which matches the input packet ID
        Annotation masterAnnotation = null;
        Annotation packetAnnotation = null;
        for (Annotation annotation : annotations)
        {
            if (annotation.getPacketId() == 0)
            {
                masterAnnotation = annotation;
                continue;
            }
            packetAnnotation = annotation;
        }

        // Asked for a master annotation. If present, we are done...
        if (packetId == 0 && masterAnnotation != null) {
            return masterAnnotation;
        }
        // or if there is no master annotation there's nothing to resolve.
        else if (packetId > 0 && masterAnnotation == null) {
            log.debug("Packed ID is {} and there is no master annotation.\n\tReturning annotation: {}", packetId, packetAnnotation);
            return packetAnnotation;
        }

        // We are processing a packet specific annotation, which inherits
        // the master packet "lines" and notations
        log.debug("Resolving packet annotation\n{}\n  vs. master annotation\n{}\n", packetAnnotation, masterAnnotation);
        List<AnnotationLine>annotationLines = new ArrayList<>();
        StringBuilder notations = new StringBuilder();

        if (masterAnnotation != null)
        {
            if (masterAnnotation.getLabelAbove() != null) annotationLines.add(masterAnnotation.getLabelAbove());
            if (masterAnnotation.getLabelBelow() != null) annotationLines.add(masterAnnotation.getLabelBelow());
            notations.append(masterAnnotation.getNotation() != null ? masterAnnotation.getNotation() : "");
        }

        Annotation annotation = packetAnnotation != null ? packetAnnotation : masterAnnotation;

        return new Annotation(annotation.getId(),
                    annotation.getUserId(),
                    annotation.getSectionBaseTable(),
                    annotation.getRecordId(),
                    packetId,
                    annotation.getFootnote(),
                    notations.append(annotation.getNotation() != null ? annotation.getNotation() : "").toString(),
                    true,//annotation.getDisplay(),
                    annotationLines);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#getAnnotations(int, long, java.lang.String[])
     */
    @Override
    public Map<String, Annotation> getAnnotations(final int userId, final long packetId, final String ... recTypes)
    {
        final ArrayList<Object> paramList = new ArrayList<>();
        // All queries have the UserId, just add here...could also have passed to method to add
        paramList.add(userId);
        paramList.add(packetId);
        final String query = buildSectionsQuery(selectAnnotationsByUserIdAndRecordTypesPartialSQL, recTypes, paramList);
        return getAnnotationsList(query, paramList.toArray());
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#getAnnotationsForDisplay(int, long, java.lang.String[])
     */
    @Override
    public Map<String, Annotation> getAnnotationsForDisplay(final int userId, final long packetId, final String ... recTypes)
    {
        final ArrayList<Object>paramList = new ArrayList<>();
        // All queries have the UserId, just add here...could also have passed to method to add
        paramList.add(userId);
        paramList.add(packetId);
        final String query = buildSectionsQuery(selectAnnotationsByUserIdAndRecordTypeForDisplayPartialSQL, recTypes, paramList);
        return getAnnotationsList(query, paramList.toArray());
    }


    /**
     * Get the annotations from the database using the given statement and parameters.
     *
     * @param query SQL statement
     * @param params statement parameters
     * @return annotation map
     */
    private Map<String, Annotation> getAnnotationsList(final String query, final Object ... params)
    {
        Map<String, Annotation> recordMap = new HashMap<>();

        for (Annotation annotation : getJdbcTemplate().query(query, params, annotationMapper))
        {
            // See if this record is already in the map
            if (recordMap.containsKey(annotation.getLookupKey()))
            {
                // If the record is already there, check for a packet specific vs master annotation
                // If there is one of each, roll the master annotation lines and notations into the packet specific annotation
                Annotation inMap = recordMap.get(annotation.getLookupKey());
                if (inMap.getPacketId() > 0 && annotation.getPacketId() == 0)
                {
                    inMap.setLineAbove(annotation.getLabelAbove())
                         .setLineBelow(annotation.getLabelBelow())
                         .addNotations(annotation.getNotation());
                    continue;
                }
                else if (inMap.getPacketId() == 0 && annotation.getPacketId() > 0)
                {
                    inMap//.setDisplay(annotation.getDisplay())
                         .setFootnote(annotation.getFootnote())
                         .addNotations(annotation.getNotation())
                         .setPacketId(annotation.getPacketId());
                    continue;
                }
            }

            recordMap.put(annotation.getLookupKey(), annotation);
        }

        return recordMap;
    }


    /**
     * Maps Annotation records to {@link Annotation} objects.
     */
    private final RowMapper<Annotation> annotationMapper = new RowMapper<Annotation>()
    {
        @Override
        public Annotation mapRow(final ResultSet rs, final int rowNum) throws SQLException
        {
            int annotationId = rs.getInt("ID");

            try
            {
                return new Annotation(annotationId,
                                      rs.getInt("UserID"),
                                      rs.getString("SectionBaseTable"),
                                      rs.getInt("RecordID"),
                                      rs.getLong("PacketID"),
                                      rs.getString("Footnote"),
                                      rs.getString("Notation"),
                                      rs.getBoolean("Display"),
                                      getAnnotationLines(annotationId));
            }
            catch (IllegalArgumentException e)
            {
                log.warn("Could not build publication object for record with ID '" + rs.getInt("ID") + "'", e);
            }

            return null;
        }
    };


    /**
     * Maps annotation line records into the {@link AnnotationLines} objects.
     */
    private final RowMapper<AnnotationLine> annotationLineMapper = new RowMapper<AnnotationLine>()
    {
        @Override
        public AnnotationLine mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            try
            {
                return new AnnotationLine(rs.getInt("ID"),
                                          rs.getInt("AnnotationID"),
                                          rs.getString("Label"),
                                          rs.getBoolean("RightJustify"),
                                          rs.getBoolean("DisplayBefore"));
            }
            catch (IllegalArgumentException e)
            {
                log.warn("Could not build AnnotationLine object for AnnotationID '" + rs.getInt("AnnotationID") + "'", e);
            }

            return null;
        }
    };


    /**
     * @param annotationId annotation ID
     * @return annotation line
     */
    private List<AnnotationLine> getAnnotationLines(int annotationId)
    {
        return getJdbcTemplate().query(selectAnnotationLinesByAnnotationId, new Object[] { annotationId }, annotationLineMapper);
    }


    private String buildSectionsQuery(String baseQuery, final String[] recTypes, final List<Object> paramList)
    {
        String savedRecType = "[never set]";
        try
        {
            // Build a map of sectionTables by record type
            if (sectionTablesByRecordTypeMap == null || sectionTablesByRecordTypeMap.isEmpty()) // the 'null' check should be enough now.
            {
                setupSectionTablesByRecordTypeMap();
            }

            String subQuery = "";
            // Build a query of all sections for this record type
            for (final String recordType : recTypes)
            {
                savedRecType = recordType;
                for (String sectionTable : sectionTablesByRecordTypeMap.get(recordType + "-record"))
                {
                    if (!paramList.contains(sectionTable))
                    {
                        paramList.add(sectionTable);
                        subQuery += (subQuery.length() > 0) ? ", ?" : "?";
                    }
                }
            }

            baseQuery = baseQuery.replaceFirst("[#]", subQuery);
            /*System.out.println("baseQuery : "+baseQuery);
            System.out.println("paramList : "+paramList);*/
        }
        // There is an error case where, if the MIVConfig loading of the table maps went awry, the
        // code above could attempt to use a null reference. Catch it here and log it.
        // Doing an "MIV Options > Reload Options" should usually clear the problem.
        catch (NullPointerException e)
        {
            log.error("Exception while processing recordType {}:", savedRecType);
            if (sectionTablesByRecordTypeMap != null) {
                log.error("Got sectionTables [{}] from recordTypeMap [{}]", sectionTablesByRecordTypeMap.get(savedRecType), sectionTablesByRecordTypeMap);
            }
            else {
                log.error("sectionTablesByRecordTypeMap is NULL when trying to get table name");
            }
            log.error("Traceback --", e);
        }

        return baseQuery;
    }



    private final static String GET_ANNOTATIONSLEGEND = "SELECT GROUP_CONCAT(Distinct Notation SEPARATOR '' )  Notation "+
                                                        "FROM Annotations "+
                                                        "WHERE Notation IS NOT NULL AND length(Notation) > 0 AND  UserID=? AND RecordID IN (#) ";

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#getAnnotationsLegend(java.lang.String, java.lang.String)
     */
    @Override
    public List<String> getAnnotationsLegend(final String userId, final String sectionId)
    {
        return getAnnotationsLegend(userId, sectionId, new String[0]);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#getAnnotationsLegend(java.lang.String, java.lang.String, java.lang.String[])
     */
    @Override
    public List<String> getAnnotationsLegend(final String userId, final String sectionId, final String... recordId)
    {
        final Map<String, Map<String, String>> sectionByIdMap = MIVConfig.getConfig().getMap("sectionbyid");
        final Map<String, String> section = sectionByIdMap.get(sectionId);

        List<String> annotationsLegendList = null;
        final QueryTool qt = MIVConfig.getConfig().getQueryTool();
        List<Map<String, String>> resultData = null;
        String query = GET_ANNOTATIONSLEGEND;
        int index = 0;

        String sectionLookupSQL = "SELECT " + section.get("sectionprimarykeyid") +
                                  " FROM " + section.get("fromtable") +
                                  " WHERE " + section.get("whereclause");
        query = query.replaceFirst("[#]", sectionLookupSQL);

        final String[] queryParams = new String[2 + recordId.length];
        queryParams[index++] = queryParams[index++] = userId;

        // Don't need to test recordId[] for null; it's always passed implicitly as zero-length unless NULL is explicitly specified.
        if (recordId.length > 0)
        {
            query += " AND RecordID IN (";

            for (String id : recordId)
            {
                query += "?, ";
                queryParams[index++] = id;
            }

            query = query.substring(0, query.lastIndexOf(", ")) + ")"; // remove the final trailing comma
        }

        resultData = qt.getList(query, queryParams);
        if (resultData != null)
        {
            annotationsLegendList = new ArrayList<>();
            final Map<String, String> map = resultData.get(0);
            annotationsLegendList = getNotationLegend(map.get("notation"));
        }

        log.debug("AnnotationsLegend [{}] : {}", sectionId, annotationsLegendList);
        return annotationsLegendList;
    }


    /**
     * Get the notation legend for notation key.
     *
     * @param notation notation key
     * @return notation legend
     */
    private List<String> getNotationLegend(String notation)
    {
        if (notation == null) return null;

        final List<String> annotationsLegendList = new ArrayList<>();

        if (notation.contains("*")) {
            annotationsLegendList.add("* = Included in the review period.");
        }

        if (notation.contains("x")) {
            annotationsLegendList.add("x = Most significant works.");
        }

        if (notation.contains("+")) {
            annotationsLegendList.add("+ = Major mentoring role.");
        }

        if (notation.contains("@")) {
            annotationsLegendList.add("@ = Refereed.");
        }

        return annotationsLegendList;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#deleteAnnotationLine(int)
     */
    @Override
    public boolean deleteAnnotationLine(int id) throws DataAccessException
    {
        return getJdbcTemplate().update( getSQL("lines.delete"), new Object[] { id } ) > 0;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao#insertAnnotation(edu.ucdavis.mw.myinfovault.domain.annotation.Annotation, int)
     */
    @Override
    public boolean insertAnnotation(final Annotation annotation, final int insertUserId)
    {
        KeyHolder keyHolder = new GeneratedKeyHolder();


        int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement( getSQL("insert"), Statement.RETURN_GENERATED_KEYS );

                ps.setInt(1, annotation.getUserId());
                ps.setInt(2, annotation.getRecordId());
                ps.setString(3, annotation.getSectionBaseTable());
                ps.setLong(4, annotation.getPacketId());
                ps.setString(5, annotation.getFootnote());
                ps.setString(6, annotation.getNativeNotations());
                ps.setBoolean(7, true/*annotation.getDisplay()*/);
                ps.setInt(8, insertUserId);
                return ps;
            }
        }, keyHolder);

        // Now add any annotationLines
        if (rowsAffected > 0)
        {
            List<AnnotationLine>annotationLines = new ArrayList<AnnotationLine>();
            if (annotation.getLabelAbove() != null) annotationLines.add(annotation.getLabelAbove());
            if (annotation.getLabelBelow() != null) annotationLines.add(annotation.getLabelBelow());

            // None to add, we are done
            if (annotationLines.isEmpty()) return true;

            // annotationId of the newly added annotation
            final int annotationId = keyHolder.getKey().intValue();
            final ArrayList<AnnotationLine>annotationLinesToAdd = new ArrayList<>(annotationLines);

            int[] updateCounts = this.getJdbcTemplate().batchUpdate( getSQL("lines.insert"), new BatchPreparedStatementSetter() {

                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException
                {
                    AnnotationLine line = annotationLinesToAdd.get(i);

                    ps.setInt(1, annotationId);
                    ps.setString(2, line.getLabel());
                    ps.setBoolean(3, line.isRightJustify());
                    ps.setBoolean(4, line.isDisplayBefore());
                    ps.setInt(5, insertUserId);
                }

                @Override
                public int getBatchSize()
                {
                    return annotationLinesToAdd.size();
                }
            });

            return rowsAffected > 0 && updateCounts.length > 0;
        }

        log.error("Failed to add {} annotation record for user {}", annotation.getSectionBaseTable(), annotation.getUserId());
        return false;
    }


    /**
     * <strong>NOTE</strong> that this implementation is not even <em>attempting</em> to handle
     * the possiblity that there may have been changes to an {@link AnnotationLine}.<br>
     * I added this method which is initially only called when batch-updating Notation strings,
     * so Lines and Footnotes will not have been changed.<br>
     * This method <em>should</em> be generally useful, but will have to have code added to
     * manage changes to the annotation lines.
     */
    @Override
    public int updateAnnotation(final Annotation annotation, final int updateUserId)
    {
        return getJdbcTemplate().update( new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement( getSQL("update") );

                ps.setString(1, annotation.getFootnote());
                ps.setString(2, annotation.getNativeNotations());
                ps.setInt(3, updateUserId);

                ps.setInt(4, annotation.getId());
                ps.setInt(5, annotation.getUserId());
                ps.setLong(6, annotation.getPacketId());
                ps.setInt(7, annotation.getRecordId());
                ps.setString(8, annotation.getSectionBaseTable());

                return ps;
            }
        });

    }


    @Override
    public int deleteAnnotation(final Annotation annotation, final int ownerId)
    {
        log.info("Going to delete this annotation record:\n{}", annotation);
        return deleteAnnotation( annotation.getId(), annotation.getPacketId(), ownerId );
    }

    /**
     * Delete the Annotation with the given record id number.
     * This relies on foreign key cascade to delete any annotation lines when this record is deleted.
     * @param annotationId
     */
    @Override
    public int deleteAnnotation(final int annotationId, final long packetId, final int ownerId)
    {
        return getJdbcTemplate().update( new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement( getSQL("delete") );

                ps.setInt(1, annotationId);
                ps.setLong(2, packetId);
                ps.setInt(3, ownerId);
                log.info("PreparedStatement for Delete is [[{}]]", ps);

                return ps;
            }
        });
    }


    @Override
    public void persistAnnotation(final Annotation annotation, final long packetId, final int ownerId, final int actorId)
    {
        // Do nothing if this annotation is not for the specified packet number.
        if (annotation.getPacketId() != packetId) return;

        // Existing annotation has a positive record ID, update or delete it.
        if (annotation.getId() > 0)
        {
            if (log.isDebugEnabled()) {
                log.debug("LabelAbove is /{}/  ¿ Is it empty ? ... {}", annotation.getLabelAbove(), isEmpty(annotation.getLabelAbove()));
                log.debug("LabelBelow is /{}/  ¿ Is it empty ? ... {}", annotation.getLabelBelow(), isEmpty(annotation.getLabelBelow()));
                log.debug("Footnote is /{}/  ¿ Is it empty ? ... {}", annotation.getFootnote(), isEmpty(annotation.getFootnote()));
                log.debug("Notations is /{}/  ¿ Is it empty ? ... {}", annotation.getNativeNotations(), isEmpty(annotation.getNativeNotations()));
            }

            // If there's nothing left in the Annotation after editing it, delete the reord.
            if (
                    isEmpty(annotation.getLabelAbove(),
                            annotation.getLabelBelow(),
                            annotation.getFootnote(),
                            annotation.getNativeNotations())
               )
            {
                final int deleteCount = this.deleteAnnotation(annotation, ownerId);
                log.info("Deleted {} notations\n", deleteCount);
            }
            // Otherwise update.
            else
            {
                final int updateCount = updateAnnotation(annotation, actorId);
                log.info("Updated {} notations\n", updateCount);
            }
        }
        else // new Annotation has a record ID of -1, insert a new record for it.
        {
            insertAnnotation(annotation, actorId);
        }
    }


    /**
     * Check to see if <strong>all</strong> items provided are "empty".
     * Empty is defined as being null or, if a String-ish thing, being length 0 or all whitespace.
     * @param items
     * @return
     */
    static boolean isEmpty(Object...items)
    {
        if (items == null || items.length == 0) return true;

        // If there's only one item...
        if (items.length == 1) {
            // ... and it's null, this is empty
            if (items[0] == null) return true;
            // ... not null, but if it's String-ish see if it's an empty string
            if (items[0] instanceof CharSequence) return isEmpty( (CharSequence)items[0] );
            // ... otherwise it's a not-null Object of some kind, so it's not empty.
            return false;
        }

        // More than one item, it's empty if every item is empty.
        return isEmpty( Arrays.asList(items) );
    }
    private static boolean isEmpty(List<Object> list)
    {
        return list == null || list.isEmpty() ||
                ( isEmpty( list.get(0) ) && isEmpty( list.subList(1, list.size()) ) )
                ;
    }
    private static boolean isEmpty(CharSequence s)
    {
        return s == null || s.toString().trim().length() == 0;
    }


    /**
     * Build a map of sectionTables by record type
     */
    private void setupSectionTablesByRecordTypeMap()
    {
        this.sectionTablesByRecordTypeMap = new HashMap<>();

        for (String key : sectionidByNameMap.keySet())
        {
            final Map<String,String> section = sectionidByNameMap.get(key);
            if (!sectionTablesByRecordTypeMap.containsKey(section.get("recordname")))
            {
                sectionTablesByRecordTypeMap.put(section.get("recordname"), new ArrayList<String>());
            }

            if (!(sectionTablesByRecordTypeMap.get(section.get("recordname"))).contains(section.get("sectionbasetable")))
            {
                (sectionTablesByRecordTypeMap.get(section.get("recordname"))).add(section.get("sectionbasetable"));
            }
        }
    }


    /*
     * Before I re-wrote this, all of these SQL statement variables were final or static final,
     * caching the SQL once it was loaded.  This meant that runtime changes couldn't be made,
     * because changes to the sql.properties file wouldn't be picked up -- the variables had
     * already been set and would never be set again; a restart was needed.
     * I made these static but not final, and re-load them when a ConfigurationChangeEvent
     * takes place. This is tricky though, because the DaoSupport class, of which this is a
     * subclass, also subscribes to config changes, and that is where the re-reading of the
     * sql.properties file takes place.
     *
     * Now, it depends on *which* @Subscribed method gets called first.
     * If DaoSupport.configurationChanged() is called first, it re-reads the sql.properties,
     * and AnnotationDaoImpl.loadSql() is called and re-fetches the statements using getSQL()
     * then all is fine.
     * If the calls happen the other way around, then this loadSql() will be getting the *unchanged*
     * statements, *before* they are re-read from sql.properties.  The "Reload Options" button
     * will have to be pressed *twice* for the changes to be seen.
     *
     * This can actually all be avoided by *not* caching the statements.  Instead of doing
     * things like this:
     *     getJdbcTemplate().update(deleteAnnotationLineById, params);
     * instead just get the statement every time:
     *     getJdbcTemplate().update(getSQL("lines.delete"), params);
     * Calls to DaoSupport.getSQL() are *fast* because it is just a get from cached Properties,
     * which is a simple Hashtable lookup and is O(1)
     *
     * DaoSupport itself will react to a ConfigurationChangeEvent and the next time your code
     * uses getSQL(<something>) it will pick up what DaoSupport has re-loaded from the changed
     * properties file.
     *
     * This whole essay applies to *any* class that extends DaoSupport, and *many* of those
     * existing classes are over-caching and will not see config changes.
     *
     *
     *
     * When creating some-new-DAO implementation, I strongly suggest the following pattern:
     *
     * Create FooberDaoImpl that will use sql.properties keys like myinfovault.foobar.*
     *     myinfovault.foobar.insert.basic=INSERT INTO FoobarTable (...etc...)
     *     myinfovault.foobar.insert.complex=INSERT INTO FoobarTable (...etc...)
     *     myinfovault.foobar.delete=DELETE FROM FoobarTable WHERE (...etc...)
     *     (and so on)
     * Class definition:
     *     public class FoobarDaoImpl extends DaoSupport implements FoobarDao
     * No-arg constructor:
     *     public FoobarDaoImpl()
     *     {
     *         super("foobar", true) // use a sub-schema on the default schema -> "myinfovault.foobar"
     *     }
     * In some add-a-record method, use the remaining key part to get the SQL statement...
     *     PreparedStatement ps = connection.prepareStatement( getSQL("insert.basic", Statement.RETURN_GENERATED_KEYS );
     * because you registered a sub-schema the statement key text is simple...
     *     getSQL("insert.basic")
     *     getSQL("delete")
     *
     * I have changed deleteAnnotationLine() and insertAnnotation() above to work this way,
     * and AcademicActionDaoImpl has been completely converted as an example.
     */
    @Subscribe
    public void loadSql(ConfigurationChangeEvent event)
    {
        if (sqlLoading) return; else sqlLoading = true; // if someone else is currently loading the properties then let *them* do it.

        if (event == null || event.getWhatChanged() == ALL || event.getWhatChanged() == PROPERTIES)
        {
            log.info("Loading SQL statements, event is [{}]", event);


            selectAnnotationsBaseQuery = getSQL("select.base") + " ";

            selectAnnotationsByIdSQL =
                    selectAnnotationsBaseQuery + getSQL("select.id");

            selectAnnotationsByUserIdSQL =
                    selectAnnotationsBaseQuery + getSQL("select.userid");

            selectAnnotationsByUserIdAndRecordIdSQL =
                    selectAnnotationsBaseQuery + getSQL("select.userandrecid");

            selectAnnotationsByUserIdAndRecordTypesPartialSQL =
                    selectAnnotationsBaseQuery + getSQL("select.userandtype");

            selectAnnotationsByUserIdAndRecordTypeForDisplayPartialSQL =
                    selectAnnotationsBaseQuery + getSQL("select.userfordisplay");

            selectAnnotationsByUserIdAndRecordIdAndRecordTypesSQL =
                    selectAnnotationsBaseQuery + getSQL("select.userandrecidandtypes");

            selectAnnotationLinesByAnnotationId = getSQL("select.annotationid");
        }
        sqlLoading = false;
    }
}
