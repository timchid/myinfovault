/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchStyleDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.biosketch;

import java.sql.SQLException;

import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle;

/**
 * TODO: missing javadoc
 * 
 * @author dreddy
 * @since MIV 2.1
 */
public interface BiosketchStyleDao
{
    /**
     * Returns the default biosketch style for a particular biosketchType.
     * 
     * @param biosketchType
     * @return
     * @throws SQLException
     */
    public BiosketchStyle findDefaultBiosketchStyle(int biosketchType) throws SQLException;

    /**
     * Returns the biosketch style associated with a particular style ID.
     * 
     * @param styleID
     * @return
     * @throws SQLException
     */
    public BiosketchStyle findBiosketchStyleById(int styleID) throws SQLException;

    /**
     * Inserts the biosketch style and returns the auto-generated biosketch style ID.
     * 
     * @param biosketchStyle
     * @return
     * @throws SQLException
     */
    public int insertBiosketchStyle(BiosketchStyle biosketchStyle) throws SQLException;

    /**
     * Updates the biosketch style record.
     * 
     * @param biosketchStyle
     * @throws SQLException
     */
    public void updateBiosketchStyle(BiosketchStyle biosketchStyle) throws SQLException;

    /**
     * Deletes the biosketch style record.
     * 
     * @param styleID
     * @throws SQLException
     */
    public void deleteBiosketchStyle(int styleID) throws SQLException;
}
