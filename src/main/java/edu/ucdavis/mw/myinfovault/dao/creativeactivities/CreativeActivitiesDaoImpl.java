/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CreativeActivitiesDaoImpl.java
 */


package edu.ucdavis.mw.myinfovault.dao.creativeactivities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.QueryTool;

/**
 * Implementation class of CreativeActivitiesDao
 * Methods used to access creative activities data
 *
 * @author pradeeph
 * @since MIV 4.4
 */
public class CreativeActivitiesDaoImpl implements CreativeActivitiesDao
{
    QueryTool qt = MIVConfig.getConfig().getQueryTool();
    private static Logger log = LoggerFactory.getLogger(CreativeActivitiesDaoImpl.class);

    private static final String NonAssociatedEventsByUserIdAndWorkId =
        " SELECT CONCAT('events',E.ID) `ID`, ET.Description,"
        + " DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') As EventStartDate, DATE_FORMAT(E.EventEndDate,'%m/%d/%Y') As EventEndDate,"
        + " E.Venue, E.Country, E.Province, E.City, YEAR(E.EventStartDate) `Year`,"
        + " CONCAT(DATE_FORMAT(E.EventStartDate,'%m/%d/%Y'),ifnull(CONCAT('&nbsp;',ET.Description),''),ifnull(CONCAT('&nbsp;',E.Venue),'')/*,IFNULL(CONCAT('&nbsp;',E.City),''),IFNULL(CONCAT('&nbsp;',E.Country),''),IFNULL(CONCAT('&nbsp;',E.Province),'')*/) `Value`"
        + " FROM Events E, EventType ET"
        + " WHERE E.EventTypeID = ET.ID AND E.UserID=?"
        + " AND E.ID NOT IN ( SELECT WEA.EventID FROM WorksEventsAssociation WEA WHERE WEA.UserID=E.UserID AND WEA.WorkID IN (#) )"
        + " ORDER BY E.EventStartDate";

    private static final String AssociatedEventsByUserIdAndWorkId = " SELECT CONCAT('events',E.ID) `ID`,ET.Description,DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') As EventStartDate,DATE_FORMAT(E.EventEndDate,'%m/%d/%Y') As EventEndDate,E.Venue,E.Country,E.Province,E.City,YEAR(E.EventStartDate) `Year`, "
                                    + " CONCAT(DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') ,ifnull(CONCAT('&nbsp;',ET.Description),''),ifnull(CONCAT('&nbsp;',E.Venue),'')/*,IFNULL(CONCAT('&nbsp;',E.City),''),IFNULL(CONCAT('&nbsp;',E.Country),''),IFNULL(CONCAT('&nbsp;',E.Province),'')*/) `Value`"
                                    + " FROM Events E, WorksEventsAssociation WEA, EventType ET "
                                    + " WHERE E.ID = WEA.EventID AND E.EventTypeID = ET.ID AND E.UserID=? AND WEA.UserID=E.UserID AND WEA.WorkID IN (#) "
                                    + " ORDER BY E.EventStartDate";

    private static final String  EventsByUserId = " SELECT CONCAT('events',E.ID) `ID`,ET.Description,DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') As EventStartDate,DATE_FORMAT(E.EventEndDate,'%m/%d/%Y') As EventEndDate,E.Venue,E.Country,E.Province,E.City,YEAR(E.EventStartDate) `Year`, "
                                    + " CONCAT(DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') ,ifnull(CONCAT('&nbsp;',ET.Description),''),ifnull(CONCAT('&nbsp;',E.Venue),'')/*,IFNULL(CONCAT('&nbsp;',E.City),''),IFNULL(CONCAT('&nbsp;',E.Country),''),IFNULL(CONCAT('&nbsp;',E.Province),'')*/) `Value`"
                                    + " FROM Events E, EventType ET "
                                    + " WHERE E.EventTypeID = ET.ID AND E.UserID=?"
                                    + " ORDER BY E.EventStartDate";

    private static final String NonAssociatedPublicationEventsByUserIdAndWorkId = " SELECT CONCAT('publicationevents',PE.ID) `ID`,PS.Description,PE.Year,PE.Editors as Author,PE.Title,PE.Volume,PE.Issue,PE.Pages,PE.City,PE.Province,PE.Country,"
                                    + " CONCAT(PE.Year,'&nbsp;',PE.Title,'&nbsp;',PE.Publication) `Value`"
                                    + " FROM PublicationEvents PE, PublicationStatus PS  "
                                    + " WHERE PE.StatusID = PS.ID AND PE.UserID=?  "
                                    + " AND PE.ID NOT IN ( SELECT WPA.PublicationEventID FROM WorksPublicationEventsAssociation WPA WHERE WPA.UserID=PE.UserID AND WPA.WorkID IN (#) )"
                                    + " ORDER BY PE.Year";

    private static final String AssociatedPublicationEventsByUserIdAndWorkId = " SELECT CONCAT('publicationevents',PE.ID) `ID`,PS.Description,PE.Year,PE.Editors as Author,PE.Title,PE.Volume,PE.Issue,PE.Pages,PE.City,PE.Province,PE.Country,"
                                    + " CONCAT(PE.Year,'&nbsp;',PE.Title,'&nbsp;',PE.Publication) `Value`"
                                    + " FROM PublicationEvents PE, WorksPublicationEventsAssociation WPA, PublicationStatus PS "
                                    + " WHERE PE.ID = WPA.PublicationEventID AND PE.StatusID = PS.ID AND PE.UserID=? AND WPA.UserID=PE.UserID AND WPA.WorkID IN (#) "
                                    + " ORDER BY PE.Year";

    private static final String  PublicationEventsByUserId = " SELECT CONCAT('publicationevents',PE.ID) `ID`,PS.Description,PE.Year,PE.Editors as Author,PE.Title,PE.Volume,PE.Issue,PE.Pages,PE.City,PE.Province,PE.Country,"
                                    + " CONCAT(PE.Year,'&nbsp;',PE.Title,'&nbsp;',PE.Publication) `Value`"
                                    + " FROM PublicationEvents PE, PublicationStatus PS "
                                    + " WHERE PE.StatusID = PS.ID AND PE.UserID=? "
                                    + " ORDER BY PE.Year";

    private static final String NonAssociatedWorksByUserIdAndEventId = " SELECT CONCAT('works',W.ID) `ID`,W.WorkYear,W.Creators,W.Title,WT.Description,W.WorkYear `Year`, "
                                    + " CONCAT(W.WorkYear,'&nbsp;',WT.Description,'&nbsp;',W.Title,'&nbsp;',W.Creators) `Value` "
                                    + " FROM Works W, WorkType WT  "
                                    + " WHERE W.WorkTypeID = WT.ID AND W.UserID=? "
                                    + " AND W.ID NOT IN ( SELECT WEA.WorkID FROM WorksEventsAssociation WEA WHERE WEA.UserID=W.UserID AND WEA.EventID IN (#) ) "
                                    + " ORDER BY W.WorkYear";

    private static final String AssociatedWorksByUserIdAndEventId = " SELECT CONCAT('works',W.ID) `ID`,W.WorkYear,W.Creators,W.Title,WT.Description,W.WorkYear `Year`, "
                                    + " CONCAT(W.WorkYear,'&nbsp;',WT.Description,'&nbsp;',W.Title,'&nbsp;',W.Creators) `Value`"
                                    + " FROM Works W, WorksEventsAssociation WEA, WorkType WT "
                                    + " WHERE W.ID = WEA.WorkID AND W.WorkTypeID = WT.ID AND WEA.UserID=W.UserID AND W.UserID=? AND WEA.EventID IN (#) "
                                    + " ORDER BY W.WorkYear";

    private static final String NonAssociatedWorksByUserIdAndPublicationEventId = " SELECT CONCAT('works',W.ID) `ID`,W.WorkYear,W.Creators,W.Title,WT.Description,W.WorkYear `Year`, "
                                    + " CONCAT(W.WorkYear,'&nbsp;',WT.Description,'&nbsp;',W.Title,'&nbsp;',W.Creators) `Value` "
                                    + " FROM Works W, WorkType WT  "
                                    + " WHERE W.WorkTypeID = WT.ID AND W.UserID=? "
                                    + " AND W.ID NOT IN ( SELECT WEA.WorkID FROM WorksPublicationEventsAssociation WEA WHERE WEA.UserID=W.UserID AND WEA.PublicationEventID IN (#) ) ";

    private static final String AssociatedWorksByUserIdAndPublicationEventId = " SELECT CONCAT('works',W.ID) `ID`,W.WorkYear,W.Creators,W.Title,WT.Description,W.WorkYear `Year`, "
                                    + " CONCAT(W.WorkYear,'&nbsp;',WT.Description,'&nbsp;',W.Title,'&nbsp;',W.Creators) `Value`"
                                    + " FROM Works W, WorksPublicationEventsAssociation WEA, WorkType WT "
                                    + " WHERE W.ID = WEA.WorkID AND W.WorkTypeID = WT.ID AND WEA.UserID=W.UserID AND W.UserID=? AND WEA.PublicationEventID IN (#) "
                                    + " ORDER BY W.WorkYear";

    private static final String  WorksByUserId = " SELECT CONCAT('works',W.ID) `ID`,W.WorkYear,W.Creators,W.Title,WT.Description,W.WorkYear `Year`, "
                                    + " CONCAT(W.WorkYear,'&nbsp;',WT.Description,'&nbsp;',W.Title,'&nbsp;',W.Creators) `Value`"
                                    + " FROM Works W, WorkType WT "
                                    + " WHERE W.WorkTypeID = WT.ID AND W.UserID=?"
                                    + " ORDER BY W.WorkYear";

    private static final String  NonAssociatedReviewEventsByUserIdAndReviewId = " SELECT CONCAT('events',E.ID) `ID`,ET.Description,DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') As EventStartDate,DATE_FORMAT(E.EventEndDate,'%m/%d/%Y') As EventEndDate,E.Venue,E.Country,E.Province,E.City,YEAR(E.EventStartDate) `Year`, "
                                    + " CONCAT(DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') ,ifnull(CONCAT('&nbsp;',ET.Description),''),ifnull(CONCAT('&nbsp;',E.Venue),'')/*,IFNULL(CONCAT('&nbsp;',E.City),''),IFNULL(CONCAT('&nbsp;',E.Country),''),IFNULL(CONCAT('&nbsp;',E.Province),'')*/) `Value`"
                                    + " FROM Events E, EventType ET "
                                    + " WHERE E.EventTypeID = ET.ID AND E.UserID=?"
                                    + " AND E.ID NOT IN (SELECT ERA.EventID FROM EventsReviewsAssociation ERA WHERE ERA.UserID=E.UserID AND ERA.ReviewID IN (#))"
                                    + " ORDER BY E.EventStartDate";

    private static final String  AssociatedReviewEventsByUserIdAndReviewId = " SELECT CONCAT('events',E.ID) `ID`,ET.Description,DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') As EventStartDate,DATE_FORMAT(E.EventEndDate,'%m/%d/%Y') As EventEndDate,E.Venue,E.Country,E.Province,E.City,YEAR(E.EventStartDate) `Year`, "
                                    + " CONCAT(DATE_FORMAT(E.EventStartDate,'%m/%d/%Y') ,ifnull(CONCAT('&nbsp;',ET.Description),''),ifnull(CONCAT('&nbsp;',E.Venue),'')/*,IFNULL(CONCAT('&nbsp;',E.City),''),IFNULL(CONCAT('&nbsp;',E.Country),''),IFNULL(CONCAT('&nbsp;',E.Province),'')*/) `Value`"
                                    + " FROM Events E, EventsReviewsAssociation ERA, EventType ET  "
                                    + " WHERE E.ID = ERA.EventID AND E.EventTypeID = ET.ID AND ERA.UserID=E.UserID AND E.UserID=? AND ERA.ReviewID IN (#)"
                                    + " ORDER BY E.EventStartDate";

    private static final String  NonAssociatedReviewWorksByUserIdAndReviewId = " SELECT CONCAT('works',W.ID) `ID`,W.WorkYear,W.Creators,W.Title,WT.Description,W.WorkYear `Year`, "
                                    + " CONCAT(W.WorkYear,'&nbsp;',WT.Description,'&nbsp;',W.Title,'&nbsp;',W.Creators) `Value`"
                                    + " FROM Works W, WorkType WT  "
                                    + " WHERE W.WorkTypeID = WT.ID AND W.UserID=?"
                                    + " AND W.ID NOT IN (SELECT WRA.WorkID FROM WorksReviewsAssociation WRA WHERE WRA.UserID=W.UserID AND WRA.ReviewID IN (#))"
                                    + " ORDER BY W.WorkYear";

    private static final String  AssociatedReviewWorksByUserIdAndReviewId = " SELECT CONCAT('works',W.ID) `ID`,W.WorkYear,W.Creators,W.Title,WT.Description,W.WorkYear `Year`, "
                                    + " CONCAT(W.WorkYear,'&nbsp;',WT.Description,'&nbsp;',W.Title,'&nbsp;',W.Creators) `Value`"
                                    + " FROM Works W, WorksReviewsAssociation WRA, WorkType WT "
                                    + " WHERE W.ID = WRA.WorkID AND W.WorkTypeID = WT.ID AND WRA.UserID=W.UserID AND W.UserID=? AND WRA.ReviewID IN (#)"
                                    + " ORDER BY W.WorkYear";

    /**
     * Get the list of non associated events for work records.
     * @param UserId
     * @param workids array of workids
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getNonAssociatedEventsByUserIdAndWorkIds(int UserId, int... workids)
    {
        return executeAssociatedQuery(NonAssociatedEventsByUserIdAndWorkId, UserId, workids);
    }


    /**
     * Get the list of associated events for work records.
     * @param UserId
     * @param workids array of workids
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getAssociatedEventsByUserIdAndWorkIds(int UserId, int... workids)
    {
        return executeAssociatedQuery(AssociatedEventsByUserIdAndWorkId, UserId, workids);
    }


    /**
     * Get the list of associated publicationevents for work records.
     * @param UserId
     * @param workids array of workids
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getAssociatedPublicationEventsByUserIdAndWorkIds(int UserId, int... workids)
    {
        return executeAssociatedQuery(AssociatedPublicationEventsByUserIdAndWorkId, UserId, workids);
    }


    /**
     * Get the list of non associated publicationevents for work records.
     * @param UserId
     * @param workids array of workids
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getNonAssociatedPublicationEventsByUserIdAndWorkIds(int UserId, int... workids)
    {
        return executeAssociatedQuery(NonAssociatedPublicationEventsByUserIdAndWorkId, UserId, workids);
    }


    /**
     * Get the list of events by user
     * @param UserId
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getEventsByUserId(int UserId)
    {
        return qt.getList(EventsByUserId, String.valueOf(UserId));
    }


    /**
     * Get the list of publicationevents by user
     * @param UserId
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getPublicationEventsByUserId(int UserId)
    {
        return qt.getList(PublicationEventsByUserId, String.valueOf(UserId));
    }


    /**
     * Get the list of non associated works for event records.
     * @param UserId
     * @param eventids array of eventids
     * @return List map of works data
     */
    @Override
    public List<Map<String, String>> getNonAssociatedWorksByUserIdAndEventIds(int UserId, int... eventids)
    {
        return executeAssociatedQuery(NonAssociatedWorksByUserIdAndEventId, UserId, eventids);
    }


    /**
     * Get the list of associated works for event records.
     * @param UserId
     * @param eventids array of eventids
     * @return List map of works data
     */
    @Override
    public List<Map<String, String>> getAssociatedWorksByUserIdAndEventIds(int UserId, int... eventids)
    {
        return executeAssociatedQuery(AssociatedWorksByUserIdAndEventId, UserId, eventids);
    }


    /**
     * Get the list of non associated works for publication event records.
     * @param UserId
     * @param eventids array of publicationeventids
     * @return List map of works data
     */
    @Override
    public List<Map<String, String>> getNonAssociatedWorksByUserIdAndPublicationEventIds(int UserId, int... eventids)
    {
        return executeAssociatedQuery(NonAssociatedWorksByUserIdAndPublicationEventId, UserId, eventids);
    }


    /**
     * Get the list of associated works for publication event records.
     * @param UserId
     * @param eventids array of publicationeventids
     * @return List map of works data
     */
    @Override
    public List<Map<String, String>> getAssociatedWorksByUserIdAndPublicationEventIds(int UserId, int... eventids)
    {
        return executeAssociatedQuery(AssociatedWorksByUserIdAndPublicationEventId, UserId, eventids);
    }


    /**
     * Get the list of works by user
     * @param UserId
     * @return List map of works data
     */
    @Override
    public List<Map<String, String>> getWorksByUserId(int UserId)
    {
        return qt.getList(WorksByUserId, String.valueOf(UserId));
    }


    /**
     * Get the list of non associated events for review records.
     * @param UserId
     * @param reviewids array of reviewids
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getNonAssociatedReviewEventsByUserIdAndReviewIds(int UserId, int... reviewids)
    {
        return executeAssociatedQuery(NonAssociatedReviewEventsByUserIdAndReviewId, UserId, reviewids);
    }


    /**
     * Get the list of associated events for review records.
     * @param UserId
     * @param reviewids array of reviewids
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getAssociatedReviewEventsByUserIdAndReviewIds(int UserId, int... reviewids)
    {
        return executeAssociatedQuery(AssociatedReviewEventsByUserIdAndReviewId, UserId, reviewids);
    }


    /**
     * Get the list of non associated works for review records.
     * @param UserId
     * @param reviewids array of reviewids
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getNonAssociatedReviewWorksByUserIdAndReviewIds(int UserId, int... reviewids)
    {
        return executeAssociatedQuery(NonAssociatedReviewWorksByUserIdAndReviewId, UserId, reviewids);
    }


    /**
     * Get the list of associated works for review records.
     * @param UserId
     * @param reviewids array of reviewids
     * @return List map of events data
     */
    @Override
    public List<Map<String, String>> getAssociatedReviewWorksByUserIdAndReviewIds(int UserId, int... reviewids)
    {
        return executeAssociatedQuery(AssociatedReviewWorksByUserIdAndReviewId, UserId, reviewids);
    }


    /**
     * Execute associated query
     * This method expects to be passed an SQL statement that includes
     * a number-sign to indicate where a list of ID numbers should be placed.
     * @param query
     * @param parentId
     * @param ids
     * @return query result list
     */
    private List<Map<String, String>> executeAssociatedQuery(String query, int parentId, int... ids)
    {
        StringBuilder queryPart = new StringBuilder();
        String[] params = new String[ids.length + 1];

        int count = 0;
        params[count++] = String.valueOf(parentId);
        if (ids != null)
        {
            for (int i : ids)
            {
                params[count++] = String.valueOf(i);
                if (queryPart.length() > 0) {
                    queryPart.append(",");
                }
                queryPart.append("?");
            }
        }

        query = query.replaceFirst("#", queryPart.toString());

        log.debug("query :: {}", query);

        List<Map<String, String>> resultList = qt.getList(query, params);
        return resultList;
    }


    private static final String EventIdsByUserIdAndWorkYear = "SELECT CONCAT('events',ID) `ID` FROM Events"
                                            + " WHERE UserID=? AND YEAR(EventStartDate) >= ?";

    private static final String WorkIdsByUserIdAndEventYear = "SELECT CONCAT('works',ID) `ID` FROM Works"
                                        + " WHERE UserID=? AND WorkYear<=?";

    private static final String WorkAndEventIdsByUserIdAndReviewYear = "SELECT CONCAT('works',ID) `ID` FROM Works"
                                        + " WHERE UserID=? AND WorkYear<=?"
                                        + " UNION"
                                        + " SELECT CONCAT('events',ID) `ID` FROM Events"
                                        + " WHERE UserID=? AND YEAR(EventStartDate)<=? ";

    private static final String EventAndPublicationEventsIdsByUserIdAndWorkYear = "SELECT CONCAT('events',ID) `ID` FROM Events"
                                        + " WHERE UserID=? AND YEAR(EventStartDate) >=?"
                                        + " UNION"
                                        + " SELECT CONCAT('publicationevents',ID) `ID` FROM PublicationEvents"
                                        + " WHERE UserID=? AND Year >=?";

    /**
     * Get the list of event ids for a give user and selected year
     * @param UserId
     * @param year
     * @return list of ids
     */
    @Override
    public List<String> getValidEventIdsByUserIdAndWorkYear(int UserId, int year)
    {
        return getIdList(qt.getList(EventIdsByUserIdAndWorkYear, String.valueOf(UserId), String.valueOf(year)), "id");
    }

    /**
     * Get the list of work ids for a give user and selected year
     * @param UserId
     * @param year
     * @return list of ids
     */
    @Override
    public List<String> getValidWorkIdsByUserIdAndEventYear(int UserId, int year)
    {
        return getIdList(qt.getList(WorkIdsByUserIdAndEventYear, String.valueOf(UserId), String.valueOf(year)), "id");
    }

    /**
     * Get the list of work and event ids for a give user and selected year
     * @param UserId
     * @param year
     * @return list of ids
     */
    @Override
    public List<String> getValidWorkAndEventIdsByUserIdAndReviewYear(int UserId, int year)
    {
        return getIdList(qt.getList(WorkAndEventIdsByUserIdAndReviewYear, String.valueOf(UserId), String.valueOf(year),
                                    String.valueOf(UserId), String.valueOf(year)), "id");
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.creativeactivities.CreativeActivitiesDao#getValidEventAndPublicationEventsIdsByUserIdAndWorkYear(int, int)
     */
    @Override
    public List<String> getValidEventAndPublicationEventsIdsByUserIdAndWorkYear(int UserId, int year)
    {
        return getIdList(qt.getList(EventAndPublicationEventsIdsByUserIdAndWorkYear, String.valueOf(UserId), String.valueOf(year),
                                    String.valueOf(UserId), String.valueOf(year)), "id");
    }


    private List<String> getIdList(List<Map<String,String>> list, String idcolumn)
    {
        List<String> idlist = null;

        if (list != null)
        {
            idlist = new ArrayList<String>();

            for (Map<String,String> map : list)
            {
                if (map != null && map.get(idcolumn.toLowerCase()) != null) {
                    idlist.add(map.get(idcolumn).toLowerCase());
                }
            }
        }

        return idlist;
    }


    private static final String  AssociatedEventsCountForWork =
        "SELECT WEA.EventID `ID` FROM WorksEventsAssociation WEA WHERE WEA.WorkID in (?)";

    private static final String  AssociatedWorksCountForEvent =
        "SELECT WEA.WorkID `ID` FROM WorksEventsAssociation WEA WHERE WEA.EventID in (?)";

    private static final String  AssociatedPublicationEventsCountForWork =
        "SELECT WEA.PublicationEventID `ID` FROM WorksPublicationEventsAssociation WEA WHERE WEA.WorkID in (?)";

    private static final String  AssociatedPublicationEventsCountForPublicationEvent =
        "SELECT WEA.WorkID `ID` FROM WorksPublicationEventsAssociation WEA WHERE WEA.PublicationEventID in (?)";

    private static final String  DeleteWorksEventsAssociationByWorkIds =
        "DELETE FROM WorksEventsAssociation WHERE WorkID IN (#)";

    private static final String  DeleteWorksEventsAssociationByEventIds =
        "DELETE FROM WorksEventsAssociation WHERE EventID IN (#)";

    private static final String  DeleteWorksPublicationEventsAssociationByWorkIds =
        "DELETE FROM WorksPublicationEventsAssociation WHERE WorkID IN (#)";

    private static final String  DeleteWorksPublicationEventsAssociationByPublicationEventIds =
        "DELETE FROM WorksPublicationEventsAssociation WHERE PublicationEventID IN (#)";


    /**
     * Save Works Associations for events or publicationevents
     * @param actorUserId 
     * @param ownerUserId
     * @param reqData
     * @return ResultVO
     */
    public ResultVO saveWorksAssociation(int actorUserId, int ownerUserId, Map<String, String[]> reqData)
    {

        ResultVO resultVO = null;
        boolean associationModified = true;
        if (log.isDebugEnabled())
        {
            System.out.println("===================== Save Method : saveWorksAssociation ====================");
            System.out.println("userId :: " + ownerUserId);
            System.out.println("reqData :: " + reqData);
        }

        String events = (reqData.get("events") != null ? reqData.get("events")[0] : null);
        String publicationevents = (reqData.get("publicationevents") != null ? reqData.get("publicationevents")[0] : null);
        int[] ids = null;
        String saveids = "";
        int workid = (reqData.get("workid") != null ? Integer.parseInt(reqData.get("workid")[0]) : -1);

        if (events != null)
        {
            ids = splitToIntArray(events, ",");

            // check for modification on Association
            associationModified = isAssociationModified(AssociatedEventsCountForWork, workid, ids);

            if (associationModified)
            {
                deleteAssociation(DeleteWorksEventsAssociationByWorkIds, workid);
                if (ids != null && ids.length > 0)
                {
                    saveids = saveAssociation(actorUserId, ownerUserId, "WorksEventsAssociation", "WorkID", "EventID", new int[] { workid }, ids);
                }
            }
        }

        if (publicationevents != null)
        {
            ids = splitToIntArray(publicationevents, ",");

            // check for modification on Association
         // check for modification on Association
            associationModified = isAssociationModified(AssociatedPublicationEventsCountForWork, workid, ids);

            if (associationModified)
            {
                deleteAssociation(DeleteWorksPublicationEventsAssociationByWorkIds, workid);
                if (ids != null && ids.length > 0)
                {
                    saveids = saveAssociation(actorUserId, ownerUserId, "WorksPublicationEventsAssociation", "WorkID", "PublicationEventID", new int[] { workid }, ids);
                }

            }
        }

        if (saveids != null && saveids.length() > 0)
        {
            resultVO = new ResultVO(true, Arrays.asList("Success"), null, saveids);
        }

        log.debug("associationModified :: {} -- resultVO :: {}", associationModified, resultVO);

        return resultVO;
    }


    /**
     * Save Association between works and events
     * @param actorUserId 
     * @param ownerUserId
     * @param reqData
     * @return request execution results
     */
    public ResultVO saveEventsAssociation(int actorUserId, int ownerUserId, Map<String, String[]> reqData)
    {

        ResultVO resultVO = null;
        boolean associationModified = true;
        if (log.isDebugEnabled())
        {
            System.out.println("===================== Save Method : saveEventsAssociation ====================");
            System.out.println("userId :: " + ownerUserId);
            System.out.println("reqData :: " + reqData);
        }

        String works = (reqData.get("works") != null ? reqData.get("works")[0] : null);

        int[] ids = null;
        String saveids = "";
        if (works != null)
        {
            int eventid = (reqData.get("eventid") != null ? Integer.parseInt(reqData.get("eventid")[0]) : -1);
            ids = splitToIntArray(works, ",");

            // check for modification on Association
            associationModified = isAssociationModified(AssociatedWorksCountForEvent, eventid, ids);

            if (associationModified)
            {
                deleteAssociation(DeleteWorksEventsAssociationByEventIds, eventid);
                if (ids != null && ids.length > 0)
                {
                    saveids = saveAssociation(actorUserId, ownerUserId, "WorksEventsAssociation", "EventID", "WorkID", new int[] { eventid }, ids);
                }
            }

        }

        if (saveids != null && saveids.length() > 0)
        {
            resultVO = new ResultVO(true, Arrays.asList("Success"), null, saveids);
        }

        log.debug("associationModified :: {} -- resultVO :: {}", associationModified, resultVO);

        return resultVO;
    }


    /**
     * Save Association between works and publicationevents
     * @param actorUserId 
     * @param ownerUserId
     * @param reqData
     * @return request execution results
     */
    public ResultVO savePublicationEventsAssociation(int actorUserId, int ownerUserId, Map<String, String[]> reqData)
    {

        ResultVO resultVO = null;
        boolean associationModified = true;
        if (log.isDebugEnabled())
        {
            System.out.println("===================== Save Method : savePublicationEventsAssociation ====================");
            System.out.println("userId :: " + ownerUserId);
            System.out.println("reqData :: " + reqData);
        }

        String works = (reqData.get("works") != null ? reqData.get("works")[0] : null);
        log.debug("works :: {}", works);

        int[] ids = null;
        String saveids = "";
        if (works != null)
        {
            int publicationeventid = (reqData.get("publicationeventid") != null ? Integer.parseInt(reqData.get("publicationeventid")[0]) : -1);
            log.debug("publicationeventid :: {}", publicationeventid);
            ids = splitToIntArray(works, ",");

            // check for modification on Association
            associationModified = isAssociationModified(AssociatedPublicationEventsCountForPublicationEvent, publicationeventid, ids);
            log.debug("associationModified :: {}", associationModified);
            if (associationModified)
            {
                deleteAssociation(DeleteWorksPublicationEventsAssociationByPublicationEventIds, publicationeventid);
                if (ids != null && ids.length > 0)
                {
                    saveids = saveAssociation(actorUserId, ownerUserId, "WorksPublicationEventsAssociation", "PublicationEventID", "WorkID", new int[] { publicationeventid }, ids);
                }
            }
        }

        if (saveids != null && saveids.length() > 0)
        {
            resultVO = new ResultVO(true, Arrays.asList("Success"), null, saveids);
        }

        log.debug("associationModified :: {} -- resultVO :: {}", associationModified, resultVO);

        return resultVO;
    }


    private static final String AssociatedEventsCountForReview =
        "SELECT ERA.EventID `ID` FROM EventsReviewsAssociation ERA WHERE ERA.ReviewID in (?)";

    private static final String AssociatedWorksCountForReview =
        "SELECT WRA.WorkID `ID` FROM WorksReviewsAssociation WRA WHERE WRA.ReviewID in (?)";

    private static final String DeleteEventsAssociationByReviewIds =
        "DELETE FROM EventsReviewsAssociation WHERE ReviewID IN (#)";

    private static final String DeleteWorksAssociationByReviewIds =
        "DELETE FROM WorksReviewsAssociation WHERE ReviewID IN (#)";


    /**
     * Save Reviews Associations for works or events
     * @param actorUserId 
     * @param ownerUserId
     * @param reqData
     * @return ResultVO
     */
    public ResultVO saveReviewsAssociation(int actorUserId, int ownerUserId, Map<String, String[]> reqData)
    {
        ResultVO resultVO = null;
        boolean associationModified = true;
        if (log.isDebugEnabled())
        {
            System.out.println("===================== Save Method : saveReviewsAssociation ====================");
            System.out.println("userId :: " + ownerUserId);
            System.out.println("reqData :: " + reqData);
        }

        String events = (reqData.get("events") != null ? reqData.get("events")[0] : null);
        String works = (reqData.get("works") != null ? reqData.get("works")[0] : null);
        int[] ids = null;
        String saveids = "";
        int reviewid = (reqData.get("reviewid") != null ? Integer.parseInt(reqData.get("reviewid")[0]) : -1);

        if (events != null)
        {
            ids = splitToIntArray(events, ",");

            // check for modification on Association
            associationModified = isAssociationModified(AssociatedEventsCountForReview, reviewid, ids);

            if (associationModified)
            {
                deleteAssociation(DeleteEventsAssociationByReviewIds, reviewid);
                if (ids != null && ids.length > 0)
                {
                    saveids = saveAssociation(actorUserId, ownerUserId, "EventsReviewsAssociation", "EventID", "ReviewID", ids, new int[] { reviewid });
                }

            }
        }

        if (works != null)
        {
            ids = splitToIntArray(works, ",");

            // check for modification on Association
            associationModified = isAssociationModified(AssociatedWorksCountForReview, reviewid, ids);

            if (associationModified)
            {
                deleteAssociation(DeleteWorksAssociationByReviewIds, reviewid);
                if (ids != null && ids.length > 0)
                {
                    saveids = saveAssociation(actorUserId, ownerUserId, "WorksReviewsAssociation", "WorkID", "ReviewID", ids, new int[] { reviewid });
                }
            }
        }

        if (saveids != null && saveids.length() > 0)
        {
            resultVO = new ResultVO(true, Arrays.asList("Success"), null, saveids);
        }

        log.debug("associationModified :: {} -- resultVO :: {}", associationModified, resultVO);

        return resultVO;
    }


    /**
     * Check for modified association
     * @param query
     * @param parentId
     * @param ids
     * @return if association has been modified
     */
    private boolean isAssociationModified(String query, int parentId, int... ids)
    {
        boolean associationModified = true;

        try
        {
            LinkedHashMap<String, Map<String,String>> resultMap = qt.runQuery(query, "ID", String.valueOf(parentId));

            log.debug("isAssociationModified:\n  query :: {}\n  resultMap :: {}\n  ids :: {}", new Object[] { query, resultMap, ids });
//            if (debug)
//            {
//                System.out.println("query :: "+query);
//                System.out.println("resultMap :: "+resultMap);
//                System.out.println("ids :: "+ids);
//            }

            if (resultMap != null && resultMap.size() > 0)
            {
                // no selected records : Associations has been modified
                if (ids == null)
                {
                    return true;
                }

                // saved records count != selected records count : Associations has been modified process records
                if (resultMap.size() != ids.length) { return true; }

                // otherwise compare selected records Id with saved records
                String strID = "";
                for (int id : ids)
                {
                    strID = String.valueOf(id);
                    if (resultMap.get(strID) != null) // remove matched record
                    {
                        resultMap.remove(strID);
                    }
                }

                // if resultMap is empty : do nothing
                associationModified = resultMap.size() > 0;
            }
            else
            {
                // no saved records and no selected records : do nothing
                return ids != null && ids.length > 0;
            }
        }
        catch (Exception e) // FIXME: do NOT catch plain Exception. What is this catching?
        {
            // do nothing
            e.printStackTrace(); // FIXME: do NOT use printStackTrace()
        }
        return associationModified;
    }


    /**
     * Cast Ids string into id array.
     * QUESTION: Is the incoming String just numbers like "123,345,567" or does it
     *           also contain text, like "work123,work345,work567"?
     *           If it only contains numbers then the regex Pattern and Matcher
     *           is not needed. If it contains text then that should have been
     *           documented as part of the Javadoc.
     *           The at-param makes it appear it's just numbers.
     * @param idList delimiter separated ids string i.e 123,345,567,678
     * @param delimiter
     * @return split cast IDs
     */
    private int[] splitToIntArray(String idList, String delimiter)
    {
        if (idList == null || idList.trim().length() == 0) { return null; }

        Pattern p = Pattern.compile("\\d+");
        Matcher m = null;

        String[] stringIds = idList.split(delimiter);
        int[] intIds = new int[stringIds.length];

        int index = 0;
        for (String string : stringIds)
        {
            m = p.matcher(string);
            if (m.find())
            {
                intIds[index++] = Integer.parseInt(m.group());
            }
        }

        return intIds;
    }


    /**
     * Save association into given table
     * association applies on two fields
     * field1 and field2 contains ids array of values1 and values2 respectively
     * @param ownerUserId
     * @param tableName
     * @param field1
     * @param field2
     * @param values1
     * @param values2
     * @return comma separated id string (No ID numbers, all are -1)
     */
    private String saveAssociation(int actorUserId, int ownerUserId, String tableName, String field1, String field2, int[] values1, int[] values2)
    {
// My updates here won't work until the UserID column is added to all of the Association tables...
//   alter table WorksEventsAssociation add column UserID INT NOT NULL FIRST;
        String[] fields = { "UserID", field1, field2 };
        String[] values = null;//new String[3]; // This initial array was never used. A new one is allocated in the inner loop.
        int result = -1;
        /*
         * Note: because the association tables do not have an auto-increment ID column,
         * keys are not actually generated, so all of the returned "ID numbers" are
         * actually -1 (negative one). Only the length of the results list is meaningful.
         */
        StringBuilder resultsList = new StringBuilder();

        if (values1 != null)
        {
            for (int workid : values1)
            {
                if (values2 != null)
                {
                    for (int eventId : values2)
                    {
                        if (workid != -1 && eventId != -1)
                        {
                            values = new String[3];
                            values[0] = String.valueOf(ownerUserId);
                            values[1] = String.valueOf(workid);
                            values[2] = String.valueOf(eventId);
//                            result = qt.insert(ownerUserId, tableName, fields, values);
                            result = qt.insert(actorUserId, tableName, fields, values);

                            if (resultsList.length() > 0) {
                                resultsList.append(", ");
                            }
                            resultsList.append(result);
                        }
                    }
                }
            }
        }

        return resultsList.toString();
    }


    /**
     * @param userId
     * @param reqData
     * @return request execution results
     */
    public ResultVO saveWorkType(int userId, Map<String, String[]> reqData)
    {
        ResultVO resultVO = null;

        String description = (reqData.get("description") != null ? reqData.get("description")[0] : null);
        //String shortdescription = (reqData.get("shortdescription") != null ? reqData.get("shortdescription")[0] : null);

        String shortdescription = "";
        int saveid = saveTypes(userId, "WorkType", "Description", "ShortDescription", description, shortdescription);

        if (saveid > 0)
        {
            resultVO = new ResultVO(true, Arrays.asList("Success"), null, String.valueOf(saveid));
        }

        log.debug("saveWorkType - resultVO :: {}", resultVO);

        return resultVO;
    }


    /**
     * @param userId
     * @param reqData
     * @return request execution results
     */
    public ResultVO saveEventType(int userId, Map<String, String[]> reqData)
    {
        ResultVO resultVO = null;

        String description = (reqData.get("description") != null ? reqData.get("description")[0] : null);
        //String shortdescription = (reqData.get("shortdescription") != null ? reqData.get("shortdescription")[0] : null);

        String shortdescription = "";
        int saveid = saveTypes(userId, "EventType", "Description", "ShortDescription", description, shortdescription);

        if (saveid > 0)
        {
            resultVO = new ResultVO(true, Arrays.asList("Success"), null, String.valueOf(saveid));
        }

        log.debug("saveEventType - resultVO :: {}", resultVO);

        return resultVO;
    }


    /**
     * @param userId
     * @param reqData
     * @return request execution results
     */
    public ResultVO saveEventCategory(int userId, Map<String, String[]> reqData)
    {
        ResultVO resultVO = null;

        String description = (reqData.get("description") != null ? reqData.get("description")[0] : null);
        //String shortdescription = (reqData.get("shortdescription") != null ? reqData.get("shortdescription")[0] : null);

        String shortdescription = "";
        int saveid = saveTypes(userId, "EventCategory", "Description", "ShortDescription", description, shortdescription);

        if (saveid > 0)
        {
            resultVO = new ResultVO(true, Arrays.asList("Success"), null, String.valueOf(saveid));
        }

        log.debug("saveEventCategory - resultVO :: {}", resultVO);

        return resultVO;
    }


    /**
     * Save types based on userid, tablename, fields and values
     * it first checks the availability of type record
     *
     * @param userId
     * @param tableName
     * @param field1
     * @param field2
     * @param values1
     * @param values2
     * @return record id as Int
     */
    private int saveTypes(int userId, String tableName, String field1, String field2, String values1, String values2)
    {
//        int saveId = -1;

        // check for available types
        int saveId = lookupTypes(userId, tableName, field1, values1);

        if (saveId == -1)
        {
            // need to save this record
            String[] fields = { field1, field2 };
            String[] values = { StringUtil.capitalizeFully(values1), StringUtil.capitalizeFully(values2) };
            saveId = qt.insert(userId, tableName, fields, values);
        }

        return saveId;
    }


    /**
     * Checks the availability of type record
     *
     * @param userId
     * @param tableName
     * @param field1
     * @param values1
     * @return record id as Int
     */
    private int lookupTypes(int userId, String tableName, String field1, String values1){

        int saveId = -1;
        String query = "SELECT ifnull((SELECT ID FROM " + tableName + " WHERE trim(" + field1 + ")=? ),-1) `ID`";

        log.debug("lookupTypes query: [{}]", query);

        List<Map<String, String>> resultList = qt.getList(query, values1.trim());
        if (resultList != null)
        {
            saveId = Integer.parseInt(resultList.get(0).get("id"));
        }

        return saveId;
    }


    /**
     * Delete Association by list of ids.
     * This method expects to be passed an SQL 'DELETE' statement that includes
     * a number-sign to indicate where the list of ID numbers should be placed.
     * @param query the SQL 'DELETE' statement
     * @param ids the ID numbers of records to delete
     * @return the number of records that were deleted
     */
    private int deleteAssociation(String query, int... ids)
    {
//        String QuertPart = "";
        StringBuilder queryPart = new StringBuilder();
        String[] params = new String[ids.length + 1];

        int count = 0;
        if (ids != null)
        {
            for (int i : ids)
            {
                params[count++] = String.valueOf(i);
                if (queryPart.length() > 0) {
                    queryPart.append(",");
                }
                queryPart.append("?");
//                if (QuertPart.length() == 0)
//                {
//                    QuertPart = "?";
//                }
//                else
//                {
//                    QuertPart += ",?";
//                }
            }
        }

//        query = query.replaceFirst("#", QuertPart);
        query = query.replaceFirst("#", queryPart.toString());
        log.debug("deleteAssociation query: [{}]", query);

        int deleteCount = qt.delete(query, params);
        return deleteCount;
    }

}
