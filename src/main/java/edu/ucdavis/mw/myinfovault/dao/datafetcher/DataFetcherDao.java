package edu.ucdavis.mw.myinfovault.dao.datafetcher;

import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.myinfovault.data.DataMap;

/**
 * TODO: add javadoc
 *
 * @author Rick Hendricks
 * @since MIV 4.6
 */
public interface DataFetcherDao
{
    /**
     * Get the DataMap containing the user name
     * @param userID - ID of the user for which to retrieve the name
     * @return DataMap
     */
    public DataMap getWholeNameMap(int userID);

    /**
     * Get the primary department for a user. If there is no department, the school
     * name is returned.
     * @param userID ID of the user for which to retrieve the department
     * @return String - The department name.
     */
    public String getDepartment(int userID);

    /**
     * Get the additional information header records. This method will select the SystemSectionHeader records
     * and then select the UserSectionHeader records, combining the two in order to replace the
     * default header value with the user defined header.
     * @param userID - The userID for which to retrieve the header records
     * @return - DataMap of the additional information header records
     */
    public DataMap getHeaderMap(int userID);

    /**
     * Get additional information records for a given user and headerID. The headerID will have
     * been previously retrieved from the AdditionalHeader table.
     * @param userID - The userID for which to retrieve the additional information
     * @param headerID - The AdditionalHeader ID of the records to retrieve for the user
     * @param sectionID - The section ID of the records to retrieve for the user
     * @param packet - Packet to to which the data belongs
     * @return List of Maps representing the records retrieved
     */
    public List<Map<String,Object>> getAdditionalInformation(int userID, int headerID, int sectionID, Packet packet);

    /**
     * Get the sections for a given documentID and dossierID. The section records are then used to build queries
     * to retrieve the section data.
     * @param documentID - The documentID of the documents to retrieve
     * @return List of Maps representing the records retrieved
     */
    public List<Map<String,Object>> getSectionsByDocumentID(int documentID);

    /**
     * Get the sections associated with the input record name
     * @param name - The section name, as defined in the Name column of the Section table,
     * for which to retrieve the associated sections.
     * @return List of Maps representing the records retrieved
     */
    public List<Map<String,Object>> getAssociatedSectionsByName(String name);

    /**
     * Get the section data for a user.
     * @param userID - The user for which to retrieve the section data
     * @param sectionRecordMap - A map of the
     * @param packet - Packet to to which the data belongs
     * @return List of Maps representing the records retrieved
     */
    public List<Map<String,Object>> getSectionData(int userID, Map<String,Object> sectionRecordMap, Packet packet);

    /**
     * Get all data in a given section for a given user.
     *
     * @param sectionId of the section to fetch.
     * @param userId of the user we are fetching data for.
     * @return A list of maps which are the result of the unique section query for that section.
     */
    public List<Map<String, Object>> getSectionBySectionId(Integer sectionId, Integer userId);


    /**
     * Get the uploaded documents for a document.
     *
     * @param userID of the user we are fetching data for.
     * @param documentID of the document for which to retrieve the uploads.
     * @param packet - Packet to to which the data belongs
     * @return List of upload files.
     */
    public List<Map<String, Object>> getPacketUploadFiles(int userID, int documentID, Packet packet);


}
