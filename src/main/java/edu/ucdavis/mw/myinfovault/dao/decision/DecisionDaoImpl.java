/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.decision;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.decision.DecisionType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Handles {@link Decision} persistence.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class DecisionDaoImpl extends DaoSupport implements DecisionDao
{
    private final String selectDecisionSql;
    private final String selectDecisionByDossierSql;
    private final String selectDecisionByIdSql;
    private final String insertDecisionSql;
    private final String updateDecisionSql;
    private final String deleteDecisionSql;

    /**
     * Creates the decision DAO object and initializes it with SQL
     * for handling the persistence of {@link Decision} objects.
     */
    public DecisionDaoImpl()
    {
        selectDecisionSql = getSQL("decision.select");
        selectDecisionByDossierSql = getSQL("decision.select.dossier");
        selectDecisionByIdSql = getSQL("decision.select.id");
        insertDecisionSql = getSQL("decision.insert");
        updateDecisionSql = getSQL("decision.update");
        deleteDecisionSql = getSQL("decision.delete");
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.decision.DecisionDao#getDecisions(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<Decision> getDecisions(Dossier dossier)
    {
        return getDecisions(dossier,
                            selectDecisionByDossierSql,
                            dossier.getDossierId());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.decision.DecisionDao#getDecision(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.util.MivDocument, int, int)
     */
    @Override
    public Decision getDecision(final Dossier dossier,
                                final MivDocument documentType,
                                int schoolId,
                                int departmentId)
    {
        return getFirst(getDecisions(dossier,
                                     selectDecisionSql,
                                     dossier.getDossierId(),
                                     documentType.getId(),
                                     schoolId,
                                     departmentId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.decision.DecisionDao#getDecision(int)
     */
    @Override
    public Decision getDecision(int recordId)
    {
        return getFirst(getDecisions(null,
                                     selectDecisionByIdSql,
                                     recordId));
    }

    /**
     * Get the decisions for the given SQL statement and parameters.
     *
     * @param dossier decision dossier
     * @param sql SQL statement
     * @param params statement parameters
     * @return found decisions
     */
    private List<Decision> getDecisions(final Dossier dossier,
                                        String sql,
                                        Object...params)
    {
        try
        {
            return get(sql, new RowMapper<Decision>() {
                @Override
                public Decision mapRow(ResultSet rs, int rowNum) throws SQLException
                {
                    try
                    {
                        String type = rs.getString("Decision");

                        return new Decision(dossier != null ? dossier : MivServiceLocator.getDossierService().getDossier(rs.getLong("DossierID")),
                                            rs.getInt("ID"),
                                            MivDocument.get(rs.getInt("DocumentType")),
                                            type != null ? DecisionType.valueOf(type) : DecisionType.NONE,
                                            rs.getBoolean("ContraryToCommittee"),
                                            rs.getString("Comment"),
                                            rs.getInt("SchoolID"),
                                            rs.getInt("DepartmentID"));
                    }
                    catch (WorkflowException e)
                    {
                        throw new MivSevereApplicationError("errors.dossier.read", e, rs.getLong("DossierID"));
                    }
                }
            }, params);
        }
        catch(DataAccessException e)
        {
            throw new MivSevereApplicationError("Failed to get Decision(s)", e);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.decision.DecisionDao#persistDecision(edu.ucdavis.mw.myinfovault.domain.decision.Decision)
     */
    @Override
    public boolean persistDecision(final Decision decision)
    {
        final boolean isUpdate = decision.getDocumentId() > 0;

        KeyHolder keyHolder = new GeneratedKeyHolder();

        try
        {
            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(isUpdate ? updateDecisionSql : insertDecisionSql,
                                                                       Statement.RETURN_GENERATED_KEYS);

                    ps.setString(1, decision.getComments());
                    ps.setString(2, decision.getDecisionType().name());
                    ps.setBoolean(3, decision.isContraryToCommittee());

                    if (isUpdate)
                    {
                        ps.setInt(4, decision.getDocumentId());
                    }

                    return ps;
                }
            }, keyHolder);

            // persist occurred
            if (rowsAffected > 0)
            {
                // update decision record ID
                if (!isUpdate)
                {
                    decision.setDocumentId(keyHolder.getKey().intValue());
                }

                return true;
            }
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.decision.dao.write", exception, decision);
        }

        return false;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.decision.DecisionDao#deleteDecision(edu.ucdavis.mw.myinfovault.domain.decision.Decision)
     */
    @Override
    public boolean deleteDecision(Decision decision)
    {
        try
        {
            return update(deleteDecisionSql, decision.getDocumentId());
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.decision.dao.write", exception, decision);
        }
    }
}
