/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.dossier;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.AbstractInterruptibleBatchPreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.dossier.DossierReviewType;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeType;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCStatus;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierAppointmentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierAppointmentType;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierFileDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto.EntityType;
import edu.ucdavis.mw.myinfovault.service.dossier.MivWorkflowConstants;
import edu.ucdavis.mw.myinfovault.service.dossier.SchoolDepartmentCriteria;
import edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.mw.myinfovault.service.signature.SignatureStatus;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotDto;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVInformation;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PathConstructor;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;


/**
 * Handles dossier persistence.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class DossierDaoImpl extends DaoSupport implements DossierDao
{
    private WorkflowInfo workflowInfo = new WorkflowInfo();
    private Map<String, List<UploadDocumentDto>> uploadDocumentMap = null;
    private SigningService signingService = null;
    private DCService dcService = null;
    private UserService userService = null;

    private final String LOAD_BASE_DATA_SQL;
    private final String REVIEWERS_QUERY_SCH_DEPT_SQL;
    private final String DOSSIERS_TO_REVIEW_SQL;
    private final String UPDATE_INSERT_REVIEWERS_SQL;
    private final String UPDATE_DELETE_REVIEWERS_SQL;
    private final String DELETE_REVIEWERS_SQL;
    private final String DOSSIERS_FOR_SNAPSHOTS_SQL;
    private final String DOSSIERSNAPSHOT_DELETE_SQL;
    private final String UPDATE_DOSSIER_LOCATION_SQL;
    private final String UPDATE_DOSSIER_SQL;
    private final String INSERT_DOSSIER_SQL;
    private final String DOSSIERATTRIBUTES_QUERY_FOR_LIST;
    private final String DOSSIERATTRIBUTES_DELETE_SQL;
    private final String UPDATE_ATTRIBUTES_LIST_SQL;
    private final String INSERT_JOINTAPPOINTMENTS_SQL;
    private final String DOSSIER_DELETE_SQL;
    private final String INSERT_ATTRIBUTES_SQL;
    private final String UPDATE_JOINTAPPOINTMENTS_SQL;
    private final String DOCUMENTQUERY;
    private final String UPLOAD_DOCUMENT_QUERY;
    private final String UPLOAD_DOCUMENT_SCH_DEPT_QUERY;
    private final String DELETE_UPLOAD_DOCUMENT;
    private final String GET_SNAPSHOT_SQL;
    private final String GET_ALL_SNAPSHOTS_SQL;
    private final String UPDATE_SNAPSHOT_SQL;
    private final String INSERT_SNAPSHOT_SQL;
    private final String INSERT_UPLOAD_SQL;
    private final String UPLOAD_SEQUENCE_SQL;
    private final String UPDATE_UPLOAD_SQL;
    private final String DOSSIERS;
    private final String ACTIVE_DOSSIERS;
    private final String FIND_DOSSIERS_BY_LOCATION_PARTIAL_SQL;
    private final String DOSSIERATTRIBUTES_QUERY_WITH_CRITERIA;
    private final String DOSSIERATTRIBUTES_SUBQUERY_WITH_1CRITERIA;
    private final String DOSSIERATTRIBUTES_SUBQUERY_WITH_1NOT_CRITERIA;
    private final String DOSSIERATTRIBUTES_SUBQUERY_WITH_2CRITERIA;
    private final String DOSSIERATTRIBUTES_SUBQUERY_WITH_2NOT_CRITERIA;
    private final String DOSSIERATTRIBUTES_SUBQUERY_FOR_DEANS_DECISION;

    private final String LOAD_ACADEMIC_ACTION_SQL;
    private final String INSERT_ACADEMIC_ACTION_SQL;
    private final String UPDATE_ACADEMIC_ACTION_SQL;


    private static final String SELECT_BY_LOCATION_BASE = "SELECT DISTINCT d.DossierId " +
                                                          "FROM Dossier d LEFT JOIN AcademicAction aa ON d.AcademicActionID=aa.ID LEFT JOIN JointAppointments j ON d.DossierId = j.DossierId " +
                                                          "WHERE isUserActive(aa.UserID) = TRUE " +
                                                          "AND d.WorkflowStatus != '" + MivWorkflowConstants.ROUTE_HEADER_CANCEL_CD + "'";

    // Partial statements! Will be appended with conditions in findUsers()
    private static final String FIND_DOSSIERS_PARTIAL_SQL = "SELECT DossierID " +
                                                            "FROM UserAccount u, Dossier d, AcademicAction aa " +
                                                            "WHERE d.WorkflowStatus != '"+MivWorkflowConstants.ROUTE_HEADER_CANCEL_CD+"' " +
                                                            "AND u.UserId=aa.UserId AND d.AcademicActionID=aa.ID";

    /**
     * Creates the dossier DAO object and initializes it with SQL
     * for handling the persistence of {@link Dossier} objects.
     */
    public DossierDaoImpl()
    {
        LOAD_BASE_DATA_SQL = getSQL("dossier.select");
        REVIEWERS_QUERY_SCH_DEPT_SQL = getSQL("dossierreviewers.select.department");
        DOSSIERS_TO_REVIEW_SQL = getSQL("dossierreviewers.select.entity");
        UPDATE_INSERT_REVIEWERS_SQL = getSQL("dossierreviewers.insert");
        UPDATE_DELETE_REVIEWERS_SQL = getSQL("dossierreviewers.delete.id");
        DELETE_REVIEWERS_SQL = getSQL("dossierreviewers.delete.dossier");
        DOSSIERS_FOR_SNAPSHOTS_SQL = getSQL("dossiersnapshot.select.distinct");
        DOSSIERSNAPSHOT_DELETE_SQL = getSQL("dossiersnapshot.delete");
        UPDATE_DOSSIER_LOCATION_SQL = getSQL("dossier.update.location");
        UPDATE_DOSSIER_SQL = getSQL("dossier.update");
        INSERT_DOSSIER_SQL = getSQL("dossier.insert");
        DOSSIERATTRIBUTES_QUERY_FOR_LIST = getSQL("dossierattributes.select.type");
        DOSSIERATTRIBUTES_DELETE_SQL = getSQL("dossierattributes.select");
        UPDATE_ATTRIBUTES_LIST_SQL = getSQL("dossierattributes.update");
        INSERT_JOINTAPPOINTMENTS_SQL = getSQL("jointappointments.insert");
        DOSSIER_DELETE_SQL = getSQL("dossier.delete");
        INSERT_ATTRIBUTES_SQL = getSQL("dossierattributes.insert");
        UPDATE_JOINTAPPOINTMENTS_SQL = getSQL("jointappointments.update");
        DOCUMENTQUERY = getSQL("dossierview.select");
        UPLOAD_DOCUMENT_QUERY = getSQL("useruploaddocument.select");
        UPLOAD_DOCUMENT_SCH_DEPT_QUERY = getSQL("useruploaddocument.select.scope");
        DELETE_UPLOAD_DOCUMENT = getSQL("useruploaddocument.delete");
        GET_SNAPSHOT_SQL = getSQL("dossiersnapshot.delete.scope");
        GET_ALL_SNAPSHOTS_SQL = getSQL("dossiersnapshot.select");
        UPDATE_SNAPSHOT_SQL = getSQL("dossiersnapshot.update");
        INSERT_SNAPSHOT_SQL = getSQL("dossiersnapshot.insert");
        INSERT_UPLOAD_SQL = getSQL("useruploaddocument.insert");
        UPLOAD_SEQUENCE_SQL = getSQL("useruploaddocument.select.count");
        UPDATE_UPLOAD_SQL = getSQL("useruploaddocument.update");
        DOSSIERS = getSQL("dossier.select.dossierid");
        ACTIVE_DOSSIERS = getSQL("dossier.select.active");
        FIND_DOSSIERS_BY_LOCATION_PARTIAL_SQL = getSQL("dossier.select.location");
        DOSSIERATTRIBUTES_QUERY_WITH_CRITERIA = getSQL("dossier.select.criteria");
        DOSSIERATTRIBUTES_SUBQUERY_WITH_1CRITERIA = getSQL("dossierattributes.select.sub.criteria");
        DOSSIERATTRIBUTES_SUBQUERY_WITH_1NOT_CRITERIA = getSQL("dossierattributes.select.sub.notcriteria");
        DOSSIERATTRIBUTES_SUBQUERY_WITH_2CRITERIA = getSQL("dossierattributes.select.sub.twocriteria");
        DOSSIERATTRIBUTES_SUBQUERY_WITH_2NOT_CRITERIA = getSQL("dossierattributes.select.sub.nottwocriteria");
        DOSSIERATTRIBUTES_SUBQUERY_FOR_DEANS_DECISION = getSQL("dossierattributes.select.sub.decision");

        LOAD_ACADEMIC_ACTION_SQL = getSQL("academicaction.select");
        INSERT_ACADEMIC_ACTION_SQL = getSQL("academicaction.insert");
        UPDATE_ACADEMIC_ACTION_SQL = getSQL("academicaction.update.dossierid");

    }

    /**
     * Spring injected SigningService bean
     *
     * @param signingService
     */
    public void setSigningService(SigningService signingService)
    {
        this.signingService = signingService;
    }

    /**
     * Spring injected DCService bean
     *
     * @param dcService
     */
    public void setDcService(DCService dcService)
    {
        this.dcService = dcService;
    }

    /**
     * Spring injected UserService bean
     *
     * @param userService
     */
    public void setuserService(UserService userService)
    {
        this.userService = userService;
    }


    /* (non-Javadoc)
     *  @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getNextDossierId()
     */
    @Override
    public Long updateNextDossierId()
    {
        // Do the query
        return this.getJdbcTemplate().queryForObject("SELECT nextval('dossierid_sequence') AS next_sequence FOR UPDATE", Long.class);
    }


    /*
     * (non-Javadoc)
     *
     * @see
     * edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getDossierAndLoadAllData
     * (long)
     */
    @Override
    public Dossier getDossierAndLoadAllData(final long dossierId) throws WorkflowException, SQLException
    {
        return loadAllDossierData(dossierId);
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getDossier(long)
     */
    @Override
    public Dossier getDossier(final long dossierId) throws WorkflowException
    {
        return loadDossierBaseData(dossierId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getDossiersByWorkflowLocation(edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public List<Long> getDossiersByWorkflowLocation(final DossierLocation location) throws WorkflowException
    {

        String sql = "select distinct d.DossierId from Dossier d where d.WorkflowStatus != '"+MivWorkflowConstants.ROUTE_HEADER_CANCEL_CD+"' and WorkflowLocations = '"+location.mapLocationToWorkflowNodeName()+"'";
        List<Map<String, Object>> dossierResults = this.getJdbcTemplate().queryForList(sql);
        List<Long> dossierList = new ArrayList<Long>();

        for (Map<String, Object> dossierRecordMap : dossierResults)
        {
            dossierList.add(new Long((Integer) dossierRecordMap.get("DossierID")));
        }

        return dossierList;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getArchivedDossiers(java.lang.String)
     */
    @Override
    public List<Long> getArchivedDossiers() throws SQLException
    {
        return this.getArchivedDossiers(0, 0);
    }


    /**
     * getArchivedDossiers - Get dossiers which are at the Archived workflow location
     * based on the input school and department id.
     *
     * Both School and Department id's of 0 = retrieve all archived dossiers
     * School id specified with Department id of 0 = retrieve all archived dossiers for the school
     * School id and Department id specified = retrieve all archived for the school and department
     * There should be no situation where only the department id is specified as this would retrieve
     * dossiers with the same department id in different schools.
     *
     * @param school id for which to retrieve archived dossiers
     * @param department id for which to retrieve archived dossiers
     * @return list of dossier ids
     * @throws SQLException
     */
    private List<Long> getArchivedDossiers(final int school, final int department)
    {
        final StringBuilder sql = new StringBuilder();
        final List<Object> params = new ArrayList<Object>();
        final List<Integer> paramTypes = new ArrayList<Integer>();

        // No department or school specified, get all
        if (school == 0 && department == 0)
        {
            sql.append("SELECT DISTINCT d.DossierId FROM Dossier d LEFT JOIN JointAppointments j ON ");
            sql.append("d.DossierId=j.dossierId WHERE d.WorkflowLocations = ?");
            //sql.append(" AND isUserActive(UserID) = true ");
            params.add(DossierLocation.ARCHIVE.mapLocationToWorkflowNodeName());
            paramTypes.add(Types.VARCHAR);
        }
        // School specified with no department
        else if (school != 0 && department == 0)
        {
            sql.append("SELECT DISTINCT d.DossierId FROM Dossier d LEFT JOIN JointAppointments j ON ");
            sql.append(" d.DossierId=j.dossierId WHERE");
            //sql.append(" isUserActive(UserID) = true AND");
            sql.append(" d.WorkflowLocations=? AND");
            sql.append(" (d.PrimarySchoolId=? or j.SchoolId=?) ");
            params.add(DossierLocation.ARCHIVE.mapLocationToWorkflowNodeName());
            params.add(school);
            params.add(school);
            paramTypes.add(Types.VARCHAR);
            paramTypes.add(Types.INTEGER);
            paramTypes.add(Types.INTEGER);
        }
        // Should never have a department id with no school id
        else
        {
            sql.append("SELECT DISTINCT d.DossierId FROM Dossier d LEFT JOIN JointAppointments j ON ");
            sql.append(" d.DossierId=j.dossierId and d.PrimarySchoolID = j.SchoolID and d.PrimaryDepartmentID = j.DepartmentID WHERE");
            //sql.append(" isUserActive(UserID) = true AND");
            sql.append(" d.WorkflowLocations=? AND");
            sql.append(" ((d.PrimarySchoolId=? and d.PrimaryDepartmentId=?) or (j.SchoolId=? and  j.DepartmentId=?))");
            params.add(DossierLocation.ARCHIVE.mapLocationToWorkflowNodeName());
            params.add(school);
            params.add(department);
            params.add(school);
            params.add(department);
            paramTypes.add(Types.VARCHAR);
            paramTypes.add(Types.INTEGER);
            paramTypes.add(Types.INTEGER);
            paramTypes.add(Types.INTEGER);
            paramTypes.add(Types.INTEGER);
        }

        // Convert the params and paramTypes to arrays required for the jdbcTemplate parameters
        final Object [] p = params.toArray();
        final int [] pt = new int[paramTypes.size()];
        for (int i = 0; i < paramTypes.size(); i++)
        {
            pt[i] = paramTypes.get(i);
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("Script:: " + sql.toString());
            logger.debug("Args:: " + params.size());
            int count = 0;
            for (Object obj : params) {
                logger.debug("Args[" + ++count + "] :: " + obj);
            }
        }

        // Do the query
        List<Map<String, Object>> dossierResults = this.getJdbcTemplate().queryForList(sql.toString(), p, pt);
        ArrayList<Long> dossierIdList = new ArrayList<Long>();

        for (Map<String, Object> dossierRecordMap : dossierResults)
        {
            dossierIdList.add(new Long((Integer) dossierRecordMap.get("DossierID")));
        }
        return dossierIdList;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getArchivedDossiersBySchoolAndDepartment(int, int)
     */
    @Override
    public List<Long> getArchivedDossiersBySchoolAndDepartment(final int schoolId, final int departmentId)
    throws SQLException
    {
        return this.getArchivedDossiers(schoolId, departmentId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getCompletedDossiers(java.lang.String)
     */
    @Override
    public List<Long> getCompletedDossiers() throws SQLException
    {
        final String sql = "select DossierID from Dossier where completedDate is not null";
        final List<Map<String, Object>> dossierResults = this.getJdbcTemplate().queryForList(sql);
        final ArrayList<Long> dossierIdList = new ArrayList<Long>();

        for (Map<String, Object> dossierRecordMap : dossierResults)
        {
            dossierIdList.add(new Long((Integer) dossierRecordMap.get("DossierID")));
        }
        return dossierIdList;
    }




    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getDossiersForSnapshots(java.lang.String)
     */
    @Override
    public List<Long> getDossiersForSnapshots() throws SQLException
    {
        final List<Map<String, Object>> dossierResults = this.getJdbcTemplate().queryForList(DOSSIERS_FOR_SNAPSHOTS_SQL);
        final ArrayList<Long> dossierIdList = new ArrayList<Long>();

        for (Map<String, Object> dossierRecordMap : dossierResults)
        {
            dossierIdList.add(new Long((Integer) dossierRecordMap.get("DossierID")));
        }
        return dossierIdList;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#deleteSnapshots(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public void deleteSnapshots(final Dossier dossier, final DossierLocation location) throws WorkflowException, SQLException
    {
        // Get all snapshots for this dossier
        final List <SnapshotDto>dossierSnapshots = this.getAllSnapshots(dossier);
        final List <SnapshotDto>snapshotsToDelete = new ArrayList<SnapshotDto>();

        // if the location is specified, find all dossiers at that location and add to delete list
        if (location != null)
        {
            for (SnapshotDto snapshotDto : dossierSnapshots)
            {
                if (location.mapLocationToWorkflowNodeName().equalsIgnoreCase(snapshotDto.getSnapshotLocation()))
                {
                    snapshotsToDelete.add(snapshotDto);
                }
            }
        }
        // Otherwise delete all
        else
        {
            snapshotsToDelete.addAll(dossierSnapshots);
        }

        // Loop though and delete each snapshot
        for (SnapshotDto snapshotToDelete : snapshotsToDelete)
        {
            this.deleteSnapshot(dossier, snapshotToDelete);
        }
    }




    /**
     * Delete a dossier snapshot record from the the DossierSnapshot table and remove the
     * associated snapshot file from disk.
     * @param snapshotDto
     */
    private void deleteSnapshot(final Dossier dossier, final SnapshotDto snapshotDto)
    {
        final Object[] params = {snapshotDto.getId(), snapshotDto.getDossierId()};
        final int[] paramTypes = {Types.INTEGER, Types.INTEGER};
        int deleteCount = this.getJdbcTemplate().update(DOSSIERSNAPSHOT_DELETE_SQL, params, paramTypes);

        if (deleteCount == 0)
        {
//            String msg = "Unable to delete "+snapshotDto.getSnapshotLocation()+" Snapshot for Dossier" + snapshotDto.getDossierId() + ".";
//            log.log(Level.SEVERE, msg);
            log.error("Unable to delete {} Snapshot for Dossier {}",snapshotDto.getSnapshotLocation(), snapshotDto.getDossierId());
        }
        // Remove the file from disk
        else
        {
            // Snapshots are kept in the dossier directory
            File file =
                new File(dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF), snapshotDto.getSnapshotFileName());
            if (file.exists() && !file.delete())
            {
//                String msg = "Unable to delete snapshot file "+
//                              snapshotDto.getSnapshotFileName()+
//                              " for Dossier" + snapshotDto.getDossierId()+
//                              ", however the DossierSnapshot table record has been deleted.";
//                log.log(Level.SEVERE, msg);
                log.error("Unable to delete snapshot file {} for Dossier {}, however the DossierSnapshot table record has been deleted.",
                          snapshotDto.getSnapshotFileName(), snapshotDto.getDossierId());
            }
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getDossiersBySchoolAndDepartment(int, int)
     */
    @Override
    public List<Long> getDossiersBySchoolAndDepartment(final int schoolId, final int departmentId) throws SQLException
    {
        return this.getDossiersBySchoolDepartmentAndLocation(schoolId, departmentId, null);
    }



    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getDossiersBySchoolDepartmentAndLocation(int, int, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public List<Long> getDossiersBySchoolDepartmentAndLocation(final int schoolId, final int departmentId, final DossierLocation location ) throws SQLException
    {
        final ArrayList<Long> dossierList = new ArrayList<Long>();
        List<Map<String, Object>> dossierResults = null;

        // If no school or department specified, just return all
        if (schoolId == 0 && departmentId == 0)
        {
            String sql = SELECT_BY_LOCATION_BASE;
            if (location != null)
            {
                sql    = SELECT_BY_LOCATION_BASE + " AND WorkflowLocations = '" + location.mapLocationToWorkflowNodeName() + "'";
            }
            dossierResults = this.getJdbcTemplate().queryForList(sql);
        }
        else if (schoolId == 0)
        {
            String sql = SELECT_BY_LOCATION_BASE + " AND (d.PrimaryDepartmentID=? OR j.DepartmentID=?)";
            if (location != null)
            {
                sql    = SELECT_BY_LOCATION_BASE + " AND WorkflowLocations = '" + location.mapLocationToWorkflowNodeName() + "' AND (d.PrimaryDepartmentID=? OR j.DepartmentID=?)";
            }
            Object[] params = { departmentId, departmentId };
            int[] paramTypes = { Types.INTEGER, Types.INTEGER };
            dossierResults = this.getJdbcTemplate().queryForList(sql, params, paramTypes);
        }
        else if (departmentId == 0)
        {
            String sql = SELECT_BY_LOCATION_BASE + " AND (d.PrimarySchoolId=? OR j.SchoolId=?)";
            if (location != null)
            {
                sql    = SELECT_BY_LOCATION_BASE + " AND WorkflowLocations = '" + location.mapLocationToWorkflowNodeName() + "' AND (d.PrimarySchoolId=? OR j.SchoolId=?)";
            }
            Object[] params = { schoolId, schoolId };
            int[] paramTypes = { Types.INTEGER, Types.INTEGER };
            dossierResults = this.getJdbcTemplate().queryForList(sql, params, paramTypes);
        }
        else
        {
            String sql = SELECT_BY_LOCATION_BASE + " AND ((d.PrimarySchoolId=? AND d.PrimaryDepartmentId=?) OR (j.SchoolId=? AND  j.DepartmentId=?))";
            if (location != null)
            {
                sql    = SELECT_BY_LOCATION_BASE + " AND WorkflowLocations = '" + location.mapLocationToWorkflowNodeName() + "' AND ((d.PrimarySchoolId=? AND d.PrimaryDepartmentId=?) OR (j.SchoolId=? AND  j.DepartmentId=?))";
            }
            Object[] params = { schoolId, departmentId, schoolId, departmentId};
            int[] paramTypes = { Types.INTEGER, Types.INTEGER, Types.INTEGER, Types.INTEGER};
            dossierResults = this.getJdbcTemplate().queryForList(sql, params, paramTypes);
        }

        for (Map<String, Object> dossierRecordMap : dossierResults)
        {
            dossierList.add(new Long((Integer) dossierRecordMap.get("DossierID")));
        }
        return dossierList;
    }


    /**
     * Helper method to load dossier base data
     *
     * @param dossierId
     * @return Dossier object
     * @throws WorkflowException
     */
    private Dossier loadDossierBaseData(final long dossierId) throws WorkflowException
    {
        final Object[] params = { dossierId };

        try
        {
            final Dossier dossier = this.getJdbcTemplate().queryForObject(LOAD_BASE_DATA_SQL, params, dossierMapper);

            // Set the primary attribute
            dossier.setAppointmentAttribute(dossier.getPrimarySchoolId(), dossier.getPrimaryDepartmentId(), true);
            // Get the joint appointments and set the attributes
            List<DossierAppointmentDto> jointAppointments = this.getJointAppointments(dossierId);
            for (DossierAppointmentDto jointAppointment : jointAppointments)
            {
                DossierAppointmentAttributeKey appointmentKey =
                    new DossierAppointmentAttributeKey(jointAppointment.getSchoolId(), jointAppointment.getDepartmentId());
                dossier.setAppointmentAttribute(appointmentKey, false);
                dossier.setIsAppointmentComplete(appointmentKey,jointAppointment.isComplete());
            }

            dossier.setBaseDataLoaded(true);
            return dossier;
        }
        catch (SQLException | EmptyResultDataAccessException e)
        {
            String msg = "Dossier " + dossierId + " not found in the Dossier table.";
            log.error(msg);
            throw new WorkflowException(msg);
        }
    }



    /**
     * Helper method to load dossier base data
     *
     * @param packetId
     * @return Dossier object
     * @throws WorkflowException
     */
    private AcademicAction loadDossierAcademicAction(final long academicActionId)
    {
        final Object[] params = { academicActionId };

        return this.getJdbcTemplate().queryForObject(LOAD_ACADEMIC_ACTION_SQL, params, academicActionMapper);
    }


    /**
     * Creates and maps 'AcademicAction' table records to {@link AcademicAction} objects.
     */
    private RowMapper<AcademicAction> academicActionMapper = new RowMapper<AcademicAction>()
    {
        @Override
        public AcademicAction mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new AcademicAction(userService.getPersonByMivId(rs.getInt("UserID")),
                                      DossierActionType.valueOf(rs.getInt("ActionTypeID")),
                                      DossierDelegationAuthority.valueOf(rs.getInt("DelegationAuthorityID")),
                                      rs.getDate("EffectiveDate"),
                                      rs.getDate("RetroactiveDate"),
                                      rs.getDate("EndDate"),
                                      rs.getInt("AccelerationYears"));
        }
    };


    /**
     * Helper method to update a dossier row in the Dossier table
     *
     * @param dossier
     * @throws WorkflowException
     * @throws SQLException
     */
    private void updateDossierData(final Dossier dossier)
    {
        // Get the most current workflow location
        final String [] nodes = workflowInfo.getCurrentNodeNames(dossier);
        StringBuilder nodeStr = new StringBuilder();
        for (String node : nodes) { nodeStr.append(node).append(" "); }
        final String workflowLocations = nodeStr.toString().trim();
        final String previousWorkflowLocations = dossier.getPreviousLocations();

        final String workflowStatus = workflowInfo.getDocumentStatus(dossier);
        final java.sql.Timestamp timeStamp = new java.sql.Timestamp(System.currentTimeMillis());
        final int updateUserId = dossier.getAction().getCandidate().getUserId();
        final long dossierId = dossier.getDossierId();


        // If base data is not loaded, only update the workflow location
        if (!dossier.isBaseDataLoaded())
        {
            this.getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(UPDATE_DOSSIER_LOCATION_SQL);
                    ps.setString(1, workflowLocations);
                    ps.setString(2, previousWorkflowLocations);
                    ps.setString(3, workflowStatus);
                    ps.setTimestamp(4, timeStamp);
                    ps.setInt(5, updateUserId);
                    ps.setLong(6, dossierId);
                    return ps;
                }
            });
        }
        // Base data is loaded, update all changeable data
        else
        {
            final int primarySchoolId = dossier.getPrimarySchoolId();
            final int primaryDeptId = dossier.getPrimaryDepartmentId();
            final Timestamp submittedDate = new Timestamp(dossier.getSubmittedDate().getTime());
            final Timestamp lastRoutedDate = dossier.getLastRoutedDate() != null ? new Timestamp(dossier.getLastRoutedDate().getTime()) : null;
            final Timestamp archivedDate = dossier.getArchivedDate() != null ? new Timestamp(dossier.getArchivedDate().getTime()) : null;
            final Timestamp completedDate = dossier.getCompletedDate() != null ? new Timestamp(dossier.getCompletedDate().getTime()) : null;
            final boolean recommendedActionFormPresent = dossier.isRecommendedActionFormPresent();
            final boolean academicActionFormPresent = dossier.isAcademicActionFormPresent();
            final String reviewType = dossier.getReviewType() == null ? null : dossier.getReviewType().toString();

            this.getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(UPDATE_DOSSIER_SQL);
                    ps.setInt(1, primarySchoolId);
                    ps.setInt(2, primaryDeptId);
                    ps.setTimestamp(3, submittedDate);
                    ps.setTimestamp(4, lastRoutedDate);
                    ps.setTimestamp(5, archivedDate);
                    ps.setTimestamp(6, completedDate);
                    ps.setString(7, reviewType);
                    ps.setBoolean(8, recommendedActionFormPresent);
                    ps.setBoolean(9, academicActionFormPresent);
                    ps.setString(10, workflowLocations);
                    ps.setString(11, previousWorkflowLocations);
                    ps.setString(12, workflowStatus);
                    ps.setTimestamp(13, timeStamp);
                    ps.setInt(14, updateUserId);
                    ps.setLong(15, dossierId);
                    return ps;
                }
            });

            updateAcademicAction(dossier);
        }
    }


    /**
     * Helper method to update a academic action row in the AcademicAction table
     *
     * @param dossier
     * @throws WorkflowException
     * @throws SQLException
     */
    private void updateAcademicAction(final Dossier dossier)
    {
        getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                AcademicAction action = dossier.getAction();
                PreparedStatement ps = connection.prepareStatement(UPDATE_ACADEMIC_ACTION_SQL);
                ps.setInt(1, action.getActionType().getActionTypeId());
                ps.setInt(2, action.getDelegationAuthority().getDelegationAuthorityId());
                ps.setDate(3, new java.sql.Date(action.getEffectiveDate().getTime()));
                ps.setDate(4, action.getRetroactiveDate() != null ? new java.sql.Date(action.getRetroactiveDate().getTime()) : null);
                ps.setDate(5, action.getEndDate() != null ? new java.sql.Date(action.getEndDate().getTime()) : null);
                ps.setInt(6, action.getAccelerationYears());
                ps.setInt(7, action.getCandidate().getUserId());
                ps.setInt(8, dossier.getAcademicActionID());
                return ps;
            }
        });
    }



    /**
     * Helper method to insert a new dossier row into the Dossier table
     *
     * @param dossier
     * @throws WorkflowException
     * @throws SQLException
     */
    private void insertDossierData(final Dossier dossier) throws WorkflowException
    {
        final int id = 0;
        final long dossierId = dossier.getDossierId();
        final int userId = dossier.getAction().getCandidate().getUserId();
        final int primarySchoolId = dossier.getPrimarySchoolId();
        final int primaryDeptId = dossier.getPrimaryDepartmentId();
        final Timestamp submittedDate = new Timestamp(dossier.getSubmittedDate().getTime());
        final Timestamp lastRoutedDate = new Timestamp(dossier.getLastRoutedDate().getTime());
        final AcademicAction action = dossier.getAction();
        final String reviewType = dossier.getReviewType() == null ? null : dossier.getReviewType().toString();
        final boolean recommendedActionFormPresent = dossier.isRecommendedActionFormPresent();
        final boolean academicActionFormPresent = dossier.isAcademicActionFormPresent();
        final Timestamp timeStamp = new Timestamp(System.currentTimeMillis());

        // Get the most current workflow location and status
        final String [] nodes = workflowInfo.getCurrentNodeNames(dossier);
        final StringBuilder nodeStr = new StringBuilder();
        for (String node : nodes) { nodeStr.append(node).append(" "); }
        final String workflowLocations = nodeStr.toString().trim();
        final String previousWorkflowLocations = dossier.getPreviousLocations();
        final String workflowStatus = workflowInfo.getDocumentStatus(dossier);

        int updateCount = 0;
        String msg = null;

        final int academicActionID = insertAcademicActionData(action);

        // updates the Packets record in the database corresponding to this
        // dossier in the workflow system
        updateCount = this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement(INSERT_DOSSIER_SQL);
                ps.setLong(1, id);
                ps.setInt(2, academicActionID);
                ps.setLong(3, dossierId);
                ps.setInt(4, primarySchoolId);
                ps.setInt(5, primaryDeptId);
                ps.setTimestamp(6, submittedDate);
                ps.setTimestamp(7, lastRoutedDate);
                ps.setString(8, reviewType);
                ps.setBoolean(9, recommendedActionFormPresent);
                ps.setBoolean(10, academicActionFormPresent);
                ps.setString(11, workflowLocations);
                ps.setString(12, previousWorkflowLocations);
                ps.setString(13, workflowStatus);
                ps.setTimestamp(14, timeStamp);
                ps.setInt(15, userId);
                return ps;
            }
        });

        if (updateCount == 0)
        {
            msg = "Unable to insert Dossier " + dossierId + " into the Dossier table.";
            log.error(msg);
            throw new WorkflowException(msg);
        }
    }


    /**
     * Helper method to insert a new dossier row into the Dossier table
     *
     * @param dossier
     * @return AcademicActionId
     * @throws WorkflowException
     * @throws SQLException
     */
    private int insertAcademicActionData(final AcademicAction action) throws WorkflowException
    {
        final KeyHolder keyHolder = new GeneratedKeyHolder();

        // updates the Packets record in the database corresponding to this
        // action in the workflow system
        int updateCount = this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement(INSERT_ACADEMIC_ACTION_SQL, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, action.getCandidate().getUserId());
                ps.setInt(2, action.getActionType().getActionTypeId());
                ps.setInt(3, action.getDelegationAuthority().getDelegationAuthorityId());
                ps.setDate(4, new java.sql.Date(action.getEffectiveDate().getTime()));
                ps.setDate(5, action.getRetroactiveDate() != null ? new java.sql.Date(action.getRetroactiveDate().getTime()) : null);
                ps.setDate(6, action.getEndDate() != null ? new java.sql.Date(action.getEndDate().getTime()) : null);
                ps.setInt(7, action.getCandidate().getUserId());
                return ps;
            }
        }, keyHolder);

        if (updateCount == 0)
        {
            String msg = "Unable to persist academic action for User " + action.getCandidate();
            log.error(msg);
            throw new WorkflowException(msg);
        }

        return keyHolder.getKey().intValue();
    }




    /**
     * loadDossierAttributes - This method loads dossier attributes for a
     * specified dossier location or the current dossier location if not
     * specified.
     *
     * @param dossier
     * @param inputLocation
     *            - defaults to the current dossier location if not provided
     * @return Dossier
     * @throws WorkflowException
     * @throws SQLException
     */
    private Dossier loadDossierAttributes(final Dossier dossier, final DossierLocation inputLocation) throws WorkflowException
    {

        // Default location is the current dossier location
        // If the inputLocation is provided, use it
        final DossierLocation dossierLocation = inputLocation != null ? inputLocation : dossier.getDossierLocation();

        // Get the existing DossierAttributes and Joint Appointments for this dossier, if any.
        List<Map<String, Object>> dossierAttributes = new ArrayList<>();

        // Don't bother with query if record id is not greater than zero (new
        // dossier record)
        if (dossier.getId() > 0)
        {
            // Existing attribute data query
            Object[] params = { dossier.getDossierId() };
            int[] paramTypes = { Types.INTEGER };
            dossierAttributes = this.getJdbcTemplate().queryForList(DOSSIERATTRIBUTES_QUERY_FOR_LIST, params, paramTypes);
        }

        // Get a map of the available attribute types by name
        Map<String, Map<String, String>> attributeTypesByName = MIVConfig.getConfig().getMap("attributetypesbyname");
        if (attributeTypesByName != null && attributeTypesByName.isEmpty())
        {
            String msg = "No attribute types defined! Unable to load attributes for dossier " + dossier.getDossierId() + ".";
            log.error(msg);
            throw new WorkflowException(msg);
        }

        // Build a DossierAttibute object for each of the appointment attribute for this dossier
        Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> dossierAttributeMap = dossier.getAppointmentAttributes();
        for (DossierAppointmentAttributeKey schoolDeptKey : dossierAttributeMap.keySet())
        {
            // Get the flag indicating if this attribute set is for the primary appointment
            boolean isPrimaryAppointment = dossierAttributeMap.get(schoolDeptKey).isPrimary();

//            List<String> attributes = null;
//            if (isPrimaryAppointment)
//            {
//                // Get attributes applicable to a primary appointment at this location
//                attributes = getAttributeTypesByDelegationAuthorityLocationAndAppointment(dossier, dossierLocation,
//                        DossierAppointmentType.PRIMARY);
//            }
//            else
//            {
//                // Get attributes applicable to a joint appointment at this location
//                attributes = getAttributeTypesByDelegationAuthorityLocationAndAppointment(dossier, dossierLocation,
//                        DossierAppointmentType.JOINT);
//            }
            List<String> attributes = getAttributeNames(dossierLocation,
                                                        isPrimaryAppointment ? DossierAppointmentType.PRIMARY : DossierAppointmentType.JOINT,
                                                        dossier.getAction().getDelegationAuthority(),
                                                        dossier.getAction().getActionType());

            for (String attributeName : attributes)
            {
                // Create a new dossier attribute object filling in data
                // applicable to this attribute from the the system
                // attributeTypesByName map
                final DossierAttribute dossierAttribute = new DossierAttribute(schoolDeptKey,
                                                                         attributeTypesByName.get(attributeName),
                                                                         dossierLocation,
                                                                         dossier.getAction().getActionType());

                // Store the attribute in the dossier attribute map for this
                // school and department
                dossierAttributeMap.get(schoolDeptKey).getAttributes().put(attributeName, dossierAttribute);

                // If this is a DOCUMENT or DOCUMENTUPLOAD or DOCUMENTUPLOADSIGNATURE DossierAttributeType,
                // load the document(s), if any into the file list for the attribute.
                final DossierAttributeType attributeType = dossierAttributeMap.get(schoolDeptKey).getAttributes().get(attributeName)
                        .getAttributeType();

                // Load the individual document attribute based on the attribute type
                // If this is a new record, there will be no attribute to load
                if (dossier.getId() > 0)
                {
                    switch (attributeType)
                    {
                        case DOCUMENT:
                            loadDocumentAttribute(dossier, dossierAttribute, isPrimaryAppointment);

                            /*
                             * Provost tenure decision is displayed only if the tenure recommendation is not.
                             */
                            if ("provosts_tenure_decision".equalsIgnoreCase(dossierAttribute.getAttributeName()))
                            {
                                dossierAttribute.setDisplay(
                                    !dossierAttributeMap.get(schoolDeptKey)
                                                        .getAttribute("provosts_tenure_recommendation")
                                                        .isDisplay());
                            }
                            else if ("provosts_tenure_appeal_decision".equalsIgnoreCase(dossierAttribute.getAttributeName()))
                            {
                                dossierAttribute.setDisplay(
                                    !dossierAttributeMap.get(schoolDeptKey)
                                                        .getAttribute("provosts_tenure_appeal_recommendation")
                                                        .isDisplay());
                            }

                            break;
                        case DOCUMENTUPLOAD:
                            loadUploadDocumentAttribute(dossier, dossierAttribute, attributeType);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        // Get the attributetypebyid map
        Map<String, Map<String, String>> attributeTypeByIdMap = MIVConfig.getConfig().getMap("attributetypesbyid");

        // Populate the data stored for each attribute, if any, from the
        // DossierAttributes table
        for (Map<String, Object> attributeRecordMap : dossierAttributes)
        {
            final int recordId = (Integer) attributeRecordMap.get("ID");
            final int deptId = (Integer) attributeRecordMap.get("DepartmentID");
            final int schoolId = (Integer) attributeRecordMap.get("SchoolID");
            final int dossierId = (Integer) attributeRecordMap.get("DossierID");
            final String value = (String) attributeRecordMap.get("Value");

            // Get the name associated with this attribute id from the attributeTypeMap
            final int attributeTypeId = (Integer) attributeRecordMap.get("AttributeTypeID");
            final String attributeName = attributeTypeByIdMap.get(attributeTypeId + "").get("name");

            // Store the record id of this attribute and its value in the
            // attribute map for this school:dept and attribute name
            final DossierAppointmentAttributeKey schoolDeptKey = new DossierAppointmentAttributeKey(schoolId, deptId);
            final DossierAppointmentAttributes attributes = dossierAttributeMap.get(schoolDeptKey);
            if (attributes == null) {
                // bad school:dept combination
                String msg = "School/Department combination " + schoolDeptKey + " is not valid! Unable to load attributes for dossier " + dossier.getDossierId() + ".";
                log.error(msg);
                throw new WorkflowException(msg);
            }

            Map<String, DossierAttribute> attMap = attributes.getAttributes();
            DossierAttribute dossierAttribute = attMap.get(attributeName);
//            DossierAttribute dossierAttribute = dossierAttributeMap.get(schoolDeptKey).getAttributes().get(attributeName);

            // Attribute won't be present if it's not valid for the current location
            // Don't update the value for documents or document uploads since they have been checked above
            if (dossierAttribute != null)
            {
                // Set the values from the database
                dossierAttribute.setRecordId(recordId);
                dossierAttribute.setDossierId(dossierId);
                dossierAttribute.setAppointmentKey(schoolDeptKey);

                // Don't update the attribute value for documents or document uploads since they have been set above
                if (dossierAttribute.getAttributeType() == DossierAttributeType.ACTION && value != null)
                {
                    try
                    {
                        dossierAttribute.setAttributeValue(DossierAttributeStatus.valueOf(value));
                    }
                    catch (IllegalArgumentException e)
                    {
                        throw new MivSevereApplicationError("errors.dossier.attribute.value", e, value);
                    }
                }
            }
        }

        dossier.setAttributedataLoaded(true);
        if (uploadDocumentMap != null)
        {
            this.uploadDocumentMap.clear();
        }
        return dossier;
    }


    /*
     * (non-Javadoc)
     *
     * @seeedu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#
     * loadDossierAttributesForLocation
     * (edu.ucdavis.mw.myinfovault.domain.dossier.Dossier,
     * edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public Dossier loadDossierAttributesForLocation(final Dossier dossier, final DossierLocation location) throws WorkflowException, SQLException
    {
        return this.loadDossierAttributes(dossier, location);
    }


    /*
     * (non-Javadoc)
     *
     * @see
     * edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#loadDossierAttributes
     * (edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public Dossier loadDossierAttributes(final Dossier dossier) throws WorkflowException, SQLException
    {
        return this.loadDossierAttributes(dossier, null);
    }



    /*
     * (non-Javadoc)
     *
     * @see
     * edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#deleteDossierAttributes
     * (edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public Dossier deleteDossierAttributes(final Dossier dossier) throws WorkflowException, SQLException
    {
        final Object[] params = { dossier.getDossierId() };
        final int[] paramTypes = { Types.INTEGER };

        // Delete the existing DossierAttributes for this dossier, if any
        final int deleteCount = this.getJdbcTemplate().update(DOSSIERATTRIBUTES_DELETE_SQL, params, paramTypes);

        if (deleteCount == 0)
        {
            log.warn("Unable to delete DossierAttributes for Dossier {}. There were no attributes deleted.", dossier.getDossierId());
        }

        String JOINTAPPOINTMENTS_DELETE_QUERY = "delete from JointAppointments where DossierID = ?";
        final Object[] ja_params = { dossier.getDossierId() };
        final int[] ja_paramTypes = { Types.INTEGER };

        // Delete the existing JointAppointments for this dossier, if any
        int ja_deleteCount = this.getJdbcTemplate().update(JOINTAPPOINTMENTS_DELETE_QUERY, ja_params, ja_paramTypes);

        if (ja_deleteCount == 0)
        {
            log.warn("Unable to delete JointAppointments for Dossier {}. There were no joint appointments deleted.", dossier.getDossierId());
        }

        dossier.getAppointmentAttributes().clear();
        return dossier;
    }


    /**
     * Helper method to update dossierAttributes for the input List of dossier attributes.
     *
     * @param updateList
     * @param userId
     * @throws SQLException
     */
    private void updateDossierAttributes(final List<DossierAttribute> updateList, final int userId)
    {
        @SuppressWarnings("unused")
        int[] updateCounts = this.getJdbcTemplate().batchUpdate(UPDATE_ATTRIBUTES_LIST_SQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException
            {
                DossierAttribute attribute = updateList.get(i);

                ps.setString(1, attribute.getAttributeValue() != null ? attribute.getAttributeValue().name() : null);
                ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                ps.setInt(3, userId);
                ps.setInt(4, attribute.getRecordId());
            }

            @Override
            public int getBatchSize()
            {
                return updateList.size();
            }
        });
    }


    /*
     * (non-Javadoc)
     *
     * @see
     * edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#updateDossier(edu.ucdavis
     * .mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public void updateDossier(final Dossier dossier) throws WorkflowException, SQLException
    {
        int retries = 10;
        boolean notified = false;
        RuntimeException lastError = null;

        while (retries-- > 0)
        {
            try {
                // Id == 0 is a new record
                if (dossier.getId() <= 0)
                {
                    this.insertDossierData(dossier);
                    // See if there are any joint appointments for this dossier. Any more than one appointment indicates
                    // joint appointments are present
                    if (dossier.getAppointmentAttributes().size() > 1)
                    {
                        Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> dossierAttributes = dossier.getAppointmentAttributes();
                        HashMap<DossierAppointmentAttributeKey, DossierAppointmentAttributeKey> insertJointAppointmentRecordMap = new LinkedHashMap<DossierAppointmentAttributeKey, DossierAppointmentAttributeKey>();
                        for (DossierAppointmentAttributeKey schoolDeptKey : dossierAttributes.keySet())
                        {
                            DossierAppointmentAttributes schoolDepartmentAttributes = dossierAttributes.get(schoolDeptKey);
                            if (!schoolDepartmentAttributes.isPrimary())
                            {
                                insertJointAppointmentRecordMap.put(schoolDeptKey, schoolDeptKey);
                            }
                        }
                        this.insertJointAppointments(insertJointAppointmentRecordMap, dossier.getAction().getCandidate().getUserId(), dossier.getDossierId());
                    }
                }
                else
                {
                    this.updateDossierData(dossier);
                    // See if there are any joint appointments for this dossier. Any more than one appointment indicates
                    // joint appointments are present
                    if (dossier.getAppointmentAttributes().size() > 1)
                    {
                        this.updateJointAppointments(dossier);
                    }
                }

                // If we get here we were successful. Reset the error so we can continue and break out of the retry loop.
                lastError = null;
                break;
            }
            catch (DataAccessResourceFailureException e) {
                // Do nothing, allow another retry loop
                if (!notified) {
                    log.warn("|| --- Data Access Error while trying to update Dossier #" + dossier.getDossierId() + " < < < < < < < < <", e);
                    notified = true;
                }
                lastError = e;
            }
        }
        if (lastError != null) throw(lastError);

        // Save attribute data only if loaded
        if (dossier.isAttributedataLoaded())
        {
            //@/SuppressWarnings("unused")
            //int[] updateCounts = null;
            LinkedHashMap<DossierAppointmentAttributeKey, DossierAppointmentAttributeKey> insertJointAppointmentRecordMap = new LinkedHashMap<DossierAppointmentAttributeKey, DossierAppointmentAttributeKey>();
            List<DossierAttribute> insertAttributeRecordList = new ArrayList<DossierAttribute>();
            // Records to be updated
            List<DossierAttribute> updateRecordsList = new ArrayList<DossierAttribute>();

            // Get any existing joint appointments
            List <DossierAppointmentDto> jointAppointments = this.getJointAppointments(dossier);

            // Iterate through all attributes for each of the dossier schools
            // and departments
            Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> dossierAttributes = dossier.getAppointmentAttributes();
            for (DossierAppointmentAttributeKey schoolDeptKey : dossierAttributes.keySet())
            {
                DossierAppointmentAttributes schoolDepartmentAttributes = dossierAttributes.get(schoolDeptKey);
                Map<String, DossierAttribute> attributes = schoolDepartmentAttributes.getAttributes();

                for (String attributeName : attributes.keySet())
                {
                    attributes.get(attributeName).setDossierId(dossier.getDossierId());
                    // If the current attributeId is <= 0, this is a new record
                    // to be inserted
                    if (attributes.get(attributeName).getRecordId() <= 0)
                    {
                        insertAttributeRecordList.add(attributes.get(attributeName));
                        // If this a joint appointment attribute (not a primary), add to the list of joint appointment records to insert
                        // for this dossier if there are no existing
                        if (!schoolDepartmentAttributes.isPrimary() && jointAppointments.isEmpty())
                        {
                            insertJointAppointmentRecordMap.put(schoolDeptKey, schoolDeptKey);
                        }
                    }
                    else
                    {
                        updateRecordsList.add(attributes.get(attributeName));
                    }
                }
            }

            // Insert any new records and reload
            final int userId = dossier.getAction().getCandidate().getUserId();
            if (!insertAttributeRecordList.isEmpty())
            {
                this.insertJointAppointments(insertJointAppointmentRecordMap, userId, dossier.getDossierId());
                this.insertDossierAttributes(insertAttributeRecordList, userId);
                this.loadDossierAttributes(dossier);
            }
            // Update existing records
            if (!updateRecordsList.isEmpty())
            {
                this.updateDossierAttributes(updateRecordsList, userId);
            }
        }
    }




    /**
     * Delete a dossier record from the the Dossier and DossierAttributes table
     * @param dossier
     * @throws SQLException
     * @throws WorkflowException
     */
    @Override
    public void deleteDossier(final Dossier dossier) throws WorkflowException, SQLException
    {
        // Delete the dossier attributes
        this.deleteDossierAttributes(dossier);

        // Delete the dossier
        final Object[] params = {dossier.getDossierId()};
        final int[] paramTypes = {Types.INTEGER};
        final int deleteCount = this.getJdbcTemplate().update(DOSSIER_DELETE_SQL, params, paramTypes);

        // Delete the dossier directory from disk
        if (deleteCount > 0)
        {
            // Files are kept in the dossier directory
            deleteDirectory(dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF));
            File file = new File(dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF).getAbsolutePath());
            if (file.exists() && !file.delete())
            {
                log.error("Unable to delete dossier directory for Dossier {}", dossier.getDossierId());
            }
        }
        else
        {
            log.error("Unable to delete dossier {}", dossier.getDossierId());
        }
    }




    /**
     * Helper method to update dossierAttributes for the input List of dossier attributes.
     *
     * @param insertRecords
     * @param userId
     * @throws SQLException
     */
    private void insertDossierAttributes(final List<DossierAttribute> insertRecords, final int userId)
    {
        @SuppressWarnings("unused")
        int[] updateCounts = this.getJdbcTemplate().batchUpdate(INSERT_ATTRIBUTES_SQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException
            {
                DossierAttribute record = insertRecords.get(i);

                ps.setInt(1, 0);
                ps.setInt(2, record.getAttributeId());
                ps.setLong(3, record.getDossierId());
                ps.setInt(4, record.getDeptId());
                ps.setInt(5, record.getSchoolId());
                ps.setString(6, record.getAttributeValue() != null ? record.getAttributeValue().name() : null);
                ps.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
                ps.setInt(8, userId);
            }

            @Override
            public int getBatchSize()
            {
                return insertRecords.size();
            }
        });
    }




    /**
     * Helper method to insert joint appointment records
     *
     * @param insertRecords contains department and school id's of the appointment
     * @param userId
     * @param dossierId
     * @throws WorkflowException
     * @throws SQLException
     */
    private void insertJointAppointments(final Map<DossierAppointmentAttributeKey, DossierAppointmentAttributeKey> insertRecords, final int userId, final long dossierId)
    {
        @SuppressWarnings("unused")
        int[] updateCounts = null;
        final int batchSize = insertRecords.size();
        final List<Long> dossierIds = new ArrayList<Long>(batchSize);
        final List<Integer> departmentIds = new ArrayList<Integer>(batchSize);
        final List<Integer> schoolIds = new ArrayList<Integer>(batchSize);

        for (DossierAppointmentAttributeKey dossierAttributeKey : insertRecords.keySet())
        {
            dossierIds.add(dossierId);
            departmentIds.add(dossierAttributeKey.getDepartmentId());
            schoolIds.add(dossierAttributeKey.getSchoolId());
        }
        updateCounts = this.getJdbcTemplate().batchUpdate(INSERT_JOINTAPPOINTMENTS_SQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException
            {
                ps.setInt(1, 0);
                ps.setLong(2, dossierIds.get(i));
                ps.setInt(3, departmentIds.get(i));
                ps.setInt(4, schoolIds.get(i));
                ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
                ps.setInt(6, userId);
            }

            @Override
            public int getBatchSize()
            {
                return batchSize;
            }
        });
    }




    /**
     * Helper method to update joint appointment records.
     *
     * @param dossier
     * @throws SQLException
     */
    private void updateJointAppointments(final Dossier dossier) throws SQLException
    {
        @SuppressWarnings("unused")
        int[] updateCounts = null;

        final List <DossierAppointmentDto> jointAppointments = this.getJointAppointments(dossier);
        final int batchSize = jointAppointments.size();
        final List<Long> dossierIds = new ArrayList<Long>(batchSize);
        final List<Integer> departmentIds = new ArrayList<Integer>(batchSize);
        final List<Integer> schoolIds = new ArrayList<Integer>(batchSize);
        final List<Boolean> completes= new ArrayList<Boolean>(batchSize);
        final int userId = dossier.getAction().getCandidate().getUserId();

        for (DossierAppointmentDto jointAppointment : jointAppointments)
        {
            dossierIds.add(jointAppointment.getDossierId());
            departmentIds.add(jointAppointment.getDepartmentId());
            schoolIds.add(jointAppointment.getSchoolId());
            // Get the current state setting of the appointment
            DossierAppointmentAttributeKey appointmentKey = Dossier.getAppointmentKey(jointAppointment.getSchoolId(), jointAppointment.getDepartmentId());
            completes.add(dossier.getAttributesByAppointment(appointmentKey).isComplete());
        }
        updateCounts = this.getJdbcTemplate().batchUpdate(UPDATE_JOINTAPPOINTMENTS_SQL, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException
            {
                ps.setBoolean(1, completes.get(i));
                ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                ps.setInt(3, userId);
                ps.setLong(4, dossierIds.get(i));
                ps.setInt(5, schoolIds.get(i));
                ps.setInt(6, departmentIds.get(i));
            }

            @Override
            public int getBatchSize()
            {
                return batchSize;
            }
        });
    }


    /**
     * Helper method to load all base and attribute data for the input Dossier object
     *
     * @param dossierId
     * @return Dossier object
     * @throws WorkflowException
     * @throws SQLException
     */
    private Dossier loadAllDossierData(final long dossierId) throws WorkflowException, SQLException
    {
        final Dossier dossier = this.loadDossierBaseData(dossierId);
        return this.loadDossierAttributes(dossier);
    }


    /*
     * (non-Javadoc)
     *
     * @seeedu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#
     * getActiveDossiers(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<Dossier> getActiveDossiers(final MivPerson person) throws WorkflowException, SQLException
    {

        final List<Dossier> dossiers = this.getAllDossiers(person);

        final ArrayList<Dossier> dossierList = new ArrayList<Dossier>();
        if (!dossiers.isEmpty())
        {
            // Iterate through the list only adding active dossiers
            for (Dossier dossier : dossiers)
            {
                // If the dossier has an archived date, it is not active
                if (dossier.getArchivedDate() != null)
                {
                    continue;
                }
//                RouteHeaderDTO routeHeader = workflowInfo.getRouteHeader(dossier.getDossierId());

//                // MIV-3546
//                // Check the document status. If PROCESSED, FINAL, or CANCEL the document is not active...skip.
//                if (routeHeader != null &&
//                    (routeHeader.getDocRouteStatus().equals(KEWConstants.ROUTE_HEADER_PROCESSED_CD) ||
//                     routeHeader.getDocRouteStatus().equals(KEWConstants.ROUTE_HEADER_FINAL_CD) ||
//                     routeHeader.getDocRouteStatus().equals(KEWConstants.ROUTE_HEADER_CANCEL_CD)))
//                {
//                    continue;
//                }
//                else if (routeHeader == null)
//                {
//                    String msg = "Unable locate Dossier" + dossier.getDossierId() + " in workflow to validate the document status.";
//                    log.log(Level.WARNING, msg);
//                    continue;
//                }
//                if (routeHeader == null)
//                {
//                    log.warn("Unable locate Dossier {} in workflow to validate the document status.", dossier.getDossierId());
//                    continue;
//                }
//                else // routeHeader is not null, but is it in a state where we should skip it?
//                {
                    // MIV-3546  Check the document status. If PROCESSED, FINAL, or CANCEL the document is not active...skip.
//                    String routeStatus = routeHeader.getDocRouteStatus();
                    String routeStatus = dossier.getWorkflowStatus();
                    if (routeStatus.equals(MivWorkflowConstants.ROUTE_HEADER_PROCESSED_CD) ||
                        routeStatus.equals(MivWorkflowConstants.ROUTE_HEADER_FINAL_CD) ||
                        routeStatus.equals(MivWorkflowConstants.ROUTE_HEADER_CANCEL_CD))
                    {
                        continue;
                    }
//                }

                // Dossier is in process, include in the list
                dossierList.add(dossier);
            }
        }
        return dossierList;
    }


    /*
     * (non-Javadoc)
     *
     * @seeedu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#
     * getAllDossiers(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<Dossier> getAllDossiers(final MivPerson person) throws DataAccessException
    {
        final String sql = "Select d.DossierId From Dossier d, AcademicAction aa Where d.AcademicActionID=aa.ID AND aa.UserId = ?";
        final Object[] params = { person.getUserId() };
        final int[] paramTypes = { Types.INTEGER };

        ArrayList<Dossier> dossierList = new ArrayList<Dossier>();

        List<Map<String, Object>> dossiers = this.getJdbcTemplate().queryForList(sql, params, paramTypes);
        if (!dossiers.isEmpty())
        {
            // Get the workflow data for this dossier
            for (Map<String, Object> dossierRecordMap : dossiers)
            {
                Integer dossierId = (Integer)dossierRecordMap.get("DossierID");
                try
                {
                    dossierList.add(this.getDossier(dossierId));
                }
                catch (WorkflowException wfe)
                {
                    log.error("Could not retrieve dossier {} for userId {} : {}",
                              new Object[] { dossierId, person.getUserId(), wfe.getMessage() });
                    continue;
                }
            }
        }
        return dossierList;
    }


    /*
     * (non-Javadoc)
     *
     * @seeedu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#
     * getFinalizedDossiers(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<Dossier> getFinalizedDossiers(final MivPerson person) throws WorkflowException, SQLException
    {

        final List<Dossier> dossiers = this.getAllDossiers(person);

        final ArrayList<Dossier> dossierList = new ArrayList<Dossier>();
        if (!dossiers.isEmpty())
        {
            // Iterate through the list only adding finalized dossiers
            for (Dossier dossier : dossiers)
            {
//                RouteHeaderDTO routeHeader = workflowInfo.getRouteHeader(dossier.getDossierId());
//                if (routeHeader == null)
//                {
//                    log.warn("Unable locate Dossier {} in workflow to validate finalized date.", dossier.getDossierId());
//                }
//                if (routeHeader.getDateFinalized() != null)
                if (dossier.getWorkflowStatus().equals(MivWorkflowConstants.ROUTE_HEADER_FINAL_CD))
                {
                    dossierList.add(dossier);
                }
            }
        }
        return dossierList;
    }




    /**
     * Get a list of documents this role is allowed to view.
     *
     * @param mivRole
     * @return List - list of DossierDocumentDto objects
     */
    @Override
    public List<DossierDocumentDto> getDocumentListByRole(final MivRole mivRole)
    {
        return get(DOCUMENTQUERY, new DossierDocumentRowMapper(), mivRole.name());
    }




    /**
     * Get a list of document upload files for this dossier.
     *
     * @param dossier
     * @return list of UploadDocumentDto objects
     */
    @Override
    public Map<String, List<UploadDocumentDto>> getDocumentUploadFiles(final Dossier dossier)
    {
        return getDocumentUploadFiles(dossier, UPLOAD_DOCUMENT_QUERY, dossier.getDossierId());
    }




    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getDocumentUploadFilesBySchoolAndDepartment(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public Map<String, List<UploadDocumentDto>> getDocumentUploadFilesBySchoolAndDepartment(final Dossier dossier, final int schoolId, final int departmentId)
    {
        return getDocumentUploadFiles(dossier, UPLOAD_DOCUMENT_SCH_DEPT_QUERY, schoolId, departmentId, dossier.getDossierId());
    }

    /**
     * Get a map of upload documents keyed their respective document IDs.
     *
     * @param dossier dossier to which the uploads belong
     * @param sql statement to retrieve the uploads
     * @param params parameters corresponding to the SQL statement
     * @return upload documents
     */
    private Map<String, List<UploadDocumentDto>> getDocumentUploadFiles(final Dossier dossier, final String sql, final Object...params)
    {
        Map<String, List<UploadDocumentDto>> uploadDocumentMap = new LinkedHashMap<String, List<UploadDocumentDto>>();

        RowMapper<UploadDocumentDto> uploadDocumentRowMapper = new RowMapper<UploadDocumentDto>()
        {
            @Override
            public UploadDocumentDto mapRow(ResultSet rs, int rowNum) throws SQLException
            {
                final File upload = new File(dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF), rs.getString("UploadFile"));

                UploadDocumentDto uploadDocumentDto = new UploadDocumentDto(upload);
                uploadDocumentDto.setId(rs.getInt("ID"));
                uploadDocumentDto.setUserId(rs.getInt("UserID"));
                uploadDocumentDto.setDossierId(rs.getInt("PacketID"));
                uploadDocumentDto.setDocumentId(rs.getInt("DocumentId"));
                uploadDocumentDto.setSequence(((Integer) rs.getObject("Sequence")) != null ? (Integer) rs.getObject("Sequence") : 0);
                uploadDocumentDto.setUploadName(rs.getString("UploadName"));
                uploadDocumentDto.setYear(rs.getString("Year"));
                uploadDocumentDto.setSchoolId(rs.getInt("SchoolID"));
                uploadDocumentDto.setDepartmentId(rs.getInt("DepartmentID"));
                uploadDocumentDto.setUploadLocation(DossierLocation.mapWorkflowNodeNameToLocation(rs.getString("UploadLocation")));
                uploadDocumentDto.setRedacted(rs.getBoolean("Redacted"));
                uploadDocumentDto.setConfidential(rs.getBoolean("Confidential"));
                uploadDocumentDto.setInsertUserId(rs.getInt("InsertUserID"));
                return uploadDocumentDto;
            }
        };

        final List<UploadDocumentDto> uploadDocumentDtoList = get(sql, uploadDocumentRowMapper, params);

        for (UploadDocumentDto uploadDocument : uploadDocumentDtoList)
        {
            // Key for the map
            String documentId = uploadDocument.getDocumentId().toString();
            if (!uploadDocumentMap.containsKey(documentId))
            {
                uploadDocumentMap.put(documentId, new ArrayList<UploadDocumentDto>());
            }
            uploadDocumentMap.get(documentId).add(uploadDocument);
        }
        return uploadDocumentMap;
    }




    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#deleteDocumentUpload(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto)
     */
    @Override
    public boolean deleteDocumentUpload(final Dossier dossier, final UploadDocumentDto uploadDocumentDto)
    {
        int deleteCount = getJdbcTemplate().update(DELETE_UPLOAD_DOCUMENT,
                                                   new Object[] {uploadDocumentDto.getId(), uploadDocumentDto.getUserId()},
                                                   new int[] {Types.INTEGER, Types.INTEGER});

        // Delete the file from disk
        if (deleteCount > 0)
        {
            final File file = uploadDocumentDto.getFile();

            if (file.exists() && !file.delete())
            {
                log.error("Unable to delete upload document file {} for Dossier {}, however the UserUploadDocument table record has been deleted.",
                          uploadDocumentDto.getUploadFileName(), uploadDocumentDto.getDossierId());

                return false;//file remains
            }

            log.info("Record Delete for user {}: table=UserUploadDocument, key={}",
                     new Object[] { dossier.getAction().getCandidate().getUserId(), "UserUploadDocument", uploadDocumentDto.getId() });

            return true;//file deleted or didn't exist
        }
        else
        {
            log.error("Unable to delete upload document record for Dossier {}", uploadDocumentDto.getDossierId());

            return false;//upload record remains
        }
    }




    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getSnapshot(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole, int, int)
     */
    @Override
    public SnapshotDto getSnapshot(final Dossier dossier, final MivRole mivRole, final int schoolId, final int departmentId)
    {
        final Iterator<SnapshotDto> snapshotDtoList = get(GET_SNAPSHOT_SQL,
                                                    new  SnapshotRowMapper(),
                                                    dossier.getDossierId(),
                                                    mivRole.name(),
                                                    schoolId,
                                                    departmentId,
                                                    dossier.getLocation()).iterator();

        return snapshotDtoList.hasNext() ? snapshotDtoList.next() : null;
    }



//    "SnapshotLocation != '"+DossierLocation.CANDIDATE.mapLocationToWorkflowNode()+"' order by InsertTimestamp";

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getAllSnapshots(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List <SnapshotDto> getAllSnapshots(final Dossier dossier)
    {
        return get(GET_ALL_SNAPSHOTS_SQL, new  SnapshotRowMapper(), dossier.getDossierId());
    }



    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#updateSnapshot(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivRole, java.io.File, edu.ucdavis.mw.myinfovault.service.dossier.SchoolDepartmentCriteria)
     */
    @Override
    public SnapshotDto updateSnapshot(final Dossier dossier, final MivRole mivRole, final File snapshotFile, final SchoolDepartmentCriteria schoolDepartmentCriteria) throws WorkflowException
    {
        final String snapshotFileName = snapshotFile.getName();
        final long dossierId = dossier.getDossierId();
        final String mivRoleId = mivRole.toString();

        // Determine the school and department Ids. If the SchoolDepartmentCriteria is null, use the primary appointment
        // school and department Id from the dossier.
        int tempSchoolId = dossier.getPrimarySchoolId();
        int tempDepartmentId = dossier.getPrimaryDepartmentId();
        if (schoolDepartmentCriteria != null)
        {
            tempSchoolId = schoolDepartmentCriteria.getSchool();
            tempDepartmentId = schoolDepartmentCriteria.getDepartment();
        }

        final int schoolId = tempSchoolId;
        final int departmentId = tempDepartmentId;
        final String snapshotLocation = dossier.getLocation();

        // Use only major and minor version
        Pattern pattern = Pattern.compile("([0-9].[0-9])");
        Matcher matcher = pattern.matcher(MIVInformation.INSTANCE.getMivVersion());

        final String mivVersion = matcher.find() ? matcher.group(0) : "3.0";
        final String description = dossier.getAction().getDescription();
        final Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
        final int userId = dossier.getAction().getCandidate().getUserId();

        PreparedStatementCreator psc = null;

        SnapshotDto dossierSnapshot = this.getSnapshot(dossier, mivRole, schoolId, departmentId);
        if (dossierSnapshot != null)
        {
            final int snapshotId = dossierSnapshot.getSnapshotId();

            psc = new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(UPDATE_SNAPSHOT_SQL);
                    ps.setString(1, description);
                    ps.setString(2, snapshotFileName);
                    ps.setString(3, snapshotLocation);
                    ps.setString(4, mivVersion);
                    ps.setTimestamp(5, timeStamp);
                    ps.setInt(6, userId);
                    ps.setInt(7, snapshotId);
                    return ps;
                }
            };
        }
        else
        {
            psc = new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(INSERT_SNAPSHOT_SQL);
                    ps.setLong(1, 0);
                    ps.setLong(2, dossierId);
                    ps.setString(3, mivRoleId);
                    ps.setInt(4, schoolId);
                    ps.setInt(5, departmentId);
                    ps.setString(6, description);
                    ps.setString(7, snapshotFileName);
                    ps.setString(8, snapshotLocation);
                    ps.setString(9, mivVersion);
                    ps.setTimestamp(10, timeStamp);
                    ps.setInt(11, userId);
                    return ps;
                }
            };
        }
        int updateCount = 0;
        String msg = null;

        // Do the update/insert
        updateCount = this.getJdbcTemplate().update(psc);

        if (updateCount == 0)
        {
            msg = "Unable to update DossierSnapshot for Dossier" + dossier.getDossierId()
                    + ". Dossier ID does not exist in the Dossier table.";
            log.error(msg);
            throw new WorkflowException(msg);
        }

        return this.getSnapshot(dossier, mivRole, schoolId, departmentId);
    }




    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#insertUserUploadDocument(edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto)
     */
    @Override
    public void insertUserUploadDocument(final UploadDocumentDto uploadDocumentDto) throws WorkflowException
    {
        final int userId = uploadDocumentDto.getUserId();
        final int documentId = uploadDocumentDto.getDocumentId();
        final String uploadName = uploadDocumentDto.getUploadName();
        final String uploadFile = uploadDocumentDto.getUploadFileName();
        final String year = uploadDocumentDto.getYear();
        final boolean display = uploadDocumentDto.isDisplay();
        final boolean confidential = uploadDocumentDto.isConfidential();
        final boolean redacted = uploadDocumentDto.isRedacted();
        final String uploadLocation = uploadDocumentDto.getUploadLocation().mapLocationToWorkflowNodeName();
        final int dossierId = uploadDocumentDto.getDossierId();
        final int schoolId = uploadDocumentDto.getSchoolId();
        final int departmentId = uploadDocumentDto.getDepartmentId();
        final boolean uploadFirst = uploadDocumentDto.isUploadFirst();
        final int updateUserId = uploadDocumentDto.getUpdateUserId();
        final Timestamp timeStamp = new Timestamp(System.currentTimeMillis());

        // Get the seuence for this uploaded document
        final Object[] params = { userId, dossierId };
        final int[] paramTypes = { Types.INTEGER, Types.INTEGER };
        final int sequence = this.getJdbcTemplate().queryForObject(UPLOAD_SEQUENCE_SQL, params, paramTypes, Integer.class);

        // Insert the new upload record
        PreparedStatementCreator psc = null;

        psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement(INSERT_UPLOAD_SQL);
                ps.setLong(1, 0);
                ps.setLong(2, userId);
                ps.setInt(3, documentId);
                ps.setString(4, uploadName);
                ps.setString(5, uploadFile);
                ps.setString(6, year);
                ps.setInt(7, sequence);
                ps.setBoolean(8, display);
                ps.setBoolean(9, confidential);
                ps.setBoolean(10, redacted);
                ps.setString(11, uploadLocation);
                ps.setInt(12, dossierId);
                ps.setInt(13, schoolId);
                ps.setInt(14, departmentId);
                ps.setBoolean(15, uploadFirst);
                ps.setTimestamp(16, timeStamp);
                ps.setInt(17, updateUserId);
                return ps;
            }
        };

        int updateCount = 0;
        String msg = null;

        // Do the update/insert
        updateCount = this.getJdbcTemplate().update(psc);

        if (updateCount == 0)
        {
            msg = "Unable to update UserUploadDocument for uploaded PDF file " + uploadDocumentDto.getUploadFileName() + " for Dossier "
                    + uploadDocumentDto.getDossierId();
            log.error(msg);
            throw new WorkflowException(msg);
        }
    }



    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#updateUserUploadDocument(edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto)
     */
    @Override
    public void updateUserUploadDocument(final UploadDocumentDto uploadDocumentDto) throws WorkflowException
    {
        final int id = uploadDocumentDto.getId();
        final int userId = uploadDocumentDto.getUserId();
        final int documentId = uploadDocumentDto.getDocumentId();
        final String uploadName = uploadDocumentDto.getUploadName();
        final String uploadFile = uploadDocumentDto.getUploadFileName();
        final String year = uploadDocumentDto.getYear();
        final boolean display = uploadDocumentDto.isDisplay();
        final boolean confidential = uploadDocumentDto.isConfidential();
        final boolean redacted = uploadDocumentDto.isRedacted();
        final String uploadLocation = uploadDocumentDto.getUploadLocation().mapLocationToWorkflowNodeName();
        final int dossierId = uploadDocumentDto.getDossierId();
        final int schoolId = uploadDocumentDto.getSchoolId();
        final int departmentId = uploadDocumentDto.getDepartmentId();
        final boolean uploadFirst = uploadDocumentDto.isUploadFirst();
        final int updateUserId = uploadDocumentDto.getUpdateUserId();
        final Timestamp timeStamp = new Timestamp(System.currentTimeMillis());

        // Update upload record

        PreparedStatementCreator psc = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement(UPDATE_UPLOAD_SQL);
                ps.setLong(1, userId);
                ps.setInt(2, documentId);
                ps.setString(3, uploadName);
                ps.setString(4, uploadFile);
                ps.setString(5, year);
                ps.setBoolean(6, display);
                ps.setBoolean(7, confidential);
                ps.setBoolean(8, redacted);
                ps.setString(9, uploadLocation);
                ps.setInt(10, dossierId);
                ps.setInt(11, schoolId);
                ps.setInt(12, departmentId);
                ps.setBoolean(13, uploadFirst);
                ps.setInt(14, updateUserId);
                ps.setTimestamp(15, timeStamp);
                ps.setInt(16, id);
                return ps;
            }
        };

        int updateCount = 0;
        String msg = null;

        // Do the update/insert
        updateCount = this.getJdbcTemplate().update(psc);

        if (updateCount == 0)
        {
            msg = "Unable to update UserUploadDocument for uploaded PDF file " + uploadDocumentDto.getUploadFileName() + " for Dossier "
                    + uploadDocumentDto.getDossierId();
            log.error(msg);
            throw new WorkflowException(msg);
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#
     * updateDossierAndUserUploadDocument
     * (edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto,
     * edu.ucdavis.mw.myinfovault.domain.dossier.Dossier) Keeps both updates in
     * the same transaction.
     */
    @Override
    public void updateDossierAndUserUploadDocument(final Dossier dossier, final UploadDocumentDto uploadDocumentDto)
            throws WorkflowException, SQLException
    {
        // Update the upload table
        this.insertUserUploadDocument(uploadDocumentDto);
        // reload the dossier attributes
        this.loadDossierAttributes(dossier);
        // update the dossier
        this.updateDossier(dossier);
    }




    /*
     * (non-Javadoc)
     *
     * @see
     * edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getReviewers(edu.ucdavis
     * .mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<DossierReviewerDto> getDossierReviewersBySchoolAndDepartment(final long dossierId, final int schoolId, final int departmentId, final String reviewLocation)
    {
        return get(REVIEWERS_QUERY_SCH_DEPT_SQL, dossierReviewerRowMapper, dossierId, schoolId, departmentId, reviewLocation);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getDossiersToReview(int)
     */
    @Override
    public List<DossierReviewerDto> getDossiersToReview(final int userId)
    {
        return get(DOSSIERS_TO_REVIEW_SQL, dossierReviewerRowMapper, userId, userId);
    }

    /**
     * Maps records from the 'DossierReviewers' table to {@link DossierReviewerDto} objects.
     */
    private RowMapper<DossierReviewerDto> dossierReviewerRowMapper = new RowMapper<DossierReviewerDto>()
    {
        @Override
        public DossierReviewerDto mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new DossierReviewerDto(rs.getInt("ID"),
                                          rs.getInt("EntityID"),
                                          EntityType.valueOf(rs.getString("EntityType").toUpperCase()),
                                          rs.getLong("DossierID"),
                                          rs.getInt("SchoolID"),
                                          rs.getInt("DepartmentID"),
                                          rs.getString("ReviewLocation"),
                                          rs.getInt("AssignedByUserID"),
                                          MivRole.valueOf(rs.getString("AssignedByUserRole").toUpperCase()),
                                          rs.getTimestamp("InsertTimestamp"),
                                          rs.getInt("InsertUserID"),
                                          rs.getInt("UpdateUserID"),
                                          rs.getTimestamp("UpdateTimestamp"));
        }
    };


    /**
     * DossierDocumentRowMapper
     * @author rhendric
     */
    private class DossierDocumentRowMapper implements RowMapper<DossierDocumentDto>
    {
        final Map<String, Map<String, String>> attributeTypeMap = MIVConfig.getConfig().getMap("attributetypesbyname");
        @Override
        public DossierDocumentDto mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            DossierDocumentDto dossierDocumentDto = new DossierDocumentDto();

            dossierDocumentDto.setDescription(rs.getString("Description"));
            dossierDocumentDto.setCollectionId(rs.getInt("CollectionID"));
            dossierDocumentDto.setDocumentId(rs.getInt("DocumentID"));
            dossierDocumentDto.setCode(rs.getString("Code"));
            dossierDocumentDto.setRedactedCode(rs.getString("RedactedCode"));
            dossierDocumentDto.setFileName(rs.getString("DossierFileName"));
            dossierDocumentDto.setPacketFileName(rs.getString("PacketFileName"));

            // Get the attribute type from the attributeTypeMap
            dossierDocumentDto.setDossierAttributeType(DossierAttributeType.DOCUMENT); // default
            String attributeName = rs.getString("AttributeName");

            String dossierAttributeType = null;
            if ((attributeName != null) && (attributeTypeMap.get(attributeName) != null))
            {
                dossierAttributeType = attributeTypeMap.get(attributeName).get("type");
                dossierDocumentDto.setDossierAttributeType(DossierAttributeType.valueOf(dossierAttributeType));
            }

            dossierDocumentDto.setAttributeName(attributeName);
            dossierDocumentDto.setAllowRedaction(rs.getBoolean("AllowRedaction"));
            dossierDocumentDto.setCrossDepartmentPermission(rs.getBoolean("CrossDepartmentPermission"));
            dossierDocumentDto.setCandidateFile(rs.getBoolean("CandidateFile"));
            dossierDocumentDto.setViewNonRedactedDocIds(rs.getString("ViewNonRedactedDocIds"));
            return dossierDocumentDto;
        }
    }


    /**
     * Creates and maps 'Dossier' table records to {@link Dossier} objects.
     */
    private RowMapper<Dossier> dossierMapper = new RowMapper<Dossier>()
    {
        @Override
        public Dossier mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new Dossier(rs.getInt("ID"),
                               rs.getInt("AcademicActionID"),
                               rs.getLong("DossierID"),
                               rs.getInt("PrimarySchoolID"),
                               rs.getInt("PrimaryDepartmentID"),
                               rs.getTimestamp("SubmittedDate"),
                               rs.getTimestamp("LastRoutedDate"),
                               rs.getTimestamp("ArchivedDate"),
                               rs.getTimestamp("CompletedDate"),
                               loadDossierAcademicAction(rs.getInt("AcademicActionID")),
                               rs.getString("ReviewType") == null ? null : DossierReviewType.valueOf(rs.getString("ReviewType")),
                               rs.getBoolean("RecommendedActionFormPresent"),
                               rs.getBoolean("AcademicActionFormPresent"),
                               rs.getString("WorkflowLocations"),
                               !StringUtils.isEmpty(rs.getString("PreviousWorkflowLocations"))
                             ? new ArrayList<String>(Arrays.asList(rs.getString("PreviousWorkflowLocations").split(",")))
                             : new ArrayList<String>(),
                               rs.getString("WorkflowStatus"));

        }
    };



    /**
     * SnapshotRowMapper
     * @author rhendric
     */
    private class SnapshotRowMapper implements RowMapper<SnapshotDto>
    {
        @Override
        public SnapshotDto mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            SnapshotDto snapshotDto = new SnapshotDto();
            snapshotDto.setSnapshotId(rs.getInt("ID"));
            snapshotDto.setDossierId(rs.getInt("DossierID"));
            snapshotDto.setMivRoleId(rs.getString("MivRoleID"));
            snapshotDto.setSchoolId(rs.getInt("SchoolID"));
            snapshotDto.setDepartmentId(rs.getInt("DepartmentID"));
            snapshotDto.setDescription(rs.getString("Description"));
            snapshotDto.setSnapshotFileName(rs.getString("SnapshotFileName"));
            snapshotDto.setSnapshotLocation(rs.getString("SnapshotLocation"));
            snapshotDto.setInsertTimestamp(rs.getTimestamp("InsertTimestamp"));
            snapshotDto.setInsertUserID(rs.getInt("InsertUserID"));
            snapshotDto.setUpdateTimestamp(rs.getTimestamp("UpdateTimestamp"));
            snapshotDto.setUpdateUserID(rs.getInt("UpdateUserID"));
            return snapshotDto;
        }
    }




    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getAllDossierIds()
     */
    @Override
    public List<Long> getAllDossierIds()
    {
        final RowMapper<Long> dossiers = new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException
            {
                Long dossierId = new Long(rs.getInt("DossierID"));
                return dossierId;
            }
        };
        return this.getJdbcTemplate().query(DOSSIERS, dossiers);
    }




    @Override
    public List<Long> getAllActiveDossierIds()
    {
        final RowMapper<Long> dossiers = new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet rs, int rowNum) throws SQLException
            {
                Long dossierId = new Long(rs.getInt("DossierID"));
                return dossierId;
            }
        };
        return this.getJdbcTemplate().query(ACTIVE_DOSSIERS, dossiers);
    }



    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#insertDossierReviewers(java.util.Set)
     */
    @Override
    public void insertDossierReviewers(final Set<DossierReviewerDto> insertReviewers)
    {
        final Iterator<DossierReviewerDto> it = insertReviewers.iterator();

        getJdbcTemplate().batchUpdate(UPDATE_INSERT_REVIEWERS_SQL, new AbstractInterruptibleBatchPreparedStatementSetter() {
            @Override
            public boolean setValuesIfAvailable(PreparedStatement ps, int i) throws SQLException
            {
                if (it.hasNext())
                {
                    DossierReviewerDto reviewer = it.next();

                    ps.setInt(1, reviewer.getEntityId());
                    ps.setString(2, reviewer.getEntityType().toString());
                    ps.setLong(3, reviewer.getDossierId());
                    ps.setLong(4, reviewer.getSchoolId());
                    ps.setLong(5, reviewer.getDepartmentId());
                    ps.setString(6, reviewer.getReviewLocation());
                    ps.setInt(7, reviewer.getAssignedByUserID());
                    ps.setString(8, reviewer.getAssignedByUserRole().name());
                    ps.setInt(9, reviewer.getInsertUserID());

                    //have reviewer insert to add to the batch
                    return true;
                }

                //do not have reviewer insert to add to the batch
                return false;
            }
        });
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#deleteDossierReviewers(java.util.Set)
     */
    @Override
    public void deleteDossierReviewers(final Set<DossierReviewerDto> deleteReviewers)
    {
        //delete old reviewers if any
        if (!deleteReviewers.isEmpty())
        {
            final int batchSize = deleteReviewers.size();
            final List<Integer> ids = new ArrayList<Integer>(batchSize);

            for (DossierReviewerDto deleteReviewer : deleteReviewers)
            {
                ids.add(deleteReviewer.getID());
            }

            getJdbcTemplate().batchUpdate(UPDATE_DELETE_REVIEWERS_SQL, new BatchPreparedStatementSetter() {
                @Override
                public void setValues(PreparedStatement ps, int i) throws SQLException
                {
                    ps.setInt(1, ids.get(i));
                }

                @Override
                public int getBatchSize()
                {
                    return batchSize;
                }
            });
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#deleteDossierReviewers(long, int, int)
     */
    @Override
    public void  deleteDossierReviewers(final long dossierId, final int schoolId, final int departmentId)
    {
        final Object[] params = {dossierId, schoolId, departmentId};
        final int[] paramTypes = {Types.INTEGER, Types.INTEGER, Types.INTEGER };

        if (this.getJdbcTemplate().update(DELETE_REVIEWERS_SQL, params, paramTypes) == 0)
        {
            String msg = "Could not delete dossier "+dossierId+" reviewers for school "+schoolId+" and department "+departmentId+".";
            log.error(msg);
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getAllAppointments(long)
     */
    @Override
    public List<DossierAppointmentDto> getAllAppointments(final long dossierId) throws WorkflowException, SQLException
    {
        // Get the Dossier, which contains the primary appointment school and department
        final Dossier dossier = this.getDossier(dossierId);
        return getAllAppointments(dossier);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getAllAppointments(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<DossierAppointmentDto> getAllAppointments(final Dossier dossier) throws WorkflowException, SQLException
    {
        final List <DossierAppointmentDto> dossierAppointments = new ArrayList<DossierAppointmentDto>();
        final DossierAppointmentDto dossierAppointment = new DossierAppointmentDto();
        dossierAppointment.setDossierId(dossier.getDossierId());
        dossierAppointment.setSchoolId(dossier.getPrimarySchoolId());
        dossierAppointment.setDepartmentId(dossier.getPrimaryDepartmentId());
        dossierAppointment.setPrimary(true);
        dossierAppointments.add(dossierAppointment);

        // Get the joint appointments for the dossier
        final List <DossierAppointmentDto> jointAppointments = this.getJointAppointments(dossier.getDossierId());
        if (!jointAppointments.isEmpty())
        {
            dossierAppointments.addAll(jointAppointments);
        }
        return dossierAppointments;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getJointAppointments(long)
     */
    @Override
    public List<DossierAppointmentDto> getJointAppointments(final long dossierId) throws SQLException
    {
        // Joint Appointment data query
        final String JOINTAPPOINTMENTS_QUERY_FOR_LIST = "select * from JointAppointments ja where DossierId = ?";

        final RowMapper<DossierAppointmentDto> jointAppointments = new RowMapper<DossierAppointmentDto>() {
            @Override
            public DossierAppointmentDto mapRow(ResultSet rs, int rowNum) throws SQLException
            {
                DossierAppointmentDto dossierJointAppointmentDto = new DossierAppointmentDto();
                dossierJointAppointmentDto.setId(rs.getInt("ID"));
                dossierJointAppointmentDto.setDossierId(rs.getInt("DossierID"));
                dossierJointAppointmentDto.setSchoolId(rs.getInt("SchoolID"));
                dossierJointAppointmentDto.setDepartmentId(rs.getInt("DepartmentID"));
                dossierJointAppointmentDto.setComplete(rs.getBoolean("Complete"));
                dossierJointAppointmentDto.setInsertTimestamp(rs.getTimestamp("InsertTimestamp"));
                dossierJointAppointmentDto.setInsertUserID(rs.getInt("InsertUserID"));
                dossierJointAppointmentDto.setUpdateTimestamp(rs.getTimestamp("UpdateTimestamp"));
                dossierJointAppointmentDto.setUpdateUserID(rs.getInt("UpdateUserID"));
                dossierJointAppointmentDto.setPrimary(false);
                return dossierJointAppointmentDto;
            }
        };
        return get(JOINTAPPOINTMENTS_QUERY_FOR_LIST, jointAppointments, dossierId);
    }


    @Override
    public List<DossierAppointmentDto> getJointAppointments(final Dossier dossier) throws SQLException
    {
        return this.getJointAppointments(dossier.getDossierId());
    }


    /**
     * Helper method to check a value for a match in an attribute list.
     *
     * @param matchValue
     * @param attributeList
     * @return if matchValue equals a value in the attribute list
     */
    @SuppressWarnings("unused")
    private boolean checkAttribute(final String matchValue, final List<String> attributeList)
    {
        boolean qualified = false;
        for (String attribute : attributeList)
        {
            if (attribute.equalsIgnoreCase(matchValue))
            {
                qualified = true;
                break; // once it's set to true it's going to stay true, so don't look any further.
            }
        }
        return qualified;
    }


    /**
     * Load an upload document attribute for the specified dossier and attribute
     *
     * @param dossier
     * @param dossierAttribute
     * @param dossierAttributeType
     */
    private void loadUploadDocumentAttribute(final Dossier dossier, final DossierAttribute dossierAttribute, final DossierAttributeType dossierAttributeType)
    {
        // Get the Document by attribute name map
        final Map<String, Map<String, String>> documentByAttributeNameMap = MIVConfig.getConfig().getMap("documentsbyattributename");

        // Get the documentId mapped to this attribute name
        final String attributeName = dossierAttribute.getAttributeName();
        final Map<String, String> docAttributes = documentByAttributeNameMap.get(attributeName);

        // Get some basic info for this document type
        final String documentId = docAttributes.get("id");
        String description = docAttributes.get("alternatedescription");
        if (description == null)
        {
            description = docAttributes.get("description");
        }
//      Boolean signable = Boolean.valueOf(documentByAttributeNameMap.get(attributeName).get("signable").equalsIgnoreCase("1") ? "true" : "false");
        final boolean signable = "1".equals(docAttributes.get("signable"));
        final boolean redactable = "1".equals(docAttributes.get("allowredaction"));

        String uploadTableName = MIVConfig.getConfig().getProperty("default-config-pdfupload-tablename");
        if (uploadTableName == null)
        {
            uploadTableName = "UserUploadDocument";
        }

        // Load the uploaded documents for this dossier
        if (this.uploadDocumentMap == null || this.uploadDocumentMap.isEmpty())
        {
            this.uploadDocumentMap = this.getDocumentUploadFiles(dossier);
        }

        // If there are no upload documents of this type, exit
        if (!this.uploadDocumentMap.containsKey(documentId))
        {
            return;
        }

        // Get the uploaded documents for the current document id
        List<UploadDocumentDto> uploadDocumentList = this.uploadDocumentMap.get(documentId);

        // Add those with the current school and department
        for (UploadDocumentDto uploadDocument : uploadDocumentList)
        {
                int dossierAttributeDeptId = dossierAttribute.getDeptId();
                int dossierAttributeSchoolId = dossierAttribute.getSchoolId();

            if ((uploadDocument.getSchoolId() == dossierAttributeSchoolId)
                    && (uploadDocument.getDepartmentId() == dossierAttributeDeptId))
            {
                // Build a dto for each document
                final DossierFileDto dossierFileDto = new DossierFileDto();
                dossierFileDto.setRecordId(uploadDocument.getId());
                dossierFileDto.setDocumentId(Integer.parseInt(documentId));
                dossierFileDto.setDossierId(dossier.getDossierId());
                dossierFileDto.setRedactable(redactable);
                dossierFileDto.setRedacted(uploadDocument.isRedacted());
                dossierFileDto.setSignable(signable);
                dossierFileDto.setId(uploadDocument.getId());
                dossierFileDto.setTableName(uploadTableName);
                dossierFileDto.setSchoolId(uploadDocument.getSchoolId());
                dossierFileDto.setDepartmentId(uploadDocument.getDepartmentId());

                /*
                 * Set file description as attribute name if no document upload name.
                 */
                dossierFileDto.setFileLabel(!StringUtils.isEmpty(uploadDocument.getUploadName())
                                          ? uploadDocument.getUploadName()
                                          : attributeName);

                // Check if this document is signable and the status of the signatures
                if (signable)
                {
                    SignatureStatus signatureStatus = signingService.getSignatureStatus(MivDocument.get(Integer.parseInt(documentId)),
                                                                                        uploadDocument.getId());
                    dossierFileDto.setSignatureStatus(signatureStatus);
                }

                // Construct the path for this file
                final File uploadFilePath = uploadDocument.getFile();
                dossierFileDto.setFilePath(uploadFilePath);

                // Construct the url form the path for this file
                dossierFileDto.setFileUrl(new PathConstructor().getUrlFromPath(uploadFilePath));

                // Add the uploaded document name to the file list for this attribute
                dossierAttribute.addDocument(dossierFileDto);
                dossierAttribute.setAttributeValue(DossierAttributeStatus.ADDED);
            }
        }
    }


//    /**
//     * Load an upload document signature attribute for the specified dossier and attribute.
//     *
//     * @param dossier
//     * @param dossierAttribute
//     * @param dossierAttributeType
//     * @param isPrimary
//     */
//    private void loadDocumentUploadSignatureAttribute(Dossier dossier, DossierAttribute dossierAttribute, DossierAttributeType dossierAttributeType, boolean isPrimary)
//    {
//        // The DocumentUploadSignature type can be a document upload and/or a document
//        // *** Load the document attribute first ***
//        this.loadDocumentAttribute(dossier, dossierAttribute, isPrimary);
//        // Now process the upload
//        this.loadUploadDocumentAttribute(dossier, dossierAttribute, dossierAttributeType);
//    }


    /**
     * Load a document attribute for the specified dossier and attribute
     *
     * @param dossier
     * @param dossierAttribute
     * @param isPrimary
     */
    private void loadDocumentAttribute(final Dossier dossier, final DossierAttribute dossierAttribute, final boolean isPrimary)
    {
        // Get the Document by attribute name map
        final Map<String, Map<String, String>> documentByAttributeNameMap = MIVConfig.getConfig().getMap("documentsbyattributename");

        // Get the documentId mapped to this attribute name
        final String attributeName = dossierAttribute.getAttributeName();
        final Map<String, String> docAttributes = documentByAttributeNameMap.get(attributeName);

        /*
         * Set file label
         */
        String label = attributeName;
        if (!StringUtils.isEmpty(docAttributes.get("alternatedescription")))
        {
            label = docAttributes.get("alternatedescription");
        }
        else if (!StringUtils.isEmpty(docAttributes.get("description")))
        {
            label = docAttributes.get("description");
        }

        final boolean signable = "1".equals(docAttributes.get("signable"));
        final boolean redactable = "1".equals(docAttributes.get("allowredaction"));

        // Build a DTO for the document
        final DossierFileDto dossierFileDto = new DossierFileDto();
        dossierFileDto.setDocumentId(Integer.parseInt(docAttributes.get("id")));
        dossierFileDto.setDocumentType(MivDocument.get(dossierAttribute.getAttributeName()));
        dossierFileDto.setDossierId(dossier.getDossierId());
        dossierFileDto.setRedactable(redactable);
        dossierFileDto.setRedacted(false);
        dossierFileDto.setSignable(signable);
        dossierFileDto.setSchoolId(dossierAttribute.getSchoolId());
        dossierFileDto.setDepartmentId(dossierAttribute.getDeptId());
        dossierFileDto.setFileLabel(label);

        switch (dossierFileDto.getDocumentType())
        {
            case AA:
            case RAF:
                // if RAF or AA has been created
                if (dossier.isRecommendedActionFormPresent()
                    || dossier.isAcademicActionFormPresent())
                {
                    File dossierFilePath = dossier.getDossierPdf(dossierFileDto.getDocumentType());

                    if (dossierFilePath.exists())
                    {
                        dossierFileDto.setFilePath(dossierFilePath);

                        // Construct the URL form the path for this file
                        dossierFileDto.setFileUrl(new PathConstructor().getUrlFromPath(dossierFilePath));

                        // load RAF document to attribute
                        dossierAttribute.addDocument(dossierFileDto);
                        dossierAttribute.setAttributeValue(DossierAttributeStatus.ADDED);
                    }
                }

                break;
            // Recommended Action Form and appeal decision signature pieces.
            case DEANS_FINAL_DECISION:
            case DEANS_RECOMMENDATION:
            case DEANS_APPEAL_DECISION:
            case JOINT_DEANS_RECOMMENDATION:
            case JOINT_DEANS_APPEAL_RECOMMENDATION:
            case VICEPROVOSTS_RECOMMENDATION:
            case VICEPROVOSTS_FINAL_DECISION:
            case PROVOSTS_RECOMMENDATION:
            case PROVOSTS_TENURE_RECOMMENDATION:
            case PROVOSTS_TENURE_DECISION:
            case PROVOSTS_FINAL_DECISION:
            case CHANCELLORS_FINAL_DECISION:
            case VICEPROVOSTS_APPEAL_RECOMMENDATION:
            case VICEPROVOSTS_APPEAL_DECISION:
            case PROVOSTS_APPEAL_RECOMMENDATION:
            case PROVOSTS_TENURE_APPEAL_DECISION:
            case PROVOSTS_TENURE_APPEAL_RECOMMENDATION:
            case PROVOSTS_APPEAL_DECISION:
            case CHANCELLORS_APPEAL_DECISION:
                          // get the signable document for the given dossier, scope, and document type
                Decision decision = signingService.getDocument(dossier,
                                                               dossierFileDto.getSchoolId(),
                                                               dossierFileDto.getDepartmentId(),
                                                               dossierFileDto.getDocumentType());

                // See if the document is signed
                SignatureStatus signatureStatus = signingService.getSignatureStatus(decision);
                dossierFileDto.setSignatureStatus(signatureStatus);

                // document has a signature
                if (signatureStatus == SignatureStatus.SIGNED
                 || signatureStatus == SignatureStatus.INVALID)
                {
                    dossierFileDto.setFilePath(decision.getDossierFile());

                    // Construct the URL form the path for this file
                    dossierFileDto.setFileUrl(new PathConstructor().getUrlFromPath(dossierFileDto.getFilePath()));

                    dossierAttribute.addDocument(dossierFileDto);
                    dossierAttribute.setAttributeValue(DossierAttributeStatus.ADDED);

                    /*
                     * If signed/invalid dossier attribute is a provost
                     * tenure recommendation, it must be displayed.
                     */
                    if (MivDocument.PROVOSTS_TENURE_RECOMMENDATION == dossierFileDto.getDocumentType()
                     || MivDocument.PROVOSTS_TENURE_APPEAL_RECOMMENDATION == dossierFileDto.getDocumentType())
                    {
                        dossierAttribute.setDisplay(true);
                    }
                }

                break;
            // Disclosure Certificate
            case DISCLOSURE_CERTIFICATE:
            case JOINT_DISCLOSURE_CERTIFICATE:
                DCStatus status = dcService.getStatus(dossier,
                                                      dossierAttribute.getSchoolId(),
                                                      dossierAttribute.getDeptId());

                // if the DC is not missing
                if (status != DCStatus.MISSING)
                {
                    DCBo dc = dcService.getDisclosureCertificate(dossier,
                                                                 dossierAttribute.getSchoolId(),
                                                                 dossierAttribute.getDeptId());

                    dossierFileDto.setSignatureStatus(signingService.getSignatureStatus(dc));

                    // Construct the path for this file
                    dossierFileDto.setFilePath(dc.getDossierFile());

                    // Construct the URL form the path for this file
                    dossierFileDto.setFileUrl(new PathConstructor().getUrlFromPath(dossierFileDto.getFilePath()));

                    // Add the dossierFileDto to the documents
                    dossierAttribute.addDocument(dossierFileDto);

                    try
                    {
                        dossierAttribute.setAttributeValue(DossierAttributeStatus.valueOf(status.name()));
                    }
                    catch (IllegalArgumentException e)
                    {
                        throw new MivSevereApplicationError("errors.dossier.attribute.value", e, status);
                    }
                }
                break;
            default: break;
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#getAttributeNames(edu.ucdavis.mw.myinfovault.domain.action.DossierLocation, edu.ucdavis.mw.myinfovault.service.dossier.DossierAppointmentType, edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority, edu.ucdavis.mw.myinfovault.domain.action.DossierActionType)
     */
    @Override
    public List<String> getAttributeNames(final DossierLocation dossierLocation,
                                          final DossierAppointmentType appointmentType,
                                          final DossierDelegationAuthority delegationAuthority,
                                          final DossierActionType actionType) throws DataAccessException
    {
        // Query to get attributes appplicable to a specific delegation
        // authority, location, and appointment type
        final String DOSSIERATTRIBUTES_QUERY_FOR_LOCATION =
            "SELECT dloc.DossierAttributeID, dtype.Description, dtype.Name, dtype.ActionTypeIds" +
            " FROM" +
            " DossierAttributeType dtype," +
            " DelegationAuthorityAttributeCollection dcol," +
            " DossierAttributeLocation dloc," +
            " DelegationAuthority dauth," +
            " WorkflowLocation wfl" +
            " WHERE" +
            " dauth.DelegationAuthority=? AND" +
            " dauth.ID=dcol.DelegationAuthorityID AND" +
            " wfl.WorkflowNodeName=? AND" +
            " dloc.LocationID = wfl.ID AND" +
            " dloc.AppointmentType=? AND" +
            " dcol.DossierAttributeLocationID=dloc.ID AND" +
            " dtype.ID=dloc.DossierAttributeID" +
            " ORDER BY dloc.Sequence";

        final Object[] params = { delegationAuthority.toString(), dossierLocation.mapLocationToWorkflowNodeName(), appointmentType };
        final int[] paramTypes = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

        // Get the dossier attribute types.
        List<Map<String, Object>> dossierAttributeTypes = this.getJdbcTemplate().queryForList(DOSSIERATTRIBUTES_QUERY_FOR_LOCATION, params, paramTypes);

        final List<String> attributeNames = new ArrayList<String>();

        for (Map<String, Object> attributeTypeRecordMap : dossierAttributeTypes)
        {
            // Check if the attribute is valid for this dossier's actionType
            final String actionTypeIds = (String) attributeTypeRecordMap.get("ActionTypeIds");

            // If there are none, the attribute is used for all action types
            if (actionTypeIds != null)
            {
                // Get the comma delimited list of valid action type id's for this attribute
                List<String> actionTypeIdList = Arrays.asList(actionTypeIds.split(","));
                if (!actionTypeIdList.contains(actionType.getActionTypeId()+""))
                {
                    continue;
                }
            }
            attributeNames.add((String) attributeTypeRecordMap.get("Name"));
        }
        return attributeNames;
    }

    /**
     * Internal class to delete a directory and its contents
     */
    private boolean deleteDirectory(final File path)
    {
        if (path.exists())
        {
            final File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++)
            {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                }
                else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    /*
     * findUsers support for building a query based on an AttributeSet
     */
    private static Map<String, SearchCondition> searches = new HashMap<String, SearchCondition>();
    static
    {
        searches.put(MivSearchKeys.Person.MIV_ACTIVE_USER, new SearchCondition(" AND Active=true", 0));
        searches.put(MivSearchKeys.Person.NAME, new SearchCondition(
                " AND ( PreferredName LIKE ? OR getUserSortNameByUserID(u.UserID) LIKE CONVERT(? USING utf8) )", 2));
        searches.put(MivSearchKeys.Person.FIRST_NAME, new SearchCondition(" AND GivenName LIKE ?", 1));
        searches.put(MivSearchKeys.Person.LAST_NAME, new SearchCondition(" AND Surname LIKE ?", 1));
    }


    /**
     * FIXME: This was copied from MivPersonDaoImpl, which has since been refactored
     * to extract the class to {@link edu.ucdavis.myinfovault.data.QueryConditionBuilder}
     * There is a difference here, and that is that the int "SearchCondition.argc" is
     * being accessed by {@link DossierDao#findDossiersByUserNameSearchCriteriaAndLocation(SearchCriteria,DossierLocation)}
     * even though argc is declared private
     * Replace this private class with that public one when possible.
     */
    private static class SearchCondition
    {
        private final String sql;
        private final int argc;

        SearchCondition(final String sql, final int argc)
        {
            this.sql = sql;
            this.argc = argc;
        }

        String getSql()
        {
            return this.sql;
        }

        /*
         * Ignores extra args, repeats last arg if too few args given
         */
        void addArgs(final List<String> args, final String... arg)
        {
            // e.g. 3 needed, 1 given
            final int loopIterations = Math.min(arg.length, this.argc);
            // e.g. would have iters = 1
            for (int i = 0; i < loopIterations; i++)
            {
                args.add(arg[i]);
            }
            if (arg.length < this.argc) // e.g. 1 < 3
            {
                // not enough args passed... keep repeating the last arg given
                for (int i = 0; i < this.argc - arg.length; i++)
                {
                    args.add(arg[arg.length - 1]);
                }
            }
        }

        @Override
        public String toString()
        {
            return sql;
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#findDossiersForUserName(edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<Long> findDossiersByUserNameSearchCriteria(final SearchCriteria criteria)
    {
        return this.findDossiersByUserNameSearchCriteriaAndLocation(criteria, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao#findDossiersByUserNameSearchCriteriaAndLocation(edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public List<Long> findDossiersByUserNameSearchCriteriaAndLocation(final SearchCriteria criteria, final DossierLocation location)
    {

        final String searchName = criteria.getInputName();
        String searchInitial = criteria.getLname();

        String searchKey;
        String searchString;
        if (!searchName.isEmpty())   // The name was entered so search for that
        {
            searchKey = MivSearchKeys.Person.NAME;
            searchString = "*" + searchName.toLowerCase() + "*";
            searchString = StringUtil.prepareTextSearchCriteria(searchString,"*");
        }
        else // Otherwise use the first initial of last name
        {
            // When "all" was chosen blank out the initial so just a wildcard remains
            if ("all".equalsIgnoreCase(searchInitial)) searchInitial = "";
            searchKey = MivSearchKeys.Person.LAST_NAME;
            searchString = searchInitial.toLowerCase() + "*";
        }

        AttributeSet as = new AttributeSet();
        as.put(searchKey, searchString);
        if (criteria.isActiveOnly())
        {
            as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");
        }


        List<String> args = new ArrayList<String>();
        int paramCount = 0;

        StringBuilder sql = new StringBuilder(FIND_DOSSIERS_PARTIAL_SQL);
        if (location != null)
        {
            sql = new StringBuilder(FIND_DOSSIERS_BY_LOCATION_PARTIAL_SQL);
            args.add(location.mapLocationToWorkflowNodeName());
            paramCount++;
        }
        for (String key : as.keySet()){
            SearchCondition c = searches.get(key);
            if (c != null)
            {
                sql.append(c.getSql());
                c.addArgs(args, searchString.replace('*', '%'));
                paramCount += c.argc;
            }
        }

        final int [] paramTypes = new int[paramCount];
        for (int i = 0; i<paramCount; i++ )
        {
            paramTypes[i] = Types.VARCHAR;
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("Script:: " + sql.toString());
            logger.debug("Args:: " + args.toArray().length);
            int count = 0;
            for (String string : args) {
                logger.debug("Args[" + ++count + "] :: " + string);
            }
        }

        List<Map<String, Object>> dossiers = new ArrayList<>();
        dossiers = this.getJdbcTemplate().queryForList(sql.toString(), args.toArray(), paramTypes);
        ArrayList<Long> dossierIdList = new ArrayList<Long>();

        for (Map<String, Object> dossierRecordMap : dossiers)
        {
            dossierIdList.add(new Long((Integer) dossierRecordMap.get("DossierID")));
        }

        return dossierIdList;
    }



    @Override
    public List<Long> getDossiersByAttributeCriteria(final int schoolId, final int departmentId, final DossierLocation matchLocation,
                                                     final Map<String, String> attributeCriteria) throws SQLException
    {
        List<Long> results = new ArrayList<Long>();
        List<Map<String, Object>> dossiers = new ArrayList<>();
        int numberOfAttributes = attributeCriteria.size();
        String sqlString = "";
        String location = matchLocation.mapLocationToWorkflowNodeName();
        if (location.contains("School") || location.contains("ViceProvost"))
        {
            location = "%"+location+"%";
        }

        if (attributeCriteria.containsKey("%dean%_release")) { //this must be deans decision/recommendation
            final Object[] params = { location, "deans_final_decision", "deans_recommendation", "ADDED", "%dean%_release", "RELEASE"};
            final int[] paramTypes = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
            sqlString = DOSSIERATTRIBUTES_QUERY_WITH_CRITERIA + DOSSIERATTRIBUTES_SUBQUERY_FOR_DEANS_DECISION;
            dossiers = this.getJdbcTemplate().queryForList(sqlString, params, paramTypes);
        }
        else if (numberOfAttributes == 1) {
            final Object[] params = { location, "1", "2"};
            final int[] paramTypes = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
            sqlString = DOSSIERATTRIBUTES_QUERY_WITH_CRITERIA + DOSSIERATTRIBUTES_SUBQUERY_WITH_1CRITERIA;
            for (Map.Entry<String, String> entry : attributeCriteria.entrySet()) {
                params[1] = entry.getKey();
                String attrValue = entry.getValue();
                params[2] = attrValue;
                if (attrValue.startsWith("NOT")) {
                    attrValue = attrValue.replaceFirst("NOT_","");
                    params[2] =attrValue;
                 // Get the dossier attribute types.
                    sqlString = DOSSIERATTRIBUTES_QUERY_WITH_CRITERIA + DOSSIERATTRIBUTES_SUBQUERY_WITH_1NOT_CRITERIA;
                }
            }
            dossiers = this.getJdbcTemplate().queryForList(sqlString, params, paramTypes);
        }
        else {
            final Object[] params = { location, "1", "2", "3", "4"};
            final int[] paramTypes = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
            sqlString = DOSSIERATTRIBUTES_QUERY_WITH_CRITERIA + DOSSIERATTRIBUTES_SUBQUERY_WITH_2CRITERIA;
            int i = 0;
            for (Map.Entry<String, String> entry : attributeCriteria.entrySet()) {
                String attrValue = entry.getValue();
                if (attributeCriteria.containsValue("NOT_ADDED") || attributeCriteria.containsValue("NOT_SIGNED")) {
                    sqlString = DOSSIERATTRIBUTES_QUERY_WITH_CRITERIA + DOSSIERATTRIBUTES_SUBQUERY_WITH_2NOT_CRITERIA;
                    if (attrValue.startsWith("NOT")) {
                        attrValue = attrValue.replaceFirst("NOT_","");
                        params[1] = entry.getKey();
                        params[2] = attrValue;
                        // Get the dossier attribute types.
                    }
                    else {
                        params[3] = entry.getKey();
                        params[4] = entry.getValue();
                    }
                }
                else {
                    ++i;
                    params[i] = entry.getKey();
                    ++i;
                    params[i] = entry.getValue();
                }
            }
            dossiers = this.getJdbcTemplate().queryForList(sqlString, params, paramTypes);
        }

        for (Map<String,Object> map : dossiers) {
            results.add(new Long((Integer) map.get("DossierID")));
        }

        return results;
    }


    /**
     * Replaces org.kuali.rice.kew.service.WorkflowInfo for the limited ways we use it.
     * @author Stephen Paulsen
     *
     */
    class WorkflowInfo
    {

        public String[] getCurrentNodeNames(final Dossier dossier)
        {
            final String[] nodes = new String[] { dossier.getLocation() };
            return nodes;
        }

        public String getDocumentStatus(final Dossier dossier)
        {
            return dossier.getWorkflowStatus();
        }

    }
}
