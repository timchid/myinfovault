/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventLogDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.event;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.action.Route;
import edu.ucdavis.mw.myinfovault.domain.event.EventLogEntry;
import edu.ucdavis.mw.myinfovault.events2.EventCategory;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Handles persistence and retrieval of {@link EventLogEntry} objects from the database.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class EventLogDaoImpl extends DaoSupport implements EventLogDao
{
    private final UserService userService = MivServiceLocator.getUserService();

    private final String ENTRY_SELECT_DOSSIER;
    private final String ENTRY_INSERT;
    private final String DETAIL_SELECT;
    private final String DETAIL_INSERT;

    /**
     * Create the event log DAO bean.
     */
    public EventLogDaoImpl()
    {
        ENTRY_SELECT_DOSSIER = getSQL("eventlog.select.dossier");
        ENTRY_INSERT = getSQL("eventlog.insert");
        DETAIL_SELECT = getSQL("eventlogdetails.select");
        DETAIL_INSERT = getSQL("eventlogdetails.insert");
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.event.EventLogDao#getEntries(long)
     */
    @Override
    public List<EventLogEntry> getEntries(long dossierId) throws DataAccessException
    {
        return get(ENTRY_SELECT_DOSSIER, entryMapper, dossierId);
    }

    /**
     * Get the event log details for the given event log ID.
     *
     * @param eventId event log ID to which the details belong
     * @return event log details
     * @throws DataAccessException if the query fails
     */
    private Map<EventCategory, Object> getDetails(byte[] eventId) throws DataAccessException
    {
        SqlRowSet rowSet = getJdbcTemplate().queryForRowSet(DETAIL_SELECT, new Object[] {eventId});

        Map<EventCategory, Object> details = new HashMap<EventCategory, Object>();

        while (rowSet.next())
        {
            EventCategory category = EventCategory.valueOf(rowSet.getString("Category"));

            details.put(category,
                        getDetailObject(category, rowSet.getString("Detail")));
        }

        return details;
    }

    /**
     * Creates and maps 'EventLog' table records to {@link EventLogEntry} objects.
     */
    private RowMapper<EventLogEntry> entryMapper = new RowMapper<EventLogEntry>()
    {
        @Override
        public EventLogEntry mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            byte[] uuid = rs.getBytes("UUID");

            return new EventLogEntry(uuid,
                                     rs.getString("Title"),
                                     userService.getPersonByMivId(rs.getInt("RealUserID")),
                                     userService.getPersonByMivId(rs.getInt("ShadowUserID")),
                                     rs.getLong("Posted"),
                                     rs.getString("Comments"),
                                     getDetails(uuid),
                                     rs.getBytes("ParentUUID"));
        }
    };

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.event.EventLogDao#persistEntry(byte[], long, edu.ucdavis.mw.myinfovault.domain.event.EventLogEntry)
     */
    @Override
    public void persistEntry(final long dossierId,
                             final EventLogEntry entry) throws DataAccessException
    {
        int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement(ENTRY_INSERT);

                ps.setBytes(1, entry.getId());

                if (entry.isChild()) {
                    ps.setBytes(2, entry.getParentId());
                }
                else {
                    ps.setNull(2, Types.BINARY);
                }

                ps.setLong(3, dossierId);
                ps.setLong(4, entry.getPosted());
                ps.setString(5, entry.getTitle());
                ps.setString(6, entry.getComment());
                ps.setInt(7, entry.getRealUser().getUserId());

                if (entry.getShadowUser() != null && entry.getShadowUser().getUserId() != entry.getRealUser().getUserId()) {
                    ps.setInt(8, entry.getShadowUser().getUserId());
                }
                else {
                    ps.setNull(8, Types.INTEGER);
                }

                return ps;
            }
        });

        // persist also details if the event log entry was persisted
        if (rowsAffected> 0)
        {
            for (final EventCategory category : entry.getCategories())
            {
                getJdbcTemplate().update(new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                    {
                        PreparedStatement ps = connection.prepareStatement(DETAIL_INSERT);

                        ps.setBytes(1, entry.getId());
                        ps.setString(2, category.name());

                        Object detail = entry.getDetail(category);

                        if (detail != null)
                        {
                            ps.setString(3, getDetailString(category, detail));
                        }
                        else
                        {
                            ps.setNull(3, Types.VARCHAR);
                        }


                        return ps;
                    }
                });
            }
        }
    }

    /**
     * Get the {@link String} representation to persist of the detail for the given category.
     *
     * @param category event category
     * @param detail detail object for which to return the representation
     * @return category detail representation
     */
    private static String getDetailString(EventCategory category, Object detail)
    {
        if (detail == null) return null;

        switch (category)
        {
            case ROUTE:
                return ((Route)detail).getOrigin() + "," + ((Route)detail).getDestination();
            case SIGNATURE:
            case HOLD:
            case RELEASE:
                return ((Enum<?>)detail).name();
            case REQUEST:
            case USER:
                return ((MivPerson)detail).getDisplayName();
            case ERROR:
                return ((Exception)detail).getLocalizedMessage();
            default:
                return detail.toString();
        }
    }

    /**
     * Get the detail object from the given persisted detail for this category.
     *
     * @param category event category
     * @param detail representation from which to derive the detail object
     * @return event category detail object or <code>null</code> if persisted detail is <code>null</code>
     */
    private static Object getDetailObject(EventCategory category, String detail)
    {
        if (detail == null) return null;

        switch (category)
        {
            case ROUTE:
                String[] route = detail.split(",");
                return new Route(DossierLocation.valueOf(route[0]), DossierLocation.valueOf(route[1]));
            case SIGNATURE:
                return MivDocument.valueOf(detail);
            case HOLD:
            case RELEASE:
                return DossierLocation.valueOf(detail);
            default:
                return detail;
        }
    }
}
