/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.packet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao;
import edu.ucdavis.mw.myinfovault.domain.annotation.Annotation;
import edu.ucdavis.mw.myinfovault.domain.annotation.AnnotationLine;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Data access object interface used to access PacketRequest objects.
 *
 * @author Rick Hendricks
 * @since MIV 5.0
 */
public class PacketDaoImpl extends DaoSupport implements PacketDao
{

    private final String PACKETREQUEST;
    private final String PACKETREQUESTS;
    private final String INSERTPACKETREQUEST;
    private final String UPDATEPACKETREQUEST;
    private final String DELETEPACKETREQUEST;
    private final String SELECT_PACKET_UPDATE_TIME;

    private static final long MASTER_PACKET_ID = 0;

    private final String PACKET;
    private final String PACKETS;
    private final String INSERTPACKET;
    private final String UPDATEPACKET;
    private final String DELETEPACKET;

    private final String PACKETCONTENT;
    private final String INSERTPACKETCONTENT;
    private final String DELETEPACKETCONTENT;
    private final String UPDATEPACKETCONTENT;

    private final String DELETEPACKETANNOTATIONS;


    /**
     * Filter out packet requests which have an associated packet
     */
    private static SearchFilter<PacketRequest> openPacketRequestFilter = new SearchFilterAdapter<PacketRequest>() {
        @Override
        public boolean include(PacketRequest item) {
            return item.getPacketId() == 0;
        }
    };

    private DossierService dossierService;
    private AnnotationDao annotationDao;

    public PacketDaoImpl()
    {
        PACKETREQUEST = getSQL("packetrequest.select.dossierid");
        PACKETREQUESTS = getSQL("packetrequest.select.userid");
        INSERTPACKETREQUEST = getSQL("packetrequest.insert");
        UPDATEPACKETREQUEST = getSQL("packetrequest.update");
        DELETEPACKETREQUEST = getSQL("packetrequest.delete");

        SELECT_PACKET_UPDATE_TIME = getSQL("packet.updatetime.select.packetid");

        PACKET = getSQL("packet.select.packetid");
        PACKETS = getSQL("packet.select.userid");
        INSERTPACKET = getSQL("packet.insert");
        UPDATEPACKET = getSQL("packet.update");
        DELETEPACKET = getSQL("packet.delete");

        PACKETCONTENT = getSQL("packetcontent.select");
        INSERTPACKETCONTENT = getSQL("packetcontent.insert");
        DELETEPACKETCONTENT = getSQL("packetcontent.delete");
        UPDATEPACKETCONTENT = getSQL("packetcontent.update");
        DELETEPACKETANNOTATIONS = getSQL("packet.annotations.delete");
    }

    /**
     * Spring injection setter for the dossier service.
     *
     * @param dossierService
     */
    public void setDossierService(DossierService dossierService)
    {
        this.dossierService = dossierService;
    }

    /**
     * Spring injection setter for the annotationDao.
     *
     * @param annotationDao
     */
    public void setAnnotationDao(AnnotationDao annotationDao)
    {
        this.annotationDao = annotationDao;
    }

    /**
     * Find all packet requests for the input userId
     * @param userId
     * @return List of PacketRequest objects
     */
    @Override
    public List<PacketRequest> getPacketRequests(final long userId)
    {
        return get(PACKETREQUESTS, new PacketRequestRowMapper(), userId);
    }

    /**
     * Get the packet request associated with the input dossier id
     * @param dossierId
     * @return PacketRequest
     */
    @Override
    public PacketRequest getPacketRequest(final long dossierId)
    {
        List<PacketRequest>packetRequests = get(PACKETREQUEST, new PacketRequestRowMapper(), dossierId);
        return packetRequests.isEmpty()  ? null : packetRequests.get(0);
    }

    /**
     * Update a PacketRequest
     * @param packetRequest
     */
    @Override
    public boolean updatePacketRequest(final PacketRequest packetRequest, final int updateUserId)
    {
        final boolean isUpdate = packetRequest.getRequestId() > 0;

        KeyHolder keyHolder = new GeneratedKeyHolder();

        try
        {

            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(isUpdate ? UPDATEPACKETREQUEST : INSERTPACKETREQUEST,
                                                                       Statement.RETURN_GENERATED_KEYS);

                    if (isUpdate)
                    {
                        ps.setLong(1, packetRequest.getPacketId());
                        ps.setTimestamp(2, new java.sql.Timestamp(System.currentTimeMillis()));
                        ps.setInt(3, updateUserId);
                        ps.setInt(4, packetRequest.getRequestId());
                    }
                    else
                    {
                        ps.setInt(1, packetRequest.getUserId());
                        ps.setLong(2, packetRequest.getPacketId());
                        ps.setLong(3, packetRequest.getDossier().getDossierId());
                        ps.setLong(4, updateUserId);
                    }

                    return ps;
                }
            }, keyHolder);

            // successful update
            if (rowsAffected > 0)
            {
                // update requestId
                if (!isUpdate)
                {
                    packetRequest.setRequestId(keyHolder.getKey().intValue());
                }

                return true;
            }
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.packetrequest.dao.write", exception, packetRequest);
        }

        return false;
    }

    /**
     * Deletes a packetRequest
     * @param packetRequest
     */
    @Override
    public boolean deletePacketRequest(final PacketRequest packetRequest)
    {
        return this.deletePacketRequest(packetRequest.getRequestId());
    }

    /**
     * Deletes a packetRequest
     * @param int RequestId
     */
    @Override
    public boolean deletePacketRequest(final int requestId)
    {
        try
        {

            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(DELETEPACKETREQUEST);
                    ps.setInt(1, requestId);
                    return ps;
                }
            });

            // successful delete
            return rowsAffected > 0;

        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.packetrequest.dao.delete", exception, requestId);
        }

    }

    /**
     * Find open packet requests for a user
     * @param userId for which to find open packet requests.
     * @return List of PacketRrequest objects
     */
    @Override
    public List<PacketRequest> getOpenPacketRequests(final int userId)
    {
        List<PacketRequest> packetRequests = new ArrayList<PacketRequest>();
        packetRequests.addAll(PacketDaoImpl.openPacketRequestFilter.apply(this.getPacketRequests(userId)));
        return packetRequests;
    }

    /**
     * Find open packet requests for a user
     * @param userId for which to find open packet requests.
     * @return List of PacketRrequest objects
     */
    @Override
    public List<PacketRequest> getOpenPacketRequests(final MivPerson mivPerson)
    {
        return this.getOpenPacketRequests(mivPerson.getUserId());
    }


    /**
     * PacketRequestRowMapper
     * @author rhendric
     */
    private class PacketRequestRowMapper implements RowMapper<PacketRequest>
    {
        @Override
        public PacketRequest mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            Dossier dossier = null;
            try
            {
                dossier = dossierService.getDossier(rs.getLong("dossierId"));
            }
            catch (WorkflowException e)
            {
                log.error("Unable retrieve dossier for packet request "+rs.getInt("ID"), e);
            }

            Timestamp updateTime = rs.getTimestamp("UpdateTimestamp");

            return dossier == null ? null : new PacketRequest(dossier,
                                                              rs.getInt("packetId"),
                                                              rs.getInt("ID"),
                                                              new Date(rs.getTimestamp("InsertTimestamp").getTime()),
                                                              updateTime == null ? null : new Date(updateTime.getTime()));
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#getPacketUpdateTime(int)
     */
    @Override
    public Date getPacketUpdateTime(final long packetId)
    {
        if (packetId != MASTER_PACKET_ID)
        {
            List<Map<String, Object>> timestampsList = get(SELECT_PACKET_UPDATE_TIME, new ColumnMapRowMapper(), packetId);

            Date insert = null;
            Date update = null;

            for (Map<String, Object>timeStamp : timestampsList)
            {
                insert = timeStamp.get("inserttimestamp") == null ? null : new Date(((Timestamp) timeStamp.get("inserttimestamp")).getTime());
                update = timeStamp.get("updatetimestamp") == null ? null : new Date(((Timestamp) timeStamp.get("updatetimestamp")).getTime());
            }
            return update != null ? update : insert;
        }

        return null;
    }


    /**
     * PacketRowMapper
     * @author rhendric
     */
    private class PacketRowMapper implements RowMapper<Packet>
    {
        @Override
        public Packet mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            Long packetId = rs.getLong("ID");

            Timestamp insertTime = rs.getTimestamp("InsertTimestamp");
            Timestamp updateTime = rs.getTimestamp("UpdateTimestamp");

            //System.out.println("Packet #" + packetId + " created at [" + insertTime + "], updated [" + updateTime + "]");

            // original constructor call w/o timestamps
            // Packet p1 = new Packet(packetId, rs.getInt("UserID"), rs.getString("Name"), (PacketDaoImpl.this).getPacketContent(packetId));

            Packet p = new Packet(packetId, rs.getInt("UserID"), rs.getString("Name"),
                                  new Date( insertTime.getTime() ),
                                  // Set the update time, or set it to the insert time if record has never been updated.
                                  new Date( updateTime != null ? updateTime.getTime() : insertTime.getTime() ),
                                  (PacketDaoImpl.this).getPacketContent(packetId));

            return p;
        }
    }

    /**
     * PacketContentsRowMapper
     * @author rhendric
     */
    private class PacketContentRowMapper implements RowMapper<PacketContent>
    {
        @Override
        public PacketContent mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new PacketContent(rs.getInt("ID"), rs.getInt("PacketID"), rs.getInt("UserID"), rs.getInt("SectionID"), rs.getInt("RecordID"), rs.getString("DisplayParts"));
        }
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#getPackets(long)
     */
    @Override
    public List<Packet> getPackets(final int userId)
    {
        return get(PACKETS, new PacketRowMapper(), userId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#getPacket(long)
     */
    @Override
    public Packet getPacket(final long packetId)
    {
        List<Packet>packet = get(PACKET, new PacketRowMapper(), packetId);
        return packet.isEmpty()  ? null : packet.get(0);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#getPacket(edu.ucdavis.mw.myinfovault.domain.packet.Packet)
     */
    @Override
    public Packet getPacket(final Packet packet)
    {
        return this.getPacket(packet.getPacketId());
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#updatePacket(edu.ucdavis.mw.myinfovault.domain.packet.Packet, int)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Packet updatePacket(final Packet packet, final int updateUserId)
    {

        // Get previous packet data from database
        Packet previousPacket = this.getPacket(packet.getPacketId());

        Collection<PacketContent> currentItems = packet.getPacketItems();

        final boolean isUpdate = packet.getPacketId() > 0;

        // If we are updating, check the previous packet data from the database against the current packet data
        // and if there are no changes, exit
        if (isUpdate && !isUpdateRequired(packet, previousPacket))
        {
            return packet;
        }

        KeyHolder keyHolder = new GeneratedKeyHolder();

        try
        {

            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(isUpdate ? UPDATEPACKET : INSERTPACKET,
                                                                       Statement.RETURN_GENERATED_KEYS);

                    if (isUpdate)
                    {
                        ps.setString(1, packet.getPacketName());
                        ps.setTimestamp(2, new java.sql.Timestamp(System.currentTimeMillis()));
                        ps.setInt(3, updateUserId);
                        ps.setLong(4, packet.getPacketId());
                    }
                    else
                    {
                        ps.setInt(1, packet.getUserId());
                        ps.setString(2, packet.getPacketName());
                        ps.setLong(3, updateUserId);
                    }

                    return ps;
                }
            }, keyHolder);

            // Successful update, now update the packet content items
            if (rowsAffected > 0)
            {
                List<PacketContent>packetItemsToDelete = new ArrayList<PacketContent>();
                if (previousPacket != null)
                {
                    // Check for the presence of the packet content items from the previous packet content in the new packet content
                    Map<String, PacketContent> previousPacketItemsMap = previousPacket.getPacketItemsMap();
                    Map<String, PacketContent> newItemsMap = packet.getPacketItemsMap();
                    for (String key : previousPacketItemsMap.keySet())
                    {
                        if (!newItemsMap.containsKey(key))
                        {
                            packetItemsToDelete.add(previousPacketItemsMap.get(key));
                        }
                    }
                }

                // Items to be added
                Collection<PacketContent>packetItemsToAdd = CollectionUtils.select(currentItems, new Predicate() {
                    @Override
                    public boolean evaluate(Object item)
                    {
                        return ((PacketContent)item).getId() <= 0;
                    }
                });

                // Items to be updated
                Collection<PacketContent>packetItemsToUpdate = CollectionUtils.select(currentItems, new Predicate() {
                    @Override
                    public boolean evaluate(Object item)
                    {
                        return ((PacketContent)item).getId() > 0;
                    }
                });

                // Get the packet ID of a new record
                if (!isUpdate)
                {
                    packet.setPacketId(keyHolder.getKey().intValue());
                    // not an update, so we are adding all content items
                    packetItemsToAdd = packet.getPacketItems();
                }

                if (!packetItemsToAdd.isEmpty())
                {
                    this.insertPacketContent(packet);
                }

                if (!packetItemsToDelete.isEmpty())
                {
                    this.deletePacketItems(packetItemsToDelete);
                }

                if (!packetItemsToUpdate.isEmpty())
                {
                    this.updatePacketItems(new ArrayList<PacketContent>(packetItemsToUpdate));
                }
            }
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.packet.dao.write", exception, packet);
        }

        return this.getPacket(packet);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#insertPacketContent(java.util.List, int)
     */
    @Override
    public boolean insertPacketContent(final Packet packet)
    {
        final List<PacketContent>packetItemsToAdd = new ArrayList<PacketContent>();

        for (PacketContent packetContentItem : packet.getPacketItems())
        {
            // Skip items with an ID
            if(packetContentItem.getId() > 0)
            {
                continue;
            }
            packetItemsToAdd.add(packetContentItem);
        }

        int[] updateCounts = this.getJdbcTemplate().batchUpdate(INSERTPACKETCONTENT, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException
            {
                PacketContent packetContentItem = packetItemsToAdd.get(i);

                ps.setLong(1, packet.getPacketId());
                ps.setInt(2, packetContentItem.getUserId());
                ps.setInt(3, packetContentItem.getSectionId());
                ps.setInt(4, packetContentItem.getRecordId());
                ps.setString(5, packetContentItem.getDisplayParts());
            }

            @Override
            public int getBatchSize()
            {
                return packetItemsToAdd.size();
            }
        });

        return packetItemsToAdd.size() > 0 && updateCounts.length != packetItemsToAdd.size() ? false : true;
    }

    @Override
    public boolean updatePacketItems(final List<PacketContent>packetItems)
    {
        int[] updateCounts = this.getJdbcTemplate().batchUpdate(UPDATEPACKETCONTENT, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException
            {
                PacketContent packetContentItem = packetItems.get(i);
                ps.setString(1, packetContentItem.getDisplayParts());
                ps.setInt(2, packetContentItem.getId());
            }

            @Override
            public int getBatchSize()
            {
                return packetItems.size();
            }
        });

        return packetItems.size() > 0 && updateCounts.length != packetItems.size() ? false : true;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#deletePacket(int)
     */
    @Override
    public boolean deletePacket(final long packetId)
    {
        // Check the packet ID to insure that it is not a master
        if (packetId <= 0)
        {
            log.error("Attempt to delete a packet with an invalid packetId of {}", new Object[] {packetId});
            throw new MivSevereApplicationError("errors.packet.dao.delete", packetId);
        }

        try
        {
            // Delete any associated Annotations - we could on delete cascade from the Annotations table, but since we have
            // all master annotations with a packetId of zero (0), it is safer to explicitly delete and not have the cascade
            // on delete set on the Annotations table.
            getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(DELETEPACKETANNOTATIONS);
                    ps.setLong(1, packetId);
                    return ps;
                }
            });

            // Now delete the Packet record
            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(DELETEPACKET);
                    ps.setLong(1, packetId);
                    return ps;
                }
            });
            return  rowsAffected > 0;
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.packet.dao.delete", exception, packetId);
        }

    }

    /**
     * @param packetId
     * @return List of PacketContent items
     */
    protected List<PacketContent> getPacketContent(final long packetId)
    {
        return get(PACKETCONTENT, new PacketContentRowMapper(), packetId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#deletePacketItem(edu.ucdavis.mw.myinfovault.domain.packet.PacketContent)
     */
    @Override
    public boolean deletePacketItem(final PacketContent packetItem)
    {
        try
        {
            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(DELETEPACKETCONTENT);
                    ps.setLong(1, packetItem.getId());
                    return ps;
                }
            });

            // successful delete
            if (rowsAffected > 0)
            {
                return true;
            }
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.packetcontent.dao.delete", exception, packetItem.getPacketId());
        }
        return false;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#deletePacketItems(java.util.List)
     */
    @Override
    public void deletePacketItems(final List<PacketContent>packetItems)
    {
        for (PacketContent packetItem : packetItems)
        {
            this.deletePacketItem(packetItem);
        }
    }

    /**
     * Check for differences in the previous packet data and current packet data.
     * If the data is unchanged, then the update is skipped
     * @param currentPacket
     * @param previousPacket
     * @return true if update is required, atherwise false
     *
     */
    private boolean isUpdateRequired(Packet currentPacket, Packet previousPacket)
    {

        // No previous packet data
        if (previousPacket == null)
        {
            return true;
        }

        Map<String, PacketContent> currentPacketItemsMap = currentPacket.getPacketItemsMap();
        Map<String, PacketContent> previousPacketItemsMap = previousPacket.getPacketItemsMap();


        // Differenet packet item count...update required
        if (currentPacketItemsMap.size() != previousPacketItemsMap.size())
        {
            return true;
        }

        // Item count is the same, check each item
        for(String currentKey :currentPacketItemsMap.keySet())
        {
            // See if the item is not in the previous map
            if (!previousPacketItemsMap.containsKey(currentKey))
            {
                return true;
            }
            // The item is in the previous map, but are the display parts changed
            else
            {
                String previousDisplayParts = previousPacketItemsMap.get(currentKey).getDisplayParts() == null ? "" : previousPacketItemsMap.get(currentKey).getDisplayParts();
                String currentDisplayParts = currentPacketItemsMap.get(currentKey).getDisplayParts() == null ? "" : currentPacketItemsMap.get(currentKey).getDisplayParts();
                if (!previousDisplayParts.equals(currentDisplayParts))
                {
                    return true;
                }
            }
        }

        // Check the packet name
        return !currentPacket.getPacketName().equals(previousPacket.getPacketName());

      }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.packet.PacketDao#copyPacketAnnotations(edu.ucdavis.mw.myinfovault.domain.packet.Packet, edu.ucdavis.mw.myinfovault.domain.packet.Packet)
     */
    @Override
    public boolean copyPacketAnnotations(final Packet sourcePacket, final Packet targetPacket, final int updateUserId)
    {
        Map<String, Annotation>annotationMap = annotationDao.getAnnotations(sourcePacket.getUserId(), sourcePacket.getPacketId());

        for (String key : annotationMap.keySet())
        {
            Annotation sourceAnnotation = annotationMap.get(key);

            // Only copy packet specific annotations
            if (sourceAnnotation.getPacketId() > 0)
            {
                // Create copies of the source annotation lines if any
                List<AnnotationLine>sourceAnnotationLines = new ArrayList<AnnotationLine>();
                if (sourceAnnotation.getLabelAbove() != null) sourceAnnotationLines.add(sourceAnnotation.getLabelAbove());
                if (sourceAnnotation.getLabelBelow() != null) sourceAnnotationLines.add(sourceAnnotation.getLabelBelow());

                List<AnnotationLine>targetAnnotationLines = new ArrayList<AnnotationLine>();
                for (AnnotationLine line : sourceAnnotationLines)
                {
                    targetAnnotationLines.add(new AnnotationLine(0,
                            0,
                            line.getLabel(),
                            line.isRightJustify(),
                            line.isDisplayBefore()));
                }

                // Create a copy of the annotation record with any annotation lines
                Annotation targetAnnotation = new Annotation(0,
                        sourceAnnotation.getUserId(),
                        sourceAnnotation.getSectionBaseTable(),
                        sourceAnnotation.getRecordId(),
                        targetPacket.getPacketId(),
                        sourceAnnotation.getFootnote(),
                        sourceAnnotation.getNotation(),
                        true,//sourceAnnotation.getDisplay(),
                        targetAnnotationLines);

                if (!annotationDao.insertAnnotation(targetAnnotation, updateUserId))
                {
                    log.error("Failed to copy annotation "+sourceAnnotation.getId()+" to packet "+targetPacket.getPacketId()+" for user "+targetPacket.getUserId());
                }
            }
        }
        return false;
    }

}

