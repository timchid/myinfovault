/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AppointmentDaoImpl.java
 */


package edu.ucdavis.mw.myinfovault.dao.person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.service.person.Appointment;

/**
 * Handles MIV person appointment persistence.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class AppointmentDaoImpl extends JdbcDaoSupport implements AppointmentDao
{
    private static final String APPOINTMENT_BY_ID_SQL =
        "SELECT ID, UserID, SchoolID, DepartmentID, PrimaryAppointment" +
        " FROM Appointment" +
        " WHERE ID=?";

    private static final String APPOINTMENTS_BY_USERID =
        "SELECT ID, UserID, SchoolID, DepartmentID, PrimaryAppointment" +
        " FROM Appointment" +
        " WHERE UserID=?" +
        " ORDER BY PrimaryAppointment DESC, Sequence ASC";

    private static final String INSERT_APPOINTMENT_SQL =
        "INSERT INTO Appointment" +
        " (UserID, SchoolID, DepartmentID, PrimaryAppointment, Sequence) VALUES" +
        " (?, ?, ?, ?, ?)" +
        "";

    private static final String UPDATE_APPOINTMENT_SQL =
        "UPDATE Appointment" +
        " SET UserID=?, SchoolID=?, DepartmentID=?, PrimaryAppointment=?, Sequence=?" +
        " WHERE ID=?" +
        "";

    private static final String DELETE_APPOINTMENT_SQL =
        "DELETE FROM Appointment" +
        " WHERE ID=? AND UserID=?" +
        "";


    @Override
    public Appointment getAppointment(int recId)
    {
        Appointment a = null;
        Object[] args = { recId };

        a = this.getJdbcTemplate().queryForObject(APPOINTMENT_BY_ID_SQL, args, appointmentMapper);

        return a;
    }


    @Override
    public Collection<Appointment> getAppointments(int userId)
    {
        Object[] args = { userId };

        List<Appointment> l = this.getJdbcTemplate().query(APPOINTMENTS_BY_USERID, args, appointmentMapper);

        return l;
    }


    /**
     * All appointments passed in the collection will be saved -- either inserted or updated.
     * Appointments that are in the table but are <em>not</em> in the collection will be
     * deleted from the table.
     */
    @Override
    public void persistAppointments(Collection<Appointment> appointments)
    {
        if (appointments.isEmpty()) return; // MIV-4030 appointments.toArray caused an ArrayIndexOutOfBoundsException when the Collection was empty.

        Appointment[] type = {};
        Appointment one = appointments.toArray(type)[0];
        if (one == null) return;

        int userId = one.getUserID();
        if (userId < 1) return;

        Collection<Appointment> stored = this.getAppointments(userId);
        Map<Appointment,Appointment> originalAppointments = new HashMap<Appointment,Appointment>(stored.size());
        for (Appointment a : stored) {
            originalAppointments.put(a, a);
        }

        List<Appointment> deletes = new ArrayList<Appointment>();

        for (Appointment key : originalAppointments.keySet()) {
            Appointment a = originalAppointments.get(key);
            if (! appointments.contains(a)) { // this original appointment has been removed
                deletes.add(a);
                logger.info("Queuing to delete appointment [" + a + "] for user " + userId);
            }
        }
        for (Appointment a : appointments) {
            if (originalAppointments.keySet().contains(a) && a.getId() < 1)
            {
                a.setId(originalAppointments.get(a).getId());
            }
        }

        persistAppointments(userId, appointments, deletes);
    }

    void persistAppointments(final int userId, Collection<Appointment> appointments, Collection<Appointment> removed)
    {
        int updateTotal = 0;
        int index = 0;
        for (final Appointment a : appointments)
        {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            // set sequence then increment index
            final int sequence = index++;

            int count = this.getJdbcTemplate().update(new PreparedStatementCreator() {
                boolean isUpdate = a.getId() > 0;
                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException
                {
                    String sql = isUpdate ? UPDATE_APPOINTMENT_SQL : INSERT_APPOINTMENT_SQL;
                    int userNum = userId > 0 ? userId : a.getUserID();
                    logger.info((isUpdate ? "Updating" : "Inserting") + " appointment [" + a + "] for user " + userNum);

                    PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

                    // user, school, dept, PrimaryAppointment(, recID)
                    ps.setInt(1, userNum);
                    ps.setInt(2, a.getScope().getSchool());
                    ps.setInt(3, a.getScope().getDepartment());
                    ps.setBoolean(4, a.isPrimary());
                    ps.setInt(5, sequence);

                    if (isUpdate) ps.setInt(6, a.getId());

                    return ps;
                }
            },
            keyHolder);
            updateTotal += count;
            logger.info("Appointment ["+a+"] update count ["+count+"] key ["+keyHolder.getKey()+"]");
        }
        logger.info("Total updates: " + updateTotal);

        StringBuilder recIds = new StringBuilder("0");
        for (Appointment a : removed) {
            recIds.append(", ").append(a.getId());
        }
        String sql = "DELETE FROM Appointment WHERE UserId=? AND ID IN ("+recIds+")";
        Object[] args = { userId };
        int deleteCount = this.getJdbcTemplate().update(sql, args);
        logger.info("Total deletes: " + deleteCount);
    }


    @Override
    public void deleteAppointment(Appointment a)
    {
        Object[] args = { a.getId(), a.getUserID() };

        this.getJdbcTemplate().update(DELETE_APPOINTMENT_SQL, args);
    }


    private RowMapper<Appointment> appointmentMapper = new RowMapper<Appointment>() {
        @Override
        public Appointment mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            int recId = rs.getInt("ID");
            int userId = rs.getInt("UserID");
            int schoolId = rs.getInt("SchoolID");
            int deptId = rs.getInt("DepartmentID");
            boolean isPrimary = rs.getBoolean("PrimaryAppointment");

            Appointment a = new Appointment(recId, userId, schoolId, deptId, isPrimary);
            return a;
        }
    };
}
