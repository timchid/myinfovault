/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RoleAssignmentDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.person;

import java.io.IOException;

import edu.ucdavis.mw.myinfovault.dao.RoleAssignmentDaoSupport;

/**
 * TODO: missing javadoc
 * 
 * @author Rick Hendricks
 * @since MIV 4.0
 */
public class RoleAssignmentDaoImpl extends RoleAssignmentDaoSupport implements RoleAssignmentDao
{
    /**
     * @throws IOException
     */
    public RoleAssignmentDaoImpl() throws IOException
    {
        //role assignment dao support for the RoleAssignment table
        super("roleassignment.delete.id",
              "roleassignment.delete.userid",
              "roleassignment.insert",
              "roleassignment.select.id",
              "roleassignment.select.userid");
    }
}
