/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RafDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.raf;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.PolarResponse;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.Department;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.domain.raf.Rank;
import edu.ucdavis.mw.myinfovault.domain.raf.Status;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Handles recommended action form persistence.
 *
 * @author Craig Gilmore
 * @since 3.0
 */
public class RafDaoImpl extends DaoSupport implements RafDao
{
    private final String selectRafFormByDossierIdSql;
    private final String selectRafFormDepartmentsByRafIdSql;
    private final String selectRafFormStatuses;
    private final String selectRafStatusRanks;
    private final String insertRafFormsSql;
    private final String updateRafFormsSql;
    private final String insertRafFormDepartmentsSql;
    private final String updateRafFormDepartmentsSql;
    private final String deleteRafFormDepartmentsSql;
    private final String insertRafFormStatusesSql;
    private final String deleteRafFormStatusesSql;
    private final String insertRafStatusRanksSql;
    private final String deleteRafFormsSql;

    /**
     * Creates the RAF DAO object and initializes it with SQL
     * for handling the persistence of {@link RafBo} objects.
     */
    public RafDaoImpl()
    {
        selectRafFormByDossierIdSql = getSQL("raf.select.dossier");
        selectRafFormDepartmentsByRafIdSql = getSQL("raf.department.select");
        selectRafFormStatuses = getSQL("raf.status.select");
        selectRafStatusRanks = getSQL("raf.status.ranks.select");
        insertRafFormsSql = getSQL("raf.insert");
        updateRafFormsSql = getSQL("raf.update");
        insertRafFormDepartmentsSql = getSQL("raf.departments.insert");
        updateRafFormDepartmentsSql = getSQL("raf.departments.update");
        deleteRafFormDepartmentsSql = getSQL("raf.departments.delete");
        insertRafFormStatusesSql = getSQL("raf.status.insert");
        deleteRafFormStatusesSql = getSQL("raf.status.delete");
        insertRafStatusRanksSql = getSQL("raf.status.ranks.insert");
        deleteRafFormsSql = getSQL("raf.delete");
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.raf.RafDao#getRaf(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public RafBo getRaf(final Dossier dossier)
    {
        try
        {
            return getJdbcTemplate().queryForObject(selectRafFormByDossierIdSql,
                                                            new Object[]{dossier.getDossierId()},
                                                            new RowMapper<RafBo>() {
                @Override
                public RafBo mapRow(ResultSet rs, int rowNum) throws SQLException
                {
                    return new RafBo(rs.getInt("ID"),
                                     dossier,
                                     rs.getString("CandidateName"),
                                     rs.getInt("PrimarySchoolID"),
                                     rs.getInt("PrimaryDepartmentID"),
                                     rs.getBoolean("AppointmentType9mo"),
                                     rs.getBoolean("AppointmentType11mo"),
                                     rs.getInt("acceleration"),
                                     rs.getInt("YearsAtRank"),
                                     rs.getInt("YearsAtStep"),
                                     rs.getBoolean("RecommendationDept"),
                                     PolarResponse.valueOf(rs.getString("RecommendationCSD")),
                                     PolarResponse.valueOf(rs.getString("RecommendationJAS")),
                                     PolarResponse.valueOf(rs.getString("RecommendationAFP")),
                                     getDepartments(rs.getInt("ID"), dossier),
                                     getStatuses(rs.getInt("ID")),
                                     rs.getInt("UpdateUserID"));
                }
            });
        }
        catch (EmptyResultDataAccessException e)
        {
            // no RAF yet for this dossier
            return null;
        }
        catch (DataAccessException e)
        {
            /*
             * Either the query was bad, or more
             * than one result was returned.
             */
            throw new MivSevereApplicationError("errors.raf.dao.read", e, dossier.getDossierId());
        }
    }

    /**
     * Get the departments for the given RAF ID.
     *
     * @param formId
     * @return list of RAf departments
     * @throws DataAccessException
     */
    private List<Department> getDepartments(int formId, final Dossier dossier) throws DataAccessException
    {
        return get(selectRafFormDepartmentsByRafIdSql,
                   new RowMapper<Department>() {
            @Override
            public Department mapRow(ResultSet rs, int rowNum) throws SQLException
            {
                return new Department(dossier,
                                      rs.getInt("ID"),
                                      Department.DepartmentType.valueOf(rs.getString("DepartmentType")),
                                      rs.getString("PercentOfTime"),
                                      rs.getInt("SchoolID"),
                                      rs.getInt("DepartmentID"),
                                      rs.getString("SchoolName"),
                                      rs.getString("DepartmentName"));
            }
        }, formId);
    }

    /**
     * Get the statuses for the given RAF ID.
     *
     * @param formId
     * @return list of RAF rank statuses
     * @throws DataAccessException
     */
    private List<Status> getStatuses(int formId) throws DataAccessException
    {
        return get(selectRafFormStatuses, statusMapper, formId);
    }

    /**
     * Creates and maps 'RAF_FormStatuses' table records to {@link Status} objects.
     */
    private RowMapper<Status> statusMapper = new RowMapper<Status>()
    {
        @Override
        public Status mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new Status(rs.getInt("ID"),
                              StatusType.valueOf(rs.getString("StatusType")),
                              getRanks(rs.getInt("ID")),
                              rs.getInt("InsertUserID"),
                              rs.getDate("InsertTimestamp"));
        }
    };

    /**
     * Get the ranks for a given RAF status ID.
     *
     * @param statusId
     * @return list RAF rank and steps
     * @throws DataAccessException
     */
    private List<Rank> getRanks(int statusId) throws DataAccessException
    {
        return get(selectRafStatusRanks, rankMapper, statusId);
    }

    /**
     * Creates and maps 'RAF_StatusRanks' table records to {@link Rank} objects.
     */
    private RowMapper<Rank> rankMapper = new RowMapper<Rank>()
    {
        @Override
        public Rank mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new Rank(rs.getString("RankAndStep"),
                            rs.getString("PercentOfTime"),
                            rs.getString("TitleCode"),
                            rs.getBigDecimal("MonthlySalary"),
                            rs.getBigDecimal("AnnualSalary"),
                            rs.getInt("InsertUserID"),
                            rs.getDate("InsertTimestamp"));
        }
    };

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.raf.RafDao#persistBo(edu.ucdavis.mw.myinfovault.domain.raf.RafBo, int)
     */
    @Override
    public boolean persistRaf(final RafBo rafBo, final int realUserId)
    {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        //decide if update or insert
        final boolean rafExists = rafBo.getId() > 0;

        try
        {
            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(rafExists ? updateRafFormsSql : insertRafFormsSql,
                                                                       Statement.RETURN_GENERATED_KEYS);

                    ps.setBoolean(1,  rafBo.isAppointmentType9mo());
                    ps.setBoolean(2,  rafBo.isAppointmentType11mo());
                    ps.setInt(3, rafBo.getAcceleration().getCoefficient() * (rafBo.getAccelerationYears() + rafBo.getDecelerationYears()));
                    ps.setInt(4, rafBo.getYearsAtRank());
                    ps.setInt(5, rafBo.getYearsAtStep());
                    ps.setInt(6, realUserId);

                    if (rafExists)
                    {
                        ps.setInt(7, rafBo.getId());
                    }
                    else//new RAF
                    {
                        ps.setString(7, rafBo.getCandidateName());
                        ps.setInt(8, rafBo.getDossier().getPrimarySchoolId());
                        ps.setInt(9, rafBo.getDossier().getPrimaryDepartmentId());
                        ps.setLong(10, rafBo.getDossier().getDossierId());
                        ps.setInt(11, realUserId);
                    }

                    return ps;
                }
            }, keyHolder);

            //continue if the RAF was persisted
            if (rowsAffected > 0)
            {
                if (!rafExists)
                {
                    rafBo.setId(keyHolder.getKey().intValue());
                }

                persistDepartments(rafBo, realUserId);
                persistStatuses(rafBo, realUserId);

                return true;//RAF was updated
            }

            return false;//RAF was NOT updated
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.raf.dao.write", exception, rafBo.getId(), rafBo.getDossier().getDossierId());
        }
    }

    /**
     * Persist to the database the departments for the given RAF.
     *
     * @param raf
     * @param realUserId
     * @throws DataAccessException
     */
    private void persistDepartments(final RafBo raf, final int realUserId) throws DataAccessException
    {
        // previous set of RAF departments before this update
        List<Department> previousDepartments = getDepartments(raf.getId(), raf.getDossier());

        // insert departments are in current, but not previous
        Set<Department> insertDepartments = new HashSet<Department>(raf.getDepartments());
        insertDepartments.removeAll(previousDepartments);

        // update departments are in both current and previous
        Set<Department> updateDepartments = new HashSet<Department>(raf.getDepartments());
        updateDepartments.retainAll(previousDepartments);

        // delete departments are in previous, but not current
        Set<Department> deleteDepartments = new HashSet<Department>(previousDepartments);
        deleteDepartments.removeAll(raf.getDepartments());

        // persist the insert, update, and delete department sets
        insertDepartments(realUserId, raf.getId(), insertDepartments);
        updateDepartments(realUserId, updateDepartments);
        deleteDepartments(deleteDepartments);
    }

    /**
     * Insert RAF departments.
     *
     * @param realUserId
     * @param formId
     * @param departments
     * @throws DataAccessException
     */
    private void insertDepartments(final int realUserId,
                                   final int formId,
                                   Set<Department> departments) throws DataAccessException
    {
        for (final Department department : departments)
        {
            getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(insertRafFormDepartmentsSql);

                    ps.setString(1, department.getPercentOfTime());
                    ps.setInt(2, realUserId);
                    ps.setInt(3, formId);
                    ps.setInt(4, realUserId);
                    ps.setString(5, department.getDepartmentType().toString());
                    ps.setInt(6, department.getDepartmentId());
                    ps.setInt(7, department.getSchoolId());
                    ps.setString(8, department.getDepartmentName());
                    ps.setString(9, department.getSchoolName());

                    return ps;
                }
            });
        }
    }

    /**
     * Update RAF departments.
     *
     * @param realUserId
     * @param departments
     * @return if any rows were updated
     * @throws DataAccessException
     */
    private boolean updateDepartments(final int realUserId,
                                   Set<Department> departments) throws DataAccessException
    {
        int rowsAffected = 0;

        for (final Department department : departments)
        {
            rowsAffected += getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(updateRafFormDepartmentsSql);

                    ps.setString(1, department.getPercentOfTime());
                    ps.setInt(2, realUserId);
                    ps.setInt(3, department.getId());

                    return ps;
                }
            });
        }

        return rowsAffected > 0;
    }

    /**
     * Delete RAF departments.
     *
     * @param departments
     * @throws DataAccessException
     */
    private void deleteDepartments(Set<Department> departments) throws DataAccessException
    {
        for (final Department department : departments)
        {
            update(deleteRafFormDepartmentsSql, department.getId());
        }
    }

    /**
     * Persist to the database the statuses for the given RAF ID.
     *
     * @param raf
     * @param realUserId
     * @throws DataAccessException
     */
    private void persistStatuses(final RafBo raf, final int realUserId) throws DataAccessException
    {
        //delete current statuses in favor of new
        update(deleteRafFormStatusesSql, raf.getId());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        for (final Status status : raf.getStatuses())
        {
            int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(insertRafFormStatusesSql,
                                                                       Statement.RETURN_GENERATED_KEYS);

                    ps.setString(1, status.getStatusType().toString());
                    ps.setInt(2, status.getInsertUserId());
                    ps.setDate(3, status.getInsertTimestamp());
                    ps.setInt(4, realUserId);
                    ps.setInt(5, raf.getId());

                    return ps;
                }
            }, keyHolder);

            if (rowsAffected > 0)
            {
                status.setId(keyHolder.getKey().intValue());

                persistRanks(status, realUserId);
            }
        }
    }

    /**
     * Persist to the database the ranks for the given RAF status ID.
     *
     * @param status
     * @param realUserId
     * @throws DataAccessException
     */
    private void persistRanks(final Status status, final int realUserId) throws DataAccessException
    {
        for (final Rank rank : status.getRanks())
        {
            this.getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(insertRafStatusRanksSql);

                    ps.setString(1, rank.getRankAndStep());
                    ps.setString(2, rank.getPercentOfTime());
                    ps.setInt(3, Integer.parseInt(rank.getTitleCode()));
                    ps.setBigDecimal(4, rank.getMonthlySalary());
                    ps.setBigDecimal(5, rank.getAnnualSalary());
                    ps.setInt(6, rank.getInsertUserId());
                    ps.setDate(7, rank.getInsertTimestamp());
                    ps.setInt(8, realUserId);
                    ps.setInt(9, status.getId());

                    return ps;
                }
            });
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.raf.RafDao#deleteRaf(java.lang.Long)
     */
    @Override
    public void deleteRaf(Long dossierId)
    {
        try
        {
            getJdbcTemplate().update(deleteRafFormsSql,
                                     new Object[]{dossierId});
        }
        catch (DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.raf.dao.delete", exception, dossierId);
        }
    }
}
