package edu.ucdavis.mw.myinfovault.daoimpl.biosketch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchType;


/**
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchDaoImpl implements BiosketchDao
{
    private static final Log logger = LogFactory.getLog(BiosketchDaoImpl.class);
    private JdbcTemplate jdbcTemplate = new JdbcTemplate();


    /**
     * @param template
     */
    public void setJdbcTemplate(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#findBiosketches(int, int)
     * returns the list of biosketch's of a particular type associated with the user
     */
    @Override
    public List<Biosketch> findBiosketches(int userID, int biosketchType) throws SQLException
    {
        logger.debug("Entering findBiosketches() ... ");

        final String sql = "SELECT * FROM Biosketch WHERE UserID = ? AND BiosketchType = ? ORDER BY Name";
        Object[] params = { new Integer(userID), new Integer(biosketchType) };
        // Retrieve query results from database
        List<Biosketch> biosketchList = jdbcTemplate.query(sql, params, biosketchmapper);

        logger.debug("Exiting findBiosketchs() ... ");
        return biosketchList;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#findBiosketch(int)
     * returns the biosketch depending on the biosketcid
     */
    @Override
    public Biosketch findBiosketch(int biosketchID) throws SQLException
    {
        logger.debug("Entering findBiosketch() ... ");

        final String sql = "SELECT * FROM Biosketch WHERE ID = ?";
        Object[] params = { new Integer(biosketchID) };
        // Retrieve query results from database
        Biosketch biosketch = jdbcTemplate.queryForObject(sql, params, biosketchmapper);

        logger.debug("Exiting findBiosketch() ... ");
        return biosketch;
    }


    /**
     * maps the biosketch result set to biosketch domain object
     */
    private RowMapper<Biosketch> biosketchmapper = new RowMapper<Biosketch>() {
        @Override
        public Biosketch mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            Biosketch biosketch = new Biosketch(rs.getInt("UserID"), BiosketchType.convert(rs.getString("BiosketchType")), rs.getInt("ID"));
            biosketch.setName(rs.getString("Name"));
            biosketch.setTitle(rs.getString("Title"));
            biosketch.setStyleID(rs.getInt("StyleID"));

            logger.debug("Exiting mapRow() ... ");
            return biosketch;
        }
    };


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#findBiosketchAttributes(int)
     * returns the biosketch attribute list of a biosketch
     */
    @Override
    public List<BiosketchAttributes> findBiosketchAttributes(int biosketchID) throws SQLException
    {
        logger.debug("Entering findBiosketchAttributesByBiosketchID() ... ");

        final String sql = "SELECT * FROM BiosketchAttributes WHERE BiosketchID = ? order by Sequence";
        Object[] params = { new Integer(biosketchID) };
        // Retrieve query results from database
        List<BiosketchAttributes> biosketchAttributesList = jdbcTemplate.query(sql, params, biosketchattributesmapper);

        logger.debug("Exiting findBiosketchAttributesByBiosketchID() ... ");
        return biosketchAttributesList;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#findDefaultBiosketchAttributes(int)
     * returns the default biosketch attribute list
     */
    @Override
    public List<BiosketchAttributes> findDefaultBiosketchAttributes(int biosketchType) throws SQLException
    {
        logger.debug("Entering findDefaultBiosketchAttributes() ... ");

        final String sql = "SELECT * FROM BiosketchAttributes WHERE BiosketchID = ? ORDER BY Sequence ";
        Object[] params = { new Integer(biosketchType) };
        // Retrieve query results from database
        List<BiosketchAttributes> attributesList = jdbcTemplate.query(sql, params, biosketchattributesmapper);
        List<BiosketchAttributes> newAttributesList = new ArrayList<BiosketchAttributes>();

        for(BiosketchAttributes attributes : attributesList)
        {
            BiosketchAttributes biosketchAttributes = new BiosketchAttributes(attributes);
            newAttributesList.add(biosketchAttributes);
        }

        logger.debug("Exiting findDefaultBiosketchAttributes() ... ");
        return newAttributesList;//attributesList;
    }


    /**
     * maps the biosketchattributes result set to biosketchattributes domain object
     */
    private RowMapper<BiosketchAttributes> biosketchattributesmapper = new RowMapper<BiosketchAttributes>() {
        @Override
        public BiosketchAttributes mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            BiosketchAttributes biosketchAttributes = new BiosketchAttributes(rs.getInt("ID"));
            biosketchAttributes.setBiosketchID(rs.getInt("BiosketchID"));
            biosketchAttributes.setName(rs.getString("Name"));
            biosketchAttributes.setValue(rs.getString("Value"));
            biosketchAttributes.setSequence(rs.getInt("Sequence"));
            biosketchAttributes.setDisplay(rs.getBoolean("Display"));

            logger.debug("Exiting mapRow() ... ");
            return biosketchAttributes;
        }
    };


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#insertBiosketch(edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch)
     * inserts the biosketch and returns the auto generated biosketch id
     */
    @Override
    public int insertBiosketch(Biosketch biosketch) throws SQLException
    {
        logger.debug("Entering insertBiosketch() ... ");

        final String INSERT_BIOSKETCH_SQL =
            "INSERT INTO Biosketch" +
            " (UserID, Name, Title, BiosketchType, StyleID, InsertTimestamp, InsertUserID)" +
            " VALUES (?,?,?,?,?,?,?)";

        final int userID = biosketch.getUserID();
        final String name = biosketch.getName();
        final String title = biosketch.getTitle();
        final int collectionID = biosketch.getBiosketchType().getTypeId();
        final int styleID = biosketch.getStyleID();
        final Date insertTimeStamp = new Date();
        final int insertUserID = biosketch.getUserID();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        // inserts the biosketch into the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(INSERT_BIOSKETCH_SQL, new String[] { "ID" });
                ps.setInt(1, userID);
                ps.setString(2, name);
                ps.setString(3, title);
                ps.setInt(4, collectionID);
                ps.setInt(5, styleID);
                ps.setTimestamp(6, new Timestamp(insertTimeStamp.getTime()));
                ps.setInt(7, insertUserID);
                return ps;
            }
        }, keyHolder);

        System.out.println("Auto-generated BiosketchID is " + keyHolder.getKey().intValue());
        logger.debug("Exiting insertBiosketch() ... ");
        return (keyHolder.getKey().intValue());
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#updateBiosketch(edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch)
     * updates the biosketch record
     */
    @Override
    public void updateBiosketch(Biosketch biosketch) throws SQLException
    {
        logger.debug("Entering updateBiosketch() ... ");

        final String UPDATE_BIOSKETCH_SQL = "UPDATE Biosketch SET Name=?, Title=?, UpdateTimestamp=?, UpdateUserID=? WHERE ID=?";

        final String name = biosketch.getName();
        final String title = biosketch.getTitle();
        final Date updateTimeStamp = new Date();
        final int updateUserID = biosketch.getUserID();
        final int ID = biosketch.getId();

        // updates the biosketch record in the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(UPDATE_BIOSKETCH_SQL);
                ps.setString(1, name);
                ps.setString(2, title);
                ps.setTimestamp(3, new Timestamp(updateTimeStamp.getTime()));
                ps.setInt(4, updateUserID);
                ps.setInt(5, ID);
                return ps;
            }
        });

        logger.debug("Exiting updateBiosketch() ... ");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#deleteBiosketch(int)
     * deletes the biosketch record
     */
    @Override
    public void deleteBiosketch(int biosketchID) throws SQLException
    {
        logger.debug("Entering deleteBiosketch() ... ");

        final String DELETE_BIOSKETCH_SQL = "DELETE FROM Biosketch WHERE ID=?";

        final int ID = biosketchID;

        // deletes the biosketch record in the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(DELETE_BIOSKETCH_SQL);
                ps.setInt(1, ID);
                return ps;
            }
        });

        logger.debug("Exiting deleteBiosketch() ... ");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#insertBiosketchAttributes(edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes, int)
     * inserts the biosketchattributes
     */
    @Override
    public void insertBiosketchAttributes(BiosketchAttributes attributes, int userID) throws SQLException
    {
        logger.debug("Entering insertBiosketchAttributes() ... ");

        final String INSERT_BIOSKETCHATTRIBUTES_SQL =
            "INSERT INTO BiosketchAttributes" +
            " (BiosketchID, Name, Value, Sequence, Display, InsertTimestamp, InsertUserID)" +
            " VALUES (?,?,?,?,?,?,?)";
        final int biosketchID = attributes.getBiosketchID();
        final String name = attributes.getName();
        final String value = attributes.getValue();
        final int sequence = attributes.getSequence();
        final boolean display = attributes.isDisplay();
        final Date insertTimeStamp = new Date();
        final int insertUserID = userID;

        // inserts the biosketchattributes into the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(INSERT_BIOSKETCHATTRIBUTES_SQL);
                ps.setInt(1, biosketchID);
                ps.setString(2, name);
                ps.setString(3, value);
                ps.setInt(4, sequence);
                ps.setBoolean(5, display);
                ps.setTimestamp(6, new Timestamp(insertTimeStamp.getTime()));
                ps.setInt(7, insertUserID);
                return ps;
            }
        });

        logger.debug("Exiting insertBiosketchAttributes() ... ");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#updateBiosketchAttributes(edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes, int)
     * updates the biosketchattributes record
     */
    @Override
    public void updateBiosketchAttributes(BiosketchAttributes attributes, int userID) throws SQLException
    {
        logger.debug("Entering updateBiosketchAttributes() ... ");

        final String UPDATE_BIOSKETCHATTRIBUTES_SQL = "UPDATE BiosketchAttributes SET Name=?, Value=?, Sequence=?, Display=?,"
                + "UpdateTimestamp=?, UpdateUserID=? WHERE ID=?";
        final String name = attributes.getName();
        final String title = attributes.getValue();
        final int sequence = attributes.getSequence();
        final Boolean display = attributes.isDisplay();
        final Date updateTimeStamp = new Date();
        final int updateUserID = userID;
        final int ID = attributes.getId();

        // updates the biosketchattributes record in the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(UPDATE_BIOSKETCHATTRIBUTES_SQL);
                ps.setString(1, name);
                ps.setString(2, title);
                ps.setInt(3, sequence);
                ps.setBoolean(4, display);
                ps.setTimestamp(5, new Timestamp(updateTimeStamp.getTime()));
                ps.setInt(6, updateUserID);
                ps.setInt(7, ID);
                return ps;
            }
        });

        logger.debug("Exiting updateBiosketchAttributes() ... ");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao#deleteBiosketchAttributes(int)
     * deletes all the biosketchattributes records associated with a biosketch
     */
    @Override
    public void deleteBiosketchAttributes(int biosketchId) throws SQLException
    {
        logger.debug("Entering deleteBiosketchAttributes() ... ");

        final String DELETE_BIOSKETCHATTRIBUTES_SQL = "DELETE FROM BiosketchAttributes WHERE BiosketchID=?";

        final int biosketchID = biosketchId;

        // deletes the biosketchattributes record from the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(DELETE_BIOSKETCHATTRIBUTES_SQL);
                ps.setInt(1, biosketchID);
                return ps;
            }
        });

        logger.debug("Exiting deleteBiosketchAttributes() ... ");
    }


    /**
     *  This method returns all the names(duplicate) of same Biosketch for a single user and useful,
     *  duplicate a biosketch.
     *  @param userID - int the list of names will be retrieved for the given userID.
     *  @param originalName - String the list of biosketch names that matach the given biosketch name.
     *  @return List -- List of names that match(basically copies of) the original biosketch name.
     */
    @Override
    public List<String> getBiosketchNamesForUser(int userID, String originalName) throws SQLException
    {
        logger.debug("Entering getBiosketchNameForUser() ... ");

        String whereClause = "Copy%of%" + originalName;
//        final String SELECT_BIOSKETCH_NAMES_SQL =
//            "SELECT Name FROM Biosketch WHERE UserID=? AND Name LIKE \'" + whereClause + "\'";
        /* SDP 2008-09-05: Changed SQL statement above to the one below because concatenating
         *  "whereClause" like is done above creates an SQL Injection vulnerability.
         *  Someone could name their biosketch in some form like:  %' AND (1==1)
         *  to execute arbitrary SQL on the server.
         */
        final String SELECT_BIOSKETCH_NAMES_SQL =
            "SELECT Name FROM Biosketch WHERE UserID=? AND Name LIKE ?";
        Object[] params = { new Integer(userID), whereClause };
        // Retrieve query results from database
        List<String> nameList = jdbcTemplate.query(SELECT_BIOSKETCH_NAMES_SQL, params, biosketchnamesmapper);

        logger.debug("Exiting getBiosketchNameForUser() ... ");
        return nameList;//biosketchAttributesList;
    }


    /**
     * maps the name result set to list of names
     */
    private RowMapper<String> biosketchnamesmapper = new RowMapper<String>() {
        @Override
        public String mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");
            String name = rs.getString("Name");
            logger.debug("Exiting mapRow() ... ");
            return name;
        }
    };
}
