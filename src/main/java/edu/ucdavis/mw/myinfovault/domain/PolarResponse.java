/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PolarResponse.java
 */

package edu.ucdavis.mw.myinfovault.domain;

/**
 * Response to a polar question.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public enum PolarResponse
{
    /**
     * Affirmative response.
     */
    YES ("Yes"),

    /**
     * Negative response.
     */
    NO ("No"),

    /**
     * Not applicable.
     */
    NA ("N/A");

    private final String description;

    /**
     * @param description description of polar response
     */
    private PolarResponse(String description)
    {
        this.description = description;
    }

    /**
     * @return description of polar response
     */
    public String getDescription()
    {
        return this.description;
    }
}
