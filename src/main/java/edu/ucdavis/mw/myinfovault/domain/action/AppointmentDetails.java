/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionDetails.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.domain.PolarResponse;


/**
 * This class represents the details for an appointment relating to the
 * appointees current status, or lack thereof at the university
 * @author rhendric
 *
 */
public class AppointmentDetails implements Serializable
{
    private static final long serialVersionUID = 201311191600L;

    private int ID = -1;
    private PolarResponse currentEmployee = PolarResponse.NA;
    private PolarResponse represented = PolarResponse.NA;
    private PolarResponse noticeToUnion = PolarResponse.NA;
    private PolarResponse noticeToLaborRelations = PolarResponse.NA;


    /**
     * @param ID
     * @param academicActionID
     * @param currentEmployee
     * @param represented
     * @param noticeToUnion
     * @param noticeToLaborRelations
     */
    public AppointmentDetails (int ID,
                        PolarResponse currentEmployee,
                        PolarResponse represented,
                        PolarResponse noticeToUnion,
                        PolarResponse noticeToLaborRelations)
    {
        this(currentEmployee,represented,noticeToUnion,noticeToLaborRelations);
        this.ID = ID;
    }

    /**
     * @param academicActionID
     * @param currentEmployee
     * @param represented
     * @param noticeToUnion
     * @param noticeToLaborRelations
     */
    public AppointmentDetails (
                        PolarResponse currentEmployee,
                        PolarResponse represented,
                        PolarResponse noticeToUnion,
                        PolarResponse noticeToLaborRelations)
    {
        this.currentEmployee = currentEmployee;
        this.represented =  represented;
        this.noticeToUnion = noticeToUnion;
        this.noticeToLaborRelations = noticeToLaborRelations;
    }

    /**
     * @param academicActionID
     */
    public AppointmentDetails () {}

    /**
     * @return ID
     */
    public int getID()
    {
        return this.ID;
    }

    /**
     * @param ID
     */
    public void setID (int ID)
    {
        this.ID = ID;
    }

    /**
     * @return currentEmployee
     */
    public PolarResponse getCurrentEmployee()
    {
        return this.currentEmployee;
    }

    /**
     * @param currentEmployee
     */
    public void setCurrentEmployee(PolarResponse currentEmployee)
    {
        this.currentEmployee = currentEmployee;
    }

    /**
     * @return
     */
    public PolarResponse getRepresented()
    {
        return  this.represented;
    }

    /**
     * @param represented
     */
    public void setRepresented(PolarResponse represented)
    {
        this.represented = represented;
    }

    /**
     * @return noticeToUnion
     */
    public PolarResponse getNoticeToUnion()
    {
        return this.noticeToUnion;
    }

    /**
     * @param noticeToUnion
     */
    public void setNoticeToUnion(PolarResponse noticeToUnion)
    {
        this.noticeToUnion = noticeToUnion;
    }

    /**
     * @return noticeToLaborRelations
     */
    public PolarResponse getNoticeToLaborRelations()
    {
        return this.noticeToLaborRelations;
    }

    /**
     * @param noticeToLaborRelations
     */
    public void setNoticeToLaborRelations(PolarResponse noticeToLaborRelations)
    {
        this.noticeToLaborRelations = noticeToLaborRelations;
    }

}
