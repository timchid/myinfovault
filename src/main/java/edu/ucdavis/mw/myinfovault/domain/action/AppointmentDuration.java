/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AppointmentDuration.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

/**
 * TODO: add javadoc
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public enum AppointmentDuration
{
    NA("N/A", -1, -1,""),
    NINE_OVER_NINE("9/9", 9, 9, "1/9 annual salary rate paid monthly over quarter(s) worked during the academic year"),
    NINE_OVER_TWELVE("9/12", 9, 12, "1/12 annual salary rate paid monthly over all three quarters during the academic year"),
    /*TEN_OVER_TWELVE("10/12", 10, 12, ""),*/
    ELEVEN_OVER_TWELVE("11/12", 11, 12, "1/12 annual salary rate paid monthly over all four quarters during the fiscal year");

    private final String label;
    private final int basis;
    private final int paidOver;
    private final String description;

    /**
     * @param label
     * @param basis
     * @param paidOver
     * @param description
     */
    private AppointmentDuration(String label, int basis, int paidOver, String description)
    {
        this.label = label;
        this.basis = basis;
        this.paidOver = paidOver;
        this.description = description;
    }

    /**
     * @return appointment duration label to display
     */
    public String getLabel()
    {
        return this.label;
    }

    /**
     * @return appointment duration description
     */
    public String getDescription()
    {
        return this.description;
    }

    /**
     * @return the basis
     */
    public int getBasis()
    {
        return basis;
    }

    /**
     * @return the paidOver
     */
    public int getPaidOver()
    {
        return paidOver;
    }

}
