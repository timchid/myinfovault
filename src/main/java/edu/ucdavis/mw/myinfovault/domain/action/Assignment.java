/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Assignment.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.service.person.AppointmentScope;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.Selectable;

/**
 * Assignment to hold appointment details and a list of associated appointment titles.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class Assignment implements Selectable, Serializable
{
    private static final long serialVersionUID = 201310041714L;

    private List<Title> titles;
    private /*final*/ boolean primary;
    private AppointmentScope scope;
    private int ID = -1;
    private BigDecimal percentOfTime;
    private String schoolName;
    private String departmentName;
    private boolean remove = false;
    
    public Assignment()
    {
    	//Spring needs this for the autopopulating list.
    }

    /**
     * Create a new assignment.
     *
     * @param titles appointment titles
     * @param primary if this assignment is primary
     * @param scope school-department scope
     * @param percentOfTime percentage of time
     */
    public Assignment(List<Title> titles,
                      boolean primary,
                      Scope scope,
                      BigDecimal percentOfTime)
    {
        this.titles = titles;
        this.primary = primary;
        this.scope = new AppointmentScope(scope);
        this.percentOfTime = percentOfTime;
    }

    /**
     * Instantiate an assignment from the database.
     *
     * @param ID
     * @param titles
     * @param primary
     * @param scope
     * @param schoolName
     * @param departmentName
     * @param percentOfTime
     */
    public Assignment(int ID,
                      List<Title> titles,
                      boolean primary,
                      Scope scope,
                      String schoolName,
                      String departmentName,
                      BigDecimal percentOfTime)
    {
        this(titles, primary, scope, percentOfTime);

        this.ID = ID;
        this.schoolName = schoolName;
        this.departmentName = departmentName;
    }

    /**
     * @return appointment titles
     */
    public List<Title> getTitles()
    {
        return titles;
    }

    /**
     * @param titles appointment titles
     */
    public void setTitles(List<Title> titles)
    {
        this.titles = titles;
    }

    /**
     * @return if this assignment is primary
     */
    public boolean isPrimary()
    {
        return primary;
    }
    /**
     * @return school-department scope
     */
    public Scope getScope()
    {
        return scope;
    }

    /**
     * @return school-department scope description
     */
    public String getScopeDescription()
    {
        return scope.getFullDescription();
    }

    /**
     * @param scope school-department scope
     */
    public void setScope(Scope scope)
    {
        this.scope = new AppointmentScope(scope);
    }

    /**
     * @return percentage of time
     */
    public BigDecimal getPercentOfTime()
    {
        return percentOfTime;
    }

    /**
     * @param percentOfTime percentage of time
     */
    public void setPercentOfTime(BigDecimal percentOfTime)
    {
        this.percentOfTime = percentOfTime.divide(new BigDecimal(100));
    }

    /**
     * @param ID
     */
    public void setID(int ID)
    {
        this.ID = ID;
    }

    /**
     * @return ID
     */
    public int getID()
    {
        return this.ID;
    }

    /**
     * @return schoolName
     */
    public String getSchoolName()
    {
        return this.schoolName;
    }

    /**
     * @return departmentName
     */
    public String getDepartmentName()
    {
        return this.departmentName;

    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#isSelected()
     */
    @Override
    public boolean isSelected()
    {
        return this.remove;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#setSelected(boolean)
     */
    @Override
    public void setSelected(boolean selected)
    {
        this.remove = selected;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return this.ID;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (!(obj instanceof Assignment))
        {
            return false;
        }
        return ((Assignment)obj).ID == this.ID;
    }
}
