/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierDelegationAuthority.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import edu.ucdavis.mw.myinfovault.domain.dossier.DossierReviewType;

/**
 * This class represents the available dossier workflow delegation authorities.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public enum DossierDelegationAuthority
{
    /**
     * Dossier is redelegated.
     */
    REDELEGATED (1, "Redelegated"),

    /**
     * Dossier is non-redelegated.
     */
    NON_REDELEGATED (2, "Non-Redelegated");


    private int delegationAuthorityId = 0;
    private final String description;

    /**
     * @return id for the action type
     */
    public int getDelegationAuthorityId()
    {
        return this.delegationAuthorityId;
    }

    /**
     * Initialize delegation authority types.
     *
     * @param delegationAuthorityId id for the delegation authority type
     * @param description description for the letter type
     */
    private DossierDelegationAuthority(int delegationAuthorityId, String description)
    {
        this.delegationAuthorityId = delegationAuthorityId;
        this.description = description;
    }

    /**
     * @return description for the letter type
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Get the description for the given review type.
     *
     * @param reviewType dossier review type
     * @return description for the letter type
     */
    public String getDescription(DossierReviewType reviewType)
    {
        // append the review type description if available
        return reviewType == null
             ? description
             : description + " (" + reviewType.getDescription() + ")";
    }

    /**
     * Returns the enum corresponding to the id
     * @param id
     * @return null if not match otherwise matched DossierDelegationAuthority
     */
    public static DossierDelegationAuthority valueOf(int id)
    {
        DossierDelegationAuthority[] enumlist = DossierDelegationAuthority.values();
        for (DossierDelegationAuthority d : enumlist)
        {
            if (d.getDelegationAuthorityId() == id) { return d; }
        }

        return null;
    }

}
