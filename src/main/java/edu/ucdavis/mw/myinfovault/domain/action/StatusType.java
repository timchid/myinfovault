package edu.ucdavis.mw.myinfovault.domain.action;

/**
 * Recommended action form status type.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public enum StatusType
{
    /**
     * Present RAF status.
     */
    PRESENT ("Present Status"),

    /**
     * Proposed RAF status.
     */
    PROPOSED ("Proposed Status");

    private final String description;

    private StatusType(final String description)
    {
        this.description = description;
    }

    /**
     * @return formatted description of the RAF status
     */
    public String getDescription()
    {
        return this.description;
    }
}
