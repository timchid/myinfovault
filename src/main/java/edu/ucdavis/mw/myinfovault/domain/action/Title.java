/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Title.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import java.io.Serializable;
import java.math.BigDecimal;

import edu.ucdavis.mw.myinfovault.util.Selectable;
import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * Academic action present or proposed appointment Title.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class Title implements Selectable, Serializable
{
    private static final long serialVersionUID = 201310031425L;

    private static final BigDecimal PERCENTAGE_DIVISOR = new BigDecimal(100);

    private String description = StringUtil.EMPTY_STRING;
    private Step step = null;
    private String code = StringUtil.EMPTY_STRING;
    private BigDecimal percentOfTime = BigDecimal.ZERO;
    private boolean withoutSalary;
    private AppointmentDuration appointmentDuration = AppointmentDuration.NA;
    private SalaryPeriod salaryPeriod = null;
    private BigDecimal periodSalary = null;
    private BigDecimal annualSalary = null;
    private int assignmentID;
    private int ID = -1;
    private boolean remove = false;
    private int yearsAtRank = -1;
    private int yearsAtStep = -1;


    /**
     * Create a new title with default values.
     */
    public Title() {}

    /**
     * Create a new title with initial percent of time.
     *
     * @param percentOfTime percent of time
     */
    public Title(BigDecimal percentOfTime)
    {
        this.percentOfTime = percentOfTime;
    }

    /**
     * Create an appointment title.
     *
     * @param ID title record ID
     * @param assignmentID ID of the assignment to which this title belongs
     * @param description title and rank description
     * @param step step of title and rank
     * @param code title code
     * @param percentOfTime percent of time
     * @param withoutSalary if title is without salary
     * @param appointmentDuration duration of appointment
     * @param salaryPeriod hourly or monthly salary period
     * @param periodSalary salary for the given salary period
     * @param annualSalary annual salary
     * @param yearsAtRank years at rank
     * @param yearsAtStep years at step
     */
    public Title(int ID,
                 int assignmentID,
                 String description,
                 Step step,
                 String code,
                 BigDecimal percentOfTime,
                 boolean withoutSalary,
                 AppointmentDuration appointmentDuration,
                 SalaryPeriod salaryPeriod,
                 BigDecimal periodSalary,
                 BigDecimal annualSalary,
                 int yearsAtRank,
                 int yearsAtStep)
    {
        this.ID = ID;
        this.assignmentID = assignmentID;
        this.description = description;
        this.step = step;
        this.code = code;
        this.percentOfTime = percentOfTime;
        this.withoutSalary = withoutSalary;
        this.appointmentDuration = appointmentDuration;
        this.salaryPeriod = salaryPeriod;
        this.periodSalary = periodSalary;
        this.annualSalary = annualSalary;
        this.yearsAtRank = yearsAtRank;
        this.yearsAtStep = yearsAtStep;
    }


    /**
     * @return  title and rank description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description title and rank description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return step of title and rank
     */
    public Step getStep()
    {
        return this.step;
    }

    /**
     * @param step step of title and rank
     */
    public void setStep(Step step)
    {
        this.step = step;
    }

    /**
     * @return title code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * @param code title code
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * @return percent of time
     */
    public BigDecimal getPercentOfTime()
    {
        return percentOfTime;
    }

    /**
     * @param percentOfTime percent of time
     */
    public void setPercentOfTime(BigDecimal percentOfTime)
    {
        this.percentOfTime = percentOfTime.divide(PERCENTAGE_DIVISOR);
    }

    /**
     * @return years at rank (quarter count for {@link DossierActionType#REAPPOINTMENT} actions)
     */
    public int getYearsAtRank()
    {
        return yearsAtRank;
    }

    /**
     * @param yearsAtRank years at rank
     */
    public void setYearsAtRank(int yearsAtRank)
    {
        this.yearsAtRank = yearsAtRank;
    }

    /**
     * @return years at step
     */
    public int getYearsAtStep()
    {
        return yearsAtStep;
    }

    /**
     * @param yearsAtStep years at rank
     */
    public void setYearsAtStep(int yearsAtStep)
    {
        this.yearsAtStep = yearsAtStep;
    }

    /**
     * @return the withoutSalary
     */
    public boolean isWithoutSalary()
    {
        return withoutSalary;
    }

    /**
     * @param withoutSalary without salary flag
     */
    public void setWithoutSalary(boolean withoutSalary)
    {
        this.withoutSalary = withoutSalary;
    }

    /**
     * @return duration of appointment
     */
    public AppointmentDuration getAppointmentDuration()
    {
        return appointmentDuration;
    }

    /**
     * @param appointmentDuration duration of appointment
     */
    public void setAppointmentDuration(AppointmentDuration appointmentDuration)
    {
        this.appointmentDuration = appointmentDuration;
    }

    /**
     * @return hourly or monthly salary period
     */
    public SalaryPeriod getSalaryPeriod()
    {
        return salaryPeriod;
    }

    /**
     * @param salaryPeriod hourly or monthly salary period
     */
    public void setSalaryPeriod(SalaryPeriod salaryPeriod)
    {
        this.salaryPeriod = salaryPeriod;
    }

    /**
     * @return salary for the given salary period
     */
    public BigDecimal getPeriodSalary()
    {
        return periodSalary;
    }

    /**
     * @param periodSalary salary for the given salary period
     */
    public void setPeriodSalary(BigDecimal periodSalary)
    {
        this.periodSalary = periodSalary;
    }

    /**
     * @return annual salary or {@link BigDecimal#ZERO} if {@link #getSalaryPeriod()} is {@link SalaryPeriod#HOURLY}.
     */
    public BigDecimal getAnnualSalary()
    {
        return SalaryPeriod.HOURLY == salaryPeriod ? BigDecimal.ZERO : annualSalary;
    }

    /**
     * @param annualSalary annual salary
     */
    public void setAnnualSalary(BigDecimal annualSalary)
    {
        this.annualSalary = annualSalary;
    }

    /**
     * @return title record ID
     */
    public int getID()
    {
        return this.ID;
    }

    /**
     * @return ID of the assignment to which this title belongs
     */
    public int getAssignmentID()
    {
        return this.assignmentID;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#isSelected()
     */
    @Override
    public boolean isSelected()
    {
        return this.remove;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#setSelected(boolean)
     */
    @Override
    public void setSelected(boolean selected)
    {
        this.remove = selected;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return this.ID;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (!(obj instanceof Title))
        {
            return false;
        }
        return ((Title)obj).ID == this.ID;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder(code)
            .append(" ").append(description)
            .append(" for ").append(percentOfTime.multiply(new BigDecimal(100))).append('%')
            .append(" at ").append(periodSalary).append("/").append(salaryPeriod.getTerm())
        ;

        return sb.toString();
    }
}
