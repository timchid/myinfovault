/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Annotation.java
 */

package edu.ucdavis.mw.myinfovault.domain.annotation;


import java.util.EnumSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * An annotation record.
 *
 * @author Rick Hendricks
 * @author Pradeep Haldiya, 2012-08-17
 * @author Stephen Paulsen, 2015-08-10
 */
public class Annotation
{
    private static final Logger logger = LoggerFactory.getLogger(Annotation.class);

    private final int id;
    private final int userId;
    private final String sectionBaseTable;
    private final int recordId;
    private long packetId;
    private String footnote;
    private String notation;
    private EnumSet<Notation> notations = EnumSet.noneOf(Notation.class);
    private AnnotationLine lineAbove;
    private AnnotationLine lineBelow;


    /**
     * Create an annotation.
     *
     * @param id
     * @param userId
     * @param sectionBaseTable
     * @param recordId
     * @param packetId
     * @param footnote
     * @param notation
     * @param display
     * @param annotationLines
     */
    public Annotation(int id,
                      int userId,
                      String sectionBaseTable,
                      int recordId,
                      long packetId,
                      String footnote,
                      String notation,
                      @Deprecated
                      boolean display,
                      List<AnnotationLine> annotationLines)
    {
        this.id = id;
        this.userId = userId;
        this.sectionBaseTable = sectionBaseTable;
        this.recordId = recordId;
        this.footnote = footnote;
        this.notation = extractNotations(notation);
//        this.display = display;
        this.packetId = packetId;

        // Set up the lines
        AnnotationLine lineAbove = null;
        AnnotationLine lineBelow = null;

        if (annotationLines != null && ! annotationLines.isEmpty())
        {
            for (AnnotationLine annotationLine : annotationLines)
            {
                if (annotationLine.isDisplayBefore()) {
                    lineAbove = annotationLine;
                }
                else {
                    lineBelow = annotationLine;
                }
            }
        }

        this.lineAbove = lineAbove;
        this.lineBelow = lineBelow;
    }


    /**
     * Get the record ID of <strong>this</strong> annotation record.
     * @return the Id of this record
     */
    public int getId()
    {
        return id;
    }


    /**
     * @return the userId of the owner of the annotated record
     */
    public int getUserId()
    {
        return userId;
    }


    /**
     * @return the sectionBaseTable
     */
    public String getSectionBaseTable()
    {
        return sectionBaseTable;
    }


    /**
     * Get the record ID of the <strong>record</strong> to which this Annotation applies.
     * @return the record ID number of the record being annotated
     */
    public int getRecordId()
    {
        return recordId;
    }


    /**
     * @return the packet ID number of the Packet where this annotation appears, or zero if this is a <em>"Master"</em> annotation.
     */
    public long getPacketId()
    {
        return packetId;
    }


    /**
     * Tell whether or not there is a Label <em>Above</em> the record for this annotation.
     * @return the label above existence
     */
    public boolean hasLabelAbove()
    {
        return this.lineAbove != null;
    }


    /**
     * Tell whether or not there is a Label <em>Below</em> the record for this annotation.
     * @return the label below existence
     */
    public boolean hasLabelBelow()
    {
        return this.lineBelow != null;
    }


    /**
     * @return the label above, or <code>null</code> if there is none
     */
    public AnnotationLine getLabelAbove()
    {
        return this.lineAbove;
    }


    /**
     * @return the label below, or <code>null</code> if there is none
     */
    public AnnotationLine getLabelBelow()
    {
        return this.lineBelow;
    }


    /**
     * @return the footnote, or <code>null</code> if there is none
     */
    public String getFootnote()
    {
        return this.footnote != null ? this.footnote.trim() : null;
    }


    /**
     * @return the notation String, or an empty string if there are no notations.
     */
    public String getNotation()
    {
        if (this.notations.isEmpty()) return null;

        // if all 4 are present the order should be: *x+@
        StringBuilder sb = new StringBuilder();

        for (Notation n : Notation.values()) {
            if (this.notations.contains(n)) {
                sb.append(n.getMark());
            }
        }

        return sb.toString();
    }

    /**
     * Get the "native" packet notations.<br>
     * An Annotation object often represents the combined Master and Packet notations.
     * This method will return only those notations that are for a master annotation,
     * or only those for a packet-specific annotation, depending on whether this Annotation
     * object is "owned" by the Master or by a Packet.
     * @return the native subset of all notations
     */
    public String getNativeNotations()
    {
        if (this.notations.isEmpty()) return null;

        boolean forMaster = this.getPacketId() == 0;

        StringBuilder sb = new StringBuilder();
        for (Notation n : Notation.values()) {
            if (n.isMaster() == forMaster && this.notations.contains(n)) {
                sb.append(n.getMark());
            }
        }

        return sb.toString();
    }


    public boolean hasNotation(Notation n)
    {
        return this.notations.contains(n);
    }


    /**
     * Does this Annotation contain any Notations at all?
     * @return <code>true</code> if at least one notation is present
     */
    public boolean hasNotations()
    {
        return ! this.notations.isEmpty();
    }


    /**
     * WTF is the <em>"lookup key" ?</em>
     * @return the lookup key
     */
    public String getLookupKey()
    {
        return this.recordId + "-" + this.sectionBaseTable;
    }


/* ----- End of Getters ----- Start of Setters ----- */


    /**
     * @param labelAbove
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation setLineAbove(AnnotationLine labelAbove)
    {
        this.lineAbove = labelAbove;
        return this;
    }


    /**
     * @param labelBelow
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation setLineBelow(AnnotationLine labelBelow)
    {
        this.lineBelow = labelBelow;
        return this;
    }


    /**
     * @param footnote
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation setFootnote(String footnote)
    {
        this.footnote = footnote != null ? footnote.trim() : footnote;
        return this;
    }


    /**
     * Replace any existing notations with the notations provided.
     * <br><strong>Avoid using this call</strong> in new code and prefer using {@link Notation} enums instead.
     * @param notations a String containing one or more known notation characters.
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation setNotation(String notations)
    {
        this.notations.clear();
        this.notation = extractNotations(notations);
        return this;
    }


    /**
     * Add the given notations to any notations already present.
     * <br><strong>Avoid using this call</strong> in new code and prefer using {@link Notation} enums instead.
     * @param notations a String containing one or more known notation characters.
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation addNotations(String notations)
    {
        logger.debug("addNotation({}) — current notations are [{}] with generated string [{}]",
                     new Object[] { notations, this.notation, this.getNotation() });

        this.notation = extractNotations(notations);

        logger.debug("addNotation({}) — after adding they are [{}] with generated string [{}]",
                     new Object[] { notations, this.notation, this.getNotation() });
        return this;
    }


    /**
     * Add the given Notation to any Notations already present.
     * @param n
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation addNotation(Notation n)
    {
        this.notations.add(n);
        this.notation = this.getNotation();
        return this;
    }


    /**
     * Remove the given Notation from the Notations that are present.
     * @param n
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation removeNotation(Notation n)
    {
        this.notations.remove(n);
        this.notation = this.getNotation();
        return this;
    }


    /**
     * Remove all notations from this Annotation.
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation clearNotations()
    {
        this.notations.clear();
        this.notation = this.getNotation();
        return this;
    }


    /**
     * @param packetId
     * @return <em>this</em> Annotation for call chaining
     */
    public Annotation setPacketId(long packetId)
    {
        this.packetId = packetId;
        return this;
    }


    /**
     * Extracts the Notation elements from the notation String and adds them to the current set of notations.
     * @param notations
     * @return the resulting total notation String
     */
    private String extractNotations(String notations)
    {
        if (! StringUtils.isBlank(notations))
        {
            // Parse the notation string into individual notations
            for (char c : notations.toCharArray())
            {
                Notation n = Notation.valueOf(c);
                if (n != null) {
                    this.addNotation(n);
                }
                else {
                    logger.warn("Skipping invalid notation character '{}' in notation string '{}' for record #{}",
                                new Object[] { c, notations, this.id });
                }
            }
        }

        return this.getNotation();
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        builder
               .append("Annotation #") .append(getId())       .append(", ")
               .append("OwnerID: ")    .append(getUserId())   .append(", ")
               .append("Packet #")     .append(getPacketId()) .append(", ")
               .append("Record #")     .append(getRecordId()) .append(" in ")
               .append("Table: ")      .append(getSectionBaseTable()) .append(' ')
               .append('\n')
               .append("    (above) : ") .append(getLabelAbove()) .append('\n')
               .append("(notations) : ") .append(getNotation())   .append('\n')
               .append("    (below) : ") .append(getLabelBelow()) .append('\n')
               .append(" (footnote) : ") .append(getFootnote())   .append('\n')
               ;

        return builder.toString();
    }
}
