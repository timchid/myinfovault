/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AnnotationLine.java
 */

package edu.ucdavis.mw.myinfovault.domain.annotation;

/**
 * An annotation line record, either above or below.
 *
 * @author Pradeep Haldiya
 * @since MIV 4.8.2
 */
public class AnnotationLine
{
    private final int id;
    private final int annotationId;
    private final String label;
    private final boolean rightJustify;
    private final boolean displayBefore;

    /**
     * Create an annotation line.
     *
     * @param id
     * @param annotationId
     * @param label
     * @param rightJustify
     * @param displayBefore
     */
    public AnnotationLine(int id,
                          int annotationId,
                          String label,
                          boolean rightJustify,
                          boolean displayBefore)
    {
        this.id = id;
        this.annotationId = annotationId;
        this.label = label;
        this.rightJustify = rightJustify;
        this.displayBefore = displayBefore;
    }

    /**
     * @return the Id
     */
    public int getId()
    {
        return this.id;
    }

    /**
     * @return the annotationId
     */
    public int getAnnotationId()
    {
        return this.annotationId;
    }

    /**
     * @return the label
     */
    public String getLabel()
    {
        return this.label;
    }

    /**
     * @return <code>true</code> if <em>Right</em> Justified, <code>false</code> if Left Justified
     */
    public boolean getRightJustify()
    {
        return this.rightJustify;
    }

    /**
     * @return <code>true</code> if this line is displayed <em>Before</em> the record, <code>false</code> if displayed<em>After</em> the record.
     */
    public boolean getDisplayBefore()
    {
        return this.displayBefore;
    }

    /**
     * @return <code>true</code> if <em>Right</em> Justified, <code>false</code> if Left Justified
     */
    public boolean isRightJustify()
    {
        return this.rightJustify;
    }

    /**
     * @return <code>true</code> if this line is displayed <em>Before</em> the record, <code>false</code> if displayed After the record.
     */
    public boolean isDisplayBefore()
    {
        return this.displayBefore;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return getLabel();
    }
}
