package edu.ucdavis.mw.myinfovault.domain.biosketch;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchType;


/**
 * Domain Object representing a Biosketch
 * @author Brij Garg
 * @since MIV 2.1
 */
public class Biosketch extends DomainObject
{
    private String name;
    private String title; // Biosketch Title
    private BiosketchType biosketchType; // Biosketch Type (2 = CV, 3 = NIH) from the
                                // Collection table
    private int styleID; // it is 0 or negative for NIH and its the ID of
                            // BiosketchStyle for CV
    private BiosketchStyle biosketchStyle = null; // required for CV at Step 1
    private List<Ruleset> ruleset = null; // required at Step 2
    private List<BiosketchSection> biosketchSections = null; // required at
                                                                // Step 2
    private List<BiosketchAttributes> biosketchAttributes = null;
    private BiosketchData biosketchData = null; // required at Step 3 and 4

    public Biosketch()
    {
        // Spring needs this
    }

    public Biosketch(int userId, int biosketchType)
    {
        this(userId, BiosketchType.INVALID, 0);
    }

    public Biosketch(int userId, BiosketchType biosketchType, int id)
    {
        super(id, userId);
        this.biosketchType = biosketchType;
    }

    /**
     * @return the type of this Biosketch
     */
    public BiosketchType getBiosketchType()
    {
        return biosketchType;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return returns 0 or negative for NIH, ID of BiosketchStyle for CV
     */
    public int getStyleID()
    {
        return styleID;
    }

    /**
     * @param styleID
     *            sets the ID of BiosketchStyle for CV
     */
    public void setStyleID(int styleID)
    {
        this.styleID = styleID;
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return the biosketchData
     */
    public BiosketchData getBiosketchData()
    {
        return biosketchData;
    }

    /**
     * @param biosketchData
     *            the biosketchData to set
     */
    public void setBiosketchData(BiosketchData biosketchData)
    {
        this.biosketchData = biosketchData;
    }

    /**
     * @return the biosketchSections
     */
    public Iterable<BiosketchSection> getBiosketchSections()
    {
        return biosketchSections;
    }

    /**
     * @param biosketchSections
     *            the biosketchSections to set
     */
    public void setBiosketchSections(List<BiosketchSection> biosketchSections)
    {
        this.biosketchSections = biosketchSections;
    }

    /**
     * @return the biosketchStyle
     */
    public BiosketchStyle getBiosketchStyle()
    {
        return biosketchStyle;
    }

    /**
     * @param biosketchStyle
     *            the biosketchStyle to set
     */
    public void setBiosketchStyle(BiosketchStyle biosketchStyle)
    {
        this.biosketchStyle = biosketchStyle;
    }

    /**
     * @return the ruleset
     */
    public Iterable<Ruleset> getRuleset()
    {
        return ruleset;
    }

    /**
     * @param ruleset
     *            the ruleset to set
     */
    public void setRuleset(List<Ruleset> ruleset)
    {
        this.ruleset = ruleset;
    }

    /**
     * @return the biosketchAttributes
     */
    public Iterable<BiosketchAttributes> getBiosketchAttributes()
    {
        return biosketchAttributes;
    }

    /**
     * @param biosketchAttributes
     *            the biosketchAttributes to set
     */
    public void setBiosketchAttributes(List<BiosketchAttributes> biosketchAttributes)
    {
        this.biosketchAttributes = biosketchAttributes;
    }
}
