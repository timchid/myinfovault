package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;
import edu.ucdavis.myinfovault.designer.Availability;

/**
 * Domain Object representing Biosketch Section Limits
 * @author Brij Garg
 * @since MIV 2.1
 */
public class BiosketchSectionLimits extends DomainObject
{
    private int biosketchType;
    private int sectionID;
    private String sectionName;
    private boolean isMainSection;
    private boolean inSelectList;
    private boolean mayHideHeader;
    private int displayInBiosketch;
    private Availability availability = Availability.OPTIONAL_OFF;

    /**
     * @param id
     */
    public BiosketchSectionLimits(int id)
    {
        super(id);
    }

    /**
     * @return the availability
     */
    public Availability getAvailability()
    {
        return availability;
    }

    /**
     * @param availability the availability to set
     */
    public void setAvailability(Availability availability)
    {
        this.availability = availability;
    }

    /**
     * @return the biosketchType
     */
    public int getBiosketchType()
    {
        return biosketchType;
    }

    /**
     * @param biosketchType the biosketchType to set
     */
    public void setBioksetchType(int biosketchType)
    {
        this.biosketchType = biosketchType;
    }

    /**
     * @return the sectionID
     */
    public int getSectionID()
    {
        return sectionID;
    }

    /**
     * @param sectionID the sectionID to set
     */
    public void setSectionID(int sectionID)
    {
        this.sectionID = sectionID;
    }

    
    public String getSectionName()
    {
        return sectionName;
    }

    public void setSectionName(String sectionName)
    {
        this.sectionName = sectionName;
    }

    public boolean isMainSection()
    {
        return isMainSection;
    }

    public void setMainSection(boolean isMainSection)
    {
        this.isMainSection = isMainSection;
    }

    public boolean isInSelectList()
    {
        return inSelectList;
    }

    public void setInSelectList(boolean inSelectList)
    {
        this.inSelectList = inSelectList;
    }

    public boolean isMayHideHeader()
    {
        return mayHideHeader;
    }

    public void setMayHideHeader(boolean mayHideHeader)
    {
        this.mayHideHeader = mayHideHeader;
    }

    public int getDisplayInBiosketch()
    {
        return displayInBiosketch;
    }

    public void setDisplayInBiosketch(int displayInBiosketch)
    {
        this.displayInBiosketch = displayInBiosketch;
    }
}
