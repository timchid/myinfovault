package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;

/**
 * @author Brij Garg
 * @since MIV 2.1
 */
public class PaperSize extends DomainObject
{

    private String name;
    private String description;
    private int width;
    private int height;
    
    public PaperSize(int id)
    {
        super(id);
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return the height
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the width
     */
    public int getWidth()
    {
        return width;
    }

}
