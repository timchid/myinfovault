/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionType.java
 */

package edu.ucdavis.mw.myinfovault.domain.decision;

import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Dean/Vice Provost decision/recommendation choices.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public enum DecisionType
{
    /**
     * Dean/Provost decision is to approve.
     */
    APPROVED ("Approved",
              "Recommend Approval"),

    /**
     * Dean/Provost decision/recommendation is to deny.
     */
    DENIED ("Denied",
            "Recommend Denial"),

    /**
     * Dean/Provost decision/recommendation is to neither approve or deny.
     */
    OTHER ("Other",
           "Recommend Other"),

    /**
     *
     */
    POSITIVE_APPRAISAL("Positive Appraisal",
                       "Recommend Positive Appraisal"),

    /**
     *
     */
    POSITIVE_GUARDED_APPRAISAL("Positive/Guarded Appraisal",
                               "Recommend Positive/Guarded Appraisal"),

    /**
     *
     */
    GUARDED_APPRAISAL("Guarded Appraisal",
                      "Recommend Guarded Appraisal"),

    /**
     *
     */
    GUARDED_NEGATIVE_APPRAISAL("Guarded/Negative Appraisal",
                               "Recommend Guarded/Negative Appraisal"),

    /**
     *
     */
    NEGATIVE_APPRAISAL("Negative Appraisal",
                       "Recommend Negative Appraisal"),

    /**
     *
     */
    SATISFACTORY_REVIEW_ADVANCEMENT("Satisfactory 5-year Review with Advancement",
                                    "Recommend Satisfactory 5-year Review with Advancement"),

    /**
     *
     */
    SATISFACTORY_REVIEW("Satisfactory 5-Year Review, No Advancement",
                        "Recommend Satisfactory 5-Year Review, No Advancement"),

    /**
     *
     */
    UNSATISFACTORY_REVIEW("Unsatisfactory 5-Year Review",
                          "Recommend Unsatisfactory 5-Year Review"),

    /**
     *
     */
    REAPPOINTMENT_APPROVED("Reappointment Approved",
                           "Recommend Reappointment"),

    /**
     *
     */
    REAPPOINTMENT_DENIED("Reappointment Denied",
                         "Do not Recommend Reappointment"),

    /**
     * There is no Dean/Provost decision/recommendation.
     */
    NONE ("None", "None");

    private final String decisionDescription;
    private final String recommendationDescription;

    private DecisionType(String decisionDescription, String recommendationDescription)
    {
        this.decisionDescription = decisionDescription;
        this.recommendationDescription = recommendationDescription;
    }

    /**
     * Get the description of the decision choice based on the given signable document type.
     *
     * @param document signable document type
     * @return Readable, text representation of the decision choice
     */
    public String getDescription(MivDocument document)
    {
        /*
         * For provost tenure document types, DENIED is always
         * presented as a recommendation; a decision otherwise.
         */
        if (document == MivDocument.PROVOSTS_TENURE_RECOMMENDATION
         || document == MivDocument.PROVOSTS_TENURE_DECISION
         || document == MivDocument.PROVOSTS_TENURE_APPEAL_RECOMMENDATION
         || document == MivDocument.PROVOSTS_TENURE_APPEAL_DECISION)
        {
            return this == DENIED ? recommendationDescription : decisionDescription;
        }

        /*
         * Else, the description depends only on document
         * type being a decision or recommendation.
         */
        return document.isDecision() ? decisionDescription : recommendationDescription;
    }

    /**
     * @return Readable, text representation of the decision choice
     */
    public String getDecisionDescription()
    {
        return decisionDescription;
    }

    /**
     * @return Readable, text representation of the recommendation choice
     */
    public String getRecommendationDescription()
    {
        return recommendationDescription;
    }
}
