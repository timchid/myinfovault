/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Material.java
 */

package edu.ucdavis.mw.myinfovault.domain.disclosurecertificate;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.util.Selectable;

/**
 * Disclosure certificate material item.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
@Deprecated
public class Material implements Selectable, Serializable
{
    private static final long serialVersionUID = -1737933765408912603L;

    private int id = 0;
    private String material = "";
    private boolean selected = false;
    private int insertUserId;
    private java.sql.Timestamp insertTimestamp = null;

    /**
     * Create disclosure certificate entry with default values.
     */
    public Material() {}

    /**
     * Create disclosure certificate entry not selected by default.
     *
     * @param material material description
     */
    public Material(String material)
    {
        this.material = material;
    }

    /**
     * Create disclosure certificate entry.
     *
     * @param material material description
     * @param chk if this material has been selected
     */
    public Material(String material, boolean chk)
    {
        this(material);

        this.selected = chk;
    }

    /**
     * Create disclosure certificate entry.
     *
     * @param material material description
     * @param chk if this material has been selected
     * @param insertUserId ID of MIV user originally inserted material in the database
     * @param insertTimestamp when the material record was originally inserted
     */
    public Material(String material,
                    boolean chk,
                    int insertUserId,
                    java.sql.Timestamp insertTimestamp)
    {
        this(material, chk);

        this.insertUserId = insertUserId;
        this.insertTimestamp = insertTimestamp;
    }

    /**
     * @return material record ID
     */
    public int getId()
    {
        return id;
    }

    /**
     * @param id material record ID
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @return material description
     */
    public String getMaterial()
    {
        return material;
    }

    /**
     * @param material material description
     */
    public void setMaterial(String material)
    {
        this.material = material;
    }

    /**
     * @return ID of the user that originally created this material
     */
    public int getInsertUserId()
    {
        return insertUserId;
    }

    /**
     * @param insertUserId ID of the user that originally created this material
     */
    public void setInsertUserId(int insertUserId)
    {
        this.insertUserId = insertUserId;
    }

    /**
     * @return when the material was originally created
     */
    public java.sql.Timestamp getInsertTimestamp()
    {
        return insertTimestamp;
    }

    /**
     * @param insertTimestamp when the material was originally created
     */
    public void setInsertTimestamp(java.sql.Timestamp insertTimestamp)
    {
        this.insertTimestamp = insertTimestamp;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#isSelected()
     */
    @Override
    public boolean isSelected()
    {
        return selected;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#setSelected(boolean)
     */
    @Override
    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }
}
