/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Rank.java
 */

package edu.ucdavis.mw.myinfovault.domain.disclosurecertificate;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.NumberFormat;

import edu.ucdavis.mw.myinfovault.domain.action.Title;

/**
 * Disclosure certificate present and proposed rank and step.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class Rank implements Serializable
{
    private static final long serialVersionUID = -1737933765408912603L;

    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    private final String presentRankAndStep;
    private final String proposedRankAndStep;
    private final String presentPercentOfTime;
    private final String proposedPercentOfTime;

    /**
     * Create rank from the database.
     *
     * @param presentRankAndStep
     * @param presentPercentOfTime
     * @param proposedRankAndStep
     * @param proposedPercentOfTime
     */
    public Rank(String presentRankAndStep,
    			String presentPercentOfTime,
                String proposedRankAndStep,
                String proposedPercentOfTime)
    {
        this.presentRankAndStep = presentRankAndStep;
        this.presentPercentOfTime = presentPercentOfTime;
        this.proposedRankAndStep = proposedRankAndStep;
        this.proposedPercentOfTime = proposedPercentOfTime;
    }

    /**
     * Create rank from present and proposed RAF rank.
     *
     * @param present present rank
     * @param proposed proposed rank
     */
    public Rank(edu.ucdavis.mw.myinfovault.domain.raf.Rank present,
                edu.ucdavis.mw.myinfovault.domain.raf.Rank proposed)
    {
        this(present != null ? present.getRankAndStep() : "",
             present != null ? present.getPercentOfTime() : "",
             proposed != null ? proposed.getRankAndStep() : "",
             proposed != null ? proposed.getPercentOfTime() : "");
    }

    /**
     * Create rank from present and proposed academic action titles.
     *
     * @param present present title
     * @param proposed proposed title
     */
    public Rank(Title present, Title proposed)
    {
        this(getRankAndStep(present),
             present != null ? NumberFormat.getIntegerInstance().format(present.getPercentOfTime().multiply(ONE_HUNDRED)) : "",
             getRankAndStep(proposed),
             proposed != null ? NumberFormat.getIntegerInstance().format(proposed.getPercentOfTime().multiply(ONE_HUNDRED)) : "");
    }

    /**
     * Get the rank and step line for the given title.
     *
     * @param title academic action title
     * @return formatted rank and step line
     */
    private static String getRankAndStep(Title title)
    {
        StringBuilder rankAndStep = new StringBuilder();

        if (title != null)
        {
            rankAndStep.append(title.getDescription());

            /*
             * Include step if not null and non-zero.
             */
            if (title.getStep() != null
             && title.getStep().getValue() != null
             && title.getStep().getValue().compareTo(BigDecimal.ZERO) != 0)
            {
                rankAndStep.append(", Step ").append(title.getStep());
            }
        }

        return rankAndStep.toString();
    }

    /**
     * @return present rank and step
     */
    public String getPresentRankAndStep()
    {
        return presentRankAndStep;
    }

    /**
     * @return proposed rank and step
     */
    public String getProposedRankAndStep()
    {
        return proposedRankAndStep;
    }

    /**
	 * @return the presentPercentOfTime
	 */
	public String getPresentPercentOfTime()
	{
		return presentPercentOfTime;
	}

	/**
	 * @return the proposedPercentOfTime
	 */
	public String getProposedPercentOfTime()
	{
		return proposedPercentOfTime;
	}
}
