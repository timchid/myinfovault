/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventCategoryFilter.java
 */

package edu.ucdavis.mw.myinfovault.domain.event;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableMap;

import edu.ucdavis.mw.myinfovault.events2.EventCategory;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Filters event log entries by {@link EventCategory}.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class EventLogFilter extends SearchFilterAdapter<EventLogEntry>
{
    /**
     * Sets of roles and the document types they are respectively allowed to view.
     *
     * That is, for a given map entry, if a person has the role belonging to the entry
     * key, they are allowed to view event log items corresponding to the entry value.
     */
    private static Map<Set<MivRole>, Set<MivDocument>> DOCUMENTS =
        ImmutableMap.<Set<MivRole>, Set<MivDocument>>builder()
                    /*
                     * Candidate disclosure certificates.
                     */
                    .put(// these roles...
                         EnumSet.of(MivRole.CANDIDATE,
                                    MivRole.DEPT_STAFF,
                                    MivRole.SENATE_STAFF,
                                    MivRole.SCHOOL_STAFF,
                                    MivRole.VICE_PROVOST_STAFF,
                                    MivRole.VICE_PROVOST,
                                    MivRole.PROVOST,
                                    MivRole.CHANCELLOR,
                                    MivRole.SYS_ADMIN),
                         // ..may view these documents
                         EnumSet.of(MivDocument.DISCLOSURE_CERTIFICATE,
                                    MivDocument.JOINT_DISCLOSURE_CERTIFICATE))
                    /*
                     * Dean decision/recommendations.
                     */
                    .put(// these roles...
                         EnumSet.of(MivRole.SCHOOL_STAFF,
                                    MivRole.VICE_PROVOST_STAFF,
                                    MivRole.VICE_PROVOST,
                                    MivRole.PROVOST,
                                    MivRole.CHANCELLOR,
                                    MivRole.SYS_ADMIN),
                         // ..may view these documents
                         EnumSet.of(MivDocument.DEANS_FINAL_DECISION,
                                    MivDocument.DEANS_APPEAL_DECISION,
                                    MivDocument.DEANS_RECOMMENDATION,
                                    MivDocument.JOINT_DEANS_RECOMMENDATION,
                                    MivDocument.JOINT_DEANS_APPEAL_RECOMMENDATION))
                    /*
                     * VP decisions.
                     */
                    .put(// these roles...
                         EnumSet.of(MivRole.VICE_PROVOST_STAFF,
                                    MivRole.VICE_PROVOST,
                                    MivRole.PROVOST,
                                    MivRole.CHANCELLOR,
                                    MivRole.SYS_ADMIN),
                         // ..may view these documents
                         EnumSet.of(MivDocument.VICEPROVOSTS_FINAL_DECISION,
                                    MivDocument.VICEPROVOSTS_RECOMMENDATION,
                                    MivDocument.PROVOSTS_RECOMMENDATION,
                                    MivDocument.PROVOSTS_TENURE_RECOMMENDATION,
                                    MivDocument.PROVOSTS_TENURE_DECISION,
                                    MivDocument.PROVOSTS_FINAL_DECISION,
                                    MivDocument.CHANCELLORS_FINAL_DECISION,
                                    MivDocument.VICEPROVOSTS_APPEAL_DECISION,
                                    MivDocument.VICEPROVOSTS_APPEAL_RECOMMENDATION,
                                    MivDocument.PROVOSTS_APPEAL_RECOMMENDATION,
                                    MivDocument.PROVOSTS_TENURE_APPEAL_RECOMMENDATION,
                                    MivDocument.PROVOSTS_TENURE_APPEAL_DECISION,
                                    MivDocument.PROVOSTS_APPEAL_DECISION,
                                    MivDocument.CHANCELLORS_APPEAL_DECISION))
                    .build();

    private final EnumSet<MivDocument> allowedDocuments = EnumSet.noneOf(MivDocument.class);
    private final EventCategory category;

    /**
     * Create an event log filter for the given category and allowed
     * documents associated with the given capacity.
     *
     * @param category event category by which to filter
     * @param capacities roles by which to filter
     */
    public EventLogFilter(EventCategory category, Set<MivRole> capacities)
    {
        this.category = category;

        for (Map.Entry<Set<MivRole>, Set<MivDocument>> entry : DOCUMENTS.entrySet())
        {
            // if at least one of the given capacities exists in this entry
            if (!Collections.disjoint(entry.getKey(), capacities))
            {
                // add the documents for this entry to the allowed set
                this.allowedDocuments.addAll(entry.getValue());
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(EventLogEntry item)
    {
        /*
         * Include if:
         *  - entry has the filter category
         *  - filter category is not 'SIGNATURE'
         *      OR
         *    the signature detail evaluates to an allowed document
         */
        return item.getCategories().contains(category)
            && (
                    EventCategory.SIGNATURE != category
                 || allowedDocuments.contains(item.getDetail(EventCategory.SIGNATURE)));
    }
}
