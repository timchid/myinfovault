/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Group.java
 */

package edu.ucdavis.mw.myinfovault.domain.group;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Defines an MIV group which is essentially a set of MivPerson objects and
 * includes descriptive, role-scope access, and permission information.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public interface Group
{
    /**
     * @return The record ID of the group
     */
    public int getId();

    /**
     * @param id The record ID of the group
     */
    public void setId(int id);

    /**
     * @return The name given to the group
     */
    public String getName();

    /**
     * @param name The name to give to the group
     */
    public void setName(String name);

    /**
     * @return The description given to the group
     */
    public String getDescription();

    /**
     * @param description The description to give to the group
     */
    public void setDescription(String description);

    /**
     * @return The type assigned to the group (Review or otherwise)
     */
    public GroupType getType();

    /**
     * @param type The type assigned to the group (Review or otherwise)
     */
    public void setType(GroupType type);

    /**
     * @return The MivPerson that originally created the group
     */
    public MivPerson getCreator();

    /**
     * @param creator The MivPerson that originally created the group
     */
    public void setCreator(MivPerson creator);

    /**
     * @return The date-time the group was created
     */
    public Date getCreated();

    /**
     * @param created The date-time the group was created
     */
    public void setCreated(Date created);

    /**
     * @return The date-time the group was last updated
     */
    public Date getUpdated();

    /**
     * @param updated The date-time the group was last updated
     */
    public void setUpdated(Date updated);

    /**
     * @return Can the group be edited by someone other than the owner?
     */
    public boolean isReadOnly();

    /**
     * @param readOnly Whether or not the group may be edited by someone other than the creator.
     */
    public void setReadOnly(boolean readOnly);

    /**
     * @return Can the group be viewed by someone other than the creator?
     */
    public Boolean getHidden();

    /**
     * @param hidden Whether or not the group may be viewed by someone other than the creator.
     */
    public void setHidden(Boolean hidden);

    /**
     * @return May the group's membership be viewed by someone other than the creator?
     */
    public boolean isConfidential();

    /**
     * @param confidential Whether or not the group's membership be viewed by someone other than the creator?
     */
    public void setConfidential(boolean confidential);

    /**
     * @return Can the group be used?
     */
    public boolean isActive();

    /**
     * @param active Whether or not the group may be used.
     */
    public void setActive(boolean active);

    /**
     * @return The role-scopes of which a user must have at least one in order to access the group
     */
    public Set<AssignedRole> getAssignedRoles();

    /**
     * @param assignedRoles The role-scopes of which a user must have at least one in order to access the group
     */
    public void setAssignedRoles(Set<AssignedRole> assignedRoles);

    /**
     * @return The subset of MivPersons in the group that are not active
     */
    public Set<MivPerson> getInactive();

    /**
     * Get the set of inactive people as a map of user id to MivPerson.
     * @return The set of inactive people as a map of user id to MivPerson.
     */
    public Map<Integer, MivPerson> getInactiveMap();

    /**
     * @return The sorted set of MivPersons in the group
     */
    public SortedSet<MivPerson> getMembers();

    /**
     * Get the set of members as a map of user id to MivPerson.
     * @return The set of members as a map of user id to MivPerson.
     */
    public Map<Integer, MivPerson> getMemberMap();

    /**
     * Creates and returns a shallow copy of this group with substituted attributes if given.
     *
     * @param type The new type assigned to the group (Review or otherwise)
     * @param assignedRoles The role-scopes of which a user must have at least one in order to access the group
     * @return The new copied group object
     */
    public Group copy(GroupType type, Set<AssignedRole> assignedRoles);
}
