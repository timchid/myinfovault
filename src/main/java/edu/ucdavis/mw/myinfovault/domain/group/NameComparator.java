/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: NameComparator.java
 */

package edu.ucdavis.mw.myinfovault.domain.group;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Comparator that compares names of objects of a given type T making null compare as lesser and two
 * nulls compare equal.
 *
 * @author Craig Gilmore
 * @param <T> Object from which name will be retrieved for comparison
 * @since MIV 4.3
 */
public abstract class NameComparator<T> implements Comparator<T>, Serializable
{
    private static final long serialVersionUID = 201203011409L;

    private static final int EQUAL = 0;
    private static final int LESSER = -1;
    private static final int GREATER = 1;

    /**
     * Compare the name of <code>a</code> to that of <code>b</code>. <code>null</code>
     * compares lower. Both <code>null</code> compare equal.
     */
    @Override
    public int compare(T a, T b)
    {
        if (a == null)
        {
            return b == null ? EQUAL : LESSER;
        }

        return b == null ? GREATER : getName(a).compareTo(getName(b));
    }

    /**
     * Returns the name of the object to compare.
     *
     * @param o The object with the name to compare
     * @return The name of the given object
     */
    protected abstract String getName(T o);
}
