/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketRequestAction.java
 */

package edu.ucdavis.mw.myinfovault.domain.packet;

/**
 * The various actions that can be taken for a Packet Request
 * @author rhendric
 *
 */
public enum PacketRequestActionType {

    REQUESTED("Requested"),
    SUBMITTED("Submitted"),
    CANCELED("Canceled");

    private String description;

    private PacketRequestActionType (String description)
    {
        this.description = description;
    }

    /**
     * @return description
     */
    final String getDescription()
    {
        return description;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return this.getDescription();
    }

}
