/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Publication.java
 */

package edu.ucdavis.mw.myinfovault.domain.publications;

import edu.ucdavis.mw.myinfovault.util.Selectable;


/**
 * MIV publication record associated with an MIV user.
 *
 * @author Craig Gilmore
 * @since MIV 4.4.2
 */
public interface Publication extends Selectable
{
    /**
     * @return The publication formatted as a citation
     */
    public String getCitation();

    /**
     * @return Unique identifier of this record from the database or zero if new
     */
    public int getId();

    /**
     * @return Order in which this publication appears in the user's dossier
     */
    public int getSequence();

    /**
     * @return The ID of the user associated with the publication
     */
    public int getUserId();

    /**
     * @return Value representing the publication status
     */
    public int getStatus();

    /**
     * @return Value representing the publication type
     */
    public int getType();

    /**
     * @return Value representing the publication source
     */
    public PublicationSource getSource();

    /**
     * @return Identifier used from originating source
     */
    public String getExternalId();

    /**
     * @return Publication year
     */
    public String getYear();

    /**
     * @return Comma delimited list of authors
     */
    public String getAuthors();

    /**
     * @return Publication title
     */
    public String getTitle() ;

    /**
     * @return Publication editor
     */
    public String getEditor();

    /**
     * @return Journal in which publication was published
     */
    public String getJournal();

    /**
     * @return Volume of journal in which publication was published
     */
    public String getVolume();

    /**
     * @return Issue of volume in which publication was published
     */
    public String getIssue();

    /**
     * @return Pages cited in the issue in which the publication was published
     */
    public String getPages();

    /**
     * @return Publication publisher
     */
    public String getPublisher();

    /**
     * @return Publisher location
     */
    public String getCity();

    /**
     * @return ISBN associated with the publication or ISSN (in the case of a serial publication)
     */
    public String getIsbn();

    /**
     * @return URL to resource associated with the publication
     */
    public String getLink();
}
