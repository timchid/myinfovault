/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationSource.java
 */

package edu.ucdavis.mw.myinfovault.domain.publications;

/**
 * Defines the source from which a publication originates.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public enum PublicationSource
{
    /**
     * Manually entered publication.
     */
    USER(0),

    /**
     * Downloaded or uploaded from PubMed.
     */
    PUBMED(1),

    /**
     * Uploaded from EndNote.
     */
    ENDNOTE(2),

    /**
     * Uploaded from a CSV.
     */
    CSV(3);

    private int id;

    /**
     * @return Identifier associated with the publication source
     */
    public int getId()
    {
        return id;
    }

    private PublicationSource(int id)
    {
        this.id = id;
    }

    /**
     * Get the publication source value associated with the given ID.
     *
     * @param id Identifier associated with the publication source
     * @return The associated publication source or <code>USER</code> if not found
     */
    public static PublicationSource valueOf(int id)
    {
        for(PublicationSource publicationSource : PublicationSource.values())
        {
            if(publicationSource.getId() == id) return publicationSource;
        }

        return PublicationSource.USER;
    }
}
