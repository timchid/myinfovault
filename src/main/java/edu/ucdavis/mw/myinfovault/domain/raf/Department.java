/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Department.java
 */

package edu.ucdavis.mw.myinfovault.domain.raf;

import java.io.Serializable;
import java.text.MessageFormat;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.AppointmentScope;
import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * Recommended action form department.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class Department implements Comparable<Department>, Serializable
{
    private static final long serialVersionUID = 201210161647L;

    /**
     * Format used in {@link #toString()}
     */
    private static final String TO_STRING = "\n*** RAF Department ***" +
                                            "\n{0} ({1})" +
                                            "\n{2}";

    private int id = 0;
    private final Dossier dossier;
    private final DepartmentType departmentType;
    private String percentOfTime = StringUtil.EMPTY_STRING;
    private final AppointmentScope scope;
    private final String schoolName;
    private final String departmentName;

    /**
     * Create department from database.
     *
     * @param dossier dossier to which the decision belongs
     * @param documentId database record ID
     * @param departmentType department type (e.g. primary, joint)
     * @param percentOfTime percentage of time
     * @param schoolId ID of the school to which the department belongs
     * @param departmentId ID of the department to which the decision belongs
     * @param schoolName name of the department's school at the time of RAF is created
     * @param departmentName name of the department at the time of RAF is created
     */
    public Department(Dossier dossier,
                      int documentId,
                      DepartmentType departmentType,
                      String percentOfTime,
                      int schoolId,
                      int departmentId,
                      String schoolName,
                      String departmentName)
    {
        this.dossier = dossier;
        this.id = documentId;
        this.scope = new AppointmentScope(schoolId, departmentId);
        this.departmentType = departmentType;
        this.percentOfTime = percentOfTime;
        this.schoolName = schoolName;
        this.departmentName = departmentName;
    }

    /**
     * Create a new RAF department.
     *
     * @param dossier
     * @param isPrimary is this the primary department?
     * @param schoolId ID of the school to which the department belongs
     * @param departmentId ID of the department
     */
    Department(Dossier dossier, boolean isPrimary, int schoolId, int departmentId)
    {
        this.dossier = dossier;
        this.scope = new AppointmentScope(schoolId, departmentId);
        this.departmentType = isPrimary ? Department.DepartmentType.PRIMARY : Department.DepartmentType.JOINT;
        this.schoolName = this.scope.getSchoolDescription();
        this.departmentName = this.scope.getDepartmentDescription();
    }

    /**
     * @return department record ID
     */
    public int getId()
    {
        return this.id;
    }

    /**
     * @param id department record ID
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @return dossier to which this RAF department belongs
     */
    public Dossier getDossier()
    {
        return this.dossier;
    }

    /**
     * @return department school ID
     */
    public int getSchoolId()
    {
        return this.scope.getSchool();
    }
    /**
     * @return department ID
     */
    public int getDepartmentId()
    {
        return this.scope.getDepartment();
    }

    /**
     * @return department scope
     */
    public AppointmentScope getScope()
    {
        return this.scope;
    }

    /**
     * @return the department type, either primary, joint, or split
     */
    public DepartmentType getDepartmentType()
    {
        return departmentType;
    }

    /**
     * @return percentage of time allotted to this department
     */
    public String getPercentOfTime()
    {
        return percentOfTime;
    }

    /**
     * @param percentOfTime percentage of time allotted to this department
     */
    public void setPercentOfTime(final String percentOfTime)
    {
        this.percentOfTime = percentOfTime;
    }

    /**
     * @return the formatted name of the department's school
     */
    public String getSchoolName()
    {
        return schoolName;
    }

    /**
     * @return the formatted name of this department
     */
    public String getDepartmentName()
    {
        return departmentName;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Department department)
    {
        return departmentType.ordinal() - department.getDepartmentType().ordinal();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that)
    {
        // quick optimization
        if (this == that) return true;
        if (!(that instanceof Department)) return false;
        Department department = (Department) that;

        // dossier, school, and department IDs must match
        return dossier.getDossierId() == department.getDossier().getDossierId()
            && getSchoolId() == department.getSchoolId()
            && getDepartmentId() == department.getDepartmentId();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = 1;

        hash = hash * 31 + (int)(dossier.getDossierId() ^ (dossier.getDossierId() >>> 32));
        hash = hash * 31 + getSchoolId();
        hash = hash * 31 + getDepartmentId();

        return hash;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.raf.Decision#toString()
     */
    @Override
    public String toString()
    {
        return MessageFormat.format(TO_STRING, scope.getFullDescription(),
                                    departmentType, super.toString());
    }

    /**
     * Possible RAF department types.
     *
     * @author Craig Gilmore
     * @since MIV 3.0
     */
    public enum DepartmentType
    {
        /*
         * IMPORTANT: keep this order (used in a comparator for sorting)
         */

        /**
         * Dossier candidate's primary department.
         */
        PRIMARY,

        /**
         * Dossier candidate's joint departments.
         */
        JOINT;
    }
}
