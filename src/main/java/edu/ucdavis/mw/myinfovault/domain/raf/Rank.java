/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Rank.java
 */

package edu.ucdavis.mw.myinfovault.domain.raf;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;

/**
 * Recommended action form status rank and steps.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class Rank implements Serializable
{
    private static final long serialVersionUID = 200904101607L;

    private String rankAndStep = "";
    private String percentOfTime = "";
    private String titleCode = "";
    private BigDecimal monthlySalary = BigDecimal.ZERO;
    private BigDecimal annualSalary = BigDecimal.ZERO;
    private int insertUserId;
    private java.sql.Date insertTimestamp = null;

    /**
     * Create RAF status rank.
     *
     * @param rankAndStep
     * @param percentOfTime
     * @param titleCode
     * @param monthlySalary
     * @param annualSalary
     * @param insertUserId
     * @param insertTimestamp
     */
    public Rank(String rankAndStep,
                String percentOfTime,
                String titleCode,
                BigDecimal monthlySalary,
                BigDecimal annualSalary,
                int insertUserId,
                java.sql.Date insertTimestamp)
    {
        this.rankAndStep = rankAndStep;
        this.percentOfTime = percentOfTime;
        this.titleCode = titleCode;
        this.monthlySalary = monthlySalary;
        this.annualSalary = annualSalary;
        this.insertUserId = insertUserId;
        this.insertTimestamp = insertTimestamp;
    }

    /**
     * Create new blank status rank.
     */
    public Rank(){}

    /**
     * @return if this rank contains no data.
     */
    public boolean isBlank()
    {
        return StringUtils.isBlank(rankAndStep)
            && StringUtils.isBlank(percentOfTime)
            && StringUtils.isBlank(titleCode)
            && monthlySalary.compareTo(BigDecimal.ZERO) == 0
            && annualSalary.compareTo(BigDecimal.ZERO) == 0;
    }

    /**
     * @return status rank and step description
     */
    public String getRankAndStep()
    {
        return rankAndStep;
    }

    /**
     * @param rankAndStep status rank and step description
     */
    public void setRankAndStep(final String rankAndStep)
    {
        this.rankAndStep = rankAndStep;
    }

    /**
     * @return rank percent of time
     */
    public String getPercentOfTime()
    {
        return percentOfTime;
    }

    /**
     * @param percentOfTime rank percent of time
     */
    public void setPercentOfTime(String percentOfTime)
    {
        this.percentOfTime = percentOfTime;
    }

    /**
     * @return rank numeric title code
     */
    public String getTitleCode()
    {
        return titleCode;
    }

    /**
     * @param titleCode rank numeric title code
     */
    public void setTitleCode(String titleCode)
    {
        this.titleCode = titleCode;
    }

    /**
     * @return monthly salary money value
     */
    public BigDecimal getMonthlySalary()
    {
        return monthlySalary;
    }

    /**
     * @param monthlySalary monthly salary money value
     */
    public void setMonthlySalary(BigDecimal monthlySalary)
    {
        this.monthlySalary = monthlySalary;
    }

    /**
     * @return annual salary money value
     */
    public BigDecimal getAnnualSalary()
    {
        return annualSalary;
    }

    /**
     * @param annualSalary annual salary money value
     */
    public void setAnnualSalary(BigDecimal annualSalary)
    {
        this.annualSalary = annualSalary;
    }

    /**
     * @return ID of real, logged-in user originally created this rank
     */
    public int getInsertUserId()
    {
        return insertUserId;
    }

    /**
     * @return when this rank was originally created
     */
    public java.sql.Date getInsertTimestamp()
    {
        return insertTimestamp;
    }
}
