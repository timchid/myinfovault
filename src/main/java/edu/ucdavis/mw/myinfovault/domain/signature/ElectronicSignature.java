/**
 * Copyright
 * Copyright (c) University of California, Davis, 2009
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ElectronicSignature.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import java.util.Date;
/**<p>
 * An ElectronicSignature is used to create a verifiable signature for anything represented as an array of bytes.
 * The ElectronicSignature object becomes immutable once signing has been applied.
 *</p>
 */
public interface ElectronicSignature
{

    /**
     * ...
     * <p>The document String is decoded as UTF-8 bytes when signing. Use {@link #sign(byte[], String)} to provide
     * a different encoding or another representation of the document.</p>
     * @param document The document, representation of the document, document identifier, or data to be signed.
     * @param signerId person actually signing the document, which could be different from the requested signer.
     */
    public void sign(String document, String signerId);

    /**
     *
     * @param document The document, representation of the document, document identifier, or data to be signed.
     * @param signerId person actually signing the document, which could be different from the requested signer.
     */
    public void sign(byte[] document, String signerId);

    /* *
     * We could extend this class further so it could sign a file
     * @param document
     * @param signerId
     * /
    public void sign(java.io.File document, long signerId)
    {
        //...
    }*/


    /**
     * Validity based on byte-level comparison of the given document's hash to that previously computed
     * for this signature.
     *
     * @param document The document, or representation of the document, to verify against this signature.
     * @return <code>true<code> if the document and signature match, <code>false</code> otherwise.
     */
    public boolean isValid(String document);

    /**
     * Validity based on byte-level comparison of the given document's hash to that previously computed
     * for this signature.
     *
     * @param document The document, or representation of the document, to verify against this signature.
     * @return <code>true<code> if the document and signature match, <code>false</code> otherwise.
     */
    public boolean isValid(byte[] document);

    /**
     * @return the digest type
     */
    public String getDigestType();

    /**
     * @return the requested signer
     */
    public String getRequestedSigner();

    /**
     * @return the signer
     */
    public String getSigner();

    /**
     * @return the Signature requested date/time;
     */
    public Date getCreatedDate();

    /**
     * @return the signedDate
     */
    public Date getSigningDate();

    /**
     * Provide a description of what this signature appies to.
     * @param description a description of the purpose of this signature
     */
    public void setDescription(CharSequence description);

    /**
     * Get the description of what this signature applies to.
     * @return the description or <code>null</code>
     */
    public String getDescription();

    /**
     * Signals that an operation was attempted on a signature in an invalid state.
     *
     * @author Stephen Paulsen
     * @since MIV 3.0
     */
    public class SigningException extends IllegalStateException
    {
        /*
         * Possible enhancements:
         * Class hierarchy of signing exceptions -- Document already signed, no usable algorithm, different document
         */

        private static final long serialVersionUID = 200902190927L;

        /**
         * @param msg
         */
        public SigningException(String msg)
        {
            super(msg);
        }
    }

}
