/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Signable.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import java.io.File;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.signature.SignatureStatus;
import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Defines a signable document.
 *
 * @author Craig Gilmore
 * @since 4.6.2
 */
public interface Signable
{
    /**
     * @return type of signable document
     */
    public MivDocument getDocumentType();
    
    /**
     * @return the signable documents signature status.
     */
    public SignatureStatus getSignatureStatus();
    
    /**
     * @return the dossier PDF file representing this signable document
     */
    public File getDossierFile();

    /**
     * @return document record ID
     */
    public int getDocumentId();

    /**
     * @param documentId document record ID
     */
    public void setDocumentId(int documentId);

    /**
     * @return dossier associated with this document
     */
    public Dossier getDossier();

    /**
     * @return school ID associated with this document
     */
    public int getSchoolId();

    /**
     * @return department ID associated with this document
     */
    public int getDepartmentId();
}
