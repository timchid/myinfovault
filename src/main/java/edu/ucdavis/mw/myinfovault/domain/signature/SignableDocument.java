/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignableDocument.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import java.io.File;
import java.io.Serializable;
import java.text.MessageFormat;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.AppointmentScope;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.signature.SignatureStatus;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.document.DocumentFormat;

/**
 * Defines a signable document.
 *
 * @author Craig Gilmore
 * @since 4.6.2
 */
public abstract class SignableDocument implements Signable, Serializable
{
    private static final long serialVersionUID = 201210311115L;

    /**
     * Format used in {@link #toString()}
     */
    private static final String TO_STRING = "\n\t*** Signable Document ***" +
                                            "\n\tID:\t\t{0,number,#}" +
                                            "\n\tType:\t\t{1}" +
                                            "\n\tDossier:\t{2,number,#}" +
                                            "\n\tDepartment:\t{3}";

    private Dossier dossier;
    private int documentId;
    private MivDocument documentType;
    private AppointmentScope scope;

    /**
     * Create a signable document.
     *
     * @param dossier dossier to which the document belongs
     * @param documentId database record ID
     * @param documentType signable document type
     * @param schoolId ID of the school to which the department belongs
     * @param departmentId ID of the department to which the document belongs
     */
    public SignableDocument(Dossier dossier,
                            int documentId,
                            MivDocument documentType,
                            int schoolId,
                            int departmentId)
    {
        this.dossier = dossier;
        this.documentId = documentId;
        this.documentType = documentType;
        this.scope = new AppointmentScope(schoolId, departmentId);
    }

    /**
     * No-arg constructor for sub-class serialization
     */
    protected SignableDocument(){}

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.Signable#getSignatureStatus()
     */
    @Override
    public SignatureStatus getSignatureStatus()
    {
        return MivServiceLocator.getSigningService().getSignatureStatus(this);
    }

    /**
     * @return the filename format pattern for the dossier PDF
     */
    protected abstract String getFilenamePattern();

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.Signable#getDossierFile()
     */
    @Override
    public File getDossierFile()
    {
        return new File(getDossier().getDossierDirectoryByDocumentFormat(DocumentFormat.PDF),
                        MessageFormat.format(getFilenamePattern(),
                                             getDocumentType().getDossierFileName(),
                                             scope.getSchool(),
                                             scope.getDepartment()));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.Signable#getDossier()
     */
    @Override
    public Dossier getDossier()
    {
        return dossier;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.Signable#getDocumentType()
     */
    @Override
    public MivDocument getDocumentType()
    {
        return documentType;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.Signable#getDocumentId()
     */
    @Override
    public int getDocumentId()
    {
        return documentId;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.Signable#setDocumentId(int)
     */
    @Override
    public void setDocumentId(int documentId)
    {
        this.documentId = documentId;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.Signable#getSchoolId()
     */
    @Override
    public int getSchoolId()
    {
        return scope.getSchool();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.Signable#getDepartmentId()
     */
    @Override
    public int getDepartmentId()
    {
        return scope.getDepartment();
    }

    /**
     * @return the formatted name of the department's school
     */
    public String getSchoolName()
    {
        return scope.getSchoolDescription();
    }

    /**
     * @return the formatted name of this department
     */
    public String getDepartmentName()
    {
        return scope.getDepartmentDescription();
    }

    /**
     * @return scope of this signature document
     */
    public AppointmentScope getScope()
    {
        return this.scope;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return MessageFormat.format(TO_STRING, documentId,
                                               documentType,
                                               dossier.getDossierId(),
                                               scope);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object ob)
    {
        /*
         * Equal if same object or document ID and type are the same.
         */
        return this == ob
            || ob instanceof SignableDocument
            && this.documentId == ((SignableDocument) ob).documentId
            && this.getDocumentType() == ((SignableDocument) ob).getDocumentType();
    }

    /**
     * @return if the department for this signable document is the primary
     */
    public boolean isPrimary()
    {
        return scope.matches(new Scope(dossier.getPrimarySchoolId(),
                                       dossier.getPrimaryDepartmentId()));
    }
}
