/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: TypeFilter.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import java.util.Arrays;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Filters by the given document types.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
abstract class TypeFilter<T> extends SearchFilterAdapter<T>
{
    private final MivDocument[] types;

    /**
     * @param types document types with which each item of the filtered collection may be associated
     */
    TypeFilter(MivDocument...types)
    {
        Arrays.sort(types);

        this.types = types;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(T item)
    {
        return Arrays.binarySearch(types, this.getDocumentType(item)) > -1;
    }
    
    /**
     * Get the document type associated with the given item.
     * 
     * @param item object on which to filter
     * @return document type associated with the item
     */
    abstract MivDocument getDocumentType(T item);
}
