/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CourseEvaluation.java
 */

package edu.ucdavis.mw.myinfovault.domain.vetmed;

import java.io.Serializable;
import java.net.URL;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.Selectable;

/**
 * VetMed course evaluation record.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class CourseEvaluation implements Serializable, Comparable<CourseEvaluation>, Selectable
{
    private static final long serialVersionUID = 201308121354L;

    private boolean selected = false;

    private final MivPerson user;
    private final CourseEvaluationType type;
    private final CourseEvaluationTerm term;
    private final int year;
    private final String course;
    private final String description;
    private final String responseTotal;
    private final String instructorScore;
    private final String courseScore;
    private final URL link;
    private final boolean display = true;

    /**
     * Create a course evaluation record.
     *
     * @param user
     * @param type
     * @param term
     * @param year
     * @param course
     * @param description
     * @param responseTotal
     * @param instructorScore
     * @param courseScore
     * @param link
     * @throws IllegalArgumentException if user is <code>null</code>
     */
    public CourseEvaluation(MivPerson user,
                            CourseEvaluationType type,
                            CourseEvaluationTerm term,
                            int year,
                            String course,
                            String description,
                            String responseTotal,
                            String instructorScore,
                            String courseScore,
                            URL link) throws IllegalArgumentException
    {
        if (user == null) throw new IllegalArgumentException("Course evaluation must be associated with an MIV person");

        this.user = user;
        this.type = type;
        this.term = term;
        this.year = year;
        this.course = course;
        this.description = description;
        this.responseTotal = responseTotal;
        this.instructorScore = instructorScore;
        this.courseScore = courseScore;
        this.link = link;
    }

    /**
     * @return
     */
    public MivPerson getUser() {
        return user;
    }

    /**
     * @return
     */
    public CourseEvaluationType getType() {
        return type;
    }

    /**
     * @return
     */
    public CourseEvaluationTerm getTerm() {
        return term;
    }

    /**
     * @return
     */
    public int getYear() {
        return year;
    }

    /**
     * @return
     */
    public String getCourse() {
        return course;
    }

    /**
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return
     */
    public String getResponseTotal() {
        return responseTotal;
    }

    /**
     * @return
     */
    public String getInstructorScore() {
        return instructorScore;
    }

    /**
     * @return
     */
    public String getCourseScore() {
        return courseScore;
    }

    /**
     * @return
     */
    public URL getLink() {
        return link;
    }

    /**
     * @return
     */
    public boolean isDisplay() {
        return display;
    }


    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(CourseEvaluation o) {
        int courseCompare = this.course.compareTo(course);
        if (courseCompare != 0) return courseCompare;

        int userCompare = this.user.getUserId() - o.user.getUserId();
        if (userCompare != 0) return userCompare;

        int termCompare = this.term.compareTo(o.term);
        if (termCompare != 0) return termCompare;

        int yearCompare = this.year - o.year;
        if (yearCompare != 0) return yearCompare;

        return this.type.compareTo(o.type);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#isSelected()
     */
    @Override
    public boolean isSelected() {
        return selected;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.Selectable#setSelected(boolean)
     */
    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
