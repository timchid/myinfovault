/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CourseEvaluationTerm.java
 */

package edu.ucdavis.mw.myinfovault.domain.vetmed;

/**
 * Course evaluation record academic term.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public enum CourseEvaluationTerm
{
    WINTER_QUARTER("01", "Winter Quarter"),
    SPRING_SEMESTER("02", "Spring Semester"),
    SPRING_QUARTER("03", "Spring Quarter"),
    SUMMER_SEMESTER("04", "Summer Semester"),
    SUMMER_SESSION_ONE("05", "Summer Session I"),
    SUMMER_SESSION_TWO("07", "Summer Session II"),
    SUMMER_QUARTER("08", "Summer Quarter"),
    FALL_SEMESTER("09", "Fall Semester"),
    FALL_QUARTER("10", "Fall Quarter"),
    UNKNOWN(null, "Unknown");

    private final String code;
    private final String description;

    /**
     * Instantiate a course evaluation term.
     *
     * @param code term code
     * @param description term description
     */
    private CourseEvaluationTerm(String code, String description)
    {
        this.code = code;
        this.description = description;
    }

    /**
     * @return term description, e.g. "Summer Session II"
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Get the term associated with the given code.
     *
     * @param code term code
     * @return term for the given code or {@link #UNKNOWN} if none exists
     */
    public static CourseEvaluationTerm getTerm(String code)
    {
        for (CourseEvaluationTerm term : values())
        {
            if (term.code.equals(code)) return term;
        }

        return UNKNOWN;
    }
}
