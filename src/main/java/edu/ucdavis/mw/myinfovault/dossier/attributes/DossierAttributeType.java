/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAttributeType.java
 */
package edu.ucdavis.mw.myinfovault.dossier.attributes;


/**
 * TODO: add javadoc
 *
 * @author Rick Hendricks
 * @since MIV ?
 */
public enum DossierAttributeType
{
    /**
     * Uploaded document.
     */
    DOCUMENTUPLOAD,

    /**
     * Generated document.
     */
    DOCUMENT,

    /**
     * Action: Open, Close, Release, Hold...
     */
    ACTION,

    /**
     * TODO: add java doc
     */
    HEADER,

    /**
     * Unknown document type.
     */
    UNKNOWN;
}
