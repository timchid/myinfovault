/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DisclosureRequestEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

/**
 * Don't start using this Event &mdash; it was created for experimental purposes only
 * without any thought given to where it should really fit in the Event hierarchy.
 *
 * @author stephenp
 */
public class DisclosureRequestEvent extends MivEvent implements DCEvent
{
    private final String candidateId;
    private final String emailSendAddress;

    public DisclosureRequestEvent(String actor, String candidate, String emailTo)
    {
        super(actor);
        this.candidateId = candidate;
        this.emailSendAddress = emailTo;
    }

    @Override
    public String toString()
    {
        return "User " + this.getActorId() + " sent email to " + this.emailSendAddress +
               " for user " + this.candidateId + " to sign their disclosure certificate";
    }
}
