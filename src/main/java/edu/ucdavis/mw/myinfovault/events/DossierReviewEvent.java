/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierReviewEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

/**
 * TODO: Add Javadoc
 * @author Stephen Paulsen
 *
 */
public abstract class DossierReviewEvent extends DossierEvent
{
    /**
     * @param dossierId
     * @param actorId
     */
    public DossierReviewEvent(long dossierId, String actorId)
    {
        super(dossierId, actorId);
        // TODO Auto-generated constructor stub
    }

}
