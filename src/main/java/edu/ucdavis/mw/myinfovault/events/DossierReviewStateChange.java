/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierReviewStateChange.java
 */

package edu.ucdavis.mw.myinfovault.events;

import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;

/**
 * TODO: Add Javadoc
 * @author Stephen Paulsen
 */
public abstract class DossierReviewStateChange extends DossierReviewEvent
{
    private final DossierAttributeStatus fromState;
    private final DossierAttributeStatus toState;


    /**
     *
     * @param dossierId
     * @param actorId
     * @param fromState
     * @param toState
     */
    protected DossierReviewStateChange(long dossierId, String actorId,
                                       final DossierAttributeStatus fromState,
                                       final DossierAttributeStatus toState)
    {
        super(dossierId, actorId);
        this.fromState = fromState;
        this.toState = toState;
    }

    /**
     * @return the fromState
     */
    public DossierAttributeStatus getPreviousState()
    {
        return fromState;
    }

    /**
     * @return the toState
     */
    public DossierAttributeStatus getNewState()
    {
        return toState;
    }
}
