/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierRouteEvent.java
 */


package edu.ucdavis.mw.myinfovault.events;

/**
 * @author Stephen Paulsen
 *
 */
public class DossierRouteEvent extends DossierEvent
{
    private final String fromLocation;
    private final String toLocation;

    public DossierRouteEvent(final long dossierId, String actorId, final String fromLocation, final String toLocation)
    {
        super(dossierId, actorId);
        this.fromLocation = fromLocation;
        this.toLocation = toLocation;
    }


    /**
     * Get the location where the dossier was before routing.
     * This is the location where the dossier was before being routed,
     * and the location where it will stay if routing fails.
     *
     * @return the previous location of the dossier
     */
    public String getPreviousLocation()
    {
        return this.getFromLocation();
    }


    /**
     * Get the destination location of the dossier.
     * @return the new location of the dossier
     */
    public String getNewLocation()
    {
        return this.toLocation;
    }


    /** An alias for {@link #getPreviousLocation()} */
    public String getFromLocation()
    {
        return this.fromLocation;
    }


    /** An alias for {@link #getNewLocation()} */
    public String getToLocation()
    {
        return this.getNewLocation();
    }
}
