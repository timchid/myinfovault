/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SigEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

/**
 * Experimental use of Interfaces for use in the EventDispatcher and event listeners.
 * Don't depend on using this event interface &mdash; it may be removed at any time.
 *
 * @author stephenp
 */
public interface SigEvent
{
    // Marker Interface
}
