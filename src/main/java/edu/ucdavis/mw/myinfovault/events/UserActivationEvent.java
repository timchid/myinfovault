/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserActivationEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

/**
 * TODO: Add Javadoc
 * @author stephenp
 *
 */
public class UserActivationEvent extends UserEvent
{
    boolean activating;
    /**
     * @param actorId
     * @param affectedUserId
     * @param activating
     */
    public UserActivationEvent(String actorId, String affectedUserId, boolean activating)
    {
        super(actorId, affectedUserId);
        this.activating = activating;
    }

    public boolean isActivating()
    {
        return this.activating;
    }

    public boolean isDeactivating()
    {
        return ! isActivating();
    }
}
