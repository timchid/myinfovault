/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * An event to indicate an academic action has changed.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class AcademicActionEvent extends DossierEventStub implements RafEvent
{
    private static final long serialVersionUID = 201308231526L;

    private final AcademicAction former;

    /**
     * Create event indicating that an academic action has been created.
     *
     * @param realPerson real, logged-in person who performed change
     * @param dossier dossier for which the action was created
     */
    public AcademicActionEvent(MivPerson realPerson, Dossier dossier)
    {
        this(realPerson, dossier, null);
    }

    /**
     * Create event indicating that an academic action has changed.
     *
     * @param realPerson real, logged-in person who performed change
     * @param dossier dossier for which the action was changed
     * @param former former academic action or <code>null</code> if a new action was created
     */
    public AcademicActionEvent(MivPerson realPerson,
                               Dossier dossier,
                               AcademicAction former)
    {
        super(realPerson, dossier);

        this.former = former;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Action";
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RafEvent#getFormerAction()
     */
    @Override
    public AcademicAction getFormerAction()
    {
        return former;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RafEvent#getCurrentAction()
     */
    @Override
    public AcademicAction getCurrentAction()
    {
        return getDossier().getAction();
    }
}
