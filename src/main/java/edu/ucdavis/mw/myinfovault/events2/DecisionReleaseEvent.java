/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionReleaseEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Describes a decision hold/release event.
 *
 * @author Rick Hendricks
 * @since MIV 4.7.2
 */
public class DecisionReleaseEvent extends DossierEventStub implements DossierEvent, ReleaseEvent
{
    private static final long serialVersionUID = 201308211220L;

    private final Scope scope;
    private final String attributeName;

    /**
     * Create a decision release event.
     *
     * @param realPerson real, logged-in person who performed triggered the event
     * @param dossier dossier on which the event occurred
     * @param scope scope in which the event occurred
     * @param attributeName release attribute name
     */
    public DecisionReleaseEvent(MivPerson realPerson,
                                Dossier dossier,
                                Scope scope,
                                String attributeName)
    {
        super(realPerson, dossier);

        this.scope = scope;
        this.attributeName = attributeName;
    }

    /**
     * Create a decision release event in response to a parent event.
     *
     * @param parent parent event
     * @param dossier dossier on which the event occurred
     * @param scope scope in which the event occurred
     * @param attributeName release attribute name
     */
    public DecisionReleaseEvent(MivEvent parent,
                                Dossier dossier,
                                Scope scope,
                                String attributeName)
    {
        super(parent, parent.getRealPerson(), dossier);

        this.scope = scope;
        this.attributeName = attributeName;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Decision Released";
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.ReleaseEvent#getLocation()
     */
    @Override
    public DossierLocation getLocation()
    {
        return this.getDossier().getDossierLocation();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.ReleaseEvent#getScope()
     */
    @Override
    public Scope getScope()
    {
        return this.scope;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.ReleaseEvent#getAttributeName()
     */
    @Override
    public String getAttributeName()
    {
        return this.attributeName;
    }
}
