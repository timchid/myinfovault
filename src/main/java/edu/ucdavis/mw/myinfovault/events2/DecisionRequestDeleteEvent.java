/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionRequestDeleteEvent.java
 */
package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;

/**
 * A signature request on a decision was deleted.
 * Inherits most of its methods from the parent {@link DecisionSignatureEvent}.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class DecisionRequestDeleteEvent extends DecisionSignatureEvent implements DossierEvent, DecisionEvent, RequestEvent, SignatureEvent
{
    private static final long serialVersionUID = 201308191710L;

    /**
     * TODO: add Javadoc
     *
     * @param parent event that caused this event
     * @param signature
     */
    public DecisionRequestDeleteEvent(MivEvent parent, MivElectronicSignature signature)
    {
        super(parent, parent.getRealPerson(), signature);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.DecisionSignatureEvent#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Decision Request Delete";
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RequestEvent#getRequestedUserId()
     */
    @Override
    public int getRequestedUserId()
    {
        return Integer.parseInt(getSignature().getRequestedSigner());
    }
}
