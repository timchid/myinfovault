/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierArchiveEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * TODO: Add Javadoc
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class DossierArchiveEvent extends DossierRouteEvent implements DossierEvent, RouteEvent
{
    private static final long serialVersionUID = 201307311025L;

    /**
     * TODO: Add Javadoc
     *
     * @param realPerson
     * @param dossier
     * @param from
     */
    public DossierArchiveEvent(final MivPerson realPerson, final Dossier dossier, final DossierLocation from)
    {
        super(realPerson, dossier, from, DossierLocation.ARCHIVE);
    }
}
