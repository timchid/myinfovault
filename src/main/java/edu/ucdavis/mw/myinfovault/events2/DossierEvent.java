/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;


/**
 * TODO: Add Javadoc
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
@LoggableEvent(EventCategory.DOSSIER)
public interface DossierEvent extends MivEvent
{
    /**
     * @return dossier for which this event occurred
     */
    public Dossier getDossier();


    /**
     * @return ID of the dossier for which this event occurred
     */
    public long getDossierId();
}
