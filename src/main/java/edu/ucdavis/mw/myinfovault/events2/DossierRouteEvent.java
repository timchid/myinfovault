/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierRouteEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.action.Route;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * An event to indicate a Dossier was routed forward to a subsequent workflow node.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class DossierRouteEvent extends DossierEventStub implements DossierEvent, RouteEvent
{
    private static final long serialVersionUID = 201307311025L;

    private final Route route;

    /**
     * @param realPerson
     * @param dossier
     * @param from
     * @param to
     */
    public DossierRouteEvent(final MivPerson realPerson, final Dossier dossier, final DossierLocation from, final DossierLocation to)
    {
        this(null, realPerson, dossier, from, to);
    }

    /**
     * @param parent
     * @param realPerson
     * @param dossier
     * @param from
     * @param to
     */
    public DossierRouteEvent(final MivEvent parent, final MivPerson realPerson, final Dossier dossier, final DossierLocation from, final DossierLocation to)
    {
        super(parent, realPerson, dossier);

        this.route = new Route(from, to);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RouteEvent#getFromLocation()
     */
    @Override
    public DossierLocation getFromLocation()
    {
        return route.getOrigin();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RouteEvent#getToLocation()
     */
    @Override
    public DossierLocation getToLocation()
    {
        return route.getDestination();
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.RouteEvent#getRoute()
     */
    @Override
    public Route getRoute()
    {
        return route;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Dossier Route Event";
    }
}
