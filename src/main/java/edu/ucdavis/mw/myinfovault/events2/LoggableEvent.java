/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LoggableEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotate an event interface as being Loggable, and provide the category of the event.
 *
 * @author Craig Gilmore
 * @author Stephen Paulsen, typist
 * @since MIV 4.7.2
 */
public @Retention(RetentionPolicy.RUNTIME) @interface LoggableEvent
{
    EventCategory value();
}
