/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RecordChangeEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

/**
 * RecordChangeEvent interface.
 * Support for INSERT, UPDATE and DELETE events.
 *
 * @author rhendric
 * @since MIV 5.0
 */
@LoggableEvent(EventCategory.RECORDCHG)
public interface RecordChangeEvent extends MivEvent
{
    public enum ChangeAction
    {
        INSERT("Insert"),
        UPDATE("Update"),
        DELETE("Delete");

        private String actionDescription;

        private ChangeAction(String actionDescription)
        {
            this.actionDescription = actionDescription;
        }

        public String getDescription()
        {
            return this.actionDescription;
        }
    }

    /**
     * @return the recordId for which the change event occurred
     */
    public long getRecordId();

    /**
     * @return the table name where the record was chenged
     */
    public String getTableName();

    /**
     * @return the action taken on the record - insert, update, delete
     */
    public ChangeAction getAction();
}
