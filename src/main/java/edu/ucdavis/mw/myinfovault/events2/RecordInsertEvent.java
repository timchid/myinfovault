/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RecordInsertEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * @author rhendric
 * @since MIV 5.0
 *
 */
public class RecordInsertEvent extends RecordChangeEventStub // implements RecordChangeEvent
{
    private static final long serialVersionUID = 201503040816L;

    public RecordInsertEvent(String tableName, int recordId, MivPerson realPerson)
    {
        super( realPerson, tableName, recordId, ChangeAction.INSERT);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Record Insert Event";
    }

}
