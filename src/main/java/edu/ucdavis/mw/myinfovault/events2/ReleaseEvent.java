/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReleaseEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * ReleaseEvent
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
@LoggableEvent(EventCategory.RELEASE)
public interface ReleaseEvent extends DossierEvent
{
    /**
     * @return workflow location at which the release event occurred
     */
    public DossierLocation getLocation();

    /**
     * @return scope in which the release event occurred
     */
    public Scope getScope();

    /**
     * @return release attribute name
     */
    public String getAttributeName();
}
