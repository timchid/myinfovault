/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;

/**
 * TODO: Add Javadoc
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
@LoggableEvent(EventCategory.SIGNATURE)
public interface SignatureEvent extends MivEvent
{
//    /**
//     * Get the document that was or is to be signed.
//     * @return an MivDocument ...
//     */
//    public MivDocument getDocumentType();

//// Maybe instead of 'MivDocument getDocument()' it should
//// be 'SignaableDocument getDocument()' -- probably not though... I think
//// we don't have the SignableDocument when a Signature Request is made.

//    /**
//     * @return MIV signature acted on in this event
//     */
//    public int getSignerId();
    
    /**
     * Get the electronic signature for this document.
     * @return MivElectronicSignature
     */
    public MivElectronicSignature getSignature();
}
