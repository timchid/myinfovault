/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserDeactivatedEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Event to indicate that a user was deactivated.
 * @author stephenp
 *
 */
public class UserDeactivatedEvent extends UserActivationEvent
{
    private static final long serialVersionUID = 201412031147L;

    /**
     * @param realPerson
     * @param targetPerson
     */
    public UserDeactivatedEvent(MivPerson realPerson, MivPerson targetPerson)
    {
        super(realPerson, targetPerson, false);
    }

}
