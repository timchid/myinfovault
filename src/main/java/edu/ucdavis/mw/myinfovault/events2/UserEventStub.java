/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserEventStub.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Provide basic support for the common requirements of all user related events.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.2
 */
public abstract class UserEventStub extends MivEventStub implements UserEvent
{
    private static final long serialVersionUID = 201403191746L;

    private final MivPerson targetUser;

    /**
     * Create a user event in response to a parent event.
     *
     * @param parent parent event
     * @param realPerson real event initiator
     * @param targetUser event target user
     */
    public UserEventStub(final MivEvent parent, final MivPerson realPerson, final MivPerson targetUser)
    {
        super(parent, realPerson);

        this.targetUser = targetUser;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.UserEvent#getTargetUser()
     */
    @Override
    public MivPerson getTargetUser()
    {
        return targetUser;
    }
}
