package edu.ucdavis.mw.myinfovault.mapping.biosketch;

import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Criterion;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset.Operator;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchCommand;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchSectionCommand;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchType;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.SelectDataCommand;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.designer.Alignment;
import edu.ucdavis.myinfovault.designer.Availability;
import edu.ucdavis.myinfovault.format.QuoteCleaner;

/**
 * Maps the command objects to domain objects and domain objects to command objects
 *
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchMapper
{
    public Biosketch mapBiosketchCommandToBiosketch(BiosketchCommand biosketchCommand, Biosketch biosketch)
    {
        biosketch.setName(biosketchCommand.getName());
        biosketch.setTitle(biosketchCommand.getTitle());

        Iterable<BiosketchAttributes> attributeList = biosketch.getBiosketchAttributes();
        for (BiosketchAttributes attribute : attributeList)
        {
            if (attribute.getName().equalsIgnoreCase("full_name"))
            {
                attribute.setValue(biosketchCommand.getFullName());
            }
            else if (attribute.getName().equalsIgnoreCase("programdirector_name"))
            {
                attribute.setValue(biosketchCommand.getProgramDirectorName());
            }
            else if (attribute.getName().equalsIgnoreCase("era_username"))
            {
                attribute.setValue(biosketchCommand.getEraUserName());
            }
            else if (attribute.getName().equalsIgnoreCase("position_title1"))
            {
                attribute.setValue(biosketchCommand.getPositionTitle1());
            }
            else if (attribute.getName().equalsIgnoreCase("position_title2"))
            {
                attribute.setValue(biosketchCommand.getPositionTitle2());
            }
        }

        BiosketchStyle biosketchStyle = biosketch.getBiosketchStyle();
        biosketchStyle.setStyleName(biosketchCommand.getStyleName());
        biosketchStyle.setPageWidth(Integer.parseInt(biosketchCommand.getPageWidth()));
        biosketchStyle.setPageHeight(Integer.parseInt(biosketchCommand.getPageHeight()));
        biosketchStyle.setMarginTop(Integer.parseInt(biosketchCommand.getPageHeight()));
        biosketchStyle.setMarginRight(Integer.parseInt(biosketchCommand.getPageHeight()));
        biosketchStyle.setMarginBottom(Integer.parseInt(biosketchCommand.getPageHeight()));
        biosketchStyle.setMarginLeft(Integer.parseInt(biosketchCommand.getPageHeight()));
        biosketchStyle.setTitleAlign(Alignment.valueOf(biosketchCommand.getTitleAlign()));
        biosketchStyle.setTitleBold(returnBoolean(biosketchCommand.getTitleBold()));
        biosketchStyle.setTitleItalic(returnBoolean(biosketchCommand.getTitleItalic()));
        biosketchStyle.setTitleUnderline(returnBoolean(biosketchCommand.getTitleUnderline()));
        biosketchStyle.setTitleRules(returnBoolean(biosketchCommand.getTitleRules()));
        biosketchStyle.setTitleEditable(returnBoolean(biosketchCommand.getTitleEditable()));
        biosketchStyle.setDisplayNameAign(Alignment.valueOf(biosketchCommand.getDisplayNameAlign()));
        biosketchStyle.setHeaderFontID(Integer.parseInt(biosketchCommand.getHeaderFontID()));
        biosketchStyle.setHeaderSize(Integer.parseInt(biosketchCommand.getHeaderSize()));
        biosketchStyle.setHeaderAlign(Alignment.valueOf(biosketchCommand.getHeaderAlign()));
        biosketchStyle.setHeaderBold(returnBoolean(biosketchCommand.getHeaderBold()));
        biosketchStyle.setHeaderItalic(returnBoolean(biosketchCommand.getHeaderItalic()));
        biosketchStyle.setHeaderUnderline(returnBoolean(biosketchCommand.getHeaderUnderline()));
        biosketchStyle.setHeaderIndent(Integer.parseInt(biosketchCommand.getHeaderIndent()));
        biosketchStyle.setBodyFontID(Integer.parseInt(biosketchCommand.getBodyFontID()));
        biosketchStyle.setBodySize(Integer.parseInt(biosketchCommand.getBodySize()));
        biosketchStyle.setBodyIndent(Integer.parseInt(biosketchCommand.getBodyIndent()));
        biosketchStyle.setBodyFormatting(returnBoolean(biosketchCommand.getBodyFormatting()));
        biosketchStyle.setFooterOn(returnBoolean(biosketchCommand.getFooterOn()));
        biosketchStyle.setFooterPageNumbers(returnBoolean(biosketchCommand.getFooterPageNumbers()));
        biosketchStyle.setFooterRules(returnBoolean(biosketchCommand.getFooterRules()));

        return biosketch;
    }

    public BiosketchCommand mapBiosketchToBiosketchCommand(Biosketch biosketch, BiosketchCommand biosketchCommand)
    {
        QuoteCleaner quoteCleaner = new QuoteCleaner();
        biosketchCommand.setName(quoteCleaner.format(MIVUtil.escapeMarkup(biosketch.getName())));
        biosketchCommand.setTitle(quoteCleaner.format(MIVUtil.escapeMarkup(biosketch.getTitle())));
        biosketchCommand.setId("" + biosketch.getId());
        biosketchCommand.setBiosketchType(""+biosketch.getBiosketchType());


        Iterable<BiosketchAttributes> attributeList = biosketch.getBiosketchAttributes();
        for (BiosketchAttributes attribute : attributeList)
        {
            String attributeName = attribute.getName();
            String attributeValue = attribute.getValue();
            if (attributeName.equalsIgnoreCase("full_name"))
            {
                biosketchCommand.setFullName(quoteCleaner.format(MIVUtil.escapeMarkup(attributeValue)));
            }
            else if (attributeName.equalsIgnoreCase("programdirector_name"))
            {
                biosketchCommand.setProgramDirectorName(quoteCleaner.format(MIVUtil.escapeMarkup(attributeValue)));

            }
            else if (attributeName.equalsIgnoreCase("era_username"))
            {
                biosketchCommand.setEraUserName(quoteCleaner.format(MIVUtil.escapeMarkup(attributeValue)));
            }
            else if (attributeName.equalsIgnoreCase("position_title1"))
            {
                biosketchCommand.setPositionTitle1(quoteCleaner.format(MIVUtil.escapeMarkup(attributeValue)));
            }
            else if (attributeName.equalsIgnoreCase("position_title2"))
            {
                biosketchCommand.setPositionTitle2(quoteCleaner.format(MIVUtil.escapeMarkup(attributeValue)));
            }
        }

        biosketchCommand.setStyleName(biosketch.getBiosketchStyle().getStyleName());
        biosketchCommand.setPageWidth("" + biosketch.getBiosketchStyle().getPageWidth());
        biosketchCommand.setPageHeight("" + biosketch.getBiosketchStyle().getPageHeight());
        biosketchCommand.setMarginTop("" + biosketch.getBiosketchStyle().getPageHeight());
        biosketchCommand.setMarginRight("" + biosketch.getBiosketchStyle().getPageHeight());
        biosketchCommand.setMarginBottom("" + biosketch.getBiosketchStyle().getPageHeight());
        biosketchCommand.setMarginLeft("" + biosketch.getBiosketchStyle().getPageHeight());
        biosketchCommand.setTitleAlign(biosketch.getBiosketchStyle().getTitleAlign().toString());
        biosketchCommand.setTitleBold(returnString(biosketch.getBiosketchStyle().isTitleBold()));
        biosketchCommand.setTitleItalic(returnString(biosketch.getBiosketchStyle().isTitleItalic()));
        biosketchCommand.setTitleUnderline(returnString(biosketch.getBiosketchStyle().isTitleUnderline()));
        biosketchCommand.setTitleRules(returnString(biosketch.getBiosketchStyle().isTitleRules()));
        biosketchCommand.setTitleEditable(returnString(biosketch.getBiosketchStyle().isTitleEditable()));
        biosketchCommand.setDisplayNameAlign(biosketch.getBiosketchStyle().getDisplayNameAign().toString());
        biosketchCommand.setHeaderFontID("" + biosketch.getBiosketchStyle().getHeaderFontID());
        biosketchCommand.setHeaderSize("" + biosketch.getBiosketchStyle().getHeaderSize());
        biosketchCommand.setHeaderAlign(biosketch.getBiosketchStyle().getHeaderAlign().toString());
        biosketchCommand.setHeaderBold(returnString(biosketch.getBiosketchStyle().isHeaderBold()));
        biosketchCommand.setHeaderItalic(returnString(biosketch.getBiosketchStyle().isHeaderItalic()));
        biosketchCommand.setHeaderUnderline(returnString(biosketch.getBiosketchStyle().isHeaderUnderline()));
        biosketchCommand.setHeaderIndent(""+biosketch.getBiosketchStyle().getHeaderIndent());
        biosketchCommand.setBodyFontID("" + biosketch.getBiosketchStyle().getBodyFontID());
        biosketchCommand.setBodySize("" + biosketch.getBiosketchStyle().getBodySize());
        biosketchCommand.setBodyIndent(""+biosketch.getBiosketchStyle().getBodyIndent());
        biosketchCommand.setBodyFormatting(returnString(biosketch.getBiosketchStyle().isBodyFormatting()));
        biosketchCommand.setFooterOn(returnString(biosketch.getBiosketchStyle().isFooterOn()));
        biosketchCommand.setFooterPageNumbers(returnString(biosketch.getBiosketchStyle().isFooterPageNumbers()));
        biosketchCommand.setFooterRules(returnString(biosketch.getBiosketchStyle().isFooterRules()));

        return biosketchCommand;
    }




    /**
     * @param selectDataCommand
     * @param biosketch
     * @return
     * returns the Biosketch object after mapping selectbiosketchdata.jsp form values to Biosketch object
     */
    public Biosketch mapSelectDataCommandToBiosketch(SelectDataCommand selectDataCommand, Biosketch biosketch)
    {
        Iterable<BiosketchSection> biosketchSectionList = biosketch.getBiosketchSections();
        List<BiosketchSectionCommand> biosketchSectionCommandList = selectDataCommand.getBiosketchSection();
        boolean personalDisplay=false;
        boolean foundPersonal = false;

        for (BiosketchSectionCommand biosketchSectionCommand : biosketchSectionCommandList)
        {
            for (BiosketchSection biosketchSection : biosketchSectionList)
            {
                if(biosketchSection.getSectionName().equals("personal") && !foundPersonal)
                {
                    personalDisplay=biosketchSectionCommand.isDisplay();
                    foundPersonal = true;
                }
                int commandSectionID =  Integer.parseInt(biosketchSectionCommand.getId());
                int sectionID = biosketchSection.getId();

                if (sectionID == commandSectionID && biosketchSection.isInSelectList() && biosketchSectionCommand.getAvailability() != Availability.MANDATORY)
                {
                    boolean display = biosketchSectionCommand.isDisplay();
                    if(display != biosketchSection.isDisplay())
                    {
                        biosketchSection.setDisplayHeader(display);
                    }
                    biosketchSection.setDisplay(display);
                    if (biosketchSectionCommand.getDisplayInBiosketch() == 1000)
                    {
                        biosketchSection.setDisplay(personalDisplay);
                    }
                    break;

                }

            }
        }




        // this is the list with the existing ruleset values and updated ruleset
        // values
        /*List<Ruleset> completeRulesetList = new LinkedList<Ruleset>();

        Iterable<Ruleset> rulesetList = biosketch.getRuleset();
        // if there is an existing ruleset, UPDATE the ruleset
        if (rulesetList != null)
        {
            for (Ruleset ruleset : rulesetList)
            {
                // adding the existing rulesets
                completeRulesetList.add(ruleset);
            }
            // getting the updated rulesets
            List<Ruleset> newrulesetList = buildRulesetOnFormValues(selectDataCommand, biosketch);
            for (Ruleset ruleset : newrulesetList)
            {
                // adding the updated rulesets
                completeRulesetList.add(ruleset);
            }
            // setting the ruleset object on biosketch
            biosketch.setRuleset(completeRulesetList);
        }
        // if there is no existing ruleset, CREATE A NEW ruleset
        else
        {
            // getting the new rulesets
            List<Ruleset> newrulesetList = buildRulesetOnFormValues(selectDataCommand, biosketch);
            // setting the ruleset object on biosketch
            biosketch.setRuleset(newrulesetList);
        }*/

        // getting the new rulesets
        List<Ruleset> newrulesetList = buildRulesetOnFormValues(selectDataCommand, biosketch);
        // setting the ruleset object on biosketch
        biosketch.setRuleset(newrulesetList);

        return biosketch;
    }

    /**
     * @param biosketch
     * @param selectDataCommand
     * @return
     * maps all the values required for the display of selectbiosketchdata.jsp page from Biosketch object to SelectDataCommand object
     */
    public SelectDataCommand mapBiosketchToSelectDataCommand(Biosketch biosketch, SelectDataCommand selectDataCommand,String biosketchTypeName)
    {
        QuoteCleaner quoteCleaner = new QuoteCleaner();
        selectDataCommand.setName(quoteCleaner.format(MIVUtil.escapeMarkup(biosketch.getName())));
        Iterable<BiosketchSection> biosketchSectionList = biosketch.getBiosketchSections();
        List<BiosketchSectionCommand> biosketchSectionCommandList = new ArrayList<BiosketchSectionCommand>();

        for (BiosketchSection biosketchSection : biosketchSectionList)
        {
            biosketchSectionCommandList.add(new BiosketchSectionCommand(biosketchSection, biosketchTypeName));
        }

        selectDataCommand.setBiosketchSection(biosketchSectionCommandList);


        Iterable<Ruleset> rulesetList = biosketch.getRuleset();
        if (rulesetList != null)
        {
            for (Ruleset ruleset : rulesetList)
            {
                Iterable<Criterion> criteriaList = ruleset.getCriterion();
                if (criteriaList != null)
                {
                    for (Criterion criteria : criteriaList)
                    {
                        switch (criteria.getOperator())
                        {
                            case LE:
                                //selectDataCommand.getRuleset().getCriteria().setEndYear(criteria.getValue());
                                selectDataCommand.setEndYear(criteria.getValue());
                                break;
                            case GE:
                                //selectDataCommand.getRuleset().getCriteria().setStartYear(criteria.getValue());
                                selectDataCommand.setStartYear(criteria.getValue());
                                break;
                            default:
                                // FIXME: This should never happen -- log an error here.
                                break;
                        }
/* Don't turn an enum into a String for comparison! The enum was defined so it wouldn't have to be treated like a String!
  (this also should have used and 'else' so it didn't compare to "GE" when it had already found "LE")
                        String comparison = criteria.getOperator().toString();
                        if (comparison.equals("LE"))
                        {
                            selectDataCommand.getRuleset().getCriteria().setEndYear(criteria.getValue());
                        }
                        if (comparison.equals("GE"))
                        {
                            selectDataCommand.getRuleset().getCriteria().setStartYear(criteria.getValue());
                        }
*/
                    } // for criteria
                }
            } // for ruleset
        }

        return selectDataCommand;
    }


    /**
     * @param selectDataCommand
     * @param biosketch
     * @return
     * creates a list of ruleset with the criteria list attached
     */
    private List<Ruleset> buildRulesetOnFormValues(SelectDataCommand selectDataCommand, Biosketch biosketch)
    {

        // creating a list of ruleset
        List<Ruleset> newrulesetList = new ArrayList<Ruleset>();
        List<BiosketchSectionCommand> biosketchSectionCommandList = selectDataCommand.getBiosketchSection();

        // collecting all the criteria fields from the jsp page
        String ge = selectDataCommand.getStartYear();//getRuleset().getCriteria().getStartYear();
        String le = selectDataCommand.getEndYear();//getRuleset().getCriteria().getEndYear();
        //boolean presentYear = returnBoolean(selectDataCommand.getRuleset().getCriteria().getPresentYear());
        String field = selectDataCommand.getRuleset().getCriteria().getField();

        // if >= and <= are entered and present is checked, only 1 criteria is
        // created with >= .
        // if >= is entered and <= is not entered and present is checked, only 1
        // criteria is created with >=.
        // if >= is entered and <= is not entered and present is not checked,
        // only 1 criteria is created with >=.
        if (ge != "" && le == "")
        {
            // creating a new ruleset object
            Ruleset ruleset = new Ruleset(0, biosketch.getUserID());
            // creating a new criteria object for >= field / present field
            Criterion criteria1 = new Criterion();

            criteria1.setField(field);
            criteria1.setOperator(Criterion.RelOp.GE);
            criteria1.setValue(ge);

            ruleset.addCriterion(criteria1);

            // adding the ruleset to the list of ruleset
            newrulesetList.add(ruleset);
        }

        // if >= is not entered and <= is entered and present is not checked,
        // only 1 criteria is created with <=.
        if (ge == "" && le != "")
        {
            // creating a new ruleset object
            Ruleset ruleset = new Ruleset(0, biosketch.getUserID());
            // creating a new criteria object for <= field
            Criterion criteria1 = new Criterion();

            criteria1.setField(field);
            criteria1.setOperator(Criterion.RelOp.LE);
            criteria1.setValue(le);

            ruleset.addCriterion(criteria1);

            // adding the ruleset to the list of ruleset
            newrulesetList.add(ruleset);
        }

        // if >= and <= are entered and present is not checked, 2 criterias are
        // created with >= and <=.
        if (ge != "" && le != "")
        {
            // creating a new ruleset object
            Ruleset ruleset = new Ruleset(0, biosketch.getUserID());
            // creating a new criteria object for >= field / present field
            Criterion criteria1 = new Criterion();
            // creating a new criteria object for <= field
            Criterion criteria2 = new Criterion();

            criteria1.setField(field);
            criteria1.setOperator(Criterion.RelOp.GE);
            criteria1.setValue(ge);

            criteria2.setField(field);
            criteria2.setOperator(Criterion.RelOp.LE);
            criteria2.setValue(le);

            ruleset.setOperator(Operator.AND);
            ruleset.addCriterion(criteria1);
            ruleset.addCriterion(criteria2);

            // adding the ruleset to the list of ruleset
            newrulesetList.add(ruleset);
        }

        Ruleset rulesetForPublications = new Ruleset(0, biosketch.getUserID()); // what if none of publications selected , do we have empty ruleset
        boolean addRuleset = false;
        for (BiosketchSectionCommand biosketchSectionCommand : biosketchSectionCommandList)
        {
            if (biosketchSectionCommand.getDisplayInBiosketch() != 0
             && biosketchSectionCommand.getDisplayInBiosketch() != 1000
             && biosketchSectionCommand.isDisplay())
            {
                addRuleset = true;
                // creating a new criteria object for >= field / present field
                Criterion criteria1 = new Criterion();

                criteria1.setField("TypeID");
                criteria1.setOperator(Criterion.RelOp.EQ);
                criteria1.setValue(Integer.toString(biosketchSectionCommand.getDisplayInBiosketch()));

                rulesetForPublications.setOperator(Operator.OR);
                rulesetForPublications.addCriterion(criteria1);
            }
        }

        if (!addRuleset && biosketch.getBiosketchType()== BiosketchType.BIOSKETCH)
        {
            Criterion none = new Criterion();
            none.setField("TypeID");
            none.setOperator(Criterion.RelOp.LT);
            none.setValue("" + -1);
            addRuleset = true;
        }

        if (addRuleset)
        {
            newrulesetList.add(rulesetForPublications);
        }


        // note : if >= is not entered and <= is entered and present is checked,
        // no ruleset is created at all
        // not : also if >= and <= are not entered and present is not checked,
        // no ruleset is created at all

        return newrulesetList;
    }


    private boolean returnBoolean(String value)
    {
        return "1".equals(value) || "true".equalsIgnoreCase(value);
    }

    private String returnString(boolean value)
    {
        return value ? "1" : "0";
    }

}
