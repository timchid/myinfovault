/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: NotificationService.java
 */

package edu.ucdavis.mw.myinfovault.service;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events2.DisclosureRequestDeleteEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierErrorEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.myinfovault.MivServerAwareMailMessage;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Notifies users in response to MIV events.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.4
 */
public class NotificationService
{
    private static final String NOTIFICATION_PROPERTIES_FILENAME = "/notification.properties";
    private static final String NOTIFICATION_PROPERTIES_LOG = "Loaded notification service properties from " + NOTIFICATION_PROPERTIES_FILENAME;

    private static Logger log = LoggerFactory.getLogger(DaoSupport.class);

    private final Properties properties = new Properties();

    /**
     * Create the notification service bean.
     */
    public NotificationService()
    {
        loadProperties(null);

        EventDispatcher2.getDispatcher().register(this);
    }

    /**
     * Load notification service properties.
     *
     * @param event configuration change
     */
    @Subscribe
    public void loadProperties(ConfigurationChangeEvent event)
    {
        try (InputStream in = DaoSupport.class.getResourceAsStream(NOTIFICATION_PROPERTIES_FILENAME))
        {
            if (in == null) {
                throw new IOException("Notification service properties file is missing!");
            }

            properties.load(in);

            log.info(NOTIFICATION_PROPERTIES_LOG);
        }
        catch (IOException e)
        {
            throw new MivSevereApplicationError("errors.notification.properties", e, NOTIFICATION_PROPERTIES_FILENAME);
        }
    }

    /**
     * Notify candidate of CDC request rescission.
     *
     * @param event CDC signature request delete
     */
    @Subscribe
    public void notifyCandidate(DisclosureRequestDeleteEvent event)
    {
        try
        {
            String subject = properties.getProperty("notification.cdc.request.delete.subject");

            String message = MessageFormat.format(properties.getProperty("notification.cdc.request.delete.message"),
                                                  event.getDossier().getAction().getActionType().getDescription(),
                                                  event.getDisclosure().getScope().getDepartmentDescription(),
                                                  event.getSignature().getCreatedDate());

            MivServerAwareMailMessage email = new MivServerAwareMailMessage(event.getDossier().getAction().getCandidate(),
                                                                            event.getShadowPerson(),
                                                                            event.getRealPerson(),
                                                                            subject);
            email.send(message);

        }
        catch (MessagingException e)
        {
            EventDispatcher2.getDispatcher().post(
                new DossierErrorEvent(event, event.getDossier(), e)
                    .setComments(properties.getProperty("notification.cdc.request.delete.error")));
        }
    }
}
