/**
 * Copyright
 * Copyright (c) University of California, Davis, 2009
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchFilter.java
 */

package edu.ucdavis.mw.myinfovault.service;

import java.util.Collection;



/**
 * Defines the interface for further filtering of search results, as specified for the UserService and the DossierService.
 * @param <T> the type (class or interface) an implementation of the SearchFilter will accept to filter on.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public interface SearchFilter<T>
{
    /**
     * Given an item from some initial search results, this method should determine
     * whether or not to include the item in the final results based on whatever
     * criteria an implementor desires.
     *
     * @param item an item of the type T that will be included or excluded from the result set
     * @return <code>true</code> to indicate this item should be included in the search results; <code>false</code> to exclude the item.
     */
    public boolean include(T item);

    /**<p>
     * Apply this filter to some Collection of items.</p>
     * <p>The returned Collection will be of the same type as the Collection that
     * is provided as the <em>items</em> parameter. If an empty Collection is passed
     * a new empty Collection of the same type will be returned. If {@code null} is
     * passed an empty List will be returned.</p>
     * @param items The items to which this filter will be applied.
     * @return a new Collection of filtered items or an empty Collection
     */
    public <C extends Collection<T>> C apply(C items);
}
