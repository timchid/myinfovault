/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.academicaction;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.common.collect.ImmutableMap;
import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.dao.aa.AcademicActionDao;
import edu.ucdavis.mw.myinfovault.dao.raf.RafDao;
import edu.ucdavis.mw.myinfovault.domain.PolarResponse;
import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.AppointmentDetails;
import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.SalaryPeriod;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionBo;
import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.service.DocumentService;
import edu.ucdavis.mw.myinfovault.service.decision.DecisionService;
import edu.ucdavis.mw.myinfovault.service.person.AppointmentScope;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.PropertyManager;

/**
 * Governs the use of {@link AcademicActionBo} business objects.
 *
 * @author Rick Hendricks
 * @since MIV 4.8
 */
public class AcademicActionServiceImpl extends DocumentService implements AcademicActionService
{
    private static final ThreadLocal<DecimalFormat> currencyFormatter =
            new ThreadLocal<DecimalFormat>() {
                @Override protected DecimalFormat initialValue() {
                    return new DecimalFormat("###,###,##0.00");
                }
            };

    private final AcademicActionDao aaDao;
    private final RafDao rafDao;
    private final DecisionService decisionService;

    /**
     * Academic action form style filename.
     */
    private static String XSLT_FILENAME = MIVConfig.getConfig().getProperty("document-config-location-xslt-aaf");

    /*
     * Field labels.
     */
    private static String DOCUMENT_NAME;
    private static String CURRENT_EMPLOYEE;
    private static String CURRENT_EMPLOYEE_DIRECTION;
    private static String REPRESENTED_EMPLOYEE;
    private static String REPRESENTED_EMPLOYEE_DIRECTION;
    private static String UNION_NOTICE_REQUIRED;
    private static String UNION_NOTICE_REQUIRED_DIRECTION;
    private static String LABOR_RELATIONS_NOTIFIED;
    private static String LABOR_RELATIONS_NOTIFIED_DIRECTION;
    private static String PERCENT_OF_TIME;
    private static String RANK_AND_TITLE;
    private static String TITLE_CODE;
    private static String STEP;
    private static String QUARTER_COUNT;
    private static String YEARS_AT_RANK;
    private static String YEARS_AT_STEP;
    private static String APPOINTMENT_DURATION;
    private static String ANNUAL_SALARY;
    private static String EFFECTIVE_DATE;
    private static String END_DATE;
    private static String RETROACTIVE_DATE;
    private static String PRIMARY_APPOINTMENT;
    private static String JOINT_APPOINTMENT;
    private static String HOURLY_RATE;
    private static String MONTHLY_SALARY;
    private static String ACCELERATION_YEARS;

    static {
        loadFieldLabels(null);
    }

    /**
     * Load academic action form field labels from properties.
     *
     * @param event configuration change
     */
    @Subscribe
    public static void loadFieldLabels(ConfigurationChangeEvent event)
    {
        Properties labels = PropertyManager.getPropertySet("editAcademicAction-flow-form", "labels");
        Properties strings = PropertyManager.getPropertySet("editAcademicAction-flow-form", "strings");

        DOCUMENT_NAME = strings.getProperty("pageTitle");
        CURRENT_EMPLOYEE = labels.getProperty("currentEmployee");
        CURRENT_EMPLOYEE_DIRECTION = strings.getProperty("currentEmployee");
        REPRESENTED_EMPLOYEE = labels.getProperty("representedEmployee");
        REPRESENTED_EMPLOYEE_DIRECTION = strings.getProperty("representedEmployee");
        UNION_NOTICE_REQUIRED = labels.getProperty("unionNoticeRequired");
        UNION_NOTICE_REQUIRED_DIRECTION = strings.getProperty("unionNoticeRequired");
        LABOR_RELATIONS_NOTIFIED = labels.getProperty("laborRelationsNotified");
        LABOR_RELATIONS_NOTIFIED_DIRECTION = strings.getProperty("laborRelationsNotified");
        PERCENT_OF_TIME = labels.getProperty("percentOfTime");
        //WITHOUT_SALARY = labels.getProperty("withoutSalary");
        RANK_AND_TITLE = labels.getProperty("rankAndTitle");
        TITLE_CODE = labels.getProperty("code");
        STEP = labels.getProperty("step");
        QUARTER_COUNT = labels.getProperty("quarterCount");
        YEARS_AT_RANK = labels.getProperty("yearsAtRank");
        YEARS_AT_STEP = labels.getProperty("yearsAtStep");
        APPOINTMENT_DURATION = labels.getProperty("appointmentDuration");
        HOURLY_RATE = labels.getProperty("hourlyRate");
        MONTHLY_SALARY = labels.getProperty("monthlySalary");
        ANNUAL_SALARY = labels.getProperty("annualSalary");
        EFFECTIVE_DATE = labels.getProperty("effectiveDate");
        END_DATE = labels.getProperty("endDate");
        RETROACTIVE_DATE = labels.getProperty("retroactiveDate");
        PRIMARY_APPOINTMENT = labels.getProperty("primaryAppointment");
        JOINT_APPOINTMENT = labels.getProperty("jointAppointment");
        ACCELERATION_YEARS = labels.getProperty("accelerationYears");
    }

    /**
     * Creates the recommend action form service implementation bean.
     *
     * @param aaDao Spring-injected academic action DAO
     * @param rafDao Spring-injected recommended action DAO
     * @param decisionService Spring-injected decision service
     */
    public AcademicActionServiceImpl(AcademicActionDao aaDao,
                                     RafDao rafDao,
                                     DecisionService decisionService)
    {
        this.aaDao = aaDao;
        this.rafDao = rafDao;
        this.decisionService = decisionService;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.academicaction.AcademicActionService#getBo(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public AcademicActionBo getBo(Dossier dossier)
    {
        AppointmentDetails appointmentDetails = aaDao.getAppointmentDetails(dossier.getAcademicActionID());

        // if no appointment details, create new academic action business object
        if (appointmentDetails == null)
        {
            return new AcademicActionBo(dossier);
        }

        // otherwise, load from database
        List<Assignment> presentAssignments = aaDao.getActionAssignments(dossier.getAcademicActionID(), StatusType.PRESENT);
        List<Assignment> proposedAssignments = aaDao.getActionAssignments(dossier.getAcademicActionID(), StatusType.PROPOSED);

        return new AcademicActionBo(dossier,
                                    appointmentDetails,
                                    ImmutableMap.of(StatusType.PRESENT, presentAssignments,
                                                    StatusType.PROPOSED, proposedAssignments));
    }

    /**
     * Persists AA to database and creates PDFs.
     *
     * @param aa recommended action
     * @param realUserId ID of real user initiating the persist
     */
    private void persistBo(final AcademicActionBo aa, int realUserId)
    {
        aaDao.persistAa(aa, realUserId);

        // update AA PDFs
        createPdfs(aa);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.academicaction.AcademicActionService#saveAcademicActionForm(edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionFormBo, edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public AcademicAction saveAcademicAction(AcademicActionBo aa,
                                  MivPerson targetPerson,
                                  MivPerson realPerson)
    {
        // Get fresh dossier object from database
        AcademicAction formerAction = getDossier(aa.getDossier().getDossierId()).getAction();

        /*
         * If delegation authority or action type has changed, remove all decision signatures.
         */
        if (aa.getDossier().getAction().getDelegationAuthority() != formerAction.getDelegationAuthority()
         || aa.getDossier().getAction().getActionType() != formerAction.getActionType())
        {
            List<MivDocument>excludeDocs = new ArrayList<MivDocument>();
            // If only the delegation authority has changed, *do not* remove the disclosure certificate signatures
            if (aa.getDossier().getAction().getActionType() == formerAction.getActionType())
            {
                excludeDocs.add(MivDocument.DISCLOSURE_CERTIFICATE);
                excludeDocs.add(MivDocument.JOINT_DISCLOSURE_CERTIFICATE);
            }
            decisionService.removeDecisions(aa.getDossier(), excludeDocs);
        }

        // using AAF for this dossier, not RAF
        aa.getDossier().setAcademicActionFormPresent(true);
        aa.getDossier().setRecommendedActionFormPresent(false);

        // the legacy RAF (if it exists) is no longer needed
        rafDao.deleteRaf(aa.getDossier().getDossierId());

        // persist AA with changes
        persistBo(aa, realPerson.getUserId());

        // update dossier with AA changes
        dossierService.updateDossier(aa.getDossier(), targetPerson, realPerson);

        return formerAction;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.academicaction.AcademicActionService#createPdfs(edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionFormBo)
     */
    @Override
    public void createPdfs(final AcademicActionBo aa)
    {
        // generate academic action PDF
        createPdf(aa);

        // generate academic action decision PDFs
        for (Decision decision : decisionService.getDecisions(aa.getDossier()))
        {
            decisionService.createPdf(decision);
        }
    }

    /**
     * Creates the PDF file for the AA.
     *
     * @param aa Academic Action
     * @return if PDF was generated successfully
     */
    private boolean createPdf(final AcademicActionBo aa)
    {
        if (aa == null) return false;

        Document doc = getDocument();

        doc.appendChild(getAaElement(doc, aa));

        /*
         * Any non-new-appointment action type is considered a RAF.
         */
        MivDocument documentType = aa.getDossier().getAction().getActionType() == DossierActionType.APPOINTMENT
                                 ? MivDocument.AA
                                 : MivDocument.RAF;
        // Create the PDF
        return createPdf(aa.getDossier().getDossierPdf(documentType),
                         XSLT_FILENAME,
                         doc).exists();
    }

    /**
     * Creates and returns the AA node.
     *
     * @param doc
     * @param aaf
     * @return
     */
    private Element getAaElement(final Document doc, final  AcademicActionBo aaf)
    {
        DossierActionType actionType = aaf.getDossier().getAction().getActionType();

        // Create root node
        final Element root = doc.createElement("aaf");

        root.setAttribute("actiontype", aaf.getDossier().getAction().getActionType().name());

        // Append children
        root.appendChild(getElement(doc, "documentname", DOCUMENT_NAME));
        root.appendChild(getElement(doc, "candidatename", aaf.getDossier().getAction().getCandidate().getDisplayName(), "Name"));
        root.appendChild(getElement(doc, "effectivedate", longDateFormat.get().format(aaf.getDossier().getAction().getEffectiveDate()), EFFECTIVE_DATE));
        root.appendChild(getElement(doc, "actiondescription", aaf.getDossier().getAction().toString()));

        if (aaf.getDossier().getAction().getEndDate() != null)
        {
            root.appendChild(getElement(doc, "enddate", longDateFormat.get().format(aaf.getDossier().getAction().getEndDate()), END_DATE));
        }

        if (aaf.getDossier().getAction().getRetroactiveDate() != null)
        {
            root.appendChild(getElement(doc, "retroactivedate", longDateFormat.get().format(aaf.getDossier().getAction().getRetroactiveDate()), RETROACTIVE_DATE));
        }

        // Add appointment specific elements
        if (actionType == DossierActionType.APPOINTMENT)
        {
            root.appendChild(getAppointmentQuestionsElement(doc, aaf.getApptDetails()));
        }
        // Acceleration years only applies to non-new appointment actions
        else
        {
            root.appendChild(getElement(doc, "accelerationyears", aaf.getDossier().getAction().getAccelerationYears(), ACCELERATION_YEARS));
        }

        // append present and proposed assignment statuses
        root.appendChild(getStatusesElement(doc, aaf.getAssignmentStatuses()));

        return root;
    }

    /**
     * Creates and returns the appointment form node.
     *
     * @param doc
     * @param apptDetails
     * @return
     */
    private Element getAppointmentQuestionsElement(final Document doc, final AppointmentDetails apptDetails)
    {
        // Create root node
        Element elem = doc.createElement("questions");

        elem.appendChild(getAppointmentQuestionElement(doc, CURRENT_EMPLOYEE, CURRENT_EMPLOYEE_DIRECTION, apptDetails.getCurrentEmployee()));
        elem.appendChild(getAppointmentQuestionElement(doc, REPRESENTED_EMPLOYEE, REPRESENTED_EMPLOYEE_DIRECTION, apptDetails.getRepresented()));
        elem.appendChild(getAppointmentQuestionElement(doc, UNION_NOTICE_REQUIRED, UNION_NOTICE_REQUIRED_DIRECTION, apptDetails.getNoticeToUnion()));
        elem.appendChild(getAppointmentQuestionElement(doc, LABOR_RELATIONS_NOTIFIED, LABOR_RELATIONS_NOTIFIED_DIRECTION, apptDetails.getNoticeToLaborRelations()));

        return elem;
    }

    /**
     * Creates and returns the question node.
     *
     * @param doc
     * @param question
     * @param direction
     * @param response
     * @return
     */
    private Element getAppointmentQuestionElement(final Document doc,
                                                  final String question,
                                                  final String direction,
                                                  final PolarResponse response)
    {
        // Create root node
        final Element root = doc.createElement("question");

        root.appendChild(getElement(doc, "description", question));
        root.appendChild(getElement(doc, "direction", direction));
        root.appendChild(getElement(doc, "response", response.getDescription()));

        return root;
    }

    /**
     * Creates and returns the assignment statuses node.
     *
     * @param doc
     * @param assignment statuses
     * @return
     */
    private Element getStatusesElement(final Document doc, Map<StatusType, List<Assignment>> statuses)
    {
        // Create root node
        final Element root = doc.createElement("statuses");

        for (Map.Entry<StatusType, List<Assignment>> status : statuses.entrySet())
        {
            root.appendChild(getStatusElement(doc, status));
        }

        return root;
    }

    /**
     * Creates and returns the assignment node.
     *
     * @param doc
     * @param assignments
     * @param actionType
     * @param assignmentStatus
     * @return
     */
    private Element getStatusElement(final Document doc, final Map.Entry<StatusType, List<Assignment>> status)
    {
        // Create status element
        Element elem = doc.createElement(status.getKey().name().toLowerCase());

        elem.setAttribute("label", status.getKey().getDescription());

        // Adding assignments to status
        for (Assignment assignment: status.getValue())
        {
            // Append to root
            elem.appendChild(getAssignmentElement(doc, assignment));
        }

        return elem;
    }

    /**
     * Creates and returns the assignment node.
     *
     * @param doc
     * @param assignment
     * @return
     */
    private Element getAssignmentElement(final Document doc, final Assignment assignment)
    {
        // Create department element
        Element elem = doc.createElement("assignment");

        // assignment percent of time
        elem.appendChild(getElement(doc, "percentoftime", MessageFormat.format("{0,number,percent}", assignment.getPercentOfTime()), PERCENT_OF_TIME));

        // assignment scope
        elem.appendChild(getScopeElement(doc, (AppointmentScope)assignment.getScope(), assignment.isPrimary()));

        // Adding titles to department
        elem.appendChild(getTitlesElement(doc, assignment.getTitles()));

        return elem;
    }

    /**
     * Creates and returns the scope node.
     *
     * @param doc
     * @param actionType
     * @param assignments
     * @return
     */
    private Element getScopeElement(final Document doc, AppointmentScope scope, boolean isPrimary)
    {
        // Create department element
        Element elem = doc.createElement("scope");

        elem.setAttribute("label", isPrimary ? PRIMARY_APPOINTMENT : JOINT_APPOINTMENT);

        // Append school description
        elem.appendChild(getElement(doc, "school", scope.getSchoolDescription()));

        // append department description if any
        if (StringUtils.isNotBlank(scope.getDepartmentDescription()))
        {
            elem.appendChild(getElement(doc, "department", scope.getDepartmentDescription()));
        }

        return elem;
    }

    /**
     * Creates and returns the titles node.
     *
     * @param doc
     * @param titles
     * @return
     */
    private Element getTitlesElement(final Document doc, final List<Title> titles)
    {
        // Create root node
        final Element root = doc.createElement("titles");

        // Loop titles to get the title
        for (final Title title: titles)
        {
            // Create title element
            Element elem = doc.createElement("title");

            // Add percent symbol to percentOfTime
            String percentOfTime = MessageFormat.format("{0,number,percent}", title.getPercentOfTime());

            // Append children
            elem.appendChild(getElement(doc, "title-code", title.getCode(), TITLE_CODE));
            elem.appendChild(getElement(doc, "rank-title", title.getDescription(), RANK_AND_TITLE));
            elem.appendChild(getElement(doc, "quartercount", title.getYearsAtRank(), QUARTER_COUNT));
            elem.appendChild(getElement(doc, "years-at-rank", title.getYearsAtRank(), YEARS_AT_RANK));
            elem.appendChild(getElement(doc, "years-at-step", title.getYearsAtStep(), YEARS_AT_STEP));
            elem.appendChild(getElement(doc, "step", title.getStep(), STEP));

            Element percentoftime = getElement(doc, "percentoftime", percentOfTime, PERCENT_OF_TIME);

            // if WithoutSalary is true than make sure PercentOfTime must be ZERO
            if (title.isWithoutSalary() && title.getPercentOfTime().compareTo(BigDecimal.ZERO) == 0)
            {
                percentoftime.setAttribute("withoutsalary", "true");
            }
            elem.appendChild(percentoftime);

            elem.appendChild(getElement(doc, "duration", title.getAppointmentDuration().getLabel(), APPOINTMENT_DURATION));

            if (title.getSalaryPeriod() == SalaryPeriod.HOURLY)
            {
                elem.appendChild(getElement(doc, "hourly-rate", currencyFormatter.get().format(title.getPeriodSalary()).toString(), HOURLY_RATE));
            }
            else if (title.getSalaryPeriod() == SalaryPeriod.MONTHLY)
            {
                elem.appendChild(getElement(doc, "monthly-salary", currencyFormatter.get().format(title.getPeriodSalary()).toString(), MONTHLY_SALARY));
                elem.appendChild(getElement(doc, "annual-salary", currencyFormatter.get().format(title.getAnnualSalary()).toString(), ANNUAL_SALARY));
            }

            // Append to root
            root.appendChild(elem);
        }

        return root;
    }
}
