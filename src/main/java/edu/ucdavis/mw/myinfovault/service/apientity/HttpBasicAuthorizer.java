/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: HttpBasicAuthorizer.java
 */
package edu.ucdavis.mw.myinfovault.service.apientity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.Collection;

import javax.security.auth.login.CredentialException;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

import com.Ostermiller.util.Base64;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Authorizer for use with the HTTP basic authentication scheme.
 *
 * @author japorito
 * @since 5.0
 */
public class HttpBasicAuthorizer extends ApiAuthorizerBase implements ApiAuthorizer
{
    private ApiEntity entity;
    private static final int KEY_LENGTH = 64;

    /**
     * @throws CredentialException
     *
     */
   public HttpBasicAuthorizer(String entityName) throws CredentialException
   {
       super(ApiSecurityScheme.BASIC);

       this.entity = this.loadEntity(entityName);
   }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#permits(java.lang.String, edu.ucdavis.mw.myinfovault.service.apientity.ApiScope)
     */
    @Override
    public Boolean permits(String authHeader, ApiScope scopeRequired) throws CredentialException
    {
        return permits(authHeader, scopeRequired, null);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#permits(java.lang.String, edu.ucdavis.mw.myinfovault.service.apientity.ApiScope, java.util.Collection)
     */
    @Override
    public Boolean permits(String authHeader, ApiScope scopeRequired, Collection<MivRole> roles) throws CredentialException
    {
        ApiSecurityScheme scheme = ApiSecurityScheme.valueOf(StringUtils.substringBefore(authHeader, " "));
        String credentials = Base64.decode(StringUtils.substringAfter(authHeader, " "));
        String key = StringUtils.substringAfter(credentials, ":");
        String entityName = StringUtils.substringBefore(credentials, ":");
        //This authorizer is for an entity not in the database.
        if (entity.getEntityId() == -1) throw new CredentialException(INVALID_ENTITY);
        //Attempting to use incorrect authentication scheme.
        if (scheme != entity.getSecurityScheme()) throw new CredentialException(INVALID_SECURITY_SCHEME);
        //Attempting to use this authorizer for an entity other than the intended entity.
        if (!entityName.equalsIgnoreCase(entity.getEntityName())) throw new CredentialException(MessageFormat.format(INVALID_ENTITY_NAME, entityName, entity.getEntityName()));
        //The entity does not have the role needed to perform the action.
        if (!roles.contains(entity.getRole())) return false;
        //Attempting to use an invalid API secret.
        if (!isKeyValid(key)) return false;

        for (ApiScope scope : entity.getScopes(scopeRequired.getResource()))
        {
            if (scope.containsScope(scopeRequired)) return true;
        }

        return false;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#getRepresentedPerson()
     */
    @Override
    public MivPerson getRepresentedPerson()
    {
        return null;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#getEntity()
     */
    @Override
    public ApiEntity getEntity()
    {
        return entity;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#generateAPIKey()
     */
    @Override
    public String generateAPIKey()
    {
        try
        {
            MessageDigest hasher = MessageDigest.getInstance("SHA-512");
            String key = RandomStringUtils.randomAlphanumeric(KEY_LENGTH);

            entity.setSalt(RandomStringUtils.randomAlphanumeric(512));
            entity.setKeyHash(Hex.encodeHexString(hasher.digest((key + entity.getSalt()).getBytes())));

            return key;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new MivSevereApplicationError("SHA-512 doesn't exist! Should not happen.", e);
        }
    }

    private Boolean isKeyValid(String key)
    {
        if (key == null || key.isEmpty()) return false;

        String toHash = key + entity.getSalt();
        MessageDigest hasher;
        try
        {
            hasher = MessageDigest.getInstance("SHA-512");
            String Hash = Hex.encodeHexString(hasher.digest(toHash.getBytes()));
            return Hash.equals(entity.getKeyHash());
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new MivSevereApplicationError("SHA 512 doesn't exist! Should not happen.", e);
        }
    }
}
