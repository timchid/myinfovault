/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierArchiveServiceImpl.java
 */
package edu.ucdavis.mw.myinfovault.service.archive;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotDto;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode;
import edu.ucdavis.mw.myinfovault.util.FileUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PDFConcatenator;

/**
 * @author rhendric
 * @since MIV 3.1
 */
public class DossierArchiveServiceImpl implements DossierArchiveService
{

    private DossierCreatorService dossierCreatorService = null;
    private DossierService dossierService = null;
    private SnapshotService snapshotService = null;
    private UserService userService = null;
    private DossierDao dossierDao = null;
    private final Logger logger = Logger.getLogger(this.getClass());
    private boolean removeSnapshots = false;

    /**
     * Spring injected DossierCreatorService
     * @param dossierCreatorService
     */
    public void setDossierCreatorService(DossierCreatorService dossierCreatorService)
    {
        this.dossierCreatorService = dossierCreatorService;
    }

    /**
     * Spring injected DossierService
     * @param dossierService
     */
    public void setDossierService(DossierService dossierService)
    {
        this.dossierService = dossierService;
    }

    /**
     * Spring injected SnapshotService
     * @param snapshotService
     */
    public void setSnapshotService(SnapshotService snapshotService)
    {
        this.snapshotService = snapshotService;
    }

    /**
     * Spring injected UserService
     * @param userService
     */
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    /**
     * Spring injected DossierDao
     * @param dossierDao
     */
    public void setDossierDao(DossierDao dossierDao)
    {
        this.dossierDao = dossierDao;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.archive.DossierArchiveService#archiveDossier(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public String archiveDossier(Dossier dossier)
    {
        return archiveDossier(dossier, true);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.archive.DossierArchiveService#archiveDossier(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, boolean)
     */
    @Override
    public String archiveDossier(Dossier dossier, boolean sendToEDMS)
    {

        // Make sure an EDMS transfer is not in progress
        String errorMessage = this.validateEdmsProcessingState();
        if (errorMessage != null)
        {
            logger.error(errorMessage);
            return errorMessage;
        }

        final String archiveLocationMsg = sendToEDMS ? "EDMS/MIV" : "MIV";

        // See if we should remove the snapshots
        String configValue = MIVConfig.getConfig().getProperty("edms-config-removesnapshots");
        this.removeSnapshots = configValue != null && configValue.equalsIgnoreCase("true");

        // MivPerson archiving/routing this dossier
        MivPerson routingPerson = dossier.getRoutingPerson();

        final MivPerson candidate = dossier.getAction().getCandidate();
        if (candidate == null)
        {
            errorMessage = MessageFormat.format("Unable to {0} archive dossier {1} - Unable to retrieve MivPerson data.",
                                                archiveLocationMsg, dossier.getDossierId());
            logger.error(errorMessage);
            return errorMessage;
        }

        String displayName = candidate.getDisplayName();
        String principalName = candidate.getPersonId();

        // Init the archive log. We will keep a separate archive log for each dossier
        this.initArchiveLog(dossier.getArchiveDirectory().getAbsolutePath());

        logger.info("BEGIN " + archiveLocationMsg + " archiving initiated by " + routingPerson.getDisplayName()
                  + " (" + routingPerson.getPersonId() + ") for " + displayName + " (" + principalName + ") dossier "
                  + dossier.getDossierId());

        // Create the MIV Archive
        errorMessage = this.createMivArchive(dossier, candidate);

        if (errorMessage != null)
        {
            // Release the archive log.
            this.releaseArchiveLog();
            return errorMessage;
        }

        // The edmsFiles will is sent to the createEdmsArchive to be populated
        ArrayList<File>edmsFiles = new ArrayList<File>();
        if (sendToEDMS)
        {
            // Create the EDMS archive
            errorMessage = this.createEdmsArchive(dossier, candidate, edmsFiles);

            if (errorMessage != null)
            {
                // Release the archive log.
                this.releaseArchiveLog();
                return errorMessage;
            }
        }

        // Route the dosser to the final workflow node
        try
        {

            // Set the archived date
            dossier.setArchivedDate(new Date());
            // Approve the dossier to move to the next (final) workflow node.
            logger.info("Routing "+displayName+" ("+principalName+") dossier "+dossier.getDossierId()+" to the final workflow node.");

            WorkflowNode finalWorkflowNode = dossierService.getFinalWorkflowNode(dossier);

            // Send to the final workflow node loction
            dossierService.routeDossier(dossier, "Dossier EDMS/MIV archiving complete.", finalWorkflowNode.getNodeLocation(), dossierService.getCurrentWorkflowNode(dossier).getNodeLocation());

            // Create the 3 archive snapshots
            // MIV-3587 - Create an Archive for the candidate
            snapshotService.createArchiveSnapshot(dossier, MivRole.CANDIDATE, "Candidate_Archive");
            // MIV-3587 - reate a Snapshot for the Archive Admin which contains redacted documents
            snapshotService.createArchiveSnapshot(dossier, MivRole.ARCHIVE_USER, "Admin_Archive");
            // Create a Snapshot for the Archive Admin which contains non-redacted documents
            snapshotService.createArchiveSnapshot(dossier, MivRole.ARCHIVE_ADMIN, "Full_Archive");

            // Check to delete the snapshots for all of the previous workflow locations except for the final node
            // which are the archive snapshots that we want to keep
            if (removeSnapshots)
            {
                for (DossierLocation dossierLocation : dossierService.getPreviousWorkflowLocations(dossier))
                {
                    // Skip removing any snapshots at the final workflow node location
                    if (dossierLocation != finalWorkflowNode.getNodeLocation())
                    {
                        try
                        {
                            snapshotService.deleteSnapshots(dossier, dossierLocation);
                            logger.info("Removed "+dossierLocation+" snapshot for "+displayName+" ("+principalName+") dossier "+dossier.getDossierId());
                        }
                        catch (SQLException e)
                        {
                            logger.info("Unable to remove snapshots after archiving was complete for "+displayName+" ("+principalName+") dossier "+dossier.getDossierId(), e);
                        }
                    }
                }
            }

            // Now remove all uploads and clear files from the file system
            dossierService.removeUploadsByLocation(dossier);

            // If the upload database records have been removed for the dossier, go ahead and
            // remove the file system directory.
            if (dossierService.getUploadFiles(dossier).isEmpty())
            {

                File dossierDirectory = dossier.getDossierDirectory();
                try
                {
                    FileUtils.deleteDirectory(dossierDirectory);
                    logger.info("Removed file system directory for  dossier "+dossier.getDossierId());
                }
                catch (IOException e)
                {
                    logger.info("Failed to remove files from file system for dossier "+dossier.getDossierId()+" FAILED: "+e.getLocalizedMessage());
                }
            }

            logger.info("COMPLETED "+archiveLocationMsg+" archiving for "+displayName+" ("+principalName+") dossier "+dossier.getDossierId());

        }
        // In the case of an error, remove the EDMS archive files and the MIV Archive files
        catch (WorkflowException wfe)
        {
            errorMessage = "FAILED "+archiveLocationMsg+" archiving - Failed to route dossier "+dossier.getDossierId()+" to the final workflow node. EDMS/MIV archiving could not be completed, EDMS/MIV archive files will be removed.";
            logger.error(errorMessage, wfe);
            logger.info("Deleting any EDMS files which may have been created.");

            // Remove any EDMS files created for this dossier
            if (sendToEDMS)
            {
                for (File edmsFile : edmsFiles)
                {
                    logger.info("Deleting " + edmsFile);
                    if (!edmsFile.delete())
                    {
                        logger.error("Unable to remove EDMS file "+edmsFile.getAbsolutePath()+" for "+displayName+" ("+principalName+") dossier "+dossier.getDossierId()+".");
                    }
                }
            }

            // Remove any MIV archive files for this dossier
            File archiveLocation = dossier.getArchiveDirectoryByDocumentFormat(DocumentFormat.PDF);
            if (archiveLocation.delete())
            {
                logger.error("Unable to remove MIV archive directory "+archiveLocation.getAbsolutePath()+" for "+displayName+" ("+principalName+") dossier "+dossier.getDossierId()+".");
            }
        }
        // Release the archive log.
        this.releaseArchiveLog();
        return errorMessage;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.archive.DossierArchiveService#validateEdmsProcessingState()
     */
    @Override
    public String validateEdmsProcessingState()
    {
        String errorMessage = null;

        // Get the edms location
        String edmsPathName = this.getEdmsPathName();

        // Make sure the EDMS directory exists
        File edmsLocationPath = new File(edmsPathName);
        if (!edmsLocationPath.exists())
        {
            // If the path is not there and we can't create, error
            if (!edmsLocationPath.mkdirs())
            {
                errorMessage = "EDMS directory "+edmsPathName+" does not exist and could not be created. EDMS archiving cannot be performed at this time.";
                logger.error(errorMessage);
                return errorMessage;
            }
        }

        // Get the edms logical lock file name
        String edmsLockFileName = MIVConfig.getConfig().getProperty("edms-config-lockfile");
        if (edmsLockFileName == null)
        {
            edmsLockFileName = "OCPworkinprogress.txt";  // Default
        }

        // If this file is present, the transfer is in progress and EDMS archiving cannot continue.
        File edmsLockFile = new File(edmsPathName,edmsLockFileName);
        if (edmsLockFile.exists()) {
            // If the file exists we cannot allow archive at this time
            errorMessage = "EDMS processing is currently underway. EDMS archiving cannot be performed at this time.";
        }
        return errorMessage;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.archive.DossierArchiveService#getDocumentsToArchive(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public Map<String, List<File>> getDocumentsToArchive(Dossier dossier)
    {
        File dossierDirectory = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);

        // The output archiveFileMap
        Map<String, List<File>> archiveFileMap = new LinkedHashMap<String, List<File>>();

        // Get a list of documents the ARCHIVE_ADMIN role is allowed to view.
        List<DossierDocumentDto>dossierDocumentList = dossierCreatorService.getAuthorizedDocuments(MivRole.ARCHIVE_ADMIN);

        // Get the uploaded documents
        Map<String, List<UploadDocumentDto>> uploadDocumentMap = this.dossierDao.getDocumentUploadFiles(dossier);

        // Iterate through the list of files allowed to view building a list of those which are present to concatenate
        for (DossierDocumentDto dossierDocumentDto : dossierDocumentList)
        {
            // Check if this is a Candidate file, which is a data generated or "packet" file.
            if (dossierDocumentDto.isCandidateFile())
            {
                // Find all files which start with the dossierFileName and have a PDF extension
                final String dossierFileName = dossierDocumentDto.getFileName();
                File [] fileList = dossierDirectory.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return name.startsWith(dossierFileName) &&
                               name.endsWith("."+DocumentFormat.PDF.getFileExtension());
                    }});

                // Add all of the packet files found to the archive file list by EDMS code. Cannot be redacted
                this.addEdmsFiles(dossierDocumentDto.getCode(), Arrays.asList(fileList), archiveFileMap);
            }

            // Some data generated files of the same attribute type may also be uploadable, so we must still go through the
            // switch block.
            // Note that the data generated files, processed above, will be the first to the list by EDMS code.
            switch (dossierDocumentDto.getDossierAttributeType())
            {
                case DOCUMENTUPLOAD:
                    if (uploadDocumentMap != null
                            && uploadDocumentMap.containsKey(dossierDocumentDto.getDocumentId()+""))
                    {
                        for (UploadDocumentDto uploadDocument : uploadDocumentMap.get(dossierDocumentDto.getDocumentId()+""))
                        {
                            // An uploaded document may be redacted or nonredeacted, if it is redacted
                            // and there is a redactedCode defined, use the redacted code.
                            String edmsCode = uploadDocument.isRedacted()
                                    && dossierDocumentDto.getRedactedCode() != null ? dossierDocumentDto.getRedactedCode() : dossierDocumentDto.getCode();

                                    File uploadFile = uploadDocument.getFile();

                                    this.addFileByEdmsCode(edmsCode, uploadFile, archiveFileMap);
                        }
                    }
                    break;

                case DOCUMENT:
                    // Data based document (RAF, CDC, etc) - Cannot be redacted
                    this.addEdmsFiles(dossierDocumentDto.getCode(), getDocuments(dossier, dossierDocumentDto, dossierDirectory), archiveFileMap);
                    break;

                default:
                    break;
            }
        }
        return archiveFileMap;
    }


    /**
     * Add files to the archiveFileMap
     * @param edmsCode
     * @param files
     * @param archiveFileMap
     */
    private void addEdmsFiles(String edmsCode, List<File>files,  Map<String, List<File>> archiveFileMap)
    {
        for (File file : files)
        {
            addFileByEdmsCode(edmsCode, file, archiveFileMap);
        }
    }


    /**
     * Add an entry to the achiveFileMap
     * @param edmsCode
     * @param file
     * @param archiveFileMap
     */
    private void addFileByEdmsCode(String edmsCode, File file,  Map<String, List<File>> archiveFileMap)
    {
        // If there is no EDMS code, don't archive
        if (StringUtils.isEmpty(edmsCode))
        {
            return;
        }

        if (!archiveFileMap.containsKey(edmsCode))
        {
            archiveFileMap.put(edmsCode, new ArrayList<File>());
        }

        if (!archiveFileMap.get(edmsCode).contains(file))
        {
            archiveFileMap.get(edmsCode).add(file);
        }

        return;
    }


    /**
     * Get the documents for this DossierDocument by searching for files with the
     * appropriate name on the file system.
     *
     * @param dossier Dossier
     * @param dossierDocumentDto DTO representing a distinct dossier document
     * @param dossierDirectory Directory to search
     * @return List of files located for this document
     */
    private List<File> getDocuments(Dossier dossier,
            DossierDocumentDto dossierDocumentDto,
            File dossierDirectory)
    {
        // Find all files which start with the dossierFileName_<schoolId>_<departmentId> and have a PDF extension
        final String dossierFileName = dossierDocumentDto.getFileName();
        final File [] fileList = dossierDirectory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name)
            {
//                if (name.startsWith(dossierFileName + "_") && name.endsWith("."+DocumentFormat.PDF.getFileExtension()))
//                {
//                    return true;
//                }
//                return false;
                return name.startsWith(dossierFileName + "_") &&
                       name.endsWith("." + DocumentFormat.PDF.getFileExtension());
            }});

        // Return all of the files found
        return Arrays.asList(fileList);
    }


    /**
     * createEdmsArchive - create the EDMS archive
     * @param dossier
     * @param mivPerson
     * @param edmsFiles - List of edms files created. This list will be available to the caller.
     * @return String - null returned if no errors, otherwise error message is returned
     */
    private String createEdmsArchive(final Dossier dossier, final MivPerson mivPerson, final ArrayList<File>edmsFiles)
    {
        String errorMessage = null;
        final PDFConcatenator pdfConcatenator = new PDFConcatenator();
        final String edmsPathName = this.getEdmsPathName();
        final String displayName = mivPerson.getDisplayName();
        final String personUuid = mivPerson.getPersonId();

        logger.info("Creating EDMS archive for "+displayName+" ("+personUuid+") dossier "+dossier.getDossierId());

        // Get the MivUserInfo for this MivPerson
        MIVUserInfo mivUserInfo = userService.getMivUserByPersonUuid( personUuid );
        if (mivUserInfo == null)
        {
            errorMessage = "Unable to retrieve MivUserInfo data for " + displayName+ " ("+personUuid+") to EDMS archive dossier "
                    + dossier.getDossierId();
            logger.error(errorMessage);
            return errorMessage;
        }

        // Get the employee id
        final String employeeId = mivUserInfo.getEmployeeID();

        if (StringUtils.isBlank(employeeId))
        {
            errorMessage = "Unable to retrieve employeeId for " + displayName + " ("+personUuid+") to EDMS archive dossier "
                    + dossier.getDossierId();
            logger.error(errorMessage);
            return errorMessage;
        }

        // Get the files to archive which are present for this dossier
        Map<String, List<File>> archiveFileMap = this.getDocumentsToArchive(dossier);

        // Loop through the archiveFileMap and make sure none of these archive files already exist,
        // if they do, we cannot EDMS archive this dossier
        for (String edmsCode : archiveFileMap.keySet())
        {
            String edmsFileName = this.constructEDMSFilename(edmsCode, employeeId, dossier.getAction().getActionType().getEDMSDocumentKey());
            if (new File(edmsPathName, edmsFileName).exists())
            {
                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

                errorMessage = "EDMS archive files created today ("+dateFormat.format(new Date())+") are already present for " + displayName + " ("+personUuid+"). These files must be processed before an EDMS archive is attempted.";
                logger.error(errorMessage);
                return errorMessage;
            }
        }

        // Loop through the archiveFileMap and write the archive file for each code
        for (String edmsCode : archiveFileMap.keySet())
        {
            String edmsFileName = this.constructEDMSFilename(edmsCode, employeeId, dossier.getAction().getActionType().getEDMSDocumentKey());
            File edmsFile = new File(edmsPathName, edmsFileName);
            // Log the file names making up the contents of the EDMS file
            logger.info("======== BEGIN EDMS Code "+edmsCode+" ("+edmsFileName+") file list ========");
            for (File file : archiveFileMap.get(edmsCode))
            {
                logger.info(file.getAbsolutePath());
            }
            logger.info("======== END EDMS Code "+edmsCode+" file list ========");
            // Concatenate the list of files into a single pdf file
            if (!pdfConcatenator.concatenateFiles(archiveFileMap.get(edmsCode), edmsFile))
            {
                logger.error("Failed to create EDMS file "+edmsFile.getAbsolutePath());
                // If there is an error, remove any files which may have already been created
                for (File file : edmsFiles)
                {
                    file.delete();
                    logger.info("Deleted "+file);
                }
                errorMessage = "Failed to create EDMS archive file " + edmsFileName + " for " + displayName +" ("+personUuid+") dossier " + dossier.getDossierId()+".";
            }
            edmsFiles.add(edmsFile);
        }

        // Copy the edms files to the dossier archive directory
        // A failure at this step is not considered fatal since the EDMS files have been created and placed
        // in the directory for the transfer.
        if (errorMessage == null)
        {
            // Get/create the target dossier archive directory for the edms files.
            File archiveDirectory = new File(dossier.getArchiveDirectory(),"edms");
            // Create the directory for the EDMS files in the MIV archive directory
            if (archiveDirectory.exists() || archiveDirectory.mkdirs())
            {
                String msg = "Copying EDMS files for dossier "+dossier.getDossierId()+" to MIV archive.";
                logger.info(msg);

                for (File inputFile : edmsFiles)
                {
                    File outputFile = new File(archiveDirectory,inputFile.getName());

                    if (FileUtil.copyFile(inputFile, outputFile))
                    {
                        logger.info("Copy "+inputFile.getAbsolutePath()+" --> "+outputFile.getAbsolutePath());
                    }
                    else
                    {
                        logger.info("*** Failed to copy "+inputFile.getAbsolutePath()+" --> "+outputFile.getAbsolutePath());
                    }
                }
            }
            else
            {
                String msg = "Unable create a backup copy of EDMS files for dossier"+dossier.getDossierId()+" in the MIV archive."+
                " - Archive directory "+archiveDirectory.getAbsolutePath()+" is not present and could not be created. "+
                "However, the original EDMS files have been created and placed in the configured EDMS directory.";
                logger.warn(msg);
            }
        }

        return errorMessage;
    }


    /**
     * createMivArchive - create the MIV archive snapshot and copy the contents of the dossier
     * directory to the archive directory
     * @param dossier
     * @param mivPerson
     * @return String - null returned if no errors, otherwise error message is returned
     */
    private String createMivArchive(Dossier dossier, MivPerson mivPerson)
    {
        String errorMessage = null;
        String displayName = mivPerson.getDisplayName();
        String principalName = mivPerson.getPersonId();

        logger.info("Creating MIV archive for "+displayName+" ("+principalName+") dossier "+dossier.getDossierId());

        if (!this.copyDossierFilesToArchive(dossier))
        {
            errorMessage = "MIV Archive creation failed for "+displayName+" ("+principalName+") dossier "+dossier.getDossierId()+"! EDMS/MIV archiving could not be completed.";
            logger.error(errorMessage);
        }
        return errorMessage;
    }


    /**
     * constructEDMSFileName - Construct the EDMS file name in the form -
     * cccEEEEEEEEEMMddyyyy[edmsDocumentKey] where:
     *
     * ccc          = EDMS code - 3 digits
     * EEEEEEEEE    = Employee ID - 9 digits
     * MM           = Current Month
     * dd           = Current Day
     * yyyy         = Current Year
     * [edmsDocumentKey] = As defined in DossierActionType
     *
     * Example file name for a Disclosure Certificate for employee 123456789 in a Promotion created on 11/24/09:
     *
     * 29712345678911242009Promotion.pdf
     *
     * This file format must be followed exactly to be properly processed by the EDMS system.
     *
     * @param edmsCode
     * @param employeeId
     * @param edmsDocumentKey
     * @return String fileName
     */
    private String constructEDMSFilename(String edmsCode, String employeeId, String edmsDocumentKey)
    {
        // There is a proposed new EDMS replacement system
        // A test has been proposed which needs the spooled file names in a slightly different format.
        // This 'newEdms' flag controls that. Leave it at "false" unless you are involved with running the test.
        boolean newEdms = false;  // LEAVE THIS AS FALSE!!
        if (newEdms)
        {
            return edmsCode + "_" + employeeId + "_" + edmsFileDateFormatter.get().format(new Date()) + "_" + edmsDocumentKey;
        }
        else
        {
            DateFormat edmsDateFormat = new SimpleDateFormat("MMddyyyy");
            String edmsDateString = edmsDateFormat.format(new Date());
            return edmsCode + employeeId + edmsDateString + edmsDocumentKey + ".pdf";
        }
    }

    private final ThreadLocal<DateFormat> edmsFileDateFormatter =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return new SimpleDateFormat("MM-dd-yyyy");
        }
    };


    /**
     * getEdmsPathName
     * @return String edmsLocation
     */
    private String getEdmsPathName()
    {
        // Get the edms location
        String edmsPathName = MIVConfig.getConfig().getProperty("edms-config-location");
        if (edmsPathName == null)
        {
            edmsPathName = "/var/spool/transfer"; // Default
        }
        return edmsPathName;
    }


    /**
     * copyDossierFilesToArchive - Copy the contents of a user's Dossier directory to
     * the user's archive directory
     * @param dossier
     * @return if copy succeeds
     */
    private boolean copyDossierFilesToArchive(Dossier dossier)
    {
        // Get the target dossier archive directory...always PDF format.
        File archiveDirectory = dossier.getArchiveDirectoryByDocumentFormat(DocumentFormat.PDF);
        // Create the directory if not present
        if (!archiveDirectory.exists() && !archiveDirectory.mkdirs())
        {
            String msg = "Unable to copy dossier "+dossier.getDossierId()+" files to MIV archive."+
            " - Archive directory "+archiveDirectory.getAbsolutePath()+" is not present and could not be created.";
            logger.error(msg);
            return false;
        }

        // Get the source dossier directory...always PDF format.
        File dossierDirectory = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);


        // If we are removing snapshots, we also do not want to copy any to the archive location
        List <String> fileNamesToSkip = new ArrayList<String>();
        if (removeSnapshots)
        {
            // Get all of the snapshots for this dossier, we do not want to copy them
            List <SnapshotDto> snapshotDtoList = null;
            try
            {
                snapshotDtoList = snapshotService.getAllSnapshots(dossier);
                for (SnapshotDto snapshotDto : snapshotDtoList)
                {
                    fileNamesToSkip.add(snapshotDto.getSnapshotFileName());
                }
            }
            catch (WorkflowException e)
            {
                logger.info("Unable to retrieve snapshots to eliminate from copy to archive.", e);
            }
        }

        // Get all files in the dossier directory
        File [] files = dossierDirectory.listFiles();

        if (files != null && files.length > 0)
        {
            for (File inputFile : files)
            {
                // Copy if not in the skip list
                if (!fileNamesToSkip.contains(inputFile.getName()))
                {
                    File outputFile = new File(archiveDirectory, inputFile.getName());
                    if (!FileUtil.copyFile(inputFile, outputFile))
                    {
                        logger.warn("*** Failed to copy "+inputFile.getAbsolutePath()+" --> "+outputFile.getAbsolutePath());
                        return false;
                    }
                    logger.info("Copy "+inputFile.getAbsolutePath()+" --> "+outputFile.getAbsolutePath());
                }
            }
        }
        else
        {
            logger.warn("**** There were no files to copy for the archive!!! ****");
            return false;
        }

        return true;
    }



    /**
     * Initialize the archive log appender.
     * For convenience there will be a log kept with the archive for each user.
     * If initialization fails, the archive process will continue
     * since the system level logging will still be present and contain
     * the same log entries.
     * @param archiveLogPath
     */
    private void initArchiveLog(String archiveLogPath)
    {
        Appender archiveFileAppender;
        try
        {
            archiveFileAppender =
                new FileAppender(new PatternLayout("%d{MMM dd, yyyy hh:mm:ss a} %c %M%n%-5p [%t]: %m%n"),archiveLogPath+"/archive.log");
            archiveFileAppender.setName("archiveLog");
            logger.addAppender(archiveFileAppender);
        }
        catch (IOException e)
        {
            logger.error("Archive file appender initialization failed!");
        }
    }


    /**
     * Release the appender for the archive log
     */
    private void releaseArchiveLog()
    {
        logger.removeAppender("archiveLog");
    }

}
