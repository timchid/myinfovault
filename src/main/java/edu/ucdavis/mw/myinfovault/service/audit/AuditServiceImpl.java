/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuditServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.audit;

import java.util.List;

import edu.ucdavis.mw.myinfovault.dao.audit.AuditDao;
import edu.ucdavis.myinfovault.audit.AuditObject;

/**
 * AuditService Implementation class
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class AuditServiceImpl implements AuditService
{
    private AuditDao auditDao = null;

    /**
     * @param auditDao Spring-injected audit trail DAO
     */
    public void setAuditDao(AuditDao auditDao)
    {
        this.auditDao = auditDao;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.audit.AuditService#createAuditTrail(edu.ucdavis.myinfovault.audit.AuditObject)
     */
    @Override
    public void createAuditTrail(AuditObject auditObject)// throws MivAuditTrailException
    {
        auditDao.createAuditTrail(auditObject);

    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.audit.AuditService#getAuditTrailByEntityObjectId(int)
     */
    @Override
    public List<AuditObject> getAuditTrailByEntityObjectId(int entityObjectId)
    {
        auditDao.getAuditTrailByEntityObjectId(entityObjectId);
        return null;
    }
}
