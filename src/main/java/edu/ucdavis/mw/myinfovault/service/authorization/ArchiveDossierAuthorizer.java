/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewSnapshotAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**<p>
 * Authorizes the archiving of dossiers.
 *
 * @author rhendric
 * @since MIV 4.7.0
 */

public class ArchiveDossierAuthorizer extends RoleBasedAuthorizer
{
    public ArchiveDossierAuthorizer()
    {
        super(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF);
    }
}
