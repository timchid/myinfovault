/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignDeptChairAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Authorize people to assign a person the Department Chair role for a department.
 *
 * @author Stephen Paulsen
 * @since MIV 4.0
 */
public class AssignDeptChairAuthorizer extends SameScopeAuthorizer // RoleBasedAuthorizer // TODO: Maybe should extend the Same/Shared-ScopeAuthorizer instead
{
    public AssignDeptChairAuthorizer()
    {
        super(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF, MivRole.SCHOOL_STAFF);
    }
}
