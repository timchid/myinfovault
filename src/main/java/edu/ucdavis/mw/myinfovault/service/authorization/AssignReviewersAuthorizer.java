/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignReviewersAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.ARCHIVE;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.COMPLETE;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.DEPARTMENT;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.FEDERATION;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.FEDERATIONAPPEAL;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.FEDERATIONSENATEAPPEAL;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.PACKETREQUEST;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.POSTAPPEALSCHOOL;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.POSTAPPEALVICEPROVOST;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.POSTAUDITREVIEW;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.POSTSENATESCHOOL;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.POSTSENATEVICEPROVOST;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.READYFORPOSTREVIEWAUDIT;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.SCHOOL;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.SENATEAPPEAL;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.SENATEFEDERATION;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.SENATE_OFFICE;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.UNKNOWN;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierLocation.VICEPROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.text.MessageFormat;
import java.util.EnumSet;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.StringUtil;


/**<p>
 * Authorizes assignment of dossier reviewers by checking that the target dossier is
 * within the qualification scope of actor's appointments.</p>
 * <p>The qualification scope varies based on the actors role, for example a department administrator may
 * assign reviewers for a dossier at the department location within be same-school and same-department as any of
 * their appointments, while a school administrator may assign reviewers for dossiers at the department or school
 * location within the same-school (any department is acceptable) as any of their appointments.</p>
 * <p>Requires input of the <code>SCHOOL</code> and <code>DEPARTMENT</code> qualification parameter as well
 * as the SnapshotLocation in the permissionsDetails parameter</p>
 *
 * @author rhendric
 * @since 3.5.2
 */
public class AssignReviewersAuthorizer extends SameScopeAuthorizer
{
    //logger pattern for authorized and not authorized log messages
    private static final String LOG_PATTERN = "User {0} ({1}) is {2}AUTHORIZED to assign reviewers at the {3} location.";

    /** Roles that are permitted to assign Dossier reviewers */
    private static MivRole[] roles = { VICE_PROVOST_STAFF, VICE_PROVOST, DEPT_STAFF, SCHOOL_STAFF, SENATE_STAFF, SYS_ADMIN };

    /** Locations where reviews never take place. */
    private static final EnumSet<DossierLocation> noReviewLocations = EnumSet.of(PACKETREQUEST, READYFORPOSTREVIEWAUDIT, ARCHIVE, COMPLETE, UNKNOWN);

    /**
     * Permission detail key for the dossier location.
     */
    public static String DOSSIER_LOCATION = "DOSSIERLOCATION";

    /**
     * Get the roles that are allowed to assign reviewers to a Dossier.
     *
     * @return An array of the MivRoles that may assign reviewers.
     */
    public static MivRole[] getPermittedRoles()
    {
        return roles.clone();
    }

    /**
     * General permission check to determine if the actor has the permission to assign reviewers.
     *
     * @see SameScopeAuthorizer#hasPermission(MivPerson, String, AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        return person.hasRole(roles);
    }


    /**
     * <p>A department administrator may assign reviewers for a dossier at the department location within be same-school and
     * same-department as any of their appointments.
     * A school administrator may assign reviewers for a dossier at the school location within the same-school (any department is acceptable)
     * as any of their appointments.</p>
     *
     * @see SameScopeAuthorizer#isAuthorized(MivPerson, String, AttributeSet, AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person,
                                String permissionName,
                                AttributeSet permissionDetails,
                                AttributeSet qualification)
    {
        boolean isAuthorized = false;

        DossierLocation location = DossierLocation.mapWorkflowNodeNameToLocation(permissionDetails.get(DOSSIER_LOCATION));

        // If dossier is in a location where reviews are allowed, and
        // If role has permission
        if (!noReviewLocations.contains(location)
         && hasPermission(person, permissionName, permissionDetails))
        {
            // VP staff, and system administrators can see all dossiers
            if (person.hasRole(VICE_PROVOST_STAFF, SYS_ADMIN))
            {
                isAuthorized = true;
            }
            // VP can only assign at viceprovost locations
            else if (person.hasRole(VICE_PROVOST) &&
                    (location == VICEPROVOST ||
                    location == POSTSENATEVICEPROVOST ||
                    location == POSTAPPEALVICEPROVOST))
            {
                isAuthorized = true;
            }
            // Senate staff assigns reviewers only when the dossier is at the academic senate, post audit review and
            // any of the appeal locations.
            else if (person.hasRole(SENATE_STAFF)
                  && (
                          location == SENATE_OFFICE
                       || location == FEDERATION
                       || location == SENATEFEDERATION
                       || location == SENATEAPPEAL
                       || location == FEDERATIONAPPEAL
                       || location == FEDERATIONSENATEAPPEAL
                       || location == POSTAUDITREVIEW))
            {
                isAuthorized = true;
            }
            // School staff can see department and school dossiers qualified by school at the department,
            // school and federation school locations.
            else if (person.hasRole(SCHOOL_STAFF)
                  && hasSharedScope(person, qualification)
                  && (
                          location == DEPARTMENT
                       || location == SCHOOL
                       || location == POSTAUDITREVIEW
                       || location == POSTAPPEALSCHOOL
                       || location == POSTSENATESCHOOL))
            {
                isAuthorized = true;
            }
            // Department can see only department dossiers qualified by school and department at the department
            // location. Delegation Authority is not a consideration.
            else if (person.hasRole(DEPT_STAFF)
                  && hasSharedScope(person, qualification)
                  && location == DEPARTMENT)
            {
                isAuthorized = true;
            }
        }

        //log result
        log.debug(MessageFormat.format(LOG_PATTERN,
                                       new Object[] {
                                           person.getDisplayName(),
                                           person.getUserId(),
                                           !isAuthorized ? "** NOT ** " : StringUtil.EMPTY_STRING,
                                           location}));

        return isAuthorized;
    }
}
