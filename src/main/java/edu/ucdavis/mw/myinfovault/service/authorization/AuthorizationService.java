/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuthorizationService.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**<p>
 * This service provides operations for evaluating permissions and querying for permission data.
 * (this section copied from the KIM API PermissionService.java and helps to explain a little
 * of the Kuali Foundation view of what base permissions might be.)</p>
 *
 * <p>A permission is the ability to perform an action.  All permissions have a permission template.
 * Both permissions and permission templates are uniquely identified by a namespace code plus a name.
 * The permission template defines the course-grained permission and specifies what additional
 * permission details need to be collected on permissions that use that template.  For example, a
 * permission template might have a name of "Initiate Document" which requires a permission detail
 * specifying the document type that can be initiated.  A permission created from the "Initiate Document"
 * template would define the name of the specific Document Type that can be initiated as a permission
 * detail.</p>
 *
 * <p>The isAuthorized and isAuthorizedByTemplateName operations
 * on this service are used to execute authorization checks for a principal against a
 * permission.  Permissions are always assigned to roles (never directly to a principal or
 * group).  A particular principal will be authorized for a given permission if the permission
 * evaluates to true (according to the permission evaluation logic and based on any supplied
 * permission details) and that principal is assigned to a role which has been granted the permission.</p>
 *
 * (SDP examples added)
 * If we have a permission "Edit Appointment" that permission might require a permission detail
 * specifying what type of appointment may be edited. A user may be permitted to edit a joint
 * appointment in a given school and department, but may not edit a primary appointment there.
 * This might be used as<pre>
 *   // Provide the required detail of the type of appointment to edit
 *   AttributeSet details = new SimpleAttributeSet("appointment_type=joint");
 *   // Give the school and department of the appointment(s) user wants to edit
 *   AttributeSet scope = new SimpleAttributeSet("school="+schoolId, "department="+deptId);
 *
 *   boolean allowEdit = service.isAuthorized(userA, "Edit Appointment", details, scope);
 * </pre>
 *
 * @author Stephen Paulsen
 * @since MIV 3.5
 * @see org.kuali.rice.kim.service.IdentityManagementService
 */
public interface AuthorizationService
{
    /**
     *
     * @param person the MIV user to authorize
     * @param permissionName the base permission to be checked, e.g. VIEW, ADD, EDIT, DELETE, SIGN etc.
     * @param permissionDetails finer details of the permission to be checked, e.g. edit ACCOUNT, edit DEAN_RECOMMENDATION
     * @param qualification the qualifications/scope the person must have, e.g. in SCHOOL=18, DEPARTMENT=235
     * @return true if the user is authorized to perform the action, false if not authorized
     */
    public boolean isAuthorized(MivPerson person, String permissionName,
                                AttributeSet permissionDetails, AttributeSet qualification);


    /**
     *
     * @param person the MIV user to authorize
     * @param permissionName the base permission to be checked, e.g. VIEW, ADD, EDIT, DELETE, SIGN etc.
     * @param permissionDetails finer details of the permission to be checked, e.g. edit ACCOUNT, edit DEAN_RECOMMENDATION
     * @return true if the user is allowed to perform the action, false if not allowed
     */
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails);


    /**
     * Commonly used permission details for the <code>hasPermission()</code> <em>permissionDetails</em> parameters.
     *
     * @author Rick Hendricks
     * @since MIV 3.5
     */
    public static interface PermissionDetail
    {
        public static final String ACTOR_ID = "ACTOR_USERID";
        public static final String TARGET_ID = "TARGET_USERID";
        public static final String DOSSIER_OWNER_USERID = "DOSSIER_OWNER_USERID";
        public static final String TARGET_USER_ROLE = "TARGET_USER_ROLE";
        public static final String ACTOR_USER_ROLE = "ACTOR_USER_ROLE";
        /** Specify a type of document. Used primarily for Signature permissions. */
        public static final String DOCUMENT_TYPE = "DOCUMENT_TYPE";
        /** Specify a decision type; Dean, Vice Provost, Chancellor, etc. */
        public static final String DECISION_TYPE = "DECISION_TYPE";
    }

    /**
     * Commonly used qualifiers for the <code>isAuthorized()</code> scope <em>qualification</em> parameters.
     *
     * @author Stephen Paulsen
     * @since MIV 3.5
     */
    public static interface Qualifier
    {
        /** Specify a person's MIV school ID number */
        public static final String SCHOOL = "SchoolID";
        /** Specify a person's MIV department ID number */
        public static final String DEPARTMENT = "DepartmentID";
        /** Specify a person's MIV User ID number */
        public static final String USERID = "userId";
        /** Specify a person's primary MIV role. */
        public static final String ROLE = "role";

        /** Specify a school ID number for a Scope object. Replace the use of this with plain SCHOOL if possible. */
        public static final String SCOPE_SCHOOL     = "SchoolID";
        /** Specify a department ID number for a Scope object. Replace the use of this with plain DEPARTMENT if possible. */
        public static final String SCOPE_DEPARTMENT = "DepartmentID";
    }

}
