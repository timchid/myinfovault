/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditUserAuthorizer.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CANDIDATE;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_ASSISTANT;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.OCP_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;


/**<p>
 * Authorizes an action when the actor and the target user are within the same scope.</p>
 * <p>Scope varies based on the actors role, for example a department administrator has
 * a scope that must be same-school and same-department, while a school administrator
 * has a scope that must be same-school (any department is acceptable)</p>
 * <p>The role of the target must at or "lower" than the role of the actor,
 * for example a Dept. Admin may not edit a School Admin user.</p>
 * <p>Requires either that the <code>PRINCIPAL</code> of the person to be edited is
 * supplied in the qualification parameter, or all three of that person's
 * <code>SCHOOL</code>, <code>DEPARTMENT</code>, and <code>ROLE</code> are supplied.</p>
 * <p>Accepts a permiission detail of <code>SELF</code> set to any value to determine
 * if the actor is allowed to edit their own account.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 3.5.2
 */
public class EditUserAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    // Grants permission to edit a user when
    //  person.hasRole(DEPT_STAFF, SCHOOL_STAFF, VICE_PROVOST_STAFF, SYS_ADMIN)
    // Candidates, Deans, Dept Chairs, Dept Helpers, etc. can't edit users.

// This would override the SameScopeAuthorizer scoped and unscoped roles, because
// scope for editing a user can be different than scope for managing a dossier,
// but this doesn't have to override because SENATE_ADMINs aren't allowed to
// edit users anyway.
//    {
//        this.scopedRoles = new MivRole[] { DEPT_ASSISTANT, DEPT_STAFF, DEPT_CHAIR, SCHOOL_STAFF, DEAN, SENATE_STAFF };
//        this.unscopedRoles = new MivRole[] { SYS_ADMIN, VICE_PROVOST, VICE_PROVOST_STAFF };
//    }

    /** Roles that are allowed to edit other users. */
    private static final MivRole[] mayEditRoles = {
        SYS_ADMIN,
        VICE_PROVOST_STAFF,
        SENATE_STAFF,
        SCHOOL_STAFF,
        DEPT_STAFF
    }; // TODO: Externalize this list so changes can be made without editing code.

    @Override
    public boolean hasPermission(MivPerson actor, String permissionName, AttributeSet permissionDetails)
    {
        return actor.hasRole(mayEditRoles);
    }


    @Override
    public boolean isAuthorized(MivPerson actor, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        if (hasPermission(actor, permissionName, permissionDetails))
        {
            try
            {
                // Get the target user
                int userId = Integer.parseInt(qualification.get(Qualifier.USERID));

                // Don't let people edit themselves unless they are a SYS_ADMIN user.
                if (actor.getUserId() == userId && !actor.hasRole(SYS_ADMIN)) {
                    return false;
                }

                // allow editing of new person to be added
                if (userId < 0) return true;

                UserService userService = MivServiceLocator.getUserService();

                MivPerson targetUser =  userService.getPersonByMivId(userId);
                if (targetUser == null) {
                    log.warn("targetUser is null - User ID used was [" + userId + "]");
                    return false;
                }
                return isAuthorizedByRole(actor, targetUser.getPrimaryRoleType()) && hasSharedScope(actor, targetUser);
            }
            catch (NumberFormatException e)
            {
                log.error("UserID is not Number for attributeSet " + qualification, e);
            }
        }

        return false; // failed the hasPermission check
    }


    /**
     * Tests if an actor is allowed to edit a given role..
     * @param actor the MivPerson requesting the authority to edit a user.
     * @param targetRole an AssignedRole indicating the role of the user to be edited.
     * @return {@code true} if the editing should be allowed
     */
    @SuppressWarnings("unused")
    private boolean isAuthorizedByRole(MivPerson actor, AssignedRole targetRole)
    {
        return this.isAuthorizedByRole(actor, targetRole.getRole());
    }


    /**
     * Tests if an actor is allowed to edit a given role..
     * @param actor the MivPerson requesting the authority to edit a user.
     * @param targetRole an MivRole indicating the role of the user to be edited.
     * @return {@code true} if the editing should be allowed
     */
    private boolean isAuthorizedByRole(MivPerson actor, MivRole targetRole)
    {
        boolean authorized = false;

        List<MivRole> allowedTargetRoles = EditUserAuthorizer.getEditableRoles(actor.getPrimaryRoleType());
        authorized = allowedTargetRoles.contains(targetRole);

        return authorized;
    }


    /**
     * Get the list of roles that the given role is allowed to assign to people.
     * @param role the role that is editing a person.
     * @return the roles that this role is allowed to assign to people, or an empty list if there are none.
     */
    public static List<MivRole> getAssignableRoles(MivRole role)
    {
        return getAssignableRoles(role, false);
    }

    /**
     * Get the list of roles that the given role is allowed to assign to people.
     *
     * @param role the role that is editing a person.
     * @param onlyPrimaries if only {@link MivRole#getPrimaryRoles() primary type roles} should be returned
     * @return the roles that this role is allowed to assign to people, or an empty list if there are none.
     */
    public static List<MivRole> getAssignableRoles(MivRole role, boolean onlyPrimaries)
    {
        List<MivRole> roles = assignableRoles.get(role);

        if (roles == null) return Collections.emptyList();

        // only retain primary type roles
        if (onlyPrimaries)
        {
            roles = new ArrayList<MivRole>(roles);

            roles.retainAll(MivRole.getPrimaryRoles());
        }

        return Collections.unmodifiableList(roles);
    }


    /**
     * Get the list of roles that the given role is allowed to edit.
     * @param role the role that wants to edit a person.
     * @return the roles that this role is allowed edit, or an empty list if there are none.
     */
    @SuppressWarnings("unchecked") // for casting Collections to List<MivRole>
    public static List<MivRole> getEditableRoles(MivRole role)
    {
        List<MivRole> roles = editableRoles.get(role);
        return (List<MivRole>) (roles != null ?
                Collections.unmodifiableList(roles) :
                Collections.emptyList());
    }


    /**
     * Map a Role to the list of Roles an actor is allowed to assign to people.
     * TODO: Externalize these lists so changes can be made without editing code.
     */
    private static final Map<MivRole,List<MivRole>> assignableRoles = new EnumMap<MivRole,List<MivRole>>(MivRole.class);
    static {

        // Roles that a SYS_ADMIN is allowed to assign to people (Sys Admin)
        List<MivRole> l = new ArrayList<MivRole>();
        assignableRoles.put(SYS_ADMIN, l);
        l.add(SYS_ADMIN);
        l.add(VICE_PROVOST_STAFF);
        l.add(OCP_STAFF);
        l.add(SENATE_STAFF);
        l.add(SCHOOL_STAFF);
        l.add(DEAN);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);

        // Roles that VICE_PROVOST_STAFF are allowed to assign to people (MIV Admin)
        l = new ArrayList<MivRole>();
        assignableRoles.put(VICE_PROVOST_STAFF, l);
        l.add(VICE_PROVOST_STAFF);
        l.add(OCP_STAFF);
        l.add(SENATE_STAFF);
        l.add(SCHOOL_STAFF);
        l.add(DEAN);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);

        // Roles that SCHOOL_STAFF are allowed to assign to people (School/College Admin)
        l = new ArrayList<MivRole>();
        assignableRoles.put(SCHOOL_STAFF, l);
        l.add(SCHOOL_STAFF);
        l.add(DEAN);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);

        // Roles that DEPT_STAFF are allowed to assign to people (Dept. Admin)
        l = new ArrayList<MivRole>();
        assignableRoles.put(DEPT_STAFF, l);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);

        // SENATE_STAFF are only allowed to add other SENATE_STAFF
        l = new ArrayList<MivRole>(1);
        assignableRoles.put(SENATE_STAFF, l);
        l.add(SENATE_STAFF);
    }


    /**
     * Map a Role to the list of Roles an actor is allowed to edit.
     * TODO: Externalize these lists so changes can be made without editing code.
     */
    private static final Map<MivRole,List<MivRole>> editableRoles = new EnumMap<MivRole,List<MivRole>>(MivRole.class);
    static {

        // Roles that a SYS_ADMIN is allowed to edit (Sys Admin)
        List<MivRole> l = new ArrayList<MivRole>();
        editableRoles.put(SYS_ADMIN, l);
        l.add(SYS_ADMIN);
        l.add(VICE_PROVOST_STAFF);
        l.add(SENATE_STAFF);
        l.add(OCP_STAFF);
        l.add(SCHOOL_STAFF);
        l.add(DEAN);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);

        // Roles that VICE_PROVOST_STAFF are allowed to edit (MIV Admin)
        l = new ArrayList<MivRole>();
        editableRoles.put(VICE_PROVOST_STAFF, l);
        l.add(VICE_PROVOST_STAFF);
        l.add(SENATE_STAFF);
        l.add(SCHOOL_STAFF);
        l.add(OCP_STAFF);
        l.add(DEAN);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);

        // Roles that SCHOOL_STAFF are allowed to edit (School/College Admin)
        l = new ArrayList<MivRole>();
        editableRoles.put(SCHOOL_STAFF, l);
        l.add(SCHOOL_STAFF);
        l.add(DEAN);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);

        // Roles that DEPT_STAFF are allowed to edit (Dept Admin)
        l = new ArrayList<MivRole>();
        editableRoles.put(DEPT_STAFF, l);
        l.add(DEPT_STAFF);
        l.add(DEPT_CHAIR);
        l.add(DEPT_ASSISTANT);
        l.add(CANDIDATE);

        // Senate staff are allowed to edit only other SENATE_STAFF
        l = new ArrayList<MivRole>(1);
        editableRoles.put(SENATE_STAFF, l);
        l.add(SENATE_STAFF);
    }
}
