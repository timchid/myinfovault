/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ModifyAssignmentAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * @author Stephen Paulsen
 * @since MIV v4.0
 */
public class ModifyAssignmentAuthorizer extends SameScopeAuthorizer
{
    /** Roles that are allowed to modify assignments */
    private static final MivRole[] authorizedRoles = { DEPT_STAFF, SCHOOL_STAFF, VICE_PROVOST_STAFF, SYS_ADMIN };

    public ModifyAssignmentAuthorizer()
    {
        super(authorizedRoles);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        String targetUserRole = permissionDetails != null ? permissionDetails.get(PermissionDetail.TARGET_USER_ROLE) : "";

        // If the person being edited is a Candidate, all of the appointments are fair game for editing.
        if (StringUtils.hasText(targetUserRole) &&
            MivRole.valueOf(targetUserRole) == MivRole.CANDIDATE)
        {
            return true;
        }

        // ...otherwise do a Scope check
        return super.isAuthorized(person, permissionName, permissionDetails, qualification);
    }

}
