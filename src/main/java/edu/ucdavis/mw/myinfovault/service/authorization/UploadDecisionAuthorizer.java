/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UploadDecisionAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Authorizes the provost or chancellor proxy to upload a wet signature for a decision/recommendation.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.2
 */
public class UploadDecisionAuthorizer implements PermissionAuthorizer
{
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        // real, logged-in person
        int realPersonID = Integer.parseInt(permissionDetails.get(PermissionDetail.ACTOR_ID));

        // only provost/chancellor PROXIES may upload a decision
        return person.hasRole(MivRole.PROVOST, MivRole.CHANCELLOR)
            && person.getUserId() != realPersonID;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        // ID of the requested signer of the decision signature with which upload is associated
        int requestedSignerID = Integer.parseInt(permissionDetails.get(PermissionDetail.TARGET_ID));

        // has permission to upload decisions and is the requested signer
        return hasPermission(person, permissionName, permissionDetails)
            && person.getUserId() == requestedSignerID;
    }
}
