/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserActivityAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;

import java.util.Arrays;
import java.util.List;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * TODO: Add Javadoc
 * @author Stephen Paulsen
 * @since MIV 4.8
 */
public class UserActivityAuthorizer extends RoleBasedAuthorizer
{
    private static Integer[] allowedUserIds = {
        18099,
        18635,
        19012,
        20001,
        20729
    };
    private static List<Integer> allowedUsers = Arrays.asList(allowedUserIds);

    public UserActivityAuthorizer()
    {
        super(SYS_ADMIN);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.PermissionAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        return super.hasPermission(person, permissionName, permissionDetails) &&
               allowedUsers.contains(person.getUserId());
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.PermissionAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        return super.isAuthorized(person, permissionName, permissionDetails, qualification) &&
                allowedUsers.contains(person.getUserId());
    }

}
