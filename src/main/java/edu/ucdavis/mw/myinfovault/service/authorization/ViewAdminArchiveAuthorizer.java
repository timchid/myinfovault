/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewSnapshotAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;


/**<p>
 * Authorizes viewing of admin archives.</p>
 * <p>The qualification scope varies based on the actors role, for example a department administrator may
 * view any archive within be same-school and same-department as any of
 * their appointments, while a school administrator may view archives created at the department or school
 * location within the same-school (any department is acceptable) as any of their appointments.</p>
 * <p>Requires input of the <code>SCHOOL</code> and <code>DEPARTMENT</code> qualification parameter as well
 * as the SnapshotType and SnapshotLocation in the permissionsDetails parameter.</p>
 *
 * @author rhendric
 * @since MIV 3.5.2
 */
public class ViewAdminArchiveAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    /**
     * General permission check to determine if the actor has the permission to view admin archives.
     *
     * @see SameScopeAuthorizer#hasPermission(MivPerson, String, AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        return person.hasRole(DEPT_STAFF, SCHOOL_STAFF, VICE_PROVOST_STAFF, VICE_PROVOST, SYS_ADMIN);
    }


    /**
     * <p>A department administrator may view an admin archive within be same-school and
     * same-department as any of their appointments.
     * A school administrator may view an admin archive within the same-school (any department is acceptable)
     * as any of their appointments.</p>
     *
     * @see SameScopeAuthorizer#isAuthorized(MivPerson, String, AttributeSet, AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
    	boolean authorized = false;

        // If role has permission, check if authorized based on department and school
        if (this.hasPermission(person, permissionName, permissionDetails))
        {
        	int dossierOwner = Integer.parseInt(qualification.get(Qualifier.USERID));
        	int dossierViewer = person.getUserId();
        	
            // Vice provost, School staff and department staff can see all admin archives...no qualification
            if (person.hasRole(VICE_PROVOST_STAFF, VICE_PROVOST, MivRole.SCHOOL_STAFF, MivRole.DEPT_STAFF) &&
                qualification.get(Qualifier.ROLE).equals(MivRole.ARCHIVE_USER.name()))
            {
            	// Vice provost role can see all except their own (pimary role is CANDIDATE)
            	if (!(person.getPrimaryRoleType().equals(MivRole.CANDIDATE) && dossierOwner == dossierViewer))
            	{
            		authorized = true;
            	}
            }
        }
        return authorized;
    }

}
