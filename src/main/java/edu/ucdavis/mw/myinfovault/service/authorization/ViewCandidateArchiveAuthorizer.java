/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewSnapshotAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CANDIDATE;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;


/**<p>
 * Authorizes viewing of a candidate archive.</p>
 * <p>The qualification scope varies based on the actors role, for example a department administrator may
 * view any snapshot created at the department location within be same-school and same-department as any of
 * their appointments, while a school administrator may view snapshots created at the department or school
 * location within the same-school (any department is acceptable) as any of their appointments.</p>
 * <p>Requires input of the <code>SCHOOL</code> and <code>DEPARTMENT</code> qualification parameter as well
 * as the SnapshotType and SnapshotLocation in the permissionsDetails parameter.</p>
 *
 * @author rhendric
 * @since MIV 3.5.2
 */
public class ViewCandidateArchiveAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    /**
     * General permission check to determine if the actor has the permission to view the candidate archive.
     *
     * @see SameScopeAuthorizer#hasPermission(MivPerson, String, AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        return person.hasRole(VICE_PROVOST_STAFF, CANDIDATE);
    }

    /**
     *
     * @see SameScopeAuthorizer#isAuthorized(MivPerson, String, AttributeSet, AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {

        // If role has permission, check if authorized based on department and school
        if (this.hasPermission(person, permissionName, permissionDetails))
        {
            // Candidate can see Archive created for the CANDIDATE role
            if (person.hasRole(VICE_PROVOST_STAFF, CANDIDATE) &&
                qualification.get(Qualifier.ROLE).equals(MivRole.CANDIDATE.name()))
            {
                return true;
            }
        }
        return false;
    }

}
