/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewDossierAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**<p>
 * Authorizes the viewing of dossiers by checking that the target dossier is within the qualification
 * scope of actor's appointments.</p>
 * <p>The qualification scope varies based on the actors role, for example a department administrator may
 * view dossiers within be same-school and same-department as any of their appointments, while a
 * school administrator view dossiers within the same-school (any department is acceptable) as any of their
 * appointments.</p>
 * <p>Requires input of the <code>SCHOOL</code> and <code>DEPARTMENT</code> qualification parameter</p>
 *
 * @author rhendric
 * @since MIV 4.0
 */
public class ViewPostAuditDossiersAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    /** Roles that are allowed to view dossiers (Manage Open Action) */
    private static final MivRole[] mayViewPostAuditDossierRoles = {
        MivRole.SENATE_STAFF,
        MivRole.SCHOOL_STAFF,
        MivRole.VICE_PROVOST,
        MivRole.VICE_PROVOST_STAFF,
        MivRole.SYS_ADMIN,
        MivRole.DEAN,
    };


    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        // These roles may view a dossier.
        return person.hasRole(mayViewPostAuditDossierRoles);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     * A school administrator view a dossier within the same-school (any department is acceptable)
     * as any of their appointments.</p>
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        boolean authorized = false;

        // If role has permission, check if authorized based on department and school
        if (this.hasPermission(person, permissionName, permissionDetails))
        {
            int school = Scope.ANY;
            int department = Scope.ANY;

            if (qualification != null)
            {
                if (qualification.containsKey(Qualifier.SCHOOL) && qualification.get(Qualifier.SCHOOL) != null)
                {
                    school = Integer.parseInt(qualification.get(Qualifier.SCHOOL));
                }
            }

            Scope scope = new Scope(school,department);

            // Vice provost, Senate Staff can see all
            if (person.hasRole(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST, MivRole.VICE_PROVOST_STAFF, MivRole.SENATE_STAFF))
            {
                return true;
            }

            // Department Chair and Dean can see department dossiers only for those departments for which
            // the DEPT_CHAIR or DEAN role is scoped, but they may not see their own dossier, regardless of scope
            if (person.hasRole(MivRole.SCHOOL_STAFF, MivRole.DEAN))
            {
                // Cannot see their own dossier
                if (qualification.containsKey(Qualifier.USERID) &&
                   (person.getUserId() != Integer.parseInt(qualification.get(Qualifier.USERID))))
                {
                    return hasSharedScope(person, scope);
                }
            }
        }
        return authorized;
    }


    /**
     * Determine if <em>any</em> Scope the actor has matches the input Scope
     * @param actor
     * @param target
     * @return
     */
    @Override
    boolean hasSharedScope(final MivPerson actor, final Scope scope)
    {
        boolean authorized = false;

        // If the actor does not have the DEAN role, check the primary roles
        if (!actor.hasRole(MivRole.DEAN))
        {
                if (super.hasSharedScope(actor, scope))
                {
                    authorized = true;
                }
        }
        // If the actor has the DEAN role, then we cannot check the primary role and must check the
        // DEAN roles individually.
        else
        {
            for (AssignedRole actorAssignedRole : actor.getAssignedRoles())
            {
                if (actorAssignedRole.getRole() == MivRole.DEAN)
                {
                    if (actorAssignedRole.getScope().matches(scope))
                    {
                        authorized = true;
                        break;
                    }
                }
            }
        }

        return authorized;
    }

}
