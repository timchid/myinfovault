package edu.ucdavis.mw.myinfovault.service.biosketch;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.document.Document;

/**
 * @author dreddy
 * @since MIV 2.1
 */
public interface IDocumentCreatorService
{
    public List<Document> createBiosketch(MIVUserInfo mui, Biosketch biosketch);
    public List<Document> createPacket(MIVUserInfo mui, int collectionID);
    public List<Document> createPacket(MIVUserInfo mui, int collectionID, long packetId);
    public List<Document> viewPacket(MIVUserInfo mui, int collectionID);
    public List<Document> viewPacket(MIVUserInfo mui, int collectionID, long packetId);

}
