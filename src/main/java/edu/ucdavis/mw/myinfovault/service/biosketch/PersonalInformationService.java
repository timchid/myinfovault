/*
 * Copyright © 2007-2010 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonalInformationService.java
 */

package edu.ucdavis.mw.myinfovault.service.biosketch;

import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.dao.biosketch.PersonalDao;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Personal;


/**
 * Only the Biosketch should use this service; no new code.
 *
 * @author dreddy
 * @since MIV 2.1
 */
public class PersonalInformationService
{

//    private static final Log logger = LogFactory.getLog(PersonalInformationService.class);
    private static final Logger log = Logger.getLogger(PersonalInformationService.class);
    private PersonalDao personalDao;

    /**
     * @param personalDao
     */
    public void setPersonalDao(PersonalDao personalDao)
    {
        this.personalDao = personalDao;
    }

    /**
     * @param userId
     * @return
     */
    public Personal getPersonalInformation(int userId)
    {
        log.debug("***** Entering PersonalInformationService.getPersonInformation *****");
        Personal personal = null;

        try
        {
            personal = personalDao.findById(userId);
            personal.setPermanentAddress(personalDao.findAddressByTypeId(userId, 1));
            personal.setCurrentAddress(personalDao.findAddressByTypeId(userId, 2));
            personal.setOfficeAddress(personalDao.findAddressByTypeId(userId, 3));
            personal.setPermanentPhone(personalDao.findPhoneByTypeId(userId,1));
            personal.setHomePhone(personalDao.findPhoneByTypeId(userId,3));
            personal.setOfficePhone(personalDao.findPhoneByTypeId(userId,5));
            personal.setCellPhone(personalDao.findPhoneByTypeId(userId,7));
            personal.setPermanentFax(personalDao.findPhoneByTypeId(userId,2));
            personal.setOfficeFax(personalDao.findPhoneByTypeId(userId,6));
            personal.setCurrentFax(personalDao.findPhoneByTypeId(userId,4));
            personal.setEmail(personalDao.findEmailByUserId(userId));
            personal.setWebsite(personalDao.findUrlByUserId(userId));


            if (personal != null)
            {
                log.info("***** Person found *****");
                log.info("Person UserId is " + personal.getUserId());
                log.info("Personal DisplayName is " + personal.getDisplayName());
                log.info("Personal BirthDate is " + personal.getBirthDate());
                log.info("Person is " + personal.isCitizen());
                log.info("Person VisaType is " + personal.getVisaType());
                log.info("Person PermanentAddress is " + personal.getPermanentAddress());
            }
//            else
//            {
//                log.warn("Personal record not found for User ID " + userId);
//            }
        }
        catch (Exception ex)
        {
            // Catches any exceptions that has raised in PersonalInformationService
            //log.debug(ex.getMessage());
            log.warn("Unexpected Exception caught when getting Personal Information", ex);
        }

        log.debug("***** Leaving PersonalInformationService.getPersonInformation *****");
        return personal;
    }

}


