/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionService.java
 */

package edu.ucdavis.mw.myinfovault.service.decision;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;

import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.signature.SignableService;
import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Service for {@link Decision} type objects.
 *
 * @author Craig Gilmore
 * @since MIV 4.7
 * @param <T> Type of decision the implementation must be able to persist and request signatures
 */
public interface DecisionService extends SignableService<Decision>
{
    /**
     * Get the recommendations/decisions for the given dossier ordered via {@link DecisionComparator}.
     *
     * @param dossier dossier to which the decision belongs
     * @return matching decisions/recommendations
     */
    public SortedSet<Decision> getDecisions(Dossier dossier);

    /**
     * Get the decisions (not recommendations) on all actions for the given candidate ordered via {@link DecisionComparator}.
     *
     * @param candidate candidate for actions to which the decisions belong
     * @return matching decisions
     */
    public SortedSet<Decision> getDecisions(MivPerson candidate);

    /**
     * Get a VP/dean recommendation/decision for the given dossier, type, and scope.
     *
     * @param dossier dossier to which the decision belongs
     * @param documentType type of decision/recommendation
     * @param schoolId school ID to which the decision applies
     * @param departmentId department ID to which the decision applies
     * @return matching decision/recommendation or <code>null</code> if DNE
     */
    public Decision getDecision(Dossier dossier,
                                MivDocument documentType,
                                int schoolId,
                                int departmentId);

    /**
     * Get a VP/dean recommendation/decision for the given document ID.
     *
     * @param documentId record ID of the decision
     * @return matching decision/recommendation or <code>null</code> if DNE
     */
    public Decision getDecision(int documentId);

    /**
     * Remove all dossier decisions and their respective signatures and PDFs.
     *
     * @param dossier dossier from which to remove decisions
     */
    public void removeDecisions(Dossier dossier);

    /**
     * Remove all dossier decisions and their respective signatures and PDFs with exclusions.
     * @param dossier
     * @param excludeDocs
     */
    public void removeDecisions(Dossier dossier, List<MivDocument>excludeDocs);

    /**
     * Request decision signatures for the dossier in the given scope and type.
     *
     * @param dossier Dossier corresponding to the signatures
     * @param documentType type of decision to request
     * @param schoolId School ID number to request signatures
     * @param departmentId Department ID number to request signatures
     * @param realPerson Real, logged-in user
     * @return requested signatures
     */
    public Collection<MivElectronicSignature> requestSignatures(Dossier dossier,
                                                                MivDocument documentType,
                                                                int schoolId,
                                                                int departmentId,
                                                                MivPerson realPerson);

}
