/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCService.java
 */

package edu.ucdavis.mw.myinfovault.service.disclosurecertificate;

import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.signature.SignableService;

/**
 * Service for the recommended action form
 * @author Jed Whitten
 * @since MIV 3.0
 */
public interface DCService extends SignableService<DCBo>
{
    /**
     * Gets latest disclosure certificate that has signed signature.
     *
     * @param dossier dossier associated with the DC
     * @param schoolId DC school ID
     * @param departmentId DC department ID
     * @return Disclosure certificate or <code>null</code> if DNE.
     */
    public DCBo getDisclosureCertificateLatestSigned(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets latest disclosure certificate with unsigned signature.
     *
     * @param dossier dossier associated with the DC
     * @param schoolId DC school ID
     * @param departmentId DC school ID
     * @return candidate disclosure certificate or <code>null</code> if DNE.
     */
    public DCBo getDisclosureCertificateLatestRequested(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets latest disclosure certificate that has a signature (signed or otherwise)
     * or if none, the latest draft.
     *
     * @param dossier dossier associated with the DC
     * @param schoolId DC school ID
     * @param departmentId DC school ID
     * @return candidate disclosure certificate
     */
    public DCBo getDisclosureCertificate(Dossier dossier, int schoolId, int departmentId);

    /**
     * Gets DC business object from DAO. Returns <code>null</code> if DNE.
     *
     * @param recordId ID of the disclosure certificate
     * @return Disclosure certificate business object or <code>null</code> if DNE.
     */
    public DCBo getDisclosureCertificate(int recordId);

    /**
     * Gets latest DC draft (DC with no signature/request made for it) from DAO.
     * <ul>
     * <li>If no draft exists, the latest signed/requested DC is copied</li>
     * <li>If no signed/requested DC exists, a new one is created</li>
     * </ul>
     *
     * @param dossier dossier associated with the DC
     * @param schoolId
     * @param departmentId
     * @return Disclosure certificate business object
     */
    public DCBo getDisclosureCertificateDraft(Dossier dossier,
                                              int schoolId,
                                              int departmentId);

    /**
     * Request disclosure certificate signature for the given DC.
     * Former signature/requests are remove in favor of the new request.
     *
     * @param dc disclosure certificate to request signature
     * @param realPerson real, logged-in person making the signature request
     * @return requested signature
     */
    public MivElectronicSignature requestSignature(DCBo dc, MivPerson realPerson);

    /**
     * {@link #deleteDc(DCBo) Deletes} candidate disclosure certificates for the given dossier
     * and scope and re-persists the latest entries as disclosure certificate drafts.
     *
     * @param dossier Dossier corresponding to the signatures
     * @param schoolId School ID of CDC to reset
     * @param departmentId Department ID of CDC to reset
     * @param realPerson Real, logged-in user
     */
    public void resetDisclosureCertificates(Dossier dossier,
                                            int schoolId,
                                            int departmentId,
                                            MivPerson realPerson);

    /**
     * Gets the disclosure certificates status as it pertains to it's existence and signature.
     *
     * @param dossier Dossier for the DC
     * @param schoolId School ID for the DC
     * @param departmentId Department ID for the DC
     * @return DC status
     */
    public DCStatus getStatus(Dossier dossier, int schoolId, int departmentId);

    /**
     * Removes the disclosure certificate database record and associated signature requests.
     * If the candidate disclosure certificate is signed, the signature record is archived
     * instead. In both cases the disclosure certificate PDF is deleted.
     *
     * @param dc disclosure certificate
     */
    public void deleteDc(DCBo dc);
}
