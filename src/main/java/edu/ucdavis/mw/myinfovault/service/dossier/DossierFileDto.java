/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierFileDto.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.File;
import java.io.Serializable;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.service.signature.SignatureStatus;
import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * This class represents a dossier document file
 * @author rhendric
 * @since MIV ?.?
 */
public class DossierFileDto implements Serializable
{
    private static final long serialVersionUID = 1L;

    private int recordId;
    private String tableName = null;
    private int documentId;
    private MivDocument documentType;
    private int schoolId;
    private int departmentId;
    private long dossierId;
    private File filePath = null;
    /** Indicates if this is a file/document type that can have both redacted and a non-redacted versions. */
    private boolean redactable = false;
    private boolean redacted = false;
    /** Indicates if this is a file/document type that can be signed. */
    private boolean signable = false;
    private SignatureStatus signatureStatus = SignatureStatus.MISSING;
    private String fileUrl = null;
    private String fileDescription = null;
    private String fileLabel = null;

    public void setRecordId(int recordId)
    {
        this.recordId = recordId;
    }

    public int getRecordId()
    {
        return recordId;
    }

    public void setId(int recordId)
    {
        this.recordId = recordId;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public int getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(int docuemntId)
    {
        this.documentId = docuemntId;
    }
    
    public MivDocument getDocumentType()
    {
        return documentType;
    }

    public void setDocumentType(MivDocument documentType)
    {
        this.documentType = documentType;
    }

    public int getSchoolId()
    {
        return schoolId;
    }

    public void setSchoolId(int schoolId)
    {
        this.schoolId = schoolId;
    }

    public int getDepartmentId()
    {
        return departmentId;
    }

    public void setDepartmentId(int departmentId)
    {
        this.departmentId = departmentId;
    }

    public long getDossierId()
    {
        return dossierId;
    }

    public void setDossierId(long dossierId)
    {
        this.dossierId = dossierId;
    }

    public File getFilePath()
    {
        return filePath;
    }

    public void setFilePath(File filePath)
    {
        this.filePath = filePath;
    }

    public DossierFileDto setRedactable(boolean redactable)
    {
        this.redactable = redactable;
        return this;
    }

    public boolean isRedactable()
    {
        return this.redactable;
    }

    public boolean isRedacted()
    {
        return redacted;
    }
    public boolean getIsRedacted()
    {
        return isRedacted();
    }

    public void setRedacted(boolean redacted)
    {
        this.redacted = redacted;
    }

    public String getFileUrl()
    {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl)
    {
        this.fileUrl = fileUrl;
    }

    /**
     * 
     * @param fileDescription description of this document
     */
    public void setFileDescription(String fileDescription)
    {
        this.fileDescription = fileDescription;
    }

    /**
     * @return description of this document if available, else the file label
     */
    public String getFileDescription()
    {
        return StringUtils.hasText(fileDescription)
             ? fileDescription
             : fileLabel;
    }
    
    /**
     * @param fileLabel short file label
     */
    public void setFileLabel(String fileLabel)
    {
        this.fileLabel = fileLabel;
    }
    
    /**
     * @return short file label
     */
    public String getFileLabel()
    {
        return this.fileLabel;
    }

    public void setSignable(boolean signable)
    {
        this.signable = signable;
    }

    public boolean isSignable()
    {
        return signable;
    }

    /**
     * @return if signature for the dossier file is both signed and valid
     */
    public boolean isSigned()
    {
        return signatureStatus == SignatureStatus.SIGNED;
    }

    /**
     * @param signatureStatus dossier file signature status
     */
    public void setSignatureStatus(SignatureStatus signatureStatus)
    {
        this.signatureStatus = signatureStatus;
    }

    /**
     * @return dossier file signature status
     */
    public SignatureStatus getSignatureStatus()
    {
        return signatureStatus;
    }
}
