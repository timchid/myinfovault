/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: WorkflowException.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

/**
 * TODO: Add Javadoc
 *
 * @author Stephen Paulsen
 */
public class WorkflowException extends Exception
{
    private static final long serialVersionUID = 201206011710L;


    /**
     *
     */
    public WorkflowException()
    {
        super();
    }


    /**
     * @param message
     */
    public WorkflowException(String message)
    {
        super(message);
    }


    /**
     * @param cause
     */
    public WorkflowException(Throwable cause)
    {
        super(cause);
    }


    /**
     * @param message
     * @param cause
     */
    public WorkflowException(String message, Throwable cause)
    {
        super(message, cause);
    }

}
