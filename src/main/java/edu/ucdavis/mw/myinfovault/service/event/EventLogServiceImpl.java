/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventLogServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.event;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.dao.event.EventLogDao;
import edu.ucdavis.mw.myinfovault.domain.event.EventLogEntry;
import edu.ucdavis.mw.myinfovault.events2.DisclosureEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierEventStub;
import edu.ucdavis.mw.myinfovault.events2.DossierReviewEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierUploadEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierVoteEvent;
import edu.ucdavis.mw.myinfovault.events2.ErrorEvent;
import edu.ucdavis.mw.myinfovault.events2.EventCategory;
import edu.ucdavis.mw.myinfovault.events2.MivEvent;
import edu.ucdavis.mw.myinfovault.events2.PacketEvent;
import edu.ucdavis.mw.myinfovault.events2.RecordChangeEvent;
import edu.ucdavis.mw.myinfovault.events2.ReleaseEvent;
import edu.ucdavis.mw.myinfovault.events2.RequestEvent;
import edu.ucdavis.mw.myinfovault.events2.RouteEvent;
import edu.ucdavis.mw.myinfovault.events2.SignatureEvent;
import edu.ucdavis.mw.myinfovault.events2.UserEvent;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;


/**
 * Governs the use of {@link EventLogEntry}.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class EventLogServiceImpl implements EventLogService
{
    private final EventLogDao eventLogDao;
    //private final UserService userService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());


    /**
     * Create the event log service bean.
     *
     * @param eventLogDao Spring-injected event log DAO
     * @param userService Spring-injected MIV user service
     */
    public EventLogServiceImpl(EventLogDao eventLogDao, UserService userService)
    {
        this.eventLogDao = eventLogDao;
        //this.userService = userService;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.event.EventLogService#getEntries(long)
     */
    @Override
    public List<EventLogEntry> getEntries(long dossierId)
    {
        try
        {
            return eventLogDao.getEntries(dossierId);
        }
        catch (DataAccessException e)
        {
            return Collections.emptyList();//FIXME: what should be done here?
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.event.EventLogService#logEvent(edu.ucdavis.mw.myinfovault.events2.MivEvent)
     */
    @Override
    public void logEvent(MivEvent event)
    {
        MivPerson realPerson = event.getRealPerson();
        MivPerson shadowPerson = event.getShadowPerson();

        EventLogEntry logEntry = new EventLogEntry(
                    event.getEventId().getBytes(),
                    event.getEventTitle(),
                    realPerson,
                    shadowPerson != null ? shadowPerson : realPerson,
                    event.getEventTime().getTime(),
                    event.getComments(),
                    buildCategoryMap(event),
                    event.getParentEvent() == null ? null : event.getParentEvent().getEventId().getBytes());

        long dossierId = 0;
        if (event instanceof DossierEvent)
        {
            dossierId = ((DossierEvent)event).getDossierId();
        }

        eventLogDao.persistEntry(dossierId, logEntry);
    }


    /**
     * Build the details for the MivEvent based on the categories which
     * are associated with a specific event implementation.
     *
     * @param event
     * @return eventCategoryMap
     */
    private Map<EventCategory, Object> buildCategoryMap(MivEvent event)
    {
        Map<EventCategory, Object> eventCategoryMap = new LinkedHashMap<EventCategory, Object>();

        for (EventCategory category: event.getCategories())
        {
            Object details = null;

            switch (category)
            {
                case DOSSIER:
                    details = ((DossierEventStub)event).getDossier().getAction().getDescription();
                    break;

                case ROUTE:
                    details = ((RouteEvent)event).getRoute();
                    break;

                case DISCLOSURE:
                    details = ((DisclosureEvent)event).getDisclosure().getSchoolName()
                    + ", "
                    + ((DisclosureEvent)event).getDisclosure().getDepartmentName();
                    break;

                case REVIEW:
                    details = "Review "
                            + ((DossierReviewEvent)event).getReviewStatus()
                            + " at "
                            + ((DossierReviewEvent)event).getReviewLocation();
                    break;

                case VOTE:
                    details = "Voting "
                            + ((DossierVoteEvent)event).getVoteStatus()
                            + " at "
                            + ((DossierVoteEvent)event).getVoteLocation();
                    break;

                case SIGNATURE:
                    details = ((SignatureEvent)event).getSignature().getDocument().getDocumentType();
                    break;

                case REQUEST:
                    details = MivServiceLocator.getUserService().getPersonByMivId(((RequestEvent)event).getRequestedUserId());
                    break;

                case HOLD:
                case RELEASE:
                    details = ((ReleaseEvent)event).getLocation();
                    break;

                case UPLOAD:
                    details = ((DossierUploadEvent)event).getUploadDocument().getUploadName()
                            + " at "
                            + ((DossierUploadEvent)event).getUploadDocument().getUploadLocation();
                    break;

                case USER:
                    details = ((UserEvent)event).getTargetUser();
                    break;

                case ERROR:
                    details = ((ErrorEvent)event).getException();
                    break;

                case RECORDCHG:
                    details = ((RecordChangeEvent)event).getAction()+" "+((RecordChangeEvent)event).getTableName()+" record "+((RecordChangeEvent)event).getRecordId()+" for user "+((RecordChangeEvent)event).getRealUserId();
                    break;

                case PACKET:
                    details = "Packet "+((PacketEvent)event).getAction();
                    break;

                default:
                    log.info("Unhandled event category [{}] while building Category Map", category);
                    break;
           }

           eventCategoryMap.put(category, details);
        }

        return eventCategoryMap;
    }
}
