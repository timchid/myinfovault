/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupCreatorFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.service.SqlAwareFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Filter out a collection of groups that the acting person has created.
 *
 * @author Craig Gilmore
 * @since MIV 4.3
 */
public class GroupCreatorFilter extends SqlAwareFilterAdapter<Group>
{
    private final static String PATTERN = "Groups.CreatorPersonUUID = ?";

    private MivPerson actingPerson;

    /**
     * @param actingPerson The switched-to, acting person
     */
    public GroupCreatorFilter(MivPerson actingPerson)
    {
        super(PATTERN, actingPerson.getPersonId());

        this.actingPerson = actingPerson;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean include(Group item)
    {
        return item.getCreator().equals(actingPerson);
    }
}
