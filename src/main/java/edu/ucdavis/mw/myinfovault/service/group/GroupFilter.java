/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import java.util.Collection;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filters a collection of groups by another collection of groups. A group passing
 * this filter will not have a membership that is a sub set of any of the given groups.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupFilter extends SearchFilterAdapter<Group>
{
    /**
     * @param groups The collection of groups by which to filter
     */
    public GroupFilter(Collection<Group> groups)
    {
        //"and" to this filter a group member filter for each given group
        for(Group group : groups)
        {
            this.and(new GroupMemberFilter(group.getMembers()));
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(Group item)
    {
        //filter only by the "and-ed" group member filters
        return true;
    }
}
