/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.dao.group.GroupDao;
import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupType;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.SqlAwareFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;

/**
 * Governs the use of MIV groups.
 *
 * @author Craig Gilmore
 * @since MIV 4.3
 */
public class GroupServiceImpl implements GroupService
{
    private static final Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);


    private GroupDao groupDao;
    private AuthorizationService authorizationService;

    /**
     * Creates a Group service object with support from the Group DAO and authorization service.
     *
     * @param groupDao group DAO
     * @param authorizationService MIV authorization service
     */
    public GroupServiceImpl(GroupDao groupDao,
                            AuthorizationService authorizationService)
    {
        this.groupDao = groupDao;
        this.authorizationService = authorizationService;

        // Refresh/initialize the cache on instantiation
        this.groupDao.refreshCache();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean persist(MivPerson targetPerson,
                           Group group) throws SecurityException
    {
        logger.info("Persisting group...");

        //continue with non null values only
        if (group != null && targetPerson != null)
        {
            //user authorized to edit?
            authorize(targetPerson,
                      Permission.EDIT_GROUP,
                      group.getId());


            try
            {
                //continue, persist group
                return groupDao.persist(group,
                                        targetPerson.getUserId());
            }
            catch (DataAccessException e)
            {
                logger.error("Unable to issue update on group named '" + group.getName() + "', for user ID '" + targetPerson.getUserId() + "'", e);
            }
            finally
            {
                // log group
                logger.info("\nTarget person:\t{}\n{}", targetPerson, group);
            }
        }

        return false;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Group copy(MivPerson targetPerson,
                      GroupType type,
                      Set<AssignedRole> assignedRoles,
                      Group group) throws SecurityException
    {
        //proceed only if group is not null
        if (group == null) return null;

        //authorize a view on this group
        authorize(targetPerson,
                  Permission.VIEW_GROUP,
                  group.getId());

        //create a new group object
        Group copy = group.copy(type, assignedRoles);

        //save copied group
        persist(targetPerson, copy);

        return copy;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Group copy(MivPerson targetPerson,
                      GroupType type,
                      Set<AssignedRole> assignedRoles,
                      int groupId) throws SecurityException
    {
        return copy(targetPerson,
                    type,
                    assignedRoles,
                    getGroup(groupId));
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Group> copy(MivPerson targetPerson,
                           GroupType type,
                           Set<AssignedRole> assignedRoles,
                           int...groupIds)
    {
        return copy(targetPerson,
                    type,
                    assignedRoles,
                    getGroups(groupIds));
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Group> copy(MivPerson targetPerson,
                           GroupType type,
                           Set<AssignedRole> assignedRoles,
                           Set<Group> groups)
    {
        Set<Group> copies = new HashSet<Group>(groups.size());

        for (Group group : groups)
        {
            try
            {
                //add the copied group to the copied set
                copies.add(copy(targetPerson,
                                type,
                                assignedRoles,
                                group));
            }
            catch (SecurityException e)
            {
                //log permission denied exceptions
                logger.error("Permission was denied", e);
            }
        }

        //remove null
        copies.remove(null);

        return copies;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Group delete(MivPerson targetPerson,
                        int groupId) throws SecurityException
    {
        return delete(targetPerson,
                      getGroup(groupId));


    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Group> delete(MivPerson targetPerson,
                             int...groupIds)
    {
        return delete(targetPerson,
                      getGroups(groupIds));
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Group delete(MivPerson targetPerson,
                        Group group) throws SecurityException
    {
        logger.info("Deleting group...");

        //continue with non null values only
        if (group != null && targetPerson != null)
        {
            //user authorized to edit?
            authorize(targetPerson,
                      Permission.EDIT_GROUP,
                      group.getId());

            try
            {
                groupDao.delete(group);
            }
            catch (DataAccessException e)
            {
                logger.error("Unable to delete group '" + group.getId() + "'", e);

                //no group deleted, return null
                return null;
            }
            finally
            {
                // log group
                logger.info("\nTarget person:\t{}\n{}", targetPerson, group);
            }
        }

        //return the deleted group
        return group;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Group> delete(MivPerson targetPerson,
                             Collection<Group> groups)
    {
        //the set of groups deleted
        Set<Group> deleted = new HashSet<Group>(groups.size());

        for (Group group : groups)
        {
            try
            {
                //add the deleted group to the deleted set
                deleted.add(delete(targetPerson,
                                   group));
            }
            catch (SecurityException e)
            {
                //log permission denied exceptions
                logger.error("Permission was denied", e);
            }
        }

        //return the deleted set
        return deleted;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Group getGroup(int groupId)
    {
        try
        {
            return groupDao.get(groupId);
        }
        catch (DataAccessException e)
        {
            logger.error("Unable to retrieve group with ID '" + groupId + "'", e);
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Group> getGroups(int... groupIds)
    {
        Set<Group> groups = new HashSet<Group>();

        //get group for each group ID
        for (int groupId : groupIds)
        {
            groups.add(getGroup(groupId));
        }

        //remove null
        groups.remove(null);

        //return corresponding groups
        return groups;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Group> getGroups(SqlAwareFilterAdapter<Group> filter,
                                SearchFilter<Group>...additionalFilters)
    {
        //AND additional filters
        for (SearchFilter<Group> additionalFilter : additionalFilters)
        {
            filter.and(additionalFilter);
        }

        try
        {
            //get groups passing the filters
            return filter.apply(new HashSet<Group>(groupDao.get(filter)), true);
        }
        catch (DataAccessException e)
        {
            logger.warn("Unable to retrieve groups", e);
        }

        return Collections.emptySet();
    }

    //throws security exception if user is not authorized to edit the group
    private void authorize(MivPerson targetPerson,
                           String permission,
                           int groupId) throws SecurityException
    {
        AttributeSet permissionDetails = new AttributeSet();

        //put group ID in permission details
        permissionDetails.put(PermissionDetail.TARGET_ID,
                              Integer.toString(groupId));

        if (!authorizationService.isAuthorized(targetPerson,
                                               permission,
                                               permissionDetails,
                                               null))
        {
            //throw permission denied exception
            throw new SecurityException("User " + targetPerson.getUserId() + " is not authorized to " + permission + " " + groupId);
        }
    }

    /**
     * Refresh the group cache
     */
    @Override
    public void refresh()
    {
        logger.info("Refreshing the group cache...");
        groupDao.refreshCache();
    }

}
