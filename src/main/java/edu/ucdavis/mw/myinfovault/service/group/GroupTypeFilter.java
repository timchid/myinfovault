/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupViewFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupType;
import edu.ucdavis.mw.myinfovault.service.SqlAwareFilterAdapter;

/**
 * Filters MIV groups by group type.
 *
 * @author Craig Gilmore
 * @since MIV 4.3
 */
public class GroupTypeFilter extends SqlAwareFilterAdapter<Group>
{
    private final static String PATTERN = "Groups.TypeID = ?";

    private GroupType type;

    /**
     * @param type The type to be associated with an MIV group
     */
    public GroupTypeFilter(GroupType type)
    {
        super(PATTERN, type.getId());

        this.type = type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean include(Group item)
    {
        return item.getType() == type;
    }
}
