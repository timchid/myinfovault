/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LdapPersonDataSource.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.ldap.NameNotFoundException;

import edu.ucdavis.iet.commons.ldap.domain.LdapMailListing;
import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.service.LdapMailListingService;
import edu.ucdavis.iet.commons.ldap.service.LdapPersonService;

/**
 * Defines an LDAP person data source.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class LdapPersonDataSource implements PersonDataSource
{
    private final LdapPersonService ldapPersonService;
    private final LdapMailListingService ldapMailListingService;


    /**
     * Create the LDAP person data source bean.
     *
     * @param ldapPersonService Spring-injected IET Commons LDAP person service
     * @param ldapMailListingService Spring-injected IET Commons LDAP mail listing service
     */
    public LdapPersonDataSource(LdapPersonService ldapPersonService,
                                LdapMailListingService ldapMailListingService)
    {
        this.ldapPersonService = ldapPersonService;
        this.ldapMailListingService = ldapMailListingService;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromId(java.lang.String)
     */
    @Override
    public PersonDTO getPersonFromId(String personId)
    {
        try
        {
            return personId != null ? getPerson(ldapPersonService.findByUniversalUserId(personId)) : null;
        }
        catch (NameNotFoundException nnf)
        {
            return null;
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromPrincipal(java.lang.String)
     */
    @Override
    public PersonDTO getPersonFromPrincipal(String principalName)
    {
        try
        {
            return principalName != null ? getPerson(ldapPersonService.findByUserId(principalName)) : null;
        }
        catch (NameNotFoundException nnf)
        {
            return null;
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromEmail(java.lang.String)
     */
    @Override
    public Collection<PersonDTO> getPersonFromEmail(String email)
    {
        List<PersonDTO> people = new ArrayList<>();

        HashMap<String, String> args = new HashMap<>();
        args.put("mail", email);

        HashSet<String> includedPeople = new HashSet<>();

        /*
         * Retrieve each person that lists this email address.
         */
        try
        {
            //Not every person in LDAP will be found by the LdapMailListingService
            //Some people don't have a Listing entry
            for (LdapPerson person : ldapPersonService.findByCriteria(args))
            {
                people.add(getPerson(person));
                includedPeople.add(person.getUcdPersonUUID());
            }
        }
        catch (NameNotFoundException e)
        { /*Nothing was found, but keep going*/ }

        try
        {
            for (LdapMailListing listing : ldapMailListingService.findByEmailAddress(email))
            {
                //If we've already found the person, don't add them twice.
                if (!includedPeople.contains(listing.getUcdPersonUUID()))
                {
                    people.add(getPersonFromId(listing.getUcdPersonUUID()));
                }
            }
        }
        catch (NameNotFoundException e)
        { /*Nothing was found, but keep going*/ }

        return people;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#isActive(java.lang.String)
     */
    @Override
    public boolean isActive(String personId)
    {
        /*
         * Being in LDAP implies being active.
         */
        try
        {
            return ldapPersonService.findByUniversalUserId(personId) != null;
        }
        catch (NameNotFoundException nnf)
        {
            return false;
        }
    }


    /**
     * Build and return a person DTO from the LDAP person.
     *
     * @param ldapPerson person returned from LDAP query
     * @return person DTO
     */
    private PersonDTO getPerson(LdapPerson ldapPerson)
    {
        if (ldapPerson == null || ldapPerson.getUid() == null) return null;

        List<String> uids = ldapPerson.getUid();
        List<Principal> principals = new ArrayList<>();
        if (uids != null)
        {
            for (String uid : uids) {
                principals.add(new PrincipalImpl(uid));
            }
        }

        List<String> mails = ldapPerson.getMail();
        List<EmailAddress> emails = new ArrayList<>();
        if (mails != null)
        {
            for (String mail : mails)
            {
                BasicEmailAddress email = new BasicEmailAddress();
                email.setAddress(mail);

                emails.add(email);
            }
        }

        return new BasicPersonDTO().setPersonId(ldapPerson.getUcdPersonUUID())
                                   .setEmployeeId(ldapPerson.getEmployeeNumber())
                                   .setActive(true)
                                   .setGivenName(ldapPerson.getGivenName())
                                   .setMiddleName(null)
                                   .setSurname(StringUtils.join(ldapPerson.getSn(), " "))
                                   .setSuffix(null)
                                   .setPreferredName(ldapPerson.getDisplayName())
                                   .setPrincipals(principals)
                                   .setEmails(emails);
    }
}
