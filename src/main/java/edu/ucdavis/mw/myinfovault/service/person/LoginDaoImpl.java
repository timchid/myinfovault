/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LoginDaoImpl.java
 */


package edu.ucdavis.mw.myinfovault.service.person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;



/**
 * Manage access to the MIV UserLogin table.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class LoginDaoImpl extends JdbcDaoSupport implements LoginDao
{
    private static final String GET_LOGIN_FOR_LOGIN = "SELECT * FROM UserLogin WHERE Login=?";
    private static final String GET_LOGINS_FOR_USERID = "SELECT * FROM UserLogin WHERE UserID=?";
    private static final String INSERT_LOGIN = "INSERT INTO UserLogin (UserID, Login, InsertUser, InsertTimestamp) VALUES (?, ?, ?, now())";

    private final Logger log = Logger.getLogger(this.getClass());


    /**
     * Get the login record for the given login name.
     * @return the Login for the provided name, or null if there is no associated login
     * @see edu.ucdavis.mw.myinfovault.service.person.LoginDao#getLogin(java.lang.String)
     */
    @Override
    public Login getLogin(String loginName)
    {
        final Object[] args = { loginName };

        Login loginEntry = null;
        try {
            loginEntry = this.getJdbcTemplate().queryForObject(GET_LOGIN_FOR_LOGIN, args, loginMapper);
        }
        catch (EmptyResultDataAccessException e) {
            // allow loginEntry to remain null
        }

        return loginEntry;
    }


    /**
     * Get all the logins for the given MIV UserID number.
     * @param userIdentifier A unique identifier for a user. For MyInfoVault, this must be convertible to a number.
     *
     * @see edu.ucdavis.mw.myinfovault.service.person.LoginDao#getLoginsForUser(java.lang.Object)
     */
    @Override
    public Collection<Login> getLoginsForUser(Object userIdentifier)
    {
        final String uid = userIdentifier.toString();
        final int userId = Integer.parseInt(uid);

        final Object[] args = { userId };

        final List<Login> logins = this.getJdbcTemplate().query(GET_LOGINS_FOR_USERID, args, loginMapper);

        return logins;
    }


    @Override
    public boolean save(final Login login)
    {
        boolean saved = false;

        Login existing = this.getLogin(login.getLoginName());
        // Never overwrite an existing login name entry
        if (existing == null)
        {
            final int userId = Integer.parseInt(login.getUserID().toString());
            this.getJdbcTemplate().update(
                    new PreparedStatementCreator()
                    {
                        @Override
                        public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException
                        {
                            PreparedStatement ps = null;
                            ps = connection.prepareStatement(INSERT_LOGIN);
                            ps.setInt(1, userId);
                            ps.setString(2, login.getLoginName());
                            ps.setInt(3, 18099); // FIXME: that's my userid - should have some way to get the real, logged-in user
                            return ps;
                        }
                    }
            );
            saved = true;
        }
        else
        {
            final String msg = "Attempted to add login name \"" + login.getLoginName() +
                               " with User ID " + login.getUserID().toString() +
                               " but that login is already in use -- [" + existing + "]";
            log.error(msg);
        }

        return saved;
    }


    private static final DateFormat df = DateFormat.getDateTimeInstance(/*DateFormat.SHORT, DateFormat.SHORT*/);
    private static final RowMapper<Login> loginMapper = new RowMapper<Login>()
    {
        @Override
        public Login mapRow(final ResultSet rs, final int rowNum) throws SQLException
        {
            final int userId = rs.getInt("UserID");
            final String loginName = rs.getString("Login");
            final Date lastLogin = rs.getTimestamp("LoginTimestamp");
            final Date lastLogout = rs.getTimestamp("LogoutTimestamp");
            final Login login = new Login(userId, loginName, lastLogin, lastLogout);

            final int insertUser = rs.getInt("InsertUser");
            final Date insertDate = rs.getTimestamp("InsertTimestamp");
            final int updateUser = rs.getInt("UpdateUser");
            final Date updateDate = rs.getTimestamp("UpdateTimestamp");

            login.addAttribute("InsertUser", insertUser+"");
            if (insertDate != null) {
                login.addAttribute("InsertTimestamp", df.format(insertDate));
            }
            if (updateUser != 0) {
                login.addAttribute("UpdateUser", updateUser+"");
            }
            if (updateDate != null) {
                login.addAttribute("UpdateTimestamp", df.format(updateDate));
            }

            return login;
        }
    };
}
