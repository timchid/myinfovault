/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivPerson.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.util.Collection;
import java.util.List;
import java.util.Set;


/**<p>
 * Definition of a Person for use in MyInfoVault.
 * This supports the standard parts from a KimEntity, plus MIV specific
 * Identity, Name, Email, Appointment, and Role information.
 * </p>
 * <p>Suggestions for changes and for additional things you need from a person are welcome.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 *
 */
public interface MivPerson extends Person
{
    /**
     * Get the MIV account identifier. This ties people to their data, preferences, etc.
     * @return This person's account ID number.
     */
    public int getUserId();


    /**
     * Get the Principal (login) name for this Person. But WHICH ONE? A person can have more than one login.
     * @return One of this Person's login names, chosen more-or-less at random.
     * @deprecated Prefer {@link #getPersonId()} to identify a Person as much as possible.
     */
    @Deprecated
    public String getPrincipalName();


    /** @deprecated Replace calls to this method with {@link #getPersonId()} instead. */
    @Deprecated
    public String getEntityId();


    /**
     * Get the unique identifier for this person.
     * @return the unique identifier for this person.
     */
    @Override
    public String getPersonId();

    @Override
    public String getGivenName();
    @Override
    public String getMiddleName();
    @Override
    public String getSurname();
    @Override
    public String getSuffix();


    /**
     * Get a person's name as they desire it to appear. This is configured
     * externally (in white pages) and does not have to correspond to their
     * official or legal name.
     *
     * @return this person's name as they have configured it to appear
     */
    @Override
    public String getDisplayName();


    /**
     * Gets the person's name as a string that can be sorted by last name, as
     *  "Last,[ First][ Middle][ Suffix]"
     *
     * @return this person's name in "sort" order
     */
    @Override
    public String getSortName();

    /**
     * Get name and ID information suitable for logging.
     * @return this person's name, user ID, and principal ID
     */
    public String getLoggingName();

    @Override
    public String getPreferredEmail();
    public void setPreferredEmail(String address);
    @Override
    public List<String> getEmailAddresses(); // Isn't this part of the base KimEntity?


    /*
     * Appointments - school/department
     *    the [Appointment] object should be preferred to school and dept ID numbers.
     */

    /**
     * Retrieve all primary and joint Appointments for a person. Exactly one of these
     * will reply <code>true</code> to the {@link Appointment#isPrimary() isPrimary()}
     * method.
     * @return TODO: javadoc the return
     */
    public Collection<Appointment> getAppointments();

    /**
     * Get the primary Appointment for this person.
     * @return An Appointment that is the primary for this person.
     */
    public Appointment getPrimaryAppointment();


    /**
     * Add the given appointment to this person's appointments.
     * If the provided appointment is marked as the primary any existing primary appointment will be changed to a joint appointment.
     *
     * @param app an appointment to add in addition to this person's existing appointments
     * @return <code>true</code> if the appointment was added, <code>false</code> otherwise (e.g. the person already had this appointment)
     */
    public boolean addAppointment(Appointment app);


    public boolean removeAppointment(Appointment app);




    /*
     * User Role(s) access and manipulation
     */


    /**
     * TODO: javadoc
     * Equivalent to <code>person.getPrimaryRoles().get(0).getRole();</code>
     * @return
     */
    public MivRole getPrimaryRoleType();


//    /**
//     * Makes the given role the primary role for the person.
//     * The role is added if not already present.
//     * <em>The existing primary role <strong>is removed</strong></em>
//     * and will need to be added again if the person should
//     * retain that role as secondary.
//     *
//     * @param role the role to make primary.
//     * @deprecated Use {@link #setPrimaryRole(AssignedRole)} instead.
//     */
//    @Deprecated
//    public void setPrimaryRole(MivRole role);

    /**
     * Makes the given role the primary role for the person.
     * The role is added if not already present.
     * <em>The existing primary role <strong>is removed</strong></em>
     * and will need to be added again if the person should
     * retain that role as secondary.
     *
     * @param role the role to make primary.
     */
    public void setPrimaryRole(AssignedRole role);

//    /**
//     * Get the set of all roles this person has.
//     * <em>Note</em> that this may return an empty Set if this person
//     * has no roles assigned.
//     * @return A java.util.Set of MivRoles this person has, or an empty Set.
//     * @deprecated Use {@link #getAssignedRoles} instead.
//     */
//    @Deprecated
//    public Set<MivRole> getRoles();


    /**
     * Get the set of all roles assigned to this person.
     * <em>Note</em> that this may return an empty Set if this person
     * has no roles assigned.
     * @return A java.util.Set of MivRoles this person has, or an empty Set.
     */
    public Set<AssignedRole> getAssignedRoles();

    /**
     * The returned list will all be the same base role, but each with its own scope.
     * There will always be at least one AssignedRole returned in the list, so that
     * <code>list.get(0)</code> is guaranteed to be safe, however the returned role
     * may be an {@link MivRole#INVALID_ROLE} in the case the Person is not an MIV User.
     * @return The Primary role(s) this person has, each with its own scope qualifier.
     */
    public List<AssignedRole> getPrimaryRoles();


//    /**
//     * Add an additional role for the person. If the role is already present
//     * it will not be duplicated and <code>false</code> will be returned.
//     *
//     * @param role the role to add to this person.
//     * @return <code>true</code> if the role was added, <code>false</code> otherwise.
//     * @deprecated Use {@link #addRole(AssignedRole)} instead
//     */
//    @Deprecated
//    public boolean addRole(MivRole role);


    /**
     * Add an additional role for the person. If the role is already present
     * it will not be duplicated and <code>false</code> will be returned.
     *
     * @param role the role to add to this person.
     * @return <code>true</code> if the role was added, <code>false</code> otherwise.
     */
    public boolean addRole(AssignedRole role);


    /**
     * Removes the given role, unless it's the Primary role.
     * To remove the primary role a new primary role must be established first, e.g.:<pre>
     * MivRole oldPrimary = person.getPrimaryRole();
     * person.setPrimaryRole(newPrimary);
     * person.removeRole(oldPrimary);</pre>
     * @param role The role to remove from this person.
     * @return <code>true</code> if the role was removed, <code>false</code> if not.
     */
    public boolean removeRole(MivRole role);

    public boolean removeRole(AssignedRole role);

    /**
     * Tests whether or not a person has a given role.
     *
     * @param roles the MivRoles to check
     * @return <code>true</code> if this person has any of the provided roles.
     */
    public boolean hasRole(MivRole... roles);

    /**
     * Tests whether or not a person has a given role.
     *
     * @param roles the MivRoles to check
     * @return <code>true</code> if this person has any of the provided roles.
     */
    public boolean hasRole(Iterable<MivRole> roles);

    public void setActive(boolean active);
    public boolean isActive();

    /**
     * Loads person details.
     *
     * @return <code>true</code> if the person details were added, <code>false</code> otherwise
     */
    public boolean loadDetail();

    public MivPerson proxiedBy(MivRole role);


    public void setName(String givenName, String middleName, String surname, String suffix);
}
