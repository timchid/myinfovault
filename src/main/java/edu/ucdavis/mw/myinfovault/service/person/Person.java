/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Person.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.util.List;

/**
 * Basics of a Person.
 * A person has some unique identifier.
 * A person has a name, which can be retrieved in various ways.
 * A person (probably) has email addresses.
 * A person (probably) has one more more logins to systems (logins are called Principals)
 *
 * @author Stephen Paulsen
 *
 */
public interface Person
{
    /*
     * A person has a unique identifier in some system. We call this the PersonId here.
     * It depends on what the identity source system actually is. Here at UCD this is
     * the ucdPersonUUID from LDAP, which comes from the MothraID, or it's the IAMID
     * from the new IAM (Identity and Access Management) system.
     */

    /**
     * Get the unique identifier for this person.
     *
     * @return the unique identifier for this person.
     */
    public String getPersonId();


    /*
     * A person has a name that we can get all or part of. This is the Person's
     * official name, or at least how the system knows them, but shouldn't be
     * a nickname.
     * First the parts...
     */

    /**
     * Get this Person's first (or "given") name.
     *
     * @return the Person's given name
     */
    public String getGivenName();


    /**
     * Get this Person's middle name <em>or names</em>. Some people have more than
     * three names. This may return several names from the middle of their name.
     *
     * @return the Person's middle name(s) or an empty String
     */
    public String getMiddleName();


    /**
     * Get this Person's surname, or "last" name. In some cultures the surname isn't last.
     *
     * @return the Person's surname
     */
    public String getSurname();


    /**
     * Get this Person's suffix, such as Jr. (junior), Sr. (senior), III, etc., if any.
     *
     * @return a suffix or an empty String
     */
    public String getSuffix();


    /*
     * Now the whole name, arranged in various ways.
     */

    /**
     * Get a person's name as they desire it to appear. This is configured
     * externally (in white pages) and does not have to correspond to their
     * official or legal name.
     *
     * @return this person's name as they have configured it to appear
     */
    public String getDisplayName();


    /**
     * Gets the person's name as a string that can be sorted by last name, as
     *  "Last,[ First][ Middle][ Suffix]"
     *
     * @return this person's name in "sort" order
     */
    public String getSortName();


    /*
     * People tend to have at least one email address, often more.
     */

    /**
     * Get all of the email addresses that are known for this Person.
     * Returns an empty list if there are no emails known. Never returns null.
     *
     * @return a List of email address Strings
     */
    public List<String> getEmailAddresses();


    /**
     * Get the email address where this Person has indicated they prefer to receive email.
     * If no preference exists this can return any known email address for the Person.
     * If no email addresses are known for this Person it will return null
     *
     * @return a String representing an email address, or null
     */
    public String getPreferredEmail();


    /*
     * A person's login information
     */

    /**
     * Get all of the login names that are known for this Person.
     * Can return null if none are known.
     *
     * @return a List of Principals, or <code>null</code>
     */
    public List<Principal> getPrincipals();


    /**<p>
     * Get just one Principal (login) for this Person. If a person has only one known login
     * this will get it. If they have more but there is a primary, or preferred, return that one.
     * Return <code>null</code> if there are no Principals for this Person.</p>
     * If there are several known principals and no way to select one this returns <strong><code>null</code></strong>
     * so as not to decieve the caller that there is a primary or preference. In that case you
     * should probably call {@link #getPrincipals()}, sort the list, and use the first, or
     * devise some other strategy to obtain a single Principal in a consistent way.
     *
     * @return a Principal or <code>null</code>
     */
    public Principal getPrincipal();
}
