/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonIdMapper.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

/**<p>
 * Map between the various identifiers for a Person.</p>
 * <p>An implementation may throw an {@link java.lang.UnsupportedOperationException UnsupportedOperationException}
 * for any of these methods if the mapping can't be determined.</p>
 * <p>Getting <em>PrincipalName</em> (logins) mapped from some other identifier is a problem
 * because a Person may have several Principals. Those have been omitted.</p>
 * Implementations that I have in mind:
 *   a) Like the UserEquivalence found in the current MivUserServiceImpl
 *   b) Calls to the IAM API api/iam/people/ids/1000017567?key=<api_key> etc.
 *                           api/iam/people/ids/search?mothraId=12345678&key=<api_key> etc.
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public interface PersonIdMapper
{
 // public String getPersonIdForPersonId(String personId);
    public String getPersonIdForIamId(String iamId);
    public String getPersonIdForPrincipalName(String principalName);
    public String getPersonIdForEmployeeId(String employeeId);
    public String getPersonIdForStudentId(String studentId);
    public String getPersonIdForUserId(String userId);

    public String getIamIdForPersonId(String personId);
 // public String getIamIdForIamId(String iamId);
    public String getIamIdForPrincipalName(String principalName);
    public String getIamIdForEmployeeId(String employeeId);
    public String getIamIdForStudentId(String studentId);
    public String getIamIdForUserId(String userId);

    public String getEmployeeIdForPersonId(String personId);
    public String getEmployeeIdForIamId(String iamId);
    public String getEmployeeIdForPrincipalName(String principalName);
 // public String getEmployeeIdForEmployeeId(String studentId);
    public String getEmployeeIdForStudentId(String studentId);
    public String getEmployeeIdForUserId(String userId);

    public String getStudentIdForPersonId(String personId);
    public String getStudentIdForIamId(String iamId);
    public String getStudentIdForPrincipalName(String principalName);
    public String getStudentIdForEmployeeId(String studentId);
 // public String getStudentIdForStudentId(String studentId);
    public String getStudentIdForUserId(String userId);

    public String getUserIdForPersonId(String personId);
    public String getUserIdForIamId(String iamId);
    public String getUserIdForPrincipalName(String principalName);
    public String getUserIdForEmployeeId(String employeeId);
    public String getUserIdForStudentId(String studentId);
 // public String getUserIdForUserId(String userId);
}
