/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SimilarNameFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.service.FuzzyMatchFilter;

/**
 * Filters MIV persons by their respective name's similarity to some target person's name as defined by {@link FuzzyMatchFilter}.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.5
 */
public class SimilarNameFilter extends FuzzyMatchFilter<MivPerson>
{
    /**
     * @param target target person whose name is compared to each name of the filter list.
     */
    public SimilarNameFilter(MivPerson target)
    {
        super(target);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.FuzzyMatchFilter#getRepresentation(java.lang.Object)
     */
    @Override
    protected String getRepresentation(MivPerson item)
    {
        if (item == null) return StringUtils.EMPTY;

        return item.getGivenName() + " " + item.getSurname();
    }
}
