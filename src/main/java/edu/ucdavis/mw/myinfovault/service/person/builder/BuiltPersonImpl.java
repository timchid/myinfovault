/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BuiltPersonImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.person.builder;

import static com.google.common.base.Preconditions.checkState;
import static edu.ucdavis.mw.myinfovault.util.StringUtil.EMPTY_STRING;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.PersonDataSource.EmailAddress;
import edu.ucdavis.mw.myinfovault.service.person.PersonDataSource.PersonDTO;
import edu.ucdavis.mw.myinfovault.service.person.Principal;
import edu.ucdavis.mw.myinfovault.service.person.PrincipalImpl;
import edu.ucdavis.mw.myinfovault.service.person.RefreshablePerson;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.PermitTag;

/**
 * An MIV Person, as built by the PersonBuilder
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class BuiltPersonImpl implements MivPerson, MivPersonInternal, RefreshablePerson, Serializable
{
    private static final long serialVersionUID = 201206181110L;

    private static  final Logger logger = LoggerFactory.getLogger(BuiltPersonImpl.class);


    /**
     * MIV User ID number.
     * An MivPerson might not actually be an MIV User. Start with a -1 userId as a flag for that.
     */
    private int userId = -1;

    /**
     * List of all principals (login accounts) a person has
     */
    private List<Principal> principals;

    /**
     * Kerberos (or other system) Login Name
     */
    private String principalName;

    /**
     * The UCD PersonUUID, the MothraId, the IamId, aka Kim EntityId
     */
    private String personId;

    private boolean active;

    // Set these 4 name parts *only* by using the setters — the setters enforce the rules.
    private String givenName  = StringUtil.EMPTY_STRING;
    private String middleName = StringUtil.EMPTY_STRING;
    private String surname    = StringUtil.EMPTY_STRING;
    private String suffix     = StringUtil.EMPTY_STRING; // Jr. Sr. III etc.

    private String displayName;

    /**
     * Person's name arranged in an order appropriate for last-name sorting.
     * This attribute is generated internally; there is no Setter.
     */
    private String sortName;

    private final List<String> emailAddresses = new ArrayList<String>();
    private String preferredEmail;

    /** for use by Proxied version of this Person */
    private volatile Set<MivRole> tempRoles = null;
    /** hasRole() and proxiedBy() will have a race condition unless synchronized */
    private final String tempRoleLock = new String("LOCK");

    private Set<AssignedRole> assignedRoles = new HashSet<AssignedRole>();
    private AssignedRole primaryRole;

    private final Set<Appointment> appointments = new HashSet<Appointment>();
//    /** The original primary appointment is (MAYBE?) needed to resolve qualified roles if either the role or appointment changes. */
//    private Appointment originalPrimaryAppointment = null;

    private long loadTime = System.currentTimeMillis();


    public BuiltPersonImpl(MivAccountDto adto, PersonDTO pdto)
    {
        // One or both of the data sources *must* be provided
        checkState(adto != null || pdto != null, "Both data sources were null when building a Person");

        // Start with the Account table info, then overlay it from the external data source (if provided)
        if (adto != null) {
            populateFromAccount(adto);
        }

        // Finish early if no Person DTO was given
        if (pdto != null) {
            populateFromExternal(pdto);
        }

        resolvePersonName(); // do Last
    }


    /**
     * Build a person that has an MIV account but isn't, or is no longer, available in the external data source.
     * @param adto
     */
    public BuiltPersonImpl(MivAccountDto adto)
    {
        populateFromAccount(adto);
        resolvePersonName();
    }


    /**
     * Build a person that doesn't necessarily have an MIV account.
     * Primarily used when adding an account for someone.
     * @param pdto Person data from an external identity data source.
     */
    public BuiltPersonImpl(PersonDTO pdto)
    {
        populateFromExternal(pdto);
        resolvePersonName();
    }

    private void breakpoint(String marker)
    {
        if (/*true || */logger.isDebugEnabled()) {
            System.out.println("Stopping for marker '" + marker + "'");
        }
    }


    private void populateFromAccount(MivAccountDto adto)
    {
        this.personId = adto.getPersonUuid();
        this.userId = adto.getUserId();

        this.active = adto.isActive();

        // Name parts and pieces should be set from the Account table,
        // but overlaid by the PersonDataSource (if available) to get
        // updates.
        this.setGivenName(   adto.getGivenName()     );
        this.setMiddleName(  adto.getMiddleName()    );
        this.setSurname(     adto.getSurname()       );
        this.setSuffix(      adto.getSuffix()        );
        this.setDisplayName( adto.getPreferredName() );

        this.preferredEmail = adto.getEmail();
        this.emailAddresses.add(this.preferredEmail);

        this.principalName = adto.getLogin();
        this.principals = principals != null ? principals : new ArrayList<Principal>();
        this.principals.add(new PrincipalImpl(this.principalName));

        breakpoint("populateFromAccount");

        // TODO: keep writing, not done yet. // Need the AssignedRoles in particular. !!No, that's done in the PersonBuilder!!
    }


    private void populateFromExternal(PersonDTO pdto)
    {
        this.personId = pdto.getPersonId();

        // Get the Principals in shortest-to-longest, so we avoid firstname.lastname when possible.
        List<Principal> foundPrincipals = pdto.getPrincipals();
        Collections.sort(foundPrincipals, shortestFirst);

        // Set the principals, making sure it won't be null
        this.principals =
                foundPrincipals != null ? foundPrincipals       // use the found principals if there are some
                        :
                            this.principals != null ? this.principals   // or use the existing principals
                                    :
                                        Collections.<Principal>emptyList();     // or use an empty list if neither of the above were present.

        if (foundPrincipals != null && foundPrincipals.size() > 0) {
            this.principalName = foundPrincipals.get(0).getPrincipalName();
        }
        // Is the principal name still null? Use the empty principal.
        if (this.principalName == null) {
            this.principalName = Principal.EMPTY_PRINCIPAL.getPrincipalName();
        }

        this.setGivenName(  pdto.getGivenName()  );
        this.setMiddleName( pdto.getMiddleName() );
        this.setSurname(    pdto.getSurname()    );
        this.setSuffix(     pdto.getSuffix()     );

        // Only set the displayName / preferredName if the external source provides one,
        // otherwise we'll build one (later) from the parts of a person's name.
        String preferredName = pdto.getPreferredName();
        if (StringUtils.isNotBlank(preferredName)) {
            this.setDisplayName( preferredName );
        }

        for (EmailAddress email : pdto.getEmails())
        {
            if (!this.emailAddresses.contains(email.getAddress())) {
                this.emailAddresses.add(email.getAddress());
            }
            // Only set the external preferred email address as our preferred if one wasn't saved as the preferred in the Account table.
            if (this.preferredEmail == null && email.isPreferred()) {
                this.preferredEmail = email.getAddress();
            }
        }

        //If the person has no preferred email addresses, use the first one
        if (this.preferredEmail == null && this.emailAddresses.size() > 0)
        {
            this.preferredEmail = this.emailAddresses.get(0);
        }
        // TODO: keep writing, not done yet.
        breakpoint("populateFromExternal");

        if (this.active && ! pdto.isActive())
        {
            // We *HAVE* a person from the external source (lookup didn't fail)
            // but they are marked as "inactive", while internally we are still
            // marked as active. What now? Should we deactivate the MIV user?
        }
    }

    /**
     * Returns the shorter string as 'less-than' the longer, or a standard string compare if they are the same length.
     * This is used to guarantee that multiple Principal Names are always returned in the same order,
     * and (hopefully) with the short kerberos name first, and firstname.lastname style logins last.
     */
    private static final Comparator<Principal> shortestFirst = new Comparator<Principal>() {
        @Override
        public int compare(Principal o1, Principal o2)
        {
            // does a null-safe comparison
            String p1 = o1.getPrincipalName();
            String p2 = o2.getPrincipalName();
            if (p1 == null) p1 = EMPTY_STRING;
            if (p2 == null) p2 = EMPTY_STRING;
            if (p1 == p2) return 0;
            int diff = p1.length() - p2.length();
            return diff != 0 ? diff : p1.compareTo(p2);
        }
    };


    /**
     * Resolve the manufactured attributes.
     * e.g. builds the 'SortName' from its constituent parts. May do other things in the future, and should be renamed if it does.
     */
    private void resolvePersonName()
    {
        this.sortName = (StringUtils.isNotBlank(this.surname) ? this.surname + ", " : "") +
                        (StringUtils.isNotBlank(this.givenName) ? this.givenName : "") +
                        (StringUtils.isNotBlank(this.middleName) ? " " + this.middleName : "") +
                        (StringUtils.isNotBlank(this.suffix) ? " " + this.suffix : "");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.Person#getPrincipals()
     */
    @Override
    public List<Principal> getPrincipals()
    {
        if (this.principals == null) {
            this.principals = new ArrayList<>(1);
            this.principals.add(Principal.EMPTY_PRINCIPAL);
        }
        return Collections.unmodifiableList(this.principals);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.Person#getPrincipal()
     */
    @Override
    public Principal getPrincipal()
    {
        List<Principal> principals = this.getPrincipals();
        if (principals != null && principals.size() > 0) {
            return this.getPrincipals().get(0);
        }
        // No principal found, return the empty Principal
        return Principal.EMPTY_PRINCIPAL;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getUserId()
     */
    @Override
    public int getUserId()
    {
        return this.userId;
    }


    /**
     * When adding a new person to the system we have to set the userid.
     * Never call this otherwise.
     * @param userId User ID number to give to this Person
     * @return this Person
     */
    @Override
    public void setUserId(int userId)
    {
        assert this.userId <= 0 : "A user ID was already set when calling setUserId!";

        this.userId = userId;
        return;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getPrincipalName()
     */
    @Override
    public String getPrincipalName()
    {
        return this.principalName;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getEntityId()
     */
    @Override
    public String getEntityId()
    {
        return this.personId;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getPersonId()
     */
    @Override
    public String getPersonId()
    {
        return this.personId;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getGivenName()
     */
    @Override
    public String getGivenName()
    {
        return StringUtils.isNotBlank( this.givenName ) ? this.givenName : StringUtil.EMPTY_STRING;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getMiddleName()
     */
    @Override
    public String getMiddleName()
    {
        return StringUtils.isNotBlank( this.middleName ) ? this.middleName : StringUtil.EMPTY_STRING;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getSurname()
     */
    @Override
    public String getSurname()
    {
        return StringUtils.isNotBlank( this.surname ) ? this.surname : StringUtil.EMPTY_STRING;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getSuffix()
     */
    @Override
    public String getSuffix()
    {
        return StringUtils.isNotBlank( this.suffix ) ? this.suffix : StringUtil.EMPTY_STRING;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getDisplayName()
     */
    @Override
    public String getDisplayName()
    {
        return this.displayName;
    }


    /**
     * The PersonBuilder will use this, but outsiders aren't allowed.
     * @param displayName
     * @return
     */
    @Override
    public void setDisplayName(String displayName)
    {
        if (StringUtils.isNotBlank(displayName)) {
            this.displayName = displayName;
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getSortName()
     */
    @Override
    public String getSortName()
    {
        return this.sortName;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getLoggingName()
     */
    @Override
    public String getLoggingName()
    {
        return (
                this.getSortName() + " / "
                + this.getUserId() + " / "
                + this.getPersonId()
                );
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getPreferredEmail()
     */
    @Override
    public String getPreferredEmail()
    {
        return this.preferredEmail;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#setPreferredEmail(java.lang.String)
     */
    @Override
    public void setPreferredEmail(String address)
    {
        if (!this.emailAddresses.contains(address)) {
            this.emailAddresses.add(address);
        }
        this.preferredEmail = address;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getEmailAddresses()
     */
    @Override
    public List<String> getEmailAddresses()
    {
        return Collections.unmodifiableList(this.emailAddresses);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getAppointments()
     */
    @Override
    public Collection<Appointment> getAppointments()
    {
        return Collections.unmodifiableCollection(this.appointments);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getPrimaryAppointment()
     */
    @Override
    public Appointment getPrimaryAppointment()
    {
        Appointment primary = null;

        for (final Appointment a : this.appointments)
        {
            if (a.isPrimary())
            {
                primary = a;
                break;
            }
        }

        // Haven't found an appointment - manufacture one from the user's home Scope
        if (primary == null)
        {
            for (AssignedRole ar : this.getAssignedRoles())
            {
                if (ar.getRole() == MivRole.MIV_USER)
                {
                    Scope s = ar.getScope();
                    // During 4.0 development, before everyone's converted, people may have a -1:-1 scope
                    // for their MIV_USER role. This should never happen by the time we're in 4.0 production,
                    // but for now don't create an Appointment if their MIV_USER is not scoped yet.
                    if (! s.equals(Scope.ANY)) {
                        primary = new Appointment(s.getSchool(), s.getDepartment(), true);
                    }
                    break;
                }
            }
        }

        return primary;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#addAppointment(edu.ucdavis.mw.myinfovault.service.person.Appointment)
     */
    @Override
    public boolean addAppointment(final Appointment app)
    {
        // If this appointment we're adding is marked as primary
        // we'll turn any existing primary into a joint appointment.
        if (app.isPrimary())
        {
            for (final Appointment a : this.appointments)
            {
                if (a.isPrimary()) {
                    a.makeJoint();
                    // There should only be one primary appointment and we ought to
                    // be able to "break;" here, but let's just make sure in case
                    // someone managed to slip extra primary appointments in.
                }
            }
//            if (! app.equals(this.originalPrimaryAppointment)) {
//                this.rolesModified = true;
//            }
        }

        return this.appointments.add(app);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#removeAppointment(edu.ucdavis.mw.myinfovault.service.person.Appointment)
     */
    @Override
    public boolean removeAppointment(Appointment app)
    {
        return this.appointments.remove(app);
    }


    /** from MivPersonInternal - we'll need this later */
    @Override
    public MivPersonInternal clearAppointments()
    {
        this.appointments.clear();

        return this;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getPrimaryRoleType()
     */
    @Override
    public MivRole getPrimaryRoleType()
    {
        AssignedRole ar = null;
        List<AssignedRole> primaries = this.getPrimaryRoles();
        if (primaries != null && primaries.size() > 0) {
            ar = primaries.get(0);
        }
        else {
            logger.warn("MISSING PRIMARY ROLE for User [{}]", this);
        }
        return ar == null ? null : ar.getRole();
    }


//    /* (non-Javadoc)
//     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#setPrimaryRole(edu.ucdavis.mw.myinfovault.service.person.MivRole)
//     */
//    @Override
//    public void setPrimaryRole(MivRole role)
//    {
//        // TO/DO Auto-generated method stub
//
//    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#setPrimaryRole(edu.ucdavis.mw.myinfovault.service.person.AssignedRole)
     */
    @Override
    public void setPrimaryRole(final AssignedRole role)
    {
        MivRole r = role.getRole();
        this.setPrimaryRole(r, role.getScope());
    }

    private void setPrimaryRole(final MivRole role, final Scope...scopes)
    {
        // Ensure that at least one scope was passed
        if (scopes == null || scopes.length < 1) {
            throw new IllegalArgumentException("At least one Scope must be provided when setting a Primary role.");
        }

        // Add a new role for each of the Scopes passed
        this.primaryRole = null;
        for (Scope scope : scopes)
        {
            AssignedRole ar = new AssignedRole(role, scope, true);
            @SuppressWarnings("unused") // 'added' is only there to view in the debugger
            boolean added = this.addRole(ar);
            if (this.primaryRole == null) this.primaryRole = ar;
        }

        // Remove any leftover roles that are prohibited to the new role
        for (MivRole r : role.getProhibitedRoles())
        {
            for (AssignedRole ar : this.getAssignedRoles()) {
                if (ar.getRole() == r) {
                    this.assignedRoles.remove(ar);
                }
            }
        }

        // Add any additional roles that the new primary role should automatically get
        for (MivRole r : role.getAdditionalRoles())
        {
            AssignedRole ar = new AssignedRole(r);
            this.addRole(ar);
        }
    }


//    /* (non-Javadoc)
//     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getRoles()
//     */
//    @Override
//    public Set<MivRole> getRoles()
//    {
//        // TO/DO Auto-generated method stub
//        return null;
//    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getAssignedRoles()
     */
    @Override
    public Set<AssignedRole> getAssignedRoles()
    {
        // Throw the roles into a Set to eliminate duplicates
        Set<AssignedRole> current = new HashSet<AssignedRole>(this.assignedRoles);
        return Collections.unmodifiableSet(current);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#getPrimaryRoles()
     */
    @Override
    public List<AssignedRole> getPrimaryRoles()
    {
        List<AssignedRole> roles = new ArrayList<AssignedRole>(5);

        for (AssignedRole role : this.getAssignedRoles())
        {
            if (role.isPrimary()) roles.add(role);
        }

        return roles;
    }


//    /* (non-Javadoc)
//     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#addRole(edu.ucdavis.mw.myinfovault.service.person.MivRole)
//     */
//    @Override
//    public boolean addRole(MivRole role)
//    {
//        // TO_DO Auto-generated method stub
//        return false;
//    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#addRole(edu.ucdavis.mw.myinfovault.service.person.AssignedRole)
     */
    @Override
    public boolean addRole(final AssignedRole role)
    {
        final boolean added = this.assignedRoles.add(role);
//        // Don't just assign "this.rolesModified = added" because if it's already true to should remain true, not be set to false
//        if (added) this.rolesModified = true; // flag that there's been a change
        return added;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#removeRole(edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public boolean removeRole(final MivRole role)
    {
        // WARNING - called in PermitTag.resolveTargetPerson
        throw new UnsupportedOperationException("Roles can no longer be removed in this manner");
        // TO_DO Auto-generated method stub
        //return false;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#hasRole(edu.ucdavis.mw.myinfovault.service.person.MivRole[])
     */
    @Override
    public boolean hasRole(final MivRole... roles)
    {
        return this.hasRole( java.util.Arrays.asList(roles) );
/*
        synchronized (tempRoleLock)
        {
            // Use 'tempRoles' if it's set, which will only be if 'proxiedBy' was used to clone this Person as a proxy
            Set<MivRole> mroles = this.tempRoles;
            // otherwise get the base MivRoles out of the Assigned Roles
            if (mroles == null)
            {
                Set<AssignedRole> aroles = this.getAssignedRoles();
                mroles = new HashSet<MivRole>(aroles.size());
                for (AssignedRole ar : aroles) {
                    mroles.add(ar.getRole());
                }
            }

            for (final MivRole r : roles) {
                if (mroles.contains(r)) return true;
            }

            return false;
        }
*/
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#hasRole(java.lang.Iterable)
     */
    @Override
    public boolean hasRole(final Iterable<MivRole> roles)
    {
        synchronized (tempRoleLock)
        {
            // Use 'tempRoles' if it's set, which will only be if 'proxiedBy' was used to clone this Person as a proxy
            Set<MivRole> mroles = this.tempRoles;
            // otherwise get the base MivRoles out of the Assigned Roles
            if (mroles == null)
            {
                Set<AssignedRole> aroles = this.getAssignedRoles();
                mroles = new HashSet<MivRole>(aroles.size());
                for (AssignedRole ar : aroles) {
                    mroles.add(ar.getRole());
                }
            }

            for (final MivRole r : roles) {
                if (mroles.contains(r)) return true;
            }

            return false;
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active)
    {
        this.active = active;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#isActive()
     */
    @Override
    public boolean isActive()
    {
        return this.active;
    }


    /**
     * Create a temporary copy of this Person with roles adjusted suitable for
     * a proxy acting as this Person.
     * Added this for use especially by the {@link PermitTag}.
     */
    @Override
    public MivPerson proxiedBy(MivRole role)
    {
        MivPerson clone = null;

        // Prevent hasRole(...) from examining the tempRoles while we're adjusting them.
        synchronized (tempRoleLock)
        {
            try
            {
                // Get the base MivRoles out of the Assigned Roles
                this.tempRoles = new HashSet<MivRole>();
                for (AssignedRole ar : this.getAssignedRoles()) {
                    this.tempRoles.add(ar.getRole());
                }

                // Eliminate the special roles that some Candidates have that shouldn't be inherited.
                switch (role)
                {
                    // A department "helper" shouldn't gain Dept. Chair or Dean abilities when acting as a proxy
                    case DEPT_ASSISTANT:
                        this.tempRoles.remove(MivRole.DEPT_CHAIR);
                        // fall through to also remove DEAN and VICE_PROVOST

                    // A department "admin" shouldn't gain Dean abilities when acting as a proxy
                    case DEPT_STAFF:
                        this.tempRoles.remove(MivRole.DEAN);
                        // fall through to also remove VICE_PROVOST

                        // A school "admin" shouldn't gain VICE_PROVOST
                        // abilities when acting as a proxy
                    case SCHOOL_STAFF:
                        this.tempRoles.remove(MivRole.VICE_PROVOST);
                        break;

                   // A OCP staff shouldn't gain Chancellor or Provost abilities when acting as a proxy
                   case OCP_STAFF:
                        this.tempRoles.remove(MivRole.PROVOST);
                        this.tempRoles.remove(MivRole.CHANCELLOR);
                        break;
                }

                // Clone this Person with the newly adjusted roles...
                clone = (MivPerson) this.clone();
                // ...then restore to use AssignedRoles when checking roles.
                this.tempRoles = null;
            }
            catch (CloneNotSupportedException e)
            {
                logger.warn("An \"impossible\" cloning exception occurred!", e);
            }
        }

        return clone;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPerson#loadDetail()
     */
    @Override
    public boolean loadDetail()
    {
        logger.error("loadDetail called for {} / {} but I'm not doing anything", this.displayName, this.userId);
        return true; // Doing nothing is always successful
    }


    /**
     * Return the system time when this object was created / loaded / or refreshed.
     * @return
     */
    @Override
    public long getLoadTime()
    {
        return this.loadTime;
    }

    @Override
    public RefreshablePerson freshen(PersonDTO p)
    {
        logger.debug("|| -- \"freshen\" called for {}", this.displayName);

        this.setActive(p.isActive());

        this.setGivenName  ( p.getGivenName()  );
        this.setMiddleName ( p.getMiddleName() );
        this.setSurname    ( p.getSurname()    );
        this.setSuffix     ( p.getSuffix()     );

        this.resolvePersonName();

        // what about         p.getEmails()  ?

        this.loadTime = System.currentTimeMillis();

        return this;
    }


    @Override
    public boolean removeRole(AssignedRole role)
    {
        return this.assignedRoles.remove(role);
    }


    @Override
    public void setName(String givenName, String middleName, String surname, String suffix)
    {
        this.setGivenName  ( givenName  );
        this.setMiddleName ( middleName );
        this.setSurname    ( surname    );
        this.setSuffix     ( suffix     );
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#setPrincipalName(java.lang.String)
     */
    @Override
    public void setPrincipalName(String principalName)
    {
        this.principalName = principalName;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#setPersonId(java.lang.String)
     */
    @Override
    public void setPersonId(String personId)
    {
        this.personId = personId;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#addEmailAddress(java.lang.String)
     */
    @Override
    public void addEmailAddress(String email)
    {
        this.emailAddresses.add(email);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#removeEmailAddress(java.lang.String)
     */
    @Override
    public String removeEmailAddress(String email)
    {
        this.emailAddresses.remove(email);
        return email;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#setGivenName(java.lang.String)
     */
    @Override
    public void setGivenName(String name)
    {
        if (name != null) {
            this.givenName = name.trim();
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#setMiddleName(java.lang.String)
     */
    @Override
    public void setMiddleName(String name)
    {
        if (name != null) {
            this.middleName = name.trim();
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#setSurname(java.lang.String)
     */
    @Override
    public void setSurname(String name)
    {
        if (name != null) {
            this.surname = name.trim();
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#setSuffix(java.lang.String)
     */
    @Override
    public void setSuffix(String suffix)
    {
        if (suffix != null) {
            this.suffix = suffix.trim();
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#clearAssignments()
     */
    @Override
    public MivPersonInternal clearAssignments()
    {
        this.assignedRoles.clear();

        return this;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.MivPersonInternal#clearRoles()
     */
    @Override
    public MivPersonInternal clearRoles()
    {
        this.assignedRoles.clear();

        return this;
    }


    public String getObjectId()
    {
        return "@" + (super.toString().split("@")[1]);
    }


    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }


    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.surname).append(", ").append(this.givenName)
          .append("  mivId: ").append(this.getUserId())
          .append("  principalName: ").append(this.getPrincipalName());
        final Appointment a = this.getPrimaryAppointment();
        if (a != null)
        {
            sb.append("  school: ").append(a.getScope().getSchoolAbbreviation())
              .append("  dept: ").append(a.getScope().getDepartmentDescription());
        }

        final String result = sb.toString();
        return result;
    }


    @Override
    protected void finalize() throws Throwable
    {
        long now = System.currentTimeMillis();
        long ageMs = now - this.loadTime;
        long ageSecs = ageMs / 1000;
        long ageMin = ageSecs / 60;
        TimeUnit tu = TimeUnit.MILLISECONDS;
        ageMin = tu.toMinutes(ageMs + 30000);
        ageSecs = tu.toSeconds(ageMs + 500);
        logger.debug("|| ---- {} being finalized - age is {} secs / {} mins :: {}",
                    new Object[] { this.displayName, ageSecs, ageMin, this.getObjectId() });
        super.finalize();
        Thread.yield();
    }

}
