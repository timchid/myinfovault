/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivAccountDto.java
 */

package edu.ucdavis.mw.myinfovault.service.person.builder;


import java.io.Serializable;
import java.util.List;

import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;

/**
 * Transfer data from the UserAccount table
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class MivAccountDto implements Serializable
{
    private static final long serialVersionUID = 201207181012L;

    private int userId = -1;
    private String login;
    private String personUuid; // aka KIM entityId, aka mothraId
    private int schoolId;
    private int departmentId;
    private String preferredName;
    private String surname;
    private String givenName;
    private String middleName;
    private String suffix;
    private String email;
    private boolean active;
//    | InsertTimestamp | timestamp    | NO   |     | CURRENT_TIMESTAMP |                |
//    | InsertUserID    | int(11)      | NO   |     |                   |                |
//    | UpdateTimestamp | datetime     | YES  |     | NULL              |                |
//    | UpdateUserID    | int(11)      | YES  |     | NULL              |                |


//    private Collection<AssignedRole> roles = Collections.emptyList();
//    private Collection<Appointment> appointments = Collections.emptyList();


    public int getUserId()
    {
        return userId;
    }
    public void setUserId(final int userId)
    {
        this.userId = userId;
    }
    public String getLogin()
    {
        return login;
    }
    public void setLogin(final String login)
    {
        this.login = login;
    }

    public String getPersonUuid() // had to make public so the user service impl findUsers could get the uuid
    {
        return personUuid;
    }
    public void setPersonUuid(final String personUuid)
    {
        this.personUuid = personUuid;
    }

    public int getSchoolId()
    {
        return schoolId;
    }
    public void setSchoolId(final int schoolId)
    {
        this.schoolId = schoolId;
    }

    public int getDepartmentId()
    {
        return departmentId;
    }
    public void setDepartmentId(final int departmentId)
    {
        this.departmentId = departmentId;
    }

    public String getPreferredName()
    {
        return this.preferredName;
    }
    public void setPreferredName(final String preferredName)
    {
        this.preferredName = preferredName;
    }

    public String getSurname()
    {
        return surname;
    }
    public void setSurname(final String surname)
    {
        this.surname = surname;
    }

    public String getGivenName()
    {
        return givenName;
    }
    public void setGivenName(final String givenName)
    {
        this.givenName = givenName;
    }

    public String getMiddleName()
    {
        return this.middleName;
    }
    public void setMiddleName(final String middleName)
    {
        this.middleName = middleName;
    }

    public String getSuffix()
    {
        return suffix;
    }
    public void setSuffix(String suffix)
    {
        this.suffix = suffix;
    }

    public String getEmail()
    {
        return email;
    }
    public void setEmail(final String email)
    {
        this.email = email;
    }

    public boolean isActive()
    {
        return active;
    }
    public void setActive(final boolean active)
    {
        this.active = active;
    }

//    public Collection<AssignedRole> getRoles()
//    {
//        return this.roles;
//    }
//    public void setRoles(final Collection<AssignedRole> roles)
//    {
//        this.roles = roles;
//    }
//
//    public Collection<Appointment> getAppointments()
//    {
//        return this.appointments;
//    }
//    public void setAppointments(final Collection<Appointment> appointments)
//    {
//        this.appointments = new HashSet<Appointment>(appointments.size());
//        for (final Appointment a : appointments) {
//            this.appointments.add(a);
//        }
//    }

//    public void setPreferredName()
//    {
//        this.preferredName = (
//            (StringUtils.hasText(this.givenName) ? this.givenName : "") +
//            (StringUtils.hasText(this.middleName) ? " " + this.middleName : "") +
//            (StringUtils.hasText(this.surname) ? " " + this.surname : "") +
//            (StringUtils.hasText(this.suffix) ? " " + this.suffix : "")
//        );
//    }

    @Override
    public String toString()
    {
        return this.login + " | " + this.userId + " | " + this.surname + ", " + this.givenName;
    }
    public void setRoles(List<AssignedRole> roles)
    {
        // TODO Auto-generated method stub

    }
}
