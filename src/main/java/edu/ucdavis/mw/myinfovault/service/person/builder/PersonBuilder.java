/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonBuilder.java
 */

package edu.ucdavis.mw.myinfovault.service.person.builder;

import static com.google.common.base.Preconditions.checkState;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import edu.ucdavis.mw.myinfovault.dao.person.AppointmentDao;
import edu.ucdavis.mw.myinfovault.dao.person.RoleAssignmentDao;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.PersonDataSource.PersonDTO;
import edu.ucdavis.mw.myinfovault.service.person.Principal;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Build an MivPerson object, using the necessary Data Transfer Objects and Data Access Objects
 * to gather all the necessary bits of information.
 *
 * TODO: Should "PersonBuilder" be an Interface, and have a PersonBuilderImpl ?
 *       That might be better for Spring dependency injection.
 *       But see below at "An entirely different strategy"
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class PersonBuilder
{
    private static final Logger logger = LoggerFactory.getLogger(PersonBuilder.class);

    /*
     * What will PersonBuilder need to do its job?
     *   MivAccountDto          ✔
     *   AppointmentDao         ✔
     *   RoleAssignmentDao      ✘
     *    -?-
     */


// XXX: SDP - maybe the DAOs don't belong in the PersonBuilder at all?  maybe they should be in the NewUserServiceImpl (?)

    // Used in build() to find existing roles the person being built might have.
    RoleAssignmentDao roleDao = (RoleAssignmentDao) MivServiceLocator.getBean("roleAssignmentDao");

    AppointmentDao apptDao = (AppointmentDao) MivServiceLocator.getBean("appointmentDao");



// An entirely different strategy ...

    private BuiltPersonImpl built; // the thing we'll be building
    private PersonDTO builderPDTO = null;
    private MivAccountDto builderADTO = null;
//    private Collection<AssignedRole> roles = null; // XXX: SDP - getting these ourselves in 'build()'

    /**
     * A PersonBuilder can only be obtained via {@link #newBuilder()}
     */
    private PersonBuilder()
    {
        // no-arg PersonBuilder can only be obtained via call to newBuilder()
        super();
        logger.debug("|| -- Creating a PersonBuilder with No-Arg Constructor.");
    }


    /**
     * Get a PersonBuilder to start building a person object.<br>
     * Use like: <code>PersonBuilder.newBuilder().withAccount(dto).build();</code>
     * @return a new PersonBuilder ready to build an MivPerson
     */
    public static PersonBuilder newBuilder()
    {
        return new PersonBuilder();
    }


    public PersonBuilder withPersonData(PersonDTO dto)
    {
        checkState(this.builderPDTO == null, "a Person data object has already been set [%s]", this.builderPDTO);
        this.builderPDTO = dto;
        return this;
    }


    public PersonBuilder withAccount(MivAccountDto dto)
    {
        checkState(this.builderADTO == null, "an Account data object has already been set [%s]", this.builderADTO);
        this.builderADTO = dto;
        return this;
    }


    public MivPerson build()
    {
        checkState(this.builderADTO != null || this.builderPDTO != null, // or both != null
                   "Neither of a Person object or an Account object was provided to build a person");


        built = new BuiltPersonImpl(builderADTO, builderPDTO);

        if (built.getDisplayName() == null)
        {
            String displayName = this.buildDisplayName(built);
            built.setDisplayName(displayName);
        }

        int userId = built.getUserId();

        Collection<AssignedRole> roles = roleDao.getAssignedRoles(userId);
        for (AssignedRole r : roles)
        {
            built.addRole(r);
            if (r.isPrimary()) {
                built.setPrimaryRole(r);
            }
        }


        Collection<Appointment> appts = apptDao.getAppointments(userId);
        for (Appointment appt : appts)
        {
            built.addAppointment(appt);
        }


        return built;
    }


    /**
     * A person may have a "preferred" name to display, but if they don't we have to build one.
     * @param person the person for whom to build a "preferred" display name
     * @return a String combining names available from Given, Middle, and Surnames, and any Suffix
     */
    private String buildDisplayName(final MivPerson person)
    {
        return (
                   (StringUtils.isNotEmpty(person.getGivenName()) ? person.getGivenName() : "")
                   + (StringUtils.isNotEmpty(person.getMiddleName()) ? " " + person.getMiddleName() : "")
                   + (StringUtils.isNotEmpty(person.getSurname()) ? " " + person.getSurname() : "")
                   + (StringUtils.isNotEmpty(person.getSuffix()) ? " " + person.getSuffix() : "")
               );
    }


    /**
     * Build an Account DTO for the given person.
     * @param person person for whom to build the DTO.
     * @return the account DTO holding the data for the given person.
     */
    public static MivAccountDto buildDto(MivPerson person)
    {
        MivAccountDto dto = new MivAccountDto();

        dto.setUserId(person.getUserId());
        List<Principal> principals = person.getPrincipals();
        dto.setLogin( principals != null && principals.size() > 0
                      ? principals.get(0).getPrincipalName()
                      : Principal.EMPTY_PRINCIPAL.getPrincipalName() );
        dto.setPersonUuid(person.getPersonId());

        Collection<AssignedRole> home = Collections2.filter(person.getAssignedRoles(), new Predicate<AssignedRole>() {
            @Override
            public boolean apply(AssignedRole input)
            {
                //New appointees are not MIV_USERs but need a home
                EnumSet<MivRole> validRoles = EnumSet.of(MivRole.MIV_USER, MivRole.APPOINTEE);
                return validRoles.contains(input.getRole());
            }
        });
        AssignedRole homeRole = home.iterator().next();
        Scope homeScope = homeRole.getScope();

        dto.setSchoolId(homeScope.getSchool());
        dto.setDepartmentId(homeScope.getDepartment());

        dto.setSurname(person.getSurname());
        dto.setGivenName(person.getGivenName());
        dto.setMiddleName(person.getMiddleName());
        dto.setSuffix(person.getSuffix());
        dto.setPreferredName(person.getDisplayName());

        dto.setEmail(person.getPreferredEmail());

        dto.setActive(person.isActive());

        return dto;
    }



}
