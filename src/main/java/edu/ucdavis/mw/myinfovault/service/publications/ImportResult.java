/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ImportResult.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.publications.Publication;
import edu.ucdavis.mw.myinfovault.util.SelectedFilter;

/**
 * Holds results and statistics about a publications
 * import. May be made generic for use elsewhere.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class ImportResult extends StatResult implements Serializable
{
    private static final long serialVersionUID = 201105161059L;

    private static final SelectedFilter<Publication> selectedFilter = new SelectedFilter<Publication>();

    private final Collection<ParseResult> parseResults;
    private final int realUser;
    private int persisted = 0;

    /**
     * Create a new import result for a list of parse results.
     *
     * @param realUser The real, logged-in MIV user ID performing this import
     * @param parseResults The parse results and statistics
     * @param importAll Import all publications parsed from file if <code>true</code>, otherwise, only the selected (@see edu.ucdavis.mw.myinfovault.util.SelectedFilter)
     */
    protected ImportResult(int realUser,
                           List<ParseResult> parseResults,
                           boolean importAll)
    {
        this.realUser = realUser;
        this.parseResults = parseResults;

        //aggregate all parse result statistics and results
        for (ParseResult parseResult : parseResults)
        {
            processed += parseResult.getProcessed();
            duplicates += parseResult.getDuplicates();
            errors.addAll(parseResult.getErrors());

            //apply selected filter to publications if not import all
            publications.addAll(importAll
                              ? parseResult.getPublications()
                              : selectedFilter.apply(parseResult.getPublications()));
        }
    }

    /**
     * @return The real, logged-in MIV user ID performing this import
     */
    protected int getRealUser()
    {
        return realUser;
    }

    /**
     * @return Number of publications successfully persisted to the database
     */
    public int getPersisted()
    {
        return persisted;
    }

    /**
     * @param persisted Number of publications successfully persisted to the database
     */
    void setPersisted(int persisted)
    {
        this.persisted = persisted;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "Import Report" +
               "\n-------------" +
               "\nReal user: " + realUser +
               "\n\nTotal Persisted: " + persisted +
               listParseReports();
    }

    //return a list of parse results for report
    private String listParseReports()
    {
        StringBuilder list = new StringBuilder();

        for (ParseResult parseResult : parseResults)
        {
            list.append("\n\n" + parseResult);
        }

        return list.toString();
    }
}
