/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ParseError.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;

import java.io.Serializable;

/**
 * Encapsulates a parse error.
 *
 * @author Craig Gilmore
 * @since MIV 4.4.2
 */
public class ParseError implements Serializable
{
    private static final long serialVersionUID = 201107271030L;

    private String id;
    private Exception exception;

    /**
     * Create a parse error.
     *
     * @param id Reference identifier for the error
     * @param exception Cause of the error
     */
    protected ParseError(String id, Exception exception)
    {
        this.id = id;
        this.exception = exception;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "[ID:" + id + "] " +
               (exception.getCause() != null
              ? exception.getCause()
              : exception);
    }
}
