/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PatentType.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;


/**
 * TODO: add javadoc
 * 
 * @author Pradeeph Haldiya
 * @since
 */
public enum PatentType
{
    /**
     * TODO: add javadoc
     */
    PATENT        (1,"Patent",""),
    
    /**
     * TODO: add javadoc
     */
    DISCLOSURE    (2, "Disclosure", ""),
    
    /**
     * TODO: add javadoc
     */
    INVALID_TYPE  (-1,"Invalid","");

    private int typeId;
    private String title;
    private String description;

    /**
     * TODO: add javadoc
     * 
     * @param typeId
     * @param title
     * @param description
     */
    private PatentType(int typeId, String title, String description)
    {
        this.typeId = typeId;
        this.title = title;
        if (description == null || description.trim().length() == 0)
        {
            this.description = title;
        }
        else
        {
            this.description = description;
        }
    }

    /**
     * @return patent type ID
     */
    public int getTypeId()
    {
        return typeId;
    }

    /**
     * @return patent type title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @return patent type description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Returns the enum corresponding to the typeId
     * @param typeId
     * @return PatentType enum
     */
    public static PatentType getPatentTypeById(int typeId)
    {
        PatentType[] enumlist = PatentType.values();
        for (PatentType patentType : enumlist)
        {
            if (patentType.getTypeId() == typeId) { return patentType; }
        }

        return PatentType.INVALID_TYPE;
    }

}
