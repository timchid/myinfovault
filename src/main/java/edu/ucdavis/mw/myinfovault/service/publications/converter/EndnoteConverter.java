/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EndnoteConverter.java
 */

package edu.ucdavis.mw.myinfovault.service.publications.converter;

import edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource;

/**
 * @author Stephen Paulsen
 * @since MIV 2.2
 */
@Deprecated
public class EndnoteConverter extends PublicationConverter
{
    public EndnoteConverter()
    {
        super(PublicationSource.ENDNOTE);
    }
}
