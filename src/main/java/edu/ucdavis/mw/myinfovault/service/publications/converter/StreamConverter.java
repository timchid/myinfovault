/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: StreamConverter.java
 */

package edu.ucdavis.mw.myinfovault.service.publications.converter;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/**
 * Defines a stream converter.
 * 
 * @author Stephen Paulsen
 * @since MIV 2.2
 */
public interface StreamConverter
{
    /**
     * Read, convert, and write the character stream.
     * 
     * @param in character stream from which to read 
     * @param out character stream to which to write
     * @throws IOException if unable to read or write
     */
    public void convert(Reader in, Writer out) throws IOException;
}
