/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RafServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.raf;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.ucdavis.mw.myinfovault.dao.aa.AcademicActionDao;
import edu.ucdavis.mw.myinfovault.dao.raf.RafDao;
import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.Department;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo.Acceleration;
import edu.ucdavis.mw.myinfovault.domain.raf.Rank;
import edu.ucdavis.mw.myinfovault.domain.raf.Status;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.DocumentService;
import edu.ucdavis.mw.myinfovault.service.decision.DecisionService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.Validator;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Governs the use of {@link RafBo} business objects.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class RafServiceImpl extends DocumentService implements RafService
{
    /**
     * Recommended action form style filename.
     */
    private static String XSLT_FILENAME = MIVConfig.getConfig().getProperty("document-config-location-xslt-raf");

    /*
     * Field labels used in RAF XML.
     */
    private static String EFFECTIVE_DATE = "Effective Date";
    private static String RETROACTIVE_DATE = "Retroactive Date";
    private static String END_DATE = "End Date";

    private final RafDao rafDao;
    private final AcademicActionDao aaDao;
    private final DecisionService decisionService;

    /**
     * Creates the recommend action form service implementation bean.
     *
     * @param rafDao Spring injected RAF DAO
     * @param aaDao Spring injected academic action DAO
     * @param decisionService Spring-injected decision service
     */
    public RafServiceImpl(RafDao rafDao,
                          AcademicActionDao aaDao,
                          DecisionService decisionService)
    {
        this.rafDao = rafDao;
        this.aaDao = aaDao;
        this.decisionService = decisionService;

        EventDispatcher2.getDispatcher().register(this);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.raf.RafService#getBo(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public RafBo getBo(Dossier dossier)
    {
        RafBo raf = rafDao.getRaf(dossier);

        // create new RAF if none yet exists
        return raf != null ? raf : new RafBo(dossier);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.raf.RafService#rafExists(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public boolean rafExists(Dossier dossier)
    {
        return dossier != null && rafDao.getRaf(dossier) != null;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.raf.RafService#saveRaf(edu.ucdavis.mw.myinfovault.domain.raf.RafBo, edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public AcademicAction saveRaf(RafBo raf,
                                  MivPerson targetPerson,
                                  MivPerson realPerson)
    {
        // Get fresh dossier object from database
        AcademicAction formerAction = getDossier(raf.getDossier().getDossierId()).getAction();

        /*
         * If delegation authority or action type has changed, remove all decision signatures.
         */
        if (raf.getDossier().getAction().getDelegationAuthority() != formerAction.getDelegationAuthority()
         || raf.getDossier().getAction().getActionType() != formerAction.getActionType())
        {
            List<MivDocument>excludeDocs = new ArrayList<MivDocument>();
            // If only the delegation authority has changed, *do not* remove the disclosure certificate signatures
            if (raf.getDossier().getAction().getActionType() == formerAction.getActionType())
            {
                excludeDocs.add(MivDocument.DISCLOSURE_CERTIFICATE);
                excludeDocs.add(MivDocument.JOINT_DISCLOSURE_CERTIFICATE);
            }
            decisionService.removeDecisions(raf.getDossier(), excludeDocs);
        }

        // using RAF for this dossier, not AAF
        raf.getDossier().setAcademicActionFormPresent(false);
        raf.getDossier().setRecommendedActionFormPresent(true);

        // the new RAF (if it exists) is no longer needed
        aaDao.delete(raf.getDossier().getAcademicActionID());

        // persist RAF with changes
        rafDao.persistRaf(raf, realPerson.getUserId());

        // update RAF PDFs
        createPdfs(raf);

        // update dossier with RAF changes
        dossierService.updateDossier(raf.getDossier(), targetPerson, realPerson);

        return formerAction;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.raf.RafService#createPdfs(edu.ucdavis.mw.myinfovault.domain.raf.RafBo)
     */
    @Override
    public void createPdfs(final RafBo raf)
    {
        Document doc = getDocument();

        doc.appendChild(getRafElement(doc, raf));

        // Create the RAF PDF
        createPdf(raf.getDossier().getDossierPdf(MivDocument.RAF),
                  XSLT_FILENAME,
                  doc);

        // regenerate VP and dean signature documents
        for (Decision decision : decisionService.getDecisions(raf.getDossier()))
        {
            decisionService.createPdf(decision);
        }
    }

    /**
     * Creates and returns the RAF node.
     *
     * @param doc
     * @param raf
     * @return
     */
    private Element getRafElement(final Document doc, final RafBo raf)
    {
        // Create root node
        final Element root = doc.createElement("raf");

        // Append children
        root.appendChild(getElement(doc, "documentname", MivDocument.RAF.getDescription()));
        root.appendChild(getElement(doc, "candidatename", raf.getDossier().getAction().getCandidate().getDisplayName()));
        root.appendChild(getElement(doc, "effectivedate", longDateFormat.get().format(raf.getDossier().getAction().getEffectiveDate()), EFFECTIVE_DATE));
        root.appendChild(getElement(doc, "yearsatrank", Integer.toString(raf.getYearsAtRank())));
        root.appendChild(getElement(doc, "yearsatstep", Integer.toString(raf.getYearsAtStep())));
        root.appendChild(getElement(doc, "actiondescription", raf.getDossier().getAction().toString()));

        if (raf.getDossier().getAction().getRetroactiveDate() != null)
        {
            root.appendChild(getElement(doc, "retroactivedate", longDateFormat.get().format(raf.getDossier().getAction().getRetroactiveDate()), RETROACTIVE_DATE));
        }

        if (raf.getDossier().getAction().getEndDate() != null)
        {
            root.appendChild(getElement(doc, "enddate", longDateFormat.get().format(raf.getDossier().getAction().getEndDate()), END_DATE));
        }

        if (raf.isAppointmentType9mo())
        {
            root.appendChild(getElement(doc, "appointmenttype9mo"));
        }

        if (raf.isAppointmentType11mo())
        {
            root.appendChild(getElement(doc, "appointmenttype11mo"));
        }

        if (raf.getAcceleration() == Acceleration.ACCELERATION)
        {
            root.appendChild(getElement(doc, "accelerationyears", Integer.toString(raf.getAccelerationYears())));
        }
        else if (raf.getAcceleration() == Acceleration.DECELERATION)
        {
            root.appendChild(getElement(doc, "decelerationyears", Integer.toString(raf.getDecelerationYears())));
        }
        else
        {
            root.appendChild(getElement(doc, "normal"));
        }

        // Append departments and statues
        root.appendChild(getDepartmentsElement(doc, raf.getDepartments()));
        root.appendChild(getStatusesElement(doc, raf.getStatuses()));

        return root;
    }

    /**
     * Creates and returns the departments node.
     *
     * @param doc
     * @param departments
     * @return
     */
    private Element getDepartmentsElement(final Document doc, final List<Department> departments)
    {
        // Create root node
        final Element root = doc.createElement("departments");

        // Loop over departments
        for (final Department department: departments)
        {
            // Create department element
            Element elem = doc.createElement("department");

            // Add percent symbol to percentOfTime
            String percentOfTime = department.getPercentOfTime();
            if (Validator.isFloat(percentOfTime)) percentOfTime += "%";

            // Append children
            elem.appendChild(getElement(doc, "name", !StringUtils.isBlank(department.getDepartmentName())
                                                   ? (department.getSchoolName() +" - "+department.getDepartmentName())
                                                   : department.getSchoolName()));
            elem.appendChild(getElement(doc, "type", department.getDepartmentType().toString()));
            elem.appendChild(getElement(doc, "percentoftime", percentOfTime));

            // Append to root
            root.appendChild(elem);
        }

        return root;
    }

    /**
     * Creates and returns the statuses node.
     *
     * @param doc
     * @param statuses
     * @return
     */
    private Element getStatusesElement(final Document doc, final List<Status> statuses)
    {
        // Create root node
        final Element root = doc.createElement("statuses");

        // Loop over statuses
        for (final Status status: statuses)
        {
            // Create status element
            Element elem = doc.createElement("status");

            // Append children
            elem.appendChild(getElement(doc, "type", status.getStatusType().getDescription()));

            // Append ranks
            elem.appendChild(getRanksElement(doc, status.getRanks()));

            // Append to root
            root.appendChild(elem);
        }

        return root;
    }

    /**
     * Creates and returns the ranks node.
     *
     * @param doc
     * @param ranks
     * @return
     */
    private Element getRanksElement(final Document doc, final List<Rank> ranks)
    {
        // Create root node
        final Element root = doc.createElement("ranks");

        // Loop over ranks
        for (final Rank rank: ranks)
        {
            // Create rank element
            Element elem = doc.createElement("rank");

            // Add percent symbol to percentOfTime
            String percentOfTime = rank.getPercentOfTime();
            if (Validator.isFloat(percentOfTime)) percentOfTime += "%";

            // Append children
            elem.appendChild(getElement(doc, "rankandstep", rank.getRankAndStep()));
            elem.appendChild(getElement(doc, "percentoftime", percentOfTime));
            elem.appendChild(getElement(doc, "titlecode", rank.getTitleCode()));
            elem.appendChild(getElement(doc, "monthlysalary", NumberFormat.getCurrencyInstance().format(rank.getMonthlySalary()).replace("$", "")));
            elem.appendChild(getElement(doc, "annualsalary", NumberFormat.getCurrencyInstance().format(rank.getAnnualSalary()).replace("$", "")));

            // Append to root
            root.appendChild(elem);
        }

        return root;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.raf.RafService#deleteRaf(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public void deleteRaf(Dossier dossier)
    {
        // Remove all RAF decision signatures
        decisionService.removeDecisions(dossier);

        // Remove RAF PDF
        File dossierFilePath = dossier.getDossierPdf(MivDocument.RAF);
        if (dossierFilePath.exists()) dossierFilePath.delete();

        // Remove RAF entry from the database
        rafDao.deleteRaf(dossier.getDossierId());

        // set the dossier RAF flag if needed
        if (dossier.isRecommendedActionFormPresent())
        {
            // Set dossier RAF flag
            dossier.setRecommendedActionFormPresent(false);

            // Re-persist the dossier
            try
            {
                dossierService.saveDossier(dossier);
            }
            catch (final WorkflowException exception)
            {
                throw new MivSevereApplicationError("errors.raf.dossier.write2", exception, dossier.getDossierId());
            }
        }
    }
}
