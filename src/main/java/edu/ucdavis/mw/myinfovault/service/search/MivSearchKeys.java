/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivPersonSearchKeys.java
 */


package edu.ucdavis.mw.myinfovault.service.search;

import java.io.Serializable;

import edu.ucdavis.iet.commons.ldap.domain.util.LdapPersonConstants;

/**
 * Search keys that can be used in a criteria AttributeSet.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class MivSearchKeys /*extends UCDKIMEntitySearchKeys*/ implements Serializable
{
    private static final long serialVersionUID = 20090618151631L;

    /** Keys used to search for People */
    public static class Person
    {
        /** The presence of this key limits searches to only active users. The value assigned to the key is ignored. */
        public static final String MIV_ACTIVE_USER = "Active";

        public static final String ENTITY_ID = "entityId";

        public static final String PERSON_UUID = ENTITY_ID;

        /**
         * Primary school ID search key.
         */
        public static final String MIV_SCHOOL_CODE = "SchoolID";

        public static final String MIV_DEPARTMENT_CODE = "DepartmentID";

        public static final String MIV_ROLE = "MivRoleIdentifier";

        public static final String FIRST_NAME = "GivenName";
        public static final String LAST_NAME = "Surname";
        public static final String MIDDLE_NAME = "MiddleName";

        /** Seach the display/preferred name, which doesn't necessarily match the official given or surnames. */
        public static final String DISPLAY_NAME = LdapPersonConstants.DISPLAY_NAME;

        /** Seach the DISPLAY_NAME, FIRST_NAME, MIDDLE_NAME, and LAST_NAME. */
        public static final String NAME = LdapPersonConstants.FULL_NAME;

        public static final String EMAIL_ADDRESS = "Email";
    }

    // The following would work, even though EMAIL_ADDRESS isn't mentioned above,
    // because it *is* in the super-class "UCDKIMEntitySearchKeys.Person"
    //   public String getEmailKey() { return Person.EMAIL_ADDRESS; }
}
