/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignableService.java
 */

package edu.ucdavis.mw.myinfovault.service.signature;

import java.text.MessageFormat;

import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.domain.signature.Signable;
import edu.ucdavis.mw.myinfovault.service.DocumentService;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.PropertyManager;

/**
 * Shared service for signable document services.
 * 
 * @author Craig Gilmore
 * @since MIV 4.7
 * @param <T> type of document to persist and request/remove signatures
 */
public abstract class SignableServiceImpl<T extends Signable> extends DocumentService implements SignableService<T>
{
    protected static final Logger log = Logger.getLogger(SignableServiceImpl.class);
    
    /**
     * Property key for the invalid signature message.
     */
    private static final String INVALID_SIGNATURE = "invalidSignature";

    protected final SigningService signingService;

    /**
     * Create a signable service bean.
     *
     * @param signingService Spring-injected signing service
     */
    public SignableServiceImpl(SigningService signingService)
    {
        this.signingService = signingService;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SignableService#persist(edu.ucdavis.mw.myinfovault.domain.signature.Signable, int)
     */
    @Override
    public abstract boolean persist(T document, int realUserId);

    /**
     * @return invalid signature message for the given document type
     */
    protected String getInvalidSignatureMessage(MivDocument type)
    {
        return MessageFormat.format(PropertyManager.getDefaultProperties().getProperty(INVALID_SIGNATURE),
                                    type.getDescription(),
                                    type.getAllowedSigner().getDescription());
    }
}
