/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SigningServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.signature;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.DocumentRoleFilter;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.domain.signature.Signable;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.MivDocument;


/**
 * Handles requesting and signing {@link MivElectronicSignature} and {@link Signable} objects.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class SigningServiceImpl implements SigningService
{
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final SignatureDao signatureDao;

    /**
     * Create the signing service bean.
     *
     * @param signatureDao signing DAO for signature persistence
     */
    public SigningServiceImpl(SignatureDao signatureDao)
    {
        this.signatureDao = signatureDao;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#requestSignature(edu.ucdavis.mw.myinfovault.domain.signature.Signable, int, int)
     */
    @Override
    public MivElectronicSignature requestSignature(Signable document,
                                                   int requestedUserId,
                                                   int realUserId) throws IllegalArgumentException
    {
        // can't request a signature for a null document
        if (document == null)
        {
            throw new IllegalArgumentException("User " + realUserId + " attempted to request a signature from user " + requestedUserId + " for a NULL document");
        }

        // Check if signature already exists
        MivElectronicSignature signature = getSignatureByDocumentAndUser(document, requestedUserId);

        // Create a new signature if DNE or invalid
        if (signature == null || !signature.isValid())
        {
            // delete old signature
            deleteSignature(signature);

            // create and persist new signature
            signature = new MivElectronicSignature(requestedUserId, document);
            signatureDao.persistSignature(signature, realUserId);
        }

        return signature;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getRequestedSignaturesByUser(int)
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignaturesByUser(int userId)
    {
        return signatureDao.getRequestedSignatures(userId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getRequestedSignatures(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignatures(Dossier dossier)
    {
        return signatureDao.getRequestedSignatures(dossier.getDossierId());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getRequestedSignatures(edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignatures(MivRole role)
    {
        return (new DocumentRoleFilter(role)).apply(signatureDao.getRequestedSignatures());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getRequestedSignatures(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.service.person.MivRole)
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignatures(MivPerson requested, MivRole capacity)
    {
        // return empty list if requested person does not have the given capacity
        return requested.hasRole(capacity)
             ? (new DocumentRoleFilter(capacity)).apply(getRequestedSignaturesByUser(requested.getUserId()))
             : Collections.<MivElectronicSignature>emptyList();
    }

    /**
     * Get all the signatures for the indicated document.
     *
     * @param document document that has been requested/signed
     * @return list of signatures for the given document
     */
    private List<MivElectronicSignature> getSignaturesByDocument(Signable document)
    {
        return document == null
             ? Collections.<MivElectronicSignature>emptyList()
             : signatureDao.getSignatures(document);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getSignature(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int, edu.ucdavis.mw.myinfovault.util.MivDocument, int)
     */
    @Override
    public MivElectronicSignature getSignature(Dossier dossier,
                                               int schoolId,
                                               int departmentId,
                                               MivDocument documentType,
                                               int userId)
    {
        return getSignatureByDocumentAndUser(getDocument(dossier,
                                                         schoolId,
                                                         departmentId,
                                                         documentType),
                                             userId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getSignatureById(int)
     */
    @Override
    public MivElectronicSignature getSignatureById(int recordId)
    {
        return signatureDao.getSignature(recordId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getSignatureByDocumentAndUser(edu.ucdavis.mw.myinfovault.domain.signature.Signable, int)
     */
    @Override
    public MivElectronicSignature getSignatureByDocumentAndUser(Signable document, int userId)
    {
        return document == null
             ? null
             : signatureDao.getSignature(userId, document);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getSignedSignatureByDocument(edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public MivElectronicSignature getSignedSignatureByDocument(Signable document)
    {
        return document == null
             ? null
             : signatureDao.getSignedSignature(document);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#sign(edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature, int)
     */
    @Override
    public void sign(MivElectronicSignature signature, int realUserId)
    {
        // If signature has been signed, invalidate so can be signed again
        if (signature.isSigned()) signature.invalidate();

        // sign the signatures associated signable document
        signature.sign(realUserId);

        // update the signable document if able to sign
        if (signatureDao.persistSignature(signature, realUserId))
        {
            // Remove previous signature requests from this document
            deleteSignatures(signatureDao.getRequestedSignatures(signature.getDocument()));

            // document type just signed
            MivDocument documentType = signature.getDocument().getDocumentType();

            /*
             * Persist the document just signed.
             *
             * MIV-4934; Disclosure certificate documents will not be persisted when
             * signed. This condition may be removed when MIV-4933 is implemented.
             */

            if (documentType == MivDocument.DISCLOSURE_CERTIFICATE
             || documentType == MivDocument.JOINT_DISCLOSURE_CERTIFICATE)
            {
                MivServiceLocator.getDCService().createPdf((DCBo)signature.getDocument());
            }
            else
            {
                MivServiceLocator.getDecisionService().persist((Decision)signature.getDocument(), realUserId);
            }

            // Record a log entry
            log.info("_AUDIT : User '{}' has signed {}on document #{} of type '{}' and school:department '{}:{}'",
                     new Object[] {realUserId,
                                   (realUserId != signature.getSignerId() ? "for user '"+ signature.getSignerId() + "' " : ""),
                                   signature.getDocument().getDocumentId(),
                                   signature.getDocument().getDocumentType(),
                                   signature.getDocument().getSchoolId(),
                                   signature.getDocument().getDepartmentId()});
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#invalidate(edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public void invalidate(MivElectronicSignature signature, MivPerson realPerson)
    {
        // can only invalidate a signed signature
        if (signature.isSigned())
        {
            signature.invalidate();

            signatureDao.persistSignature(signature, realPerson.getUserId());

            log.info("_AUDIT : User {} has invalidated signature {}", realPerson, signature);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#deleteSignatures(edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public Collection<MivElectronicSignature> deleteSignatures(Signable document)
    {
        return deleteSignatures(getSignaturesByDocument(document));
    }

    /**
     * Remove an electronic signature from the system.
     *
     * @param signatureId electronic signature ID
     * @return if a signature with the given ID was deleted
     */
    private boolean deleteSignature(int signatureId)
    {
        log.info("Deleting signature " + signatureId);

        return signatureDao.deleteSignature(signatureId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#deleteSignature(edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature)
     */
    @Override
    public boolean deleteSignature(MivElectronicSignature signature)
    {
        return signature != null && deleteSignature(signature.getRecordId());
    }

    /**
     * Remove a collection electronic signatures from the system.
     *
     * @param signatures electronic signature
     */
    private Collection<MivElectronicSignature> deleteSignatures(Collection<MivElectronicSignature> signatures)
    {
        for (MivElectronicSignature signature : signatures) deleteSignature(signature);

        return signatures;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#removeSignatureRequestByPrimaryUser(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public boolean removeSignatureRequestByPrimaryUser(final MivPerson mivPerson)
    {
        log.trace("========== Entering removeSignatureRequestByPrimaryUser method in SigningService ==========");
        log.info("Removing signature requests for {}", mivPerson);

        boolean removeFlag = false;
        try
        {
            /*
             * Check all the dossiers for the deactivated user
             * and delete any Signature Request for the dossier
             */
            for (Dossier dossier : MivServiceLocator.getDossierService().getDossierByUser(mivPerson))
            {
                log.info("\tProcessing dossier #{} ...", dossier.getDossierId());


                // Check for any signature request for the dossier scope
                for (MivElectronicSignature mivSignature : signatureDao.getRequestedSignatures(dossier.getDossierId(),
                                                                                                      dossier.getPrimarySchoolId(),
                                                                                                      dossier.getPrimaryDepartmentId()))
                {
                    log.info("\t\tDeleting signature:{}", mivSignature);

                    // signature was removed or had been in an earlier iteration
                    /*
                     * TODO: update signature document PDF
                     *
                     *       getService(mivSignature.getDocument().getDocumentType()).createPdf(mivSignature.getDocument());
                     */
                    removeFlag = deleteSignature(mivSignature) || removeFlag;
                }
            }
        }
        catch (IllegalStateException e)
        {
            log.error("IllegalStateException in {} while removing SignatureRequest for dossier(s)", this);
        }
        catch (WorkflowException e)
        {
            log.error("WorkflowException in {} while removing SignatureRequest for dossier(s)", this);
        }

        log.debug("========== Leaving removeSignatureRequestByPrimaryUser  method in SigningService ==========");
        return removeFlag;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getDocument(edu.ucdavis.mw.myinfovault.util.MivDocument, int)
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends Signable> T getDocument(MivDocument documentType, int documentId)
    {
        /*
         * Return the proper signable for the document type.
         */
        switch(documentType)
        {
            case DISCLOSURE_CERTIFICATE:
            case JOINT_DISCLOSURE_CERTIFICATE:
                return (T) MivServiceLocator.getDCService().getDisclosureCertificate(documentId);
            case VICEPROVOSTS_FINAL_DECISION:
            case VICEPROVOSTS_RECOMMENDATION:
            case PROVOSTS_RECOMMENDATION:
            case PROVOSTS_TENURE_RECOMMENDATION:
            case PROVOSTS_TENURE_DECISION:
            case PROVOSTS_FINAL_DECISION:
            case CHANCELLORS_FINAL_DECISION:
            case DEANS_FINAL_DECISION:
            case DEANS_RECOMMENDATION:
            case JOINT_DEANS_RECOMMENDATION:
            case DEANS_APPEAL_DECISION:
            case VICEPROVOSTS_APPEAL_DECISION:
            case VICEPROVOSTS_APPEAL_RECOMMENDATION:
            case PROVOSTS_APPEAL_RECOMMENDATION:
            case PROVOSTS_TENURE_APPEAL_RECOMMENDATION:
            case PROVOSTS_TENURE_APPEAL_DECISION:
            case PROVOSTS_APPEAL_DECISION:
            case CHANCELLORS_APPEAL_DECISION:
            case JOINT_DEANS_APPEAL_RECOMMENDATION:
                return (T) MivServiceLocator.getDecisionService().getDecision(documentId);
            default:
                return null;//no documents for this type
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getDocument(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int, edu.ucdavis.mw.myinfovault.util.MivDocument)
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T extends Signable> T getDocument(Dossier dossier,
                                              int schoolId,
                                              int departmentId,
                                              MivDocument documentType)
    {
        /*
         * Return the proper signable for the document type.
         */
        switch(documentType)
        {
            case DISCLOSURE_CERTIFICATE:
            case JOINT_DISCLOSURE_CERTIFICATE:
                return (T) MivServiceLocator.getDCService().getDisclosureCertificate(dossier, schoolId, departmentId);
            case VICEPROVOSTS_FINAL_DECISION:
            case VICEPROVOSTS_RECOMMENDATION:
            case PROVOSTS_RECOMMENDATION:
            case PROVOSTS_TENURE_RECOMMENDATION:
            case PROVOSTS_TENURE_DECISION:
            case PROVOSTS_FINAL_DECISION:
            case CHANCELLORS_FINAL_DECISION:
            case DEANS_FINAL_DECISION:
            case DEANS_RECOMMENDATION:
            case JOINT_DEANS_RECOMMENDATION:
            case DEANS_APPEAL_DECISION:
            case VICEPROVOSTS_APPEAL_DECISION:
            case VICEPROVOSTS_APPEAL_RECOMMENDATION:
            case PROVOSTS_APPEAL_RECOMMENDATION:
            case PROVOSTS_TENURE_APPEAL_RECOMMENDATION:
            case PROVOSTS_TENURE_APPEAL_DECISION:
            case PROVOSTS_APPEAL_DECISION:
            case CHANCELLORS_APPEAL_DECISION:
            case JOINT_DEANS_APPEAL_RECOMMENDATION:
                return (T) MivServiceLocator.getDecisionService().getDecision(dossier, documentType, schoolId, departmentId);
            default:
                return null;//no documents for this type
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getSignatureStatus(edu.ucdavis.mw.myinfovault.util.MivDocument, int)
     */
    @Override
    public SignatureStatus getSignatureStatus(MivDocument documentType, int documentId)
    {
        return getSignatureStatus(getDocument(documentType, documentId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SigningService#getSignatureStatus(edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public SignatureStatus getSignatureStatus(Signable document)
    {
        List<MivElectronicSignature> signatures = getSignaturesByDocument(document);

        // no signature requests for the document
        if (signatures.isEmpty()) return SignatureStatus.MISSING;

        for (MivElectronicSignature signature : signatures)
        {
            // document is signed
            if (signature.isSigned())
            {
                // document may only be signed or valid
                return signature.isValid()
                     ? SignatureStatus.SIGNED
                     : SignatureStatus.INVALID;
            }
        }

        // document has not been signed
        return SignatureStatus.REQUESTED;
    }
}
