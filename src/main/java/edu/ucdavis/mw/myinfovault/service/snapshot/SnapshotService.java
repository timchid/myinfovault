/**
 * Copyright © The Regents of the University of California, Davis campus. 
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SnapshotService.java
 */
package edu.ucdavis.mw.myinfovault.service.snapshot;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.SchoolDepartmentCriteria;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * TODO: add javadoc
 * 
 * @author Rick Hendricks
 * @since
 */
public interface SnapshotService
{
    /**
     * Create a snapshot of the dossier for the input MivRole.
     * The snapshot is qualified only by role. Content is not qualified by school and department.
     * Use createDossierSnapshot(Dossier dossier, MivRole role, SchoolDepartmentCriteria schoolDepartmentCriteria) to qualify 
     * by school and department.   
     * @param dossier The dossier to make into a snapshot 
     * @param role The role for which to make the snapshot
     * @return SnapshotDto object containing info for the snapshot created.
     * @throws WorkflowException
     */
    public SnapshotDto createDossierSnapshot(Dossier dossier, MivRole role) throws WorkflowException;
 
    /**
     * Create a snapshot of the dossier for the input MivRole.
     * @param dossier The dossier to make into a snapshot 
     * @param role The role for which to make the snapshot
     * @param schoolDepartmentCriteriaMap School and Role qualification for the created snapshot. Can be null if none 
     * @return SnapshotDto object containing info for the snapshot created.
     * @throws WorkflowException
     */
    public SnapshotDto createDossierSnapshot(Dossier dossier, MivRole role, Map<String, SchoolDepartmentCriteria> schoolDepartmentCriteriaMap) throws WorkflowException;

    /**
     * Create an archive snapshot of the dossier.
     * The snapshot content is not qualified School or Department.
     * The redacted or non-redacted documents are include based on role qualification.
     * @param dossier The dossier to make into a snapshot
     * @param role The role for which the snapshot is to be created
     * @param name The name of the snapshot
     * @return SnapshotDto object containing info for the snapshot created
     * @throws WorkflowException
     */
    public SnapshotDto createArchiveSnapshot(Dossier dossier, MivRole role, String name) throws WorkflowException;

    /**
     * Get a snapshot for the input dossier and  role.
     * @param dossier The dossier for which to retrieve the snapshot 
     * @param mivRole The role for which to retrieve the snapshot
     * @param schoolId The schoolId for which to retrieve the snapshot
     * @param departmentId The departmentId for which to retrieve the snapshot
     * @return SnapshotDto object containing info for the snapshot created
     * @throws WorkflowException
     */
    public SnapshotDto getSnapshot(Dossier dossier, MivRole mivRole, int schoolId, int departmentId) throws WorkflowException;

    /**
     * Get all snapshots for the input dossier
     * @param dossier The dossier for which to retrieve the snapshots 
     * @return SnapshotDto objects
     * @throws WorkflowException
     */
    public List <SnapshotDto> getAllSnapshots(Dossier dossier) throws WorkflowException;

    /**
     * Delete snapshots for a dossier. All dossier snapshots may be deleted, or just the 
     * snapshots for a specified location may be deleted.
     * location, or all snapshot may
     * @param dossier dossier for which snapshots are to me deleted
     * @param location dossier workflow location for which to delete snapshots. If the DossierLocation is set to null, all snapshots for the dossier will be deleted. 
     * @throws WorkflowException 
     * @throws SQLException 
     */
    public void deleteSnapshots(Dossier dossier, DossierLocation location) throws WorkflowException, SQLException;

}
