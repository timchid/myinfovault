/**
 * Copyright © The Regents of the University of California, Davis campus. 
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SnapshotType.java
 */
package edu.ucdavis.mw.myinfovault.service.snapshot;

/**
 * TODO: add javadoc
 * 
 * @author Rick Hendricks
 * @since
 */
public enum SnapshotType
{
    /**
     * Dossier snapshot.
     */
    DOSSIER ("Dossier"),
    
    /**
     * Candidate archive snapshot.
     */
    CANDIDATE_ARCHIVE("Candidate Archive"),
    
    /**
     * Administrator archive snapshot.
     */
    ADMIN_ARCHIVE("Admin Archive"),
    
    /**
     * Full archive snapshot.
     */
    FULL_ARCHIVE("Full Archive");

    /**
     * FIXME: should be private with getter
     */
    public final String description;
    
    /**
     * Initialize the description  for the snapshot type.
     * 
     * @param description snapshot type description
     */
    private SnapshotType(String description)
    {
        this.description = description;
    }
}
