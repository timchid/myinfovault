/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AbstractMivPersonAwareSuggestionHandler.java
 */

package edu.ucdavis.mw.myinfovault.service.suggest;

import java.util.Properties;
import java.util.Set;

import com.google.common.collect.Sets;

import edu.ucdavis.mw.common.service.suggest.AbstractSuggestionHandler;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * {@link MivPerson} aware suggestion handler with caching and search term tokenization.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 * @param <T> type of returned suggestions
 */
public abstract class AbstractPersonAwareSuggestionHandler<T> extends AbstractSuggestionHandler<T> implements PersonAwareSuggestionHandler<T>
{
    /**
     * Create the suggestion handler for the given name and topic.
     *
     * @param name handler name
     * @param topic handler topic
     * @param p configuration properties
     */
    protected AbstractPersonAwareSuggestionHandler(String name, String topic, Properties p)
    {
        super(name, topic, p);
    }

    /*
     *
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.suggest.MivPersonAwareSuggestionHandler#getSuggestions(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, java.lang.String)
     */
    @Override
    final public Set<T> getSuggestions(MivPerson person, String topic, String term)
    {
        return getSuggestions(person, topic, term, tokenize);
    }

    /**
     * Get suggestions for the given topic and search term.
     *
     * @param person person requesting suggestions
     * @param topic suggestion topic
     * @param term search term
     * @param tokenize if search term is to be tokenized
     * @return suggestions for the given topic and search term
     */
    private Set<T> getSuggestions(MivPerson person,
                                  String topic,
                                  String term,
                                  boolean tokenize)
    {
        // ignore search terms that are null or too short
        if (term == null || term.length() < minTermLength) return null;

        String cacheKey = topic + "-" + term + "-" + person.getPersonId();

        // get suggestions from cache
        Set<T> suggestions = suggestionCache.get(cacheKey);

        // if none, get some
        if (suggestions == null)
        {
            // should the term be tokenized?
            if (tokenize)
            {
                /*
                 * Split term in two:
                 *  - Head, an alphabetic token
                 *  - Tail, the rest of the term
                 */
                String[] tokens = term.split(NON_ALPHABETIC_RE, 2);

                // get suggestions from the head token
                suggestions = getSuggestions(person, topic, tokens[0], false);

                // if the head token yielded valid suggestions
                if (suggestions != null)
                {
                    // recurse if head suggestions aren't empty and there's a tail
                    if (!suggestions.isEmpty() && tokens.length > 1)
                    {
                        // get suggestions from the tail
                        Set<T> tailSuggestions = getSuggestions(person, topic, tokens[1]);

                        // if the tail yields valid suggestions
                        if (tailSuggestions != null)
                        {
                            // find the intersection of head and tail suggestions
                            suggestions = Sets.intersection(suggestions, tailSuggestions);
                        }
                    }
                }
                // otherwise, get suggestions from the tail if exists
                else if (tokens.length > 1)
                {
                    suggestions = getSuggestions(person, topic, tokens[1]);
                }
            }
            else
            {
                suggestions = Sets.newHashSet(getNewSuggestions(person, topic, term));
            }

            // add suggestions to cache
            suggestionCache.put(cacheKey, suggestions);
        }

        return suggestions;
    }

    /**
     * Get new suggestions for the given topic and search term.
     *
     * @param person person requesting the suggestions
     * @param topic suggestion topic
     * @param term search term
     * @return suggestions for the given topic and search term
     */
    protected abstract Iterable<T> getNewSuggestions(MivPerson person, String topic, String term);
}
