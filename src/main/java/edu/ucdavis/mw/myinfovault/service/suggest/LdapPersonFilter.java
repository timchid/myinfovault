/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LdapPersonFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.suggest;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.myinfovault.MIVConfig;

/**
 * Filters out LDAP persons also in MIV by personUUID.
 *
 * @author Craig Gilmore
 * @since 4.8
 */
public class LdapPersonFilter extends SearchFilterAdapter<LdapPerson>
{
    /**
     * Every PersonUUID for each MIV account.
     */
    private static final String SQL = "SELECT DISTINCT PersonUUID from UserAccount where PersonUUID IS NOT NULL";

    private final Set<String> personUUIDs = new HashSet<String>();

    /**
     * Create filter to remove LDAP persons also in MIV.
     */
    public LdapPersonFilter()
    {
        for (Map<String, String> record : MIVConfig.getConfig().getQueryTool().getList(SQL))
        {
            personUUIDs.add(record.get("personuuid"));
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(LdapPerson item)
    {
        return !personUUIDs.contains(item.getUcdPersonUUID());
    }
}
