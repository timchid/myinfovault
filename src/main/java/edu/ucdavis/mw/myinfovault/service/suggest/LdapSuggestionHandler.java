/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LdapSuggestionHandler.java
 */

package edu.ucdavis.mw.myinfovault.service.suggest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.springframework.ldap.NameNotFoundException;

import com.google.common.collect.ImmutableMap;

import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.iet.commons.ldap.service.LdapPersonService;
import edu.ucdavis.mw.common.service.suggest.AbstractSuggestionHandler;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;

/**
 * Serves suggestions from LDAP.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class LdapSuggestionHandler extends AbstractSuggestionHandler<Map<String,String>>
{
    private final LdapPersonService ldapPersonService = MivServiceLocator.getLdapPersonService();

    private final String attribute;
    private final boolean filtermiv;

    /**
     * Create a suggestion handler for the given handler name, topic, and properties.
     *
     * @param name handler name
     * @param topic handler topic
     * @param p handler properties
     */
    public LdapSuggestionHandler(String name, String topic, Properties p)
    {
        super(name, topic, p);

        /*
         * LDAP search attribute.
         */
        this.attribute = getOption("attribute");

        /*
         * Filter out MIV users?
         */
        this.filtermiv = Boolean.parseBoolean(getOption("filtermiv"));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.suggest.AbstractSuggestionHandler#getNewSuggestions(java.lang.String, java.lang.String)
     */
    @Override
    protected Iterable<Map<String,String>> getNewSuggestions(String topic, String term)
    {
        try
        {
            List<LdapPerson> ldapPeople = ldapPersonService.findByCriteria(ImmutableMap.<String, String>builder()
                                                                                       .put(attribute, term)
                                                                                       .build(),
                                                                           MatchType.EXACT == this.matchType);
            // apply MIV filter
            if (this.filtermiv)
            {
                ldapPeople = new LdapPersonFilter().apply(ldapPeople);
            }

            List<Map<String, String>> suggestions = new ArrayList<Map<String, String>>(ldapPeople.size());

            for (LdapPerson person : ldapPeople)
            {
                suggestions.add(getSuggestion(person));
            }

            return suggestions;
        }
        catch (NameNotFoundException e)
        {
            // no suggestions found
            return Collections.emptySet();
        }
    }

    /**
     * Get immutable map suggestion for given LDAP person with the display name as the label,
     * UCD Person UUID as the value, surname, given name, and middle name.
     *
     * <p>e.g. <code>{"label"     : "Harry Caray",
     *                "value"     : "34582",
     *                "surname"   : "Carabina",
     *                "givenname" : "Harold"}</code></p>
     *
     * @param person LDAP person
     * @return label, value, surname, and givenname keyed suggestion map
     */
    private Map<String, String> getSuggestion(LdapPerson person)
    {
        return ImmutableMap.<String, String>builder()
                           .putAll(getSuggestion(person.getDisplayName(),
                                                 person.getUcdPersonUUID()))
                           .put("surname", StringUtils.join(person.getSn(), " "))
                           .put("givenname", person.getGivenName())
                           .build();
    }
}
