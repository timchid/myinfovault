/**
 * Copyright © The Regents of the University of California, Davis campus. 
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivSpringContext.java
 */
package edu.ucdavis.mw.myinfovault.system;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/* This class provides an application-wide access to the
 * Spring ApplicationContext! The ApplicationContext is
 * injected in a static method of the class "MivAppContext".
 *
 * Use AppContext.getApplicationContext() to get access
 * to all Spring Beans.
 *
 */ 

public class MivSpringContext implements ApplicationContextAware { 
	public void setApplicationContext(ApplicationContext ctx) throws BeansException { 
		// Wiring the ApplicationContext into a static method 
		MivApplicationContext.setApplicationContext(ctx); 
	} 
} // .EOF  

