/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AttributeSet.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Local copy replacement for edu.ucdavis.mw.myinfovault.util.AttributeSet
 * @author Stephen Paulsen
 *
 */
public class AttributeSet extends HashMap<String, String>
{
    private static final long serialVersionUID = 201206041226L;


    public AttributeSet() {
        super();
    }

    public AttributeSet(int initialSize) {
        super(initialSize);
    }

    public AttributeSet(String key, String value) {
        this();
        put(key, value);
    }


    public AttributeSet(Map<String, String> map) {
        super();
        if (map != null) {
            putAll(map);
        }
    }

}
