/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BatchPager.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;


/**
 * Governs paging over a selectable list of records lazily added in batches.
 *
 * @author Craig Gilmore
 * @param <T> Object within collection to be paged
 * @since MIV 4.4.2
 */
public class BatchPager<T> extends Pager<T> implements Serializable
{
    private static final long serialVersionUID = 201206131620L;

    private final int recordCount;
    private int batchCount = 0;

    /**
     * Create a batch paging object with a default page size.
     *
     * @param recordCount Total number of potential records to page over
     * @throws IllegalArgumentException
     */
    public BatchPager(int recordCount) throws IllegalArgumentException
    {
        super(new ArrayList<T>(recordCount));

        this.recordCount = recordCount;
    }

    /**
     * Add another batch of records to page.
     *
     * @param records additional batch of records
     */
    public void addBatch(Collection<T> records)
    {
        getRecords().addAll(records);

        batchCount++;
    }

    /**
     * Is another batch of records needed for the current page?
     *
     * @return <code>true</code> if another batch is needed, <code>false</code> otherwise
     */
    public boolean needBatch()
    {
        /*
         * Need another batch if the number of batch records is less than both:
         *  - the last record index on the current page
         *  - the total record count
         */
        return super.getRecordCount() < getRecordIndex() + getPageSize()
            && super.getRecordCount() < getRecordCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRecordCount()
    {
        return recordCount;
    }

    /**
     * Get the number of batches added to the pager.
     *
     * @return Number of batches added
     */
    public int getBatchCount()
    {
        return batchCount;
    }
}
