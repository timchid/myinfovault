/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CSVParser.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.Ostermiller.util.ExcelCSVParser;
import com.Ostermiller.util.LabeledCSVParser;

/**
 * Defines an {@link Iterable} collection of column label to column value maps for a CSV stream or file.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class CSVIteration implements Iterable<Map<String, String>>
{
    private final List<Map<String, String>> parsed = new ArrayList<Map<String, String>>();

    /**
     * Create an {@link Iterable} collection of column label to column value maps for a CSV file.
     *
     * @param csv CSV file
     * @throws FileNotFoundException if unable to find/access the CSV file
     * @throws IOException if unable to read the file
     */
    public CSVIteration(File csv) throws FileNotFoundException, IOException
    {
        this(new FileReader(csv));
    }

    /**
     * Create an {@link Iterable} collection of column label to column value maps for a CSV stream.
     *
     * @param in CSV character stream
     * @throws IOException if unable to read from stream
     */
    public CSVIteration(Reader in) throws IOException
    {
        LabeledCSVParser csv = new LabeledCSVParser(new ExcelCSVParser(in));

        String[] labels = csv.getLabels();
        for (int i=0; i<labels.length; i++)
        {
            labels[i] = labels[i].toLowerCase();
        }

        String[] values;
        while ((values = csv.getLine()) != null)
        {
            Map<String, String> row = new HashMap<String, String>();

            for (int i=0; i<values.length; i++)
            {
                row.put(labels[i], values[i]);
            }

            parsed.add(row);
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Iterable#iterator()
     */
    @Override
    public Iterator<Map<String, String>> iterator()
    {
        return parsed.iterator();
    }
}
