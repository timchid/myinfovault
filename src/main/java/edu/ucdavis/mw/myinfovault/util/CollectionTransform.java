/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ListTransform.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Contains static methods for transforming a collection from one generic type to another.
 *
 * @author Craig Gilmore
 * @since MIV 4.4.2
 */
public class CollectionTransform
{
    private final static Transformer<String,Integer> stringToInteger = new Transformer<String,Integer>() {
        @Override
        protected Integer transform(String item)
        {
            return Integer.parseInt(item);
        }
    };

    /**
     * Transforms a collection of Strings to Integers.
     *
     * @param input Collection of Strings
     * @return Collection of Integers
     */
    public static Collection<Integer> stringToInteger(Collection<String> input)
    {
        return stringToInteger.transform(input);
    }

   /**
    * Transform collections from one generic type to another.
    *
    * @author Craig Gilmore
    * @since MIV 4.4.2
    * @param <T> Type to transform to
    * @param <F> Type transformed from
    */
   public static abstract class Transformer<F,T>
   {
       /**
        * Transforms a collection of items of one type to a collection of another.
        *
        * @param input Collection of items to transform
        * @return Transformed collection
        */
       public Collection<T> transform(Collection<F> input)
       {
           Collection<T> output = new ArrayList<T>(input.size());

           for (F item : input)
           {
               output.add(transform(item));
           }

           return output;
       }

       /**
        * Transforms item from one type to another.
        *
        * @param item Object to transform
        * @return Transformed object
        */
       protected abstract T transform(F item);
   }
}
