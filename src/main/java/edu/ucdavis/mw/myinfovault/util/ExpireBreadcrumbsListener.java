/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ExpireBreadcrumbsListener.java
 */

package edu.ucdavis.mw.myinfovault.util;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.web.ControllerBase;


/**
 * Session listener to expire breadcrumbs for an MVC controller session
 *
 * @author rhendric
 */
public class ExpireBreadcrumbsListener implements HttpSessionListener
{
    protected final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
     */
    @Override
    public void sessionCreated(HttpSessionEvent paramHttpSessionEvent)
    {
        // no need to do anything here.
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent paramHttpSessionEvent)
    {
        logger.info("Removing breadcrumbs for session id {}", paramHttpSessionEvent.getSession().getId());
        ControllerBase.removeBreadcrumbs(paramHttpSessionEvent.getSession().getId());
    }
}




