/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FileUtil.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Formatter;

import org.apache.log4j.Logger;

import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * @author Stephen Paulsen
 *
 */
public class FileUtil
{
    /**<p>
     * Copy a file from source to destination, swallowing all exceptions. A boolean indicator is returned
     * telling whether or not the copy succeeded.</p>
     * <p>The destination file is truncated before the copy takes place; even if the copy fails
     * <strong>the contents of the destination file will be lost</strong>.</p>
     * <p>Taken more or less from The Java Developers Almanac 1.4
     * <a href="http://www.exampledepot.com/egs/java.nio/File2File.html">http://www.exampledepot.com/egs/java.nio/File2File.html</a></p>
     *
     * @param src source File to copy from
     * @param dest destination File to copy to
     * @return true if the copy succeeded, false if it failed
     */
    public static boolean copyFile(File src, File dest)
    {
        boolean copied = false;
        FileInputStream fis = null;
        FileOutputStream fos = null;
        FileChannel srcChannel = null;
        FileChannel destChannel = null;

        try {
            // Create channel on the source
//            srcChannel = new FileInputStream(src).getChannel(); // Break into 2 statements so both can be closed; otherwise a resource leak will result.
            fis = new FileInputStream(src);
            srcChannel = fis.getChannel();

            // Create channel on the destination
//            destChannel = new FileOutputStream(dest).getChannel();
            fos = new FileOutputStream(dest);
            destChannel = fos.getChannel();

            // Copy file contents from source to destination
            final long size = srcChannel.size();
            destChannel.truncate(0);
            long bytesCopied = destChannel.transferFrom(srcChannel, 0, size);

            if (bytesCopied < size) {
                Formatter f = new Formatter();
                f.format("Incomplete copy: %d bytes of %d total copied from %s to %s",
                        bytesCopied, size, src, dest);
                String msg = f.out().toString();
                System.err.println(msg);
                Logger.getLogger(MIVUtil.class).warn(msg);
                f.close();
            }

            copied = true;
        }
        catch (IOException e) {
            final String msg = "Failed to copy [" + src + "] to [" + dest + "]";
            Logger.getLogger(MIVUtil.class).warn(msg, e);
        }
        finally {
            // Close the channels
            if (destChannel != null) try { destChannel.close(); } catch (IOException e) {/*ignore*/}
            if (srcChannel != null) try { srcChannel.close(); } catch (IOException e) {/*ignore*/}
            if (fos != null) try { fos.close(); } catch (IOException e) {/*ignore*/}
            if (fis != null) try { fis.close(); } catch (IOException e) {/*ignore*/}
        }

        return copied;
    }


    /**
     * Returns the byte array for a file or null if the file does not exist.
     *
     * @param file the file for which a byte array is requested
     * @return an array of bytes
     */
    public static byte[] getFileBytes(File file)
    {
        byte[] bytes = new byte[(int) file.length()];

        try
        {
            new FileInputStream(file).read(bytes);
        }
        catch (FileNotFoundException exception)
        {
            return null;
        }
        catch (IOException exception)
        {
            throw new MivSevereApplicationError("errors.FileViewer.IOError", exception, file.toString());
        }

        return bytes;
    }

}
