/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: HttpServletUtil.java
 */
package edu.ucdavis.mw.myinfovault.util;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for HttpServlet
 *
 * @author pradeeph
 * @since v4.6
 *
 */
public class HttpServletUtil
{
    private static Logger logger = LoggerFactory.getLogger(HttpServletUtil.class);

    /**
     * To indicate whether or not this is an AJAX request
     *
     * @param request
     * @return
     */
    public static boolean isAjaxRequest(HttpServletRequest request)
    {
        boolean ajaxRequest =false;

        // Check for the "Requested With" header that indicates an AJAX call.
        boolean foundRequestedWith = false;
        String requestType = request.getHeader("Requested-With");
        if (requestType != null)
        {
            foundRequestedWith = true;
            logger.debug("Found the Requested-With header, which says [{}]", requestType);
        }
        // Check for the experimental version of the header if the plain one wasn't found.
        if (!foundRequestedWith)
        {
            requestType = request.getHeader("X-Requested-With");
            if (requestType != null)
            {
                foundRequestedWith = true;
                logger.debug("Found the X-Requested-With header, which says [{}]", requestType);
            }
        }

        // Indicate whether or not this is an AJAX request
        ajaxRequest = foundRequestedWith && "XMLHttpRequest".equalsIgnoreCase(requestType);

        if (ajaxRequest)
        {
            logger.debug("This is assumed to be an AJAX request and, therefore, client-side JavaScript is enabled.");
        }

        return ajaxRequest;
    }
}
