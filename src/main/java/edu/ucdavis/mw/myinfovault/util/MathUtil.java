/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MathUtil.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.math.BigDecimal;

/**
 * @author Stephen Paulsen
 *
 */
public class MathUtil
{
    /**
     * Round a double to specified decimal places
     * @param number number to round
     * @param places number of decimal places to round
     * @return rounded double
     */
    public static double round(double number, int places)
    {
        double mod = Math.pow(10.0, places);
        return Math.round(number*mod)/mod;
    }

    /**
     * Is the number divisible by the given divisor?
     *
     * @param number number value
     * @param divisor divisor value
     * @return if the number is divisible by the given divisor
     * @throws NullPointerException if the number or divisor is <code>null</code>
     */
    public static boolean isDivisibleBy(BigDecimal number, BigDecimal divisor)
    {
        /*
         * TRUE if the divisor is non-zero and the remainder of
         * the division of the number by the divisor is zero.
         */
        return divisor.compareTo(BigDecimal.ZERO) != 0
            && number.remainder(divisor).compareTo(BigDecimal.ZERO) == 0;
    }
}
