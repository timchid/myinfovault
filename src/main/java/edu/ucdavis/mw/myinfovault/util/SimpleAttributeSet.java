/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SimpleAttributeSet.java
 */


package edu.ucdavis.mw.myinfovault.util;

import java.util.HashMap;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;


/**<p>
 * Subclass of the Kuali AttributeSet to provide an easy one-liner constructor.
 * Rather than creating the attribute set then adding attributes to it, this can be
 * created and populated with one constructor:</p>
 * <pre>
 * AttributeSet s1 = new AttributeSet();
 * s1.add("fname", "Stephen");
 * s1.add("lname", "Paulsen");
 * s1.add("department", "ADDA");</pre>
 *  vs.
 * <pre>
 * AttributeSet s2 = new SimpleAttributeSet("fname=Stephen", "lname=Paulsen", "department=ADDA");</pre>
 *<p>
 * Kuali's AttributeSet is itself a subclass of HashMap&lt;String,String&gt;
 *</p>
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class SimpleAttributeSet extends edu.ucdavis.mw.myinfovault.util.AttributeSet
//public class SimpleAttributeSet extends edu.ucdavis.mw.myinfovault.util.AttributeSet
{
    private static final long serialVersionUID = 200903241122L;

    /**
     * @see HashMap#HashMap()
     */
    public SimpleAttributeSet()
    {
        super();
    }

    /**
     * @see HashMap#HashMap(int)
     */
    public SimpleAttributeSet(int initialSize)
    {
        super(initialSize);
    }

    /**
     * Create a SimpleAttributeSet from an existing AttributeSet
     * @see HashMap#HashMap(java.util.Map)
     * @param set the AttributeSet whose mappings are to be placed in this set
     */
    public SimpleAttributeSet(AttributeSet set)
    {
        super(set);
    }

    public SimpleAttributeSet(Map<String,String> map)
    {
        super(map);
    }


    /**
     * Easy constructor to create and populate an AttributeSet.
     * @param pairs Any number of strings in the form "attribute=value"
     */
    public SimpleAttributeSet(String... pairs)
    {
        this();

        for (String pair : pairs)
        {
            String[] p = pair.split("[ ]*=[ ]*", 2);
            if (p.length != 2) {
                throw new IllegalArgumentException("Malformed name=value pair: " + pair);
            }
            this.put(p[0], p[1]);
        }
    }


    /**<p>
     * Easy constructor to create and populate as AttributeSet.
     * Allows a String with "attribute=value" pairs separated by either comma
     * or semi-colon such as <kbd>"attribute=value;attribute=value"</kbd></p>
     * <p>For example: <code>AttributeSet a = new SimpleAttributeSet("School=38, Department=313");</code>
     * @param attributes
     */
    public SimpleAttributeSet(String attributes)
    {
        this(attributes.split("[ ]*[,;][ ]*"));
    }


    /** Test the class */
    public static void main(String[] args)
    {
        // Strings to use if no command line args were given.
        String[] noArgs = { "this=that", "foo=bar=baz", "a=nother", "required=true" };
        if (args == null || args.length == 0) args = noArgs;

        System.out.println("The raw args are:");
        for (String a : args) {
            System.out.println("  " + a);
        }

        try {
            AttributeSet s = new SimpleAttributeSet(args);
            System.out.println("The resulting attribute set: " + s);
            for (String key : s.keySet())
            {
                System.out.printf("  key: %1$s\t\tvalue: %2$s\n", key, s.get(key));
            }
        }
        catch (IllegalArgumentException e) {
            System.out.println("Illegal Parameter -- " + e.getMessage());
        }

        // Test using literal strings
        AttributeSet a = new SimpleAttributeSet("one=1", "two=2", "three=3");
        System.out.println("The resulting attribute set: " + a);
        for (String key : a.keySet())
        {
            System.out.printf("  key: %1$s\t\tvalue: %2$s\n", key, a.get(key));
        }

        // Test using multi-attribute strings
        System.out.println("Multi-Attribute Tests");
        String[] testStrings = { "School=38  ;  Department=313", "foo=one,bar=two", "one = 1   ,   two = 2   ,   three = 3" };
        for (String s : testStrings)
        {
            System.out.printf("Input string is \"%s\"\n", s);
            AttributeSet multi = new SimpleAttributeSet(s);
            System.out.println("The resulting attribute set: " + multi);
        }
    }
}
