/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: XmlManager.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jdom2.CDATA;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.util.StringUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Manager class for XML operations.
 *
 * @author Pradeep Haldiya
 * @since MIV 4.0
 */
public abstract class XmlManager
{
    /**
     * Used in {@link DocumentBuilderFactory#setFeature(String, boolean)}.
     *
     * @see http://xerces.apache.org/xerces2-j/features.html
     */
    private static final String LOAD_DTD_GRAMMAR_FEATURE = "http://apache.org/xml/features/nonvalidating/load-dtd-grammar";
    private static final String LOAD_EXTERNAL_DTD_FEATURE = "http://apache.org/xml/features/nonvalidating/load-external-dtd";

    /**
     * Default, strict parsing document builder.
     */
    private static final DocumentBuilder documentBuilder;

    /**
     * Lenient parsing document builder that ignores <abbr title="Document type definition">DTD</abbr>.
     */
    private static final DocumentBuilder lenientDocumentBuilder;

    static {
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

            documentBuilder = factory.newDocumentBuilder();

            // set factory features to ignore DTDs
            factory.setFeature(LOAD_DTD_GRAMMAR_FEATURE, false);
            factory.setFeature(LOAD_EXTERNAL_DTD_FEATURE, false);

            lenientDocumentBuilder = factory.newDocumentBuilder();

        }
        catch (ParserConfigurationException e)
        {
            throw new MivSevereApplicationError("Unable to create document builder with custom features", e);
        }
    }


    /**
     * Create instance of XML Element.
     *
     * @param elementName name for the element
     * @return XML element for the given name
     */
    public static Element createElement(String elementName)
    {
        return StringUtils.hasText(elementName) ? new Element(elementName) : null;
    }


    /**
     * Cast JDOM Document Object into String.
     *
     * @param document DOM document
     * @return JDOM string representation of the DOM document
     */
    public static String documentToString(Document document)
    {
        return document == null ? null : new XMLOutputter(Format.getPrettyFormat()).outputString(document);
    }


    /**
     * Cast text into {@link CDATA} Object.
     *
     * @param Text
     * @return {@link CDATA} text
     */
    public static CDATA getCDATA(String Text)
    {
        return new CDATA(Text);
    }


    /**
     * Gets XML string for an XML document.
     *
     * @param doc org.w3c.dom.Document <b>NOT</b> org.jdom.Document
     * @return XML string for the given document
     */
    public static String getXmlStr(org.w3c.dom.Document doc)
    {
        StringWriter sw = new StringWriter();

        transformXml(doc, new StreamResult(sw));

        return sw.toString();
    }


    /**
     * Write XML document from URL resource to file.
     *
     * @param url Location of the XML resource
     * @param output File to which XML is to be written
     * @throws SAXException If parse error occurs
     * @throws IOException If unable to open the stream
     */
    public static void writeXml(URL url, File output) throws SAXException, IOException
    {
        //transformXml(getDocument(url), new StreamResult(output));
        writeXml(getDocument(url), output);
    }


    /**
     * Write XML document to file.
     *
     * @param doc org.w3c.dom.Document <b>NOT</b> org.jdom.Document
     * @param output File to which XML is to be written
     */
    public static void writeXml(org.w3c.dom.Document doc, File output)
    {
        transformXml(doc, new StreamResult(output));
    }


    /**
     * Transform the XML document to the given result.
     * @param doc
     * @param result
     */
    private static void transformXml(org.w3c.dom.Document doc, Result result)
    {
        try
        {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");

            transformer.transform(new DOMSource(doc), result);
        }
        catch (TransformerConfigurationException exception)
        {
            throw new MivSevereApplicationError("errors.utility.xmlString", exception);
        }
        catch (TransformerException exception)
        {
            throw new MivSevereApplicationError("errors.utility.xmlString", exception);
        }
    }


    /**
     * Get an XML document <em>strictly</em> parsed from the stream from the given URL.
     *
     * @param url Location of the XML resource
     * @return <b>NOT</b> org.jdom.Document
     * @throws SAXException If parse error occurs
     * @throws IOException If unable to open the stream
     */
    public static org.w3c.dom.Document getDocument(URL url) throws SAXException, IOException
    {
        return getDocument(url, false);
    }

    /**
     * Get an XML document parsed from the stream from the given URL.
     *
     * @param url Location of the XML resource
     * @param ignoreDTD if parsing should ignore <abbr title="Document type definition">DTD</abbr>
     * @return <b>NOT</b> org.jdom.Document
     * @throws SAXException If parse error occurs
     * @throws IOException If unable to open the stream
     */
    public static org.w3c.dom.Document getDocument(URL url, boolean ignoreDTD) throws SAXException, IOException
    {
        /*
         * 1. connect to the resource associated with the URL
         * 2. encapsulate stream within an input source via a buffered reader
         * 3. parse to DOM from input source
         */
        return (ignoreDTD ? lenientDocumentBuilder : documentBuilder).parse(
                   new InputSource(
                       new BufferedReader(
                           new InputStreamReader(
                               url.openStream()))));
    }
}
