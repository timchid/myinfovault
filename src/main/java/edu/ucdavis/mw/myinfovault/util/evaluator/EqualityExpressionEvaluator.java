/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EqualityExpressionEvaluator.java
 */

package edu.ucdavis.mw.myinfovault.util.evaluator;

import java.text.ParseException;

/**
 * <p>Evaluates an expression using the super class {@link ExpressionEvaluator}.
 * Equalities found within expressions are evaluated using the
 * {@link EqualityEvaluator} required for instantiation.</p>
 *
 * <p>
 * <strong>Legal operators:</strong>
 * <ul>
 * <li>{@link ExpressionEvaluator} operators</li>
 * <li>"=" (Equals)</li>
 * <li>"!=" (Not equals)</li>
 * </ul>
 * </p>
 *
 * <p>
 * <strong>Example:</strong>
 * <pre>(SOMETHING=this|SOMEONE!=me)&!(SOMETHING=that&SOMEONE!=you)</pre>
 * </p>
 *
 * @author Craig Gilmore
 * @since MIV 4.6.2
 */
public class EqualityExpressionEvaluator extends ExpressionEvaluator
{
    private static final String EQUALS = "=";

    static {
        LEXICON.add(EQUALS);
    }

    private final EqualityEvaluator evaluator;

    /**
     * Create an equality expression evaluator with the given equality evaluator.
     *
     * @param evaluator used to evaluate equalities found in expressions
     */
    public EqualityExpressionEvaluator(EqualityEvaluator evaluator)
    {
        this.evaluator = evaluator;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.evaluator.ExpressionEvaluator#parseValue()
     */
    @Override
    protected boolean parseValue() throws ParseException
    {
        return parseEqual(parseOperand());
    }

    /**
     * Parse an equality.
     *
     * @param LHS left-hand operand for comparison
     * @return the evaluation of the equality
     * @throws ParseException if unable to parse or evaluate the equality
     */
    private boolean parseEqual(String LHS) throws ParseException
    {
        if (isNext(NOT))
        {
            return parseNotEqual(LHS);
        }

        read(EQUALS);

        return evaluator.evaluate(LHS, parseOperand());
    }

    /**
     * Parse a negative equality.
     *
     * @param LHS left-hand operand for comparison
     * @return the evaluation of the negative equality
     * @throws ParseException if unable to parse or evaluate the negative equality
     */
    private boolean parseNotEqual(String LHS) throws ParseException
    {
        read(NOT);

        return !parseEqual(LHS);
    }

    /**
     * Parse and return the value of the next token as an operand.
     *
     * @return the value of the operand
     * @throws ParseException if unable to parse or evaluate the operand
     */
    private String parseOperand() throws ParseException
    {
        return read();
    }
}
