/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LiteralEvaluator.java
 */

package edu.ucdavis.mw.myinfovault.util.evaluator;

import java.text.ParseException;

/**
 * Implement this interface to evaluate the boolean value of literals found
 * expressions. Additionally, a literal may have been flagged in the expression.
 *
 * @author Craig Gilmore
 * @since MIV 4.6.2
 */
public interface LiteralEvaluator
{
    /**
     * Evaluates the literal for a boolean value.
     *
     * @param literal representation to parse and evaluate
     * @return the evaluation, either <code>true</code> or <code>false</code>
     * @throws ParseException if the evaluator is unable to parse the literal
     */
    public boolean evaluate(String literal) throws ParseException;

    /**
     * Evaluates the literal for a boolean value.
     *
     * @param literal representation to parse and evaluate
     * @param flagged has the literal been flagged?
     * @return the evaluation, either <code>true</code> or <code>false</code>
     * @throws ParseException if the evaluator is unable to parse the literal
     */
    public boolean evaluate(String literal, boolean flagged) throws ParseException;
}
