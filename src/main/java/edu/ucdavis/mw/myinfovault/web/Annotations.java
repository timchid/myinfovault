/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AnnotationLine.java
 */

package edu.ucdavis.mw.myinfovault.web;

import static edu.ucdavis.mw.myinfovault.domain.annotation.Notation.INCLUDED;
import static edu.ucdavis.mw.myinfovault.domain.annotation.Notation.MENTORING;
import static edu.ucdavis.mw.myinfovault.domain.annotation.Notation.REFEREED;
import static edu.ucdavis.mw.myinfovault.domain.annotation.Notation.SIGNIFICANT;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;

import edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao;
import edu.ucdavis.mw.myinfovault.domain.annotation.Annotation;
import edu.ucdavis.mw.myinfovault.domain.annotation.AnnotationLine;
import edu.ucdavis.mw.myinfovault.domain.annotation.Notation;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.util.DateUtil;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.DisplaySection;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.QueryTool;
import edu.ucdavis.myinfovault.data.RecordHandler;
import edu.ucdavis.myinfovault.data.SectionFetcher;
import edu.ucdavis.myinfovault.designer.DisplayOptions;
import edu.ucdavis.myinfovault.format.FormatManager;
import edu.ucdavis.myinfovault.format.GreekFixer;
import edu.ucdavis.myinfovault.format.QuoteCleaner;

/**
 * Annotations servlet.
 *
 * @author Benny Poon, 2002-04-03
 * @author Stephen Paulsen, 2007-04-20
 * @author Pradeep K Haldiya, 2013-09-26
 * @author Stephen Paulsen, 2015-07-15
 */
public class Annotations extends MIVServlet
{
    private static final long serialVersionUID = 200704201510L;

    QueryTool qt = MIVConfig.getConfig().getQueryTool();

    private static final String PLACE_BEFORE = "1";
    private static final String PLACE_AFTER = "0";
    private static final String LEFT_JUSTIFY = "0";
    private static final String RIGHT_JUSTIFY = "1";


    private static final List<String> notationRequiredList = Arrays.asList("publications", "creativeactivities");

    // Get the sectionId for this record type name
    private static final Map<String, Map<String, String>> sectionidByNameMap =
            MIVConfig.getConfig().getMap("sectionidbyname");
    private static final Map<String, Map<String, String>> tableByRectypeMap =
            MIVConfig.getConfig().getMap("tablebyrectype");

    private static final Joiner withComma = Joiner.on(',').skipNulls();

    AnnotationDao annotationDao = (AnnotationDao) MivServiceLocator.getBean("annotationDao");
    PacketService packetService = MivServiceLocator.getPacketService();

    private final QuoteCleaner quoteCleaner = new QuoteCleaner();

    private boolean isDesignLines = false;
    private boolean isDesignExtensive = false;

    private final String[] annotationFieldList = { "RecordID", "PacketID", "SectionBaseTable", "Footnote", "Notation", "Display"};
    private final String[] annotationLineFields = { "AnnotationID", "Label", "RightJustify", "DisplayBefore"};
    private final String ANNOTATION_TABLE = "Annotations";
    private final String ANNOTATION_LINE_TABLE = "AnnotationLines";


    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        logger.trace("\n ***** Annotations doGet called! *****\t\tGET\n");
        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();

        req.setCharacterEncoding("UTF-8");
        res.setContentType("text/html");
        res.setCharacterEncoding("UTF-8");

        if (logger.isDebugEnabled()) showReqParameters(req);

        // Get params and guard against XSS and SQL attacks
        String recordID = MIVUtil.sanitize(req.getParameter("recid"));
        String recType = MIVUtil.sanitize(req.getParameter("type"));
        String sectionName = MIVUtil.sanitize(req.getParameter("section"));
        String annotationType = MIVUtil.sanitize(req.getParameter("annotationtype"));
        String displayParam = MIVUtil.sanitize(req.getParameter("display"));
        String actionType = MIVUtil.sanitize(req.getParameter("actionType"));
        String packetID = MIVUtil.sanitize(req.getParameter("packetid"));

        // treat display == null or empty as "true"
        boolean display = StringUtils.isEmpty(displayParam) || StringUtil.parseBoolean(displayParam);

        String modeParam = req.getParameter("mode");
        isDesignExtensive = "extensive".equalsIgnoreCase(modeParam);
        isDesignLines = !isDesignExtensive;

        // If we're in design mode, we have a recordID, and we're not saving
        if ( isDesignLines && recordID != null && ! "save".equalsIgnoreCase(actionType) )
        {
            Map<String, String> previewRecord = mui.getRecordHandler().getSectionRecord(recType, sectionName, recordID);

            if (!previewRecord.isEmpty())
            {
                new FormatManager(mui).applyFormats(recType, previewRecord, new GreekFixer());// , null);

                // Get any annotations attached to the record.
                // Use the annotation dao to retrieve annotation records
                Annotation annotation = annotationDao.
                                        getAnnotation(mui.getPerson().getUserId(),
                                                      Integer.parseInt(recordID),
                                                      Long.parseLong(packetID),
                                                      recType);

                Map<String, String> annotationRecordMap = createAnnotationRecordMap(annotation);

                // Add the record preview for the publication being managed
                annotationRecordMap.put("preview", previewRecord.get("preview"));

                // Used to display PDF included Records
                annotationRecordMap.put("display", previewRecord.get("display"));

                // and provide the whole mess to the JSP
                req.setAttribute("rec", annotationRecordMap);
            }
        } // recordID!=null

        java.io.PrintWriter out = res.getWriter();

        req.setAttribute("recid", recordID);
        req.setAttribute("type", recType);
        req.setAttribute("section", sectionName);
        req.setAttribute("annotationname", MIVConfig.getConfig().getProperty(annotationType + "-annotation-name"));
        req.setAttribute("annotationtype", annotationType);
        req.setAttribute("display", display);
        req.setAttribute("packetid", packetID);
        req.setAttribute("annotationmessage", !packetID.equals("0") ? MIVConfig.getConfig().getProperty(annotationType + "-annotation-packet-message") : "");

        include("/jsp/annotationlineform.jsp", req, res);

        // New Preview Area
        showLinePreviews(mui, annotationType, display, recType, sectionName, recordID, packetID, out);

        out.println("<!-- main div closing --></div> <!-- main div closing -->"); // Is it??

        include("/jsp/mivfooter.jsp", req, res);

        out.println("</body>");
        out.println("</html>");

        logger.trace("\n ***** Leaving Annotations doGet! *****\t\tGET\n");
    }
    // doGet()


    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        logger.trace("\n ***** Annotations doPost called! *****\t\tPOST\n");
        req.setCharacterEncoding("UTF-8");
        res.setContentType("text/html");
        res.setCharacterEncoding("UTF-8");

        String requestType = MIVUtil.sanitize(req.getParameter("requestType"));
        String save = MIVUtil.sanitize(req.getParameter("Save"));
        String cancel = MIVUtil.sanitize(req.getParameter("Cancel"));

        if ("save-annotations".equals(requestType)) {

            if (StringUtils.isNotBlank(save)) {
                saveAnnotation(req,res);
            }
            else if (StringUtils.isNotBlank(cancel)) {
                String annotationType = MIVUtil.sanitize(req.getParameter("annotationtype"));
                String displayParam = MIVUtil.sanitize(req.getParameter("display"));
                String packetID = MIVUtil.sanitize(req.getParameter("packetid"));

                // treat display == null or empty as "true"
                boolean display = StringUtils.isEmpty(displayParam) || StringUtil.parseBoolean(displayParam);

                String destination = req.getContextPath() + "/" + className +
                        "?annotationtype=" + annotationType + "&packetid="+packetID + (!display ? "&display=false" : "");

                res.sendRedirect(destination);
            }

        }
        else if ("save-record-visibility".equals(requestType)) {
            saveRecordVisibility(req, res);
        }
        else if ("save-notations".equals(requestType)) {
            saveNotations(req, res);
        }
    }


    /**
     * create annotation record map for jsp
     *
     * @param annotation
     */
    private Map<String, String> createAnnotationRecordMap(Annotation annotation)
    {
        Map<String, String> map = new HashMap<>();

        if (annotation == null) return map;

        map.put("annotationId", Integer.toString( annotation.getId() ));

        map.put("fIncluded",    Boolean.toString( annotation.hasNotation(INCLUDED) ));
        map.put("fSignificant", Boolean.toString( annotation.hasNotation(SIGNIFICANT) ));
        map.put("fMentoring",   Boolean.toString( annotation.hasNotation(MENTORING) ));
        map.put("fRefereed",    Boolean.toString( annotation.hasNotation(REFEREED) ));

        // Clean up the footnote if any
        final String footnote = MIVUtil.escapeMarkup(annotation.getFootnote());
        map.put("footnote", footnote);

        if (annotation.hasLabelAbove())
        {
            AnnotationLine labelAbove = annotation.getLabelAbove();

            map.put("idBefore", String.valueOf(labelAbove.getId()));
            map.put("labelBefore", MIVUtil.escapeMarkup(quoteCleaner.format(labelAbove.getLabel())));
            map.put("justifyBefore", String.valueOf(labelAbove.getRightJustify()));
            map.put("displayBefore", String.valueOf(labelAbove.getDisplayBefore()));
        }

        if (annotation.hasLabelBelow())
        {
            AnnotationLine labelBelow = annotation.getLabelBelow();

            map.put("idAfter", String.valueOf(labelBelow.getId()));
            map.put("labelAfter", MIVUtil.escapeMarkup(quoteCleaner.format(labelBelow.getLabel())));
            map.put("justifyAfter", String.valueOf(labelBelow.getRightJustify()));
            map.put("displayAfter", String.valueOf(labelBelow.getDisplayBefore()));
        }

        return map;
    }


    /**
     * Save multiple notations that have been batch updated.
     * @param req
     * @param res
     * @throws IOException
     */
    private void saveNotations(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
        try {
            int loggedInUserId = getSession(req, res).getUser().getUserId();
            int targetUserId   = getSession(req, res).getUser().getTargetUserInfo().getPerson().getUserId();

            String packetParam = req.getParameter("packetid");
            if (packetParam == null || packetParam.trim().length() == 0) packetParam = "0";  // assume Master packet ID=0 if param not passed.
            long packetId = Long.parseLong( packetParam );

            String cbStates = req.getParameter("cb_states");

            // Parse the received JSON into a list of maps...
            // Each map represents a selected record, with 'rectype' and 'recid' attributes in the map.
            String json = req.getParameter("selections");
            List<Map<String,String>> records = new ObjectMapper().readValue(
                                                   json, new TypeReference<List<Map<String,String>>>() {});

            Map<String,String> updates = new HashMap<>();
            for (String box : StringUtil.getListSplitter().split(cbStates))
            {
                String[] setting = box.split("=");
                updates.put(setting[0].toUpperCase(), setting[1].toLowerCase());
            }

            for (Map<String,String> rec : records)
            {
                String rectype = rec.get("rectype");
                int    recId = Integer.parseInt( rec.get("recid") );
                String table = tableByRectypeMap.get(rectype).get("sectionbasetable");
                logger.info("Process record #{} of type {} from {}", new Object[] { recId, rectype, table });

                Annotation a = annotationDao.getAnnotation(targetUserId, recId, packetId, rectype);
                // Create a new Annotation if no annotation was found, or if we're working on a Packet but we got back only a Master annotation.
                if (a == null || (packetId > 0 && a.getPacketId() == 0)) {
                    logger.debug("Creating a new Annotation because:\n\t(a == null [{}] || (packetId > 0 [{}] && a.getPacketId() == 0 [{}]))",
                                 new Object[] { a==null, packetId>0,
                                 a != null ? a.getPacketId()==0 : "null" });
                    a = new Annotation(-1, targetUserId, table, recId, packetId, null, null, true, null);
                    logger.debug("New Annotation is:\n {}", a);
                }

                // If this annotation is not for the packet we're processing then skip it.
                if (a.getPacketId() != packetId) {      // TODO: I don't think this will ever happen after the "if ()" change at line 336
                    logger.info("Wowzers! I didn't think this could happen anymore!\n\tSkipping update of {} because {} != {}",
                                new Object[] { a, a.getPacketId(), packetId });
                    continue;
                }

                for (String note : updates.keySet())
                {
                    String choice = updates.get(note);
                    try {
                        Notation n = Notation.valueOf(note);
                        if ("checked".equals(choice)) {
                            a.addNotation(n);
                        }
                        else {
                            a.removeNotation(n);
                        }
                    }
                    catch (IllegalArgumentException e) {
                        // Something that did not match a Notation enum was passed. Just skip it.
                        logger.warn("An unknown notation \"{}\" with checkbox state \"{}\" was passed.", note, choice);
                    }
                }

                //  Persist the annotation 'a' which will do an insert, update, or delete as necessary.
                annotationDao.persistAnnotation(a, packetId, targetUserId, loggedInUserId);
            }

            if (isAjaxRequest())
            {
                PrintWriter writer = res.getWriter();
                writer.print("{ \"success\":true }");
            }
        }
        catch (Exception e) {
            logger.error("Exception while trying to save multiple notation updates", e);
            PrintWriter writer = res.getWriter();
            writer.print("{ \"success\":false }");
        }
    }


    /**
     * Save record visibility into the dossier
     *
     * @param req
     * @param res
     * @throws IOException
     */
    private void saveRecordVisibility(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();

        String recordID = MIVUtil.sanitize(req.getParameter("recid"));
        String sectionName = MIVUtil.sanitize(req.getParameter("section"));
        String displayRecord = MIVUtil.sanitize(req.getParameter("display-record"));
        String packetId = MIVUtil.sanitize(req.getParameter("packetid"));
        logger.info("Save record display change for packet #{}", packetId);

        Map<String, String> sectionMap = sectionidByNameMap.get(sectionName);

        boolean success =
                this.setRecordVisibilityInDossier(mui.getLoginUser().getUserId(), Long.parseLong(packetId), Integer.parseInt(recordID), Integer.parseInt(sectionMap.get("id")), StringUtil.parseBoolean(displayRecord));

        if (isAjaxRequest())
        {
            PrintWriter writer = res.getWriter();
            writer.print("{ \"success\":" + success + " }");
        }
    }
    // saveRecordVisibility()


    /**
     * Save annotation for the section record
     *
     * @param req
     * @param res
     * @throws IOException
     */
    private void saveAnnotation(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
        boolean debug = logger.isDebugEnabled();

        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();
        String uid = Integer.toString(mui.getPerson().getUserId());
        int userId = Integer.parseInt(uid);
        RecordHandler rh = mui.getRecordHandler();

        String annotationId = MIVUtil.sanitize(req.getParameter("annotationId"));
        String recordID = MIVUtil.sanitize(req.getParameter("recid")); // The record ID these notations are attached to.
        String recType = MIVUtil.sanitize(req.getParameter("type"));
        String sectionName = MIVUtil.sanitize(req.getParameter("section"));
        String annotationType = MIVUtil.sanitize(req.getParameter("annotationtype"));
        String displayRecord = MIVUtil.sanitize(req.getParameter("display-record"));
        String displayParam = MIVUtil.sanitize(req.getParameter("display"));
        String packetID = MIVUtil.sanitize(req.getParameter("packetid"));

        // treat display == null or empty as "true"
        boolean display = StringUtils.isEmpty(displayParam) || StringUtil.parseBoolean(displayParam);

        boolean isMasterAnnotation = Long.parseLong(packetID) == 0;

        Map<String, String> sectionMap = sectionidByNameMap.get(sectionName);

        String labelBeforeRecID = req.getParameter("idBefore");
        String labelBefore = req.getParameter("labelBefore") != null ? req.getParameter("labelBefore").trim() : "";
        String justBefore = req.getParameter("justifyBefore");
        if (justBefore == null) justBefore = "right";
        String displayBefore = req.getParameter("displayBefore"); // 'display' true/false for the 'Before' label
        if (displayBefore == null || displayBefore.length() <= 0) displayBefore = "1";

        String labelAfterRecID = req.getParameter("idAfter");
        String labelAfter = req.getParameter("labelAfter") != null ? req.getParameter("labelAfter").trim() : "";
        String justAfter = req.getParameter("justifyAfter");
        if (justAfter == null) justAfter = "right";
        String displayAfter = req.getParameter("displayAfter"); // 'display' true/false for the 'After' label
        if (displayAfter == null || displayAfter.length() <= 0) displayAfter = "1";

        boolean rightJustBefore = "right".equals(justBefore);
        boolean rightJustAfter = "right".equals(justAfter);

        String footnote = "";
        if (!StringUtils.isEmpty(req.getParameter("footnote")))
        {
            footnote = req.getParameter("footnote").trim();
        }

        // Not available for master annotations. Assume false in that case.
        boolean fIncluded = isMasterAnnotation ? false : "on".equals(req.getParameter("included"));
        boolean fSignificant = isMasterAnnotation ? false : "on".equals(req.getParameter("significant"));
        // Not available for packet annotations. Assume false in that case.
        boolean fMentoring = isMasterAnnotation ? "on".equals(req.getParameter("mentoring")) : false;
        boolean fRefereed = isMasterAnnotation ? "on".equals(req.getParameter("refereed")) : false;

        String notation = new StringBuilder()
                            .append( fIncluded    ? INCLUDED.getMark() : "" )
                            .append( fSignificant ? SIGNIFICANT.getMark() : "" )
                            .append( fMentoring   ? MENTORING.getMark() : "" )
                            .append( fRefereed    ? REFEREED.getMark() : "" )
                        .toString();

        if (debug)
        {
            System.out.println("Record ID(s) : 1=[" + labelBeforeRecID + "] 2=[" + labelAfterRecID + "]");
            System.out.println(" lineBefore is [" + labelBefore + "], just is [" + justBefore + "]");
            System.out.println(" lineAfter  is [" + labelAfter + "], just is [" + justAfter + "]");
            System.out.println(" footnote   is [" + footnote + "]");
            System.out.println(" constructed notation string is \"" + notation + "\"");
        }


        String sectionBaseTable = sectionMap.get("sectionbasetable");

        // Set record visibility in packet
        if (!isMasterAnnotation) {
            this.setRecordVisibilityInDossier(mui.getLoginUser().getUserId(), Long.parseLong(packetID),
                                              Integer.parseInt(recordID), Integer.parseInt(sectionMap.get("id")),
                                              StringUtil.parseBoolean(displayRecord));
        }

        // Insert New Annotation Record
        if (StringUtils.isEmpty(annotationId) ||
                (!isMasterAnnotation && annotationDao.getAnnotationByPacket(Integer.parseInt(annotationId), Long.parseLong(packetID)) == null) )
        {
            String[] values = { recordID, packetID, sectionBaseTable, footnote, notation, "1" };
            int annotationID = rh.insert(ANNOTATION_TABLE, annotationFieldList, values);

            Annotation dbAnnotation = new Annotation(annotationID, userId, sectionBaseTable, Integer.parseInt(recordID), Long.parseLong(packetID), footnote, notation, true, null);

            // insert annotation lines only for master annotation
            if (isMasterAnnotation)
            {
                manipulateAnnotationLine(null, labelBefore, rightJustBefore ? RIGHT_JUSTIFY : LEFT_JUSTIFY, PLACE_BEFORE, userId, dbAnnotation, rh);
                manipulateAnnotationLine(null, labelAfter, rightJustAfter ? RIGHT_JUSTIFY : LEFT_JUSTIFY, PLACE_AFTER, userId, dbAnnotation, rh);
            }
        }
        // ... not Insert, either delete or update. (TODO: this could be improved by using the new "persist" feature of AnnotationDao)
        else
        {
            // Check to delete the annotation record:
            // Only delete master annotations.
            // Keep packet specific annotations around for now.
            if (isMasterAnnotation && footnote.length() == 0 && notation.length() == 0
                    && labelBefore.length() == 0 && labelAfter.length() == 0)
            {
                rh.delete(ANNOTATION_TABLE,Integer.parseInt(annotationId));
            }
            else
            {
                Annotation dbAnnotation = annotationDao.getAnnotationByPacket(Integer.parseInt(annotationId), Long.parseLong(packetID));

                // Only update master annotation lines
                if (isMasterAnnotation)
                {
                    manipulateAnnotationLine(labelBeforeRecID, labelBefore, rightJustBefore ? RIGHT_JUSTIFY : LEFT_JUSTIFY, PLACE_BEFORE, userId, dbAnnotation, rh);
                    manipulateAnnotationLine(labelAfterRecID, labelAfter, rightJustAfter ? RIGHT_JUSTIFY : LEFT_JUSTIFY, PLACE_AFTER, userId, dbAnnotation, rh);
                }
                // Update Annotation if footnote or notation have been changed
                if (!footnote.equals(dbAnnotation.getFootnote()) || !notation.equals(dbAnnotation.getNotation()))
                {
                    String[] annotationUpdateFields =
                        { "RecordID", "SectionBaseTable", "Footnote", "Notation", "Display", "UpdateTimestamp", "UpdateUserID"};
                    String[] values =
                        { recordID, sectionBaseTable, footnote, notation, "1", DateUtil.getSqlSafeCurrentDateTime(), String.valueOf(userId) };
                    qt.update(ANNOTATION_TABLE, Integer.parseInt(annotationId), annotationUpdateFields, values);
                }
            }
        }

        /*
         * NOTE: Instead of doing this sendRedirect() like the old system did, re-displaying the record that was just
         * edited, we could do this POST request through an XMLHttpRequest which would leave the current record in place
         * in the form just like the old system, and write some output that the XHR could catch and display.
         */
        String destination = req.getContextPath() + "/" + className +
                "?annotationtype=" + annotationType + "&type=" + recType + "&section=" + sectionName
                + "&recid=" + recordID + "&packetid="+packetID + "&actionType=save"+(!display ? "&display=false" : "");

        res.sendRedirect(destination);
    }


    /**<p>
     * manipulate annotation line<br><em>Manupulate?</em> — Manipulate <em><strong>How??</strong></em>
     * — in <em>what <strong>Way??</strong></em> — for <em>what <strong>Purpose???</strong></em>
     * </p>
     * <p><strong> FIXME: Needs javadoc. </strong></p>
     *
     * @param labelRecID
     * @param label
     * @param textJustification
     * @param linePosition
     * @param annotation
     * @param rh
     */
    private void manipulateAnnotationLine(String labelRecID, String label, String textJustification, String linePosition, int userId, Annotation annotation, RecordHandler rh)
    {
        // if labelRecID is not empty means record already exists
        if (StringUtils.isNotEmpty(labelRecID))
        {
            // if label is empty delete the record
            if (StringUtils.isEmpty(label))
            {
                annotationDao.deleteAnnotationLine(Integer.parseInt(labelRecID));
            }
            // update the record when label has changed
            else if (
                        (PLACE_AFTER.equals(linePosition) && annotation.getLabelBelow() != null &&
                            (!annotation.getLabelBelow().getLabel().equals(label) ||
                             !textJustification.equals(annotation.getLabelBelow().getAnnotationId())))
                     || (PLACE_BEFORE.equals(linePosition) && annotation.getLabelAbove() != null &&
                             (!annotation.getLabelAbove().getLabel().equals(label) ||
                              !textJustification.equals(annotation.getLabelAbove().getAnnotationId())))
                    )
            {
                String[] annotationLineUpdateFields = { "AnnotationID", "Label", "RightJustify", "DisplayBefore", "UpdateTimestamp", "UpdateUserID" };
                String[] values = {
                                    String.valueOf(annotation.getId()),
                                    label,
                                    textJustification,
                                    linePosition,
                                    DateUtil.getSqlSafeCurrentDateTime(),
                                    String.valueOf(userId)
                                  };

                qt.update(ANNOTATION_LINE_TABLE, Integer.parseInt(labelRecID), annotationLineUpdateFields, values);
            }
        }
        else if (StringUtils.isNotEmpty(label)) // save label
        {
            String[] values = {
                                String.valueOf(annotation.getId()),
                                label,
                                textJustification,
                                linePosition
                              };

            qt.insert(userId, ANNOTATION_LINE_TABLE, annotationLineFields, values);
        }
    }

    /**
     * Set record visibility in dossier
     *
     * @param userId
     * @param packetId
     * @param recordId
     * @param sectionId
     * @param display
     * @return boolean
     */
    private boolean setRecordVisibilityInDossier(int userId, long packetId, int recordId, int sectionId, boolean display)
    {
        Packet packet = packetService.getPacket(packetId);
        String packetContentKey = PacketContent.PacketContentKey.build(packetId, recordId, sectionId);
        PacketContent contentItem = packet.getPacketItemsMap().get(packetContentKey);

        if (contentItem == null)
        {
            // No current content item, create a new one and add to the packet
            contentItem = new PacketContent(packetId, userId, sectionId, recordId, "display");
            packet.addContentItem(contentItem);
        }
        // Set the displayParts based on the setting of the display and remove any leading trailing commas
        contentItem.setDisplayParts(contentItem.getDisplayParts().replace("display", display ? "display" : "").replaceAll("^\\,|\\,$", ""));

        // If there are no display parts, we don't need the content item
        if (StringUtils.isEmpty(contentItem.getDisplayParts()))
        {
            packet.removeContentItem(contentItem);
        }

        // Update packet content to include/exclude this content item in the dossier
       return packetService.updatePacket(packet, userId) != null;
    }


    /**
     * Create preview item list block
     *
     * @param mui
     * @param annotationType
     * @param display
     * @param recType
     * @param sectionName
     * @param recordID
     * @param packetID
     * @param out
     * @throws IOException
     */
    private void showLinePreviews(final MIVUserInfo mui, final String annotationType, final boolean display, final String recType,
                                  final String sectionName, final String recordID, final String packetID, final PrintWriter out) throws IOException
    {
        long startTime = System.currentTimeMillis();

        // Get all of the record types for this annotation type
        String[] recTypes = MIVConfig.getConfig().getProperty(annotationType + "-annotation-record-types").split(",");
        String annotationName = MIVConfig.getConfig().getProperty(annotationType + "-annotation-name");

        /* ------------------------ */
        SectionFetcher sf = mui.getSectionFetcher();

        // Get all of the annotations for all record types associated with this annotationtype
        Map<String, Annotation> notesByRecID = annotationDao.getAnnotations(mui.getPerson().getUserId(), Long.parseLong(packetID), recTypes);

        out.println("<div class=\"buttonset section-buttonset right\">");

        StringBuilder recordUrl = new StringBuilder("/miv/Annotations?annotationtype=").append(annotationType);

        if (StringUtils.isNotEmpty(recType))
        {
            recordUrl.append("&type=").append(recType);
        }

        if (StringUtils.isNotEmpty(sectionName))
        {
            recordUrl.append("&section=").append(sectionName);
        }

        if (StringUtils.isNotEmpty(recordID))
        {
            recordUrl.append("&recid=").append(recordID);
        }

        if (StringUtils.isNotEmpty(packetID))
        {
            recordUrl.append("&packetid=").append(packetID);
        }



        // FIXME: This "display" parameter is totally confusing!  —  It means the opposite of what you might think it means.
        //
        //   If display is TRUE then we are going to add the "Show All Records" button,
        //   which implies that display==true means some records are hidden
        //                  and display==false means all records are showing.
        if (display)
        {
            out.println("<a class=\"linktobutton\" title=\"Show all records\" href=\"" + recordUrl + "&display=false\">Show All Records</a>");
        }
        else
        {
            out.println("<a class=\"linktobutton\" title=\"Show Records selected to display in dossier\" href=\"" + recordUrl + "\">Show Selected Records Only!</a>");
        }
        out.println("</div>");

        out.println("<div parseWidgets=\"false\" class=\"previewitemlist\">");
        int totalCount = 0;
        int totalSectionRecordsCount = 0;
        for (String recordType : recTypes)
        {
            List<DisplaySection> sections = null;
            sections = sf.getRecordSections(recordType);

            totalSectionRecordsCount += getTotalSectionRecordsCount(sections);

            // treat display == null or empty as "true"
            if (display)
            {
                sections = applyFilter(new SearchFilterAdapter<DisplaySection>() {
                    @Override
                    public boolean include(DisplaySection item)
                    {
                        List<Map<String, String>> sectionRecordList = new ArrayList<Map<String, String>> (item.getRecords().values());

                        List<Map<String, String>> sectionRecords = applyFilter(new SearchFilterAdapter<Map<String, String>>() {
                            @Override
                            public boolean include(Map<String, String> itemRec)
                            {
                                return StringUtil.parseBoolean(itemRec.get("display"));
                            }
                        }, sectionRecordList );

                        filterSectionRecords(item, sectionRecords);
                        return !sectionRecords.isEmpty();
                    }
                }, new ArrayList<DisplaySection>( sections/*sf.getRecordSections(recordType)*/ ) );  // !!! Was getting all record sections AGAIN!
            }
            // System.out.println("sections: ["+sections+"]");

            if (sections != null)
            {
                int count = sections.size();
                totalCount += count;
                if (count > 0)
                {
                    doRectypeSections(out, annotationType, recordType, sections, notesByRecID, display, packetID);
                }
            }
        }

        // No records were found for any section if the total count is still 0.
        // Hide the normal heading h2 and write a new one out.
        if (totalCount < 1)
        {
            out.println("<div style=\"display:block;\" id=\"errormessage\">");
            out.println("<div id=\"errorbox\">");
            if (totalSectionRecordsCount < 1)
            {
                out.println("<style type=\"text/css\">\n");
                out.println("div#selectheader, #main .section-buttonset { display: none; }\n");
                out.println("</style>");

                out.println("Annotations can be added to " + annotationName.toLowerCase()
                        + " records. You have not entered any " + annotationName.toLowerCase() + " records.");
            }
            else
            {
                out.println("<style type=\"text/css\">\n");
                out.println("div#selectheader { display: none; }\n");
                out.println("</style>");

                out.println("Annotations can be added to " + annotationName.toLowerCase()
                        + " records.<br> You have not selected any " + annotationName.toLowerCase() + " records to display at "
                        + " <a style=\"color: inherit;\" title=\"Design My Packet\" href=\"/miv/packet/design\"><em>Design My Packet</em></a> page  or click on <em>Show All Records</em> button.");
            }
            out.println("</div><!-- closing #errorbox div -->");
            out.println("</div><!-- closing #errormessage div -->");
        }

        //out.println("<br>");
        out.println("</div><!-- closing parseWidgets div -->");
        /* ------------------------ */

        if (MIVConfig.getConfig().getDoTimings())
        {
            long endTime = System.currentTimeMillis();
            long duration = endTime - startTime;
            long seconds = duration / 1000;
            long msecs = duration % 1000;
            logger.info(" Doing *NEW* style previews took {}.{} seconds", seconds, msecs);
        }
    }
    // showLinePreviews()


    /**
     * Get the total section record
     *
     * @param displaySection
     * @return
     */
    private int getTotalSectionRecordsCount(List<DisplaySection> displaySection)
    {
        int totalSectionsCount = 0;

        for(DisplaySection section: displaySection)
        {
            totalSectionsCount += section.getRecords().size();
        }

        return totalSectionsCount;
    }


    /**
     * Remove invisible section records
     *
     * @param displaySection - Section Object
     * @param sectionRecords - List of visible section records
     */
    private void filterSectionRecords(DisplaySection displaySection, List<Map<String, String>> sectionRecords)
    {
        List<String> displaySectionKeys = new ArrayList<String> (displaySection.getRecords().keySet());

        for (String id : displaySectionKeys)
        {
            if (!sectionRecords.contains(displaySection.getRecords().get(id)))
            {
                displaySection.getRecords().remove(id);
            }
        }
    }


    /**
     * Filter results with the given filter.
     *
     * @param filter
     * @param results
     * @return filtered results
     */
    protected <E> List<E> applyFilter(SearchFilter<E> filter, List<E> collection)
    {
        List<E> ds = collection;
        collection = new LinkedList<E>();
        Collection<E> c = filter.apply(ds);
        collection.addAll(c);
        return collection;
    }


    /**
     * Output a set of sections for a type of record.
     *
     * @param out Writer to send output to
     * @param annotationType type of the annotation
     * @param recordType type of the records being output
     * @param sections the sections to be displayed for this record type
     * @param notesByRecID notes keyed by the ID of the record the note is attached to
     * @param display flag to show or hide visible records
     * @param packetId for these records
     * @throws IOException
     */
    private void doRectypeSections(PrintWriter out, String annotationType, String recordType,
                                   List<DisplaySection> sections,
                                   Map<String, Annotation> notesByRecID,
                                   boolean display,
                                   String packetId)
            throws IOException
    {
        Properties pstr = PropertyManager.getPropertySet(recordType, "strings");
        out.println("\n<div class=\"recordblock\"><!-- BEGIN " + recordType + " RECORDS -->");

        for (DisplaySection section : sections)
        {
            // System.out.println("  section: ["+section+"]");
            String sectionHeading = section.getHeading();
            if (sectionHeading == null || sectionHeading.trim().length() < 1) {
                sectionHeading = pstr.getProperty(section.getKey()+"-sectionhead");
            }

            // Description needed for Events and Works
            if (recordType.equalsIgnoreCase("events") || recordType.equalsIgnoreCase("works"))
            {
                String description = pstr.getProperty("description");
                sectionHeading = DisplayOptions.setRecordDescription(description) + sectionHeading;
            }

            // out.println("  <div id=\"recordsection\" class=\"recordsection\">"); // id="" added by bmgarg way back
            // when, but that's illegal
            out.println("  <div class=\"recordsection\">");
            out.println("  <h2>" + sectionHeading + "</h2><!-- begin section -->");

            Map<String, Map<String, String>> records = section.getRecords();
            // System.out.println("  record map: ["+records+"]");
            if (records != null)
            {
                int count = 0;
                for (Map<String, String> rec : records.values())
                {
                    // System.out.println("  record: ["+rec.toString()+"]");
                    displayRecord(out, annotationType, recordType, section.getKey(), rec, ++count, notesByRecID, display, packetId);
                }

                out.println("  </div><!-- recordsection : end of this section -->\n");
            }
        } // for section
        out.println("</div><!-- recordblock : end of this record type -->\n");
    }
    // doRectypeSections()



    /*
     * Templates used to format and populate annotation DOM elements in displayRecord() below.
     */
    static final String editAnnotationDivTemplate =
            "<div class=\"editannotations\">\n    {0}\n    {1}\n  </div><!-- .editannotations -->\n";

    static final String editAnnotationCBTemplate =
            "<input type=\"checkbox\" class=\"recorddisplay\" name=\"recorddisplay\" id=\"{0}\" value=\"{2}\"" + // was: value={0}
                  " title=\"Check to include this item in dossier\"{1}> &nbsp;";

     static final String editAnnotationLinkTemplate =
            "<a class=\"linktobutton\"" +
              " href=\"Annotations?annotationtype={0}&type={1}&section={2}" +
              "&recid={3}&packetid={4}{5}\"" +
              " title=\"Edit {6} Annotation #{3}\"" +
              " id=\"AE{4}\"" +
              ">Edit</a>";

    /**
     * Output a numbered record with its notations and any lines above/below.
     *
     * @param out Writer to send output to
     * @param recType type of the record being output
     * @param sectionName name of the section within the decord
     * @param record the record to be written
     * @param count the current number used to prefix the record
     * @param notesByRecID map of notations, accessed by record ID number
     * @param display flag for this record
     * @param packetID of this record
     * @throws IOException
     */
    private void displayRecord(Writer out, String annotationType, String recType, String sectionName,
                               Map<String, String> record, int count,
                               Map<String, Annotation> notesByRecID,
                               boolean display,
                               String packetID) throws IOException
    {
        String notations = "";
        String footnotes = "";
        String lineAbove = null;
        String lineBelow = null;
        Annotation notes = null;

        final boolean isMaster = packetID.equals("0");

        Map<String, String> sectionMap = sectionidByNameMap.get(sectionName);

        // Check the record ID # of this record...
        String pubRecordID = record.get("id").toString();
        String sectionRecType = sectionMap.get("recordname").replace("-record", "");
        String sectionBaseTable = sectionMap.get("sectionbasetable");
        String lookupKey = pubRecordID + "-" + sectionBaseTable;

        // If it's in the annotation map then deal with its lines, footnotes, annotations.
        Map<String, String> annotationSet = new HashMap<>();
        if (notesByRecID != null && sectionRecType.equals(recType) && notesByRecID.get(lookupKey) != null)
        {
            notes = notesByRecID.get(lookupKey);
            if (notes != null)
            {
                if (notationRequiredList.contains(annotationType))
                {
                    if ( StringUtils.isNotEmpty(notes.getNotation()) )
                    {
                        List<Notation> allNotes = new ArrayList<>();
//                      System.out.println(" -> Finding notation enums for " + notes.getNotation());
                        for (char c : notes.getNotation().toCharArray()) {
                            Notation notation = Notation.valueOf(c);
                            if ( (isMaster && notation.isMaster())  ||  (!isMaster && !notation.isMaster()) ) {
                                allNotes.add(notation);
                            }
//                          System.out.println("\t -> " + c + " = " + notation);
                        }
                        String noteString = withComma .join( allNotes );

                        StringBuilder sb = new StringBuilder("<span class=\"notations\"");
                        if ( noteString != null && noteString.trim().length() > 0 ) {
                            sb.append(" data-notes=\"") .append(noteString) .append('"');
                            annotationSet.put(lookupKey, noteString);
                        }
                        sb.append('>') .append(notes.getNotation()) .append("</span>");

                        //notations = "<span class=\"notations\" data-notes=\"" + withComma.join(allNotes) + "\">" + notes.getNotation() + "</span>";
                        notations = sb.toString();
                    }
                }
                else
                {
                    //logger.info("\t**=-----==> Putting a blank <span class='notations'></span> for annotationType {}", annotationType);
                    notations = "<span class=\"notations\"></span>";
                }

                footnotes = (StringUtils.isNotEmpty(notes.getFootnote()) ?
                            "<span class=\"footnotes\">#</span>" : "");

                if (notes.hasLabelAbove()) lineAbove = getAnnotationLine(notes.getLabelAbove());
                if (notes.hasLabelBelow()) lineBelow = getAnnotationLine(notes.getLabelBelow());
            }
        }


        String cbId = sectionName + "-" + pubRecordID;

        String editAnnotationCB = MessageFormat.format(editAnnotationCBTemplate,
                cbId,
                StringUtil.parseBoolean(record.get("display")) ? " checked" : ""
                    , sectionName + "~" + pubRecordID    // was not here, used 'cbId' for this, but then noticed the tilde (not dash) on this one
                );

        String editAnnotationLink = MessageFormat.format(editAnnotationLinkTemplate,
                                                         annotationType, recType, sectionName,
                                                         pubRecordID, packetID, !display ? "&display=false" : "",
                                                         isMaster ? "Master" : "Packet"
                                                         );

        String editAnnotationDiv = MessageFormat.format(editAnnotationDivTemplate,
                                                        isMaster ? "" : editAnnotationCB, editAnnotationLink
                                                        );


        //out.write("<!--\n");
        //out.write("editAnnotationCB = \n" + editAnnotationCB + "\n");
        //out.write("\neditAnnotationLink = \n" + editAnnotationLink + "\n");
        //out.write("-->\n");

        final StringBuilder preview = new StringBuilder();

        preview.append("<div class=\"annotationentry\">");
        preview.append("<div class=\"notes\">")
               .append(notations).append(footnotes).append(count > 0 ? count + ". " : "")
               .append("</div>");

        // remove year div from publicationevents because previews contains year itself.
        if (!recType.equals("publicationevents"))
        {
            if (record.get("year") != null)
            {
                preview.append("<div class=\"year\">").append(record.get("year")).append("</div>");
            }
        }

        preview.append("<div class=\"recordentry\">")
               .append(record.get("preview"))
               .append("</div>");

        preview.append("</div><!-- .annotationentry -->");



        final StringBuilder publineDiv = tagStart("div");
        publineDiv .append( attribute("id") .value("L" + pubRecordID + recType) )
                   .append( attribute("data-section") .value(sectionName) )
                   .append( attribute("data-rectype") .value(recType) )
                   .append( attribute("data-recid")   .value(pubRecordID) )
            ;

        final String noteData = annotationSet.get(lookupKey);
        if ( StringUtils.isNotBlank(noteData) ) {
            publineDiv .append( attribute("data-notations").value(noteData.trim()) );
        }

        final String evenodd = count % 2 == 0 ? "even" : "odd";
        publineDiv .append( attribute("class")
                            .value( "publine " + evenodd +
                                   (StringUtil.parseBoolean(record.get("display")) ? "" : " inactive")
                                   )
                    )
                   .append( tagFinish() );

        out.write(publineDiv.toString());


        // if line before, print it here
        if (lineAbove != null)
        {
            out.write("  <!-- line Above -->" + lineAbove + "\n");
        }

        out.write("<div class=\"record\">\n");
        out.write("  " + (isDesignLines ? editAnnotationDiv : "") + preview.toString() + "\n");
        out.write("</div><!-- .record -->\n");

        // if line after, print it here
        if (lineBelow != null)
        {
            out.write("  <!-- line Below -->" + lineBelow + "\n");
        }

        out.write("  </div><!-- L" + pubRecordID + recType + " -->\n" + "\n"); // closes the publineDiv
    }
    // displayRecord()


    /**
     * Get the line above / below
     * @param annotationLine
     * @return
     */
    private String getAnnotationLine(AnnotationLine annotationLine)
    {
        if (annotationLine == null) return null;

        String line = "";
        String label = annotationLine.getLabel();

        if (StringUtils.isNotEmpty(label))
        {
            label = MIVUtil.escapeMarkup(label);
            label = MIVUtil.replaceNewline(label);

            String justify = annotationLine.isRightJustify() ? "labelright" : "labelleft";
            label = "<div class=\"" + justify + "\" >" + label + "</div>";
            if (annotationLine.isDisplayBefore())
            {
                line = "&nbsp;" + label;
            }
            else
            {
                line = label + "&nbsp;";
            }
        }
        return line;
    }


    /**
     * Display the incoming request parameters and values. Used for debugging.
     *
     * @param req
     */
    private void showReqParameters(HttpServletRequest req)
    {
        System.out.println("Annotations URL parameters...");
        // Let's see all the parameters...
        Enumeration<String> names = req.getParameterNames();
        while (names.hasMoreElements())
        {
            String name = names.nextElement();
            String[] values = req.getParameterValues(name);

            System.out.print("  param [" + name + "] == {");
            boolean first = true;
            for (String val : values)
            {
                System.out.print(val + (first ? "" : ", "));
                first = false;
            }
            System.out.println("}");
        }
        System.out.println();
    }


/* Conveniences for building tags.
 * If you have a StringBuilder 'sb' with the start of a tag ('<span', '<div', etc.)
 * you can easily add attributes without having to deal with escaping the quotes
 * around the attribute's value...
 *     sb.append( attribute("class").value("selected") );
 * General use is...
 *     StringBuilder sb = tagStart("div");
 *     sb.append( attribute("id") .value("unique") )
 *       .append( attribute("class") .value("pinned") );
 * tagStart returns a StringBuilder so some things can be done without a local var...
 *     out.write( tagStart("div")
 *                    .append( attribute("id") .value("one")
 *                    .append( attribute("class") .value("first")
 *                    .toString()
 *              );
 * If this appears to be useful enough it can be moved to its own top-level class.
 */
    private StringBuilder tagStart(String tag) { return new StringBuilder("<" + tag); }
    private String tagFinish() { return ">"; }
    @SuppressWarnings("unused")
    private String tagFinish(String comment) {
        return "><!-- " + comment + " -->";
    }
    private DomAttribute attribute(String name) { return new DomAttribute(name); }

    private class DomAttribute
    {
        private final String attributeName;
        private String attributeValue;
        private String attributeString;

        DomAttribute(String name)
        {
            this.attributeName = name;
            this.attributeString = name;
        }
        DomAttribute value(String val)
        {
            this.attributeValue = val;
            this.attributeString =
                    StringUtils.isBlank(val)
                        ? this.attributeName
                        : attributeName + "=" + '"' + attributeValue + '"';
            return this;
        }
        @SuppressWarnings("unused")
        DomAttribute value()
        {
            return value(null);
        }
        @Override
        public String toString()
        {
            return " " + this.attributeString;
        }
    }

}
