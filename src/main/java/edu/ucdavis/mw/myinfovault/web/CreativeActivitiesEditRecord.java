/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CreativeActivitiesEditRecord.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.dao.creativeactivities.CreativeActivitiesDao;
import edu.ucdavis.mw.myinfovault.dao.creativeactivities.CreativeActivitiesDaoImpl;
import edu.ucdavis.mw.myinfovault.valuebeans.RecordVO;
import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.DateParser;
import edu.ucdavis.myinfovault.data.Validator;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Servlet to handle creative activities save and edit operations for works, events and Reviews
 *
 * @author pradeeph
 * @since MIV 4.4
 */
public class CreativeActivitiesEditRecord extends EditRecord
{
    private static final long serialVersionUID = 201202131322L;

    private static final int PRINT_REVIEW = 1;
    private static final int MEDIA_REVIEW = 2;

    private static final String COMMA_SPLIT = "\\s*,\\s*";

    /**
     * to check into the session for old associate data and clear that data
     */
    @Override
    public void setUpForm(HttpServletRequest req, HttpServletResponse res)
    {
        Map<String, String[]> reqData = getRequestMap(req);
        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();
        int userId = mui.getPerson().getUserId();

        // setup form specific data
        String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);
        logger.debug("recordType :: {}", recordType);
        String associationKey = userId+"-associate-"+recordType;
        // check for the association session data. delete if exists.
        if(mivSession.get().getHttpSession().getAttribute(associationKey)!=null)
        {
            mivSession.get().getHttpSession().removeAttribute(associationKey);
        }
    }

    /**
     * Get the array of records to be executed
     * @param req HttpServletRequest Object
     * @return Array of RecordVO Object
     */
    @Override
    RecordVO[] getRecords(Map<String, String[]> reqData)
    {
        logger.trace(" ******* Entering CreativeActivitiesEditRecord:getRecords ******** ");
        logger.debug("reqData :: {}", reqData);

        String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);
        logger.debug("recordType :: {}", recordType);

        RecordVO[] recordVO = null;
        if (recordType != null)
        {
            recordVO = getRecords(recordType, reqData);
        }

        logger.trace(" ******* Leaving CreativeActivitiesEditRecord:getRecords ******** ");

        return recordVO;
    }


    /**
     * Prepare array of RecordVO to save multiple records
     * @param recordType
     * @param reqData
     * @return
     */
    private RecordVO[] getRecords(String recordType, Map<String, String[]> reqData)
    {
        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();
        int userId = mui.getPerson().getUserId();

        // Preparing RecordVO for Associations
        String selectedIds = (reqData.get("selectedids") != null ? reqData.get("selectedids")[0] : null);
        logger.debug("selectedIds from reqData :: {}", selectedIds);

        // When java script turn off lookup into session for an associated id list.
        if ( ( selectedIds == null || selectedIds.trim() == "" ) && !isAjaxRequest() &&
               mivSession.get().getHttpSession().getAttribute(userId + "-associate-" + recordType) != null)
        {
            logger.debug("getting ids from session");
            selectedIds = (String) mivSession.get().getHttpSession().getAttribute(userId+"-associate-"+recordType);
            mivSession.get().getHttpSession().removeAttribute(userId+"-associate-"+recordType);
        }

        logger.debug("In getRecords(), recordType is :: {}\n  selectedIds :: {}", recordType, selectedIds);

        List<RecordVO> recordList;
        try {
            RecordType recType = RecordType.valueOf(recordType.toUpperCase());
            recordList = recType.getRecords(userId, selectedIds, reqData);
        }
        catch (IllegalArgumentException e) {
            // The record type passed was not valid
            throw new MivSevereApplicationError("A request was made to display an unknown record type: " +  recordType, e);
        }

        return recordList.toArray(new RecordVO[0]);
    }


    /**
     * Validate association used by Validator
     * @param selectedIds
     * @param reqData
     * @return
     */
    public ResultVO validateAssociation(String selectedIds, Map <String, String[]> reqData)
    {
        ResultVO resultVO = null;
        try
        {
            String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);
            int userId = Integer.parseInt(reqData.get("userId") != null ? reqData.get("userId")[0] : "0");

            // Step 1: Check when no association found no need to validate association
            if (! StringUtils.hasText(selectedIds))
            {
                return new ResultVO(true);
            }

            List<String> dataList = null;

            // Step 2: Get the type of association then get the valid records for selected Year
            RecordTypeAssociation rta = null;
            try {
                rta = RecordTypeAssociation.valueOf(recordType.toUpperCase());
            }
            catch (IllegalArgumentException e) {
                // The association pair passed was not valid
                throw new MivSevereApplicationError("A request was made to create an invalid association : " +  recordType, e);
            }

            dataList = rta.getAssociationRecords(userId, reqData);


            // Step 3: Validate records
            resultVO = validateAssociationWithYear(recordType, dataList, selectedIds);

            logger.debug("Errorfields :: {}\n  Message :: {}", resultVO.getErrorfields(), resultVO.getMessages());
        }
        // FIXME: Don't catch plain 'Exception' - catch something specific
        // What Exception(s) might happen here?  What statements above might cause them?
        // A giant try-and-catch-all block like this shouldn't be used. Catch a more specific exception
        // in a smaller try/catch block.
        // The MIV coding standard (Developer Cookbook) clearly states "Don't catch Exception or Throwable
        //   unless you intend to actually handle every possible error in an intelligent way."
        // https://confluence.ucdavis.edu/confluence/display/MIV/Developer+Cookbook#DeveloperCookbook-ExceptionHandling
        catch (Exception e)
        {
            //e.printStackTrace(); // FIXME: printStackTrace is not an acceptable response to catching an exception
            logger.error("A totally unexpected error occured when validating associations. I'm flabbergasted.", e);
            return new ResultVO(false, "Association error", "association");
        }

        return resultVO;
    }


    /**
     * Validate association from valid association list
     * @param recordType
     * @param dataList
     * @param selectedIds
     * @return
     */
    private ResultVO validateAssociationWithYear(String recordType, List<String> dataList, String selectedIds)
    {
        for (String id : selectedIds.split(COMMA_SPLIT))
        {
            logger.debug("id :: {}", id);

            if (dataList != null && !dataList.contains(id.trim()))
            {
                String associationRule = PropertyManager.getPropertySet(recordType, "labels").getProperty("associationrule");

                return new ResultVO(false, associationRule);
            }
        }

        return new ResultVO(true);
    }


    /**
     * Validate event date based on regular or published event
     * @param eventDate
     * @param reqData
     * @return
     */
    public static ResultVO validateEventDate(String eventDate, Map<String, String[]> reqData)
    {
        Validator validator = new Validator(reqData);
        eventDate = validator.validate("eventstartdate", eventDate, "date::Y+2");

        if (validator.isValid())
        {
            // validate end date
            validator = new Validator(reqData);
            String eventstartdate = (reqData.get("eventstartdate") != null ? reqData.get("eventstartdate")[0] : null);

            if (StringUtils.hasText(eventstartdate))
            {
                eventstartdate = validator.validate("eventstartdate", eventstartdate, "date");
                if (validator.isValid())
                {
                    try
                    {
                        Date startDate = sdf.get().parse(eventstartdate);
                        Date endDate = sdf.get().parse(eventDate);

                        // start date must be smaller or equals to end date
                        if (startDate.after(endDate))
                        {
                            return new ResultVO(false, "End date must be after the Start date.");
                        }
                        else if (startDate.equals(endDate)) // no need to save same end date as start date
                        {
                            // set blank because removing field not work for edit
                            reqData.put("eventenddate", new String[] { "" });
                            //reqData.remove("eventenddate");
                        }
                    }
                    // This ParseException can't happen because the eventStartDate and eventDate
                    // have already been successfully parsed by the Validator
                    catch (ParseException e) { }
                }
            }
        }

        return new ResultVO(true);
    }


    /**
     * Validate review date based on print or media review.
     * This is used via reflection as a configured constraint validation.
     * @param reviewDate
     * @param reqData
     * @return
     */
    public static ResultVO validateReviewDate(String reviewDate, Map<String, String[]> reqData)
    {
        Validator validator = new Validator(reqData);

        String categoryid = (reqData.get("categoryid") != null ? reqData.get("categoryid")[0] : null);

        // if no category selected no need to validate venue
        if (! StringUtils.hasText(categoryid))
        {
            return new ResultVO(true);
        }

        String fieldName = "";
        String contains = null;

        int category = Integer.parseInt(categoryid);
        switch (category)
        {
            case PRINT_REVIEW:
                reviewDate = (reqData.get("printreviewyear") != null ? reqData.get("printreviewyear")[0] : null);
                fieldName = "printreviewyear";
                contains = "year:Y+2";
                break;
            case MEDIA_REVIEW:
                fieldName = "reviewdate";
                contains = "date::Y+2";
                break;
        }

        if (! StringUtils.hasText(reviewDate))
        {
            return new ResultVO(false, "This field is required.", fieldName);
        }

        String value = validator.validate(fieldName, reviewDate, contains);

        if (validator.isValid())
        {
            return new ResultVO(true).setValue(value);
        }
        else
        {
            return new ResultVO(false, validator.getErrorMessageList(), validator.getErrorFieldList());
        }
    }

    /**
     * Validate description based on print or media review
     * @param description
     * @param reqData
     * @return
     */
    public ResultVO validateReviewDescription(String description, Map<String, String[]> reqData)
    {
        Validator validator = new Validator(reqData);
        String value = null;

        String categoryid = reqData.get("categoryid") != null ? reqData.get("categoryid")[0] : null;

        // If no category selected no need to validate status
        if (!StringUtils.hasText(categoryid)) { return new ResultVO(true); }

        String fieldName = "";

        int category = Integer.parseInt(categoryid);
        switch (category)
        {
            case PRINT_REVIEW:
                fieldName = "printdescription";
                break;
            case MEDIA_REVIEW:
                fieldName = "description";
                break;
        }

        value = validator.validate(fieldName, description, "length:255");

        if (validator.isValid())
        {
            return new ResultVO(true).setValue(value);
        }
        else
        {
            return new ResultVO(false, validator.getErrorMessageList(), validator.getErrorFieldList());
        }
    }


    /**
     * Validate reviewer based on print or media review
     * @param reviewer
     * @param reqData
     * @return
     */
    public ResultVO validateReviewer(String reviewer, Map<String, String[]> reqData)
    {
        String categoryid = reqData.get("categoryid") != null ? reqData.get("categoryid")[0] : null;

        // FIXME: If no category selected then someone has been tampering with the form submission.
        //        The category ID is an absolutely required field and MUST be either 1 or 2.
        //        So, contrary to "no need to validate status", we should throw MivSevereApplicationError

        // If no category selected no need to validate status
        if (! StringUtils.hasText(categoryid)) { return new ResultVO(true); }

        String fieldName = "";

        int category = Integer.parseInt(categoryid);
        switch (category)
        {
            case PRINT_REVIEW:
                fieldName = "printreviewer";
                break;
            case MEDIA_REVIEW:
                fieldName = "reviewer";
                break;
        }

        if (StringUtils.hasText(reviewer)) {
            return new ResultVO(true);
        }
        else {
            return new ResultVO(false, "This field is required.", fieldName);
        }
    }

    /**
     * Validate review venue for media review
     * @param venue
     * @param reqData
     * @return
     */
    public ResultVO validateReviewVenue(String venue, Map<String, String[]> reqData)
    {
        String categoryid = (reqData.get("categoryid") != null ? reqData.get("categoryid")[0] : null);

        // FIXME: If no category selected then someone has been tampering with the form submission.
        //        The category ID is an absolutely required field and MUST be either 1 or 2.
        //        So, contrary to "no need to validate status", we should throw MivSevereApplicationError

        // If no category selected no need to validate status
        if (! StringUtils.hasText(categoryid)) { return new ResultVO(true); }

        int category = Integer.parseInt(categoryid);
        int maxlength = 255;

        if (category == MEDIA_REVIEW)
        {
            if ( !StringUtils.hasText(venue)) {
                return new ResultVO(false, "This field is required.","venue");
            }
            else if (venue.length() > maxlength) {
                return new ResultVO(false, "Too many characters have been added to this field (" + maxlength + " characters allowed).", "venue");
            }
        }

        return new ResultVO(true);
    }


    /**
     * Represents the record types within Creative Activities, and provides
     * a way to get the records for that type.
     * @author Stephen Paulsen
     */
    private enum RecordType
    {
        WORKS
        {
            @Override
            List<RecordVO> getRecords(int userId, String selectedIds, Map<String, String[]> reqData)
            {
                List<RecordVO> results = new ArrayList<RecordVO>();

                // main record-type
                results.add(new RecordVO(true, reqData));

                //if (selectedIds != null && selectedIds.trim().length() > 0 && ( selectedIds.indexOf("events") != -1 || selectedIds.indexOf("publicationevents") != -1) )
                if (selectedIds != null)
                {
                    StringBuilder events = new StringBuilder();
                    StringBuilder publicationevents = new StringBuilder();

                    for (String s : selectedIds.split(COMMA_SPLIT))
                    {
                        if (s.startsWith("publicationevents"))
                        {
                            if (publicationevents.length() > 0) {
                                publicationevents.append(", ");
                            }
                            publicationevents.append(s);
                        }
                        else if (s.startsWith("events"))
                        {
                            if (events.length() > 0) {
                                events.append(", ");
                            }
                            events.append(s);
                        }
                    }

                    Map<String, String[]> associationRequestData;
                    associationRequestData = new HashMap<String, String[]>();
                    associationRequestData.put("workyear", reqData.get("workyear"));
                    associationRequestData.put("rectype", new String[] { "worksevents" });
                    associationRequestData.put("association", new String[] { selectedIds });
                    associationRequestData.put("events", new String[] { events.toString().replaceAll("events", "") });
                    associationRequestData.put("publicationevents", new String[] { publicationevents.toString().replaceAll("publicationevents", "") });
                    associationRequestData.put("userId", new String[] { String.valueOf(userId) });
                    results.add(new RecordVO(false, associationRequestData));
                }

                return results;
            }
        },


        EVENTS
        {
            @Override
            List<RecordVO> getRecords(int userId, String selectedIds, Map<String, String[]> reqData)
            {
                List<RecordVO> results = new ArrayList<RecordVO>();

                String eventenddate = (reqData.get("eventenddate") != null ? reqData.get("eventenddate")[0] : null);
                // FIXME: What does this next line do?  It creates a new enclosing object and calls the validate,
                // but doesn't do anything with either the object or the ResultVO validation results.
                // If there is a side-effect purpose for this, please document it.
//                (new CreativeActivitiesEditRecord()).validateEventDate(eventenddate, reqData);
                CreativeActivitiesEditRecord.validateEventDate(eventenddate, reqData);

                // main record-type
                results.add(new RecordVO(true, reqData));

                //if (selectedIds != null && selectedIds.trim().length() > 0 && selectedIds.indexOf("works") != -1)
                if (selectedIds != null)
                {
                    Map<String, String[]> associationRequestData;
                    associationRequestData = new HashMap<String, String[]>();
                    associationRequestData.put("eventstartdate", reqData.get("eventstartdate"));
                    associationRequestData.put("rectype", new String[] { "eventsworks" });
                    associationRequestData.put("association", new String[] { selectedIds });
                    associationRequestData.put("works", new String[] { selectedIds.replaceAll("works", "") });
                    associationRequestData.put("userId", new String[] { String.valueOf(userId) });
                    results.add(new RecordVO(false, associationRequestData));
                }

                return results;
            }
        },


        PUBLICATIONEVENTS
        {
            @Override
            List<RecordVO> getRecords(int userId, String selectedIds, Map<String, String[]> reqData)
            {
                List<RecordVO> results = new ArrayList<RecordVO>();

                // main record-type
                results.add(new RecordVO(true, reqData));

                //if (selectedIds != null && selectedIds.trim().length() > 0 && selectedIds.indexOf("works") != -1)
                if (selectedIds != null)
                {
                    Map<String, String[]> associationRequestData;
                    associationRequestData = new HashMap<String, String[]>();
                    associationRequestData.put("year", reqData.get("year"));
                    associationRequestData.put("rectype", new String[] { "publicationeventsworks" });
                    associationRequestData.put("association", new String[] { selectedIds });
                    associationRequestData.put("works", new String[] { selectedIds.replaceAll("works", "") });
                    associationRequestData.put("userId", new String[] { String.valueOf(userId) });
                    results.add(new RecordVO(false, associationRequestData));
                }

                return results;
            }
        },


        REVIEWS
        {
            @Override
            List<RecordVO> getRecords(int userId, String selectedIds, Map<String, String[]> reqData)
            {
                List<RecordVO> results = new ArrayList<RecordVO>();

                Map<String, String[]> associationRequestData;
                // main record-type
                String categoryid = (reqData.get("categoryid") != null ? reqData.get("categoryid")[0] : null);
                String reviewdate = "";
                if (categoryid != null && !categoryid.equals("0") && !categoryid.equals(""))
                {
                    // remove fields those are not belongs to print review
                    if (categoryid.equals(String.valueOf(PRINT_REVIEW))) // print review
                    {
                        reviewdate = (reqData.get("printreviewyear") != null ? reqData.get("printreviewyear")[0] : null);

                        String printreviewer = (reqData.get("printreviewer") != null ? reqData.get("printreviewer")[0] : null);
                        reqData.put("reviewer", new String[] { printreviewer });

                        String printdescription = (reqData.get("printdescription") != null ? reqData.get("printdescription")[0] : null);
                        reqData.put("description", new String[] { printdescription });

                        String printurl = (reqData.get("printurl") != null ? reqData.get("printurl")[0] : null);
                        reqData.put("url", new String[] { printurl });

                        // remove media review fields
                        reqData.remove("venue");
                        reqData.remove("reviewdate");
                    }
                    else if (categoryid.equals(String.valueOf(MEDIA_REVIEW))) // media review
                    {
                        reviewdate = (reqData.get("reviewdate") != null ? reqData.get("reviewdate")[0] : null);
                        // remove print review fields
                        reqData.remove("title");
                        reqData.remove("publication");
                        reqData.remove("publisher");
                        reqData.remove("volume");
                        reqData.remove("issue");
                        reqData.remove("pages");
                        reqData.remove("printreviewyear");
                    }
                }

                //System.out.println("\n\nREVIEWS reqData :: "+reqData);

                results.add(new RecordVO(true, reqData));

                if (selectedIds != null)
                {
                    associationRequestData = new HashMap<String, String[]>();
                    associationRequestData.put("reviewdate", new String[] { reviewdate });
                    associationRequestData.put("rectype", new String[] { "reviewsassociation" });
                    associationRequestData.put("association", new String[] { selectedIds });

                    StringBuilder works = new StringBuilder();
                    StringBuilder events = new StringBuilder();
                    for (String s : selectedIds.split(COMMA_SPLIT))
                    {
                        if (s.contains("works"))
                        {
                            if (works.length() > 0) {
                                works.append(", ");
                            }
                            works.append(s);
                        }
                        else if (s.contains("events"))
                        {
                            if (events.length() > 0) {
                                events.append(", ");
                            }
                            events.append(s);
                        }
                    }

                    associationRequestData.put("events", new String[] { events.toString().replaceAll("events", "") });
                    associationRequestData.put("works",  new String[] { works.toString().replaceAll("works", "") });
                    associationRequestData.put("userId", new String[] { String.valueOf(userId) });
                    associationRequestData.put("categoryid", reqData.get("categoryid"));
                    results.add(new RecordVO(false, associationRequestData));
                }
                return results;
            }
        } ;

        /** Each RecordType needs to provide a getRecords() method. */
        abstract List<RecordVO> getRecords(int userId, String selectedIds, Map<String, String[]> reqData);
    }


    /**
     * Represents the association records between record types.
     * @author Stephen Paulsen
     */
    private enum RecordTypeAssociation
    {
        WORKSEVENTS
        {
            @Override
            List<String> getAssociationRecords(int userId, Map<String, String[]> reqData)
            {
                List<String> dataList;

                int workyear = Integer.valueOf( "0" + (reqData.get("workyear") != null ? reqData.get("workyear")[0] : ""));

                // Get the valid records by year
                CreativeActivitiesDao ca = new CreativeActivitiesDaoImpl();
                dataList = ca.getValidEventAndPublicationEventsIdsByUserIdAndWorkYear(userId, workyear);

                return dataList;
            }
        },

        EVENTSWORKS
        {
            @Override
            List<String> getAssociationRecords(int userId, Map<String, String[]> reqData)
            {
                List<String> dataList = null;

                // Get the valid records by year
                String eventdate = (reqData.get("eventstartdate") != null ? reqData.get("eventstartdate")[0] : null);

                int eventyear = 0;

                Date d = DateParser.parse(eventdate);
                // If the date failed to parse we'll skip getting the year & leave it at 0
                if (d != null)
                {
                    try {
                        eventyear = Integer.parseInt(yf.get().format(d));
                    }
                    // Allow eventyear to remain at 0 if the impossible happens
                    catch (NumberFormatException e) { }
                }

                // Get the valid records by year
                CreativeActivitiesDao ca = new CreativeActivitiesDaoImpl();
                dataList = ca.getValidWorkIdsByUserIdAndEventYear(userId, eventyear);

                return dataList;
            }
        },

        PUBLICATIONEVENTSWORKS
        {
            @Override
            List<String> getAssociationRecords(int userId, Map<String, String[]> reqData)
            {
                List<String> dataList;

                // Get the valid records by year
                String eventdate = reqData.get("year") != null ? reqData.get("year")[0] : null;
                int eventyear = 0;

                if (StringUtils.hasText(eventdate))
                {
                    eventyear = Integer.valueOf("0" + eventdate.trim());
                }

                // Get the valid records by year
                CreativeActivitiesDao ca = new CreativeActivitiesDaoImpl();
                dataList = ca.getValidWorkIdsByUserIdAndEventYear(userId, eventyear);

                return dataList;
            }
        },

        REVIEWSASSOCIATION
        {
            @Override
            List<String> getAssociationRecords(int userId, Map<String, String[]> reqData)
            {
                List<String> dataList;

                String categoryid = reqData.get("categoryid") != null ? reqData.get("categoryid")[0] : null;

                // Get the valid records by year
                String reviewdate = reqData.get("reviewdate") != null ? reqData.get("reviewdate")[0] : null;
                int reviewyear = 0;
                CreativeActivitiesDao ca = new CreativeActivitiesDaoImpl();

                if (StringUtils.hasText(categoryid))
                {
                    int category = Integer.parseInt(categoryid);
                    switch (category)
                    {
                        case PRINT_REVIEW:
                            reviewyear = Integer.valueOf("0" + reviewdate.trim());
                            break;
                        case MEDIA_REVIEW:
                            try
                            {
                                //Date d = sdf.get().parse(reviewdate);
                                Date d = DateParser.parse(reviewdate, sdf.get());
                                if (d != null) {
                                    reviewyear = Integer.parseInt(yf.get().format(d));
                                }
                            }
                            // Allow eventyear to remain at 0 if one of these exceptions occurs
                            //catch (ParseException e) { }
                            catch (NumberFormatException e) { }
                            break;
                    }

                    // Get the valid records by year
                    dataList = ca.getValidWorkIdsByUserIdAndEventYear(userId, reviewyear);
                }

                // Get the valid records by year
                dataList = ca.getValidWorkAndEventIdsByUserIdAndReviewYear(userId, reviewyear);

                return dataList;
            }
        };

        /** Each RecordTypeAssociation needs to provide a getAssociationRecords() method. */
        abstract List<String> getAssociationRecords(int userId, Map<String, String[]> reqData);
    }
}
