/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CreativeActivitiesRequestHandler.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.dao.creativeactivities.CreativeActivitiesDao;
import edu.ucdavis.mw.myinfovault.dao.creativeactivities.CreativeActivitiesDaoImpl;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Servlet to handle creative activities requests
 *
 * @author pradeeph
 * @since MIV 4.4
 */
public class CreativeActivitiesRequestHandler extends MIVServlet
{
    private static final long serialVersionUID = 201202081226L;
    private MIVUserInfo mui = null;
    private static CreativeActivitiesDao creativeActivitiesDao = new CreativeActivitiesDaoImpl();
//    boolean debug = MIVConfig.getConfig().isDebug();
    private HttpSession session = null;
    private int userId = -999;
    private String recordType = null;
    private String recID = null;
    private int year = 0;


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        logger.debug("\n ***** CreativeActivitiesRequestHandler doGet called! *****\t\tGET\n");

        initRequest(req,res);

        String requestType = getRequestType(req);
        String returnPage = "";

        if (requestType != null && requestType.length() > 0)
        {
            if (requestType.equalsIgnoreCase("add") || requestType.equalsIgnoreCase("remove")
                    || requestType.equalsIgnoreCase("searchavailable") || requestType.equalsIgnoreCase("searchselected"))
            {
                returnPage = modifyAssociations(requestType, req);
            }
        }
        else
        {
//            req.setAttribute("errormessage", "Error: Unknown Request");
//            returnPage = "/jsp/customerror.jsp";
            throw new MivSevereApplicationError("A request was made for an unknown request type: {0}", requestType);
        }

//        if (debug) System.out.println(userId + "-associate-" + recordType + " :: " + session.getAttribute(userId + "-associate-" + recordType));
        logger.debug("{}-associate-{} :: {}", new Object[] { userId, recordType, session.getAttribute(userId + "-associate-" + recordType) });

        RequestDispatcher dispatcher = req.getRequestDispatcher(returnPage);

        logger.debug("dispatching to target. (Dispatcher: [{}]", dispatcher);
        try
        {
            // Get the referer and set in the session servlet context.
            // This value will be retrieved by the PDF upload to allow returning to the
            // servlet which referred to this page, which is most likely ItemList servlet.
            String returnTo = req.getHeader("referer");
            req.getSession().setAttribute("returnTo", returnTo);

            res.setHeader("cache-control", "no-cache, must-revalidate");
            res.addHeader("Pragma", "no-cache");
            res.setDateHeader("Expires", System.currentTimeMillis() - 2);
            dispatcher.forward(req, res);
        }
//        catch (ServletException se) // FIXME: Don't just catch and re-throw. There's no point in catching here.
//        {
//            // log.throwing("EditRecord", "doGet", se);
//            throw (se);
//        }
//        catch (IOException ioe)
//        {
//            // log.throwing("EditRecord", "doGet", ioe);
//            throw (ioe);
//        }
        finally
        {
            logger.debug(" ***** Leaving CreativeActivitiesRequestHandler doGet  *****\t\tGET\n");
        }
    }


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.debug("\n ***** CreativeActivitiesRequestHandler doGet called! *****\t\tGET\n");

        initRequest(req,res);

//        if (debug) System.out.println(userId + "-associate-" + recordType + " :: " +session.getAttribute(userId + "-associate-" + recordType));
        logger.debug("{}-associate-{} :: {}", new Object[] { userId, recordType, session.getAttribute(userId + "-associate-" + recordType) });

        String returnPage = getAssociatePages(req);
        RequestDispatcher dispatcher = req.getRequestDispatcher(returnPage);

        logger.debug("dispatching to target. (Dispatcher: [{}]", dispatcher);
        try
        {
            // Get the referer and set in the session servlet context.
            // This value will be retrieved by the PDF upload to allow returning to the
            // servlet which referred to this page, which is most likely ItemList servlet.
            String returnTo = req.getHeader("referer");
            req.getSession().setAttribute("returnTo", returnTo);

            res.setHeader("cache-control", "no-cache, must-revalidate");
            res.addHeader("Pragma", "no-cache");
            res.setDateHeader("Expires", System.currentTimeMillis() - 2);
            dispatcher.forward(req, res);
        }
//        catch (ServletException se) // Don't just catch and re-throw. There's no point in catching here.
//        {
//            // log.throwing("EditRecord", "doGet", se);
//            throw (se);
//        }
//        catch (IOException ioe)
//        {
//            // log.throwing("EditRecord", "doGet", ioe);
//            throw (ioe);
//        }
        finally
        {
            logger.debug(" ***** Leaving CreativeActivitiesRequestHandler doGet  *****\t\tGET\n");
        }
    }


    /**
     * to prepare ids list based on data list, id column and separator
     * @param dataList
     * @param idColumn
     * @param separator
     * @return
     */
    private String getSelectedIds(List<Map<String, String>> dataList, String idColumn, String separator)
    {
        String ids = "";

        if (separator == null || separator.length() == 0)
        {
            separator = ",";
        }

        if (dataList != null)
        {
            for (Map<String, String> map : dataList)
            {
                if (ids.length() > 0) {
                    ids += (separator + " ");
                }
                ids += map.get(idColumn.toLowerCase());

//                if (ids.length() == 0)
//                {
//                    ids = map.get(idColumn.toLowerCase());
//                }
//                else
//                {
//                    ids += separator + " " + map.get(idColumn.toLowerCase());
//                }
            }
        }

        return ids;
    }


    /**
     * add or remove items from Available list
     * @param requestType Type of the request to add or remove items from Available list
     * @param req HttpServletRequest Object
     * @return String page name
     */
    private String modifyAssociations(String requestType, HttpServletRequest req)
    {
//        try
//        {
            List<Map<String, String>> itemList = new ArrayList<Map<String, String>>();
            List<Map<String, String>> availableList = null;
            List<Map<String, String>> selectedList = null;

            if (recordType != null)
            {
                if (recordType.equalsIgnoreCase("works"))
                {
                    List<Map<String, String>> eventList = creativeActivitiesDao.getEventsByUserId(userId);
                    List<Map<String, String>> publicationEventList = creativeActivitiesDao.getPublicationEventsByUserId(userId);

                    if (eventList != null && eventList.size() > 0)
                    {
                        itemList = eventList;
                    }

                    if (publicationEventList != null && publicationEventList.size() > 0)
                    {
                        if (itemList == null)
                        {
                            itemList = publicationEventList;
                        }
                        else
                        {
                            itemList.addAll(publicationEventList);
                        }
                    }
                }
                else if (recordType.equalsIgnoreCase("events") || recordType.equalsIgnoreCase("publicationsevents"))
                {
                    itemList = creativeActivitiesDao.getWorksByUserId(userId);
                }
                else if (recordType.equalsIgnoreCase("reviews"))
                {
                    List<Map<String, String>> workList = creativeActivitiesDao.getWorksByUserId(userId);
                    List<Map<String, String>> eventList = creativeActivitiesDao.getEventsByUserId(userId);

                    if (eventList != null && eventList.size() > 0)
                    {
                        itemList = eventList;
                    }

                    if (workList != null && workList.size() > 0)
                    {
                        if (itemList == null)
                        {
                            itemList = workList;
                        }
                        else
                        {
                            itemList.addAll(workList);
                        }
                    }
                }
            }

            String[] selectedids = null;
            String selectedIds = "";
            if (req.getParameter("selectedids") != null)
            {
                selectedids = req.getParameter("selectedids").replaceAll("\\s+","").split(",");
            }

            if (requestType != null)
            {
                availableList = new ArrayList<Map<String, String>>();
                selectedList = new ArrayList<Map<String, String>>();

                if (requestType.equals("add"))
                {
                    // prepare availableList and selectedList
                    String[] available = req.getParameterValues("available");

                    for (Map<String, String> map : itemList)
                    {
                        if (map != null && map.get("id") != null)
                        {
                            if (available == null)
                            {   // List all selected ids only
                                if (ArrayUtils.indexOf(selectedids, map.get("id")) != -1)
                                {
                                    selectedList.add(map);
                                }
                                else
                                {
                                    availableList.add(map);
                                }
                            }
                            else
                            {
                                if (ArrayUtils.indexOf(selectedids, map.get("id")) != -1
                                        || ArrayUtils.indexOf(available, map.get("id")) != -1)
                                {
                                    selectedList.add(map);
                                }
                                else
                                {
                                    availableList.add(map);
                                }
                            }
                        }
                    }
                }
                else if (requestType.equals("remove"))
                {
                    String[] selected = req.getParameterValues("selected");

                    for (Map<String, String> map : itemList)
                    {
                        if (map != null && map.get("id") != null)
                        {
                            if (ArrayUtils.indexOf(selectedids, map.get("id")) != -1
                                    && (selected == null || ArrayUtils.indexOf(selected, map.get("id")) == -1))
                            {
                                selectedList.add(map);
                            }
                            else
                            {
                                availableList.add(map);
                            }
                        }
                    }

                }
                else
                // for search
                {
                    for (Map<String, String> map : itemList)
                    {
                        if (map != null && map.get("id") != null)
                        {
                            if (ArrayUtils.indexOf(selectedids, map.get("id")) != -1)
                            {
                                selectedList.add(map);
                            }
                            else
                            {
                                availableList.add(map);
                            }
                        }
                    }
                }

                selectedIds = getSelectedIds(selectedList, "ID", ",");

                // filter code
                String searchText = "";
                if (req.getParameter("txtsearchavailable") != null && req.getParameter("txtsearchavailable").trim().length() > 0)
                {
                    searchText = req.getParameter("txtsearchavailable");
                    availableList = applyFilter(availableList,searchText);
                }

                if (req.getParameter("txtsearchselected") != null && req.getParameter("txtsearchselected").trim().length() > 0)
                {
                    searchText = req.getParameter("txtsearchselected");
                    selectedList = applyFilter(selectedList,searchText);
                }
            }

            setValidYear(availableList);
            setValidYear(selectedList);

            req.setAttribute("txtsearchselected", req.getParameter("txtsearchselected"));
            req.setAttribute("txtsearchavailable", req.getParameter("txtsearchavailable"));
            req.setAttribute("availableList", availableList);
            req.setAttribute("selectedList", selectedList);
            req.setAttribute("selectedIds", selectedIds);
            req.setAttribute("associationListJson", StringUtil.toJson(itemList));

            session.setAttribute(userId + "-associate-" + recordType, selectedIds);

//        }
//        catch (Exception e) // FIXME: Do NOT catch plain "Exception"
//        {
//            e.printStackTrace(); // FIXME: Do NOT .printStackTrace()
//            req.setAttribute("errormessage", "Error: "+e.getMessage());
//            return "/jsp/customerror.jsp";
//        }

        return "/jsp/creativeactivitiesassociation.jsp";
    }


    /**
     * to filter dataList by searchTxt
     * @param dataList
     * @param searchTxt
     * @return List<Map<String, String>>
     */
    private List<Map<String, String>> applyFilter(List<Map<String, String>> dataList, String searchTxt)
    {
        List<Map<String, String>> resultList = null;
        if (dataList != null && searchTxt != null && searchTxt.trim().length() > 0)
        {
            resultList = new ArrayList<Map<String,String>>();

            for (Map<String, String> map : dataList)
            {
                if (map != null && map.get("value") != null && map.get("value").toLowerCase().contains(searchTxt.toLowerCase()))
                {
                    resultList.add(map);
                }
            }
        }
        else
        {
            resultList = dataList;
        }

        return resultList;
    }


    /**
     * setup Associate Pages
     * @param req HttpServletRequest Object
     * @return String page name
     */
    private String getAssociatePages(HttpServletRequest req)
    {
//        try
//        {
            List<Map<String, String>> availableList = null;
            List<Map<String, String>> selectedList = null;
            List<Map<String, String>> tempList = null;
            List<Map<String, String>> itemList = new ArrayList<Map<String, String>>();

            if (recordType != null)
            {
                if (recordType.equalsIgnoreCase("works"))
                {
                    tempList = creativeActivitiesDao.getAssociatedEventsByUserIdAndWorkIds(userId, Integer.parseInt(recID));
                    if (tempList != null)
                    {
                        selectedList = tempList;
                    }
                    tempList = creativeActivitiesDao.getAssociatedPublicationEventsByUserIdAndWorkIds(userId, Integer.parseInt(recID));
                    if (tempList != null)
                    {
                        if (selectedList != null)
                        {
                            selectedList.addAll(tempList);
                        }
                        else
                        {
                            selectedList = tempList;
                        }
                    }

                    tempList = creativeActivitiesDao.getNonAssociatedEventsByUserIdAndWorkIds(userId, Integer.parseInt(recID));
                    if (tempList != null)
                    {
                        availableList = tempList;
                    }
                    tempList = creativeActivitiesDao.getNonAssociatedPublicationEventsByUserIdAndWorkIds(userId, Integer.parseInt(recID));
                    if (tempList != null)
                    {
                        if (availableList != null)
                        {
                            availableList.addAll(tempList);
                        }
                        else
                        {
                            availableList = tempList;
                        }
                    }
                }
                else if (recordType.equalsIgnoreCase("events"))
                {
                    selectedList = creativeActivitiesDao.getAssociatedWorksByUserIdAndEventIds(userId, Integer.parseInt(recID));
                    availableList = creativeActivitiesDao.getNonAssociatedWorksByUserIdAndEventIds(userId, Integer.parseInt(recID));
                }
                else if (recordType.equalsIgnoreCase("publicationevents"))
                {
                    selectedList = creativeActivitiesDao.getAssociatedWorksByUserIdAndPublicationEventIds(userId, Integer.parseInt(recID));
                    availableList = creativeActivitiesDao.getNonAssociatedWorksByUserIdAndPublicationEventIds(userId, Integer.parseInt(recID));
                }
                else if (recordType.equalsIgnoreCase("reviews"))
                {
                    tempList = creativeActivitiesDao.getAssociatedReviewEventsByUserIdAndReviewIds(userId, Integer.parseInt(recID));
                    if (tempList != null)
                    {
                        selectedList = tempList;
                    }
                    tempList = creativeActivitiesDao.getAssociatedReviewWorksByUserIdAndReviewIds(userId, Integer.parseInt(recID));
                    if (tempList != null)
                    {
                        if (selectedList != null)
                        {
                            selectedList.addAll(tempList);
                        }
                        else
                        {
                            selectedList = tempList;
                        }
                    }

                    tempList = creativeActivitiesDao.getNonAssociatedReviewEventsByUserIdAndReviewIds(userId, Integer.parseInt(recID));
                    if (tempList != null)
                    {
                        availableList = tempList;
                    }
                    tempList = creativeActivitiesDao.getNonAssociatedReviewWorksByUserIdAndReviewIds(userId, Integer.parseInt(recID));
                    if (tempList != null)
                    {
                        if (availableList != null)
                        {
                            availableList.addAll(tempList);
                        }
                        else
                        {
                            availableList = tempList;
                        }
                    }
                }
            }

            setValidYear(availableList);
            setValidYear(selectedList);

            String selectedIds = getSelectedIds(selectedList, "ID", ",");
            req.setAttribute("availableList", availableList);
            req.setAttribute("selectedList", selectedList);
            req.setAttribute("selectedIds", selectedIds);

            if (availableList != null) {
                itemList.addAll(availableList);
            }

            if (selectedList != null) {
                itemList.addAll(selectedList);
            }

            req.setAttribute("associationListJson", StringUtil.toJson(itemList));

            session.setAttribute(userId + "-associate-" + recordType, selectedIds);
//        }
//        catch (Exception e) // FIXME: Do NOT catch plain "Exception"
//        {
//            e.printStackTrace(); // FIXME: Do NOT .printStackTrace()
//            req.setAttribute("errormessage", "Error: "+e.getMessage());
//            return "/jsp/customerror.jsp";
//        }

        return "/jsp/creativeactivitiesassociation.jsp";
    }


    /**
     * to add isvalid field into datamap to validate record for given year
     * @param list
     */
    private void setValidYear(List<Map<String, String>> list)
    {
        if (year == 0 || year < 1900)
        {
            return;
        }

        if (recordType != null)
        {
            if (recordType.equalsIgnoreCase("works"))
            {
                for (Map<String, String> map : list)
                {
                    // work year must be less than or equal to event date year.
                    if (map != null && map.get("year") != null && Integer.parseInt(map.get("year")) >= year )
                    {
                        map.put("isvalid", "true");
                    }
                    else
                    {
                        map.put("isvalid", "false");
                    }
                }
            }
            else if (recordType.equalsIgnoreCase("events") || recordType.equalsIgnoreCase("reviews") || recordType.equalsIgnoreCase("publicationevents") )
            {
                for (Map<String, String> map : list)
                {
                    // event date year must be greater than or equal to work year. / review date year must be greater than or equal to work year/event date year.
                    if (map != null && map.get("year") != null && Integer.parseInt(map.get("year")) <= year )
                    {
                        map.put("isvalid", "true");
                    }
                    else
                    {
                        map.put("isvalid", "false");
                    }
                }
            }
            // else unknown record type?
        }
    }


    /**
     * to get the request type from requestObject
     * @param req HttpServletRequest Object
     * @return String
     */
    private String getRequestType(HttpServletRequest req)
    {
        logger.debug("ParameterMap :: {}", req.getParameterMap());

        String requestType = null;

        if (req.getParameter("requestType") != null)
        {
            requestType = req.getParameter("requestType");
        }
        else if (req.getParameter("add") != null && (req.getParameter("add").equals(">>") ||
                req.getParameter("add").equals("add")))
        {
            requestType = "add";
        }
        else if (req.getParameter("remove") != null && (req.getParameter("remove").equals("<<") ||
                req.getParameter("remove").equals("remove")))
        {
            requestType = "remove";
        }
        else if (req.getParameter("searchavailable") != null )
        {
            requestType = "searchavailable";
        }
        else if (req.getParameter("searchselected") != null )
        {
            requestType = "searchselected";
        }

        return requestType;
    }


    /**
     * initialize common parameters
     * @param req HttpServletRequest Object
     * @param res HttpServletResponse Object
     */
    private void initRequest(HttpServletRequest req, HttpServletResponse res)
    {
        recID = req.getParameter("recid");
        recordType = req.getParameter("rectype");
        if (req.getParameter("year")!=null)
        {
            year = Integer.parseInt("0"+getYear(req.getParameter("year")));
        }
        recordType = MIVUtil.sanitize(recordType);

        logger.debug("CreativeActivitiesRequestHandler - initRequest - record type is {}", recordType);

        if (recordType == null || recordType.length() == 0)
        {
//            showError("The record type is missing when trying to edit a record.", req, res);
//            return;
            throw new MivSevereApplicationError("The record type is missing when trying to edit a record.");
        }

        mui = mivSession.get().getUser().getTargetUserInfo();
        userId = mui.getPerson().getUserId();
        session = mivSession.get().getHttpSession();

        recordType = MIVUtil.sanitize(recordType);
        req.setAttribute("recordTypeName", recordType);

        if (recID == null || recID.length() == 0)
        {
            recID = "0000"; // dummy record id
        }

        String associatetitle = PropertyManager.getPropertySet(recordType, "labels").getProperty("associatetitle");
        String associationRule = PropertyManager.getPropertySet(recordType, "labels").getProperty("associationrule");

        req.setAttribute("associationRule", associationRule);
        req.setAttribute("recid", recID);
        req.setAttribute("recordID", recID);
        req.setAttribute("associatetitle", associatetitle);
        req.setAttribute("year", year);
    }


    /** Pattern for 4-digit year, used in getYear() */
    private static final Pattern yearPattern = Pattern.compile("\\d{4}");
    /**
     * get 4 digit Year
     * @param inputDateData
     * @return
     */
    private String getYear(String inputDateData)
    {
        String year = "0";
        if (StringUtils.hasText(inputDateData))
        {
//            Pattern p = Pattern.compile("\\d{4}"); // compile the pattern once, rather than in every call to getYear()
            Matcher m = yearPattern.matcher(inputDateData);
            if (m.find())
            {
                year = m.group();
            }
        }
        return year;
    }
}
