package edu.ucdavis.mw.myinfovault.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown by the API to return a 403.
 *
 * @author japorito
 * @since 5.0
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class ForbiddenException extends RuntimeException {
    private static final long serialVersionUID = 201504011447L;
}