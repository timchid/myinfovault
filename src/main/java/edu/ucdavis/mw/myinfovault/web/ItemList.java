/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ItemList.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.DisplaySection;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.SectionFetcher;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;
import edu.ucdavis.myinfovault.format.PreviewExtender;
import edu.ucdavis.myinfovault.format.RecordFormatter;
import edu.ucdavis.myinfovault.htmlcleaner.HtmlCleaner;
import edu.ucdavis.myinfovault.htmlcleaner.InvalidMarkupException;


/**
 * Display a list of records to be managed.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class ItemList extends MIVServlet
{
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        MIVUser user = mivSession.get().getUser();

        // We escape any HTML markup which may be present in the input parameters
        // to guard against cross-site scripting (XSS) attacks

        String type = req.getParameter("type");

        // if type is null or empty than lookup for a sectionid
        String sectionid = req.getParameter("sectionid");
        if (sectionid != null && sectionid.length() > 0)
        {
            Map<String, Map<String,String>> sectionMap = MIVConfig.getConfig().getMap("sectionbyid");
            if (sectionMap != null && sectionMap.get(sectionid) != null)
            {
                Map<String,String> section = sectionMap.get(sectionid);
                type = section.get("recordname");
                if (type != null)
                {
                    type = type.replace("-record", "");
                }
            }
        }

        String originalType = type;
        type = MIVUtil.sanitize(type);
        req.setAttribute("type", type);

        String subtype = MIVUtil.sanitize(req.getParameter("subtype"));   // some types (e.g. Books) may have sub-types (Edited, Reviewed)
        req.setAttribute("subtype", subtype);

        // SectionName and DocumentID are used by AdditionalInformation Related Pages.
        String sectionName = MIVUtil.sanitize(req.getParameter("sectionname"));

        String documentID = MIVUtil.sanitize(req.getParameter("documentid"));

        if (documentID != null && documentID.length() > 0)
        {
            // Make sure the documentID is a valid integer...validation is in place of escape markup here
            try
            {
               if (documentID != null)
               {
                   Integer.parseInt(documentID);
               }
            }
            catch (NumberFormatException nfe)
            {
//                showError("Document ID \"" + documentID + "\" is not a valid number in Item List", req, res);
                throw new MivSevereApplicationError("errors.itemlist.documentid", documentID);
//                return;
            }
        }

        String targetUrl = null;

        List<DisplaySection> sections = null;

        //FIX-ME: if type==null redirect to error
        if (type == null || type.length() <= 0)
        {
            String query = req.getQueryString();
            Exception e = new IllegalArgumentException(
                    "Record type is required (before sanitation [" + originalType + "]" +
                    " after sanitation [" + type + "])");
            logger.warn("Record type is missing when trying to display a list.\nQuery string was [" + query + "]", e);
//            showError("Record type is missing when trying to display a list.", req, res);
            throw new MivSevereApplicationError("Record type is missing when trying to display a list.");
//            return;
        }

        String recordType = type.toLowerCase();
        // As of 2007-Mar-07 we're not using subtypes, but this will allow it if we do later.
        if (subtype == null) subtype = "";
        if (subtype.length() > 0) {
            subtype = "-" + subtype.toLowerCase();
        }

        logger.debug("ItemList processing record type[{}] subtype [{}]", recordType, subtype);

        String recordClass = req.getParameter("recordClass");
        if (recordClass != null)
        {
            req.setAttribute("recordClass", recordClass);
        }

        Map<String,Object> config = new HashMap<String,Object>();

        Properties p = PropertyManager.getPropertySet(recordType, "config");
        config.put("config", p);

        p = PropertyManager.getPropertySet(recordType + subtype, "strings");
        config.put("strings", p);

        p = PropertyManager.getPropertySet(recordType + subtype, "tooltips");
        config.put("tooltips", p);

        p = PropertyManager.getPropertySet(recordType + subtype, "classes");
        config.put("classes", p);

        config.put("options", MIVConfig.getConfig().getConstants());

        // Get the SectionFetcher for the user whose records are being listed.
        // TODO: It's vaguely possible for getSectionFetcher() to return null (e.g. if the table is missing)
//        sections = user.getTargetUserInfo().getSectionFetcher().getRecordSections(recordType);
        SectionFetcher sf = user.getTargetUserInfo().getSectionFetcher();


        // TODO: preparing for HTML cleanup --- but this escapes too many things,
        // including the <span class="author"> tags inserted by the AuthorFormatter

        // Fetch the pieces for an "Additional Information" section
        if (recordType.equalsIgnoreCase("additional") && sectionName != null && sectionName.length() > 0)
        {
            // here we are using sectionname to get all the records that are related to that particular type of additional information.
            RecordFormatter[] f = null;
            sections = sf.getRecordSection(recordType, sectionName, f);
            // documentid will be used when we create a AdditionalInformation Heading. so we maintain documentid in request scope till the AdditionalHeaderRecord page
            req.setAttribute("documentid", documentID);
            req.setAttribute("sectionname", sectionName);

            // Get the headers
            Map<String,Map<String,String>> sectionHeaderMap = sf.getHeaderMap(recordType+"-record");
            if (sectionHeaderMap != null)
            {
                Map<String, String> sectionMap = sectionHeaderMap.get(sectionName);
                if (sectionMap != null && sectionMap.size() > 0) {
                    req.setAttribute("sectionheading", sectionMap.get("header"));
                }
            }
        }
        else // Handle all the normal types; anything other than Additional Information.
        {
            String[] fields = new String[2];
            /*fields[0] = "link";*/ // link is not the part of preview now so no need to include, Adding links separately to preview
            fields[0] = "sponsor";
            fields[1] = "remark";
            RecordFormatter f = new PreviewExtender(fields);
            sections = sf.getRecordSections(recordType, f);
            sections = this.resolveSections(sections, sf, f);
        }


        // An empty sections list means no records were found for the record type passed,
        // which is perfectly valid, but a 'null' sections list means the record type
        // is unknown.
        if (sections != null)// && !sections.isEmpty())
        {
            String page = MIVConfig.getConfig().getProperty(recordType + "-displaypage");
            targetUrl = "/jsp/" + (page != null && page.length() > 0 ? page : "recordlist.jsp");
            logger.debug("Attempted dispatch target is /jsp/{} - Final dispatch target is ", page, targetUrl);
        }


        if (targetUrl != null)
        {
            // XXX: SDP - "timestamp" was used to prevent re-displaying the Completed Message when the browser "Back" button was used.
            //        Q: Why was it removed?
            //if (req.getParameter("CompletedMessage") != null && req.getParameter("timestamp") != null)
            if (req.getParameter("CompletedMessage") != null)
            {
                /*long timeStamp = new Long(req.getParameter("timestamp")).longValue();
                long currentTimeStamp = System.currentTimeMillis();
                if (currentTimeStamp - timeStamp > 2000)
                {
                    req.setAttribute("CompletedMessage", "");
                }
                else
                {*/
                    // 1. getParameter already returns a String, don't need to toString() it.
                    // 2. we set the value of success, don't need to trim() it
                    // 3. we set the value of success to lower-case 'true' or 'false', don't need to ignore case on equals.
                    // 4. it's better to treat things as what they actually are, not as Strings. in this case, this is a Boolean result.
//                  if (req.getParameter("success") != null && req.getParameter("success").toString().trim().equalsIgnoreCase("true"))
                    if (Boolean.parseBoolean(req.getParameter("success")))
                    {
/* Things gone wrong in this block - too much repeated code:
   1. The number of times req.getParameter("CompletedMessage") is called.
   2. The number of (new HtmlCleaner())s that get instantiated.
   3. The number of times (HtmlCleaner).removeTags(getParameter(CompletedMessage)) is repeated.
   4. The formatting of the "}else"

                        String msg = req.getParameter("CompletedMessage");      // msg is set here, but there's no point; it's about to get replaced on the next line.
                        try
                        {
                            msg = (new HtmlCleaner()).removeTags(req.getParameter("CompletedMessage"));
                            if(msg.toLowerCase().contains("deleted"))
                            {
                                msg = "<div id=\"deletebox\">"+((new HtmlCleaner()).removeTags(req.getParameter("CompletedMessage")))+"</div>";
                            }else
                            {
                                msg = "<div id=\"successbox\">"+((new HtmlCleaner()).removeTags(req.getParameter("CompletedMessage")))+"</div>";
                            }

                        }
                        catch (InvalidMarkupException e)
                        {
                            if(msg.toLowerCase().contains("deleted"))
                            {
                                msg = "<div id=\"deletebox\">Record Deleted Successfully</div>";
                            }else
                            {
                                msg = "<div id=\"successbox\">Record Saved Successfully</div>";
                            }
                        }
                        req.setAttribute("CompletedMessage", msg);
                        req.setAttribute("success", req.getParameter("success").toString().trim());
*/
                        // Set up the default / common case
                        String origMsg = req.getParameter("CompletedMessage");
                        String boxType = "successbox";
                        String cleanMsg = "Record Saved Successfully";
                        if (origMsg.toLowerCase().contains("deleted")) {
                            boxType = "deletebox";
                            cleanMsg = "Record Deleted Successfully";
                        }
                        // Try to clean the message. Leave the defaults in place if cleaning fails.
                        final HtmlCleaner cleaner = new HtmlCleaner();
                        try {
                            cleanMsg = cleaner.removeTags(origMsg);
                        }
                        catch (InvalidMarkupException e) {
                            // Don't do anything. Leave the pre-set values in place.
                        }
                        final String finalMsg = "<div id=\"" + boxType + "\">" + cleanMsg + "</div>";
                        req.setAttribute("CompletedMessage", finalMsg);
                        // at this point we're in a block that's only executed if "success" evaluated to TRUE, so there's no point in re-fetching the value of "success"
                        //req.setAttribute("success", req.getParameter("success"));
                        req.setAttribute("success", "true");
                    }
                /*}*/
            }

            // Make the sections available to the JSP for display
            req.setAttribute("sections", sections);
            // Provide useful text strings for display
            req.setAttribute("constants", config);
            req.setAttribute("recordTypeName", recordType);
            // and send the request on to it for final processing.
            dispatch(targetUrl, req, res);
        }
        else {
//            showError("A request was made to display an unknown record type: " + recordType, req, res);
            throw new MivSevereApplicationError("errors.itemlist.recordtype", recordType);
        }

        //log.exiting(className, "doGet");
    }//doGet()


    /**
     * resolveSections - This method takes the list of sectons retrieved buy the getRecordSections method
     * and checks each record retrieved for each section. Each record is checked for the presence of a
     * referencerecordtype which indicates that the record references other record(s). If the referencerecordtype
     * is present along with a referencerecordid, the referenced records are resolved by passing the records to
     * the resolveAssociatedRecords method.
     *
     * Note that there may be multiple referencerecordtypes for a record, each of which is defined by the
     * referencerecordtype plus a sequence. For example, the Creative Works reviews record may reference
     * a works record and an event record. Each of these referencerecordtypes are defined in the select
     * for the reviews record as referencerecordtype0 and referencerecordtype1. Each of the other columns
     * relating to the associated records (referencerecordid and referencetable) also have sequence numbers
     * appended to the column name.
     *
     * @param sections - the sections retrieved via getRecordSections
     * @param sf - section fetcher instance
     * @param f - record formatter
     * @return List - List of DisplaySection objects
     */
    private List<DisplaySection> resolveSections(List<DisplaySection>sections, SectionFetcher sf, RecordFormatter f)
    {
        if (sections == null) return null;

        for (DisplaySection section : sections)
        {
            Map<String, Map<String, String>> records = section.getRecords();
            for (String parentRecordKey : records.keySet())
            {
                int referenceRecordSeq = 0;
                final Map<String, String> parentRec = records.get(parentRecordKey);
                while (parentRec.containsKey("referencerecordid" + referenceRecordSeq))
                {
                    if (parentRec.get("referencerecordid" + referenceRecordSeq) != null && // FIXME: we just checked containsKey 2 lines above here - why do we have to check for null here?
                       !parentRec.get("referencerecordid" + referenceRecordSeq).isEmpty())
                    {
                        Map<String, String> record = this.resolveAssociatedRecords("referencerecordtype" + referenceRecordSeq, sf, parentRec, f);
                        records.put(parentRecordKey, record);
                    }
                    referenceRecordSeq++;
                }
            }
        }

        return sections;
    }


    /**
     * resloveAssociatedRecords - This method uses the section fetcher to retrieve the input referencerecordtype
     * for the input records. When retrieved, the associated records are added to the records map with a key
     * of "preview" with a sequence number appended. The total number of associated records retrieved is also
     * added to the records map and is used in the recordlist.jsp page to determine the number of associated
     * preview records to display.
     *
     * @param referenceRecordTypeKey - The key for the referenceRecordType
     * @param sf - section fetcher instance
     * @param parentRecord - the map of the parent record to check for associated records
     * @param parentRecordKey - the key or record id of the parent record
     * @param f - RecordFormatter
     * @return records - the map of modified records.
     */
    private Map<String,String> resolveAssociatedRecords(String referenceRecordTypeKey, SectionFetcher sf, Map<String,String>parentRecord, RecordFormatter f)
    {
        // Get the records associated with this record id
        List<DisplaySection> associatedSections = sf.getRecordSection(parentRecord.get(referenceRecordTypeKey),null, parentRecord.get("parentrecordid"), f);
        for (DisplaySection associatedSection : associatedSections)
        {
            int seq=0;
            for (String associatedRecordKey : associatedSection.getRecords().keySet())
            {
                // There are potentially multiple associated record types so we must
                // insure a unique key for the associated preview records
                while (parentRecord.containsKey("preview"+seq))
                {
                    seq++;
                }
                parentRecord.put("previewrecordtype"+seq, parentRecord.get(referenceRecordTypeKey));
                parentRecord.put("preview"+seq, associatedSection.getRecords().get(associatedRecordKey).get("preview"));
                if(StringUtils.isNotEmpty(associatedSection.getRecords().get(associatedRecordKey).get("link")))
                {
                    parentRecord.put("link"+seq, associatedSection.getRecords().get(associatedRecordKey).get("link"));
                }
            }

            // Keep total count of the records associated with the parent record.
            int associatedRecCount = associatedSection.getRecords().size();
            if (parentRecord.containsKey("associatedRecordCount"))
            {
                associatedRecCount+=Integer.parseInt(parentRecord.get("associatedRecordCount"));
            }
            parentRecord.put("associatedRecordCount", ""+associatedRecCount);
        }
        return parentRecord;
    }


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        doGet(req, res);
    }
}
