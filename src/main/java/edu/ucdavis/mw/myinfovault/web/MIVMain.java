/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVMain.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.raf.RafService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVSession;


/**
 * Guarantee that MyInfoVault application and user information is initialized
 * and forward to the main display page.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class MIVMain extends MIVServlet
{
    private static final long serialVersionUID = 1L;

    private static final String MAIN_URL = "/jsp/MIVMain.jsp";
    private static final String APPOINTEE_MAIN_URL = "/EnterData";

    private DossierService dossierService = null;
    private UserService userService = null;
    private SigningService signingService = null;
    private DCService dcService = null;
    private RafService rafService = null;
    private PacketService packetService = null;


    MivActivityMonitor am;



    /**
     * @param dossierService Spring-injected dossier service
     */
    public void setDossierService(DossierService dossierService)
    {
        this.dossierService = dossierService;
    }

    /**
     * @param userService Spring-injected user service
     */
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    /**
     * @param signingService Spring-injected signing service
     */
    public void setSigningService(SigningService signingService)
    {
        this.signingService = signingService;
    }

    /**
     * @param dcService Spring-injected disclosure certificate service
     */
    public void setDcService(DCService dcService)
    {
        this.dcService = dcService;
    }

    /**
     * @param rafService Spring-injected recommended action form service
     */
    public void setRafService(RafService rafService)
    {
        this.rafService = rafService;
    }

    /**
     * @param rafService Spring-injected recommended action form service
     */
    public void setPacketService(PacketService packetService)
    {
        this.packetService = packetService;
    }


    /*
     * (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    @Override
    public void init() throws ServletException
    {
        super.init();

        //log.info("running MIVMain.init()");
        if (this.mivConfig == null) this.mivConfig = MIVConfig.getConfig();

        //log.info("MIVMain: userService is ["+userService+"]");
        if (this.userService == null) {
            //log.info("MIVMain: userService was null -- setting it");
            this.setUserService(MivServiceLocator.getUserService());
            //log.info("MIVMain: userService is now ["+userService+"]");
        }

        //log.info("MIVMain: dossierService is ["+dossierService+"]");
        if (this.dossierService == null) {
            //log.info("MIVMain: dossierService was null -- setting it");
            this.setDossierService(MivServiceLocator.getDossierService());
            //log.info("MIVMain: dossierService is now ["+dossierService+"]");
        }

        if (this.signingService == null) {
            this.setSigningService(MivServiceLocator.getSigningService());
        }
        //log.info("finished MIVMain.init()");

        if (this.dcService == null) {
            this.setDcService(MivServiceLocator.getDCService());
        }

        if (this.rafService == null) {
            this.setRafService(MivServiceLocator.getRafService());
        }

        if (this.packetService == null) {
            this.setPacketService(MivServiceLocator.getPacketService());
        }

        am = MivActivityMonitor.getMonitor();
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.MIVServlet#init(javax.servlet.ServletConfig)
     */
    @Override
    public void init(ServletConfig c) throws ServletException
    {
        super.init(c);
        this.init();
    }


    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        // Ensure a new-style MIVSession is created for this login/session
        // We don't have to save it, just guarantee one is created.
        // Don't use the superclass this.getSession() because this (MIVMain) is the entry point,
        // where we redirect to from other servlets and we'd infinitely loop.  Here's where we
        // make sure to create the session if needed.
        MIVSession ms = MIVSession.getSession(req, true);
        MivPerson realPerson = ms.getUser().getUserInfo().getPerson();
        MivPerson targetPerson = ms.getUser().getTargetUserInfo().getPerson();

        // If targetPerson has a primary Appointee role then show enter data list page as home page.
        if (targetPerson.getPrimaryRoleType() == MivRole.APPOINTEE)
        {
            dispatch(APPOINTEE_MAIN_URL, req, res);
            return;
        }

        Map<String, String> errors = new HashMap<String, String>();

        // Count of Dossiers to review.
        int reviewCount = 0;
        try
        {
            reviewCount = dossierService.findDossiersToReview(targetPerson.getUserId()).size();
        }
        catch (WorkflowException e1)
        {
            errors.put("errormsg", "MIVMain.java got a workflow exception getting the action count");
        }

        // Get disclosure certificate, dean decision/recommendation, and vice provost decision counts
        int dcCount = signingService.getRequestedSignatures(targetPerson, MivRole.CANDIDATE).size();
        int deanCount = signingService.getRequestedSignatures(targetPerson, MivRole.DEAN).size();
        int vpCount = signingService.getRequestedSignatures(targetPerson, MivRole.VICE_PROVOST).size();
        int provostCount = signingService.getRequestedSignatures(targetPerson, MivRole.PROVOST).size();
        int chancellorCount = signingService.getRequestedSignatures(targetPerson, MivRole.CHANCELLOR).size();
        int packetRequestCount = packetService.getOpenPacketRequests(targetPerson).size();

        // Put those into counts map
        Map<String, Integer> counts = new HashMap<String, Integer>();
        counts.put("OpenActions", 0/*actionList.size()*/);
        counts.put("ReviewDossiers", reviewCount);
        counts.put("SignDisclosure", dcCount);
        counts.put("SignDean", deanCount);
        counts.put("SignViceProvost", vpCount);
        counts.put("SignProvost", provostCount);
        counts.put("SignChancellor", chancellorCount);
        counts.put("PacketRequests",  packetRequestCount);

        if (targetPerson.hasRole(MivRole.OCP_STAFF))
        {
            counts.put("ViewProvost", signingService.getRequestedSignatures(MivRole.PROVOST).size());
            counts.put("ViewChancellor", signingService.getRequestedSignatures(MivRole.CHANCELLOR).size());
        }

        // Put counts map into request
        req.setAttribute("counts", counts);

        if (realPerson.hasRole(MivRole.SYS_ADMIN)) {
            req.setAttribute("activity", am.getActivity());
        }

        // Now send it off to the Main Page JSP
        dispatch(MAIN_URL, req, res);
    }


    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        doGet(req, res);
    }
}
