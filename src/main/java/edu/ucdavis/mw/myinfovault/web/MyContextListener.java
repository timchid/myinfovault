/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MyContextListener.java
 */


package edu.ucdavis.mw.myinfovault.web;


import java.lang.ref.Reference;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Mainly a place to experiment with context listeners, the one real this has
 * is to ensure the creation of the ActivityMonitor.<br>
 * What this has shown is that there are betters ways to bootstrap MIV at startup,
 * replacing the old "create singleton the first time it's fetched" strategy that,
 * for example, MIVConfig used before; MIVConfig is now created in the contextInitialized
 * method of MIVContextLoader.
 *
 * @author stephenp
 *
 */
@WebListener
public final class MyContextListener implements ServletContextListener
{
    @SuppressWarnings("unused")
    private ServletContext context = null;

    // The activity monitor doesn't have to be used; it only needs a reference to it so it doesn't get garbage collected.
    private MivActivityMonitor activityMonitor = null;

    public MyContextListener() {}


    // This method is invoked when the Web Application
    // is ready to service requests
    @Override
    public void contextInitialized(ServletContextEvent event)
    {
        this.context = event.getServletContext();

        // Output a simple message to the server's console
        System.out.println("MIV Is Ready");

        this.activityMonitor = MivActivityMonitor.getMonitor();
    }


    // This method is invoked when the Web Application has been removed
    // and is no longer able to accept requests.
    @Override
    public void contextDestroyed(ServletContextEvent event)
    {
        this.activityMonitor.shutdown();
        // Output a simple message to the server's console
        System.out.println("MIV Has Been Removed");
//        cleanThreadLocals();
        this.context = null;
        this.activityMonitor = null;
    }


    // copied straight from http://stackoverflow.com/questions/3869026/how-to-clean-up-threadlocals/16644476#16644476
    @SuppressWarnings("unused")
    private void cleanThreadLocals()
    {
        try {
            // Get a reference to the thread locals table of the current thread
            Thread thread = Thread.currentThread();
            Field threadLocalsField = Thread.class.getDeclaredField("threadLocals");
            threadLocalsField.setAccessible(true);
            Object threadLocalTable = threadLocalsField.get(thread);

            // Get a reference to the array holding the thread local variables inside the
            // ThreadLocalMap of the current thread
            Class<?> threadLocalMapClass = Class.forName("java.lang.ThreadLocal$ThreadLocalMap");
            Field tableField = threadLocalMapClass.getDeclaredField("table");
            tableField.setAccessible(true);
            Object table = tableField.get(threadLocalTable);

            // The key to the ThreadLocalMap is a WeakReference object. The referent field of this object
            // is a reference to the actual ThreadLocal variable
            Field referentField = Reference.class.getDeclaredField("referent");
            referentField.setAccessible(true);

            for (int i=0; i < Array.getLength(table); i++)
            {
                try {
                    // Each entry in the table array of ThreadLocalMap is an Entry object
                    // representing the thread local reference and its value
                    Object entry = Array.get(table, i);
                    if (entry != null) {
                        // Get a reference to the thread local object and remove it from the table
                        ThreadLocal<?> threadLocal = (ThreadLocal<?>)referentField.get(entry);
                        threadLocal.remove();
                    }
                } catch (Throwable t) { // catch and ignore any error while in the loop.
                    //@SuppressWarnings("unused")
                    int x10 = 10; // just a place to put a breakpoint.
                }
            }
        }
        catch (Exception e) {
            // We will tolerate an exception here and just log it
            throw new IllegalStateException(e);
        }
    }
}

