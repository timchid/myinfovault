/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PatentEditRecord.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import edu.ucdavis.mw.myinfovault.service.publications.PatentType;
import edu.ucdavis.mw.myinfovault.valuebeans.ValidateResultVO;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.Validator;


/**
 * Patent specific Servlet, it used to save and update patent records.
 * Patent records need special validation to cross reference required fields
 * based on the selected patent type ID (patent or disclosure) and the patent
 * status (filed vs. granted)
 *
 * @author Pradeep Haldiya
 * @since MIV 4.2
 */
public class PatentEditRecord extends EditRecord
{
    private static final long serialVersionUID = 201112291629L;

    /**
     * Gather the request parameters and values into parallel arrays.
     * The first array is the list of parameter names; the second is the values.
     *
     * @param reqData Map of request's parameter name and value
     * @return An array of exactly two (2) String arrays.
     */
    @Override
    ValidateResultVO collectValidateParameters(Map <String, String[]> reqData, Properties cfgProps)
    {
        ValidateResultVO validateResultVO = null;

        Validator validator = new Validator();
        List<String> fields = new LinkedList<String>();
        List<String> values = new LinkedList<String>();
        String[][] retArray = { {}, {} };
        String[] returnType = {};

        java.util.Set<String> requiredFields = new java.util.HashSet<String>();
        String typeid = "";
        if (reqData.get("typeid") != null && reqData.get("typeid").length > 0)
        {
            typeid = reqData.get("typeid")[0];

            String reqStringKey = PatentType.getPatentTypeById(Integer.parseInt(typeid)).getTitle().toLowerCase() + "-required";

            if (PatentType.getPatentTypeById(Integer.parseInt(typeid)) == PatentType.PATENT)
            {
                String statusid = reqData.get("patentstatusid")[0];

                reqStringKey = PatentType.getPatentTypeById(Integer.parseInt(typeid)).getTitle().toLowerCase() + "-"
                        + MIVConfig.getConfig().getMap("patentstatus").get(statusid).get("status").toString().toLowerCase()
                        + "-required";
            }

            String reqString = cfgProps.getProperty(reqStringKey);
            String [] requiredFieldList = null;
            requiredFieldList = reqString.split("\\s*[,]\\s*");
            for (String s : requiredFieldList) {
                requiredFields.add(s.replaceAll("\"", ""));
            }

        }

        Iterator<String> reqIterator = reqData.keySet().iterator();

        while (reqIterator.hasNext())
        {
            String pname = reqIterator.next();
            if (EditRecordHelper.skipFields.contains(pname)) continue;
            //dojo sends _displayed_ fields from IE
            if (pname.endsWith("_displayed_")) continue;
            String key = "constraints-" + pname;
            String constraints = cfgProps.getProperty(key);
            if (requiredFields.contains(pname))
            {
                if (constraints == null || constraints.trim().length() == 0) {
                    constraints = "required";
                }
                else {
                    constraints = "required," + constraints;
                }
            }

            //TODO: Currently the PatentEditRecord only processes one value for a parameter
            String value = reqData.get(pname)[0];
            value = MIVUtil.replaceNumericReferences(value);

            if (constraints != null)
            {
                String [] temp = null;
                temp = constraints.split("\\s*[,]\\s*");
                value = validator.validate(pname, value, temp);
            }

            fields.add(pname);
            values.add(value);
        }

        retArray[0] = fields.toArray(returnType);
        retArray[1] = values.toArray(returnType);

        if(validator.isValid()){
            validateResultVO = new ValidateResultVO(retArray);
        }else{
            validateResultVO = new ValidateResultVO(validator.getErrorMessageList(),validator.getErrorFieldList());
        }

        return validateResultVO;
    }
}
