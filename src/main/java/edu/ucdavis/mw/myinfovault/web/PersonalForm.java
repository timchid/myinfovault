/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonalForm.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;

import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.data.Address;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.PersonalData;
import edu.ucdavis.myinfovault.data.Phone;
import edu.ucdavis.myinfovault.data.RecordHandler;
import edu.ucdavis.myinfovault.data.SectionFetcher;
import edu.ucdavis.myinfovault.data.exception.MIVSaveRecordException;


/**
 * This servlet retrieves personal information from data source and sends it to personal edit form.
 * Once user saves the personal record this one saves the data into UserPersonal, UserPhone, UserEmail,
 * UserLink, UserAddress tables.
 *
 * @author Venkat Vegesna
 * @since MIV v2.0
 */
public class PersonalForm extends MIVServlet
{
    private static final long serialVersionUID = 200708221629L;
    private static final String EXCEPTION_MSG = "Exception occurred while attempting to save" + " ";


    /**
     * In doGet we retrieve the personal data using SectionFetcher and present data to jsp.
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();

        // Creating custom object, personal data to present data onto personal edit page.
        PersonalData perData = new PersonalData();
        // Retrieving PersonalData from SectionFetcher
        SectionFetcher sf = mui.getSectionFetcher();
        perData = sf.getPersonalData();
        logger.debug("Personal data object is {}", perData);

        // Setting the PersonalData object in request scope for PersonalForm.jsp
        req.setAttribute("personaldata", perData);

        if (req.getParameter("CompletedMessage") != null && req.getParameter("timestamp") != null)
        {
            long timeStamp = new Long(req.getParameter("timestamp")).longValue();
            long currentTimeStamp = System.currentTimeMillis();
            if (currentTimeStamp - timeStamp > 2000)
            {
                req.setAttribute("CompletedMessage", "");
            }
            else
            {
                req.setAttribute("CompletedMessage", req.getParameter("CompletedMessage"));
            }
            req.setAttribute("success", true);
        }

        String targetUrl = "/jsp/PersonalForm.jsp";
        dispatch(targetUrl, req, res);
    }


    /**
     * In doPost we collect data from jsp and save the information into the data source using local methods.
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();
        int userID = mui.getPerson().getUserId();
        RecordHandler rh = mui.getRecordHandler();
        String currentRecord = "Personal Information";
        String failedRecords = "";
        String errorString="";
        String displayName = req.getParameter("displayName");
        // validate page
        if (!StringUtils.hasText(displayName))
        {
            errorString += "<li>Display Name : This field is required.</li>";
        }

        Map perData = EditRecordHelper.getFormParameterMap(req);
        //System.out.println(perData); // logger.debug?

        String targetUrl = "/jsp/PersonalForm.jsp";
        req.setAttribute("rec", perData);

        // check for error
        if (StringUtils.hasText(errorString))
        {
            String resultUrl = req.getContextPath() + "/PersonalForm";
            if (isAjaxRequest())
            {
                //ServletOutputStream out = res.getOutputStream();
                PrintWriter out = res.getWriter();
                out.print("{ success:false, resultUrl:'" + resultUrl +
                          "', timestamp:" + System.currentTimeMillis() +
                          ",CompletedMessage:" + errorString + "}");
            }
            else
            {
                req.setAttribute("success", "false");
                req.setAttribute("CompletedMessage", "Error(s) : <ul>" + errorString + "</ul>");
                req.setAttribute("timestamp", System.currentTimeMillis());

                // Get the referer and set in the session servlet context.
                // This value will be retrieved by the PDF upload to allow returning to the
                // servlet which referred to this page, which is most likely ItemList servlet.
                String returnTo = req.getHeader("referer");
                req.getSession().setAttribute("returnTo", returnTo);

                res.setHeader("cache-control", "no-cache, must-revalidate");
                res.addHeader("Pragma", "no-cache");
                res.setDateHeader("Expires", System.currentTimeMillis() - 2);
                dispatch(targetUrl, req, res);
            }
            return;
        }

        try
        {
                String pStreet1 = req.getParameter("paddressstreet1");
                String pStreet2 = req.getParameter("paddressstreet2");
                String pCity = req.getParameter("pcity");
                String pState = req.getParameter("pstate");
                String pZip = req.getParameter("pzip");
                String pRecID = req.getParameter("pAddressRecID");

                String cStreet1 = req.getParameter("caddressstreet1");
                String cStreet2 = req.getParameter("caddressstreet2");
                String cCity = req.getParameter("ccity");
                String cState = req.getParameter("cstate");
                String cZip = req.getParameter("czip");
                String cRecID = req.getParameter("cAddressRecID");

                String wStreet1 = req.getParameter("waddressstreet1");
                String wStreet2 = req.getParameter("waddressstreet2");
                String wCity = req.getParameter("wcity");
                String wState = req.getParameter("wstate");
                String wZip = req.getParameter("wzip");
                String wRecID = req.getParameter("wAddressRecID");

                String pTelPhone = req.getParameter("pTelephone");
                String pTelRecID = req.getParameter("pTelRecID");

                String pFaxNumber = req.getParameter("pFax");
                String pFaxRecID = req.getParameter("pFaxRecID");

                String cTelPhone = req.getParameter("cTelephone");
                String cTelRecID = req.getParameter("cTelRecID");

                String cFaxNumber = req.getParameter("cFax");
                String cFaxRecID = req.getParameter("cFaxRecID");

                String wTelPhone = req.getParameter("wTelephone");
                String wTelRecID = req.getParameter("wTelRecID");

                String wFaxNumber = req.getParameter("wFax");
                String wFaxRecID = req.getParameter("wFaxRecID");

                String cellPhoneNumber = req.getParameter("cellPhone");
                String cellRecID = req.getParameter("cellRecID");

                try
                {
                    currentRecord = "Permanent home address ";
                    saveAddress(userID, Address.PERMANENT_HOME_ADDRESS, pRecID, pStreet1, pStreet2, pCity, pState, pZip, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords = currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Current home address ";
                    saveAddress(userID, Address.CURRENT_HOME_ADDRESS, cRecID, cStreet1, cStreet2, cCity, cState, cZip, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Office address ";
                    saveAddress(userID, Address.OFFICE_ADDRESS, wRecID, wStreet1, wStreet2, wCity, wState, wZip, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Permanent home phone ";
                    savePhoneNumbers(userID, Phone.PERMANENT_HOME_PHONE, pTelRecID, pTelPhone, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Permanent home fax ";
                    savePhoneNumbers(userID, Phone.PERMANENT_FAX, pFaxRecID, pFaxNumber, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Current home phone ";
                    savePhoneNumbers(userID, Phone.CURRENT_HOME_PHONE, cTelRecID, cTelPhone, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Current home fax ";
                    savePhoneNumbers(userID, Phone.CURRENT_FAX, cFaxRecID, cFaxNumber, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Office phone ";
                    savePhoneNumbers(userID, Phone.OFFICE_PHONE, wTelRecID, wTelPhone, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Office fax ";
                    savePhoneNumbers(userID, Phone.OFFICE_FAX, wFaxRecID, wFaxNumber, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                try
                {
                    currentRecord = "Mobile phone ";
                    savePhoneNumbers(userID, Phone.MOBILE_PHONE, cellRecID, cellPhoneNumber, rh);
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }


            /* ********* Email Related ***************************************** */
                try
                {
                    currentRecord = "Email ";
                    String emailRecID = req.getParameter("emailRecID");
                    String email = MIVUtil.replaceNumericReferences(req.getParameter("email"));
                    if (emailRecID != null && emailRecID.length() > 0)
                    {
                        String[] updateFields = { "value" };
                        String[] updateValues = { email };
                        int count = rh.update("UserEmail", Integer.parseInt(emailRecID), updateFields, updateValues);
                        if (count < 1)
                        {
                            logger.error("Update failed on user email record, record number {}", emailRecID);
                            throw new MIVSaveRecordException("Update failed on user email record, record number"+emailRecID);
                        }
                    }
                    else if (email != null && email.length() > 0)
                    {
                        String[] insertFields = { "value", "Display" };
                        String[] insertValues = { email, "1" };
                        int newRecordID = rh.insert("UserEmail", insertFields, insertValues);
                        if (newRecordID == -1)
                        {
                            logger.error("Save failed on user email record");
                            throw new MIVSaveRecordException("Save failed on user email record");
                        }
                    }
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }


                /* ********* URL Related ***************************************** */
                try
                {
                    currentRecord = "web site ";
                    String urlRecID = req.getParameter("urlRecID");
                    String url = MIVUtil.replaceNumericReferences(req.getParameter("url"));
                    if (urlRecID != null && urlRecID.length() > 0)
                    {
                        String[] updateFields = { "value" };
                        String[] updateValues = { url };
                        int count = rh.update("UserLink", Integer.parseInt(urlRecID), updateFields, updateValues);
                        if (count < 1)
                        {
                            logger.error("Update failed on user link record, record number {}", urlRecID);
                            throw new MIVSaveRecordException("Update failed on user link record, record number " + urlRecID);
                        }
                    }
                    else if (url != null && url.length() > 0)
                    {
                        String[] insertFields = { "value", "Display" };
                        String[] insertValues = { url, "1" };
                        int newRecordID = rh.insert("UserLink", insertFields, insertValues);
                        if (newRecordID == -1)
                        {
                            logger.error("Save failed on user link record");
                            throw new MIVSaveRecordException("Save failed on user link record");
                        }
                    }
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                /* *********** Personal Information Related  ************************* */
                try
                {
                    currentRecord = "Personal Information ";
                    String perRecId = req.getParameter("perRecID");
                    String usCitizenInd = req.getParameter("usCitizenInd");
                    int intUsCitizenInd = Integer.parseInt(usCitizenInd);
                    String dob = req.getParameter("dob");
                    String dateEntered = req.getParameter("dateEntered");
                    String visaType = req.getParameter("visaType");

                    displayName = MIVUtil.replaceNumericReferences(displayName);
//                    mui.setDisplayName(displayName); // MIV-2818 this display name isn't used for User Info any more - replaced by White Pages
                    dob = MIVUtil.replaceNumericReferences(dob);
                    dateEntered = MIVUtil.replaceNumericReferences(dateEntered);
                    visaType = MIVUtil.replaceNumericReferences(visaType);

                    String[] fields = { "birthDate", "citizen", "dateEntered", "visaType", "displayname" };
                    String[] values = { dob, "" + intUsCitizenInd, dateEntered, visaType, displayName };
                    if (perRecId != null && perRecId.length() > 0 && !perRecId.equals("0"))
                    {
                        int count = rh.update("UserPersonal", Integer.parseInt(perRecId), fields, values);
                        if (count < 1)
                        {
                            logger.error("Update failed on user personal record, record number {}", perRecId);
                            throw new MIVSaveRecordException("Update failed on user personal record, record number " + perRecId);
                        }
                    }
                    else
                    {
                        int newRecordID = rh.insert("UserPersonal", fields, values);
                        if (newRecordID == -1)
                        {
                            logger.error("Save failed on user personal record");
                            throw new MIVSaveRecordException("Save failed on user personal record");
                        }
                    }
                }
                catch (MIVSaveRecordException e)
                {
                    failedRecords += (isEmpty(failedRecords) ? "" : ", ") + currentRecord;
                    logger.error(EXCEPTION_MSG + currentRecord, e);
                }

                if (failedRecords != null && failedRecords.length() > 0)
                {
                    throw new MIVSaveRecordException("An error occured while saving/updating following record(s) "+failedRecords);
                }
        }
        catch (MIVSaveRecordException e)
        {
            ServletOutputStream out = res.getOutputStream();
            String resultUrl = req.getContextPath()+"/jsp/errorpage.jsp?error=message&from="+this.className+"&message="+e.getMessage();
            logger.error("An exception occurred while saving " + e.getMessage(), e);
            out.print("{ success:false, resultUrl:'"+resultUrl+"', timestamp:" + System.currentTimeMillis() +"}");
        }
        catch (NullPointerException e)
        {
            ServletOutputStream out = res.getOutputStream();
            String resultUrl = req.getContextPath()+"/jsp/errorpage.jsp?error=message&from="+this.className+"&message=An error occured while saving personal information";
            logger.error("An exception occurred while saving " + e.getMessage(), e);
            out.print("{ success:false, resultUrl:'"+resultUrl+"', timestamp:" + System.currentTimeMillis() +"}");
        }

        if (isEmpty(failedRecords))
        {

            String message = "Your Personal Information has been saved";

            if (isAjaxRequest())
            {
                String resultUrl = req.getContextPath()+"/PersonalForm";
                ServletOutputStream out = res.getOutputStream();
                out.print("{ success:true, message:'"+message+"', resultUrl:'"+resultUrl+"', timestamp:" + System.currentTimeMillis() +"}");
            }
            else
            {
                StringBuilder redirectPath = new StringBuilder(req.getContextPath());
                redirectPath.append("/PersonalForm?success=true");
                redirectPath.append("&CompletedMessage=").append(message);
                redirectPath.append("&timestamp=").append(System.currentTimeMillis());

                res.setContentType("text/html");
                res.sendRedirect(redirectPath.toString());
            }
            return;
        }
    }
    //doPost()


    /**
     * Save the phone numbers into the data source which are retrieved from the page.
     *
     * @param userID ID of the user whose record needs to be saved or updated. we are not using it now.
     * @param phoneType to specify which kind of phone for e.g. permanent home, current home etc.
     * @param recID if "0" means saving new record else updating the existing record.
     * @param phoneNumber number to saved or updated.
     * @param rh record handler to either save or update the record.
     */
    private void savePhoneNumbers(int userID, int phoneType, String recID, String phoneNumber, RecordHandler rh) throws MIVSaveRecordException
    {
        phoneNumber = MIVUtil.replaceNumericReferences(phoneNumber);
        if (recID != null && recID.length() > 0)
        {
            // this block will be executed when we have record id, that means we are updating the record

            String[] updateFields = { "Number" };
            String[] updateValues = { phoneNumber };
            int count = rh.update("UserPhone", Integer.parseInt(recID), updateFields, updateValues);
            if (count < 1)
            {
                logger.error("Update failed on user phone record, record number {}", recID);
                throw new MIVSaveRecordException("Update failed on user phone record ");
            }

        }
        else if (phoneNumber != null && phoneNumber.length() > 0)
        {
            String[] insertFields = { "TypeID", "Number", "Display" };
            String[] insertValues = { "" + phoneType, phoneNumber, "1" };
            int recId = rh.insert("UserPhone", insertFields, insertValues);
            if (recId == -1)
            {
                logger.error("Save failed on user phone record");
                throw new MIVSaveRecordException("Save failed on UserPhone record");
            }
        }
    }
    //savePhoneNumbers()


    private static String[] addressFields = { "TypeID", "Street1", "Street2", "City", "State", "ZipCode", "Display" };
    /**
     * Get the addresses from the page and save them in the database.
     *
     * @param userID ID of the user whose record needs to be saved or updated. we are not using it now.
     * @param addressType to specify which kind of address for eg. permanent address,cureent address etc.
     * @param recID if "0" means saving new record else updating the existing record.
     * @param street1 Strre1 of the address.
     * @param street2 Strre2 of the address.
     * @param city city of the address.
     * @param state state of the address.
     * @param zip zip code of the address.
     * @param rh record handler to either save or update the record.
     */
    private void saveAddress(int userID, int addressType, String recID, String street1, String street2, String city, String state, String zip, RecordHandler rh) throws MIVSaveRecordException
    {
        street1 = MIVUtil.replaceNumericReferences(street1);
        street2 = MIVUtil.replaceNumericReferences(street2);
        city = MIVUtil.replaceNumericReferences(city);
        state = MIVUtil.replaceNumericReferences(state);
        zip = MIVUtil.replaceNumericReferences(zip);
        String[] values = { Integer.toString(addressType), street1, street2, city, state, zip, "1" };

        // this block will be executed when we have record id, that means we are updating the record
        if (recID != null && recID.length() > 0)
        {
            int recordID = 0;
            try
            {
                recordID = Integer.parseInt(recID);
                int updateCount = 0;
                updateCount = rh.update("UserAddress", recordID, addressFields, values);
                // Check for non-zero update count
                if (updateCount < 1)
                {
                    logger.error("Update failed on user address record, record number {}", recordID);
                    throw new MIVSaveRecordException("Update failed on user address record");
                }
            }
            catch (NumberFormatException nfe) {
                // what to do here?      message = " Got exception";
                logger.warn("Bad record ID number found ["+recID+"] when trying to update an Address", nfe);
            }
        }
        // this block will be executed when we don't have record id, that means we are inserting a new record
        else if ((street1 != null && street1.length() > 0) || (street2 != null && street2.length() > 0)
                || (city != null && city.length() > 0) || (state != null && state.length() > 0) || (zip != null && zip.length() > 0))
        {
            int newRecordID = 0;
            newRecordID = rh.insert("UserAddress", addressFields, values);
            // TODO(done): test for new record ID != -1 (which indicates an error)
            if (newRecordID == -1)
            {
                logger.error("Save failed on user address record");
                throw new MIVSaveRecordException("Save failed on user address record");
            }
        }
    }
    //saveAddress()


    /**
     * this method checks not null and non-empty for the passed string.
     *
     * @param s -- variable that is to be checked for not null and non-empty.
     * @return -- either true or false.
     */
    private boolean isEmpty(String s)
    {
        return s == null || s.length() <= 0;
    }
}
