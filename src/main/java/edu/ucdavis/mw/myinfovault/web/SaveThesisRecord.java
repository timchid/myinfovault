/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SaveThesisRecord.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.myinfovault.http.MivRequestAdapter;



/**
 * Perform special processing for Thesis Committee records, then send to EditRecord
 * for standard save processing.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class SaveThesisRecord extends MIVServlet
{
    private static final long serialVersionUID = 200703301601L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        logger.trace(" ***** SaveThesisRecord doPost called");

        Map<String, String[]> reqData = getRequestMap(req, false);

        MivRequestAdapter newreq = new MivRequestAdapter(req, reqData);

        String degStatus = (reqData.get("degreestatusid") != null ? reqData.get("degreestatusid")[0] : null);
        String degType = (reqData.get("degreetypeid") != null ? reqData.get("degreetypeid")[0] : null);
        String committee = (reqData.get("committeeposition") != null ? reqData.get("committeeposition")[0] : null);

        /*
        BufferedReader br = req.getReader();
        String line;
        System.out.println(" Begin reading lines");
        do {
            line = br.readLine();
            if (line != null) {
                System.out.println("   Line: ["+line+"]");
            }
        }
        while (line != null);
        System.out.println(" Done reading lines");
        */

        if ("1".equals(committee))
        {
            //System.out.println(" Committee Member");
            newreq.addParam("committeemember", "1");
            newreq.addParam("committeechair", "0");
            newreq.addParam("cochair", "0");
        }
        else if ("2".equals(committee))
        {
            //System.out.println(" Committee Chair");
            newreq.addParam("committeemember", "0");
            newreq.addParam("committeechair", "1");
            newreq.addParam("cochair", "0");
        }
        else
        {
            newreq.addParam("committeemember", "0");
            newreq.addParam("committeechair", "0");
            newreq.addParam("cochair", "1");
        }
        newreq.removeParam("committeeposition");


        if ("1".equals(degType))
        {
            //System.out.println(" Masters Degree");
            newreq.addParam("mastersdegree", "1");
            newreq.addParam("doctoraldegree", "0");
            newreq.addParam("doctoratedegree", "0");
        }
        else if ("2".equals(degType))
        {
            //System.out.println(" Ph.D.");
            newreq.addParam("mastersdegree", "0");
            newreq.addParam("doctoraldegree", "1");
            newreq.addParam("doctoratedegree", "0");
        }
        else
        {
            newreq.addParam("mastersdegree", "0");
            newreq.addParam("doctoraldegree", "0");
            newreq.addParam("doctoratedegree", "1");
        }
        newreq.removeParam("degreetypeid");


        if ("1".equals(degStatus))
        {
            //System.out.println(" Degree still In Progress");
            newreq.addParam("degreeawarded", "0");
            newreq.addParam("degreeprogress", "1");
        }
        else
        {
            //System.out.println(" Degree Awarded");
            newreq.addParam("degreeawarded", "1");
            newreq.addParam("degreeprogress", "0");
        }
        newreq.removeParam("degreestatusid");

        String target = "EditRecord";
        dispatch(target, newreq, res);

        logger.trace(" ***** Leaving SaveThesisRecord doPost");
    }
    //doPost()
}
