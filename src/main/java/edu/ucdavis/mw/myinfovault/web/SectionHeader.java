/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SectionHeader.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.SectionHeaderUtil;
import edu.ucdavis.myinfovault.data.MIVUtil;


/**
 * This servlet is responsible for updating the Section Header.
 * If the user section header matches the system section header, the record in the UserSectionHeader would be
 * deleted but if the user section header is different from system section header a new record would be created in
 * UserSectionHeader, if there is no record present, otherwise it would update the existing UserSectionHeader for that user.
 *
 * @author Brij Garg
 * @since MIV 2.0
 */
public class SectionHeader extends MIVServlet
{
    private static final long serialVersionUID = 2235276578843321814L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.trace("\n ***** SectionHeader doGet called! *****\t\tGET\n");

        try
        {
            if (!isAjaxRequest())
            {
                editHeader(req, res);
            }
            else
            {
                MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();
                int userId = mui.getPerson().getUserId();
                int insertUserId = mivSession.get().getUser().getUserId();

                String strAction = req.getParameter("action");

                String strSectionId = req.getParameter("sectionid");
                int sectionId = strSectionId != null ? new Integer(strSectionId).intValue() : 0;

                String strAdditionalSectionId = req.getParameter("addheaderid");
                int additionalSectionId = strAdditionalSectionId != null ? new Integer(strAdditionalSectionId).intValue() : 0;

                String strNewHeader = req.getParameter("header");

                String strDisplayOption = req.getParameter("displayoption");
                boolean display = false;
                if (strDisplayOption != null && "1".equals(strDisplayOption)) {
                    display = true;
                }

                /* NOTE!  What if no 'action' parameter was sent?
                 * That should only happen when we're originally doing the non-javascript edit using the form
                 * so this code should not be reached; BUT tampering by removing js=false can get us here.
                 * In that case, this code will fall through, not executing any of the actions below, and
                 * just echoing the input header back to the output.  Echoing like this leaves an XSS
                 * vulnerability, which we've closed by using escapeMarkup() just prior to sending back
                 * the new header.
                 * TODO: should this throw an error if no action is found?
                 */
                if ("save".equals(strAction)) {
                    // Call SectionHeaderUtil.updateUserSectionHeader which will identify if the record need to be updated
                    // with header and/or display option or if a record has to be created or deleted.
                    SectionHeaderUtil.updateUserSectionHeader(userId, sectionId, strNewHeader, insertUserId, display);
                }
                else if ("restore".equals(strAction))
                {
                    strNewHeader = SectionHeaderUtil.getSystemSectionHeader(sectionId);
                    if ("0".equals(strDisplayOption))
                    {
                        // If restore default header action is triggered and the checbox is disabled, update the existing UserSectionHeader
                        // with Display as '0' and the header value same as SystemSectionHeader.
                        SectionHeaderUtil.updateUserSectionHeader(userId, sectionId, strNewHeader, insertUserId, display);
                    }
                    else
                    {
                        //If restore default header action is triggered and the checbox is enabled, delete any existing UserSectionHeader
                        SectionHeaderUtil.deleteUserSectionHeader(sectionId, userId);
                    }
                }
                else if ("saveadditional".equals(strAction)) {
                    SectionHeaderUtil.updateAdditionalSectionHeader(additionalSectionId, strNewHeader);
                }
                ServletOutputStream out = null;
                out = res.getOutputStream();
                res.setContentType("text/html");
                out.print(MIVUtil.escapeMarkup(strNewHeader));
            }
        }
        catch(IOException e) {
            //log.log(Level.WARNING, "IOException while editing the User Section Header " + e);
            logger.error("IOException while editing the User Section Header", e);
        }
        catch(Exception e) {
            //log.log(Level.WARNING, "Exception while editing the User Section Header " + e);
            logger.error("Exception while editing the User Section Header", e);
        }

        //log.exiting(className, "doGet");
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        // If reset was selected don't perform anything, just dispatch the request to DesignPacketController
        if (req.getParameter("reset") == null)
        {
            //log.entering(className, "doPost");
            String strNewHeader = req.getParameter("header");

            // set default success true
            req.setAttribute("success", "true");
            req.setAttribute("CompletedMessage","<div id=\"successbox\">Section header has been updated successfully.</div>");

            // validate header page
            if ("".equals(strNewHeader))
            {
                Properties p = PropertyManager.getPropertySet("sectionheader", "strings");

                req.setAttribute("success", "false");
                String errorMessage = "<ul>Error(s) : <li> Section Header : "+p.getProperty("headerblank", "This field is required.")+"</li></ul>";
                req.setAttribute("CompletedMessage","<div id=\"errorbox\">"+errorMessage+"</div>");
                editHeader(req, res);
            }

            MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();
            int userId = mui.getPerson().getUserId();
            int insertUserId = mivSession.get().getUser().getUserId();
            String strSectionId = req.getParameter("sectionid");
            String strAdditionalSectionId = req.getParameter("addheaderid");
            if (strSectionId != null && !("".equals(strSectionId))) {
                SectionHeaderUtil.updateUserSectionHeader(userId, new Integer(strSectionId).intValue(), strNewHeader, insertUserId);
            }
            else if (strAdditionalSectionId != null && !("".equals(strAdditionalSectionId))) {
                SectionHeaderUtil.updateAdditionalSectionHeader(new Integer(strAdditionalSectionId).intValue(), strNewHeader);
            }
        }

        req.setAttribute("referer", "sectionheader");
        String targetUrl = "/packet/design";
        dispatch(targetUrl,
                 new HttpServletRequestWrapper(req) {
                     @Override
                    public String getMethod()
                     {
                         return "GET";
                     }
                 },
                 res);
    }


    private void editHeader(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        String strSectionId = req.getParameter("sectionid");
        String strAdditionalSectionId = req.getParameter("addheaderid");
        String strHeader = req.getParameter("header");

        // Guard against XSS attack
        if (strSectionId != null) {strSectionId = MIVUtil.escapeMarkup(strSectionId);}
        if (strAdditionalSectionId != null) {strAdditionalSectionId = MIVUtil.escapeMarkup(strAdditionalSectionId);}
        if (strHeader != null) {strHeader = MIVUtil.escapeMarkup(strHeader);}

        // Treat edit Section Header as record type 'sectionheader'. This will make us reuse the properties
        // and will make it consitent with the framework.
        String recordType = "sectionheader";

        Map<String,Object> config = new HashMap<String,Object>();
        Properties p = PropertyManager.getPropertySet("sectionheader", "strings");
        config.put("strings", p);

        p = PropertyManager.getPropertySet(recordType, "config");
        config.put("config", p);

        p = PropertyManager.getPropertySet(recordType, "labels");
        config.put("labels", p);

        p = PropertyManager.getPropertySet(recordType, "tooltips");
        config.put("tooltips", p);

        config.put("options", MIVConfig.getConfig().getConstants());

        req.setAttribute("constants", config);

        String destination = "/jsp/editform.jsp";
        req.setAttribute("editorform", "headereditform.jsp");
        req.setAttribute("sectionid", strSectionId);
        req.setAttribute("addsectionid", strAdditionalSectionId);
        req.setAttribute("sectionheader", strHeader);
        /* To make use of existing data form edit web framework we are required to set
         * rec in the request. Since this is dummy rec type add an id of '0' to rec.
         */
        Map<String, String> record = new HashMap<String,String>();
        record.put("id", "0");
        req.setAttribute("rec", record);
        RequestDispatcher dispatcher = req.getRequestDispatcher(destination);

        logger.debug("dispatching to target. (Dispatcher: [{}]", dispatcher);
        try {
            res.setHeader("cache-control", "no-cache, must-revalidate");
            res.addHeader("Pragma", "no-cache");
            res.setDateHeader("Expires", System.currentTimeMillis()-2);
            dispatcher.forward(req, res);
        }
        catch (ServletException se) {
            //log.throwing(className, "doPost", se);
            throw(se);
        }
        catch (IOException ioe) {
            //log.throwing(className, "doPost", ioe);
            throw(ioe);
        }
        finally {
            logger.debug(" ***** Leaving SectionHeader.editHeader *****\t\tPOST\n");
        }
    }
}
