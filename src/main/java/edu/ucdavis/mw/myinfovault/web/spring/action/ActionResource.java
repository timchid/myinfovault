/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ActionResource.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.action;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;

/**
 * Represents an academic action as passed in to the Action API.
 *
 * @author japorito
 * @since 5.0
 */
@XmlRootElement(name="action")
public class ActionResource extends ResourceSupport
{
    public ActionResource() {
        return;
    }

    public Integer userId = -1;
    public Integer submitterId = -1;
    public Long dossierId = null;
    public Boolean isProposal = false;
    DossierActionType actionType = DossierActionType.MERIT;
    DossierDelegationAuthority delegationAuthority = DossierDelegationAuthority.REDELEGATED;
    Date effectiveDate;
    public Integer accelerationYears = 0;

    /**
     * @return the userId
     */
    public Integer getUserId()
    {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    /**
     * @return the submitterId
     */
    public Integer getSubmitterId()
    {
        return submitterId;
    }

    /**
     * @param submitterId the submitterId to set
     */
    public void setSubmitterId(Integer submitterId)
    {
        this.submitterId = submitterId;
    }

    /**
     * @return the actionType
     */
    public DossierActionType getActionType()
    {
        return actionType;
    }

    /**
     * @param actionType the actionType to set
     */
    public void setActionType(DossierActionType actionType)
    {
        this.actionType = actionType;
    }

    /**
     * @return the delegationAuthority
     */
    public DossierDelegationAuthority getDelegationAuthority()
    {
        return delegationAuthority;
    }

    /**
     * @param delegationAuthority the delegationAuthority to set
     */
    public void setDelegationAuthority(DossierDelegationAuthority delegationAuthority)
    {
        this.delegationAuthority = delegationAuthority;
    }

    /**
     * @return the effectiveDate
     */
    public Date getEffectiveDate()
    {
        return effectiveDate;
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate)
    {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the accelerationYears
     */
    public Integer getAccelerationYears()
    {
        return accelerationYears;
    }

    /**
     * @param accelerationYears the accelerationYears to set
     */
    public void setAccelerationYears(Integer accelerationYears)
    {
        this.accelerationYears = accelerationYears;
    }

    /**
     * @return the dossierId
     */
    @JsonInclude(value=Include.NON_NULL)
    public Long getDossierId()
    {
        return dossierId;
    }

    /**
     * @param dossierId the dossierId to set
     */
    public void setDossierId(Long dossierId)
    {
        this.dossierId = dossierId;
    }

    /**
     * @return the isProposal
     */
    public Boolean getIsProposal()
    {
        return isProposal;
    }

    /**
     * @param isProposal the isProposal to set
     */
    public void setIsProposal(Boolean isProposal)
    {
        this.isProposal = isProposal;
    }
}
