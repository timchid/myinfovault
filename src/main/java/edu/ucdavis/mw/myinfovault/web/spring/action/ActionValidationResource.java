/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ActionValidationResource.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.action;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.hateoas.ResourceSupport;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.ValidationErrorResource;

/**
 * Performs the validation for ActionResources, and represents the results.
 *
 * @author japorito
 * @since 5.0
 */
@XmlRootElement(name="ActionValidationError")
public class ActionValidationResource extends ValidationErrorResource
{
    private static final String MUST_SPECIFY = "{0} must be specified.";
    private static final String ACTION = "Action type";
    private static final String DELEGATION = "Delegation of authority";
    private static final String EFFECTIVE_DATE = "Effective date";
    private static final String USER = "The person for which the action is being started";
    private static final String USER_DOESNT_EXIST = "The user you are trying to start an action for doesn't exist.";
    private static final String SUBMIT_USER = "The person or entity who is starting the action";
    private static final String DELEGATION_AUTHORITY_INVALID = "The {0} action type cannot be {1}";
    private static final String INVALID_EFFECTIVE_DATE = "Effective date must be between {1,number,#} and {2,number,#}, inclusive";
    private static final String DUPLICATE_ACTION = "{0} already has a {1} action in process for {2,number,#}.";

    private static final int MAX_YEAR = Calendar.getInstance().get(Calendar.YEAR) + 2;
    private static final int MIN_YEAR = 1900;

    private static final DossierService dossierService = MivServiceLocator.getDossierService();
    private static final UserService userService = MivServiceLocator.getUserService();

    public ActionValidationResource(ActionResource action)
    {
        this.validate(action);
    }

    /**
     * Validate the action.
     *
     * @param actionType action type
     * @param delegationAuthority delegation of authority
     * @param errors form errors
     */
    @Override
    public Boolean validate(ResourceSupport resource)
    {
        ActionResource action = (ActionResource) resource;

        if (action.getActionType() == null)
        {
            this.addError(MessageFormat.format(MUST_SPECIFY, ACTION));
        }

        if (action.getDelegationAuthority() == null)
        {
            this.addError(MessageFormat.format(MUST_SPECIFY, DELEGATION));
        }

        if (!action.getActionType().isValid(action.getDelegationAuthority()))
        {
            this.addError(MessageFormat.format(DELEGATION_AUTHORITY_INVALID,
                                               action.getActionType().getDescription(),
                                               action.getDelegationAuthority().getDescription()));
        }

        if (action.getUserId() == null || action.getUserId() == -1)
        {
            this.addError(MessageFormat.format(MUST_SPECIFY, USER));
        }
        else
        {
            MivPerson actionUser = userService.getPersonByMivId(action.getUserId());

            if (actionUser == null)
            {
                this.addError(USER_DOESNT_EXIST);
            }
            else
            {
                try
                {
                    for (Dossier dossier : dossierService.getDossiersInProcess(actionUser))
                    {
                        AcademicAction dossierAction = dossier.getAction();

                        //Get the dossier's effective calendar year
                        Calendar dossierYear = Calendar.getInstance();
                        dossierYear.setTime(dossierAction.getEffectiveDate());

                        //Get the action's effective calendar year
                        Calendar actionYear = Calendar.getInstance();
                        actionYear.setTime(action.getEffectiveDate());

                        //Cannot start an action if there is an action of the same type
                        //for the same year already in process.
                        if (dossierAction.getActionType() == action.getActionType() &&
                            dossierYear.get(Calendar.YEAR) == actionYear.get(Calendar.YEAR))
                        {
                            this.addError(MessageFormat.format(DUPLICATE_ACTION,
                                                               actionUser.getDisplayName(),
                                                               action.getActionType().getDescription(),
                                                               actionYear.get(Calendar.YEAR)));
                        }
                    }
                }
                catch (WorkflowException e)
                {
                    this.addError("Dossier could not be routed. " +
                                  "Contact <a href=\"mailto:miv-help@ucdavis.edu?" +
                                  "subject=Error Starting Action&" +
                                  "body=There was an error starting a " + action.getActionType().getDescription() +
                                  " action " + (action.getUserId() != null && action.getUserId() != -1 ?
                                              ("for " + userService.getPersonByMivId(action.getUserId()).getDisplayName()) : "") +
                                  " on " + new Timestamp((new Date()).getTime()) +
                                  ".\">MIV help</a> for assistance.");
                    logger.error("Problem starting action for user with id: " + action.getUserId(), e);
                }
            }
        }

        if (action.getSubmitterId() == null)
        {
            this.addError(MessageFormat.format(MUST_SPECIFY, SUBMIT_USER));
        }

        if (action.getEffectiveDate() != null)
        {
            Calendar effectiveDate = Calendar.getInstance();
            effectiveDate.setTime(DateUtils.truncate(action.getEffectiveDate(), Calendar.DATE));

            int effectiveYear = effectiveDate.get(Calendar.YEAR);

            /*
             * Check to make sure the effective year entered is
             * between the minimum year and next year, inclusive.
             */
            if (effectiveYear < MIN_YEAR || effectiveYear > MAX_YEAR)
            {
                this.addError(MessageFormat.format(INVALID_EFFECTIVE_DATE, MIN_YEAR, MAX_YEAR));
            }
        }
        else
        {
            this.addError(MessageFormat.format(MUST_SPECIFY, EFFECTIVE_DATE));
        }

        this.valid = this.errors.isEmpty();

        return this.valid;
    }
}
