/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ArchiveDossiersAction.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.archivedossiers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.events2.DossierArchiveEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.archive.DossierArchiveService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.spring.search.MivActionList;
import edu.ucdavis.mw.myinfovault.web.spring.search.OpenAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Spring WebFlow form action for archiving dossiers.
 *
 * @author Rick Hendricks
 * @since MIV 3.1
 */
public class ArchiveDossiersAction extends OpenAction
{
    private DossierArchiveService dossierArchiveService;

    /**
     * Creates the form action bean for archiving dossiers.
     *
     * @param searchStrategy Spring-injected search strategy
     * @param authorizationService Spring-injected authorization service
     * @param userService Spring-injected user service
     * @param dossierService Spring-injected dossier service
     * @param dossierArchiveService Spring-injected dossier archive service
     */
    public ArchiveDossiersAction(SearchStrategy searchStrategy,
                                 AuthorizationService authorizationService,
                                 UserService userService,
                                 DossierService dossierService,
                                 DossierArchiveService dossierArchiveService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService);

        this.dossierArchiveService = dossierArchiveService;
    }


    /**
     * Checks if user is authorized to archive dossiers. Only
     * roles VICE_PROVOST_STAFF is allowed to archive dossiers.
     *
     * @param context Request context from webflow
     * @return WebFlow event status, allowed or denied
     */
    public Event isAuthorized(RequestContext context)
    {
        MivPerson currentPerson = getShadowPerson(context);

        if (currentPerson.hasRole(MivRole.VICE_PROVOST_STAFF))
        {
            logger.info("(Old log) User "+currentPerson.getDisplayName()+" ("+currentPerson.getPersonId()+") is AUTHORIZED to archive dossiers.");
            logger.info("(New log) User {} is AUTHORIZED to archive dossiers.", currentPerson.getLoggingName());
            return allowed();
        }

        logger.info("User "+currentPerson.getDisplayName()+" ("+currentPerson.getPersonId()+") is ** NOT ** AUTHORIZED to archive dossiers.");
        return denied();
    }


    /**
     * Checks the EDMS processing status. If an error message is returned,
     * new EDMS files cannot be added at this time.
     *
     * @param context Request context from webflow
     * @return WebFlow event status, success or error
     */
    public Event validateEdmsProcessingState(RequestContext context)
    {
        MutableAttributeMap<Object> flashScope = context.getFlashScope();

        String errorMsg = dossierArchiveService.validateEdmsProcessingState();
        if (errorMsg != null)
        {
            flashScope.put("edmsInProcess", errorMsg);
            return error();
        }
        return success();
    }


    /**
     * Get the Dossiers that this user is allowed to view.
     *
     * @param user
     * @return dossiers in an action list
     */
    public List<MivActionList> getDossiers(MIVUser user)
    {
        return this.searchStrategy.getDossiersAtLocation(user, DossierLocation.POSTAUDITREVIEW);
    }


    /**
     * Get the Dossiers to be archived.
     *
     * @param context RequestContext
     * @return WebFlow event status
     */
    public Event getDossiersToArchive(RequestContext context)
    {
        ArchiveDossiersForm archiveDossiersForm = (ArchiveDossiersForm) this.getFormObject(context);
        Map<String, String> dossiersToArchiveMap = new HashMap<String, String>();

        List<String> dossiers = archiveDossiersForm.getToArchive();

        // Iterate through the dossiers
        for (String dossierId : dossiers)
        {
            String fullName = null;
            try
            {
                Dossier dossier = this.dossierService.getDossier(dossierId);
                MivPerson mivPerson = dossier.getAction().getCandidate();
                String actionType = dossier.getAction().getActionType().getDescription();
                String delegationAuthority = dossier.getAction().getDelegationAuthority().getDescription(dossier.getReviewType());

                String schoolName = mivPerson.getPrimaryAppointment().getScope().getSchoolDescription();
                String departmentName = mivPerson.getPrimaryAppointment().getScope().getDepartmentDescription();
                String schoolDepartment = schoolName+(StringUtils.isBlank(departmentName) ? "" : " - "+departmentName);
                fullName = (mivPerson.getSurname() + ", " + mivPerson.getGivenName() + " " + mivPerson.getMiddleName()).trim();
                dossiersToArchiveMap.put(fullName, schoolDepartment+"; <strong>"+delegationAuthority+" - "+actionType+"</strong>");
            }
            catch (WorkflowException wfe)
            {
                logger.error("Unable to retrieve dossier " + dossierId + " to archive for " + fullName, wfe);
            }
        }

        // Sort the list of dossiers to archive into alphabetic order by last name
        context.getFlashScope().put("dossiersToArchiveMap", new TreeMap<String, String>(dossiersToArchiveMap));

        return success();
    }


    /**
     * Archive dossiers.
     *
     * @param context Webflow request context
     * @return WebFlow event status, success or error
     */
    public Event archiveDossiers(RequestContext context)
    {
        MivPerson shadowPerson = getShadowPerson(context);
        MivPerson realPerson = this.getRealPerson(context);

        MutableAttributeMap<Object> flashScope = context.getFlashScope();

        ArchiveDossiersForm archiveDossiersForm = (ArchiveDossiersForm) this.getFormObject(context);
        Map<String, String> archivedDossiersMap = new HashMap<String, String>();

        List<String> dossiers = archiveDossiersForm.getToArchive();

        logger.info("_AUDIT : User {}{} has requested archiving for Dossiers: {}",
                        new Object[] {
                            realPerson.getUserId(),
                            (shadowPerson.getUserId() != realPerson.getUserId() ?
                                    ", acting as " + shadowPerson.getUserId() + "," : ""),
                            dossiers.toString()
                        }
                    );

        // Iterate through the dossiers
        for (String dossierId : dossiers)
        {
            String fullName = null;
            try
            {
                Dossier dossier = this.dossierService.getDossier(dossierId);
                MivPerson mivPerson = dossier.getAction().getCandidate();
                String actionType = dossier.getAction().getActionType().getDescription();
                String delegationAuthority = dossier.getAction().getDelegationAuthority().getDescription(dossier.getReviewType());

                String schoolName = mivPerson.getPrimaryAppointment().getScope().getSchoolDescription();
                String departmentName = mivPerson.getPrimaryAppointment().getScope().getDepartmentDescription();
                String schoolDepartment = schoolName + (StringUtils.isBlank(departmentName) ? "" : " - " + departmentName);
                fullName = mivPerson.getSurname() + ", " + mivPerson.getGivenName() +
                           (StringUtils.isNotBlank(mivPerson.getMiddleName()) ? " " + mivPerson.getMiddleName() : "");

                String resultMessage =
                    schoolDepartment + "; <strong>" + delegationAuthority + " - " + actionType + ":</strong>"
                    + " <span class=\"success\"><strong>Success</strong> - Dossier has been sent to the EDMS Archive.</span>";

                // Set the routing principal for this dossier
                dossier.setRoutingPerson(shadowPerson);

                // Archive the dossier
                String errorMessage = dossierArchiveService.archiveDossier(dossier);

                // Check for an error message returned
                if (errorMessage != null)
                {
                    resultMessage = "<span class=\"failed\">"+"<strong>Error</strong> - "+errorMessage+"</span>";
                }
                archivedDossiersMap.put(fullName, resultMessage);

                EventDispatcher2.getDispatcher().post( new DossierArchiveEvent(
                        realPerson,
                        dossier,
                        dossierService.getCurrentWorkflowNode(dossier).getNodeLocation()).setShadowPerson(shadowPerson).setComments(errorMessage));


            }
            catch (WorkflowException wfe)
            {
                String msg = "Workflow exception retrieving dossier " + dossierId + " to archive.";
                archivedDossiersMap.put(fullName, msg);
                logger.error(msg, wfe);
            }
        }

        // Clear the dossiersToArchive from the form
        archiveDossiersForm.clearToArchive();

        // Sort the list of dossiers archived into alphabetic order by last name
        flashScope.put("archivedDossiersMap", new TreeMap<String, String>(archivedDossiersMap));

        return success();
    }
}
