/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignmentAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupComparator;
import edu.ucdavis.mw.myinfovault.domain.group.GroupType;
import edu.ucdavis.mw.myinfovault.domain.group.MemberComparator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.group.GroupFilter;
import edu.ucdavis.mw.myinfovault.service.group.GroupMemberFilter;
import edu.ucdavis.mw.myinfovault.service.group.GroupSearchFilter;
import edu.ucdavis.mw.myinfovault.service.group.GroupService;
import edu.ucdavis.mw.myinfovault.service.group.GroupTypeFilter;
import edu.ucdavis.mw.myinfovault.service.group.GroupViewFilter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.assignreviewers.AssignReviewersAction;
import edu.ucdavis.mw.myinfovault.web.spring.group.GroupAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.DossierAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchMethod;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Supports {@link GroupAction} and {@link AssignReviewersAction}.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public abstract class AssignmentAction extends DossierAction
{
    //scope parameter keys
    private static final String INACTIVE_PARAM = "inactive";
    private static final String ASSIGNED_PARAM = "assigned";
    private static final String AVAILABLE_PARAM = "available";
    private final static String CANSWITCH_PARAM = "canSwitch";
    protected final static String ISEDIT_PARAM = "isEdit";

    /**
     * Spring-injected group service.
     */
    protected GroupService groupService;

    /**
     * Create the assignment webflow form action.
     *
     * @param searchStrategy implementation of SearchStrategy from super
     * @param authorizationService authorization service from super
     * @param userService user service from super
     * @param dossierService dossier service from super
     * @param groupService group service from super
     */
    protected AssignmentAction(SearchStrategy searchStrategy,
                               AuthorizationService authorizationService,
                               UserService userService,
                               DossierService dossierService,
                               GroupService groupService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService);

        this.groupService = groupService;
    }

    /**
     * Checks if user is authorized for this assignment.
     *
     * @param context Request context from webflow
     * @return Webflow event status
     */
    public Event isAuthorized(RequestContext context)
    {
        //is this flow an assign review view or edit?
        Boolean isEdit = context.getFlowScope().getBoolean(ISEDIT_PARAM);

        //if DNE, check general permission for assigning reviewers
        if (isEdit == null)
        {
            return canAccess(context)
                 ? allowed()
                 : denied();
        }
        //allowed to act on specific dossier?
        else
        {
            boolean canEdit = canEdit(context);

            //persist in the flow if the acting user can switch to edit mode
            context.getFlowScope().put(CANSWITCH_PARAM,
                                       canEdit//authorized to edit the group
                                    && context.getFlowScope().getBoolean(CANSWITCH_PARAM));//flow allows switching

            return canEdit || !isEdit && canView(context)
                 ? allowed()
                 : denied();
        }
    }

    /**
     * Is the given acting person authorized to act on the given
     * group with the given permission.
     *
     * @param actingPerson MIV person in question
     * @param group MIV group in question
     * @param permission View or edit permission in question
     * @return <code>true</code> if authorized, <code>false</code> otherwise
     */
    protected boolean isGroupAuthorized(MivPerson actingPerson,
                                        Group group,
                                        String permission)
    {
        //put group ID in permission details
        AttributeSet permissionDetails = new AttributeSet();
        permissionDetails.put(PermissionDetail.TARGET_ID,
                              Integer.toString(group.getId()));

        //is the user allowed to edit this group?
        return authorizationService.isAuthorized(actingPerson,
                                                 permission,
                                                 permissionDetails,
                                                 null);
    }

    /**
     * Return if acting user has general access to this flow.
     *
     * @param context Webflow request context
     * @return If acting user allowed to access this flow
     */
    protected abstract boolean canAccess(RequestContext context);

    /**
     * Return if acting user is allowed to view this assignment.
     *
     * @param context Webflow request context
     * @return If acting user allowed to view this assignment
     */
    protected abstract boolean canView(RequestContext context);

    /**
     * Return if acting user is allowed to edit this assignment.
     *
     * @param context Webflow request context
     * @return If acting user allowed to edit this assignment
     */
    protected abstract boolean canEdit(RequestContext context);

    /**
     * Add the bread crumb appropriate for this sub-flow.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public abstract Event addBreadcrumb(RequestContext context);

    /**
     * Put the available people and/or groups into the request.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event available(RequestContext context)
    {
        context.getRequestScope().put(AVAILABLE_PARAM, getAvailable(context));

        return success();
    }

    /**
     * Put the assigned/inactive people and/or groups into the request. These
     * collections will also be in the form object, but due to a quirk
     * in the Sojo JSON writer, elements with paths longer than four
     * nodes are not written properly. Therefore, it is necessary to
     * move the assigned and inactive people collections trees up a node.
     *
     * That is, the JSON elements schoolName is inaccessible via the OGNL path:
     *      {@code form.assigned[0].primaryAppointment.schoolName}
     * but accessible via:
     *      {@code assigned[0].primaryAppointment.schoolName}
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event assigned(RequestContext context)
    {
        //get form object
        AssignmentForm form = getFormObject(context);

        //conceal memberships of confidential groups
        concealMembership(getShadowPerson(context),
                          form.getAssignedGroups());

        //combine into one list all assigned groups and people
        List<Object> assigned = new ArrayList<Object>();
        assigned.addAll(form.getAssignedGroups());
        assigned.addAll(form.getAssignedPeople());

        context.getRequestScope().put(ASSIGNED_PARAM,
                                      assigned);

        context.getRequestScope().put(INACTIVE_PARAM,
                                      form.getInactivePeople());

        return success();
    }

    /**
     * Gets the available people and/or groups in a single object array list.
     *
     * @param context Webflow request context
     * @return List of people and/or groups available for this request
     */
    protected abstract List<Object> getAvailable(RequestContext context);

    /**
     * Get available people for the search term bound to the form object.
     *
     * @param context Webflow request context
     * @return List of MIV person objects corresponding to the search term
     */
    protected SortedSet<MivPerson> getAvailablePeople(RequestContext context)
    {
        //get form object
        AssignmentForm form = getFormObject(context);

        //search results an initially empty sorted set
        SortedSet<MivPerson> results = new TreeSet<MivPerson>(new MemberComparator());

        results.addAll(getUsersByName(getUser(context), form));
        // NOTE: AssignReviewersSearchStrategy always looks for "all"
        results.removeAll(form.getAssignedPeople());
        form.setAvailablePeople(results);
        //proceed if a search method has been chosen
        /*
         * if (form.getMethod() != null) { switch(form.getMethod()) { case
         * SEARCHNAME: // fall to case below if input name supplied
         * if(!StringUtils.hasText(form.getInputName())) break; case
         * SEARCHLNAME: results.addAll(getUsersByName(getUser(context), form));
         * break; case SEARCHDEPT:
         * results.addAll(getUsersByDepartment(getUser(context), form)); break;
         * case SEARCHSCHOOL: results.addAll(getUsersBySchool(getUser(context),
         * form)); break; // no results for any other search method default:
         * break; }
         *
         * //remove all assigned users
         * results.removeAll(form.getAssignedPeople()); }
         */
        //return the available people to assign
        return results;
    }

    /**
     * Get available group for the search term bound to the form object.
     *
     * @param context Webflow request context
     * @return List of MIV group objects corresponding to the search term
     */
    @SuppressWarnings("unchecked")
    protected SortedSet<Group> getAvailableGroups(RequestContext context)
    {
        //get form object
        AssignmentForm form = getFormObject(context);

        //search results initially empty sorted set
        SortedSet<Group> results = new TreeSet<Group>(new GroupComparator());

        //if search by name or no search
        if (form.getMethod() == SearchMethod.SEARCHNAME)
        {
            //the switched-to, acting person
            MivPerson actingPerson = getShadowPerson(context);

            try
            {
                /*
                 * Add the available groups to assign.
                 *
                 * Filter out groups:
                 * - with names not containing the search term (if the search term is not empty)
                 * - whose membership is a subset of the currently assigned people
                 * - not visible to the current user
                 * - not of the default type
                 */
                results.addAll(groupService.getGroups(new GroupTypeFilter(GroupType.GROUP),
                                                      new GroupViewFilter(actingPerson),
                                                      new GroupSearchFilter(form.getInputName()),
                                                      new GroupMemberFilter(form.getAssignedPeople()),
                                                      new GroupFilter(form.getAssignedGroups())));

                //conceal memberships of confidential groups
                concealMembership(actingPerson, results);
            }
            catch(IllegalArgumentException e)
            {
                throw new MivSevereApplicationError("Bad group filter; unable to retrieve group results", e);
            }
        }

        form.setAvailableGroups(results);
        //return the available groups to assign
        return results;
    }

    /**
     * Save the updated assignment.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public abstract Event save(RequestContext context);

    /**
     * Update the assignment from bulk arrays in form.
     *
     * {@inheritDoc}
     */
    @Override
    public Event bind(RequestContext context) throws Exception
    {
        Event e = super.bind(context);

        //get form object
        AssignmentForm form = getFormObject(context);

        //If the remove all flag is set, remove all assigned people/groups
        if (form.isRemoveAll())
        {
            form.getAssignedPeople().clear();
            form.getAssignedGroups().clear();
        }
        //otherwise, add/remove selected people to/from the assigned
        else
        {
            //the switched-to, acting person
            MivPerson actingPerson = getShadowPerson(context);

            //add/remove selected people/groups to/from the assigned
            form.getAssignedPeople().addAll(userService.getPeopleByMivId(form.getAddPeople()));

            Iterator<MivPerson>assignedIt = form.getAssignedPeople().iterator();
            Set<MivPerson> removedPeople = userService.getPeopleByMivId(form.getRemovePeople());
            while (assignedIt.hasNext())
            {
                MivPerson assignedPerson = assignedIt.next();
                if (removedPeople.contains(userService.getPersonByMivId(assignedPerson.getUserId())))
                {
                    assignedIt.remove();
                }
            }

            form.getAssignedGroups().addAll(groupService.getGroups(form.getAddGroups()));
            form.getAssignedGroups().removeAll(groupService.getGroups(form.getRemoveGroups()));

            //update group members active statuses
            updateGroupMembers(actingPerson,
                               form.getActivateGroups(),
                               form.getAssignedGroups(),
                               true);
            updateGroupMembers(actingPerson,
                               form.getDeactivateGroups(),
                               form.getAssignedGroups(),
                               false);
        }

        return e;
    }

    /*
     * Update the respective inactive sets of the assigned groups for the given
     * array of strings in the form "<GROUPID>:<USERID>" corresponding
     * to an assigned group and one of its members
     */
    private void updateGroupMembers(MivPerson actingPerson,
                                    String[] groupsToMembers,
                                    Set<Group> assignedGroups,
                                    boolean isActivation)
    {
        Map<Integer, Set<MivPerson>> parsedGroupsToMembers = parseGroupsToMembers(groupsToMembers);

        for (Group assignedGroup : assignedGroups)
        {
            //get the members to be updated for this assigned group
            Set<MivPerson> members = parsedGroupsToMembers.get(assignedGroup.getId());

            //if there are members to update  for this assigned group
            if (members != null)
            {
                //remove members from the inactive set for activations
                if (isActivation)
                {
                    Iterator<MivPerson> inactiveIt = assignedGroup.getInactive().iterator();
                    while(inactiveIt.hasNext())
                    {
                        MivPerson inactive = inactiveIt.next();
                        for (MivPerson member : members)
                        {
                            if (inactive.getUserId() == member.getUserId())
                            {
                                inactiveIt.remove();
                            }
                        }
                    }
                }
                //add members to the inactive set for deactivations
                else
                {
                    Set<Integer> inactiveIds = assignedGroup.getInactiveMap().keySet();
                    for (MivPerson member : members)
                    {
                        if (!inactiveIds.contains(member.getUserId()))
                        {
                            assignedGroup.getInactive().add(member);
                        }
                    }
                }
            }
        }
    }

    //parse the array of groupUser strings into a map of group IDs to sets of group members
    private Map<Integer, Set<MivPerson>> parseGroupsToMembers(String[] groupUsers)
    {
        //map of group IDs to set of group members
        Map<Integer, Set<MivPerson>> parsed = new HashMap<Integer, Set<MivPerson>>();

        //for each groupUser string
        for (String groupUser : groupUsers)
        {
            //split each entry string into group ID and user ID
            String[] splitEntry = groupUser.split(":");

            //continue IFF there are two parts
            if (splitEntry.length == 2)
            {
                try
                {
                    //parse the group ID
                    int groupId = Integer.parseInt(splitEntry[0]);

                    //get the member set mapped to the group ID
                    Set<MivPerson> members = parsed.get(groupId);

                    //create the set and map it to the group ID if it doesn't exist
                    if (members == null)
                    {
                        members = new HashSet<MivPerson>();

                        parsed.put(groupId,
                                   members);
                    }

                    //parse the user ID, get the associated MIV person, and add to the members
                    members.add(userService.getPersonByMivId(Integer.parseInt(splitEntry[1])));
                }
                catch(NumberFormatException e)
                {
                    //log the event and continue
                    logger.warn("Bad parse for '" + splitEntry[0] + "' or '" + splitEntry[1] + "'", e);
                }
            }
        }

        return parsed;
    }

    //removes group membership from groups that the acting user may not view
    private void concealMembership(MivPerson actingPerson,
                                    Set<Group> groups)
    {
        //check if acting user is authorized to view each assigned group
        for (Group group : groups)
        {
            //if acting person is not authorized to view the assigned group's membership
            if (!isGroupAuthorized(actingPerson,
                                   group,
                                   Permission.VIEW_MEMBERSHIP))
            {
                //clear out these confidential group members
                group.getMembers().clear();
                group.getInactive().clear();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AssignmentForm getFormObject(RequestContext context)
    {
        return (AssignmentForm) super.getFormObject(context);
    }
}
