/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchDisplayOptionsController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.mapping.biosketch.BiosketchMapper;
import edu.ucdavis.mw.myinfovault.service.biosketch.BiosketchService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;


/**
 * @author dreddy
 * @since MIV 2.1
 *
 * Updated: Pradeep on 08/03/2012
 */
@Controller
@RequestMapping("/biosketch/BiosketchDisplayOptions")
@SessionAttributes("biosketchCommand")
public class BiosketchDisplayOptionsController extends BiosketchFormController
{

    public BiosketchService biosketchService;

    @Autowired
    public BiosketchDisplayOptionsController(BiosketchService biosketchService)
    {
         this.biosketchService = biosketchService;
    }
    @InitBinder
    protected void initBinder(WebDataBinder binder)
    {
        binder.setValidator(new BiosketchCommandValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView setup(HttpServletRequest request) throws Exception
    {
        logger.trace(" ***** BiosketchDisplayOptionsController : handleRequestInternal ***** ");

        String biosketchID = (String) request.getSession().getAttribute("biosketchID");
        String newRecord = (String) request.getSession().getAttribute("newrecord");
        if (biosketchID == null && newRecord == null)
        {
            return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/BiosketchPreview"));
        }

        ModelMap model = new ModelMap();
        logger.trace(" ***** BiosketchDisplayOptionsController : referenceData ***** ");

        Map<String,Object> config = new HashMap<String,Object>();
        String showwizard = (String) request.getSession().getAttribute("showwizard");
        BiosketchType biosketchType = (BiosketchType)request.getSession().getAttribute("biosketchType");

        Properties p = PropertyManager.getPropertySet(biosketchType.getCode(), "config");
        String editorForm = p.getProperty("editorpage");
        p = PropertyManager.getPropertySet(biosketchType.getCode(), "labels");
        config.put("labels", p);
        p = PropertyManager.getPropertySet(biosketchType.getCode(), "tooltips");
        config.put("tooltips", p);
        Properties pstr = PropertyManager.getPropertySet(biosketchType.getCode(), "strings");
        config.put("strings", pstr);
        request.setAttribute("editorform", editorForm);
        request.setAttribute("constants", config);
        model.addAttribute("newrecord", newRecord);
        model.addAttribute("showwizard", showwizard);
        logger.trace(" ***** BiosketchDisplayOptionsController : formBackingObject ***** ");
        MivPerson mui = MIVSession.getSession(request).getUser().getTargetUserInfo().getPerson();
        int userID = mui.getUserId();

        Biosketch biosketch = null;
        BiosketchMapper biosketchMapper = new BiosketchMapper();
        BiosketchCommand biosketchCommand =  new BiosketchCommand();


        if (biosketchID != null && Integer.parseInt(biosketchID) != 0)
        {
            biosketch = biosketchService.getBiosketch(Integer.parseInt(biosketchID));
            biosketchCommand = biosketchMapper.mapBiosketchToBiosketchCommand(biosketch, biosketchCommand);
        }
        else if (biosketchID == null)
        {
            biosketch = biosketchService.createNewBiosketch(biosketchType,userID);//new Biosketch(userID,3);//Integer.parseInt(biosketchTypeID));
            biosketchCommand = biosketchMapper.mapBiosketchToBiosketchCommand(biosketch, biosketchCommand);
            String displayName = mui.getDisplayName();//mivSession.get().getUser().getTargetUserInfo().getDisplayName();
            biosketchCommand.setFullName(displayName);
            biosketchCommand.setProgramDirectorName(mui.getSurname()+", "+mui.getGivenName()+(StringUtils.isBlank(mui.getMiddleName()) ? "" : ", "+mui.getMiddleName()));

            String title = pstr.getProperty("defaulttitle");
            biosketchCommand.setTitle(title);
        }

        model.addAttribute("biosketchCommand", biosketchCommand);
        return new ModelAndView("biosketchdisplayoptions", model);
    }



    @RequestMapping(method = RequestMethod.POST)
    protected ModelAndView processFormSubmission(HttpServletRequest request,
            @Valid @ModelAttribute("biosketchCommand") BiosketchCommand biosketchCommand)
    // BindException result)
    {
        /*
         * if (result.hasErrors()) { return new
         * ModelAndView("biosketchdisplayoptions"); }
         */
        logger.trace(" ***** entering BiosketchDisplayOptionsController : onSubmit ***** ");

        Biosketch biosketch = null;
        BiosketchMapper biosketchMapper = new BiosketchMapper();

        String biosketchID = (String)request.getSession().getAttribute("biosketchID");
        BiosketchType biosketchType = (BiosketchType)request.getSession().getAttribute("biosketchType");

        String newRecord = (String) request.getSession().getAttribute("newrecord");

        if ("true".equals(newRecord) && biosketchID == null) //          inserting a new record
        {
            MIVUserInfo mui = MIVSession.getSession(request).getUser().getTargetUserInfo();
            int userID = mui.getPerson().getUserId();
            biosketch = biosketchService.createNewBiosketch(biosketchType,userID);
            biosketch = biosketchMapper.mapBiosketchCommandToBiosketch(biosketchCommand, biosketch);
            int biosketchId = biosketchService.saveBiosketch(biosketch).getId();
            //request.getSession().setAttribute("newdatarecord", "true");
            request.getSession().setAttribute("biosketchID", biosketchId+"");
            return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/SelectBiosketchData"));
        }
        else // means updating the record
        {
            biosketch = biosketchService.getBiosketch(Integer.parseInt(biosketchID));
            biosketch = biosketchMapper.mapBiosketchCommandToBiosketch(biosketchCommand, biosketch);
            biosketchService.saveBiosketch(biosketch);
            //request.getSession().setAttribute("newdatarecord","false");
            if ("true".equals(newRecord))
            {
                return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/SelectBiosketchData"));
            }
            else
            {
                return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/BiosketchMenu"));
            }
        }
    }

    @ExceptionHandler
    public ModelAndView handleException(BindException result)
    {
        return new ModelAndView("biosketchdisplayoptions");
    }

}
