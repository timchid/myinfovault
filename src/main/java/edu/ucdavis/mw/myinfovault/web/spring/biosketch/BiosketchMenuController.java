/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchMenuController.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.service.biosketch.BiosketchService;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.format.QuoteCleaner;

@Controller
@RequestMapping("/biosketch/BiosketchMenu")
public class BiosketchMenuController extends BiosketchFormController
{
    public BiosketchService biosketchService;

    @Autowired
    public BiosketchMenuController(BiosketchService biosketchService)
    {
        this.biosketchService = biosketchService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getMenu(HttpServletRequest req)
    {
        String biosketchID = (String)req.getSession().getAttribute("biosketchID");
        //String biosketchType = (String)request.getSession().getAttribute("biosketchType");
        if(biosketchID ==  null)
        {
            //request.setAttribute("biosketchType", biosketchType);
            return new ModelAndView(new RedirectView(req.getContextPath() + "/biosketch/BiosketchPreview"));
        }


        ModelMap uiModel = new ModelMap();
        req.getSession().removeAttribute("newrecord");
        req.getSession().removeAttribute("showwizard");

        if (biosketchID != null)
        {
            BiosketchType biosketchType = (BiosketchType) req.getSession().getAttribute("biosketchType");
            Properties pstr = PropertyManager.getPropertySet(biosketchType.getCode(), "strings");
            Map<String, Object> config = new HashMap<String, Object>();
            config.put("strings", pstr);
            req.setAttribute("constants", config);
            QuoteCleaner quoteCleaner = new QuoteCleaner();

            Biosketch biosketch = biosketchService.getBiosketch(Integer.parseInt(biosketchID));
            uiModel.addAttribute("biosketchID", biosketchID);
            String cleanedName = quoteCleaner.format(MIVUtil.escapeMarkup(biosketch.getName()));
            uiModel.addAttribute("biosketchname", cleanedName);
        }

        return new ModelAndView("biosketchmenu", uiModel);
    }
}
