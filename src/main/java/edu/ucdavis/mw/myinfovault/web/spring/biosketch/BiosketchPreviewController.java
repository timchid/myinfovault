/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchPreviewController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.service.biosketch.BiosketchService;
import edu.ucdavis.mw.myinfovault.util.HttpServletUtil;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.MIVUtil;

/**
 * @author dreddy
 * @since MIV 2.1
 *
 * Updated: Pradeep on 08/03/2012
 */
@Controller
@RequestMapping("/biosketch/BiosketchPreview")
public class BiosketchPreviewController extends BiosketchFormController
{
    private BiosketchService biosketchService;

    @Autowired
    public BiosketchPreviewController(BiosketchService biosketchService)
    {
        this.biosketchService = biosketchService;
    }

    /**
     * @see org.springframework.web.servlet.mvc.SimpleFormController#onSubmit(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse, java.lang.Object,
     *      org.springframework.validation.BindException)
     */
    @RequestMapping(method = RequestMethod.POST)
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response)
    {
        logger.trace(" ***** BiosketchPreviewController : onSubmit ***** ");

        List<Biosketch> biosketchList = null;
        String key = findKey(request);
        String keyval = "";
        String deleteMsg = "";

        BiosketchType biosketchType = (BiosketchType)request.getSession().getAttribute("biosketchType");
        Properties pstr = PropertyManager.getPropertySet(biosketchType.getCode(), "strings");
        Map<String,Object> config = new HashMap<String,Object>();
        config.put("strings", pstr);
        request.setAttribute("constants", config);

        if (key != null && key.length() > 0)
        {
            keyval = key.replaceFirst("[a-zA-Z]*", "");
        }
        try { // added try..finally block to guarantee log output when exiting method
            if (key.startsWith("DE")) // executed when delete button is clicked.
            {
                Biosketch biosketch = biosketchService.getBiosketch(Integer.parseInt(keyval));
                biosketchService.deleteBiosketch(biosketch);

                try
                {
                    deleteMsg = biosketchType.getTitle() + " document has been deleted.";

                    if(HttpServletUtil.isAjaxRequest(request))
                    {
                        ServletOutputStream out = null;
                        out = response.getOutputStream();
                        response.setContentType("text/html");
                        // out.print("Record deleted from  Biosketch");
                        out.print(deleteMsg);
                        // The response has already been written, don't return ModelAndView
                        return null;
                    }else
                    {
                        String redirectUrl = request.getContextPath() + "/biosketch/BiosketchPreview?biosketchtypeid="+biosketchType.getTypeId();
                        redirectUrl += "&deletesuccess=true";
                        return new ModelAndView(new RedirectView(redirectUrl));
                    }
                }
                catch (IOException e)
                {
                    logger.warn("Unable to write record-delete response", e);
                    return new ModelAndView("biosketchpreview", "biosketchList", biosketchList);
                }
            }
            else if (key.startsWith("DU")) // means duplicate this document is clicked.
            {
                int biosketchId = Integer.parseInt(keyval);
                int newBiosketchId = 0;

                // Allow duplication only if the current user is the owner of the biosketch.
                Biosketch b = biosketchService.getBiosketch(biosketchId);
                int biosketchOwner = b.getUserID();
                int currentUser = MIVSession.getSession(request).getUser().getTargetUserInfo().getPerson().getUserId();

                if (currentUser == biosketchOwner)
                {
                    newBiosketchId = biosketchService.duplicateBiosketch(biosketchId);
                }
                else
                {
                    // report that a user tried to duplicate a biosketch that doesn't belong to them
                    logger.warn("User {} tried to duplicate a biosketch belonging to user {}", currentUser, biosketchOwner);
                }

                // Report the new biosketch number if a duplicate was made.
                if (newBiosketchId != 0)
                {
                    String redirectUrl = request.getContextPath() + "/biosketch/BiosketchPreview?biosketchtypeid="+biosketchType.getTypeId();
                    redirectUrl += "&biosketchID="+String.valueOf(newBiosketchId);
                    redirectUrl += "&duplicatesuccess=true";
                    return new ModelAndView(new RedirectView(redirectUrl));
                }else
                {
                    return new ModelAndView("biosketchpreview", "biosketchList", biosketchList);
                }
            }
            else if (key.startsWith("E"))
            {
                request.getSession().setAttribute("newrecord", "false");
                request.getSession().setAttribute("biosketchID", keyval);
                return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/BiosketchMenu"));
            }
            else if (key.startsWith("C"))
            {
                request.getSession().setAttribute("biosketchID", keyval);
                request.getSession().setAttribute("newcreaterecord", "false");
                return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/CreateMyBiosktech"));
            }
            else// if (key.startsWith("A"))
            {
                /* there are two ways to redirect request to another controller
                 * In the xml -- <property name="successView" value="redirect:biosketchdisplayoptions.htm"></property>
                 * In the controller class -- new RedirectView("biosketchdisplayoptions.htm"));
                 */
                request.getSession().setAttribute("newrecord", "true");
                request.getSession().setAttribute("showwizard", "true");
                return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/BiosketchDisplayOptions"));
            }
        }
        finally {
            logger.trace("Exiting BiosketchPreviewController.onSubmit");
        }
    }


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView referenceData(HttpServletRequest req)
    {
        logger.trace(" ***** BiosketchPreviewController : referenceData ***** ");

        ModelMap uiModel = new ModelMap();
        MIVUserInfo mui = MIVSession.getSession(req).getUser().getTargetUserInfo();
        int userID = mui.getPerson().getUserId();

        BiosketchType biosketchTypeFromSession = (BiosketchType)req.getSession().getAttribute("biosketchType");
        String biosketchTypeID = req.getParameter("biosketchtypeid");

        if (isEmpty(biosketchTypeID) && biosketchTypeFromSession != null)
        {
            biosketchTypeID = String.valueOf(biosketchTypeFromSession.getTypeId());
        }

        BiosketchType biosketchType = BiosketchType.convert(biosketchTypeID);
        Properties pstr = PropertyManager.getPropertySet(biosketchType.getCode(), "strings");
        Map<String,Object> config = new HashMap<String,Object>();
        config.put("strings", pstr);
        req.setAttribute("constants", config);

        Map<String,List<Biosketch>> additionalData = new HashMap<String,List<Biosketch>>();

        if(biosketchType == BiosketchType.INVALID)
        {
            req.setAttribute("exception", true);
            uiModel.addAttribute("biosketchList", null);
            req.getSession().removeAttribute("biosketchType");
        }
        else
        {
            String deletesuccess = req.getParameter("deletesuccess");
            String duplicatesuccess = req.getParameter("duplicatesuccess");

            if(!isEmpty(deletesuccess) && deletesuccess.equalsIgnoreCase("true"))
            {
                req.setAttribute("success", true);
                req.setAttribute("CompletedMessage", "<div id=\"deletebox\">"+biosketchType.getTitle()+" document has been deleted.</div>");
            }
            else
            if(!isEmpty(duplicatesuccess) && duplicatesuccess.equalsIgnoreCase("true"))
            {
                req.setAttribute("success", true);
                req.setAttribute("biosketchID", req.getParameter("biosketchID"));
                req.setAttribute("CompletedMessage", "<div id=\"successbox\">"+biosketchType.getTitle()+" document has been added.</div>");
            }

            List<Biosketch> biosketchList = biosketchService.getBiosketchList(userID, biosketchType.getTypeId());
            for (Biosketch b : biosketchList)
            {
                b.setName(MIVUtil.escapeMarkup(b.getName()));
            }
            req.getSession().setAttribute("biosketchType", biosketchType);
            req.getSession().removeAttribute("biosketchID");
            uiModel.addAttribute("biosketchList", biosketchList);
        }

        req.getSession().removeAttribute("newrecord");
        req.getSession().removeAttribute("showwizard");

        return new ModelAndView("biosketchpreview", uiModel);
    }


    private boolean isEmpty(String value)
    {
        return (value == null || value.length() <= 0);
    }


    private String findKey(HttpServletRequest req)
    {
        Map<?, ?> params = req.getParameterMap();
        boolean matched = false;
        String keyval = null;

        for (Object k : params.keySet())
        {
            String p = (String) k;

            if (p.matches("E[0-9]+") || p.matches("C[0-9]+") || p.matches("DE[0-9]+") || p.matches("DU[0-9]+") || p.matches("Add"))
            {
                String[] vals = req.getParameterValues(p);
                for (String v : vals)
                {
                    if (v.equalsIgnoreCase("edit"))
                    {
                        matched = true;
                        break;
                    }
                    else if (v.equalsIgnoreCase("Delete"))
                    {
                        matched = true;
                        break;
                    }
                    if (v.equalsIgnoreCase("Duplicate This Document"))
                    {
                        matched = true;
                        break;
                    }
                    if (v.equalsIgnoreCase("Create This Document"))
                    {
                        matched = true;
                        break;
                    }
                    if (v.equalsIgnoreCase("Add a New NIH Biosketch Document"))
                    {
                        matched = true;
                        break;
                    }
                    if (v.equalsIgnoreCase("Add a New CV Document"))
                    {
                        matched = true;
                        break;
                    }
                }
                if (matched)
                {
                    keyval = p;
                    break;
                }
            }
        }
        return keyval;
    }
    // findKey()
}
