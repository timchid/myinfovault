/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CreateMyBiosketchController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.service.biosketch.BiosketchService;
import edu.ucdavis.mw.myinfovault.service.biosketch.DocumentCreatorService;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.document.Document;
import edu.ucdavis.myinfovault.format.QuoteCleaner;

/**
 * TODO: Add Javadoc comments!
 *
 * @author venkatv
 * @since MIV 2.1
 *
 * Updated: Pradeep on 08/03/2012
 */
@Controller
@RequestMapping("/biosketch/CreateMyBiosktech")
public class CreateMyBiosketchController extends BiosketchFormController
{
    private BiosketchService biosketchService;
    private DocumentCreatorService documentCreatorService;

    @Autowired
    public CreateMyBiosketchController(BiosketchService biosketchService, DocumentCreatorService documentCreatorService)
    {
        this.biosketchService = biosketchService;
        this.documentCreatorService = documentCreatorService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(HttpServletRequest req)
    {
        String biosketchID = (String) req.getSession().getAttribute("biosketchID");

        if (biosketchID == null)
        {
            return new ModelAndView(new RedirectView(req.getContextPath() + "/biosketch/BiosketchPreview"));
        }

        ModelMap uiModel = new ModelMap();

        MIVUserInfo mui = MIVSession.getSession(req).getUser().getTargetUserInfo();

        Biosketch biosketch = biosketchService.getBiosketch(Integer.parseInt(biosketchID));
        biosketch = biosketchService.loadBiosketchData(biosketch);

        QuoteCleaner quoteCleaner = new QuoteCleaner();
        List<Document> documents = documentCreatorService.createBiosketch(mui, biosketch);
        for (Document document : documents )
        {
            String cleanedDocumentDescription = quoteCleaner.format(MIVUtil.escapeMarkup(document.getDescription()));
            document.setDescription(cleanedDocumentDescription);
        }

        uiModel.addAttribute("documents", documents);
        String cleanedName = quoteCleaner.format(MIVUtil.escapeMarkup(biosketch.getName()));
        uiModel.addAttribute("biosketchname", cleanedName);

        BiosketchType biosketchType = (BiosketchType) req.getSession().getAttribute("biosketchType");
        Properties pstr = PropertyManager.getPropertySet(biosketchType.getCode(), "strings");
        Map<String,Object> config = new HashMap<String,Object>();
        config.put("strings", pstr);
        req.setAttribute("constants", config);
        req.setAttribute("hasDocuments",!documents.isEmpty());

        String showwizard = (String) req.getSession().getAttribute("showwizard");

        uiModel.addAttribute("showwizard", showwizard);

        return new ModelAndView("createmybiosketch", uiModel);
    }
}
