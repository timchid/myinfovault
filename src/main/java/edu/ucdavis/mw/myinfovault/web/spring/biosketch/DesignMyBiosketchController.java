/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DesignMyBiosketchController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.mapping.biosketch.BiosketchDesignMapper;
import edu.ucdavis.mw.myinfovault.service.biosketch.BiosketchService;
import edu.ucdavis.myinfovault.PropertyManager;

/**
 * Controller for the Design My Biosketch page
 * @author svidya
 *
 * Updated: Pradeep on 08/03/2012
 */
@Controller
@RequestMapping("/biosketch/DesignMyBiosketch")
@SessionAttributes({ "biosketch", "biosketchDesignCommand" })
public class DesignMyBiosketchController extends BiosketchFormController
{
    private BiosketchService biosketchService;

    @Autowired
    public DesignMyBiosketchController(BiosketchService biosketchService)
    {
        this.biosketchService = biosketchService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(HttpServletRequest request)
    {
        String biosketchID = (String)request.getSession().getAttribute("biosketchID");
        if (biosketchID == null)
        {
            return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/BiosketchPreview"));
        }

        ModelMap model = new ModelMap();

        BiosketchType biosketchType = (BiosketchType) request.getSession().getAttribute("biosketchType");

        Map<String,Object> config = new HashMap<String,Object>();
        Properties pstr = PropertyManager.getPropertySet(biosketchType.getCode(), "strings");
        config.put("strings", pstr);
        request.setAttribute("constants", config);

        String showwizard = (String) request.getSession().getAttribute("showwizard");

        model.addAttribute("showwizard", showwizard);

        //Load the biosketch design data and create the command object for the jsp by mapping it into the
        //command(BiosketchDesignCommand) object
        BiosketchDesignMapper designMapper = new BiosketchDesignMapper();
        BiosketchDesignCommand biosketchDesignCommand = new BiosketchDesignCommand();
        Biosketch biosketch = biosketchService.loadBiosketchDesignData(Integer.parseInt(biosketchID));

        // Check to apply formatting
        request.getSession().setAttribute("applyFormatting", Boolean.toString(biosketch.getBiosketchStyle().isBodyFormatting()));

        biosketchDesignCommand = designMapper.mapToBiosketchDesignCommand(biosketch, biosketchDesignCommand, biosketchType.getCode());

        model.addAttribute("biosketch", biosketch);
        model.addAttribute("biosketchDesignCommand", biosketchDesignCommand);

        return new ModelAndView("designmybiosketch", model);
    }

    @RequestMapping(method = RequestMethod.POST)
    protected ModelAndView onSubmit(HttpServletRequest request,
            @ModelAttribute("biosketch") Biosketch biosketch,
            @ModelAttribute("biosketchDesignCommand") BiosketchDesignCommand biosketchDesignCommand)
    {
        logger.debug("***** entering DesignMyBiosketchController onSubmit() *****");
        BiosketchDesignMapper designMapper = new BiosketchDesignMapper();
        String biosketchID = (String) request.getSession().getAttribute("biosketchID");
        // Map the command object into the biosketch object, save the data and redirect the request to the respective page
        biosketch = designMapper.mapToBiosketch(biosketchDesignCommand, biosketch);
        if (biosketch != null && Integer.parseInt(biosketchID) == biosketch.getId())
        {
            biosketchService.saveBiosketch(biosketch);
        }
        else
        {
            logger.error("***** DesignMyBiosketchController biosketch object is NULL! *****");
        }
        String btnName = request.getParameter("btnSubmit");

        if (btnName.equalsIgnoreCase("save")) // means updating the record
        {
            return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/BiosketchMenu"));
        }
        else
        {
            return new ModelAndView(new RedirectView(request.getContextPath() + "/biosketch/CreateMyBiosktech"));
        }
    }
}
