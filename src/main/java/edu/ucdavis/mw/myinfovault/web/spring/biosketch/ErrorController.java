/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ErrorController.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Handle errors from the Spring MVC stack, introduced with the original Biosketch rewrite.
 * @author Venkat Vegesna
 * @since MIV 2.1
 */
@Controller
public class ErrorController
{
    /**
     * Handles all exceptions
     *
     * @param e exception
     */
    @ExceptionHandler(Exception.class)
    public void handleException(Exception e)
    {
        String message = e.getMessage();

        message = "Unhandled Exception occurred in Spring stack: " +
            ( message != null && message.length() > 0 ?
                        message :
                        e.getClass().getCanonicalName()
            );

        throw new MivSevereApplicationError(message, e);
    }
}
