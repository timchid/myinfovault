/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CancelDossierAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.canceldossier;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.search.CancelDossierSearchCriteria;
import edu.ucdavis.mw.myinfovault.web.spring.search.ManageOpenAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * TODO: Add Javadoc
 *
 * @author Rick Hendricks
 *
 */
public class CancelDossierAction extends ManageOpenAction
{

    private static String[] breadcrumbs = {"Cancel Dossier: Search"};

    private static final String AUTH_LOG_PATTERN = "User {0} ({1}) is AUTHORIZED to {2}";
    private static final String NOT_AUTH_LOG_PATTERN = "User {0} ({1}) is ** NOT ** AUTHORIZED to {2}";

    /**
     * TODO: Add Javadoc
     *
     * @param searchStrategy
     * @param authorizationService
     * @param userService
     * @param dossierService
     */
    public CancelDossierAction(SearchStrategy searchStrategy,
                                     AuthorizationService authorizationService,
                                     UserService userService,
                                     DossierService dossierService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService);
    }


    /**
     * Checks if user is authorized to cancel dossiers.
     *
     * @param context Request context from webflow
     * @return Event, success or error
     */
    public Event isAuthorized(RequestContext context)
    {
        MivPerson currentPerson = getShadowPerson(context);

        // Set the permission
        String permission = Permission.CANCEL_DOSSIER;

        AttributeSet qualification = new AttributeSet();
        qualification.put(Qualifier.USERID,
                          Integer.toString(currentPerson.getUserId()));

        boolean isAuthorized = authorizationService.hasPermission(currentPerson,
                                                                  permission,
                                                                  qualification);

        // log authentication result
        logger.info(MessageFormat.format(isAuthorized ? AUTH_LOG_PATTERN : NOT_AUTH_LOG_PATTERN,
                                         currentPerson.getDisplayName(),
                                         currentPerson.getPersonId(),
                                         permission));

        // return error if not authorized
        if (!isAuthorized) return denied();

        return allowed();
    }


    /**
     * Get dossiers at the candidate location. Only dossiers at the department location may be canceled.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event getDossiers(RequestContext context)
    {
        //real logged in MIV user
        MIVUser realUser = getUser(context);


       //get dossiers for the target user
       //set results in request scope
        try
        {
           setResults(context,
                      searchStrategy.dossierSearch(getFormObject(context),
                                  realUser,
                                  dossierService.getDossiersByWorkflowLocations(realUser.getTargetUserInfo().getPerson(),
                                  DossierLocation.DEPARTMENT)));
        }
        catch (WorkflowException e)
        {
            logger.warn("Unable to retrieve dossiers at the Department location to cancel" + realUser.getTargetUserInfo().getDisplayName(), e);
            return error();
        }

        return success();
    }


    /**
     * Get the Dossiers to be Canceled.
     *
     * @param context RequestContext
     * @return WebFlow event status
     */
    public Event getDossiersToCancel(RequestContext context)
    {
        CancelDossierSearchCriteria cancelDossierForm = (CancelDossierSearchCriteria) this.getFormObject(context);
        Map<String, String> dossiersToCancelMap = new HashMap<String, String>();

        List<String> dossiers = cancelDossierForm.getDossiersToCancel();

        // Iterate through the dossiers
        for (String dossierId : dossiers)
        {
            String fullName = null;
            try
            {
                final Dossier dossier = this.dossierService.getDossier(dossierId);
                final MivPerson mivPerson = dossier.getAction().getCandidate();

                final String schoolDepartment = mivPerson.getPrimaryAppointment().getScope().getFullDescription();
                fullName = mivPerson.getSortName(); // (mivPerson.getSurname() + ", " + mivPerson.getGivenName() + " " + mivPerson.getMiddleName()).trim();
                String nameAction = fullName + " - " + dossier.getAction().getFullDescription();

                final String dossierDescription = schoolDepartment;// + "; <strong>" + dossier.getAction().getFullDescription() + "</strong>";
                dossiersToCancelMap.put(nameAction, dossierDescription);
            }
            catch (WorkflowException wfe)
            {
                logger.error("Unable to retrieve dossier " + dossierId + " to Cancel for " + fullName, wfe);
            }
        }

        // Sort the list of dossiers to Post Audit Review into alphabetic order by last name
        context.getFlashScope().put("dossiersToCancelMap", new TreeMap<String, String>(dossiersToCancelMap));

        return success();
    }


    /**
     * Cancel the selected dossiers.
     *
     * @param context Webflow request context
     * @return WebFlow event status, success or error
     */
    public Event cancelDossiers(RequestContext context)
    {
        final MivPerson shadowPerson = getShadowPerson(context);
        final MivPerson realPerson = this.getRealPerson(context);

        CancelDossierSearchCriteria cancelDossierForm = (CancelDossierSearchCriteria) this.getFormObject(context);
        Map<String, String> canceledDossiersMap = new HashMap<String, String>();

        List<String> dossiers = cancelDossierForm.getDossiersToCancel();

        logger.info("_AUDIT : User {}{} has canceled Dossiers: {}",
                        new Object[] {
                            realPerson.getUserId(),
                            (shadowPerson.getUserId() != realPerson.getUserId() ?
                                    ", acting as " + shadowPerson.getUserId() + "," : ""),
                            dossiers.toString()
                        }
                    );

        // Iterate through the dossiers
        for (String dossierId : dossiers)
        {
            String fullName = null;
            try
            {
                final Dossier dossier = this.dossierService.getDossier(dossierId);
                final MivPerson mivPerson = dossier.getAction().getCandidate();

                final String schoolDepartment = mivPerson.getPrimaryAppointment().getScope().getFullDescription();
                fullName = mivPerson.getSortName();
                String nameAction = fullName + " - " + dossier.getAction().getFullDescription();

                final String resultMessage = schoolDepartment;// + "; <strong>" + dossier.getAction().getFullDescription() + "</strong>";

                this.dossierService.cancelDossier(shadowPerson, Long.valueOf(dossierId), null);

                canceledDossiersMap.put(nameAction, resultMessage);
            }
            catch (WorkflowException wfe)
            {
                final String msg = "Workflow exception canceling dossier " + dossierId;
                canceledDossiersMap.put(fullName, msg);
                logger.error(msg, wfe);
            }
        }

        // Clear the dossiersToArchive from the form
        cancelDossierForm.getDossiersToCancel().clear();

        // Sort the list of dossiers canceled into alphabetic order by last name
        context.getFlashScope().put("canceledDossiersMap", new TreeMap<String, String>(canceledDossiersMap));

        return success();
    }


    /**
     * Add bread crumbs for manage open action as parent flow.
     *
     * @param context Webflow request context
     */
    @Override
    public void addBreadcrumbs(RequestContext context)
    {
        addBreadcrumbs(context, breadcrumbs);
    }


    /**
     * remove bread crumbs for manage open action as parent flow.
     *
     * @param context Webflow request context
     */
    @Override
    public void removeBreadcrumbs(RequestContext context)
    {
        removeBreadcrumbs(context, breadcrumbs.length);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriteria getFormObject(RequestContext context)
    {
        return (SearchCriteria) super.getFormObject(context);
    }
}
