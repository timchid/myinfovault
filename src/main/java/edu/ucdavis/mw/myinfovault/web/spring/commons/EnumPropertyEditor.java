/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * From: 'valodzka' (http://stackoverflow.com/users/159550/valodzka)
 *   at http://stackoverflow.com/a/14412317/17300
 * MODULE NAME: EnumPropertyEditor.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.commons;

import java.beans.PropertyEditorSupport;


/**<p>
 * A Property Editor for any Enum type.</p>
 * <p>This was built in an attempt to eliminate a large number of logged warnings
 * whenever the new RAF (aka "appointment form") is accessed or saved. The
 * warnings are:<br>
 * <code>&nbsp; TypeConverterDelegate - PropertyEditor [com.sun.beans.editors.EnumEditor]
 *   found through deprecated global PropertyEditorManager fallback -
 *   consider using a more isolated form of registration,
 *   e.g. on the BeanWrapper/BeanFactory!</code>
 * </p>
 * <p>The warnings have not been eliminated because I have been unable to find the
 * proper place to register this property editor.</p>
 *
 * @author valodzka
 * @see <a href="http://stackoverflow.com/a/14412317/17300">"Depracation warning from spring"</a> on Stack Overflow
 */
public final class EnumPropertyEditor extends PropertyEditorSupport
{
    public EnumPropertyEditor() {
    }

    @Override
    public String getAsText() {
        return (String) getValue();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        setValue(text);
    }
}
