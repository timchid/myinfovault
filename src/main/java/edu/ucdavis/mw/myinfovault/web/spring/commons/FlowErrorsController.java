/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FlowErrorsController.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.commons;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.webflow.conversation.NoSuchConversationException;
import org.springframework.webflow.execution.repository.NoSuchFlowExecutionException;
//import org.springframework.webflow.executor.support.FlowExecutorArgumentExtractionException;

/**
 * @author mary northup
 * @since MIV 3.0
 */
public class FlowErrorsController implements HandlerExceptionResolver/*, Ordered*/
{
    protected Logger log = Logger.getLogger(this.getClass());

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.HandlerExceptionResolver#resolveException(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e)
    {
        log.error("FlowErrorsController: Unhandled Exception occurred in WebFlow stack", e);
        String resultUrl = null;

        //MIV-3023 catching the NoSuchFlowExecutionException and the FlowExecutorArgumentExtractionException
        // which appear to be Web Flow "timeout" issues
        // The FlowExecutorArgumentExtractionException happens when the user is idle for some RANDOM period of time;
        // it might make sense to display a different page for this error (referring to IDLE time).
        if (/*e instanceof FlowExecutorArgumentExtractionException  || */
            e instanceof NoSuchFlowExecutionException ||
            e instanceof NoSuchConversationException)
        {
//          resultUrl = request.getContextPath() + "/jsp/webflowexception.jsp";
            resultUrl = "/jsp/webflowexception.jsp";
        }

        if (resultUrl == null)
        {
            //  let the MIVErrorFilter handle this
            return null;
        }

        // Fill in stack trace information from this point if there isn't any yet
        StackTraceElement[] trace = e.getStackTrace();
        if (trace == null || trace.length == 0) {
            e.fillInStackTrace();
        }

        log.error("FlowErrorsController: redirecting this error - " + e.getClass().getCanonicalName());

        // Original...
//      return new ModelAndView(new RedirectView(resultUrl));
        // Don't have to use "request.getContextPath()" if you pass 'true' for the contextRelative parameter
//        return new ModelAndView(new RedirectView(resultUrl, true));
        /*
           But even better is to use an Internal Resource instead of redirecting.
           This sets the resultUrl page as the view, rather than sending the browser a redirect that tells
           it to request the webflowexception.jsp page.
         */
        return new ModelAndView(new InternalResourceView(resultUrl));
    }


    /**,"message",message
     * @uml.property  name="order"
     */
    private int order;

    /**
     * @uml.property  name="order"
     */
    /* (non-Javadoc)
     * @see org.springframework.core.Ordered#getOrder()
     */
    public int getOrder() {
        return order;
    }

    /**
     * Setter of the property <tt>order</tt>
     * @param order  The order to set.
     * @uml.property  name="order"
     */
    public void setOrder(int order) {
        this.order = order;
    }

}
