/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AppointeeResponse.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.datatables;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.google.common.collect.ImmutableMap;

import edu.ucdavis.iet.commons.ldap.domain.LdapPerson;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.decision.DecisionType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.decision.RecommendationFilter;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Data Tables response for {@link DataTablesController#getUsers(MivPerson, DataTablesRequest, String) campus search}
 * and {@link DataTablesController#getUsers(MivPerson, DataTablesRequest, MivRole, String) MIV appointee search} requests.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.5
 */
public abstract class AddUserResponse extends DataTablesResponse<Map<String, Object>>
{
    /**
     * Action type of actions for which determination of "complete" is required.
     */
    private static final DossierActionType actionType = DossierActionType.APPOINTMENT;

    /*
     * Record disabled reasons.
     */
    private static final String EXISTS = "User already exists as a {0} in {1}.";
    private static final String WRONG_LOCATION = "The {0} is not yet at the {1} or {2} locations.";
    private static final String WRONG_DECISION = "The {0} on the {1} action was \"{2}\".";
    private static final String NO_DECISION = "No decision has been made for the {0} action.";
    private static final String NOT_AUTHORIZED = "You are not authorized to convert a user in the {0} department.";
    private static final String NO_ACTION = "No {0} action exists for this person.";
    private static final String INCOMPLETE_ACTION = "The {0} action is incomplete. For more information contact your dean's office.";

    /**
     * Instantiate an Data Tables response that may include appointee records.
     *
     * @param request
     */
    public AddUserResponse(DataTablesRequest request)
    {
        super(request);
    }

    /**
     * @return list of MIV persons from which to generate records
     */
    protected abstract List<MivPerson> getPeople();

    /**
     * @return MIV person requesting the people
     */
    protected abstract MivPerson getRequester();

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.datatables.DataTablesResponse#getRecords()
     */
    @Override
    protected List<Map<String, Object>> getRecords()
    {
        List<MivPerson> people = getPeople();

        List<Map<String, Object>> records = new ArrayList<>(people.size());

        // filter out recommendations
        SearchFilter<Decision> decisionFilter = new RecommendationFilter();

        /*
         * Build a record for each person.
         */
        for (MivPerson person : people)
        {
            String primaryDepartment = "";
            String status = "";
            String location = "";

            // attempt to get department information from primary appointment
            if (person.getPrimaryAppointment() != null)
            {
                primaryDepartment = person.getPrimaryAppointment().getScope().getDescription();
            }
            // otherwise, retrieve from LDAP
            else
            {
                LdapPerson ldapPerson = MivServiceLocator.getLdapPersonService().findByUniversalUserId(person.getPersonId());

                //LdapPerson returns null in some circumstances (when it is empty?) Causing some errors.
                if (ldapPerson.getOu() != null && !ldapPerson.getOu().isEmpty())
                {
                    primaryDepartment = ldapPerson.getOu().get(0);
                }
            }

            String reasonDisabled = null;

            if (person.hasRole(MivRole.APPOINTEE))
            {
                status = "Incomplete";
                location = DossierLocation.UNKNOWN.getDescription();

                Dossier dossier = getLatestCompletedAppointmentDossier(person);

                // if at least one appointment dossier
                if (dossier != null)
                {
                    location = dossier.getDossierLocation().getDescription();

                    // all decisions on this appointment action for this person
                    Iterator<Decision> decisions = decisionFilter.apply(
                                                       MivServiceLocator.getDecisionService().getDecisions(dossier)
                                                   ).iterator();

                    /*
                     * Action is complete if there's a decision not denied and either:
                     *   - decision is for an appeal; OR
                     *   - action is archived; OR
                     *   - action is at the post audit review location
                     */
                    if (decisions.hasNext())
                    {
                        Decision decision = decisions.next();

                        if (decision.getDecisionType() != null &&
                            decision.getDecisionType() != DecisionType.DENIED)
                        {
                            if (decision.getDocumentType().name().contains("APPEAL")
                             || decision.getDossier().getDossierLocation() == DossierLocation.ARCHIVE
                             || decision.getDossier().getDossierLocation() == DossierLocation.POSTAUDITREVIEW)
                            {
                                status = "Complete";
                            }
                            else
                            {
                                reasonDisabled = MessageFormat.format(WRONG_LOCATION,
                                                                      decision.getDossier().getAction().getActionType().getDescription(),
                                                                      DossierLocation.ARCHIVE.getDescription(),
                                                                      DossierLocation.POSTAUDITREVIEW.getDescription());
                            }
                        }
                        else
                        {
                            /*
                             * Authorize the requester to view this decision.
                             */
                            AttributeSet permissionDetails = new AttributeSet();
                            permissionDetails.put(PermissionDetail.DECISION_TYPE, decision.getDocumentType().getAllowedSigner().name());
                            boolean canViewDecision = MivServiceLocator.getAuthorizationService()
                                                                       .hasPermission(getRequester(),
                                                                                      Permission.VIEW_DECISION,
                                                                                      permissionDetails);

                            if (canViewDecision)
                            {
                                // those allowed to view the decision, may know if it were not approved
                                reasonDisabled = MessageFormat.format(WRONG_DECISION,
                                                                      decision.getDocumentType().getDescription(),
                                                                      decision.getDossier().getAction().getActionType().getDescription(),
                                                                      decision.getDecisionType().getDecisionDescription());
                            }
                            else
                            {
                                reasonDisabled = MessageFormat.format(INCOMPLETE_ACTION,
                                                                      decision.getDossier().getAction().getActionType().getDescription());
                            }
                        }
                    }
                    else
                    {
                        reasonDisabled = MessageFormat.format(NO_DECISION, actionType.getDescription());
                    }

                    /*
                     * Authorize the requester to add the person.
                     */
                    AttributeSet qualification = new AttributeSet();
                    qualification.put(Qualifier.USERID, String.valueOf(person.getUserId()));
                    boolean canAdd = MivServiceLocator.getAuthorizationService()
                                                      .isAuthorized(getRequester(),
                                                                    Permission.ADD_USER,
                                                                    null,
                                                                    qualification);
                    if (!canAdd)
                    {
                        reasonDisabled = MessageFormat.format(NOT_AUTHORIZED, person.getPrimaryAppointment().getScope().getDescription());
                    }
                }
                else
                {
                    reasonDisabled = MessageFormat.format(NO_ACTION, actionType.getDescription());
                }
            }
            else if (person.getUserId() > 0)
            {

                reasonDisabled = MessageFormat.format(EXISTS, person.getPrimaryRoleType().getDescription(), primaryDepartment);
            }

            Map<String, Object> record = ImmutableMap.<String, Object>builder()
                                                     .put("user", person)
                                                     .put("primaryDepartment", primaryDepartment)
                                                     .put("status", status)
                                                     .put("location", location)
                                                     .build();

            // mark record disabled
            addData(record, "disabled", reasonDisabled);

            records.add(record);
        }

        return records;
    }

    /**
     * Filter out dossiers with action types other than {@link #actionType}.
     */
    private static SearchFilter<Dossier> dossierFilter = new SearchFilterAdapter<Dossier>() {
        @Override
        public boolean include(Dossier item) {
            return item.getAction().getActionType() == actionType;
        }
    };

    /**
     * Order dossiers by completion date, considering a null date as occurring before any date.
     */
    private static Comparator<Dossier> dossierComparator = new Comparator<Dossier>(){
        @Override
        public int compare(Dossier a, Dossier b) {
            if (a.getCompletedDate() == null)
            {
                return b.getCompletedDate() == null ? 0 : -1;
            }
            else if (b.getCompletedDate() == null)
            {
                return 1;
            }
            else
            {
                return a.getCompletedDate().compareTo(b.getCompletedDate());
            }
        }
    };

    /**
     * Get the latest completed new appointment dossier for the given person.
     *
     * @param person person with new appointment dossier(s)
     * @return latest completed new appointment dossier or <code>null</code> if DNE
     */
    private static Dossier getLatestCompletedAppointmentDossier(MivPerson person)
    {
        Set<Dossier> dossiers = new TreeSet<>(dossierComparator);

        try
        {
            // filter by action type and add to ordered set
            dossiers.addAll(dossierFilter.apply(
                MivServiceLocator.getDossierService().getAllDossiersByUser(person)
            ));
        }
        catch (WorkflowException e)
        {
            throw new MivSevereApplicationError("errors.dossier.candidate", e, person);
        }

        // return null if none
        return dossiers.isEmpty() ? null : dossiers.iterator().next();
    }
}
