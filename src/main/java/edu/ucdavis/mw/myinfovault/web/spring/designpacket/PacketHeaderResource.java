package edu.ucdavis.mw.myinfovault.web.spring.designpacket;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author japorito
 * @since 5.0
 */
public class PacketHeaderResource
{
    private String label;
    private Integer sectionId;
    private Integer additionalInfoHeaderId;
    private Integer userId;
    private Boolean included;

    public PacketHeaderResource()
    {
        //used by Spring
    }

    public PacketHeaderResource(String label,
                                Integer sectionId,
                                Integer userId,
                                Boolean included)
    {
        this.label = label;
        this.sectionId = sectionId;
        this.userId = userId;
        this.included = included;
    }

    public PacketHeaderResource(String label,
                                Integer sectionId,
                                Integer additionalInfoHeaderId,
                                Integer userId,
                                Boolean included)
    {
        this(label, sectionId, userId, included);
        this.additionalInfoHeaderId = additionalInfoHeaderId;
    }

    /**
     * @return the label
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label)
    {
        this.label = label;
    }

    /**
     * @return the sectionId
     */
    public Integer getSectionId()
    {
        return sectionId;
    }

    /**
     * @param sectionId the sectionId to set
     */
    public void setSectionId(Integer sectionId)
    {
        this.sectionId = sectionId;
    }

    /**
     * @return the additionalInfoHeaderId
     */
    @JsonInclude(Include.NON_NULL)
    public Integer getAdditionalInfoHeaderId()
    {
        return additionalInfoHeaderId;
    }

    /**
     * @param additionalInfoHeaderId the additionalInfoHeaderId to set
     */
    public void setAdditionalInfoHeaderId(Integer additionalInfoHeaderId)
    {
        this.additionalInfoHeaderId = additionalInfoHeaderId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId()
    {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    /**
     * @return the included
     */
    public Boolean getIncluded()
    {
        return included;
    }

    /**
     * @param included the included to set
     */
    public void setIncluded(Boolean included)
    {
        this.included = included;
    }
}
