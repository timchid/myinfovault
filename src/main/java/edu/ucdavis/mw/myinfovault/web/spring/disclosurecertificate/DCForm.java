/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.disclosurecertificate;

import java.io.Serializable;
import java.util.Date;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.document.PathConstructor;

/**
 * Form backing for disclosure certificate.
 *
 * @author Jed Whitten
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class DCForm implements Serializable
{
    private static final long serialVersionUID = 20090724172243L;

    private static PathConstructor pathConstructor = new PathConstructor();

    private DCBo dc;
    private final MivPerson realPerson;
    private final MivPerson targetPerson;
    private final Date dateSigned;
    private final String dossierUrl;
    private final String emailSubject;
    private final String emailMessage;
    private String emailTo = StringUtil.EMPTY_STRING;
    private String emailCc = StringUtil.EMPTY_STRING;
    private String emailBody = StringUtil.EMPTY_STRING;
    private Date emailSent = null;

    /**
     * Create the DC form backing object.
     *
     * @param dc disclosure certificate draft
     * @param realPerson The logged-in MIV administrator acting on the disclosure certificate
     * @param targetPerson MIV target person effecting changes
     * @param dateSigned
     * @param emailSubject
     * @param emailMessage
     */
    DCForm(DCBo dc,
           MivPerson realPerson,
           MivPerson targetPerson,
           Date dateSigned,
           String emailSubject,
           String emailMessage)
    {
        this.dc = dc;
        this.realPerson = realPerson;
        this.targetPerson = targetPerson;
        this.dateSigned = dateSigned;
        this.dossierUrl = dc.getDossier().getDossierPdfUrl(MivRole.CANDIDATE,
                                                           dc.getSchoolId(),
                                                           dc.getDepartmentId());

        this.emailSubject = emailSubject;
        this.emailMessage = emailMessage;
    }

    /**
     * @return disclosure certificate business object
     */
    public DCBo getDc()
    {
        return dc;
    }

    /**
     * @return real, logged-in person effecting change
     */
    public MivPerson getRealPerson()
    {
        return realPerson;
    }

    /**
     * @return the DC signing date
     */
    public Date getDateSigned()
    {
        return dateSigned;
    }

    /**
     * @return if the DC is in revision
     */
    public boolean isRevision()
    {
        return dateSigned != null;// revision if has been signed before
    }

    /**
     * @return URL to the dossier for the DC
     */
    public String getDossierUrl()
    {
        return dossierUrl;
    }

    /**
     * @return URL to the disclosure for the DC
     */
    public String getDisclosureUrl()
    {
        return pathConstructor.getUrlFromPath(dc.getDossierFile());
    }

    /**
     * @return the date the email message was sent
     */
    public Date getEmailSent()
    {
        return emailSent;
    }

    /**
     * @param emailSent the date the email message was sent
     */
    public void setEmailSent(Date emailSent)
    {
        this.emailSent = emailSent;
    }

    /**
     * @return the email message text
     */
    public String getEmailMessage()
    {
        return emailMessage;
    }

    /**
     * @return to whom the email is sent unless overridden
     */
    public String getEmailToDefault()
    {
        return dc.getDossier().getAction().getCandidate().getDisplayName() + " <" + dc.getDossier().getAction().getCandidate().getPreferredEmail() + ">";
    }

    /**
     * @return to whom the email is sent
     */
    public String getEmailTo()
    {
        return StringUtils.hasText(emailTo) ? emailTo : getEmailToDefault();
    }

    /**
     * @param emailTo to whom the email is sent
     */
    public void setEmailTo(String emailTo)
    {
        this.emailTo = emailTo;
    }

    /**
     * @return to whom the email is additionally sent
     */
    public String getEmailCc()
    {
        return emailCc;
    }

    /**
     * @param emailCc to whom the email is additionally sent
     */
    public void setEmailCc(String emailCc)
    {
        this.emailCc = emailCc;
    }

    /**
     * @return text added to the email message
     */
    public String getEmailBody()
    {
        return emailBody;
    }

    /**
     *
     * @param emailBody text added to the email message
     */
    public void setEmailBody(String emailBody)
    {
        this.emailBody = emailBody;
    }

    /**
     * @return from whom the email is sent
     */
    public String getEmailFrom()
    {
        return targetPerson.getDisplayName() + " <" + targetPerson.getPreferredEmail() + ">";
    }

    /**
     * @return the subject of the email
     */
    public String getEmailSubject()
    {
        return emailSubject;
    }

    /**
     * Ignore, may not change.
     *
     * @param emailMessage
     */
    public void setEmailMessage(String emailMessage){}

    /**
     * Ignore, may not change.
     *
     * @param emailFrom
     */
    public void setEmailFrom(String emailFrom){}

    /**
     * Ignore, may not change.
     *
     * @param emailSubject
     */
    public void setEmailSubject(String emailSubject){}
}
