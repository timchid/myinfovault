/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.disclosurecertificate;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVConfig;

/**
 * Validates the disclosure certificate form.
 *
 * @author Jed Whitten
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class DCValidator implements Validator
{
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final boolean IS_PRODUCTION = MIVConfig.getConfig().isProductionServer();

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return DCForm.class.isAssignableFrom(clazz);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object obj, Errors errors)
    {
        DCForm dc = (DCForm) obj;
        
		if (dc.isRevision() && StringUtils.isBlank(dc.getDc().getChangesAdditions()))
        {
			errors.reject(null, "Changes/Additions : This field is required.");
        }

        // only a logged-in MIV administrator may alter the 'To' field on production
        if (IS_PRODUCTION
         && !dc.getEmailTo().equals(dc.getEmailToDefault())
         && !dc.getRealPerson().hasRole(MivRole.VICE_PROVOST_STAFF, MivRole.SYS_ADMIN))
        {
            // reset 'To' address (forces default to be used)
            dc.setEmailTo(null);

            // Log possible hacking attempt
            logger.warn("Possible Hacking Attempt! User attempted to change the email form's 'TO' field. " +
                        "\nLogged-in User: " + dc.getRealPerson());
        }
    }
}
