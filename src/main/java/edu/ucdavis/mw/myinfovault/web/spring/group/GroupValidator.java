/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.group;

import java.util.Arrays;
import java.util.Set;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupType;
import edu.ucdavis.mw.myinfovault.service.group.GroupCreatorFilter;
import edu.ucdavis.mw.myinfovault.service.group.GroupService;
import edu.ucdavis.mw.myinfovault.service.group.GroupTypeFilter;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteriaValidator;

/**
 * Validates the form backing object for the manage groups flow.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupValidator extends SearchCriteriaValidator implements Validator
{
    //maximum string lengths for the name and description fields of GroupForm
    private final static int MAX_NAME_LENGTH = 25;
    private final static int MAX_DESCRIPTION_LENGTH = 100;

    //error messages
    private final static String NAME_EMPTY = "\"Group Name\" is required";
    private final static String NAME_LENGTH = "\"Group Name\" must not be greater than " + MAX_NAME_LENGTH + " characters";
    private final static String NAME_EXISTS = "A group with this name by this creator already exists";
    private final static String DESCRIPTION_LENGTH = "\"Group Description\" must not be greater than " + MAX_DESCRIPTION_LENGTH + " characters";
    private final static String SHARED_SELECTION = "\"Shared\" or \"Private\" must be selected under the \"Sharing and permission\" section";
    private final static String SHARED_PERMISSION = "You do not have permission to mark this group \"Private\"";
    private final static String NOT_SHARED = "At least one administrator group must be selected in the \"Sharing and permission\" section";
    private final static String READONLY_PERMISSION = "You do not have permission to mark this group \"Read Only\"";

    private GroupService groupService;

    /**
     * Create a Group validator object with support from the Group service.
     *
     * @param groupService
     */
    public GroupValidator(GroupService groupService)
    {
        this.groupService = groupService;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteriaValidator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return GroupForm.class.isAssignableFrom(clazz)
            && super.supports(clazz);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteriaValidator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void validate(Object obj, Errors errors)
    {
        GroupForm form = (GroupForm) obj;

        //check name is empty
        if (!StringUtils.hasText(form.getGroup().getName()))
        {
            errors.reject(null, NAME_EMPTY);
        }
        //check name length
        else if (form.getGroup().getName().length() > MAX_NAME_LENGTH)
        {
            errors.reject(null, NAME_LENGTH);
        }
        //check name uniqueness amongst 'GROUP' type groups for this group's creator
        else if (form.getGroup().getType() == GroupType.GROUP)
        {
            //split group name into word array
            String[] groupNameWords = getWords(form.getGroup().getName());

            //get every group by this creator and type
            Set<Group> creatorGroups = groupService.getGroups(new GroupTypeFilter(form.getGroup().getType()),
                                                              new GroupCreatorFilter(form.getGroup().getCreator()));

            //reduce set to every OTHER group by this creator and type
            creatorGroups.remove(form.getGroup());

            //for each group by this creator
            for (Group creatorGroup : creatorGroups)
            {
                //error if every word in the group name matches every in another group by this creator
                if (Arrays.equals(groupNameWords,
                                  getWords(creatorGroup.getName())))
                {
                    errors.reject(null, NAME_EXISTS);

                    //no need to continue checking
                    break;
                }
            }
        }

        //check description length
        if (StringUtils.hasText(form.getGroup().getDescription())
         && form.getGroup().getDescription().length() > MAX_DESCRIPTION_LENGTH)
        {
            errors.reject(null, DESCRIPTION_LENGTH);
        }

        //check shared/private
        if (form.getGroup().getHidden() == null)
        {
            errors.reject(null, SHARED_SELECTION);
        }
        //check permission to set private
        else if (form.getGroup().getHidden()
         && !form.getGroup().getCreator().equals(form.getActingPerson()))
        {
            errors.reject(null, SHARED_PERMISSION);
        }
        //check accessibility
        else if(!form.getGroup().getHidden()
             && form.getGroup().getAssignedRoles().isEmpty())
        {
            errors.reject(null, NOT_SHARED);
        }

        //check permission set read only
        if (form.getGroup().isReadOnly()
         && !form.getGroup().getCreator().equals(form.getActingPerson()))
        {
            errors.reject(null, READONLY_PERMISSION);
        }
    }

    /**
     * Get lower case words of a phrase by tokenizing by 'space'.
     * 
     * @param phrase
     * @return tokenized phrase
     */
    private String[] getWords(String phrase)
    {
        return StringUtils.tokenizeToStringArray(phrase.toLowerCase(), " ");
    }

    /**
     * Validate the search criteria form.
     *
     * @param form Search criteria form
     * @param errors Repository of errors for in this context.
     */
    public void validateSearchCriteria(SearchCriteria form,
                                       Errors errors)
    {
        super.validate(form, errors);
    }
}
