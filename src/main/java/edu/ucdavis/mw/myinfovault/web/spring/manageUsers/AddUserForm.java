/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AddUserForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.MivRole.Category;
import edu.ucdavis.mw.myinfovault.service.person.UserService;

/**
 * Spring Form backing object adding users.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class AddUserForm extends EditUserForm implements Serializable
{
    private static final long serialVersionUID = 201310211226L;

    private Category accountType;
    private MivPerson campusPerson;
    private MivPerson shadowPerson;
    private Integer userId;
    UserService userService = MivServiceLocator.getUserService();


    @Override
    public String getSearchText()
    {
        return searchText;
    }

    @Override
    public void setSearchText(String searchText)
    {
        this.searchText = searchText;
    }

    public MivPerson getCampusPerson()
    {
        return campusPerson;
    }

    public void setCampusPerson(MivPerson campusPerson)
    {
        this.campusPerson = campusPerson;
    }

    /**
     * @return the accountType
     */
    public Category getAccountType()
    {
        return accountType;
    }

    /**
     * @param accountType the accountType to set
     */
    public void setAccountType(Category accountType)
    {
        this.accountType = accountType;
    }

    public String getEmail()
    {
        return this.getSelectedEmail();
    }

    public void setEmail(String email)
    {
        this.setSelectedEmail(email);
        this.setCampusPerson(userService.findPersonByEmail(email).get(0));

        if (getCampusPerson() != null)
        {
            this.setDisplayName(getCampusPerson().getDisplayName());
        }

    }

    public MivRole getRole()
    {
        return this.getSelectedRole();
    }

    public void setRole(MivRole role)
    {
        this.setSelectedRole(role);
    }

    public Integer getUserId()
    {
        return this.userId;
    }

    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    public MivPerson getShadowPerson()
    {
        return this.shadowPerson;
    }

    public void setShadowPerson(MivPerson shadowPerson)
    {
        this.shadowPerson = shadowPerson;
    }
}
