/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AddUserValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.APPOINTEE;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.MIV_USER;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.decision.DecisionType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.decision.DecisionService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.MivWorkflowConstants;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.MivRole.Category;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;

/**
 * TODO: add javadoc
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
@Component
public class AddUserValidator implements Validator
{

    protected final Logger log = LoggerFactory.getLogger(this.getClass());


    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return AddUserForm.class.isAssignableFrom(clazz);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object obj, Errors errors)
    {
        AddUserForm addUserForm = (AddUserForm) obj;

        MivPerson addPerson = addUserForm.getCampusPerson();
        MivPerson editor = addUserForm.getShadowPerson();
        AttributeSet qualification = new AttributeSet();

        // Person not found
        if (addPerson == null)
        {
            errors.reject(null, "This email address could not be found in the UC Davis Online Directory.");
        }
        // Person was found, but has no login ID
        else if (StringUtils.isBlank(addPerson.getLoggingName()))
        {
            errors.reject(null, "User could not be found.");
        }

        // Person was found, but is already an MIV user (already has the MIV_USER role), but must not also be an APPOINTEE
        // Note that a staff role for which an appointment has been started will have the MIV_USER
        // role AND the APPOINTEE role.
        else if (addPerson.hasRole(MIV_USER) && !addPerson.hasRole(APPOINTEE))
        {
            String displayName = addPerson.getDisplayName();
            // Already added
            if (addPerson.isActive())
            {
                MivRole role = addPerson.getPrimaryRoleType();
                Appointment primaryAppointment = addPerson.getPrimaryAppointment();
                String errorMsg = "This email address is already in use by " + displayName;

                // Adding person's preferred email only when its different from entered email id.
                if (!addPerson.getPreferredEmail().equalsIgnoreCase(addUserForm.getSearchText().trim()))
                {
                    errorMsg += " (email " + addPerson.getPreferredEmail() + ")";
                }

                errorMsg += " who has the " + role.getDescription()
                          + " role in " + primaryAppointment.getScope().getDescription()
                          + ".";

                errors.reject(null, errorMsg);
            }
            // User deactivated
            else
            {
                errors.reject(null, displayName + " already has an MIV account but has been deactivated.");
            }
        }
        // If we have a userId, then we are converting an appointee and the following validation is to insure
        // that the userId which has been sent from the form has not been tampered with
         else if (addUserForm.getUserId() > 0)
        {
            // The userId in the form should be that of an appointee
            MivPerson appointee = MivServiceLocator.getUserService().getPersonByMivId(addUserForm.getUserId());
            String errMsg = null;

            if (!appointee.hasRole(MivRole.APPOINTEE))
            {
                errMsg = appointee.getDisplayName() + " must have the appointee role in order to be converted.";
                errors.reject(null, errMsg);
                log.warn("Possible Hacking Attempt ! - "+errMsg);
            }
            else if (addUserForm.getAccountType() == Category.STAFF)
            {
                errMsg = appointee.getDisplayName() + " is an appointee. Appointees may not be added as staff.";
                errors.reject(null, errMsg);
                log.warn("Possible Hacking Attempt ! - "+errMsg);
            }

            // Make sure the converting user is authorized to add a user
            qualification.put(Qualifier.USERID, String.valueOf(addUserForm.getUserId()));

            if (!MivServiceLocator.getAuthorizationService().isAuthorized(editor,Permission.ADD_USER,null,qualification))
            {
                errMsg = editor.getDisplayName()+" is not authorized to convert user "+appointee.getDisplayName()+" to a candidate.";
                errors.reject(null, errMsg);
                log.warn("Possible Hacking Attempt ! - "+errMsg);
            }

            // Verify that the appointee is indeed a fuzzy match
            if (!MivServiceLocator.getUserService().findAppointeesBySimilarName(addUserForm.getCampusPerson()).contains(appointee))
            {
                errMsg = appointee.getDisplayName()+" is not similar to "+addPerson.getDisplayName()+" and can not be converted to a candidate.";
                errors.reject(null, errMsg);
                log.warn("Possible Hacking Attempt ! - "+errMsg);
            }

            // Check that there are no dossiers in process and that there is an approved decision present for the appointee
            try
            {
                DossierService dossierService = MivServiceLocator.getDossierService();

                // There can be no dossier in process for an appointee in order to qualify for conversion to candidate
                if (dossierService.getDossiersInProcess(appointee).size() > 0)
                {
                    errMsg = appointee.getDisplayName()+" currently has a dossier in process and is not eligible to be converted to a candidate.";
                    errors.reject(null, errMsg);
                    log.warn("Possible Hacking Attempt ! - "+errMsg);
                }

                // There must be an approved APPOINTMENT decision for the appointee in order to convert to a candidate
                DecisionService decisionService = MivServiceLocator.getDecisionService();
                List<Dossier>dossiers = dossierService.getAllDossiersByUser(appointee);

                // It is theoretically possible for a person to have multiple appointment dossiers
                // so we will create a map of appointment decisions sorted by date in order to check
                // the most recent decision
                NavigableMap<Date, Decision>appointmentDecisionMap = new TreeMap<Date, Decision>();
                for (Dossier dossier : dossiers)
                {
                    // Skip cancelled dossiers
                    if (dossier.getWorkflowStatus() == MivWorkflowConstants.ROUTE_HEADER_CANCEL_CD)
                    {
                        continue;
                    }

                    // Put all decisions for the APPOINTMENT in the map, regardless of the decision
                    if (dossier.getAction().getActionType() == DossierActionType.APPOINTMENT)
                    {
                        /*
                         * Returns decisions in order of significance.
                         *
                         * E.g. An appeal decision trumps an existing decision on the
                         * action, and therefore appears earlier in the iteration.
                         */
                        Iterator<Decision> decisions = decisionService.getDecisions(dossier).iterator();

                        if (decisions.hasNext())
                        {
                            Decision decision = decisions.next();

                            if (decision.getDocumentType().isDecision())
                            {
                                appointmentDecisionMap.put(dossier.getCompletedDate(), decision);
                            }
                        }
                    }
                }

                // Check the last (most recent) appointment decision to make sure it was not denied
                if (appointmentDecisionMap.isEmpty() || appointmentDecisionMap.lastEntry().getValue().getDecisionType() == DecisionType.DENIED)
                {
                    errMsg = appointee.getDisplayName()+" currently has an incomplete decision and is not eligible to be converted to a candidate.";
                    errors.reject(null, errMsg);
                    log.warn("Possible Hacking Attempt ! - "+errMsg);
                }

            }
            catch (WorkflowException e)
            {
                errMsg = "Unable to confirm eligibility of "+appointee.getDisplayName()+" for conversion to candidate.";
                errors.reject(null, errMsg);
                log.warn("Possible Hacking Attempt ! - "+errMsg);
            }
        }
        //There is no user id, so the person isn't an existing appointee.
        //The person is also not an existing MIV_USER
        else
        {
            String errMsg = null;

            //the scope that they're trying to add the target person with
            AutoPopulatingList<AssignmentLine> formLines = addUserForm.getFormLines();

            //The person's appointments
            if (addUserForm.getAccountType() == Category.STAFF)
            {
                //assignments should be limited to the scope of the editor
                Iterator<AssignmentLine> formLinesIterator = formLines.iterator();
                while (formLinesIterator.hasNext()) {
                    if (!validateFormLineScope(formLinesIterator.next(), editor)) {
                        errMsg = editor.getDisplayName() +
                                " is not authorized to add " +
                                addPerson.getDisplayName()+
                                " in this school/department.";

                        errors.reject(null, errMsg);
                        log.warn("Possible Hacking Attempt ! - "+errMsg);
                    }
                }

            }
            else if (addUserForm.getAccountType() == Category.FACULTY)
            {
                //primary must be in scope of editor
                if (!validateFormLineScope(formLines.get(0), editor)) {
                    errMsg = editor.getDisplayName() +
                            " is not authorized to add " +
                            addPerson.getDisplayName()+
                            " in this school/department.";

                    errors.reject(null, errMsg);
                    log.warn("Possible Hacking Attempt ! - "+errMsg);
                }
            }
        }
    }

    public Boolean validateFormLineScope(AssignmentLine line, MivPerson editor)
    {
        Scope addScope = line.getScope();
        AttributeSet qualification = new AttributeSet();

        qualification.put(Qualifier.SCHOOL, String.valueOf(addScope.getSchool()));
        qualification.put(Qualifier.DEPARTMENT, String.valueOf(addScope.getDepartment()));

        //if form line has no scope, it will not be added and does not need validated
        //otherwise, validate that the editor has permission to add a person with that
        //scope
        return addScope.matches(Scope.NoScope) ||
               MivServiceLocator.getAuthorizationService().isAuthorized(editor,
                                                                        Permission.ADD_USER,
                                                                        null,
                                                                        qualification);
    }
}
