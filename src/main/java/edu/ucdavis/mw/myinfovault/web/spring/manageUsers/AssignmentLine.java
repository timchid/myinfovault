package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.service.person.AppointmentScope;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Represents an Appointment/Assignment line on the Edit User form w/drop-down list and possible checkboxes.
 * @author Stephen Paulsen
 *
 */
public class AssignmentLine implements Serializable
{
    private static final long serialVersionUID = 201108101046L;

    // School + Department, chosen from a drop-down list
    private AppointmentScope scope = new AppointmentScope(Scope.NoScope);

    // The checkboxes - used only when the role is "Candidate"
    private Boolean candidate = false;
    private Boolean deptChair = false;
    private Boolean dean = false;

    /**
     * Flag to indicate this line should be rendered read-only (as text)
     * rather than active controls.
     * Provided so rows can be shown safely when the actor does not have
     * authority in the scope represented by this line.
     */
    private boolean readOnly = false;

    /* no arg constructor for spring */
    public AssignmentLine()
    {
        this(Scope.None);
        this.candidate = false;
        this.dean = false;
        this.deptChair = false;
    }

    public AssignmentLine(Scope scope)
    {
        this.scope = new AppointmentScope(scope);
    }

    public AssignmentLine(Scope scope, boolean isCandidate, boolean isDeptChair, boolean isDean)
    {
        this(scope);

        this.candidate = isCandidate;
        this.deptChair = isDeptChair;
        this.dean = isDean;
    }


    /**
     * @return the scope
     */
    public Scope getScope()
    {
        return this.scope;
    }

    /**
     * @param scope the scope to set
     * @return self reference for chained invocations
     */
    public AssignmentLine setScope(Scope scope)
    {
        this.scope = new AppointmentScope(scope);
        return this;
    }

    /**
     * @return The description of the School+Department for the scope
     */
    public String getDescription()
    {
        return this.scope == null ? "" : this.scope.getFullDescription();
    }

    /**
     * @return the candidate
     */
    public Boolean getCandidate()
    {
        return this.candidate;
    }

    /**
     * @param isCandidate if is candidate
     * @return self reference for chained invocations
     */
    public AssignmentLine setCandidate(Boolean isCandidate)
    {
        this.candidate = isCandidate;
        return this;
    }


    /**
     * @return the deptChair
     */
    public Boolean getDeptChair()
    {
        return this.deptChair;
    }

    /**
     * @param isDeptChair if is department chair
     * @return self reference for chained invocations
     */
    public AssignmentLine setDeptChair(Boolean isDeptChair)
    {
        this.deptChair = isDeptChair;
        return this;
    }


    /**
     * @return the dean
     */
    public Boolean getDean()
    {
        return this.dean;
    }

    /**
     * @param isDean if is dean
     * @return self reference for chained invocations
     */
    public AssignmentLine setDean(Boolean isDean)
    {
        this.dean = isDean;
        return this;
    }


    /**
     * @return if is read-only
     */
    public boolean isReadonly()
    {
        return this.readOnly;
    }

    /**
     * Mark this line as read-only or editable. Lines default to editable unless marked read-only.
     * @param isReadonly set <code>true</code> if this line should be read-only, <code>false</code> if it should be read-write
     * @return self reference for chained invocations
     */
    public AssignmentLine setReadonly(boolean isReadonly)
    {
        this.readOnly = isReadonly;
        return this;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return "{ Scope=" + scope + "  candidate:" + candidate + " chair:" + deptChair + " dean:" + dean + " }";
    }
}
