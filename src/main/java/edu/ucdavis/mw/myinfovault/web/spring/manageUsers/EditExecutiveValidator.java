/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditExecutiveValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.text.MessageFormat;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Validates the manage executive form.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.2
 */
public class EditExecutiveValidator implements Validator
{
    private static final String ERROR_REQUIRED = "{0}: This field is required";
    private static final String ERROR_ACTIVE = "{0}: User {1} is not active";
    private static final String ERROR_CANDIDATE = "{0}: User {1} is not a candidate";

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return EditExecutiveForm.class.isAssignableFrom(clazz);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object obj, Errors errors)
    {
        EditExecutiveForm form = (EditExecutiveForm) obj;

        validate(MivRole.VICE_PROVOST, form.getViceProvost(), errors);
        validate(MivRole.PROVOST, form.getProvost(), errors);
        validate(MivRole.CHANCELLOR, form.getChancellor(), errors);
    }

    /**
     * Validates the proposed executive.
     *
     * @param role executive role
     * @param proposedExecutive proposed executive
     * @param errors contextual state about the validation process
     */
    private void validate(MivRole role,
                          MivPerson proposedExecutive,
                          Errors errors)
    {
        // verify a proposed executive has been selected for this role
        if (proposedExecutive == null)
        {
            errors.reject(null, MessageFormat.format(ERROR_REQUIRED, role.getDescription()));
        }
        // verify proposed executive is active
        else
        {
            if (!proposedExecutive.isActive())
            {
                errors.reject(null, MessageFormat.format(ERROR_ACTIVE,
                                                         role.getDescription(),
                                                         proposedExecutive.getDisplayName()));
            }

            // verify proposed executive is a candidate
            if (!proposedExecutive.hasRole(MivRole.CANDIDATE))
            {
                errors.reject(null, MessageFormat.format(ERROR_CANDIDATE,
                                                         role.getDescription(),
                                                         proposedExecutive.getDisplayName()));
            }
        }
    }
}
