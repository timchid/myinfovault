/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketRequestForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.packetrequest;

import java.io.Serializable;
import java.util.Date;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * Form backing for packet request.
 *
 * @since MIV 5.0
 */
public class PacketRequestForm implements Serializable
{
    private static final long serialVersionUID = 20150408155545L;

    private Dossier dossier;
    private final MivPerson realPerson;
    private final MivPerson targetPerson;
    private final String emailSubject;
    private final String emailMessage;
    private String emailTo = StringUtil.EMPTY_STRING;
    private String emailCc = StringUtil.EMPTY_STRING;
    private String emailBody = StringUtil.EMPTY_STRING;
    private Date emailSent = null;
    private boolean skipEmailNotification = false;

    /**
     * Create the Packet Request form backing object.
     *
     * @param Dossier for which to issue the packet request
     * @param realPerson The logged-in MIV administrator acting on the disclosure certificate
     * @param targetPerson MIV target person effecting changes
     * @param emailSubject
     * @param emailMessage
     */
    PacketRequestForm(Dossier dossier,
           MivPerson realPerson,
           MivPerson targetPerson,
           String emailSubject,
           String emailMessage)
    {
        this.dossier = dossier;
        this.realPerson = realPerson;
        this.targetPerson = targetPerson;
        this.emailSubject = emailSubject;
        this.emailMessage = emailMessage;
    }

    /**
     * @return Dossier
     */
    public Dossier getDossier()
    {
        return dossier;
    }

    /**
     * @return real, logged-in person effecting change
     */
    public MivPerson getRealPerson()
    {
        return realPerson;
    }

    /**
     * @return person being affected by change
     */
    public MivPerson getTargetPerson()
    {
        return targetPerson;
    }

    /**
     * @return the date the email message was sent
     */
    public Date getEmailSent()
    {
        return emailSent;
    }

    /**
     * @param emailSent the date the email message was sent
     */
    public void setEmailSent(Date emailSent)
    {
        this.emailSent = emailSent;
    }

    /**
     * @return the email message text
     */
    public String getEmailMessage()
    {
        return emailMessage;
    }

    /**
     * @return to whom the email is sent unless overridden
     */
    public String getEmailToDefault()
    {
        return dossier.getAction().getCandidate().getDisplayName() + " <" + dossier.getAction().getCandidate().getPreferredEmail() + ">";
    }

    /**
     * @return to whom the email is sent
     */
    public String getEmailTo()
    {
        return StringUtils.hasText(emailTo) ? emailTo : getEmailToDefault();
    }

    /**
     * @param emailTo to whom the email is sent
     */
    public void setEmailTo(String emailTo)
    {
        this.emailTo = emailTo;
    }

    /**
     * @return to whom the email is additionally sent
     */
    public String getEmailCc()
    {
        return emailCc;
    }

    /**
     * @param emailCc to whom the email is additionally sent
     */
    public void setEmailCc(String emailCc)
    {
        this.emailCc = emailCc;
    }

    /**
     * @return text added to the email message
     */
    public String getEmailBody()
    {
        return emailBody;
    }

    /**
     *
     * @param emailBody text added to the email message
     */
    public void setEmailBody(String emailBody)
    {
        this.emailBody = emailBody;
    }

    /**
     * @return from whom the email is sent
     */
    public String getEmailFrom()
    {
        return targetPerson.getDisplayName() + " <" + targetPerson.getPreferredEmail() + ">";
    }

    /**
     * @return the subject of the email
     */
    public String getEmailSubject()
    {
        return emailSubject;
    }

    /**
     * Ignore, may not change.
     *
     * @param emailMessage
     */
    public void setEmailMessage(String emailMessage){}

    /**
     * Ignore, may not change.
     *
     * @param emailFrom
     */
    public void setEmailFrom(String emailFrom){}

    /**
     * Ignore, may not change.
     *
     * @param emailSubject
     */
    public void setEmailSubject(String emailSubject){}


    /**
     * Indicate email notification has been skipped
     */
    public void setSkipEmailNotification()
    {
        this.skipEmailNotification = true;
    }


}
