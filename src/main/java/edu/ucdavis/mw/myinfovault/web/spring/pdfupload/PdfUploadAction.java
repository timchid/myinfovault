/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PdfUploadAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.pdfupload;

import java.beans.PropertyEditorSupport;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.DataBinder;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.events2.DossierUploadEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PDFConcatenator;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * This class is the form controller for the PdfUpload webflow.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class PdfUploadAction extends MivFormAction
{
    private static Logger log = Logger.getLogger("MIV");
    private static Map<String, Map<String,String>> documentMap = MIVConfig.getConfig().getMap("documentsbyattributename");
    private DossierDao dossierDao;

    /**
     * Create PDF upload form action bean.
     *
     * @param userService Spring-injected user service
     * @param authorizationService Spring-injected authorization service
     * @param dossierDao Spring-injected dossier DAO
     */
    public PdfUploadAction(UserService userService,
                           AuthorizationService authorizationService,
                           DossierDao dossierDao)
    {
        super(userService, authorizationService);

        this.dossierDao = dossierDao;
    }

    /**
     * Is the apparent user authorized to upload a PDF of the requested dossier attribute.
     *
     * @param context Web flow request context
     * @return Web flow event status; denied or allowed
     */
    public Event isAuthorized(RequestContext context)
    {
        String documentAttributeName = context.getFlowScope().getString("documentAttributeName", StringUtils.EMPTY);

        // If this is an appeal request, check permissions
        if (documentAttributeName != null && documentAttributeName.contains("appeal_request"))
        {
            Dossier dossier = (Dossier) context.getConversationScope().get("dossier");

            AttributeSet scope = new AttributeSet();
            scope.put(Qualifier.DEPARTMENT, Integer.toString(dossier.getPrimaryDepartmentId()));
            scope.put(Qualifier.SCHOOL, Integer.toString(dossier.getPrimarySchoolId()));

            if (!authorizationService.isAuthorized(getShadowPerson(context), Permission.INITIATE_APPEAL,null, scope)) return denied();
        }

        return allowed();
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Object createFormObject(RequestContext context) throws Exception
    {
        MutableAttributeMap<Object> flowScope = context.getFlowScope();

        Dossier dossier = (Dossier) context.getConversationScope().get("dossier");

        String documentAttributeName = flowScope.getString("documentAttributeName", StringUtils.EMPTY);

        int schoolId = 0;
        try
        {
            schoolId = flowScope.getInteger("schoolId", 0);
        }
        catch (NullPointerException e)
        {
            // we already set the default to 0
        }

        int departmentId = 0;
        try
        {
            departmentId = flowScope.getInteger("departmentId", 0);
        }
        catch (NullPointerException e)
        {
            // we already set the default to 0
        }

        boolean isYearField = false;
        try
        {
            isYearField = flowScope.getBoolean("isYearField", false);
        }
        catch (NullPointerException e)
        {
            // we already set the default to false
        }

    	Map<String,String> documentTypeMap = documentMap.get(documentAttributeName);

    	// No upload for an unknown dossier attribute type
        if (documentTypeMap == null)
        {
            throw new MivSevereApplicationError("errors.pdfupload.dossierAttribute", documentAttributeName);
        }

        // If there is an alternate description, use it
        String uploadDescription = documentTypeMap.get("alternatedescription") == null
                                 ? documentTypeMap.get("description")
                                 : documentTypeMap.get("alternatedescription");

        // Get the redactable flag
        boolean isRedactable = "1".equalsIgnoreCase(documentTypeMap.get("allowredaction"));

        // Get the uploadFirst flag
        boolean uploadFirst = "1".equalsIgnoreCase(documentTypeMap.get("uploadfirst"));

        // Get the max uploads flag
        int maxUploads = documentTypeMap.get("maxuploads") == null
                         ? 1// default is one
                         : Math.max(Integer.parseInt(documentTypeMap.get("maxuploads")), 0);// otherwise, at least zero

        // Get the document type
        int documentId = documentTypeMap.get("id") == null ? 0 : Integer.parseInt(documentTypeMap.get("id"));

        // Get any additional text to display on the upload page for this documentAttributeName to be placed in the flow scope
        String addtionalText = MIVConfig.getConfig().getProperty("upload-additionaltext-"+documentAttributeName);
        ArrayList<String> additionalTextList = new ArrayList<String>();
        if (addtionalText != null)
        {
            String [] addtionalTextArr = addtionalText.split(",");
            if (addtionalTextArr != null)
            {
                for (String additionalString : addtionalTextArr)
                {
                    additionalTextList.add(MIVConfig.getConfig().getProperty("upload-string-"+additionalString));
                }
            }
        }

        String formhelp = MIVConfig.getConfig().getProperty(documentAttributeName+"-string-formhelp");
        String returnUrl = MIVConfig.getConfig().getProperty(documentAttributeName+"-config-returnurl");

        flowScope.put("formhelp", formhelp);
        flowScope.put("returnurl", returnUrl);
        flowScope.put("dossierFileName", documentTypeMap.get("dossierfilename"));
        flowScope.put("additionalTextList", additionalTextList);
        flowScope.put("documentId", documentId);
        flowScope.put("uploadDescription", uploadDescription);
        flowScope.put("isRedactable", isRedactable);
        flowScope.put("uploadFirst", uploadFirst);
        flowScope.put("displayName", getShadowPerson(context).getDisplayName());

        // If we are uploading for a dossier, get the display name for the dossier owner
        if (dossier != null)
        {
            flowScope.put("displayName", dossier.getAction().getCandidate().getDisplayName());
        }

        flowScope.put("documentAttributeName", documentAttributeName);
        flowScope.put("returnTo", getReturnToLink(context));

        return new PdfUploadForm(dossier == null ? 0L : dossier.getDossierId(),
                                 documentId,
                                 schoolId,
                                 departmentId,
                                 isRedactable,
                                 maxUploads,
                                 isYearField);
    }

    /**
     * To get the return link
     * Date entry PDF upload applications have there return links into mivconfig.properties file.
     * @param context
     * @return
     */
    private String getReturnToLink(RequestContext context)
    {
    	// Get the page we are to return to from the request and place in the flowscope
        HttpServletRequest request = this.getRequest(context);
        
        String documentAttributeName = context.getFlowScope().getString("documentAttributeName", StringUtils.EMPTY);
        if (documentAttributeName.isEmpty()) {
            documentAttributeName = request.getParameter("documentAttributeName");
        }
        String returnUrl = MIVConfig.getConfig().getProperty(documentAttributeName+"-config-returnurl");

        String returnTo = "";

        if(StringUtils.isNotEmpty(returnUrl))
        {
        	returnTo = returnUrl;
        }else
        {
        	returnTo = (String) request.getSession().getAttribute("returnTo");
        	// If no return to value is present, get the referer
            if (returnTo == null)
            {
                returnTo = request.getHeader("referer");
                // If no referer, go back to main
                if (returnTo == null)
                {
                    returnTo = "/MIVMain";
                }
            }
        }

        return returnTo;
    }

    /**
     * Set the view to which to return after upload, cancel, or most recent bread crumb selection.
     *
     * @param context WebFlow request context
     * @return WebFlow event status; success
     */
    public Event setReturn(RequestContext context)
    {
    	MutableAttributeMap<Object> flowScope = context.getFlowScope();
        flowScope.put("breadcrumb1", getReturnToLink(context));
        return success();
    }

    /**
     * Do the file upload and update the userUploadDocument table.
     *
     * @param context
     * @return WebFlow event status; success or error
     */
    public Event doUpload(RequestContext context)
    {
        Dossier dossier = (Dossier) context.getConversationScope().get("dossier");

        PdfUploadForm pdfUploadForm =  (PdfUploadForm)this.getFormObject(context);
        CommonsMultipartFile commonsMultipartFile = pdfUploadForm.getUploadFileName();
        String uploadDescription = context.getFlowScope().getString("uploadDescription");
        boolean isRedacted = pdfUploadForm.isRedactable() && pdfUploadForm.getLetterType().equalsIgnoreCase("redacted");

        // Get the output file
        File outputFile = this.getOutputFile(dossier, commonsMultipartFile, isRedacted, context);
        if (outputFile == null)
        {
            return error();
        }

        MivPerson realPerson = getRealPerson(context);
        MivPerson shadowPerson = this.getShadowPerson(context);

        // Do the upload
        try
        {
            log.info("User " + realPerson.getUserId() + " uploading file \"" + commonsMultipartFile.getOriginalFilename() +
                    "\" to " + outputFile.toString() +
                    " for " + (dossier == null ? "user "+shadowPerson.getUserId(): "dossier "+dossier.getDossierId()) );

            commonsMultipartFile.transferTo(outputFile);

            // Add a book mark to the uploaded pdf and remove any which may exist
            if (!PDFConcatenator.addBookmark(outputFile,uploadDescription, true))
            {
                // Log if the bookmark could not be added
                String msg = "Update of bookmark for uploaded PDF failed for  File \"" + commonsMultipartFile.getOriginalFilename();
                log.log(Level.WARNING, msg);
            }
        }
        catch (IOException e)
        {
            throw new MivSevereApplicationError("errors.pdfupload.uploadfailed", e, outputFile);
        }

        // Update the UserUploadDocumentTable
        UploadDocumentDto uploadDocument = new UploadDocumentDto(outputFile);
        uploadDocument.setUserId(shadowPerson.getUserId());
        uploadDocument.setUpdateUserId(realPerson.getUserId());
        uploadDocument.setDocumentId(context.getFlowScope().getInteger("documentId"));

        // Default to the upload description for this document type
        uploadDocument.setUploadName(uploadDescription);

        // If a custom description has been entered, use it
        if (StringUtils.isNotBlank(pdfUploadForm.getUploadFileDescription()))
        {
            uploadDocument.setUploadName(pdfUploadForm.getUploadFileDescription());
        }

        uploadDocument.setRedacted(isRedacted);

    	if (pdfUploadForm.isYearField())
    	{
    	    uploadDocument.setYear(pdfUploadForm.getYear());
    	}
    	else
    	{
    		uploadDocument.setYear(Calendar.getInstance().get(Calendar.YEAR)+ "");
    	}

        // dossierId is will not be present for uploads done prior to the dossier being submitted to department
        uploadDocument.setDossierId((int)pdfUploadForm.getDossierId());
        uploadDocument.setSchoolId(pdfUploadForm.getSchoolId());
        uploadDocument.setDepartmentId(pdfUploadForm.getDepartmantId());

        uploadDocument.setUploadFirst(context.getFlowScope().getBoolean("uploadFirst", false));

        try
        {
            // If there is a dossier id, get the dossier location, school and department
            if (uploadDocument.getDossierId()> 0L)
            {
                uploadDocument.setUploadLocation(dossier.getDossierLocation());
                uploadDocument.setUserId(dossier.getAction().getCandidate().getUserId());

                // Update the dossier and the userUploadDocument table.
                try
                {
                    this.dossierDao.updateDossierAndUserUploadDocument(dossier, uploadDocument);
                    EventDispatcher2.getDispatcher().post( new DossierUploadEvent(
                            realPerson, dossier, uploadDocument).setShadowPerson(shadowPerson));
                }
                catch (SQLException e)
                {
                    // Delete the uploaded file when the database update fails
                    outputFile.delete();
                    throw new MivSevereApplicationError("errors.pdfupload.uploadfailedUpdatingUserUploadDocument", e, uploadDocument);
                }
            }
            // No dossier associated with this upload, update the upload table only
            else
            {
                this.dossierDao.insertUserUploadDocument(uploadDocument);
            }

        }
        catch (WorkflowException e)
        {
            // Delete the uploaded file when the database update fails
            outputFile.delete();
            throw new MivSevereApplicationError("errors.pdfupload.uploadfailedUpdatingUserUploadDocument", e, uploadDocument);
        }

        return success();
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#initBinder(org.springframework.webflow.execution.RequestContext, org.springframework.validation.DataBinder)
     * To be able to convert Multipart instance to CommonsMultipartFile we have to register a custom editor.
     */
    @Override
    protected void initBinder(RequestContext context, DataBinder binder)
    {
        binder.registerCustomEditor(CommonsMultipartFile.class, new PropertyEditorSupport());
        super.initBinder(context, binder);
    }


    /** Used to construct the output file name */
    private static final ThreadLocal<DateFormat> extensionFormat =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("yyyyMMdd_hhmmssSS");
            }
        };


    /**
     * Get the output file path to write the upload.
     *
     * @param dossier
     * @param commonsMultipartFile
     * @return output file to write the uploaded file
     */
    private File getOutputFile(Dossier dossier, CommonsMultipartFile commonsMultipartFile, boolean isRedacted, RequestContext context)
    {
        File outputDirectory = null;
        File outputFile = null;
        MivPerson shadowPerson = getShadowPerson(context);

        // If there is a dossier for this upload, get the output directory for the submitted dossier
        if (dossier != null)
        {
            outputDirectory = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);
        }
        if (outputDirectory == null)
        {
            // Get the relative location of the dossier directory
            outputDirectory = MIVConfig.getConfig().getDossierPdfLocation(shadowPerson.getUserId());
        }

        // Now get the file outputFile name
        try
        {
            // Use the dossier file name for the output file if we have it
            String fileName = context.getFlowScope().getString("dossierFileName");
            if (fileName == null)
            {
                fileName = commonsMultipartFile.getOriginalFilename();
            }

            // get the original file name without the extension
            int lastPosition = fileName.lastIndexOf(".");
            if (lastPosition >= 0 )
            {
                fileName = fileName.substring(0,lastPosition);
            }

            StringBuilder outputFileName = new StringBuilder(fileName);

            outputFileName.append(isRedacted ? "_REDACTED_" : "_")
                .append(extensionFormat.get().format(Calendar.getInstance().getTimeInMillis()))
                .append(".").append(DocumentFormat.PDF.getFileExtension());

            outputFile = new File(outputDirectory.getAbsolutePath(),outputFileName.toString());
            if (!outputDirectory.exists())
            {
                outputDirectory.mkdirs();
            }
            outputFile.createNewFile();
        }
        catch (IOException e)
        {
            throw new MivSevereApplicationError("errors.pdfupload.uploadfailedCreatingTemporaryFile", e, outputFile);
        }

        return outputFile;
    }
}
