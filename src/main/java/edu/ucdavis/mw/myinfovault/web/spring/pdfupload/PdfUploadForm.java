/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PdfUploadForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.pdfupload;

import java.io.Serializable;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import edu.ucdavis.myinfovault.data.MIVUtil;

/**
 * Form backing for the {@link PdfUploadAction}.
 * 
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class PdfUploadForm implements Serializable{

    private static final long serialVersionUID = 2801628618739998739L;

    private CommonsMultipartFile uploadFileName = null;
    private String uploadFileDescription = null;
    private String year = null;
    private String letterType  = null;

    private final Boolean redactable;
    private final int maxUploads;
    private final long dossierId;
    private final int documentId;
    private final int schoolId;
    private final int departmentId;
    private final boolean yearField;

    /**
     * Create PDF upload form bean.
     *
     * @param dossierId
     * @param documentId
     * @param schoolId
     * @param departmentId
     * @param redactable
     * @param maxUploads
     * @param yearField
     */
    public PdfUploadForm(long dossierId,
                         int documentId,
                         int schoolId,
                         int departmentId,
                         boolean redactable,
                         int maxUploads,
                         boolean yearField)
    {
        this.redactable = redactable;
        this.maxUploads = maxUploads;
        this.dossierId = dossierId;
        this.documentId = documentId;
        this.schoolId = schoolId;
        this.departmentId = departmentId;
        this.yearField = yearField;
    }

    /**
     * @return uploaded file
     */
    public CommonsMultipartFile getUploadFileName()
    {
        return uploadFileName;
    }

    /**
     * @param uploadFileName uploaded file
     */
    public void setUploadFileName(CommonsMultipartFile uploadFileName)
    {
        this.uploadFileName = uploadFileName;
    }

    /**
     * @return uploaded file description
     */
    public String getUploadFileDescription()
    {
        return uploadFileDescription;
    }

    /**
     * @param uploadFileDescription uploaded file description
     */
    public void setUploadFileDescription(String uploadFileDescription)
    {
        this.uploadFileDescription = MIVUtil.escapeMarkup(uploadFileDescription);
    }
    
    /**
     * @param year uploaded year
     */
    public void setYear(String year)
    {
        this.year = year;
    }

    /**
     * @return uploaded year
     */
    public String getYear()
    {
        return this.year;
    }

    /**
     * @param letterType letter type; "redacted" or otherwise
     */
    public void setLetterType(String letterType)
    {
        this.letterType = letterType;
    }

    /**
     * @return letter type; "redacted" or otherwise
     */
    public String getLetterType()
    {
        return this.letterType;
    }

    /**
     * @return if able to be redacted
     */
    public Boolean isRedactable()
    {
        return this.redactable;
    }

    /**
     * @return maximum number of uploads for the document type
     */
    public int getMaxUploads()
    {
        return this.maxUploads;
    }

    /**
     * @return ID of dossier associated with the upload
     */
    public long getDossierId()
    {
        return this.dossierId;
    }

    /**
     * @return document type ID
     */
    public int getDocumentId()
    {
        return this.documentId;
    }

    /**
     * @return ID of the school for the upload
     */
    public int getSchoolId()
    {
        return schoolId;
    }

    /**
     * @return ID of the department for the upload
     */
    public int getDepartmantId()
    {
        return departmentId;
    }

	/**
	 * @return if the year field has been given a value
	 */
	public boolean isYearField()
	{
		return yearField;
	}
}
