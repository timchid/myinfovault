/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PostAuditDossiersAction.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.postauditdossiers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.events2.DossierRouteEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode;
import edu.ucdavis.mw.myinfovault.web.spring.search.DossierAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.MivActionList;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Spring WebFlow form action for send dossiers to post audit.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.7
 */
public class PostAuditDossiersAction extends DossierAction
{
    /**
    *
    * @param searchStrategy
    * @param authorizationService
    * @param userService
    * @param dossierService
    */
   public PostAuditDossiersAction(SearchStrategy searchStrategy,
                                AuthorizationService authorizationService,
                                UserService userService,
                                DossierService dossierService)
   {
       super(searchStrategy,
             authorizationService,
             userService,
             dossierService);
   }

   /**
    * Checks if user is authorized to route the dossiers  from "Ready for Post Review" to "Post Audit Review" location.
    * Only roles VICE_PROVOST_STAFF is allowed to route dossiers.
    *
    * @param context Request context from webflow
    * @return WebFlow event status, allowed or denied
    */
   public Event isAuthorized(RequestContext context)
   {
       MivPerson shadowPerson = getShadowPerson(context);

       if (shadowPerson.hasRole(MivRole.VICE_PROVOST_STAFF))
       {
           logger.info("(Old log) User "+shadowPerson.getDisplayName()+" ("+shadowPerson.getPersonId()+") is AUTHORIZED to route dossiers to Post Audit Review location.");
           logger.info("(New log) User {} is AUTHORIZED to route dossiers to Post Audit Review location.", shadowPerson.getLoggingName());
           return allowed();
       }

       logger.info("User "+shadowPerson.getDisplayName()+" ("+shadowPerson.getPersonId()+") is ** NOT ** AUTHORIZED to route dossiers to Post Audit Review location.");
       return denied();
   }

   /**
    * Get the Dossiers that this user is allowed to view.
    *
    * @param user
    * @return dossiers in an action list
    */
   public List<MivActionList> getDossiers(MIVUser user)
   {
       return this.searchStrategy.getDossiersAtLocation(user, DossierLocation.READYFORPOSTREVIEWAUDIT);
   }

   /**
    * Get the Dossiers to be Post Audit Review.
    *
    * @param context RequestContext
    * @return WebFlow event status
    */
   public Event getDossiersToPostAuditReview(RequestContext context)
   {
	   PostAuditDossiersForm postAuditDossiersForm = (PostAuditDossiersForm) this.getFormObject(context);

       Map<String, String> dossiersToPostAuditReviewMap = new HashMap<String, String>();

       List<String> dossiers = postAuditDossiersForm.getToPostAuditReview();

       // Iterate through the dossiers
       for (String dossierId : dossiers)
       {
           String fullName = null;
           try
           {
               Dossier dossier = this.dossierService.getDossier(dossierId);
               MivPerson mivPerson = dossier.getAction().getCandidate();
               String actionType = dossier.getAction().getActionType().getDescription();
               String delegationAuthority = dossier.getAction().getDelegationAuthority().getDescription(dossier.getReviewType());

               String schoolName = mivPerson.getPrimaryAppointment().getScope().getSchoolDescription();
               String departmentName = mivPerson.getPrimaryAppointment().getScope().getDepartmentDescription();
               String schoolDepartment = schoolName+(StringUtils.isBlank(departmentName) ? "" : " - "+departmentName);
               fullName = (mivPerson.getSurname() + ", " + mivPerson.getGivenName() + " " + mivPerson.getMiddleName()).trim();
               dossiersToPostAuditReviewMap.put(fullName, schoolDepartment+"; <strong>"+delegationAuthority+" - "+actionType+"</strong>");
           }
           catch (WorkflowException wfe)
           {
               logger.error("Unable to retrieve dossier " + dossierId + " to Post Audit Review for " + fullName, wfe);
           }
       }

       // Sort the list of dossiers to Post Audit Review into alphabetic order by last name
       context.getFlashScope().put("dossiersToPostAuditReviewMap", new TreeMap<String, String>(dossiersToPostAuditReviewMap));

       return success();
   }

   /**
    * Return a dossier.
    *
    * @param context RequestContext
    * @return WebFlow event status, success or error
    */
   public Event returnDossier(RequestContext context)
   {
       MutableAttributeMap<Object> flowScope = context.getFlowScope();
       MutableAttributeMap<Object> conversationScope  = context.getConversationScope();

       Integer dossierId = context.getExternalContext().getRequestParameterMap().getInteger("dossierId");
       MivPerson dossierOwner = null;
       MivPerson dossierRouter = null;

       if ((conversationScope.getInteger("dossierReturned") != null))
       {
           logger.info("++++ Duplicate requests to return dossiers while return is already in process for dossier "+conversationScope.get("dossierId")+" for "+conversationScope.get("candidate"));
           flowScope.put("returnInProgress",conversationScope.get("dossierId"));
           flowScope.put("previousLocation", conversationScope.get("previousLocation"));
           flowScope.put("candidate", conversationScope.get("candidate"));
           flowScope.put("department", conversationScope.get("department"));
           flowScope.put("action", conversationScope.get("action"));
           return success();
       }

       try
       {
           Dossier dossier = this.dossierService.getDossier(dossierId);
           dossierOwner = dossier.getAction().getCandidate();
           dossierRouter = getShadowPerson(context);
           String actionType = dossier.getAction().getActionType().getDescription();

           String schoolName = dossierOwner.getPrimaryAppointment().getScope().getSchoolDescription();
           String departmentName = dossierOwner.getPrimaryAppointment().getScope().getDepartmentDescription();
           String schoolDepartment = schoolName+(StringUtils.isBlank(departmentName) ? "" : " - "+departmentName);
           String middleName = StringUtils.isBlank(dossierOwner.getMiddleName()) ? " " : " "+dossierOwner.getMiddleName()+" ";
           String fullName = dossierOwner.getGivenName()+middleName+dossierOwner.getSurname();

           WorkflowNode previousWorkflowNode = dossierService.getPreviousWorkflowNode(dossier);
           WorkflowNode currentWorkflowNode = dossierService.getCurrentWorkflowNode(dossier);
           String action = dossier.getAction().getDelegationAuthority().getDescription(dossier.getReviewType())+" - "+actionType;
//           DossierLocation location = DossierLocation.mapWorkflowNodeNameToLocation(previousWorkflowLocation);

           flowScope.put("previousLocation", previousWorkflowNode.getWorkflowNodeDesc());
           flowScope.put("candidate", fullName);
           flowScope.put("department", schoolDepartment);
           flowScope.put("action", action);

           // Dossier has already been returned from READYTOARCHIVE
           if (!(DossierLocation.READYFORPOSTREVIEWAUDIT == currentWorkflowNode.getNodeLocation()))
           {
               flowScope.put("previousLocation", currentWorkflowNode.getWorkflowNodeName());
               logger.info("++++ Duplicate request to return dossier "+dossierId+" for "+fullName);
               return success();
           }

           String msg = "Returning dossier "+dossierId+" for "+fullName+" from "+currentWorkflowNode.getWorkflowNodeName()+" to "+previousWorkflowNode.getWorkflowNodeName()+".";
           dossierService.returnDossierToPreviousNode(dossierRouter, getRealPerson(context), dossierId, msg, previousWorkflowNode.getNodeLocation(), currentWorkflowNode.getNodeLocation());

           // Set variables in the conversation scope for the dossier which has been returned
           // These will be checked to prevent a double submit
           conversationScope.put("dossierReturnedEvent", success());
           conversationScope.put("dossierReturned", dossierId);
           conversationScope.put("previousLocation", previousWorkflowNode.getWorkflowNodeDesc());
           conversationScope.put("candidate", fullName);
           conversationScope.put("department", schoolDepartment);
           conversationScope.put("action", action);

           logger.info(msg);
       }
       catch (WorkflowException wfe)
       {
           String message = "Unable to return dossier " + dossierId + " for " + dossierOwner.getDisplayName()+"("+dossierOwner.getPersonId()+")";
           flowScope.put("confirmationError", message);

           // Set the dossier has been returned in the conversation scope along with Event result'
           conversationScope.put("dossierReturnedEvent", error());
           conversationScope.put("dossierReturned", dossierId);
           conversationScope.put("confirmationError", message);

           logger.error(message, wfe);
           return error();
       }

       return success();
   }

   /**
    * Route dossiers to Post Audit Review.
    *
    * @param context Webflow request context
    * @return WebFlow event status, success or error
    */
   public Event routeDossiersToPostAuditReview(RequestContext context)
   {
       MivPerson shadowPerson = getShadowPerson(context);
       MivPerson realPerson = this.getRealPerson(context);

       MutableAttributeMap<Object> flashScope = context.getFlashScope();

       PostAuditDossiersForm postAuditDossiersForm = (PostAuditDossiersForm) this.getFormObject(context);
       Map<String, String> dossiersToPostAuditReviewMap = new HashMap<String, String>();

       List<String> dossiers = postAuditDossiersForm.getToPostAuditReview();

       logger.info("_AUDIT : User {}{} has route the Dossiers: {} to Post Audit Review location",
                       new Object[] {
                           realPerson.getUserId(),
                           (shadowPerson.getUserId() != realPerson.getUserId() ?
                                   ", acting as " + shadowPerson.getUserId() + "," : ""),
                           dossiers.toString()
                       }
                   );

       // Iterate through the dossiers
       for (String dossierId : dossiers)
       {
           String fullName = null;
           try
           {
               Dossier dossier = this.dossierService.getDossier(dossierId);
               MivPerson mivPerson = dossier.getAction().getCandidate();
               String actionType = dossier.getAction().getActionType().getDescription();
               String delegationAuthority = dossier.getAction().getDelegationAuthority().getDescription(dossier.getReviewType());

               String schoolName = mivPerson.getPrimaryAppointment().getScope().getSchoolDescription();
               String departmentName = mivPerson.getPrimaryAppointment().getScope().getDepartmentDescription();
               String schoolDepartment = schoolName+(StringUtils.isBlank(departmentName) ? "" : " - "+departmentName);
               fullName = mivPerson.getSurname() + ", " + mivPerson.getGivenName() + " " + mivPerson.getMiddleName();

               String resultMessage = schoolDepartment+"; <strong>"+delegationAuthority+" - "+actionType+"</strong>";

               // Set the routing principal for this dossier
               dossier.setRoutingPerson(shadowPerson);

               // Archive the dossier
//               String errorMessage = dossierArchiveService.archiveDossier(dossier);

               //dossierService.approveDossier(dossier, "Routed to Post Audit Review", DossierLocation.POSTAUDITREVIEW, dossierService.getCurrentWorkflowNode(dossier).getNodeLocation());
               dossierService.approveDossier(dossier, "Routed to Post Audit Review", DossierLocation.POSTAUDITREVIEW, DossierLocation.READYFORPOSTREVIEWAUDIT);

               EventDispatcher2.getDispatcher().post( new DossierRouteEvent(
                       realPerson,
                       dossier,
                       DossierLocation.POSTAUDITREVIEW,
                       DossierLocation.READYFORPOSTREVIEWAUDIT).setShadowPerson(shadowPerson));


               //this.dossierService.routeDossier(dossier, annotation, targetLocation, currentLocationForValidation)

               // Check for an error message returned
//               if (errorMessage != null)
//               {
//                   resultMessage = "<strong>ERROR</strong> - "+errorMessage;
//               }
               dossiersToPostAuditReviewMap.put(fullName, resultMessage);
           }
           catch (WorkflowException wfe)
           {
               String msg = "Workflow exception retrieving dossier " + dossierId + " to route to Post Audit Review.";
               dossiersToPostAuditReviewMap.put(fullName, msg);
               logger.error(msg, wfe);
           }
       }

       // Clear the dossiersToArchive from the form
       postAuditDossiersForm.clearToPostAuditReview();

       // Sort the list of dossiers archived into alphabetic order by last name
       flashScope.put("dossiersToPostAuditReviewMap", new TreeMap<String, String>(dossiersToPostAuditReviewMap));

       return success();
   }


}
