/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DownloadAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.publications;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;

import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.publications.PublicationService;
import edu.ucdavis.mw.myinfovault.util.CollectionTransform;
import edu.ucdavis.mw.myinfovault.util.XmlManager;
import edu.ucdavis.mw.myinfovault.util.XpathUtil;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Webflow form action for the upload flow.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class DownloadAction extends MivFormAction
{
    private static Logger logger = LoggerFactory.getLogger(DownloadAction.class);

    //number of records to fetch per batch for the PubMed download
    private static final int BATCH_SIZE = 500;

    //PubMed eSearch and eFetch URLs
    private static final String NIH_EUTILS_URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils";

    // The 'author' [au] qualifier limits search to this one field, giving a huge speed increase
    private static final MessageFormat ESEARCH_URL_FORM = new MessageFormat(NIH_EUTILS_URL +
                                                                            "/esearch.fcgi?db=pubmed&usehistory=y&datetype=pdat&sort=pub+date" +
                                                                            "&retmax=50000" +
                                                                            "&term={0}[au]" +
                                                                            "&mindate={1,number,#}" +
                                                                            "&maxdate={2,number,#}");

    private static final MessageFormat EFETCH_URL_FORM = new MessageFormat(NIH_EUTILS_URL +
                                                                           "/efetch.fcgi?db=pubmed&rettype=xml&retmode=text" +
                                                                           "&query_key={0}" +
                                                                           "&WebEnv={1}" +
                                                                           "&retstart={2,number,#}" +
                                                                           "&retmax={3,number,#}");

    //default PubMed directory path if not among configuration properties
    private static final String PUBMED_DEFAULT_PATH = "/ucd/miv/pubmed";

    //XPath expressions for PubMed eSearch XML
    private static final String ESEARCH_ROOT = "/eSearchResult";
    private static final String ESEARCH_COUNT = ESEARCH_ROOT + "/Count/text()";
    private static final String ESEARCH_QUERYKEY = ESEARCH_ROOT + "/QueryKey/text()";
    private static final String ESEARCH_WEBENV = ESEARCH_ROOT + "/WebEnv/text()";
    private static final String ESEARCH_IDS = ESEARCH_ROOT + "/IdList/Id/text()";

    //message to users if the PubMed service is unavailable
    private static final String PUBMED_DOWN_MSG = "The Pubmed EUtils service is currently unavailable, try your request later";

    private PublicationService publicationService;
    private final File pubmedDirectory;

    /**
     * Create the publication download action bean.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     * @param publicationService Spring injected publication service
     */
    public DownloadAction(UserService userService,
                          AuthorizationService authorizationService,
                          PublicationService publicationService)
    {
        super(userService, authorizationService);

        this.publicationService = publicationService;

        //initialize PubMed directory
        String pubmedPath = MIVConfig.getConfig().getProperty("document-config-location-pubmed");
        pubmedDirectory = new File(StringUtils.hasText(pubmedPath) ? pubmedPath : PUBMED_DEFAULT_PATH);
        if (!pubmedDirectory.exists()) pubmedDirectory.mkdirs();
    }


    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    protected Object createFormObject(RequestContext context)
    {
        MivPerson shadowPerson = this.getShadowPerson(context);

        return new DownloadForm(shadowPerson.getSurname(),
                                shadowPerson.getGivenName(),
                                publicationService.getPublications(shadowPerson.getUserId(),
                                                                   PublicationSource.PUBMED));
    }


    /**
     * Searches the PubMed database by author name and publication years.
     *
     * @param context Webflow request context
     * @return Webflow event status
     * @throws Exception
     */
    public Event search(RequestContext context) throws Exception
    {
        DownloadForm form = getFormObject(context);

        Exception exception = null;

        try
        {
            /*
             * Get PubMed search document.
             *
             * MIV-5271: DTD needs to be ignored during parsing.
             */
            Document searchDoc = XmlManager.getDocument(getSearchURL(form.getGivenInitial(),
                                                                     form.getMiddleInitial(),
                                                                     form.getSurname(),
                                                                     Math.min(form.getFromYear(), form.getToYear()),
                                                                     Math.max(form.getFromYear(), form.getToYear())),
                                                        true);
            //store search results from document
            form.setSearchResult(Integer.parseInt(XpathUtil.parseNodeValue(ESEARCH_COUNT,searchDoc)),
                                 XpathUtil.parseNodeValue(ESEARCH_QUERYKEY, searchDoc),
                                 XpathUtil.parseNodeValue(ESEARCH_WEBENV, searchDoc),
                                 CollectionTransform.stringToInteger(XpathUtil.parseNodeValueList(ESEARCH_IDS, searchDoc)));
        }
        catch (XPathExpressionException e)
        {
            throw new MivSevereApplicationError("Failed to evaluate PubMed XML", e);
        }
        catch (NumberFormatException e)
        {
            exception = e;
        }
        catch (SAXException e)
        {
            exception = e;
        }
        catch (IOException e)
        {
            exception = e;
        }
        finally
        {
            if (exception != null)
            {
                logger.warn("Exception caught when searching PubMed articles for " +
                            form.getGivenInitial() + form.getMiddleInitial() + form.getSurname(),
                            exception
                            );

                getFormErrors(context).reject(null, PUBMED_DOWN_MSG);

                return error();
            }
        }

        return success();
    }


    /**
     * Fetches a batch of PubMed records from the search unless the
     * batch was already  fetched previously. Resulting XML is saved
     * to the user's PubMed directory, parsed as publication objects,
     * and bound to the form for use in the view.
     *
     * @param context Webflow request context
     * @return Webflow event status
     * @throws Exception
     */
    public Event fetch(RequestContext context) throws Exception
    {
        DownloadForm form = getFormObject(context);

        /*
         * Fetch next batch of publications if pager needs it
         */
        if (form.getPage().needBatch())
        {
            //each parse result in the form represents one batch
            int batchIndex = form.getPage().getBatchCount() * BATCH_SIZE;

            // Create the XML output file
            File outputFile = createOutputFile(form.getGivenInitial(),
                                               form.getMiddleInitial(),
                                               form.getSurname(),
                                               batchIndex);
            Exception exception = null;

            try
            {
                //write document from URL to output file
                XmlManager.writeXml(getFetchURL(form.getSearchQuerykey(),
                                                form.getSearchWebenv(),
                                                batchIndex),
                                                outputFile);

                //parse set of publications from PubMed XML file and store result in the form
                form.addParseResult(publicationService.parsePublications(outputFile,
                                                                         PublicationSource.PUBMED,
                                                                         getShadowPerson(context).getUserId()));
            }
            catch (XPathExpressionException e)
            {
                throw new MivSevereApplicationError("Failed to evaluate PubMed XML", e);
            }
            catch (SAXException e)
            {
                exception = e;
            }
            catch (IOException e)
            {
                exception = e;
            }
            finally
            {
                if (exception != null)
                {
                    logger.warn("Exception caught when fetching PubMed articles for " +
                                form.getGivenInitial() + form.getMiddleInitial() + form.getSurname(),
                                exception
                                );

                    getFormErrors(context).reject(null, PUBMED_DOWN_MSG);

                    return error();
                }
            }
        }

        return success();
    }


    /**
     * Increment the page of publications.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event next(RequestContext context)
    {
        getFormObject(context).getPage().incrementPage();

        return success();
    }


    /**
     * Decrement the page of publications.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event previous(RequestContext context)
    {
        getFormObject(context).getPage().decrementPage();

        return success();
    }


    //Create a file in the PubMed directory to store PubMed downloaded XML
    private File createOutputFile(final String firstInitial,
                                  final String middleInitial,
                                  final String lastName,
                                  final int batchIndex)
    {
        //create directory and file name for PubMed downloads
        String pubmedFileName = (firstInitial + middleInitial + lastName).replace(" ", "");

        // First see if a folder exists for this person. If not create it
        File pubmedPersonalDirectory = new File(pubmedDirectory, pubmedFileName);
        if (!pubmedPersonalDirectory.exists()) pubmedPersonalDirectory.mkdir();

// Added to pre-create the file as a test.
// This would replace the signle-line "return new File(...)" if you use it.
/*
        // Create the XML output file for the current batch and return the File object
        File outfile = new File(pubmedPersonalDirectory, pubmedFileName + batchIndex +".xml");
        if (! outfile.exists()) {
            boolean created = false;
            try {
                created = outfile.createNewFile();
            }
            catch (IOException e) {
                // created = false ???
                throw new MivSevereApplicationError("Failed to create file {0}, 'created'={1}", e, outfile, created);
            }
            if (!created) {
                System.out.println("XmlManager.writeXml: file [" + outfile.getPath() + "] already existed.");
            }

        }

        return outfile;
*/
        // Create the XML output file [object] for the current batch and return it
        return new File(pubmedPersonalDirectory, pubmedFileName + batchIndex + ".xml");
    }


    //Build and return the PubMed search URL for the given qualifiers
    private URL getSearchURL(final String givenInitial,
                             final String middleInitial,
                             final String surname,
                             final int minYear,
                             final int maxYear) throws IOException
    {
        return getURL(ESEARCH_URL_FORM,
                      URLEncoder.encode(surname + " " + givenInitial + middleInitial, "UTF-8"),
                      minYear,
                      maxYear);
    }


    //Build and return the PubMed fetch URL for the generated response keys from the PubMed search
    private URL getFetchURL(final String querykey,
                            final String webenv,
                            final int retstart) throws MalformedURLException
    {
        return getURL(EFETCH_URL_FORM,
                      querykey,
                      webenv,
                      retstart,
                      BATCH_SIZE);
    }


    //build a URL from a format pattern and arguments
    private URL getURL(MessageFormat form, Object...arguments) throws MalformedURLException
    {
        return new URL(form.format(arguments));
    }


    /**
     * Imports the selected publications of the parse results from the form.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event importPublications(RequestContext context)
    {
        MIVUser user = getUser(context);

        publicationService.importPublications(getFormObject(context).getParseResults(),
                                              user.getUserId(),
                                              user.getTargetUserInfo().getPerson().getUserId());
        return success();
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#getFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public DownloadForm getFormObject(RequestContext context)
    {
        return (DownloadForm) super.getFormObject(context);
    }
}
