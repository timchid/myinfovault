/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DownloadForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.publications;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.domain.publications.Publication;
import edu.ucdavis.mw.myinfovault.service.publications.ParseResult;
import edu.ucdavis.mw.myinfovault.service.publications.PublicationFilter;
import edu.ucdavis.mw.myinfovault.util.BatchPager;
import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * Form backing for publications download.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class DownloadForm extends ParseForm implements Serializable
{
    private static final long serialVersionUID = 201105161102L;

    //this year
    private final int CURRENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);

    //target users current PubMed publications
    final Set<Publication> pubmedPublications;

    //search criteria
    private String surname;
    private String givenInitial;
    private String middleInitial = StringUtil.EMPTY_STRING;
    private int yearRange = 3;
    private int toYear;
    private int fromYear;

    //search results
    private int searchCount;
    private String searchQuerykey;
    private String searchWebenv;

    //paging
    BatchPager<Publication> page;

    /**
     * Create the publication download form backing object.
     *
     * @param surname target person's surname
     * @param givenName target person's given name
     * @param pubmedPublications target user's current set of PubMed publications
     */
    public DownloadForm(String surname,
                        String givenName,
                        Set<Publication> pubmedPublications)
    {
        this.surname = surname;
        this.givenInitial = StringUtil.getInitial(givenName);
        this.toYear = CURRENT_YEAR;
        this.fromYear = CURRENT_YEAR - yearRange;
        this.pubmedPublications = pubmedPublications;
    }

    /**
     * @return The surname associated with the download
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname The surname associated with the download
     */
    public void setSurname(String surname) {
        this.surname = surname.trim();
    }

    /**
     * @return The initial of the given name associated with the download
     */
    public String getGivenInitial() {
        return givenInitial;
    }

    /**
     * @param givenInitial The initial of the given name associated with the download
     */
    public void setGivenInitial(String givenInitial) {
        this.givenInitial = StringUtil.getInitial(givenInitial);
    }

    /**
     * @return The initial of the middle name associated with the download
     */
    public String getMiddleInitial() {
        return middleInitial;
    }

    /**
     * @param middleInitial The initial of the middle name associated with the download
     */
    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = StringUtil.getInitial(middleInitial);
    }

    /**
     * @return This year
     */
    public int getCurrentYear()
    {
        return this.CURRENT_YEAR;
    }

    /**
     * @return One, three, five, or custom (zero) year range
     */
    public int getYearRange()
    {
        return yearRange == 1 || yearRange == 3 || yearRange == 5
             ? yearRange
             : 0;
    }

    /**
     * @param yearRange Predefined selection of one, three, five, or custom (zero) year range
     */
    public void setYearRange(int yearRange)
    {
        this.yearRange = yearRange;
    }

    /**
     * @return Current year or user selected earlier year
     */
    public int getToYear()
    {
        return yearRange == 1 || yearRange == 3 || yearRange == 5 || toYear > CURRENT_YEAR
             ? CURRENT_YEAR
             : toYear;
    }

    /**
     * @param toYear User defined maximum year
     */
    public void setToYear(int toYear)
    {
        this.toYear = toYear;
    }

    /**
     * @return One, three, or five years ago or user defined not exceeding the current year
     */
    public int getFromYear()
    {
        return yearRange == 1 || yearRange == 3 || yearRange == 5
             ? CURRENT_YEAR - yearRange
             : fromYear;
    }

    /**
     * @param fromYear User defined minimum year
     */
    public void setFromYear(int fromYear)
    {
        this.fromYear = fromYear;
    }

    /**
     * @return Total records found from PubMed search
     */
    public int getSearchCount()
    {
        return searchCount;
    }

    /**
     * @return Required string for retrieving PubMed search results
     */
    public String getSearchQuerykey()
    {
        return searchQuerykey;
    }

    /**
     * @return Required string for retrieving PubMed search results
     */
    public String getSearchWebenv()
    {
        return searchWebenv;
    }

    /**
     * @return Downloaded publications pager
     */
    public BatchPager<Publication> getPage()
    {
        return page;
    }

    /**
     * Set total search count and required strings for retrieving PubMed search results
     *
     * @param searchCount total records
     * @param searchQuerykey
     * @param searchWebenv
     * @param searchIDs publication IDs by which to filter
     */
    public void setSearchResult(int searchCount,
                                String searchQuerykey,
                                String searchWebenv,
                                Collection<Integer> searchIDs)
    {
        this.searchCount = searchCount;
        this.searchQuerykey = searchQuerykey;
        this.searchWebenv = searchWebenv;

        // number of search IDs found that have a match in the target user's publications
        int matchCount = (new PublicationFilter(searchIDs)).apply(pubmedPublications).size();

        //create pager object with total record count of the search count less the match count
        this.page = new BatchPager<Publication>(searchCount - matchCount);
    }

    /**
     * Add another parse result to the form.
     *
     * @param parseResult Parse result to add to the list of results
     */
    public void addParseResult(ParseResult parseResult)
    {
        getParseResults().add(parseResult);

        this.page.addBatch(parseResult.getPublications());
    }
}
