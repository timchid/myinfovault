/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UploadAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.publications;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;

import javax.mail.MessagingException;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.xml.sax.SAXException;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.publications.ImportResult;
import edu.ucdavis.mw.myinfovault.service.publications.PublicationService;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.MivMailMessage;


/**
 * Webflow form action for the upload flow.
 *
 * @author Craig Gilmore
 * @since 3.9.1
 */
public class UploadAction extends MivFormAction
{
    private static final String DATA_IMPORT_DIR = "dataimport";

    private static final boolean IS_PRODUCTION = MIVConfig.getConfig().isProductionServer();

    private static final String FROM_ADDRESS = "MIV Data Import <noreply@ucdavis.edu>";

    private static final String UPLOAD_PATTERN = "User {0,number,#} uploading file \"{1}\" to {2} for user {3,number,#}";
    private static final String UPLOAD_FAIL_PATTERN = "Data upload failed for user {0,number,#}. File \"{1}\" was uploaded by {2,number,#} as \"{3}\" on machine {4}";
    private static final String MESSAGE_PATTERN = "{0} ({1,number,#}) uploaded \"{2}\" for {3} ({4,number,#}) on {5}\n {6} records were imported.\n";
    private static final String SUBJECT_PATTERN = "{0}Data Imported into MIV by {1} {2}";
    private static final String IMPORT_EMPTY_PATTERN = "The uploaded document \"{0}\" contains no {1} data";
    private static final String IMPORT_INVALID_PATTERN = "The uploaded document \"{0}\" is not a proper {1} file";

    private PublicationService publicationService;

    /**
     * Create the publication upload action bean.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     * @param publicationService Spring injected publication service
     */
    public UploadAction(UserService userService,
                        AuthorizationService authorizationService,
                        PublicationService publicationService)
    {
        super(userService, authorizationService);

        this.publicationService = publicationService;
    }


    /**
     * Upload, parse and import publications from the file bound to the form.
     */
    @Override
    public Event bindAndValidate(RequestContext context) throws Exception
    {
        // Call standard bind to get submitted values
        super.bindAndValidate(context);

        Errors errors = getFormErrors(context);

        // Done if we already have errors from the bind/validate
        if (errors.hasErrors()) return error();

        UploadForm uploadForm = (UploadForm) getFormObject(context);

        // name of file originally uploaded
        String originalFileName = uploadForm.getUploadFile().getOriginalFilename();

        // The logged in MIV user
        MIVUser user = getUser(context);

        try
        {
            // Import the uploaded file
            ImportResult importResult = publicationService.importPublications(uploadFile(uploadForm.getUploadFile(), user),
                                                                              uploadForm.getSource(),
                                                                              user.getUserID(),
                                                                              user.getTargetUserInfo().getPerson().getUserId());

            if (importResult.getProcessed() <= 0) throw new IOException("No publication records processed");

            // Send the import notification email
            mailResults(importResult.getPersisted(),
                        user,
                        originalFileName);

            // Store import result in the form
            uploadForm.setImportResult(importResult);
        }
        catch (IOException e)
        {
            String errorMessage = MessageFormat.format(IMPORT_EMPTY_PATTERN,
                                                       originalFileName,
                                                       uploadForm.getSource());
            logger.warn(errorMessage, e);

            errors.reject(null, errorMessage);

            return error();
        }
        catch (SAXException e)
        {
            String errorMessage = MessageFormat.format(IMPORT_INVALID_PATTERN,
                                                       originalFileName,
                                                       uploadForm.getSource());
            logger.warn(errorMessage, e);

            errors.reject(null, errorMessage);

            return error();
        }

        return success();
    }


    /**
     * Persists the bound file to the user's directory
     * @param uploadFile
     * @param user
     * @return abstract presentation of uploaded file
     */
    private File uploadFile(MultipartFile uploadFile, MIVUser user)
    {
        // Get write file for user upload
        File outputFile = getUniqueFile(user, uploadFile.getOriginalFilename());

        logger.info(MessageFormat.format(UPLOAD_PATTERN,
                                         user.getUserID(),
                                         uploadFile.getOriginalFilename(),
                                         outputFile,
                                         user.getTargetUserInfo().getPerson().getUserId()));


        // Write uploaded file to user directory
        try
        {
            uploadFile.transferTo(outputFile);
        }
        catch (IOException e)
        {
            if (logger.isWarnEnabled())
            {
                logger.warn(MessageFormat.format(UPLOAD_FAIL_PATTERN,
                                                 user.getTargetUserInfo().getPerson().getUserId(),
                                                 uploadFile.getOriginalFilename(),
                                                 user.getUserID(),
                                                 outputFile,
                                                 MIVConfig.getConfig().getServer()), e);
            }
        }

        return outputFile;
    }


    /**
     * Assures that uploaded files will not overwrite each other.
     *
     * @param user Real, logged-in MIV user
     * @param filename base name of the file
     * @return the File object for a uniquely named file
     */
    private File getUniqueFile(MIVUser user, String filename)
    {
        // Get write directory for user
        File outputDir = new File(user.getTargetUserInfo().getUserDir(), DATA_IMPORT_DIR);
        if (!outputDir.exists())
        {
            outputDir.mkdirs();
        }

        int extDotPosition = filename.lastIndexOf('.');
        String basename = extDotPosition == -1 ? filename : filename.substring(0, extDotPosition);
        String extension = extDotPosition == -1 ? "" : filename.substring(extDotPosition + 1);
        String testname = filename;
        int counter = 0;
        File newname = new File(outputDir, testname);
        while (newname.exists())
        {
            testname = basename + "_" + (++counter) + "." + extension;
            newname = new File(outputDir, testname);
        }
        return newname;
    }


    /**
     * Sends notification email for this import.
     *
     * @param persisted
     * @param user
     * @param filename
     */
    private void mailResults(int persisted,
                             MIVUser user,
                             String filename)
    {
        final String toAddress = MIVConfig.getConfig().getProperty("importmail");

        if (StringUtils.hasText(toAddress))
        {
            try
            {
                MivMailMessage email = new MivMailMessage(toAddress,
                                                          FROM_ADDRESS,
                                                          MessageFormat.format(SUBJECT_PATTERN,
                                                                               IS_PRODUCTION ? "" : "[TEST] ",
                                                                               user.getUserInfo().getPerson().getGivenName(),
                                                                               user.getUserInfo().getPerson().getSurname()));

                email.send(MessageFormat.format(MESSAGE_PATTERN,
                                                user.getUserInfo().getFullName(),
                                                user.getUserInfo().getPerson().getUserId(),
                                                filename,
                                                user.getTargetUserInfo().getFullName(),
                                                user.getTargetUserInfo().getPerson().getUserId(),
                                                MIVConfig.getConfig().getServer(),
                                                persisted));
            }
            catch (MessagingException e)
            {
                logger.warn("Sending import results via mail failed.", e);
            }
        }
        else
        {
            logger.warn("No importmail address was found in mivconfig.properties. Not sending email.");
        }
    }
}
