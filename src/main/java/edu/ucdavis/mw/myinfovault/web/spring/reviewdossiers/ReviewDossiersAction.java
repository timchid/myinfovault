/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReviewDossiersAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.reviewdossiers;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.SchoolDepartmentCriteria;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.spring.search.MivActionList;
import edu.ucdavis.mw.myinfovault.web.spring.search.OpenAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PathConstructor;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * This class is the form controller for the ReviewDossiers webflow.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class ReviewDossiersAction extends OpenAction
{
    private DossierCreatorService dossierCreatorService;

//    private static final DateFormat dossierDateFormat = new SimpleDateFormat("MM/dd/yy, h:mm a");
// TODO: Does "DateFormat.SHORT" below produce the same strings as "MM/dd/yy, h:mm a" above?
    private static final ThreadLocal<DateFormat> dossierDateFormat =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        }
    };

    /**
     * Creates the form action bean for viewing dossier snapshots.
     *
     * @param searchStrategy Spring-injected search strategy
     * @param authorizationService Spring-injected  service
     * @param userService Spring-injected user service
     * @param dossierService Spring-injected dossier service
     * @param dossierCreatorService Spring-injected dossier creator service
     */
    public ReviewDossiersAction(SearchStrategy searchStrategy,
                                AuthorizationService authorizationService,
                                UserService userService,
                                DossierService dossierService,
                                DossierCreatorService dossierCreatorService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService);

       this.dossierCreatorService = dossierCreatorService;
    }

    /**
     * Checks if user is authorized to review dossiers.
     * All roles except DEPT_ASSISTANT are allowed to review dossiers
     * @param context Request context from webflow
     * @return boolean status, success or error
     */
    public Event isAuthorized(RequestContext context)
    {
        // MIV-3847 Permission to review dossiers is now based on the logged-in user, not the proxied user.
        //MivPerson targetPerson = this.getActingPerson(context);
        MivPerson targetPerson = getRealPerson(context);

        if (authorizationService.hasPermission(targetPerson, Permission.REVIEW_DOSSIER, null))
        {
            logger.info("User "+targetPerson.getDisplayName()+" ("+targetPerson.getPersonId()+") is AUTHORIZED to review dossiers.");
            return success();
        }

        logger.info("User "+targetPerson.getDisplayName()+" ("+targetPerson.getPersonId()+") is ** NOT ** AUTHORIZED to review dossiers.");
        return denied();
    }


    /**
     * Check if this user has any records in the DossierReviewers table and validates the state of the
     * dossiers as ready to be reviewed.
     * @param context Request context from webflow
     * @return boolean status, success or error
     */
    public Event loadDossiersToReview(RequestContext context)
    {
        MIVSession mivSession = this.getMivSession(context);
        MIVUserInfo mivTargetUserInfo = mivSession.getUser().getTargetUserInfo();
        MutableAttributeMap<Object> flowscope = context.getFlowScope();

        List<DossierReviewerDto> dossiersToReview = null;
        try
        {
            // Get the dossiers available for this user to review
            dossiersToReview = dossierService.findDossiersToReview(mivTargetUserInfo.getPerson().getUserId());
            flowscope.put("dossiersToReview", dossiersToReview);
            return success();
        }
        catch (WorkflowException wfe)
        {
            logger.info("Unable to retrieve dossiers to review for user " + mivTargetUserInfo.getDisplayName(), wfe);
            return error();
        }
    }


    /**
     * Get the Dossiers that this user is allowed to review
     *
     * @param user
     * @return dossier action lists that the user can review
     */
    public List<MivActionList> getDossiersToReview(MIVUser user)
    {
//        MivPerson mivPerson = userService.getPersonByMivId(user.getTargetUserInfo().getUserID());
        List<MivActionList> actionList = new ArrayList<MivActionList>();
//        Map<String, Map<String, String>> departments = MIVConfig.getConfig().getMap("departments");
        Dossier dossier = null;

        List<DossierReviewerDto> dossiersToReview = null;
        try
        {
            // Get the dossiers available for this user to review
            dossiersToReview = dossierService.findDossiersToReview(user.getTargetUserInfo().getPerson().getUserId());
        }
        catch (WorkflowException wfe)
        {
            logger.info("Unable to retrieve dossier to review for user " + user.getTargetUserInfo().getDisplayName(), wfe);
            throw new MivSevereApplicationError(wfe.getLocalizedMessage());
        }

        for (DossierReviewerDto dossierReviewer : dossiersToReview)
        {
            try
            {
                dossier = dossierService.getDossier(dossierReviewer.getDossierId());
            }
            catch (WorkflowException wfe)
            {
                logger.info("Unable to retrieve dossier " + dossierReviewer.getDossierId() +
                            " for review by user " + user.getTargetUserInfo().getDisplayName(), wfe);
                continue;
            }

            MivPerson dossierPerson = dossier.getAction().getCandidate();//userService.getPersonByMivId(dossier.getUserId());

            // Determine the appointmentType
            int schoolId = dossierReviewer.getSchoolId();
            int departmentId = dossierReviewer.getDepartmentId();
            Map<DossierAppointmentAttributeKey,DossierAppointmentAttributes> appointments = dossier.getAppointmentAttributes();
            DossierAppointmentAttributes appointmentAttributes = appointments.get(new DossierAppointmentAttributeKey(schoolId, departmentId));

            // Build the MivActionList item for this dossier
            MivActionList action = new MivActionList(dossier);
            action.setActionLocation(dossier.getLocation());
            action.setActionType(dossier.getAction().getDescription());
            action.setAppointmentType(appointmentAttributes != null && appointmentAttributes.isPrimary() ? "Primary" : "Joint");
            action.setDelegationAuthority(dossier.getAction().getDelegationAuthority().getDescription(dossier.getReviewType()));
            action.setDepartmentId(Integer.parseInt(appointmentAttributes.getDeptId()));
            action.setDepartmentName(appointmentAttributes.getDepartmentDescription());
            action.setGivenName(dossierPerson.getGivenName());
            action.setSurname(dossierPerson.getSurname());
            action.setSortName(dossierPerson.getSortName());
            action.setDossierId(dossier.getDossierId());
            action.setLocationDescription(DossierLocation.mapWorkflowNodeNameToLocation(action.getActionLocation()).getDescription());
            action.setSchoolId(Integer.parseInt(appointmentAttributes.getSchoolId()));
            action.setSchoolName(appointmentAttributes.getSchoolDescription());

            action.setSubmitDate(dossierDateFormat.get().format(dossier.getSubmittedDate().getTime()));
            Date routeDate = dossier.getLastRoutedDate();
            action.setLastRoutedDate(routeDate != null ? dossierDateFormat.get().format(routeDate.getTime()) : null);

            action.setUserId(dossier.getAction().getCandidate().getUserId());
            actionList.add(action);
        }

        return actionList;
    }


    /**
     * Put into the flow scope the map of dossier file URLs to review.
     *
     * @param context WebFlow request context
     * @return WebFlow event status; success or error
     */
    public Event retrieveDossier(RequestContext context)
    {
        MIVSession mivSession = this.getMivSession(context);
        String dossierId = context.getExternalContext().getRequestParameterMap().get("dossierId");
        String userName = context.getExternalContext().getRequestParameterMap().get("userName");
        // School and department id of the dossier being reviewed
        String schoolId = context.getExternalContext().getRequestParameterMap().get("schoolId");
        String departmentId = context.getExternalContext().getRequestParameterMap().get("departmentId");

        try
        {
            Dossier dossier = dossierService.getDossierAndLoadAllData(Integer.parseInt(dossierId));

            // If either the department or school don't match the dossier primary dept/school
            // this is a joint school reviewer
            boolean isPrimaryReviewer = (dossier.getPrimaryDepartmentId() == Integer.parseInt(departmentId))
                                     && (dossier.getPrimarySchoolId() == Integer.parseInt(schoolId));

            // Default reviewer role and review dossier name
            String combinedDossierName = "dossier.pdf";
            MivRole reviewerRole = MivRole.DEPT_REVIEWER;

            switch (DossierLocation.mapWorkflowNodeNameToLocation(dossier.getLocation()))
            {
                // If the document is at the VICEPROVOST location, change to the applicable dossier name and reviewer role
                // TODO: With the new senate locations, the VICEPROVOST location may not be applicable to the CRC_REVIEWER
                case VICEPROVOST:
                case SENATE_OFFICE:
                case FEDERATION:
                case SENATEFEDERATION:
                case POSTSENATEVICEPROVOST:
                case POSTAPPEALVICEPROVOST:
                case SENATEAPPEAL:
                case FEDERATIONAPPEAL:
                case FEDERATIONSENATEAPPEAL:
                               reviewerRole = MivRole.CRC_REVIEWER;
                    break;
                case SCHOOL:
                case POSTAPPEALSCHOOL:
                case POSTSENATESCHOOL:
                case POSTAUDITREVIEW:
                    reviewerRole = isPrimaryReviewer ? MivRole.SCHOOL_REVIEWER : MivRole.J_SCHOOL_REVIEWER;
                    break;
                case DEPARTMENT:
                    reviewerRole = isPrimaryReviewer ? MivRole.DEPT_REVIEWER : MivRole.J_DEPT_REVIEWER;
                    break;
                default:
                    reviewerRole = MivRole.DEPT_REVIEWER;
            }

            PathConstructor pathConstructor = new PathConstructor();
            String combinedDossierDocument = pathConstructor.getUrlFromPath(
                            new File(dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF), combinedDossierName)
                        )
                        + "?roleId="+reviewerRole.toString(); // + "&schoolId="+schoolId + "&departmentId="+departmentId;

            MutableAttributeMap<Object> flowscope = context.getFlowScope();
            flowscope.put("dossierId", dossierId);
            flowscope.put("userName", userName);
            flowscope.put("combinedDossierDocument", combinedDossierDocument);
            SchoolDepartmentCriteria schoolDepartmentCriteria = new SchoolDepartmentCriteria();
            schoolDepartmentCriteria.setSchool(Integer.parseInt(schoolId));
            schoolDepartmentCriteria.setDepartment(Integer.parseInt(departmentId));
            Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap = new HashMap<String, SchoolDepartmentCriteria>();
            schoolDepartmentCriteriaMap.put(
                    schoolDepartmentCriteria.getSchool() + ":" + schoolDepartmentCriteria.getDepartment(), schoolDepartmentCriteria);
            Map<String,String> dossierFileUrlMap = dossierCreatorService.getDossierFileUrlMap(dossier, reviewerRole, schoolDepartmentCriteriaMap);
            flowscope.put("dossierFileUrlMap", dossierFileUrlMap);
        }
        catch (WorkflowException wfe)
        {
            logger.info("Unable to retrieve dossier documents to review for user " +
                    mivSession.getUser().getTargetUserInfo().getDisplayName(), wfe);
            return error();
        }

        return success();
    }

}
