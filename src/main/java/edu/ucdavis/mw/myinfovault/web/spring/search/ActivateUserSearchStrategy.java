/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ActivateUserSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import edu.ucdavis.mw.myinfovault.service.authorization.Permission;

/**
 * Exactly the same as the EditUserSearchStrategy, but uses a different Permission check.
 * 
 * @author Stephen Paulsen
 * @since MIV 4.6.1
 */
public class ActivateUserSearchStrategy extends EditUserSearchStrategy
{
    /**
     * Create the activate user search strategy by overriding
     * the permission to check from the edit user strategy.
     */
    public ActivateUserSearchStrategy()
    {
        this.checkPermission = Permission.ACTIVATE_USER;
    }
}
