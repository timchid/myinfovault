/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ArchiveDossiersSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.Collections;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Implement the strategy to be used when searching for dossiers which are ready to archive.
 *
 * The search strategy is determined by the role of the searching user as follows:
 *
 * Only the Vice Provost role is allowed to archive dossiers and will receive a list of
 * dossiers which may be archived.
 *
 * @author Rick Hendricks
 * @since MIV 4.0
 */
public class ArchiveDossiersSearchStrategy extends DefaultSearchStrategy
{
    /**
     * Get the Dossiers that are at the given location for the given user.
     *
     * @param user who will be accessing the dossiers
     * @param location dossier location
     * @return List of MivActionList items
     */
    @Override
    public List<MivActionList> getDossiersAtLocation(MIVUser user, DossierLocation location)
    {
        // Only the Vice Provost Staff role will have any items returned
        return user.getTargetUserInfo().getPerson().hasRole(MivRole.VICE_PROVOST_STAFF)
             ? super.getDossiersAtLocation(user, location)//vp staff
             : Collections.<MivActionList>emptyList();//not vp staff, return empty list
    }

    // Tell the superclass that this strategy needs URLs
    {
        this.needsUrl = true;
    }
}
