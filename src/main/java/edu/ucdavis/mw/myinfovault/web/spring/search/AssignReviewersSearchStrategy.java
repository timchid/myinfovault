/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignReviewersSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.List;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Implement the strategy to be used when assigning reviewers for dossiers. Reviewers
 * must be active and either a candidate or MIV administrator.
 *
 * @author Rick Hendricks
 * @author Craig Gilmore
 * @since 4.0
 */
public class AssignReviewersSearchStrategy extends DefaultSearchStrategy
{
    //filter used for search methods
    private final RoleFilter filter = new RoleFilter(MivRole.CANDIDATE,
                                                     MivRole.VICE_PROVOST_STAFF,
                                                     MivRole.VICE_PROVOST);

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.DefaultSearchStrategy#getUsersByName(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersByName(MIVUser user,
                                          SearchCriteria criteria)
    {
        criteria.setLastName("all");
        return super.getUsersByName(user.getTargetUserInfo().getPerson(),
                                    criteria,
                                    filter);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.DefaultSearchStrategy#getUsersByDepartment(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersByDepartment(MIVUser user,
                                                SearchCriteria criteria)
    {
        return super.getUsersByDepartment(user.getTargetUserInfo().getPerson(),
                                          criteria,
                                          filter);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.DefaultSearchStrategy#getUsersBySchool(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersBySchool(MIVUser user,
                                            SearchCriteria criteria)
    {
        return super.getUsersBySchool(user.getTargetUserInfo().getPerson(),
                                      criteria,
                                      filter);
    }
}
