/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DefaultSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.authorization.AssignReviewersAuthorizer;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierAppointmentType;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivDisplayPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotDto;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PathConstructor;

/**
 * DefaultSearchStrategy - The default searching strategy to be used when searching for dossiers or users. The default
 * search strategy basically uses no role filtering/scoping in the searches. This class is meant to be subclassed to
 * implement search strategies which require role filtering or scoping, such as the ManageOpenActions dossier/user
 * searches.
 *
 * @author Rick Hendricks
 * @since MIV 4.0
 */
public class DefaultSearchStrategy implements SearchStrategy
{
    protected static final AuthorizationService authorizationService = MivServiceLocator.getAuthorizationService();
    private static final SnapshotService snapshotService = MivServiceLocator.getSnapshotService();
    private static final UserService userService = MivServiceLocator.getUserService();

    private static final MIVConfig cfg = MIVConfig.getConfig();
    private static final Map<String, Map<String, String>> schoolMap = MIVConfig.getConfig().getMap("schools");
    private static final Map<String, Map<String, String>> departmentMap = MIVConfig.getConfig().getMap("departments");

    /**
     * This is used only in fillDossierPerson, but stored here as a cache
     * so it doesn't have to be re-fetched from the UserService if it hasn't
     * changed since the last call (often in a loop)
     * It was subject to race conditions, so turned it into a ThreadLocal
     */
    public static final ThreadLocal<MivPerson> tlPerson = new ThreadLocal<MivPerson>();

    protected static final ThreadLocal<DateFormat> dossierDateFormat =
            new ThreadLocal<DateFormat>() {
                @Override
                protected DateFormat initialValue()
                {
                    return new SimpleDateFormat("MM/dd/yy, hh:mm a");
                }
            };
    protected static final ThreadLocal<DateFormat> timestampFormat =
            new ThreadLocal<DateFormat>() {
                @Override
                protected DateFormat initialValue()
                {
                    return new SimpleDateFormat("yyyyMMddHHmmss");
                }
            };

    /**
     * Constructs URL and File paths for search results.
     */
    protected PathConstructor pathConstructor = new PathConstructor();
    protected boolean needsUrl = false;


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#dossierSearch(edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.myinfovault.MIVUser, java.util.List)
     */
    @Override
    public List<MivActionList> dossierSearch(SearchCriteria criteria, MIVUser user, List<Dossier> dossiers)
            throws WorkflowException
    {
        final String tname = criteria.getInputName().toLowerCase();
        final String linit = criteria.getLname().toLowerCase();
        final int schoolId = criteria.getSchoolId();
        final int deptId = criteria.getDepartmentId();
        MivPerson mivPerson = user.getTargetUserInfo().getPerson();

        boolean showAll = true;
        int[] excludeList = null;
        List<MivActionList> list = null;

        // Associate people with the dossiers
        List<MivActionList> people = findPeople(dossiers, mivPerson, showAll, schoolId, deptId, excludeList);
        if (StringUtils.hasText(tname)) // the name was entered so search for that
        {
            list = applyFilter(new SearchFilterAdapter<MivActionList>() {
                @Override
                public boolean include(MivActionList item)
                {
                    return matchMivActionListUserName(item, tname);
                }
            }, people);

            List<MivActionList> exactMatch = new ArrayList<MivActionList>();
            for (MivActionList oneDossier : list)
            {
                if (matchMivActionListUserName(oneDossier, tname))
                {
                    if (!exactMatch.contains(oneDossier))
                    {
                        exactMatch.add(oneDossier);
                    }
                }

            }

            if (exactMatch.size() > 0) { return exactMatch; }
        }
        else
        // search for last name
        {
            list = applyFilter(new SearchFilterAdapter<MivActionList>() {
                @Override
                public boolean include(MivActionList item)
                {
                    return "all".equals(linit) || item.getSurname().toLowerCase().indexOf(linit) == 0;
                }
            }, people);
        }

        return list;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getDossiersByDepartment(edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.myinfovault.MIVUser)
     */
    @Override
    public List<MivActionList> getDossiersByDepartment(SearchCriteria criteria, MIVUser user)
    {
        final int schoolId = criteria.getDeptSchoolId();
        final int deptId = criteria.getDepartmentId();
        MivPerson mivPerson = user.getTargetUserInfo().getPerson();
        boolean showAll = true;
        int[] excludeList = null;
        List<Dossier> dossiers =
                getDossiers(mivPerson, schoolId, deptId);

        List<MivActionList> people = findPeople(dossiers, mivPerson, showAll, schoolId, deptId, excludeList);
        if (schoolId == 0 && deptId == 0) { return people; }

        // filter by the specified department and school
        return applyFilter(new SearchFilterAdapter<MivActionList>() {
            @Override
            public boolean include(MivActionList item)
            {
                return ((item.getSchoolId() == schoolId) &&
                (item.getDepartmentId() == deptId || deptId == 0));
            }
        }, people);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getDossiersBySchool(edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.myinfovault.MIVUser)
     */
    @Override
    public List<MivActionList> getDossiersBySchool(SearchCriteria criteria, MIVUser user)
    {
        final int schoolId = criteria.getSchoolId();
        MivPerson mivPerson = user.getTargetUserInfo().getPerson();
        boolean showAll = true;

        List<Dossier> dossiers = getDossiers(mivPerson, schoolId, 0);

        int[] excludeList = null;

        List<MivActionList> people = findPeople(dossiers, mivPerson, showAll, schoolId, 0, excludeList);

        return people;
    }


    /**
     * Using the principalName, school, and department, finds all of the dossiers this person can see.
     *
     * @param person
     * @param schoolId
     * @param deptId
     * @return List of Dossiers
     */
    private List<Dossier> getDossiers(MivPerson mivPerson, int schoolId, int deptId)
    {
        try
        {
            return dossierService.getDossiersBySchoolAndDepartment(mivPerson, schoolId, deptId);
        }
        catch (WorkflowException e)
        {
            logger.warn("getDossiers: got a WorkflowException doing getDossiersBySchoolDepartmentAndLocation", e);
        }

        return new ArrayList<Dossier>();//FIXME: immutable empty list okay?
    }


    /**
     * Filter results with the given filter.
     *
     * @param filter
     * @param results
     * @return filtered results
     */
    protected List<MivActionList> applyFilter(SearchFilter<MivActionList> filter, List<MivActionList> results)
    {
        if (filter != null && results != null && !results.isEmpty())
        {
            List<MivActionList> people = results;
            results = new LinkedList<MivActionList>();
            Collection<MivActionList> c = filter.apply(people);
            results.addAll(c);
        }

        return results;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getDossiersToAssignReviewers(edu.ucdavis.myinfovault.MIVUser)
     */
    @Override
    public List<MivActionList> getDossiersToAssignReviewers(MIVUser user)
    {
        final MivPerson person = user.getTargetUserInfo().getPerson();
        List<MivActionList> mivActions = new ArrayList<MivActionList>();

        // Build a list of dossier actions scoped to the requesting user
        try
        {
            for (Dossier dossier : dossierService.getDossiers(person))
            {
                for (DossierAppointmentAttributeKey key : dossier.getAppointmentAttributes().keySet())
                {
                    DossierAppointmentAttributes attributes = dossier.getAppointmentAttributes().get(key);
                    if (attributes != null)
                    {
                        MivActionList mal = fillDossierPerson(dossier, dossier.getLocation(), attributes, 0, 0);
                        if (mal != null) mivActions.add(mal);
                    }
                    else
                    {
                        logger.error("No dossier attributes for dossier {} key={} for {} to assign reviewers.",
                                     new Object[] { dossier.getDossierId(), key, person });
                    }
                }
            }
        }
        catch (WorkflowException e)
        {
            logger.error("Unable to retrieve dossiers for " + person + " to assign reviewers.", e);
        }

        // The dossier actions have already been qualified by school and department. Now qualify by location.
        // Filter to only show dossiers at the Department for department admins and at the School for school admins.
        // The ViceProvost location can see all REDELEGATED dossiers at the school location,
        // as well as all at the department and ViceProvost locations.
        List<MivActionList> dossierList = applyFilter(new SearchFilterAdapter<MivActionList>() {
            @Override
            public boolean include(MivActionList item)
            {
                // Additional permission details for location and delegation authority
                AttributeSet permissionDetails = new AttributeSet();
                permissionDetails.put(AssignReviewersAuthorizer.DOSSIER_LOCATION, item.getActionLocation());

                // School and department qualifiers
                AttributeSet qualification = new AttributeSet();
                qualification.put(Qualifier.DEPARTMENT, Integer.toString(item.getDepartmentId()));
                qualification.put(Qualifier.SCHOOL, Integer.toString(item.getSchoolId()));

                // include if authorized
                return authorizationService.isAuthorized(person,
                                                         Permission.ASSIGN_REVIEWERS,
                                                         permissionDetails,
                                                         qualification);
            }
        }, mivActions);
        return dossierList;
    }


    /** Used in {@link #findPeople(List, MivPerson, boolean, int, int, int...) findPeople()} below. */
    private static final EnumSet<DossierLocation> excludeLocations =
            EnumSet.of(
                       DossierLocation.ARCHIVE,
                       DossierLocation.POSTAUDITREVIEW,
                       DossierLocation.SENATEAPPEAL,
                       DossierLocation.FEDERATIONAPPEAL,
                       DossierLocation.FEDERATIONSENATEAPPEAL,
                       DossierLocation.POSTAPPEALSCHOOL,
                       DossierLocation.POSTAPPEALVICEPROVOST
                       );
    /**
     * Associate dossiers with their owners.
     *
     * @param dossiers List of Dossiers to process
     * @param targetPerson mivPerson requesting the dossiers (Not Used)
     * @param showAll flag indicating if dossiers at the Candidate location should be shown
     * @param schoolId schoolId
     * @param deptId departmentId
     * @param dropUsers array of user IDs to exclude from the returned list
     * @return List of MivAction objects
     */
    protected List<MivActionList> findPeople(List<Dossier> dossiers, MivPerson targetPerson, boolean showAll,
                                             int schoolId, int deptId, int... dropUsers)
    {
        // apply Fliter -- do not add if dropUser = dossier.getUserId()
        List<MivActionList> people = new ArrayList<MivActionList>(dossiers.size());
        for (Dossier ds : dossiers)
        {
            String theDossierLocation = ds.getLocation();
            // Lowercase it for .equals() testing, but keep the unaltered string
            // to pass into the fillDossierPerson() method.

            // MIV-4241 this is where we fix for the new MIV Open Action Reports (to display dossiers at candidate &
            // readyforpostreviewaudit)
            // Don't show any dossiers past ready for post review audit
            // If another location must be excluded just add it to the static declaration above.
            if (excludeLocations.contains(ds.getDossierLocation())) {
                continue;
            }

            // Don't show candidate dossiers unless told to show all
            // MIV-4241 if (location.equals("candidate") && !showAll) continue;

            final int dossierUserId = ds.getAction().getCandidate().getUserId();
            // If a drop users list was passed, don't show dossier if this user is on the list
            if (dropUsers != null)
            {
                boolean drop = false;
                for (int exclude : dropUsers)
                {
                    if (dossierUserId == exclude)
                    {
                        drop = true;
                        break;
                    }
                }
                if (drop) continue;
            }

            // Associate the dossier with a user and add each appointment for the dossier to the list
            Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> dossierAppointments =
                    ds.getAppointmentAttributes();
            for (DossierAppointmentAttributeKey key : dossierAppointments.keySet())
            {
                // School and department qualifiers for authorization checks
                AttributeSet qualification = new AttributeSet();
                qualification.put(Qualifier.USERID, dossierUserId + "");
                qualification.put(Qualifier.DEPARTMENT, Integer.toString(key.getDepartmentId()));
                qualification.put(Qualifier.SCHOOL, Integer.toString(key.getSchoolId()));
                // Make sure that the user is qualified for each appointment
                if (authorizationService.isAuthorized(targetPerson, Permission.VIEW_DOSSIER, null, qualification))
                {
                    DossierAppointmentAttributes attributes = dossierAppointments.get(key);
                    if (attributes != null)
                    {
                        MivActionList mal = fillDossierPerson(ds, theDossierLocation, attributes, schoolId, deptId);
                        if (mal != null) people.add(mal);
                    }
                    else
                    {
                        logger.error("No dossier attributes for dossier {}  key={} for {} to assign reviewers.",
                                     new Object[] { ds.getDossierId(), key, targetPerson });
                    }
                }
            }
        }

        return people;
    }


    /**
     * Pull together all of the person information and dossier information for the Dossier object received.
     *
     * @param dossier Dossier Object
     * @param location
     * @param appointmentAttributes
     * @param schoolId
     * @param deptId
     * @return an MivActionList Object (which is really a combination of person information and dossier information)
     */
    protected MivActionList fillDossierPerson(Dossier dossier, String location,
                                              DossierAppointmentAttributes appointmentAttributes, int schoolId,
                                              int deptId)
    {
        return fillDossierPerson(dossier, location, appointmentAttributes, schoolId, deptId, false);
    }


    /**
     * Pull together all of the person information and dossier information for the Dossier object recieved
     *
     * @param dossier Dossier Object
     * @param location
     * @param appointmentAttributes
     * @param schoolId
     * @param deptId
     * @param includeInactive if <code>true</code>, include inactive users, otherwise, do not include
     * @return an MivActionList Object (which is really a combination of person information and dossier information)
     */
    protected MivActionList fillDossierPerson(Dossier dossier, String location,
                                              DossierAppointmentAttributes appointmentAttributes, int schoolId,
                                              int deptId, boolean includeInactive)
    {
        MivPerson mivPerson = tlPerson.get();

        // Skip userService call if we are processing the same mivPerson
        final int dossierUserId = dossier.getAction().getCandidate().getUserId();
        if (mivPerson == null || mivPerson.getUserId() != dossierUserId)
        {
            mivPerson = dossier.getAction().getCandidate();
            tlPerson.set(mivPerson);
        }

        // Get any decisions for this dossier
        SortedSet<Decision> decisions = MivServiceLocator.getDecisionService().getDecisions(dossier);

        MivActionList person = new MivActionList(dossier);
        if (mivPerson == null)
        {
            logger.warn("Unable to get the MivPerson by Person or userId for dossier ID \"{}\"", dossier.getDossierId());
            person.setGivenName("not found");
            person.setSurname("not found");
            person.setSortName("not found");
            person.setDisplayName("not found");
        }
        // return null if person is inactive and we are not to include inactive users
        else if (!mivPerson.isActive() && !includeInactive)
        {
            return null;
        }
        else
        {
            person.setGivenName(mivPerson.getGivenName());
            person.setSurname(mivPerson.getSurname());
            person.setSortName(mivPerson.getSortName());
            person.setDisplayName(mivPerson.getDisplayName());
        }

        person.setUserId(dossierUserId);
        person.setRoleType(mivPerson.getPrimaryRoleType().getDescription());
        person.setAppointmentType(appointmentAttributes.isPrimary() ? DossierAppointmentType.PRIMARY.shortDescription
                : DossierAppointmentType.JOINT.shortDescription);
        person.setDelegationAuthority(dossier.getAction().getDelegationAuthority().getDescription(dossier.getReviewType()));
        person.setSubmitDate(String.format("%tD", dossier.getSubmittedDate()));
        person.setLastRoutedDate(dossier.getLastRoutedDate() != null ? String.format("%tD", dossier.getLastRoutedDate()) : null);
        person.setCompletedDate(dossier.getCompletedDate() != null ? String.format("%tD", dossier.getCompletedDate()) : null);
        person.setArchiveDate(dossier.getArchivedDate() != null ? String.format("%tD", dossier.getArchivedDate()) : null);
        person.setActionType(dossier.getAction().getDescription());
        person.setActionLocation(location);
        person.setLocationDescription(dossier.getLocationDescription(location));
        person.setDecisionType(decisions.isEmpty() ? "" : decisions.first().getDecisionType().getDecisionDescription());
        person.setReviewCommittees(dossier.getReviewCommittees());


        // Return null if this appointment is outside the school/dept the viewer can see
        if ((schoolId > 0 && schoolId != Integer.parseInt(appointmentAttributes.getSchoolId())) ||
                (deptId > 0 && deptId != Integer.parseInt(appointmentAttributes.getDeptId()))) { return null; }

        person.setSchoolId(Integer.parseInt(appointmentAttributes.getSchoolId()));
        person.setDepartmentId(Integer.parseInt(appointmentAttributes.getDeptId()));
        person.setSchoolName(this.getSchoolName(person.getSchoolId()));
        person.setDepartmentName(this.getDepartmentName(person.getSchoolId(), person.getDepartmentId()));
        person.setDossierId(dossier.getDossierId());

        // Get the dossier Url for this dossier if it's needed
        if (needsUrl) {
            person.setDossierUrl(pathConstructor.getUrlFromPath(dossier.getDossierPdf()));
        }

        return person;
    }


    /**
     * Get the school name for the given school ID.
     *
     * @param school school ID
     * @return school name
     */
    protected String getSchoolName(int school)
    {
        String returnName = "";
        Map<String, String> schoolInfo = cfg.getMap("schools").get(school + "");
        // if schoolInfo is null we got nothing back from the map,
        // which means the provided school number is not valid
        if (schoolInfo != null)
        {
            returnName = schoolInfo.get("description");
        }
        else
        {
            String msg = "Dossier with an invalid school ID: " + school;
            logger.warn(msg, new IllegalArgumentException(msg));
        }

        return returnName;
    }


    /**
     * Get the department name for the given school and department IDs.
     *
     * @param school school ID
     * @param department department ID
     * @return department name
     */
    protected String getDepartmentName(int school, int department)
    {
        String returnName = "";
        String deptKey = school + ":" + department;
        // If deptInfo is null we got nothing back from the map, which
        // means the provided school:dept combination is not valid
        Map<String, String> deptInfo = cfg.getMap("departments").get(deptKey);
        if (deptInfo != null)
        {
            returnName = deptInfo.get("description");
        }
        else
        {
            String msg = "Dossier with an invalid school:department combination: " + deptKey;
            logger.warn(msg, new IllegalArgumentException(msg));
        }

        return returnName;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getUsersByName(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersByName(MIVUser mivUser, SearchCriteria criteria)
    {
        return this.getUsersByName(mivUser.getTargetUserInfo().getPerson(), criteria, null);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getUsersByName(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<MivPerson> getUsersByName(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s)
    {

        List<AttributeSet> searchList = new ArrayList<AttributeSet>();

        String searchName = criteria.getInputName();
        String searchInitial = criteria.getLname();

        String searchKey;
        String searchString;
        if (StringUtils.hasText(searchName)) // The name was entered so search for that
        {
            searchKey = MivSearchKeys.Person.NAME;
            searchString = "*" + searchName.toLowerCase() + "*";
            searchString = StringUtil.prepareTextSearchCriteria(searchString, "*");
        }
        else
        // Otherwise use the first initial of last name
        {
            // When "all" was chosen blank out the initial so just a wildcard remains
            if ("all".equalsIgnoreCase(searchInitial)) searchInitial = "";
            searchKey = MivSearchKeys.Person.LAST_NAME;
            searchString = searchInitial.toLowerCase() + "*";
        }
        AttributeSet as = new AttributeSet();
        as.put(searchKey, searchString);
        if (criteria.isActiveOnly()) as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");
        searchList.add(as);

        // Search for each set of attributes given in the search list,
        // and combine the results into a single list.
        List<MivPerson> people = new LinkedList<MivPerson>();

        // Get the role searchfilter if we are searching by role.
        SearchFilter<MivPerson> searchFilter = getRoleSearchFilter(criteria);

        // If we are not searching by role, use the input search filter;
        if (searchFilter == null)
        {
            searchFilter = s;
        }

        for (AttributeSet attributeSet : searchList)
        {
            List<MivPerson> found = null;
            // See if we are filtering users by role (for View MIV Dean or Department Chairs report)
            found = userService.findUsers(attributeSet, searchFilter);

            if (found != null && !found.isEmpty())
            {
                people.addAll(found);
            }
        }

        return people;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getUsersByDepartment(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersByDepartment(MIVUser mivUser, SearchCriteria criteria)
    {
        return this.getUsersByDepartment(mivUser.getTargetUserInfo().getPerson(), criteria, null);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getUsersByDepartment(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<MivPerson> getUsersByDepartment(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s)
    {
        List<AttributeSet> searchList = new ArrayList<AttributeSet>();
        int searchSchool = criteria.getDeptSchoolId();
        int searchDept = criteria.getDepartmentId();

        // "All" was chosen - what "All" means varies depending on the role of the actor
        if (searchSchool == 0 && searchDept == 0)
        {
            AttributeSet as = new AttributeSet();
            // Don't set either a school or department code, so the search is not limited
            if (criteria.isActiveOnly()) as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");
            searchList.add(as);
        }
        // Some specific school + department was chosen, or "All" as <school>:0 meaning
        // all departments within the given school.
        else
        {
            AttributeSet as = new AttributeSet();
            as.put(MivSearchKeys.Person.MIV_SCHOOL_CODE, Integer.toString(searchSchool));
            if (searchDept != 0)
            {
                as.put(MivSearchKeys.Person.MIV_DEPARTMENT_CODE, Integer.toString(searchDept));
            }
            if (criteria.isActiveOnly()) as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");
            searchList.add(as);
        }

        // Search for each set of attributes given in the search list,
        // and combine the results into a single list.
        List<MivPerson> people = new LinkedList<MivPerson>();

        // Get the role searchfilter if we are searching by role.
        SearchFilter<MivPerson> searchFilter = getRoleSearchFilter(criteria);

        // If we are not searching by role, use the input search filter;
        if (searchFilter == null)
        {
            searchFilter = s;
        }

        for (AttributeSet attributeSet : searchList)
        {
            List<MivPerson> found = null;
            // See if we are filtering users by role (for View MIV Dean or Department Chairs report)
            found = userService.findUsers(attributeSet, searchFilter);

            if (found != null && !found.isEmpty())
            {
                people.addAll(found);
            }
        }

        return people;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getUsersBySchool(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersBySchool(MIVUser mivUser, SearchCriteria criteria)
    {
        return this.getUsersBySchool(mivUser.getTargetUserInfo().getPerson(), criteria, null);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getUsersBySchool(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<MivPerson> getUsersBySchool(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s)
    {
        AttributeSet as = new AttributeSet();
        int searchSchool = criteria.getSchoolId();

        // If a specific school was chosen from the dropdown, or one was assigned
        // for a SCHOOL_STAFF search, create a search on just that school.
        if (searchSchool != 0)
        {
            as.put(MivSearchKeys.Person.MIV_SCHOOL_CODE, Integer.toString(searchSchool));
        }

        if (criteria.isActiveOnly())
        {
            as.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");
        }

        List<MivPerson> people = null;

        // See if we are filtering users by role (for View MIV Dean or Department Chairs report)
        SearchFilter<MivPerson> searchFilter = getRoleSearchFilter(criteria);

        // If we are not searching by role, use the input search filter;
        if (searchFilter == null)
        {
            searchFilter = s;
        }

        people = userService.findUsers(as, searchFilter);

        return people;
    }


    /**
     * Get a MivRole search filter based on the search criteria. If there is no role in the SearchCriteria, return null.
     *
     * @param criteria
     * @return SearchFilter or null if none
     */
    private SearchFilter<MivPerson> getRoleSearchFilter(SearchCriteria criteria)
    {
        SearchFilter<MivPerson> searchFilter = null;
        MivRole filterRole = criteria.getRole();

        if (filterRole != null)
        {
            searchFilter = new RoleFilter(filterRole);
        }

        return searchFilter;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getSnapshots(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public SnapshotResultVO getSnapshots(MivPerson person, Dossier dossier) throws WorkflowException
    {
        SnapshotResultVO snapshotResultVO = null;

        List<Snapshot> snapshots = new ArrayList<Snapshot>();

        List<SnapshotDto> snapshotList = snapshotService.getAllSnapshots(dossier);

        // If snapshotList is null or empty than no need to process further
        if (snapshotList == null || snapshotList.isEmpty())
        {
            // set authorized = true so on page we can say that "No snapshots available for this dossier."
            return new SnapshotResultVO(true);
        }

        snapshotResultVO = new SnapshotResultVO(snapshotList);


        // See if this is a joint appointment
        boolean jointAppointment = dossier.getAppointmentAttributes().size() > 1; // Single appointment - not joint appointment

        // The dossiers themselves have already been filtered by department and school but now the individual
        // snapshots for those dossiers must be authorized by the school and department of the viewer.
        final MivPerson mivPerson = person;

        List<SnapshotDto> qualifiedSnapshotsList = filterSnapshots(getSnapshotFilter(mivPerson), snapshotList);


        System.out.println("qualifiedSnapshotsList in DefaultSearchStrategy :: " + qualifiedSnapshotsList);

        for (SnapshotDto snapshotDto : qualifiedSnapshotsList)
        {
            Snapshot snapshot = new Snapshot();

            // Use the insert/updated timestamps for the "sent to" department, school, etc dates
            Date sentToNodeDate = (snapshotDto.getUpdateTimestamp() != null)
                    ? snapshotDto.getUpdateTimestamp()
                    : snapshotDto.getInsertTimestamp();

            // Get the next workflow node which was being routed to
            String snapshotLocation = snapshotDto.getSnapshotLocation();
            // Get the previous locations for this dossier
            List<String> prevLocations = dossier.getPreviousLocationsList();
            // Find the snapshotlocation in the list and move forward one entry to the location routed to
            // or the current location and get the description
            String routedToLocation = dossier.getLocation(); // default to the current location
            if (prevLocations.indexOf(snapshotLocation) + 1 < prevLocations.size())
            {
                routedToLocation = prevLocations.get(prevLocations.indexOf(snapshotLocation) + 1);
            }

            String routedToNodeDesc =
                DossierLocation.mapWorkflowNodeNameToLocation(routedToLocation).getDescription();

            snapshot.setDescription(snapshotDto.getDescription())
                    .setRoutedToNode(routedToNodeDesc + ": " +
                        dossierDateFormat.get().format(sentToNodeDate.getTime()))
                    .setSnapshotTimestamp(timestampFormat.get().format(snapshotDto.getInsertTimestamp())); // copied line from ViewSnapshotsSearchStrategy

            // Get the appointment type, school and department for this snapshot
            String appointmentType = DossierAppointmentType.JOINT.shortDescription;

            // If the department and school id's are not specified, both primary and joint documents may be
            // present in the snapshot. If this is the case and this dossier represents a joint
            // appointment, the type is not relevant
            if ((snapshotDto.getSchoolId() == 0) && (snapshotDto.getDepartmentId() == 0))
            {
                appointmentType = DossierAppointmentType.PRIMARY.shortDescription;
                if (jointAppointment)
                {
                    appointmentType = "";
                }
            }
            // If neither the department or school match the dossier primary department and school, this is
            // a joint appointment
            else if (
                    ((dossier.getPrimarySchoolId() == snapshotDto.getSchoolId()) &&
                     (dossier.getPrimaryDepartmentId() == snapshotDto.getDepartmentId()))
                    ||
                    ((snapshotDto.getDepartmentId() == 0) &&
                     (dossier.getPrimarySchoolId() == snapshotDto.getSchoolId()))
                    )
            {
                appointmentType = DossierAppointmentType.PRIMARY.shortDescription;
            }

            // Get the school and department for this snapshot
            Map<String, String> schoolNameMap = null;
            Map<String, String> deptNameMap = null;

            if (jointAppointment)
            {
                // Note that the school and department maps will not be retrieved in cases where the school and/or
                // department id's are 0. The reason for this is that in the case of a joint appointment, the snapshots
                // at the school and vice provost locations may contain multiple departments and/or schools,
                // respectively.
                // Therefore the department and/or school will be left blank in such cases.
                schoolNameMap = schoolMap.get(snapshotDto.getSchoolId() + "");
                deptNameMap = departmentMap.get(snapshotDto.getSchoolId() + ":" + snapshotDto.getDepartmentId());
            }
            else
            {
                // In the case of non-joint appointment snapshots, we will always show the department and school
                // of the primary appointment.
                schoolNameMap = schoolMap.get(dossier.getPrimarySchoolId() + "");
                deptNameMap = departmentMap.get(dossier.getPrimarySchoolId() + ":" + dossier.getPrimaryDepartmentId());
            }

            String school = schoolNameMap != null ? schoolNameMap.get("description") : "";
            String department = deptNameMap != null ? deptNameMap.get("description") : "";

            // Construct the snapshot file Url.
            // Snapshots are stored in the dossier directory for dossiers which have not been archived.
            // Snapshots are stored in the archive directory for dossiers which have been archived.
            File snapshotDirectory = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);
            String snapshotDescription = DossierLocation.mapWorkflowNodeNameToLocation(snapshotLocation).getDescription()
                    .replaceAll("\'", "&#39;");

            // If this is an archive snapshot, get the archive directory and set the snapshotlocation to be
            // more descriptive where necessary
            if (dossier.getArchivedDate() != null)
            {
                snapshotDirectory = dossier.getArchiveDirectoryByDocumentFormat(DocumentFormat.PDF);

                // If the snapshot location is Archive, add additional description to indicate the type of archive
                if (DossierLocation.mapWorkflowNodeNameToLocation(snapshotLocation) == DossierLocation.ARCHIVE)
                {
                    MivRole snapshotRole = MivRole.valueOf(snapshotDto.getMivRoleId());
                    switch (snapshotRole)
                    {
                        case CANDIDATE:
                            snapshotDescription = snapshotDescription + " (Candidate)";
                            break;
                        case ARCHIVE_USER:
                            snapshotDescription = snapshotDescription + " (Admin)";
                            break;
                        default:
                            snapshotDescription = snapshotDescription + " (Full)";
                            break;
                    }
                }
            }

            File snapshotPath = new File(snapshotDirectory, snapshotDto.getSnapshotFileName());

            snapshot.setSnapshotFileUrl(pathConstructor.getUrlFromPath(snapshotPath))
                    .setSnapshotLocation(snapshotDescription)
                    .setSnapshotAppointment(appointmentType)
                    .setSnapshotSchool(school)
                    .setSnapshotDepartment(department);
            snapshots.add(snapshot);
        }

        if (snapshots != null && snapshotList.size() > 0)
        {
            snapshotResultVO.setQualifiedSnapshotsList(snapshots);
            snapshotResultVO.setAuthorized(true);
        }

        return snapshotResultVO;
    }
    // That's the End of That




    SearchFilter<SnapshotDto> getSnapshotFilter(MivPerson person)
    {
        return new SnapshotFilter(person);
    }

    private class SnapshotFilter extends SearchFilterAdapter<SnapshotDto>
    {
        private final MivPerson mivPerson;

        SnapshotFilter(MivPerson person)
        {
            this.mivPerson = person;
        }
        @Override
        public boolean include(SnapshotDto item)
        {
            // Authorize snapshot viewing based on Location
            AttributeSet permissionDetails = new AttributeSet();
            permissionDetails.put("SNAPSHOTLOCATION", item.getSnapshotLocation());

            // Get the permission required to view this item
            String permission = getSnapshotPermission(item);

            // School, department and role qualifiers
            AttributeSet qualification = new AttributeSet();
            qualification.put(Qualifier.DEPARTMENT, Integer.toString(item.getDepartmentId()));
            qualification.put(Qualifier.SCHOOL, Integer.toString(item.getSchoolId()));
            qualification.put(Qualifier.ROLE, item.getMivRoleId());

            if (authorizationService.isAuthorized(mivPerson, permission, permissionDetails, qualification)) {
                return true;
            }

            return false;
        }
    }

    /**
     * Filter the SnapshotDto list
     *
     * @param filter
     * @param results
     * @return list of filtered SnapshotDto objects
     */
    private List<SnapshotDto> filterSnapshots(SearchFilter<SnapshotDto> filter, List<SnapshotDto> results)
    {
        if (filter != null)
        {
            List<SnapshotDto> snapshots = results;
            results = new LinkedList<>();
            Collection<SnapshotDto> c = filter.apply(snapshots);
            results.addAll(c);
        }

        return results;
    }


    /**
     * Determine the permission necessary to view a snapshot. The permission is determined by the location and role for
     * which it was created.
     *
     * @param SnapshotDto
     * @return permission
     */
    private String getSnapshotPermission(SnapshotDto snapshotDto)
    {

        // Default permission
        String permission = Permission.VIEW_SNAPSHOTS;

        // If the snapshot was created at the Archive location, it is an archive
        if (DossierLocation.mapWorkflowNodeNameToLocation(snapshotDto.getSnapshotLocation())
                .equals(DossierLocation.ARCHIVE))
        {
            // Get the role for which the snapshot was created
            switch (MivRole.valueOf(snapshotDto.getMivRoleId()))
            {
                case ARCHIVE_ADMIN:
                    permission = Permission.VIEW_FULL_ARCHIVES;
                    break;
                case ARCHIVE_USER:
                    permission = Permission.VIEW_ADMIN_ARCHIVES;
                    break;
                case CANDIDATE:
                    // fall-through to default
                default:
                    permission = Permission.VIEW_CANDIDATE_ARCHIVES;
                    break;
            }
        }

        return permission;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getSelectFilter(edu.ucdavis.myinfovault.MIVUser)
     */
    @Override
    public SelectFilter getSelectFilter(MIVUser mivUser)
    {
        return new SelectFilter(mivUser.getUserId(), mivUser.getTargetUserInfo().getPerson().getUserId());
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getQualifiedDossierReviewers(edu.ucdavis.myinfovault.MIVUser, long)
     */
    @Override
    public List<MivDisplayPerson> getQualifiedDossierReviewers(MIVUser mivUser, long dossierId)
            throws WorkflowException
    {
        List<MivDisplayPerson> qualifiedReviewers = userService.findActiveCandidates();

        // Get the dossier owner
        MivPerson person = dossierService.getDossier(dossierId).getAction().getCandidate();
        MivDisplayPerson candidate = new MivDisplayPerson(person);

        // Make sure that the owner of the dossier is not in the reviewers list.
        qualifiedReviewers.remove(candidate);

        return qualifiedReviewers;
    }


    /**
     * Filter the MivPerson list based on the input SearchFilter
     *
     * @param filter
     * @param results
     * @return list of filtered MivPersons
     */
    public List<MivPerson> personFilter(SearchFilter<MivPerson> filter, List<MivPerson> results)
    {
        if (filter != null)
        {
            List<MivPerson> people = results;
            results = new LinkedList<MivPerson>();
            Collection<MivPerson> c = filter.apply(people);
            results.addAll(c);
        }

        return results;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#getDossiersAtLocation(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public List<MivActionList> getDossiersAtLocation(MIVUser user, DossierLocation location)
    {
        MivPerson mivPerson = user.getTargetUserInfo().getPerson();
        List<Dossier> dossiers = new ArrayList<Dossier>();

        try
        {
            dossiers = dossierService.getDossiersByWorkflowLocations(mivPerson, location);
        }
        catch (WorkflowException e)
        {
            logger.warn("getDossiersAtLocation: got a WorkflowException doing getDossiersAtLocation.");
        }

        List<MivActionList> people = new ArrayList<MivActionList>();
        for (Dossier ds : dossiers)
        {
            String theDossierLocation = ds.getLocation();
            // Get the attributes for the primary appointment only
            DossierAppointmentAttributes dossierAppointmentAttributes =
                    ds.getAppointmentAttributes().get(ds.getPrimaryAppointmentKey());
            MivActionList mal = fillDossierPerson(ds, theDossierLocation, dossierAppointmentAttributes, 0, 0);
            if (mal != null)
            {
                people.add(mal);
            }
        }

        return people;
    }


    /**
     * Matches the user name in MivActionList with given user name.
     *
     * @param actionList
     * @param name
     * @return
     */
    public/* static */boolean matchMivActionListUserName(MivActionList actionList, String name)
    {
        final String[] parsedName = StringUtil.prepareTextSearchCriteria(name, " ").split(" ");
        for (String namepart : parsedName)
        {
            if (!(actionList.getGivenName().toLowerCase().indexOf(namepart) != -1 ||
                    actionList.getSurname().toLowerCase().indexOf(namepart) != -1 ||
                    actionList.getSortName().toLowerCase().indexOf(namepart) != -1 || actionList.getDisplayName()
                    .toLowerCase().indexOf(namepart) != -1))
            {
                // Stop matching and return as soon as we've unmatched something
                return false;
            }
        }

        return true;
    }


    class RoleFilter extends SearchFilterAdapter<MivPerson>
    {
        private final MivRole[] includeRoles;


        public RoleFilter(MivRole... filterRoles)
        {
            this.includeRoles = filterRoles;
        }

        /*
         * (non-Javadoc)
         * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
         */
        @Override
        public boolean include(MivPerson item)
        {
            return item.hasRole(includeRoles);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy#dossierSearchByCriteria(java.util.Map, edu.ucdavis.myinfovault.MIVUser)
     */
    @Override
    public List<MivActionList> dossierSearchByCriteria(Map<String, String> allCriteria, MIVUser user)
    {
        final int schoolId = Integer.parseInt(allCriteria.get("schoolId"));
        final int deptId = Integer.parseInt(allCriteria.get("departmentId"));
        MivPerson mivPerson = user.getTargetUserInfo().getPerson();

        boolean showAll = true;
        int[] excludeList = null;

        List<Dossier> dossiers = new ArrayList<Dossier>();
        try
        {
            dossiers = dossierService.getDossiersWithCriteria(allCriteria);
        }
        catch (WorkflowException e)
        {
            logger.warn("dossierSearchByCriteria: got a WorkflowException doing getDossiersWithCriteria using the following criteria : "
                    + allCriteria.toString());
        }

        // Associate people with the dossiers
        List<MivActionList> people = findPeople(dossiers, mivPerson, showAll, schoolId, deptId, excludeList);
        return people;
    }

}
