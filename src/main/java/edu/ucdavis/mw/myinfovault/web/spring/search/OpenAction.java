/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: OpenAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.webflow.core.collection.ParameterMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequestActionType;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.events2.DecisionHoldEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionReleaseEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierReturnEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierReviewEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierRouteEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierVoteEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode;

/**
 * Contains the methods used to support various actions executed in the web flow.
 *
 * @author Mary Northup
 * @since MIV 3.0
 */
public class OpenAction extends DossierAction
{
    private static final String[] breadcrumbs = {"Open Action"};

    //URL parameters
    private static final String SCHOOL_PARAM = "schoolId";
    private static final String DEPARTMENT_PARAM = "departmentId";
    private static final String DOSSIER_PARAM = "dossierId";
    private static final String NEXTNODE_PARAM = "nextNode";
    private static final String OPENREVIEW_MSG = "A review period is open for this action. Changing the dossier by saving edits to the Action Form will automatically close the review period.";
    private static final String DCREQUESTED_MSG = "A candidate disclosure certificate has been released for signature. Changing the dossier by editing the Action Form will automatically cancel the signature request.  A notification email will be sent to the candidate alerting them to the rescission of the signature request.";
    private static final String DCSIGNED_MSG = "A candidate disclosure certificate (CDC) has already been signed. Changing the dossier by editing the Action Form will automatically invalidate the CDC signature and a new CDC will have to be signed before the dossier can be routed.";
    private static final String JOINTREVIEW_MSG = "A joint review period is open in subdept. If you edit and save changes to the Action Form, the joint review period will close automatically. Please contact subdept so that their administrators may reopen the review period when changes to the Action Form are complete.";
    private static final String JOINTDCREQUESTED_MSG = "A joint candidate disclosure certificate has been released for signature. Changing the dossier by editing the Action Form will automatically cancel the signature request.  A notification email will be sent to the candidate alerting them to the rescission of the signature request.";
    private static final String JOINTDCSIGNED_MSG = "A joint candidate disclosure certificate (CDC) has already been signed in subdept. Changing the dossier by editing the Action Form may automatically invalidate the CDC signature and a new CDC will have to be signed before the dossier can be routed. Please contact subdept so that their administrators may request a new signature on the CDC.";

    /**
     * Instantiate the dossier open action form action bean.
     *
     * @param searchStrategy Spring-injected search strategy
     * @param authorizationService Spring-injected authorization service
     * @param userService Spring-injected user service
     * @param dossierService Spring-injected dossier service
     */
    public OpenAction(SearchStrategy searchStrategy,
                      AuthorizationService authorizationService,
                      UserService userService,
                      DossierService dossierService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService);
    }

    /**
     * Add the document attribute name contained in the previous event ID to the upload request.
     * Delete the PDF upload for the document attribute name unless the button selected was "add".
     *
     * @param context the context associated with the current web flow.
     * @return either success or failure, used by web flow to control the flow execution.
     */
    public Event setPdfUpload(RequestContext context)
    {
        String[] parsedState = context.getCurrentEvent().getId().split("-");
        String documentAttributeName = parsedState[0];

        // set up to send to PDF upload sub-flow
        context.getRequestScope().put("documentAttributeName", documentAttributeName);

        // continue to upload sub-flow if key clicked was "add"
        if ("add".equalsIgnoreCase(getParameterValue(context, "_eventId_" + documentAttributeName))) return success();

        // JSP loop counter starts at 1 not 0
        int documentOccurance = parsedState.length > 1 ? (Integer.parseInt(parsedState[1]) - 1) : 0;

        // Delete this PDF
        dossierService.deleteUploadFile(getDossier(context),
                                        getParameterValue(context, SCHOOL_PARAM, 0),
                                        getParameterValue(context, DEPARTMENT_PARAM, 0),
                                        documentAttributeName,
                                        documentOccurance,
                                        getRealPerson(context),
                                        getShadowPerson(context));

        // return to manage open action page
        return error();
    }

    public Event packetRequest(RequestContext context)
    {
        // for a Button click event, parmName is the text of the button
        String parmName = getParameterValue(context, "_eventId_" + context.getCurrentEvent().getId()).toUpperCase();
        DossierAttributeStatus status = DossierAttributeStatus.valueOf(parmName);
        context.getConversationScope().put("requestAction", status == DossierAttributeStatus.REQUEST ? PacketRequestActionType.REQUESTED : PacketRequestActionType.CANCELED);
        return success();
    }

    /**
     * <p>Method to update the action, any action (voting or reviewing).  The specific action is determined by the last Event
     * since there are different events for each action type.  The open or close action is determine by the button pressed.
     * The StateId passed in here from the request is actually the dossierAttriuteName and can be used to match
     * the appropriate dossierAttribute to update.</p>
     *
     * <p>Previously had assumed the only possible states are "OPEN" and "CLOSED"</p>
     *
     * @param context - the context associated with the current web flow.
     * @return Event - either success or failure, used by web flow to control the flow execution.
     */
    public Event setState(RequestContext context)
    {
        MivPerson realPerson = getRealPerson(context);
        MivPerson shadowPerson = getShadowPerson(context);

        String attributeKey = context.getCurrentEvent().getId();

        // for a Button click event, parmName is the text of the button
        String parmName = getParameterValue(context, "_eventId_" + attributeKey).toUpperCase();
        context.getFlowScope().remove("actionMsg");

        try
        {
            Dossier dossier = getDossier(context);
            DossierAttributeStatus status = DossierAttributeStatus.valueOf(parmName);
            DossierAttribute attribute = getDossierAttribute(dossier, getDossierAppointmentKey(context), attributeKey);

            if (!setDossierAttributeStatus(dossier,
                                           attribute,
                                           status))
            {
                return error();//an error occurred
            }

            /*
             * Handle review, vote, release, and hold events.
             */
            if (attribute.getAttributeName().equalsIgnoreCase("review"))
            {
                EventDispatcher2.getDispatcher().post(
                        new DossierReviewEvent(realPerson,
                                               dossier,
                                               attribute.getLocation(),
                                               status).setShadowPerson(shadowPerson));
            }
            else if (attribute.getAttributeName().equalsIgnoreCase("vote"))
            {
                EventDispatcher2.getDispatcher().post(
                        new DossierVoteEvent(realPerson,
                                             dossier,
                                             attribute.getLocation(),
                                             status).setShadowPerson(shadowPerson));
            }
            else if (attribute.getAttributeName().contains("release"))
            {
                if (DossierAttributeStatus.HOLD == status)
                {
                    EventDispatcher2.getDispatcher().post(
                        new DecisionHoldEvent(realPerson,
                                              dossier,
                                              new Scope(attribute.getSchoolId(),
                                                        attribute.getDeptId()),
                                              attribute.getAttributeName()
                        ).setShadowPerson(shadowPerson));
                }
                else if (DossierAttributeStatus.RELEASE == status)
                {
                    EventDispatcher2.getDispatcher().post(
                        new DecisionReleaseEvent(realPerson,
                                                 dossier,
                                                 new Scope(attribute.getSchoolId(),
                                                           attribute.getDeptId()),
                                                 attribute.getAttributeName()
                    ).setShadowPerson(shadowPerson));
                }
            }
        }
        catch (IllegalArgumentException e)
        {
            // The param didn't match an expected Action state (Open, Close, Hold, Release)
            logger.error("Unknown action state \"" + parmName + "\"", e);
        }

        return success();
    }


    /**
     * Method to route the dossier to the next node.
     *
     * @param context - the context associated with the current web flow.
     * @return Event - either success or failure, used by web flow to control the flow execution.
     */
    public Event routeDossier(RequestContext context)
    {
        final MivPerson realPerson = getRealPerson(context);
        final int realPersonId = realPerson.getUserId();
        final MivPerson shadowPerson = getShadowPerson(context);

        final String dossierParam = context.getExternalContext().getRequestParameterMap().get(DOSSIER_PARAM);
        final String nextNode = context.getExternalContext().getRequestParameterMap().get(NEXTNODE_PARAM);
        final String lastDossierParam = context.getConversationScope().getString("dossierParam");

        // MIV-4572 : Immediately track and log all "route dossier" clicks regardless of success / failure / validity
        logger.debug("User {}{} clicked the \"route\" button/link to route a dossier. Parameters are: dossierParam={} / lastDossierParam={} / nextNode={}",
                    new Object[] {
                        realPersonId,
                        (shadowPerson.getUserId() != realPersonId ?
                                ", acting as " + shadowPerson.getUserId() + "," : ""),
                        dossierParam, lastDossierParam, nextNode
                    }
                );

        Dossier dossier = getDossier(context);

        context.getFlowScope().put("lastTransaction","routerequest");
        context.getConversationScope().put("dossierParam", dossierParam);

        final DossierLocation nextLocation = DossierLocation.mapWorkflowNodeNameToLocation(nextNode);

        try
        {
            // Get the current location from the flow scope
            String currentNodeName = context.getFlowScope().getString("currentNodeName");
            // Use the current location from the flowscope for validation since the actual location may have been changed
            // by a previous routing event.
            DossierLocation currentLocation = DossierLocation.mapWorkflowNodeNameToLocation(currentNodeName);

            logger.info("_AUDIT : User {}{} requested to route Dossier #{} from the {} to the {} location",
                            new Object[] {
                                realPersonId,
                                (shadowPerson.getUserId() != realPersonId ?
                                        ", acting as " + shadowPerson.getUserId() + "," : ""),
                                dossier.getDossierId(),
                                currentNodeName, nextLocation.getDescription()
                            }
                        );

            dossier.setRoutingPerson(shadowPerson);
            final Dossier routedDossier = dossierService.approveDossier(dossier, null, nextLocation, currentLocation);

            context.getFlowScope().put("actionMsg", "The following action has been sent to the " + nextLocation.getDescription() + ".");
            context.getConversationScope().put("lastMsg", "The following action has been sent to the " + nextLocation.getDescription() + ".");

            logger.info("routeDossier: {} is Routing dossier == {} for candidate {}",
                        new Object[] { shadowPerson.getDisplayName(), dossier.getDossierId(), routedDossier.getAction().getCandidate().getUserId() });

            EventDispatcher2.getDispatcher().post( new DossierRouteEvent(
                    realPerson,
                    routedDossier,
                    currentLocation,
                    nextLocation
                ).setShadowPerson(shadowPerson));
        }
        catch (WorkflowException e)
        {
            logger.warn("routeDossier: failed to ROUTE dossier == " + dossier.getDossierId() +
                        " with the following workflowError -> " + e.getMessage(), e);
            context.getFlowScope().put("actionMsg", "The action has not been routed due to a workflow error.");
            context.getFlowScope().put("actionError", "failed");
        }

        return success();
    }


    /**
     * Method to return the dossier to the previous node (includes return to joint appointment department).
     *
     * @param context the context associated with the current web flow.
     * @return either success or failure, used by web flow to control the flow execution.
     */
    public Event returnDossier(RequestContext context)
    {
        final MivPerson realPerson = getRealPerson(context);
        final MivPerson shadowPerson = getShadowPerson(context);

        ParameterMap params = context.getExternalContext().getRequestParameterMap();
        final String dossierParam = params.get(DOSSIER_PARAM);
        final String lastDossierParam = context.getConversationScope().getString("dossierParam");

        // MIV-4572 : Immediately track and log all "return dossier" clicks regardless of success / failure / validity
        logger.debug("User {}{} clicked the \"return\" button/link to return a dossier. Parameters are: dossierParam={} / lastDossierParam={}",
                    new Object[] {
                        realPerson.getUserId(),
                        (shadowPerson.getUserId() != realPerson.getUserId() ?
                                ", acting as " + shadowPerson.getUserId() + "," : ""),
                        dossierParam, lastDossierParam
                    }
                );

        Dossier dossier = getDossier(context);

        context.getFlowScope().put("lastTransaction", "routerequest");
        context.getConversationScope().put("dossierParam", dossierParam);

        String previousNodeName = context.getFlowScope().getString("previousNodeName");
        DossierLocation previousLocation = DossierLocation.mapWorkflowNodeNameToLocation(previousNodeName);
        WorkflowNode previousWorkflowNode = null;

        try
        {
            previousWorkflowNode = dossierService.getWorkflowNodeForLocation(dossier, previousLocation);
            final String currentLocation = dossier.getLocation();

            logger.info("_AUDIT : User {}{} requested to return Dossier #{} from the {} to the {} location",
                            new Object[] {
                                realPerson.getUserId(),
                                (shadowPerson.getUserId() != realPerson.getUserId() ?
                                        ", acting as " + shadowPerson.getUserId() + "," : ""),
                                dossier.getDossierId(),
                                currentLocation, previousNodeName
                            }
                        );

            // Loop through all the departments attributes for the dossier
            for (DossierAppointmentAttributeKey apppointmentKey : dossier.getAppointmentAttributes().keySet())
            {
                Map<String, DossierAttribute> attributes = dossier.getAttributesByAppointment(apppointmentKey).getAttributes();

                ArrayList <DossierAttribute> attribList = new ArrayList<DossierAttribute>();

                if (previousWorkflowNode.getNodeLocation() == DossierLocation.SCHOOL ||
                    previousWorkflowNode.getNodeLocation() == DossierLocation.POSTSENATESCHOOL)
                {
                    // From the VP back to the school - remove vice provost, provost and chancellor release attribute(s)
                    attribList.add(attributes.get("vp_release"));
                    attribList.add(attributes.get("vp_recommendation_release"));
                    attribList.add(attributes.get("p_recommendation_release"));
                    attribList.add(attributes.get("p_tenure_release"));
                    attribList.add(attributes.get("p_release"));
                    attribList.add(attributes.get("chancellor_release"));
                }
                // From the federationschool back to the federatioviceprovost or
                // from the postsenateschool back to a senate location
                else if (previousWorkflowNode.getNodeLocation() == DossierLocation.SENATE_OFFICE ||
                         previousWorkflowNode.getNodeLocation() == DossierLocation.FEDERATION ||
                         previousWorkflowNode.getNodeLocation() == DossierLocation.SENATEFEDERATION)
                {
                    // Remove dean_release attribute for the location we just left
                    attribList.add(attributes.get("dean_release"));
                    attribList.add(attributes.get("dean_recommendation_release"));
                    attribList.add(attributes.get("joint_dean_release"));

                    // Must also remove remove vice provost, provost and chancellor release attribute(s), but must load for the senate
                    // locations since the senate location is where we are going...
                    switch(previousLocation)
                    {
                        case SENATE_OFFICE:
                        case FEDERATION:
                        case SENATEFEDERATION:
                            dossierService.loadDossierAttributesForLocation(dossier, previousLocation);
                            break;
                        default:
                            throw new WorkflowException("Attempting to return a dossier to and invalid previous location: "+previousLocation);
                    }
                    attribList.add(attributes.get("vp_release"));
                    attribList.add(attributes.get("vp_recommendation_release"));
                    attribList.add(attributes.get("p_recommendation_release"));
                    attribList.add(attributes.get("p_tenure_release"));
                    attribList.add(attributes.get("p_release"));
                    attribList.add(attributes.get("chancellor_release"));
                }
                else if (DossierLocation.DEPARTMENT == previousLocation)
                {
                    // From the school back to the department - remove dean_release attribute(s)
                    attribList.add(attributes.get("dean_release"));
                    attribList.add(attributes.get("dean_recommendation_release"));
                    attribList.add(attributes.get("joint_dean_release"));
                }
                // If we got an attribute we're going to null it out.
                for (DossierAttribute attrib : attribList) {
                    if (attrib != null)
                    {
                        logger.info("Got attribute [{}] with value [{}] which we should now remove.",
                                    attrib.getAttributeDescription(), attrib.getAttributeValue());
                        attrib.setAttributeValue(null);
                    }
                }
            }

            final String annotation = "returning this dossier";

            /*
             * Use the current location from the flow scope for validation since the
             * actual location may have been changed by a previous routing event.
             */
            final DossierLocation currentDossierLocation = DossierLocation.mapWorkflowNodeNameToLocation(
                                                               context.getFlowScope().getString("currentNodeName"));

            dossierService.returnDossierToPreviousNode(shadowPerson,
                                                       realPerson,
                                                       dossier,
                                                       annotation,
                                                       previousLocation,
                                                       currentDossierLocation);

            final String msg = "The following action has been returned to the " + previousLocation.getDescription() + ".";

            context.getFlowScope().put("actionMsg", msg);
            context.getConversationScope().put("lastMsg", msg);

            logger.info("returnDossier: {} is Returning dossier == {}", shadowPerson.getDisplayName(), dossier.getDossierId());

            EventDispatcher2.getDispatcher().post( new DossierReturnEvent(
                    realPerson,
                    dossier,
                    currentDossierLocation,
                    previousLocation
                ).setShadowPerson(shadowPerson));

        }
        catch (WorkflowException e)
        {
            logger.warn("returnDossier: failed to ROUTE dossier == " + dossier.getDossierId() +
                      " with the following workflowError -> " + e.getMessage(), e);

            context.getFlowScope().put("actionMsg",
                    "The following action cannot be returned due to an error in routing.");
            context.getFlowScope().put("actionError", "failed");

            return error();
        }

        return success();
    }


    /**
     * Method to route the joint appointment dossier to the primary (joint is complete!).
     *
     * @param context the context associated with the current web flow
     * @return either success or failure, used by web flow to control the flow execution
     */
    public Event sendJoint(RequestContext context)
    {
        return routeJoint(context, true);
    }


    /**
     * Method to return the joint appointment dossier back to the joint department (joint is no-longer complete!).
     *
     * @param context the context associated with the current web flow
     * @return either success or failure, used by web flow to control the flow execution
     */
    public Event returnJoint(RequestContext context)
    {
        return routeJoint(context, false);
    }

    /**
     * Method to set the joint appointment "is complete" flag. Effectively
     * routing or sending back the joint appointment to the primary.
     *
     * @param context the context associated with the current web flow
     * @return either success or failure, used by web flow to control the flow execution
     */
    private Event routeJoint(RequestContext context, boolean isComplete)
    {
        final int shadowUserId = getShadowPerson(context).getUserId();
        final int realPersonId = this.getRealPerson(context).getUserId();

        Dossier dossier = getDossier(context);

        final DossierAppointmentAttributeKey jointAppointmentKey = getDossierAppointmentKey(context);

        logger.info(isComplete
                  ? "_AUDIT : User {}{} is releasing joint dossier from school {}, department {} to the primary"
                  : "_AUDIT : User {}{} is sending the dossier back to joint school {}, department {}",
                    new Object[] {
                        realPersonId,
                        (shadowUserId != realPersonId ? ", acting as " + shadowUserId + "," : ""),
                        jointAppointmentKey.getSchoolId(),
                        jointAppointmentKey.getDepartmentId()
                    }
                );

        dossier.setIsAppointmentComplete(jointAppointmentKey, isComplete);

        try
        {
            dossierService.saveDossier(dossier);
        }
        catch (WorkflowException e)
        {
            logger.warn((isComplete ? "sendJoint" : "returnJoint")
                      + ": failed to save the Completed flag in dossier == "
                      + dossier.getDossierId()
                      + " exception -> "
                      + e.getMessage(), e);

            return error();
        }

        return success();
    }

    /**
     * Determine if the dossier has been routed before the current transaction execution.
     *
     * @param context WebFlow request context
     * @return WebFlow event status; success on valid, error otherwise
     */
    public Event validateContinue(RequestContext context)
    {
        String dossierParam = context.getExternalContext().getRequestParameterMap().get(DOSSIER_PARAM);

        //MIV-3387 to prevent ANY transaction execution after a dossier has been routed
        String lastDossierParam = context.getConversationScope().getString("dossierParam");

        if (dossierParam.equals(lastDossierParam))
        {
            logger.warn("validateContinue: dossier has been routed dossier == {} but a new request has come through??", dossierParam);
            context.getFlowScope().put("lastTransaction","routerequest");
            context.getFlowScope().put("actionMsg", context.getConversationScope().get("lastMsg"));
            context.getFlowScope().put("displayAppointmentDetails", false); // default to no display appointment details
            context.getFlowScope().put("displayOnly", true); // this will prevent the display of the links on the page
            return error();
        }

        return success();
    }

    /**
     * isConfirmRequired will check the various dossier states that have been identified as needing user confirmation to continue
     *          If the review period is open requires user confirmation to continue on to edit the RAF/Academic Action form.
     *          If the disclosure certificate is "signed" or "requested" then a user confirmation is required to continue to edit the RAF.
     * @param context
     * @return success() means that confirmation is required, error() means continue without confirmation
     */
    public Event isConfirmRequired(RequestContext context)
    {
        Dossier dossier = getDossier(context);

        /*
         * Only confirming potential CDC invalidation at the department.
         */
        if (dossier.getDossierLocation() != DossierLocation.DEPARTMENT ||
            dossier.getAction().getActionType() == DossierActionType.APPOINTMENT)
        {
            return error();
        }

        ArrayList <String> displayMsgs = new ArrayList<String>();

        Map <DossierAppointmentAttributeKey, DossierAppointmentAttributes> dossierAppointmentAttributes = dossier.getAppointmentAttributes();
        for (DossierAppointmentAttributeKey apptKey : dossierAppointmentAttributes.keySet())
        {
            if (dossier.getPrimaryAppointmentKey().equals(apptKey))
            {
                DossierAttributeStatus dcstatus = dossier.getAttributeByAppointment(apptKey, "disclosure_certificate").getAttributeValue();
                boolean isSigned = DossierAttributeStatus.SIGNED == dcstatus || DossierAttributeStatus.SIGNEDIT == dcstatus;
                boolean isRequested = DossierAttributeStatus.REQUESTED == dcstatus || DossierAttributeStatus.REQUESTEDIT == dcstatus;
                if (isSigned || isRequested)
                {
                    displayMsgs.add(isSigned ? DCSIGNED_MSG : DCREQUESTED_MSG);
                }
                if (dossier.getAttributeByAppointment(apptKey, "review").getAttributeValue() == DossierAttributeStatus.OPEN)
                {
                    displayMsgs.add(OPENREVIEW_MSG);
                }
            }
            else
            {
                DossierAttributeStatus jstatus = dossier.getAttributeByAppointment(apptKey, "j_disclosure_certificate").getAttributeValue();
                boolean isJointSigned = DossierAttributeStatus.SIGNED == jstatus || DossierAttributeStatus.SIGNEDIT == jstatus;
                boolean isJointRequested = DossierAttributeStatus.REQUESTED == jstatus || DossierAttributeStatus.REQUESTEDIT == jstatus;
                if (isJointSigned || isJointRequested)
                {
                    displayMsgs.add(isJointSigned ? JOINTDCSIGNED_MSG.replaceAll("subdept", dossier.getAttributesByAppointment(apptKey).getDepartmentDescription()) : JOINTDCREQUESTED_MSG);
                }
                if (dossier.getAttributeByAppointment(apptKey, "review").getAttributeValue() == DossierAttributeStatus.OPEN)
                {
                    displayMsgs.add(JOINTREVIEW_MSG.replaceAll("subdept", dossier.getAttributesByAppointment(apptKey).getDepartmentDescription()));
                }
            }
        }
        context.getFlowScope().put("messages",displayMsgs);

        return !displayMsgs.isEmpty() ? success() : error();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#loadProperties(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public void loadProperties(RequestContext context)
    {
        super.loadProperties(context, "strings", "labels", "tooltips", "constants");
    }


    /**
     * Add bread crumbs for open action as parent flow
     *
     * @param context Webflow request context
     */
    public void addBreadcrumbs(RequestContext context)
    {
        addBreadcrumbs(context, breadcrumbs);
    }

    /**
     * remove bread crumbs for open action as parent flow
     *
     * @param context Webflow request context
     */
    public void removeBreadcrumbs(RequestContext context)
    {
        removeBreadcrumbs(context, breadcrumbs.length);
    }


    /**
     * Get dossier appointment key from the 'schoolId' and 'departmentId' URL parameters.
     *
     * @param context Webflow request context
     * @return The dossier appointment key containing school and department IDs
     */
    protected DossierAppointmentAttributeKey getDossierAppointmentKey(RequestContext context)
    {
        return new DossierAppointmentAttributeKey(getParameterValue(context, SCHOOL_PARAM, 0),
                                                  getParameterValue(context, DEPARTMENT_PARAM, 0));
    }
}
