/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: OpenActionReportFormAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.util.HtmlUtils;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.ReportUtil;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.valuebeans.report.ReportVO;
import edu.ucdavis.mw.myinfovault.valuebeans.report.ResultCellVO;
import edu.ucdavis.mw.myinfovault.web.spring.search.ReportSearchCriteria.CheckboxCriteria;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Contains the methods used to support various actions executed in the web flow.
 *
 * @author Mary Northup
 * @since MIV 4.5
 */
public class OpenActionReportFormAction extends DossierAction
{
    private static final String[] breadcrumbs = { "View MIV Open Actions Report: Search", "Search Results" };

    public OpenActionReportFormAction(SearchStrategy searchStrategy,
            AuthorizationService authorizationService,
            UserService userService,
            DossierService dossierService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService);
    }


    /**
     * This is the main search processor for all the Open Actions Report searches.
     *
     * @param criteria main search form containing the search "criteria" requested
     * @param user MIVUser requesting the search
     * @param location DossierLocation that is the originating "location" of the searcher
     * @param request
     * @return List of ReportVO objects
     */
    public List<MivActionList> dossierSearch(ReportSearchCriteria criteria,
    		MIVUser user,
    		DossierLocation location,
    		CheckboxCriteria request)
    		{
    	List<MivActionList> actionList = new ArrayList<MivActionList>();
    	String schoolId = String.valueOf(criteria.getSchoolId());
    	String departmentId = String.valueOf(criteria.getDepartmentId());

    	if (request == null)
    	{
    		actionList = this.getDossiers(criteria, user, location, null);
    	}

    	else
    	{
    		Map<String, String> allCriteria = new HashMap<String, String>();
    		allCriteria.put("LOCATION", location.mapLocationToWorkflowNodeName());
    		allCriteria.put("schoolId", schoolId);
    		allCriteria.put("departmentId", departmentId);
    		// for (CheckboxCriteria request : criteria.getSearchRequests())
    		// {
    		switch (request)
    		{
    		case DC:
    			allCriteria.put("disclosure_certificate", "NOT_SIGNED");
    			break;
    		case Dean:
    			allCriteria.put("deans_final_decision", "NOT_ADDED");
    			allCriteria.put("deans_recommendation", "NOT_ADDED");
    			allCriteria.put("%dean%_release", "RELEASE");
    			break;
    		case ORF:
    			location = DossierLocation.FEDERATION;
    			allCriteria.put("LOCATION", location.mapLocationToWorkflowNodeName());
    			allCriteria.put("review", "OPEN");
    			break;
    		case ORS:
    			location = DossierLocation.SENATE_OFFICE;
    			allCriteria.put("LOCATION", location.mapLocationToWorkflowNodeName());
    			allCriteria.put("review", "OPEN");
    			break;
    		case ORSF:
    			location = DossierLocation.SENATEFEDERATION;
    			allCriteria.put("LOCATION", location.mapLocationToWorkflowNodeName());
    			allCriteria.put("review", "OPEN");
    			break;
    		case OR:
    			allCriteria.put("review", "OPEN");
    			break;
                case OPR:
                        location = DossierLocation.PACKETREQUEST;
                        allCriteria.put("packet_request", "");
                        break;
 		case IJA:
    			allCriteria.put("joint", "incomplete");
    			break;
    		case AllFed:
    		case ReadyFed:
    			location = DossierLocation.FEDERATION;
    			allCriteria.put("LOCATION", location.mapLocationToWorkflowNodeName());
    			allCriteria.put("dossier", "ready");
    			break;
    		case AllSenate:
    		case ReadySenate:
    			location = DossierLocation.SENATE_OFFICE;
    			allCriteria.put("LOCATION", location.mapLocationToWorkflowNodeName());
    			allCriteria.put("dossier", "ready");
    			break;
    		case AllSenateFed:
    		case ReadySenateFed:
    			location = DossierLocation.SENATEFEDERATION;
    			allCriteria.put("LOCATION", location.mapLocationToWorkflowNodeName());
    			allCriteria.put("dossier", "ready");
    			break;
    		case Audit:
    			/**
    			 * really these are "Ready" but from Academic Affairs looking at the school
    			 */
    			location = DossierLocation.SCHOOL;
    			allCriteria.put("LOCATION", location.mapLocationToWorkflowNodeName());
    			allCriteria.put("dossier", "ready");
    			break;
    		case Ready:
    			allCriteria.put("dossier", "ready");
    			break;
    		case VP:
    			allCriteria.put("decision_or_recommendation", "NOT_ADDED");
    			allCriteria.put("vp%_release", "RELEASE");
    			break;
    		}

    		if (allCriteria.containsKey("packet_request") || allCriteria.containsKey("dossier") || allCriteria.containsKey("joint") || allCriteria.containsKey("vp%_release"))
    		{
    			actionList = this.getDossiers(criteria, user, location, request);
    		}
    		else
    		{
    			actionList = this.searchStrategy.dossierSearchByCriteria(allCriteria, user);
    		}
    		// If they requested the dean_release then we need to refine the list once more.
    		// It turns out that some dossiers have BOTH deans_decision and deans_recommendation attributes so the
    		// sql statement will select them but the dossier service will let us know if this dossier is truly
    		// "not added".
    		// Also, for some reason the vice_provosts_final_decision (dossier attribute) null,
    		// but we found a decision/signature (so was added).
    		// So this is where we remove those extra ones
    		List<MivActionList> removeThese = new ArrayList<MivActionList>();
    		if (allCriteria.containsKey("%dean%_release")
    				|| allCriteria.containsKey("vp%_release"))
    		{
    			for (MivActionList al : actionList)
    			{
    				if (al.getDecisionStatus().equalsIgnoreCase("added")
    						|| al.getReleasedStatus().equalsIgnoreCase("hold"))
    				{
    					removeThese.add(al);
    				}
    			}
    		}
    		// also there are disclosure certificate attributes out there that do not indicate they are "signed"
    		// when they are
    		else if (allCriteria.containsKey("disclosure_certificate"))
    		{
    			for (MivActionList al : actionList)
    			{
    				if (al.getDisclosureStatus().equalsIgnoreCase("signed"))
    				{
    					removeThese.add(al);
    				}
    			}
    		}
    		// now remove any lines we really shouldn't display
    		if (removeThese.size() > 0)
    		{
    			actionList.removeAll(removeThese);
    		}
    	}

    	// we don't show "joint" at the vice provost level or any Senate locations either
    	if (location == DossierLocation.VICEPROVOST ||
    			location == DossierLocation.FEDERATION ||
    			location == DossierLocation.SENATE_OFFICE ||
    			location == DossierLocation.SENATEFEDERATION)
    	{
    		List<MivActionList> removeThese = new ArrayList<MivActionList>();
    		for (MivActionList al : actionList)
    		{
    			if (al.getAppointmentType().equalsIgnoreCase("joint"))
    			{
    				removeThese.add(al);
    			}
    		}

    		if (removeThese.size() > 0)
    		{
    			actionList.removeAll(removeThese);
    		}
    	}

    	return actionList;
    		}

    /**
     * Get dossier report for the given search criteria and user.
     *
     * @param criteria main search form containing the search "criteria" requested
     * @param user MIVUser requesting the search
     * @return dossier report
     */
    public ReportVO getReport(ReportSearchCriteria criteria, MIVUser user)
    {
        List<MivActionList> actionList = new ArrayList<MivActionList>();

        DossierLocation location = criteria.getMethod().getLocation();

        if (criteria.getSearchRequests().length == 0)
        {
            actionList = this.dossierSearch(criteria, user, location, null);
        }
        else
        {
            for (CheckboxCriteria request : criteria.getSearchRequests())
            {
                actionList.addAll(this.dossierSearch(criteria, user, location, request));
            }
        }

        if (criteria.getMethod() == SearchMethod.SEARCHSENATE
         && criteria.getSearchRequests().length == 0)
        {
            actionList.addAll(this.dossierSearch(criteria, user, DossierLocation.FEDERATION, null));
            actionList.addAll(this.dossierSearch(criteria, user, DossierLocation.SENATEFEDERATION, null));
        }
        else if (criteria.getMethod() == SearchMethod.SEARCHACADEMIC
              && !actionList.isEmpty()
              && actionList.get(0).getActionLocation().equalsIgnoreCase(DossierLocation.SCHOOL.name()))
        {
            location = DossierLocation.SCHOOL;
        }

        return prepareResult(actionList, location, criteria);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Event setupSearchForm(RequestContext context)
    {
        MivPerson actor = getShadowPerson(context);

        SearchCriteriaCheckboxes displayCriteria = null;

        // choose search criteria checkbox configuration based on primary role
        switch (actor.getPrimaryRoleType())
        // for testing
        {
            case CANDIDATE:
                // Candidates will get here if they are a DEAN or DEPT CHAIR or VICEPROVOST
                if (actor.hasRole(MivRole.VICE_PROVOST))
                {
                    displayCriteria = SearchCriteriaCheckboxes.getAcademicCriteria();
                }
                else if (actor.hasRole(MivRole.DEAN))
                {
                    displayCriteria = SearchCriteriaCheckboxes.getSchoolCriteria();
                }
                else if (actor.hasRole(MivRole.DEPT_CHAIR))
                {
                    displayCriteria = SearchCriteriaCheckboxes.getDepartmentCriteria();
                }
                break;
            case DEPT_ASSISTANT:
            case DEPT_STAFF:
                displayCriteria = SearchCriteriaCheckboxes.getDepartmentCriteria();
                break;
            case SCHOOL_STAFF:
                displayCriteria = SearchCriteriaCheckboxes.getSchoolCriteria();
                break;
            case SENATE_STAFF:
                displayCriteria = SearchCriteriaCheckboxes.getSenateCriteria();
                break;
            case SYS_ADMIN:
            case VICE_PROVOST_STAFF:
                displayCriteria = SearchCriteriaCheckboxes.getAcademicCriteria();
                break;
            default:
                displayCriteria = new SearchCriteriaCheckboxes();
                break;
        }

        context.getFlashScope().put("displayCriteria", displayCriteria);

        return super.setupSearchForm(context);
    }


    /**
     * Sets properties for use by the userlist.jsp so that the breadcrumbs and titles display the appropriate headings.
     *
     * @param context
     *            - the context associated with the current web flow.
     * @return Event - either success or failure, used by web flow to control the flow execution.
     * @throws Exception
     *             - not sure about this, may force the "catch-all" exception handler".
     */
    @Override
    public Event pageSetup(RequestContext context) throws Exception
    {
        logger.trace("Entering pageSetup");

        MutableAttributeMap<Object> requestScope = context.getRequestScope();

        String myStateId = getStateId(context);

        // If the state-id contains "display" then this is a state that will end up viewing the userlist.jsp
        // the config.properties are setup as "xxx-flow-display" for the userlist.jsp so we need to look for that.
        // examples of these states are displayNameResults, displaySchoolResults, etc

        if (myStateId.indexOf("display") != -1)
        {
            // myStateId = myStateId.substring(myStateId.indexOf("display"), 7);
            myStateId = "display";
        }

        // Add role type to retrieve the bread crumbs by role type
        MivRole role = context.getFlowScope().get("role", MivRole.class);
        requestScope.put("user_role", role);
        // If a specific role was passed, alter the flow ID to get the correct set of strings
        if (role != null)
        {
            myStateId += "-" + role;
        }

        loadProperties(myStateId, context);

        String resultsMsg = "";
        ReportSearchCriteria criteria = (ReportSearchCriteria) getFormObject(context);
        // Setup general message based on search method
        // if (criteria.getMethod() != null && criteria.getMethod().equals(SearchMethod.SEARCHDEPT))
        // Don't have to test for null first if you use == instead of .equals() ... Comparing with an 'enum' should
        // always use ==
        if (criteria.getMethod() == SearchMethod.SEARCHDEPT)
        {
            resultsMsg = "Department for";
        }
        else if (criteria.getMethod() == SearchMethod.SEARCHSCHOOL)
        {
            resultsMsg = "School/College for";
        }
        else if (criteria.getMethod() == SearchMethod.SEARCHSENATE)
        {
            resultsMsg = "Senate Office for";
        }
        else if (criteria.getMethod() == SearchMethod.SEARCHACADEMIC)
        {
            resultsMsg = "Academic Affairs for";
        }
        // Now add specifics for the criteria entered
        if (criteria.getSearchRequests().length == 0)
        {
            requestScope.put("searchCriteriaText", resultsMsg + " All Open Actions");
        }
        else
        // if (criteria.getSearchRequests().length>0)
        {
            /**
             * note: this is designed for multiple criteria selection BUT currently UI restricts to a single criteria
             * via javascript
             */
            int i = 0;
            for (CheckboxCriteria request : criteria.getSearchRequests())
            {
                if (i > 0 && i < criteria.getSearchRequests().length)
                {
                    resultsMsg = resultsMsg + " or";
                }
                switch (request)
                {
                    case DC:
                        resultsMsg += " Awaiting a Signed Disclosure Certificate";
                        break;
                    case Dean:
                        resultsMsg += " Released to the Dean without a Dean's signature";
                        break;
                    case VP:
                        resultsMsg += " Released without a final decision";
                        break;
                    case IJA:
                        resultsMsg += " Incomplete Joint Actions";
                        break;
                    case OR:
                    case ORF:
                    case ORS:
                    case ORSF:
                        resultsMsg += " Open Reviews";
                        break;
                    case OPR:
                        resultsMsg += " Open Packet Requests";
                        break;
                    case Audit:
                        resultsMsg += " Completed Open Actions at the School/College";
                        break;
                    case Ready:
                    case ReadyFed:
                    case ReadySenate:
                    case ReadySenateFed:
                        resultsMsg += " Completed Open Actions";
                        break;
                    case AllFed:
                    case AllSenate:
                    case AllSenateFed:
                        resultsMsg += " All Open Actions";
                        break;
                    default:
                        resultsMsg = "Still working on this page, no results to display yet!";
                        break;
                }
                i++;
            }
            requestScope.put("searchCriteriaText", resultsMsg);
        }

        return success();
    }


    /**
     * Will get ALL dossiers at a given location that the user is allowed to see and then trim the list by
     * the requested criteria - either 'completed' (at the location) dossier or 'incomplete' joint appointment
     *
     * @param criteria can handle criteria of 'ready' (several version of this) or 'IJA' (incomplete joint appointment)
     * @param user MIVUser needed to determine which dossiers can be shown
     * @param location DossierLocation used to gather only dossiers at this location
     * @param request
     * @return dossier action list
     */
    public List<MivActionList> getDossiers(ReportSearchCriteria criteria,
                                           MIVUser user,
                                           DossierLocation location,
                                           CheckboxCriteria request)
    {
        List<MivActionList> additional = searchStrategy.getDossiersAtLocation(user, location);

        if (request == null) { return additional; }

        List<MivActionList> results = new ArrayList<MivActionList>();

        switch (request)
        {
            case Audit:
            case Ready:
            case ReadyFed:
            case ReadySenate:
            case ReadySenateFed:
                for (MivActionList action : additional)
                {
                    if (action.getLocationStatus().equalsIgnoreCase("completed"))
                    {
                        results.add(action);
                    }
                }
                break;
            case IJA:
                for (MivActionList action : additional)
                {
                    // if a joint exists and is not complete
                    if (!action.getJointStatus().equalsIgnoreCase("N/A")
                     && !action.getJointStatus().equalsIgnoreCase("completed"))
                    {
                        results.add(action);
                    }
                }
                break;
            case AllFed:
            case AllSenate:
            case AllSenateFed:
                results.addAll(additional);
                break;
            default:
                results.addAll(additional);
                break;
        }

        return results;
    }

    /**
     * prepareResult will translate the list of actions into report columns for the userreport.jsp
     *
     * @param actions
     *            - List<MivActionList> that are ready to be displayed on the report
     * @param location
     *            - DossierLocation of the user that made the request, used to show the correct report columns
     * @return
     */
    private ReportVO prepareResult(List<MivActionList> actions, DossierLocation location, ReportSearchCriteria criteria)
    {
        boolean showReviewers = false;
        boolean vpDecision = false;
        if (criteria.getSearchRequests().length > 0) {
            for (CheckboxCriteria box: criteria.getSearchRequests()) {
                switch (box)
                {
                    case OR:
                    case ORF:
                    case ORS:
                    case ORSF:
                        showReviewers = true;
                        break;
                    case VP:
                    	vpDecision = true;
                    	break;
                    // If we are reporting on packet requests, set the location to PACKETREQUEST to set
                    // different columns than would normally be shown
                    case OPR:
                        location = DossierLocation.PACKETREQUEST;
                        break;
                    default:

                }
            }
        }
        ReportVO reportVO = new ReportVO();

        List<String> keyList = new ArrayList<String>();
        // this is the first column on each open action report
        keyList.add("action.report.user");
        if (location == null) location = DossierLocation.UNKNOWN;
        switch (location)
        {
            case PACKETREQUEST:
                keyList.add("action.report.appointment");
                keyList.add("action.report.department");
                keyList.add("action.report.delegation");
                keyList.add("action.report.action");
                keyList.add("action.report.joint");
                break;
            case DEPARTMENT:
                keyList.add("action.report.appointment");
                keyList.add("action.report.department");
                keyList.add("action.report.delegation");
                keyList.add("action.report.action");
                keyList.add("action.report.review");
                if (showReviewers) { keyList.add("action.report.reviewers"); }
                keyList.add("action.report.disclosure");
                keyList.add("action.report.joint");
                break;
            case SCHOOL:
            case POSTSENATESCHOOL:
                keyList.add("action.report.appointment");
                keyList.add("action.report.school");
                keyList.add("action.report.department");
                keyList.add("action.report.delegation");
                keyList.add("action.report.action");
                keyList.add("action.report.review");
                if (showReviewers) { keyList.add("action.report.reviewers"); }
                keyList.add("action.report.releasedtodean");
                keyList.add("action.report.deansdecision");
                keyList.add("action.report.decisiontype");
                keyList.add("action.report.decisiondate");
                keyList.add("action.report.joint");
                break;
            case SENATE_OFFICE:
            case FEDERATION:
            case SENATEFEDERATION:
                keyList.add("action.report.appointment");
                keyList.add("action.report.location");
                keyList.add("action.report.school");
                keyList.add("action.report.department");
                keyList.add("action.report.delegation");
                keyList.add("action.report.action");
                keyList.add("action.report.review");
                if (showReviewers) { keyList.add("action.report.reviewers"); }
                keyList.add("action.report.daterouted");
                break;
            case VICEPROVOST:
            case POSTSENATEVICEPROVOST:
                keyList.add("action.report.school");
                keyList.add("action.report.department");
                keyList.add("action.report.delegation");
                keyList.add("action.report.action");
                keyList.add("action.report.review");
                if (showReviewers) { keyList.add("action.report.reviewers"); }
                keyList.add("action.report.releasedtovp");
                keyList.add("action.report.vpsdecision");
                keyList.add("action.report.decisiontype");
                if (!vpDecision) { keyList.add("action.report.decisiondate"); }
                break;
        }
        // this is the last column on each action report
        keyList.add("action.report.status");

        reportVO.setKeyList(keyList);

        List<String> headerList = ReportUtil.prepareHeaderListByKeyList(keyList);
        reportVO.setHeaderList(headerList);

        if (actions != null && actions.size() > 0)
        {
            List<Map<String, ResultCellVO>> resultList = new ArrayList<Map<String, ResultCellVO>>();
            Map<String, ResultCellVO> resultMap = null;

            for (MivActionList action : actions)
            {
                resultMap = new HashMap<String, ResultCellVO>();
                String testURL = "OpenActions?userId=" + action.getUserId() + "&dossierId=" + action.getDossierId()
                        + "&_eventId=select";
                String URLTitle = "Manage Action for " + action.getSortName();

                UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.user",
                                                      new ResultCellVO(StringUtil.insertValue(action.getSortName()),
                                                              HtmlUtils.htmlEscape(testURL), URLTitle));
             // for review groups
                String reviewURL = "";
                String reviewURLTitle = "";
                if (showReviewers) {
                    reviewURL = "AssignReviewers?userId=" + action.getUserId() + "&dossierId=" + action.getDossierId()
                        + "&schoolId=" + action.getSchoolId() + "&departmentId=" + action.getDepartmentId()
                        + "&_eventId=assignReviewers";
                    //reviewURLTitle = StringUtil.convertListToString(action.getReviewGroups(), ", ");
                    reviewURLTitle = StringUtils.join(action.getReviewGroups(), ", ");
                }

                switch (location)
                {
                    case PACKETREQUEST:
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.appointment", new ResultCellVO(
                                StringUtil.insertValue(action.getAppointmentType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.department", new ResultCellVO(
                                StringUtil.insertValue(action.getDepartmentName())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.delegation", new ResultCellVO(
                                StringUtil.insertValue(action.getDelegationAuthority())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.action", new ResultCellVO(
                                StringUtil.insertValue(action.getActionType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.joint", new ResultCellVO(
                                StringUtil.insertValue(action.getJointStatus())));
                        break;
                    case DEPARTMENT:
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.appointment", new ResultCellVO(
                                StringUtil.insertValue(action.getAppointmentType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.department", new ResultCellVO(
                                StringUtil.insertValue(action.getDepartmentName())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.delegation", new ResultCellVO(
                                StringUtil.insertValue(action.getDelegationAuthority())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.action", new ResultCellVO(
                                StringUtil.insertValue(action.getActionType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.review", new ResultCellVO(
                                StringUtil.insertValue(action.getReviewStatus())));
                        if (showReviewers) {
                            UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.reviewers",
                                                                  new ResultCellVO(StringUtils.join(action.getReviewGroups(), ','),
                                                                          HtmlUtils.htmlEscape(reviewURL), reviewURLTitle));
                        }
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.disclosure", new ResultCellVO(
                                StringUtil.insertValue(action.getDisclosureStatus())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.joint", new ResultCellVO(
                                StringUtil.insertValue(action.getJointStatus())));
                        break;
                    case SCHOOL:
                    case POSTSENATESCHOOL:
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.appointment", new ResultCellVO(
                                StringUtil.insertValue(action.getAppointmentType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.school", new ResultCellVO(
                                StringUtil.insertValue(action.getSchoolName())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.department", new ResultCellVO(
                                StringUtil.insertValue(action.getDepartmentName())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.delegation", new ResultCellVO(
                                StringUtil.insertValue(action.getDelegationAuthority())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.action", new ResultCellVO(
                                StringUtil.insertValue(action.getActionType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.review", new ResultCellVO(
                                StringUtil.insertValue(action.getReviewStatus())));
                        if (showReviewers) {
                            UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.reviewers",
                                                                  new ResultCellVO(StringUtils.join(action.getReviewGroups(), ','),
                                                                          HtmlUtils.htmlEscape(reviewURL), reviewURLTitle));
                        }
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.releasedtodean",
                                                              new ResultCellVO(StringUtil.insertValue(action
                                                                      .getReleasedStatus())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.deansdecision",
                                                              new ResultCellVO(StringUtil.insertValue(action
                                                                      .getDecisionStatus())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.decisiontype",
                                                              new ResultCellVO(StringUtil.insertValue(action
                                                                      .getDecisionType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.decisiondate",
                                                              new ResultCellVO(StringUtil.insertValue(action
                                                                      .getDecisionDate())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.joint", new ResultCellVO(
                                StringUtil.insertValue(action.getJointStatus())));
                        break;
                    case SENATE_OFFICE:
                    case FEDERATION:
                    case SENATEFEDERATION:
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.appointment", new ResultCellVO(
                                StringUtil.insertValue(action.getAppointmentType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.location", new ResultCellVO(
                                StringUtil.insertValue(action.getLocationDescription())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.school", new ResultCellVO(
                                StringUtil.insertValue(action.getSchoolName())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.department", new ResultCellVO(
                                StringUtil.insertValue(action.getDepartmentName())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.delegation", new ResultCellVO(
                                StringUtil.insertValue(action.getDelegationAuthority())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.action", new ResultCellVO(
                                StringUtil.insertValue(action.getActionType())));
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.review", new ResultCellVO(
                                StringUtil.insertValue(action.getReviewStatus())));
                        if (showReviewers) {
                            UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.reviewers",
                                                                  new ResultCellVO(StringUtils.join(action.getReviewGroups(), ','),
                                                                          HtmlUtils.htmlEscape(reviewURL), reviewURLTitle));
                        }
                        UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.daterouted", new ResultCellVO(
                                StringUtil.insertValue(action.getLastRoutedDate())));
                        break;
                    case VICEPROVOST:
                    case POSTSENATEVICEPROVOST:
                    	UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.school", new ResultCellVO(
                    			StringUtil.insertValue(action.getSchoolName())));
                    	UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.department", new ResultCellVO(
                    			StringUtil.insertValue(action.getDepartmentName())));
                    	UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.delegation", new ResultCellVO(
                    			StringUtil.insertValue(action.getDelegationAuthority())));
                    	UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.action", new ResultCellVO(
                    			StringUtil.insertValue(action.getActionType())));
                    	UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.review", new ResultCellVO(
                    			StringUtil.insertValue(action.getReviewStatus())));
                    	if (showReviewers) {
                    		UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.reviewers",
                    				new ResultCellVO(StringUtils.join(action.getReviewGroups(), ','),
                    						HtmlUtils.htmlEscape(reviewURL), reviewURLTitle));
                    	}
                    	UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.releasedtovp",
                    			new ResultCellVO(StringUtil.insertValue(action
                    					.getReleasedTo())));
                    	UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.vpsdecision", new ResultCellVO(
                    			StringUtil.insertValue(action.getRecommendationOrDecision())));
                    	if (!vpDecision) {
                    		UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.decisiontype",
                    				new ResultCellVO(StringUtil.insertValue(action
                    						.getDecisionType())));
                    		UserReportSearchStrategy.safeAddToMap(resultMap, "action.report.decisiondate",
                    				new ResultCellVO(StringUtil.insertValue(action
                    						.getDecisionDate())));
                    	}
                    	break;
                }

                UserReportSearchStrategy.safeAddToMap(resultMap,
                                                      "action.report.status",
                                                      new ResultCellVO(StringUtil.insertValue(action
                                                              .getLocationStatus())));

                // safeAddToMap(resultMap, "dossierId", action.getDossierId());

                resultList.add(resultMap);
            }

            reportVO.setResultList(resultList);
            if(!resultList.isEmpty())
            {
                reportVO.setIndividualColumnFilters("all");
            }

        }

        return reportVO;
    }


    /**
     * Add bread crumbs for open action report as parent flow
     *
     *
     * @param context Webflow request context
     */
    public void addBreadcrumbs(RequestContext context)
    {
        addBreadcrumbs(context, breadcrumbs);
    }


    /**
     * Remove bread crumbs for open action report as parent flow.
     *
     * @param context Webflow request context
     */
    public void removeBreadcrumbs(RequestContext context)
    {
        removeBreadcrumbs(context, breadcrumbs.length);
    }
}
