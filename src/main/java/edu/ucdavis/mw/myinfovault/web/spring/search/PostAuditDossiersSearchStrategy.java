package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.Collections;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

public class PostAuditDossiersSearchStrategy extends DefaultSearchStrategy
{
    /**
     * Get the Dossiers that are at the given location for the given user.
     *
     * @param user who will be accessing the dossiers
     * @param location dossier location
     * @return List of MivActionList items
     */
    @Override
    public List<MivActionList> getDossiersAtLocation(MIVUser user, DossierLocation location)
    {
        // Only the Vice Provost Staff role will have any items returned
        return user.getTargetUserInfo().getPerson().hasRole(MivRole.VICE_PROVOST_STAFF)
             ? super.getDossiersAtLocation(user, location)//vp staff
             : Collections.<MivActionList>emptyList();//not vp staff, return empty list
    }

    // Tell the superclass that this strategy needs URLs
    {
        this.needsUrl = true;
    }
}
