/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReactivateUsersSearchFormAction.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.service.audit.AuditService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.ConvertPersonXml;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.MivEntity;
import edu.ucdavis.myinfovault.audit.AuditAction;
import edu.ucdavis.myinfovault.audit.AuditObject;
import edu.ucdavis.myinfovault.message.AuditMessages;


/**
 * Handle the search & search result rules that are specific to de/re-activating users.
 *
 * @author Stephen Paulsen
 * @since MIV 4.0
 */
public class ReactivateUsersSearchFormAction extends SearchFormAction
{
    private static final String[] breadcrumbs = {"Deactivate/Reactivate a User: Search", "Search Results"};

    protected AuditService auditService = null;

    /**
     * Spring injected AuditService
     * @param auditService
     */
    public void setAuditService(AuditService auditService)
    {
        logger.debug("\n  SET AuditService in ReactivateUsersSearchFormAction : service has been set");
        logger.debug("    ACTUAL CLASS is: " + this.className + "\n");
        this.auditService = auditService;
    }

    public ReactivateUsersSearchFormAction(SearchStrategy searchStrategy,
                                           AuthorizationService authorizationService,
                                           UserService userService)
    {
        super(searchStrategy, authorizationService, userService);
    }

    /**
     * Checks if user has permission to access the manage users sub-flow.
     *
     * @param context Request context from webflow
     * @return boolean status, success or error
     */
    public Event hasPermission(RequestContext context)
    {
        return authorizationService.hasPermission(getShadowPerson(context),
                                                  Permission.ACTIVATE_USER,
                                                  null)
             ? allowed()
             : denied();
    }

    public List<MivPerson> getUsers(MIVUser user, PersonSearchCriteria criteria)
    {
        criteria.setActiveOnly(false);

        List<MivPerson> found = super.getUsers(user, criteria);

        criteria.setFilteredList(found);

        return found;
    }

/*
    @Override
    @Deprecated
    @SuppressWarnings("unchecked") // for getting List<MivPerson>
    public Event getUsersByDepartment(RequestContext context)
    {
this.logger.warn(" +=+=+=+=+=+=+=+=+ THE INTERMEDIATE getUsersByDepartment IS CALLED in the new ReactivateUsersSearchFormAction !! +=+=+=+=+=+=+=+=+");
        PersonSearchCriteria criteria = (PersonSearchCriteria) getFormObject(context);
        criteria.setActiveOnly(false);

        Event result = super.getUsersByDepartment(context, null);

        criteria.setFilteredList((List<MivPerson>) context.getRequestScope().get("results"));

        return result;
    }
*/

    public Event updateUserActivation(RequestContext context)
    {
        context.getFlowScope().put("flowState", getStateId(context));

        // Get user ids from request, which correspond to the checked - active users
        Set<?> keys = context.getRequestParameters().asMap().keySet();

        // We're going to store all the users and the changes in the form object
        PersonSearchCriteria criteria = (PersonSearchCriteria) getFormObject(context);

        // Lists to hold activated and deactivated users
        List<MivPerson> people = criteria.getFilteredList();
        List<MivPerson> activated = new ArrayList<MivPerson>();
        List<MivPerson> deactivated = new ArrayList<MivPerson>();

        String originalPersonXml = null;
        List<String> activatedPersonXml = new ArrayList<String>();
        List<String> deactivatedPersonXml = new ArrayList<String>();


        // Process all people in the originally displayed list, gathering
        // newly activated or newly deactivated people

        for (MivPerson person : people)
        {
            String uid = Integer.toString(person.getUserId());

            // The keys contains the person if their checkbox was marked for activated
            if (keys.contains(uid))
            {
                // They are newly activated if they were inactive before
                if (!person.isActive())
                {
                    activated.add(person);
                    // Get the Original Person Details in Xml string format
                    activatedPersonXml.add(ConvertPersonXml.generateXMLString(person));
                }
            }
            else
            {
                // Someone who's box is unchecked but is active is being newly deactivated
                if (person.isActive())
                {
                    deactivated.add(person);
                    // Get the Original Person Details in Xml string format
                    deactivatedPersonXml.add(ConvertPersonXml.generateXMLString(person));
                }
            }
        }

        // Activate users
        Map<MivPerson, Map<String, Boolean>> results = userService.activate(activated);
        // Deactive users - always succeeds, don't have to check success/failure results
        userService.deactivate(deactivated);

        /* Code for Audit Trail for User Active or DeActivited */
// getUser() and getUserInfo() don't have to be checked for null
// If they *are* returning null then other more serious things have gone wrong.
//        String loggedInuserPrincipalID = null;
//        if (getUser(context) != null && getUser(context).getUserInfo() != null) {
//            loggedInuserPrincipalID = getUser(context).getUserInfo().getPrincipalID();
//        }

        MivPerson realPerson = this.getRealPerson(context);
        String loginUserDisplayName = realPerson.getDisplayName();

        AuditObject auditObject = null;
        String description = null;

        if (auditService != null)
        {
            int deactivatedIndex = 0;
            for (MivPerson person : deactivated)
            {
                originalPersonXml = deactivatedPersonXml.get(deactivatedIndex++);
                description = AuditMessages.getInstance().getString("audit.user.deactivated", person.getDisplayName(), loginUserDisplayName);
                auditObject = new AuditObject(AuditAction.EDIT, MivEntity.USER, String.valueOf(person.getUserId()),
                                              description, originalPersonXml, String.valueOf(realPerson.getUserId()));
                auditService.createAuditTrail(auditObject);
            }
        }


        // Create list of changed users
        List<MivPerson> activationSucceeded = new ArrayList<MivPerson>();
        List<MivPerson> activationFailed = new ArrayList<MivPerson>();
        int activatedIndex=0;
        for (MivPerson person : activated)
        {
            description = null;
            Map<String, Boolean> result = results.get(person);

            boolean success = result == null ? false : result.get("success");

            if (success)
            {
                originalPersonXml = activatedPersonXml.get(activatedIndex);
                activationSucceeded.add(person);
                description = AuditMessages.getInstance().getString("audit.user.activated", person.getDisplayName(), loginUserDisplayName);
            }
            else
            {
                activationFailed.add(person);
                originalPersonXml = null;
                description = null;
                // description = AuditMessages.getInstance().getString("audit.user.activationFailed",person.getPrincipalId());
            }

            if (auditService != null && description != null)
            {
                auditObject = new AuditObject(AuditAction.EDIT, MivEntity.USER, String.valueOf(person.getUserId()),
                                              description, originalPersonXml, String.valueOf(realPerson.getUserId()));
                auditService.createAuditTrail(auditObject);
            }
            activatedIndex++;
        }

        List<MivPerson> changed = new ArrayList<MivPerson>();
        changed.addAll(activationSucceeded);
        changed.addAll(deactivated);

        // Sort and store the changed and failed lists
        Comparator<MivPerson> comparator = new Comparator<MivPerson>() {
            @Override
            public int compare(MivPerson a, MivPerson b) {
                return a.getSortName().compareToIgnoreCase(b.getSortName());
            }
        };

        Collections.sort(changed, comparator);
        criteria.setChangeList(changed);

        Collections.sort(activationFailed, comparator);
        criteria.setFailureList(activationFailed);

        return success();
    }

    /**
     * Add the bread crumb for the previous page in this flow.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumb(RequestContext context)
    {
        addBreadcrumbs(context, breadcrumbs[0]);
    }

    /**
     * Add bread crumbs for both previous pages
     *
     * @param context Webflow request context
     */
    public void addBreadcrumbs(RequestContext context)
    {
        addBreadcrumbs(context, breadcrumbs);
    }

    /**
     * remove bread crumbs for both previous pages
     *
     * @param context Webflow request context
     */
    public void removeBreadcrumbs(RequestContext context)
    {
        removeBreadcrumbs(context, breadcrumbs.length);
    }
}
