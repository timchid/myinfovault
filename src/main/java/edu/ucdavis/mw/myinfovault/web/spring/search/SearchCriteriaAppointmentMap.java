/*
 * Copyright © 2007-2010 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchCriteria.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.Serializable;
import java.util.HashMap;

import edu.ucdavis.mw.myinfovault.service.person.Appointment;

/**
 * This class is used by classes which must filter by multiple search criteria within the same category.
 *
 * @author rhendric
 * @since MIV 3.3
 */
public class SearchCriteriaAppointmentMap implements Serializable
{
    private static final long serialVersionUID = 201003311130L; 

    private HashMap<String,Boolean>appointmentMap = new HashMap<String,Boolean>();


    public void addAppointment(Appointment appointment)
    {
        appointmentMap.put(appointment.getSchool()+":"+appointment.getDepartment(),appointment.isPrimary());
    }

    public boolean hasAppointment(Appointment appointment)
    {
        return appointmentMap.containsKey(appointment.getSchool()+":"+appointment.getDepartment());
    }

    public boolean hasPrimaryAppointment(Appointment appointment)
    {
        return appointmentMap.containsKey(appointment.getSchool()+":"+appointment.getDepartment()) &&
               appointmentMap.get(appointment.getSchool()+":"+appointment.getDepartment());
    }

    public void clearAppointmentMap()
    {
        appointmentMap.clear();
    }
}
