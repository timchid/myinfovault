package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.Serializable;

/**
 * Value Object to hold the Snapshot Records to display on page
 * @author pradeeph
 * @since MIV 4.7.1
 */
public class Snapshot implements Serializable 
{
	private static final long serialVersionUID = 3752773322625765632L;
	
	String snapshotAppointment=null; 
	String description=null;
	String snapshotFileUrl=null;
	String snapshotLocation=null;
	String snapshotSchool=null;
	String snapshotDepartment=null;
	String snapshotTimestamp=null;
	String routedToNode=null;
	
	Snapshot(){}

	/**
	 * @return the snapshotAppointment
	 */
	public String getSnapshotAppointment() {
		return snapshotAppointment;
	}

	/**
	 * @param snapshotAppointment the snapshotAppointment to set
	 * @return self-reference for chaining
	 */
	public Snapshot setSnapshotAppointment(String snapshotAppointment) {
		this.snapshotAppointment = snapshotAppointment;
		return this;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 * @return self-reference for chaining
	 */
	public Snapshot setDescription(String description) {
		this.description = description;
		return this;
	}

	/**
	 * @return the snapshotFileUrl
	 */
	public String getSnapshotFileUrl() {
		return snapshotFileUrl;
	}

	/**
	 * @param snapshotFileUrl the snapshotFileUrl to set
	 * @return self-reference for chaining
	 */
	public Snapshot setSnapshotFileUrl(String snapshotFileUrl) {
		this.snapshotFileUrl = snapshotFileUrl;
		return this;
	}

	/**
	 * @return the snapshotLocation
	 */
	public String getSnapshotLocation() {
		return snapshotLocation;
	}

	/**
	 * @param snapshotLocation the snapshotLocation to set
	 * @return self-reference for chaining
	 */
	public Snapshot setSnapshotLocation(String snapshotLocation) {
		this.snapshotLocation = snapshotLocation;
		return this;
	}

	/**
	 * @return the snapshotSchool
	 */
	public String getSnapshotSchool() {
		return snapshotSchool;
	}

	/**
	 * @param snapshotSchool the snapshotSchool to set
	 * @return self-reference for chaining
	 */
	public Snapshot setSnapshotSchool(String snapshotSchool) {
		this.snapshotSchool = snapshotSchool;
		return this;
	}

	/**
	 * @return the snapshotDepartment
	 */
	public String getSnapshotDepartment() {
		return snapshotDepartment;
	}

	/**
	 * @param snapshotDepartment the snapshotDepartment to set
	 * @return self-reference for chaining
	 */
	public Snapshot setSnapshotDepartment(String snapshotDepartment) {
		this.snapshotDepartment = snapshotDepartment;
		return this;
	}

	/**
	 * @return the snapshotTimestamp
	 */
	public String getSnapshotTimestamp() {
		return snapshotTimestamp;
	}

	/**
	 * @param snapshotTimestamp the snapshotTimestamp to set
	 * @return self-reference for chaining
	 */
	public Snapshot setSnapshotTimestamp(String snapshotTimestamp) {
		this.snapshotTimestamp = snapshotTimestamp;
		return this;
	}

	/**
	 * @return the routedToNode
	 */
	public String getRoutedToNode() {
		return routedToNode;
	}

	/**
	 * @param routedToNode the routedToNode to set
	 * @return self-reference for chaining
	 */
	public Snapshot setRoutedToNode(String routedToNode) {
		this.routedToNode = routedToNode;
		return this;
	};
	
	
}
