/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filter signatures by the associated dossiers action type.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.2
 */
public class ActionTypeFilter extends SearchFilterAdapter<MivElectronicSignature>
{
    private final DossierActionType[] actionTypes;

    /**
     * Create a action type filter.
     *
     * @param actionTypes action types of which the signature's associated dossier must have one
     */
    public ActionTypeFilter(DossierActionType...actionTypes)
    {
        this.actionTypes = actionTypes;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(MivElectronicSignature request)
    {
        for (DossierActionType actionType : actionTypes)
        {
            if (actionType == request.getDocument().getDossier().getAction().getActionType())
            {
                return true;// action for the given signature matches
            };
        }

        return false;// signature's action doesn't match any
    }
}
