/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import java.io.File;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.decision.DecisionType;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.document.PathConstructor;

/**
 * Form backing for the electronic signature decision form.
 *
 * @author Craig Gilmore
 * @since MIV 4.6.3
 */
public class DecisionForm extends SignatureForm implements Serializable
{
    private static final long serialVersionUID = 201212211123L;

    private final Decision decision;
    private final MivPerson realPerson;
    private final MivPerson signingPerson;

    /**
     * Create the decision form backing.
     *
     * @param signature signature/request associated with the decision
     * @param realPerson real, logged-in person accessing the form
     */
    DecisionForm(MivElectronicSignature signature, MivPerson realPerson)
    {
        super(signature);

        this.decision = signature.getDocument();
        this.realPerson = realPerson;
        this.signingPerson = MivServiceLocator.getUserService().getPersonByMivId(Integer.parseInt(signature.getRequestedSigner()));
    }

    /**
     * @return decision
     */
    public Decision getDecision()
    {
        return decision;
    }

    /**
     * @return real, logged-in person accessing the form
     */
    public MivPerson getRealPerson()
    {
        return realPerson;
    }

    /**
     * @return person signing the decision
     */
    public MivPerson getSigningPerson()
    {
        return signingPerson;
    }

    /**
     * @return URL to the decision PDF
     */
    public String getWetSignatureUrl()
    {
        File wetSignature = decision.getWetSignature();

        // null URL for no wet signature
        return wetSignature != null
             ? new PathConstructor().getUrlFromPath(wetSignature)
             : null;
    }

    /**
     * Get the available decision types for this decision.
     *
     * Decision types are presented as a map, e.g.:
     * <pre>
     * { "APPROVED" : "Recommend Approval",
     *   "DENIED"   : "Recommend Denial",
     *   "OTHER"    : "Recommend Other"     }
     * </pre>
     *
     * @return available decision types for this decision
     */
    public Map<String, String> getDecisionTypes()
    {
        Map<String, String> decisionTypes = new LinkedHashMap<String, String>();

        for (DecisionType decisionType : decision.getDecisionTypes())
        {
            decisionTypes.put(decisionType.name(), decisionType.getDescription(decision.getDocumentType()));
        }

        return decisionTypes;
    }
}
