/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DisclosureAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import java.util.List;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.events2.DisclosureSignatureEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService;
import edu.ucdavis.mw.myinfovault.web.spring.disclosurecertificate.DCAction;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Webflow form action for candidate disclosure electronic signatures.
 * This is used when a Candidate is <em>signing</em> the disclosure certificate.
 * Creating and managing the DC is otherwise handled by {@link DCAction}.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class DisclosureAction extends SignatureAction
{
    private static final String UPDATED_DC = "The disclosure certificate you attempted to sign has been updated. Please review again.";

    private static final String ID_PARAM = "id";

    private SnapshotService snapshotService;

    /**
     * Creates the disclosure action bean and registers the event dispatcher.
     *
     * @param userService Spring-injected user service
     * @param authorizationService Spring-injected authorization service
     * @param signingService Spring-injected signing service
     * @param snapshotService Spring-injected snapshot service
     */
    public DisclosureAction(UserService userService,
                            AuthorizationService authorizationService,
                            SigningService signingService,
                            SnapshotService snapshotService)
    {
        super(userService,
              authorizationService,
              signingService);

        this.snapshotService = snapshotService;

        EventDispatcher.getDispatcher().register(this);
    }

    /**
     * Does the user have general permission to
     * sign disclosures as the shadow person?
     *
     * @param context Webflow request context
     * @return allowed event status if permitted, denied otherwise
     */
    public Event hasPermission(RequestContext context)
    {
        AttributeSet permissionDetails = new AttributeSet();
        permissionDetails.put(PermissionDetail.TARGET_ID,
                              Integer.toString(getShadowPerson(context).getUserId()));

        return authorizationService.hasPermission(getRealPerson(context),
                                                  Permission.SIGN_DISCLOSURE,
                                                  permissionDetails)
             ? allowed()
             : denied();
    }

    /**
     * Is the real, logged-in person allowed to sign the disclosure?
     *
     * @param context Web flow request context
     * @return Web flow event status, allowed or denied
     */
    public Event isAuthorized(RequestContext context)
    {
        AttributeSet permissionDetails = new AttributeSet();
        permissionDetails.put(PermissionDetail.TARGET_ID,
                              Integer.toString(getFormObject(context).getSignature().getRecordId()));

        return authorizationService.isAuthorized(getRealPerson(context),
                                                 Permission.SIGN_DISCLOSURE,
                                                 permissionDetails,
                                                 null)
             ? allowed()
             : denied();
    }

    /**
     * Get the list of candidate disclosure certificate signature requests for the target user.
     *
     * @param targetUser the target, switched-to user
     * @return list MIV signature requests
     */
    public List<MivElectronicSignature> getRequestedSignatures(MivPerson targetUser)
    {
        return signingService.getRequestedSignatures(targetUser, MivRole.CANDIDATE);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    protected Object createFormObject(RequestContext context)
    {
        return new DisclosureForm(signingService.getSignatureById(getParameterValue(context, ID_PARAM, 0)));
    }

    /**
     * Sign the candidate disclosure certificate.
     *
     * @param context Webflow request context
     * @return Web flow event status, success or error
     */
    public Event sign(RequestContext context)
    {
        MivElectronicSignature signature = getFormObject(context).getSignature();

        // MIV-4566: Create a snapshot of the dossier if DC is signed
        if (sign(getRealPerson(context), signature))
        {
            EventDispatcher2.getDispatcher().post(new DisclosureSignatureEvent(getRealPerson(context), signature)
            .setShadowPerson(getShadowPerson(context)));

            try
            {
                snapshotService.createDossierSnapshot(signature.getDocument().getDossier(),
                                                      MivRole.CANDIDATE);
            }
            catch (WorkflowException exception)
            {
                throw new MivSevereApplicationError("errors.dc.sign.snapshot",
                                                    exception,
                                                    signature.getDocument());
            }

            return success();
        }

        /*
         * Signing was not successful, the DC must have been updated.
         */
        getFormErrors(context).reject(null, UPDATED_DC);

        return error();
    }
}
