/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;

/**
 * Form backing for an electronic signature action.
 * 
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public abstract class SignatureForm implements Serializable
{
    private static final long serialVersionUID = 200906111110L;

    private MivElectronicSignature signature;
    
    /**
     * Creates the signature form backing.
     * 
     * @param signature signature/request to sign
     */
    protected SignatureForm(MivElectronicSignature signature)
    {
        this.signature = signature;
    }
    
    // no-arg constructor for sub-class serialization
    protected SignatureForm(){}
    
    /**
     * @return signature signature/request to sign
     */
    public MivElectronicSignature getSignature()
    {
        return signature;
    }
    
    /**
     * @return URL to the dossier PDF
     */
    public String getDossierUrl()
    {
        return signature.getDocument()
                        .getDossier()
                        .getDossierPdfUrl(signature.getDocument().getDocumentType().getAllowedSigner(),
                                          signature.getDocument().getSchoolId(),
                                          signature.getDocument().getDepartmentId());
    }
}
