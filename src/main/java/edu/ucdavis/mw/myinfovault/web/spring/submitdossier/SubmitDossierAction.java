/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SubmitDossierAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.submitdossier;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest;
import edu.ucdavis.mw.myinfovault.events2.AcademicActionEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierRouteEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.events2.PacketSubmissionEvent;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.academicaction.AcademicActionService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.biosketch.DocumentCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.raf.RafService;
import edu.ucdavis.mw.myinfovault.util.FileUtil;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivPropertyEditors;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.MivMailMessage;
import edu.ucdavis.myinfovault.document.Document;
import edu.ucdavis.myinfovault.document.DocumentFile;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PathConstructor;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * This class is the form controller for the SubmitDossier webflow.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
@Deprecated
public class SubmitDossierAction extends MivFormAction
{
    private DocumentCreatorService documentCreatorService;
    private DossierCreatorService dossierCreatorService;
    private DossierService dossierService;
    private RafService rafService;
    private AcademicActionService aaService;

    private static final ThreadLocal<DateFormat> dossierDateFormat =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("MM/dd/yy, h:mm a");
            }
        };

    /**
     * Create the submit dossier form action bean.
     *
     * @param userService Spring-injected user service
     * @param authorizationService Spring-injected authorization service
     * @param documentCreatorService Spring-injected document creator service
     * @param dossierCreatorService Spring-injected dossier creator service
     * @param dossierService Spring-injected dossier service
     * @param rafService Spring-injected recommended action form service
     * @param aaService Spring-injected academic action form service
     */
    public SubmitDossierAction(UserService userService,
                               AuthorizationService authorizationService,
                               DocumentCreatorService documentCreatorService,
                               DossierCreatorService dossierCreatorService,
                               DossierService dossierService,
                               RafService rafService,
                               AcademicActionService aaService)
    {
        super(userService, authorizationService);

        this.documentCreatorService = documentCreatorService;
        this.dossierCreatorService = dossierCreatorService;
        this.dossierService = dossierService;
        this.rafService = rafService;
        this.aaService = aaService;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    protected Object createFormObject(RequestContext context) throws WorkflowException, IllegalStateException
    {
        Iterator<Dossier> dossiers = dossierService.getDossierByUser(getShadowPerson(context)).iterator();

        return dossiers.hasNext()
             ? new SubmitDossierForm(dossiers.next())
             : new SubmitDossierForm();
    }

    /**
     * Validate the packet for the current user. Make sure there is not already
     * a dossier in process and that there is data
     *
     * @param context
     * @return Event
     */
    public Event validatePacket(RequestContext context)
    {
        final MutableAttributeMap<Object> flowScope = context.getFlowScope();
        final MIVUserInfo mivTargetUserInfo = getUser(context).getTargetUserInfo();

        // Get the data submitted from the form backing object
        SubmitDossierForm submitDossierForm = getFormObject(context);

        // See if dossier is already in process
        if (this.isDossierInProcess(submitDossierForm, getShadowPerson(context), flowScope))
        {
            return error();
        }

        final Map<String, String> combinedPdfPacketFileMap = this.getCombinedPacketFile(mivTargetUserInfo);

        // If no combined PDF file, the dossier has not yet been created. If there has been no dossier
        // created, show error for all but Appointment action type
        if (combinedPdfPacketFileMap.isEmpty() &&
            (submitDossierForm.getDossier().getAction().getActionType() != DossierActionType.APPOINTMENT))
        {
            // Set the dossier already in process for this user
            flowScope.put("dossierId", "");
            flowScope.put("confirmationError", "dossierNotCreatedError");
            return error();
        }
        // If there are any dossier create errors present, don't send to department
        else if (combinedPdfPacketFileMap.containsKey("createError"))
        {
            flowScope.put("dossierId", "");
            flowScope.put("confirmationError", "dossierCreateErrors");
            return error();
        }
        // OK to send
        else
        {
            flowScope.put("combinedPdfPacketFileMap", combinedPdfPacketFileMap);
            return success();
        }
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#bind(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Event bind(RequestContext context) throws Exception
    {
        Event event = super.bind(context);

        SubmitDossierForm submitDossierForm = getFormObject(context);

        // if the set delegation authority is not valid for the selected action type
        if (!submitDossierForm.getActionType().isValid(submitDossierForm.getDelegationAuthority()))
        {
            // set the delegation authority to the action type default
            submitDossierForm.setDelegationAuthority(submitDossierForm.getActionType().getDefaultDelegationAuthority());
        }

        return event;
    }

    /**
     * Initiate the workflow routing of the Dossier
     *
     * @param context
     * @return Event
     */
    public Event initiateDossier(RequestContext context)
    {
        MutableAttributeMap<Object> flowScope = context.getFlowScope();

        MivPerson realPerson = this.getRealPerson(context);
        MivPerson shadowPerson = this.getShadowPerson(context);

        // Get the data submitted from the form backing object
        SubmitDossierForm submitDossierForm = getFormObject(context);

        // See if dossier is already in process
        if (isDossierInProcess(submitDossierForm, shadowPerson, flowScope)) return error();

        // Should be a Candidate or MivAdmin to submit a dossier, if not send email and log to alert us
        this.checkSubmitterRole(getUser(context));

        // Initiate/approve the dossier in workflow
        try
        {
            AcademicAction action = new AcademicAction(shadowPerson,
                                                       submitDossierForm.getActionType(),
                                                       submitDossierForm.getDelegationAuthority(),
                                                       submitDossierForm.getEffectiveDateAsDate(),
                                                       submitDossierForm.getAccelerationYears());

            Dossier dossier = null;

            // If the dossier is not already in process, initiate into workflow
            final Long dossierInProcessId = (Long) flowScope.get("dossierId");

            if (dossierInProcessId == null)
            {
                logger.info("_AUDIT : User {}{} is submitting a new dossier to the Department",
                            realPerson.getUserId(),
                            (shadowPerson.getUserId() != realPerson.getUserId() ?
                                    ", acting as " + shadowPerson.getUserId() + ", " : "")
                            );
                // This dossier is not currently in workflow so we must initiate
                dossier = dossierService.initiateDossierRouting(action, shadowPerson);
            }
            // The dossier is in process at the first node, approve to route to
            // the next node in workflow
            else
            {
                // Initiate the dossier, not deleting uploads
                dossier = dossierService.initiateDossierRouting(dossierInProcessId,
                                                                action,
                                                                false,        /* do not delete uploads here */
                                                                shadowPerson);

                logger.info("_AUDIT : User {}{} is re-submitting a dossier #{} to the Department",
                                new Object[] {
                                    realPerson.getUserId(),
                                    (shadowPerson.getUserId() != realPerson.getUserId() ?
                                            ", acting as " + shadowPerson.getUserId() + ", " : ""),
                                    dossier.getDossierId()
                                }
                            );

                // Get rid of any packet files which may be present for a previously in process dossier
                this.deletePacketFiles(dossier);

                // If the RAF or AA form exists, create the PDF on the way back to the department
                if (dossier.isRecommendedActionFormPresent())
                {
                    rafService.createPdfs(rafService.getBo(dossier));
                }
                else if (dossier.isAcademicActionFormPresent())
                {
                    aaService.createPdfs(aaService.getBo(dossier));
                }
            }

            // post academic action event
            EventDispatcher2.getDispatcher().post(new AcademicActionEvent(getRealPerson(context), dossier));

            // Copy the dossier files
            if (!this.copyPacketFiles(dossier, getUser(context).getTargetUserInfo()))
            {
                logger.error("Unable to complete packet file copy for dossier " + dossier.getDossierId());
                throw new MivSevereApplicationError(
                        "errors.SubmitDossier.unableToCopyPacketFilesError",
                        Long.toString(dossier.getDossierId()));
            }

            // Get the existing combined pdf file
            File dossierDirectory = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);
            File dossierFile = new File(dossierDirectory, "dossiercombined.pdf");
            HashMap<String, String> combinedPdfFileMap = new HashMap<String, String>();

            // Put the combined file in the flowscope
            combinedPdfFileMap.put("combinedPdfFileUrl", new PathConstructor().getUrlFromPath(dossierFile));
            flowScope.put("combinedPdfPacketFileMap", combinedPdfFileMap);

            String submitDate = "Submitted " + dossierDateFormat.get().format(dossier.getSubmittedDate().getTime());

            flowScope.put("dossierId", dossier.getDossierId());
            flowScope.put("dossierDescription", dossier.getAction().getDescription());
            flowScope.put("candidate", shadowPerson.getDisplayName());
            flowScope.put("departments", dossier.getDepartmentList());
            flowScope.put("submitDate", submitDate);


            // TODO : Temp update the packet request for this dossier with a dummy packet id (just using the request id for now) to indicate the request has been satisfied.
            PacketService packetService =  MivServiceLocator.getPacketService();
            PacketRequest packetRequest = packetService.getPacketRequest(dossier);
            if (packetRequest != null)
            {
                packetRequest.setPacketId(packetRequest.getRequestId());
                packetService.updatePacketRequest(packetRequest, realPerson.getUserId());
                EventDispatcher2.getDispatcher().post(new PacketSubmissionEvent(realPerson, dossier)
                     .setComments("Satisfied "+ (dossierInProcessId == null ? "INITIAL packet request" : "packet RE-REQUEST")+" for dossier "+dossier.getDossierId()+" - "+dossier.getAction().getDelegationAuthority()+": "+dossier.getAction().getActionType()));
            }

            EventDispatcher2.getDispatcher().post( new DossierRouteEvent(
                    realPerson,
                    dossier,
                    dossierService.getInitialWorkflowNode(dossier).getNodeLocation(),
                    dossierService.getCurrentWorkflowNode(dossier).getNodeLocation())
                        .setShadowPerson(shadowPerson)
                        .setComments(dossierInProcessId == null ? "Initial submission" : "Re-submission"));

        }
        catch (WorkflowException wfe)
        {
            logger.error("Unable to route dossier for " + getShadowPerson(context).getPersonId(), wfe);
            throw new MivSevereApplicationError(
                    "errors.SubmitDossier.unableToRouteDossierError",
                    wfe, getShadowPerson(context).getPersonId());
        }
        return success();
    }


    /**
     * Get the combined packet file for the current user
     *
     * @param MIVUserInfo
     *            - MIVUserInfo object
     * @return Map - map of attributes for the combined pdf file.
     */
    private Map<String, String> getCombinedPacketFile(MIVUserInfo mui)
    {
        // TODO: fix servletContext and sequence
        // Get existing documents
        List<Document> documents = documentCreatorService.viewPacket(mui, 1);

        // Find the combined PDF file in the documents returned
        HashMap<String, String> combinedPdfFileMap = new HashMap<String, String>();
        String combinedPdfFileUrl = null;
        String lastModified = null;

        for (Document document : documents)
        {
            for (DocumentFile documentFile : document.getOutputDocuments())
            {
                // Check for any create errors
                if (!documentFile.getCreateSuccess())
                {
                    // See if there was an error on the combined PDF
                    if (documentFile.getDocumentFormat() == DocumentFormat.COMBINED_PDF)
                    {
                        documentCreatorService.createPacket(mui, 1);
                        // Check to see if it was created ok
                        if (!documentFile.getOutputFile().exists())
                        {
                            combinedPdfFileMap.put("createError", "");
                        }
                    }
                    else
                    {
                        // Just add the createError key for now to indicate there was a create error
                        combinedPdfFileMap.put("createError", "");
                    }
                }
                // If there is only one document, there will be no combined file
                // so use the single pdf file if present.
//                if (documentFile.getDocumentFormat().compareTo(DocumentFormat.COMBINED_PDF) == 0 ||
//                    (documents.size() == 1 && documentFile.getDocumentFormat().compareTo(DocumentFormat.PDF) == 0))
                if (documentFile.getDocumentFormat() == DocumentFormat.COMBINED_PDF ||
                   (documents.size() == 1 && documentFile.getDocumentFormat() == DocumentFormat.PDF))
                {
                    // Get the output URL and the last modified time of the file itself
                    combinedPdfFileUrl = documentFile.getOutputUrl();
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(documentFile.getOutputFile().lastModified());
                    lastModified = "Last created: " + dossierDateFormat.get().format(calendar.getTime());

                    combinedPdfFileMap.put("combinedPdfFileUrl", combinedPdfFileUrl);
                    combinedPdfFileMap.put("lastModified", lastModified);
                    break;
                }
            }
            // Only one will be present in the document list. Get out when we've got it.
            if (combinedPdfFileUrl != null)
            {
                break;
            }
        }

        return combinedPdfFileMap;
    }


    /**
     * Copy packet files to the final location for the dossier.
     *
     * @param dossier dossier object
     * @param mui
     * @return if successful
     */
    private boolean copyPacketFiles(Dossier dossier, MIVUserInfo mui)
    {
        // Get the packet files for this user
        List<Document> documents = documentCreatorService.viewPacket(mui, 1);

        Map<File, File> fileMap = new HashMap<File, File>();

        // Loop through the documents and build a map of files to copy
        for (Document document : documents)
        {
            // for each set of document files produced in each format for this document
            for (DocumentFile packetFile : document.getOutputDocuments())
            {
                // Build archive input name from the packet output file
                File inputFile = packetFile.getOutputFile();

                // Build archive output name from the packet output file extension.
                // Files will be stored in the dossier directory by extension
                File outputFile = new File(dossier.getDossierDirectoryByDocumentFormat(packetFile.getDocumentFormat()),
                                           document.getDossierFileName()
                                         + "."
                                         + packetFile.getDocumentFormat().getFileExtension());


                fileMap.put(inputFile, outputFile);
            }
        }

        // Do the copy
        return this.copyFiles(fileMap);
    }


    /**
     * Delete any packet files for the dossier
     *
     * @param Dossier - dossier object
     */
    private void deletePacketFiles(Dossier dossier)
    {
        // Get rid of any packet files which may be present
        logger.info("Deleting packet files for resubmitted dossier " + dossier.getDossierId() + " for user " + dossier.getAction().getCandidate().getUserId());

        File dossierPath = dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF);

        for (DossierDocumentDto dossierDocument : dossierCreatorService.getAuthorizedDocuments(MivRole.CANDIDATE))
        {
            // Delete all candidate files from the dossier directory,
            // they will be re-copied when the dossier is resubmitted.
            if (dossierDocument.isCandidateFile())
            {
                File dossierFile = new File(dossierPath, dossierDocument.getFileName() + "." + DocumentFormat.PDF.getFileExtension());
                if (dossierFile.exists())
                {
                    if (dossierFile.delete())
                    {
                        logger.info("Deleted packet file " + dossierDocument.getFileName() + " from dossier " + dossier.getDossierId());
                    }
                    else
                    {
                        logger.warn("Failed to delete packet file " + dossierDocument.getFileName() + " from dossier " + dossier.getDossierId());
                    }
                }
            }
        }
    }


    /**
     * Copy files in the input file map from source to target destination.
     *
     * @param fileMap
     *            map of files to copy where the key is the input file and the
     *            value is the output file.
     * @return boolean true if successful, otherwise false
     */
    private boolean copyFiles(Map<File, File> fileMap)
    {

        boolean success = true;

        // Loop through the map and do the copy to the archive location
        for (File inputFile : fileMap.keySet())
        {
            if (!inputFile.exists())
            {
                logger.warn("Dossier file " + inputFile.getAbsolutePath() + " missing");
                continue;
            }

            File outputFile = fileMap.get(inputFile);
            // Make sure the output path exists and create if necessary
            File outputPath = new File(outputFile.getParent());
            if (!outputPath.exists() && !outputPath.mkdirs())
            {
                logger.error("Copy of Dossier file "
                        + inputFile.getAbsolutePath()
                        + " failed. Unable to create output directory "
                        + outputFile.getAbsolutePath());
                success = false;
            }

            if (!success || !FileUtil.copyFile(inputFile, outputFile))
            {
                logger.error("Copy of Dossier file " + inputFile.getAbsolutePath() + " failed");
                success = false;
            }
        }

        return success;
    }

    /**
     * isDossierInProcess - check for a dossier(s) already in process for the input user
     * @param MivUserInfo for the target user
     * @param flowScope
     * @return boolean - true if dossier in process, otherwise false
     */
    private boolean isDossierInProcess(SubmitDossierForm submitDossierForm, MivPerson candidate, MutableAttributeMap<Object> flowScope)
    {
        boolean inProcess = false;

        Dossier dossier = submitDossierForm.getDossier();
        if (dossier!=null)
        {
            // Set the dossier already in process for this user
            flowScope.put("dossierId", dossier.getDossierId());

            // If we are at any other than the PACKETREQUEST route node, the dossier may not be submitted
            if (!dossier.getLocation().contains(DossierLocation.PACKETREQUEST.mapLocationToWorkflowNodeName()))
            {
                HashMap<String, String> combinedPdfFileMap = new HashMap<String, String>();
                // Get the existing combined pdf file
                File dossierDirectory = new File(dossier.getDossierDirectory(), "pdf");
                File dossierFile = new File(dossierDirectory, "dossiercombined.pdf");
                PathConstructor pc = new PathConstructor();
                combinedPdfFileMap.put("combinedPdfFileUrl", pc.getUrlFromPath(dossierFile));

                flowScope.put("combinedPdfPacketFileMap", combinedPdfFileMap);
                String submitDate = "Submitted " + dossierDateFormat.get().format(dossier.getSubmittedDate().getTime());

                flowScope.put("candidate", candidate.getDisplayName());
                flowScope.put("confirmationError", "inProcessError");
                flowScope.put("dossierDescription", dossier.getAction().getDescription());
                flowScope.put("departments", dossier.getDepartmentList());
                flowScope.put("submitDate", submitDate);
                flowScope.put("location", dossier.getLocationDescription(dossier.getLocation()));
                inProcess = true;
            }
        }
        return inProcess;
    }


    /**
     * checkSubmitterRole - Check the role of the person submitting a dossier. If the person's role is not a
     * APPOINTEE, CANDIDATE or MIV_ADMIN, send an email and log since normally only the previously mentioned roles
     * will submit dossiers.
     * @param mivUser
     */
    private void checkSubmitterRole(MIVUser mivUser)
    {
        // Should be a Candidate or MivAdmin to submit a dossier, if not send email and log to alert us
        MivPerson loggedInPerson = mivUser.getUserInfo().getPerson();
        MivPerson targetPerson = mivUser.getTargetUserInfo().getPerson();
        if (targetPerson != null && (!targetPerson.hasRole(MivRole.CANDIDATE, MivRole.APPOINTEE) && !targetPerson.hasRole(MivRole.SYS_ADMIN)))
        {
            String targetPersonDisplayName = targetPerson.getDisplayName();
            if (!mivUser.isProxying())
            {
                targetPersonDisplayName = "themself";
            }

            // Report that dossier was submitted by a person with a role other than Candidate or Miv_Admin
            final String toAddress = MIVConfig.getConfig().getProperty("submitdossiermail");
            final String loggedInDisplayName = loggedInPerson.getDisplayName() != null ? loggedInPerson.getDisplayName() : "Unknown";
            final String loggedInRole = loggedInPerson.getDisplayName() != null ? loggedInPerson.getPrimaryRoleType().toString() : "Unknown";
            final String targetUserRole = targetPerson.getPrimaryRoleType().toString();

            final String subject =
                (MIVConfig.getConfig().isProductionServer() ? "" : "[TEST] ") +
                "Dossier submitted to department by " + loggedInDisplayName +
                " on behalf of " + targetPersonDisplayName + ".";

            final String message =
                "A dossier was submitted to department on " + MIVConfig.getConfig().getServer() +
                " by " + loggedInDisplayName +
                " with primary role " + loggedInRole +
                " on behalf of " + targetPersonDisplayName +
                " with primary role " + targetUserRole + "." +
                " Normally, only persons with the role of CANDIDATE or MIV_ADMIN will have dossiers submitted to the department.";

            logger.info(message);

            // Send email
            final String fromAddress = "MIV Send Dossier To My Dept <noreply@ucdavis.edu>";

            try
            {
                final MivMailMessage email = new MivMailMessage(toAddress, fromAddress, subject);

                email.send(message);
            }
            catch (MessagingException e)
            {
                // Just log. We don't want to stop because of an error here.
                logger.error("Failed to send email to report dossier sumbmission outside of CANDIDATE and MIV_ADMIN role", e);
            }

        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#getFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public SubmitDossierForm getFormObject(RequestContext context)
    {
        return (SubmitDossierForm) super.getFormObject(context);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(PropertyEditorRegistry registry)
    {
        registry.registerCustomEditor(Date.class, new MivPropertyEditors.DatePropertyEditor());
    }
}
