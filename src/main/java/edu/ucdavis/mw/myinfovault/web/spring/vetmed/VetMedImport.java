/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: VetMedImport.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.vetmed;

import java.util.Set;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.dao.vetmed.CourseEvaluationDao;
import edu.ucdavis.mw.myinfovault.domain.vetmed.CourseEvaluation;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.SelectedFilter;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;

/**
 * Handles import of VetMed CSV files and persisting to the MyInfoVault database.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class VetMedImport extends MivFormAction
{
    private final CourseEvaluationDao courseEvaluationDao;

    /**
     * Create the VetMed import action bean.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     * @param courseEvaluationDao Spring injected course evaluation DAO
     */
    public VetMedImport(UserService userService,
                        AuthorizationService authorizationService,
                        CourseEvaluationDao courseEvaluationDao)
    {
        super(userService, authorizationService);

        this.courseEvaluationDao = courseEvaluationDao;
    }

    /**
     * If the real, logged in person has permission to import VetMed records.
     *
     * @param context Web flow request context
     * @return Web flow event status, allowed or denied
     */
    public Event hasPermission(RequestContext context)
    {
        return authorizationService.hasPermission(getRealPerson(context),
                                                  Permission.IMPORT_VETMED,
                                                  null)
             ? allowed()
             : denied();
    }

    /**
     * Persist processed evaluation records to the database.
     *
     * @param context Web flow request context
     * @return Web flow event status, success or error
     */
    public Event persist(RequestContext context)
    {
        VetMedImportForm form = (VetMedImportForm) getFormObject(context);

        Set<CourseEvaluation> evaluations = (new SelectedFilter<CourseEvaluation>()).apply(form.getProcessed());

        courseEvaluationDao.persistEvaluations(getRealPerson(context).getUserId(), evaluations);

        form.getProcessed().removeAll(evaluations);
        form.getPersisted().addAll(evaluations);

        return success();
    }
}
