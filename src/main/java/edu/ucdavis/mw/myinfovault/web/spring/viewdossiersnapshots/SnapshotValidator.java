/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SnapshotValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.viewdossiersnapshots;

import java.util.Calendar;
import java.util.Date;

import edu.ucdavis.myinfovault.data.DateParser;

/**
 * to validate snapshots.
 * @author pradeeph
 *
 */
public class SnapshotValidator
{
    /**
     * get snapshots produced start date
     * @return
     */
    private static Calendar getSnapshotsProducedStartDate(){
        final Calendar calendar = Calendar.getInstance();
        calendar.set(2010, Calendar.JANUARY, 20);
        return calendar;
    }

     /**
      * See if this dossier was created prior to Jan 19, 2010. There were no snapshots produced for locations where the dossier
      * was routed  prior to that date
      * @param submitDate - Date Object
      * @return boolean
      */
     public static boolean isPriorToSnapshotsProducedStartDate(Date submitDate){
         return (submitDate.before(getSnapshotsProducedStartDate().getTime()));
     }

     /**
      * See if this dossier was created prior to Jan 19, 2010. There were no snapshots produced for locations where the dossier
      * was routed  prior to that date
      * @param submitDate - String Object
      * @return boolean
      */
     public static boolean isPriorToSnapshotsProducedStartDate(String submitDate){
         return (DateParser.parse(submitDate).before(getSnapshotsProducedStartDate().getTime()));
     }
}
