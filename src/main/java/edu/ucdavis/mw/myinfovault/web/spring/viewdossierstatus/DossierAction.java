/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.viewdossierstatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeType;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierFileDto;

/**
 * Contains the fields, getters, and setters for Dossier documents results list.
 * @author Mary Northup
 * @since MIV 3.0
 */
public class DossierAction implements Serializable
{
    private static final long serialVersionUID = 200907241135L;

    private static final String ENABLE_BUTTON = "";
    private static final String DISABLE_BUTTON = " disabled=\"disabled\"";

    private String attributeId = null;
    private String attributeName = null;
    private DossierAttributeStatus attributeStatus = null;
    private String description = null;
    private String displayProperty = null;

    private boolean isRequired = false;
    private boolean isMissing = true;

    private int updateButtonCount = 0;
    private String disableAddButton = "";
    private String disableCloseButton = "";

    private DossierAttributeType actionType = DossierAttributeType.UNKNOWN;
    private String value = null;

    private List<DossierFileDto> documents = new ArrayList<DossierFileDto>();


    public void setAttributeId(String attributeId)
    {
        this.attributeId = attributeId;
    }
    public String getAttributeId()
    {
        return this.attributeId;
    }


    public void setAttributeName(String attributeName)
    {
        this.attributeName = attributeName;
    }
    public String getAttributeName()
    {
        return this.attributeName;
    }

    public void setAttributeStatus(DossierAttributeStatus attributeStatus)
    {
        this.attributeStatus = attributeStatus;
    }
    public DossierAttributeStatus getAttributeStatus()
    {
        return this.attributeStatus;
    }

    public void setDisplayProperty(String displayProperty)
    {
        this.displayProperty = displayProperty;
    }
    public String getDisplayProperty()
    {
        return this.displayProperty;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    public String getDescription()
    {
        return this.description;
    }


    public void setRequired(boolean isRequired)
    {
        this.isRequired = isRequired;
    }
    public boolean getIsRequired()
    {
        return this.isRequired;
    }
    public boolean isRequired()
    {
        return this.isRequired;
    }


    public void setMissing(boolean isMissing)
    {
        this.isMissing = isMissing;
    }
    public boolean getIsMissing()
    {
        return this.isMissing;
    }
    public boolean isMissing()
    {
        return this.isMissing;
    }


    public void setUpdateButtonCount(int updateButtons)
    {
        this.updateButtonCount = updateButtons;
    }
    public int getUpdateButtonCount()
    {
        return this.updateButtonCount;
    }
    public int getUpdateButtons()
    {
        return this.updateButtonCount;
    }


    /**
     * Enable or disable the "Add" button (the first button)
     * @param enable true to enable, false to disable
     */
    public void enableAddButton(boolean enable)
    {
        this.disableAddButton = enable ? ENABLE_BUTTON : DISABLE_BUTTON;
    }
    /** Enable the "Add" button (the first button) */
    public void enableAddButton()
    {
        this.enableAddButton(true);
    }
    /** Disable the "Add" button (the first button) */
    public void disableAddButton()
    {
        this.enableAddButton(false);
    }
    public String getDisableAddButton()
    {
        return this.disableAddButton;
    }


    /**
     * Enable or disable the "Close" (or "Delete") button (the second button)
     * @param enable true to enable, false to disable
     */
    public void enableCloseButton(boolean enable)
    {
        this.disableCloseButton = enable ? ENABLE_BUTTON : DISABLE_BUTTON;
    }
    /** Enable the "Close" (or "Delete") button (the second button) */
    public void enableCloseButton()
    {
        this.enableCloseButton(true);
    }
    /** Disable the "Close" (or "Delete") button (the second button) */
    public void disableCloseButton()
    {
        this.enableCloseButton(false);
    }
    public String getDisableCloseButton()
    {
        return this.disableCloseButton;
    }


    public void setActionType(DossierAttributeType actionType)
    {
        this.actionType = actionType;
    }
    // TODO: But WAIT!  All the uses of action.actionType are now commented out.. do we even need this any more?
    public DossierAttributeType getActionType()
    {
        return this.actionType;
    }


    public void setValue(String value)
    {
        this.value = value;
    }
    public String getValue()
    {
        return this.value;
    }


    public List<DossierFileDto> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DossierFileDto> documents)
    {
        this.documents = documents;
    }

    public void addDocument(DossierFileDto document)
    {
        this.documents.add(document);
    }

    @Override
    public String toString()
    {
        return attributeName + "(" + description + ") = [" + value + "]";
    }
}
