/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewDossierStatus.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.viewdossierstatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeType;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCStatus;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.ManageOpenAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.MivActionList;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.document.PathConstructor;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;


/**
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class ViewDossierStatus extends MivFormAction
{
    private final DCService dcService;
    private final DossierService dossierService;

    private static Logger log = LoggerFactory.getLogger(ViewDossierStatus.class);

    private static final Map<String, Map<String,String>> documentMap = MIVConfig.getConfig().getMap("documentsbyattributename");
    private static final PathConstructor pathConstructor = new PathConstructor();

    private static final Properties strings = PropertyManager.getPropertySet("viewdossierstatus-flow-dossierselect", "strings");

    private static final String SIGNED = "Signed";
    private static final String INVALID = "Signed - Invalid";
    private static final String REQUIRED = "Required - ";
    private static final String OPTIONAL = "Optional - ";
    private static final String REQUIRED_SIGNABLE = "Required - Not Signed";
    private static final String OPTIONAL_SIGNABLE = "Optional - Not Signed";
    private static final String REQUIRED_ADD = "Required - Not Added";
    private static final String OPTIONAL_ADD = "Optional - Not Added";
    private static final String DOCUMENT_ADDED = "Added";
    private static final String REQUIRED_OPEN = "Required - Not Open";
    private static final String OPTIONAL_OPEN = "Optional - Not Open";
    private static final String REQUIRED_RELEASE = "Required - Not Released";
    private static final String NOT_REQUESTED = "Not Requested";

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/YY");


    /**
     * Create the view dossier status spring form action bean.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     * @param dossierService Spring injected dossier service
     * @param dcService Spring injected disclosure certificate service
     */
    public ViewDossierStatus(UserService userService,
                             AuthorizationService authorizationService,
                             DossierService dossierService,
                             DCService dcService)
    {
        super(userService, authorizationService);

        this.dossierService = dossierService;
        this.dcService = dcService;
    }

    /**
     * Put the dossier for the shadow user in the conversation scope and
     * load open action page flow scope attributes if and only if ONE exists.
     * Put the "dossierNotInProcess" status error otherwise.
     *
     * @param context Web flow request context
     * @return webflow event status: success to continue, error to stop
     */
    public Event retrieveDossiers(RequestContext context)
    {
        MivPerson person = getShadowPerson(context);
        List<MivActionList> results = new ArrayList<>();
        try
        {
            Iterator<Dossier> dossiers = dossierService.getAllDossiersByUser(person).iterator();

            while (dossiers.hasNext())
            {
                Dossier dossier = dossiers.next();
                if (!dossier.getLocation().equalsIgnoreCase("Archive"))
                {
                    DossierAppointmentAttributes primaries = dossier.getAttributesByAppointment(dossier.getPrimaryAppointmentKey());
                    MivActionList result = new MivActionList(dossier);
                    result.setSortName(person.getSortName());
                    result.setUserId(person.getUserId());
                    result.setActionLocation(dossier.getLocation());
                    result.setDossierId(dossier.getDossierId());
                    result.setAppointmentType("Primary");
                    result.setSchoolName(primaries.getSchoolDescription());
                    result.setDepartmentName(primaries.getDepartmentDescription());
                    result.setDelegationAuthority(dossier.getDelegationAuthorityDescription());
                    result.setActionType(dossier.getAction().getActionType().getDescription());
                    result.setSubmitDate(dateFormat.format(dossier.getSubmittedDate()));
                    result.setLastRoutedDate(dateFormat.format(dossier.getLastRoutedDate()));
                    result.setLocationDescription(dossier.getLocationDescription(dossier.getLocation()));

                    results.add(result);
                }
            }
        }
        catch (WorkflowException e)
        {
            log.warn("retrieveDossier: failed to get dossier");
        }

        context.getFlowScope().put("results", results);

        return success();
    }

    public void setStrings(RequestContext context)
    {
        context.getFlashScope().put("strings", strings);
    }

    /**
     * Put the dossier for the shadow user in the conversation scope and
     * load open action page flow scope attributes if and only if ONE exists.
     * Put the "dossierNotInProcess" status error otherwise.
     *
     * @param context Web flow request context
     * @return webflow event status: success to continue, error to stop
     */
    public Event retrieveDossier(RequestContext context)
    {
        String dossierId = context.getRequestParameters().get("dossierId");
        MivPerson person = getShadowPerson(context);

        Dossier dossier = null;
        try
        {
            dossier = dossierService.getDossierAndLoadAllData(Long.parseLong(dossierId));
        }
        catch (WorkflowException e)
        {
            log.warn("retrieveDossier: failed to get dossier");
        }

        // add view status error if new dossier or at packet request location
        if (dossier == null)
        {
            context.getFlowScope().put("viewStatusError", "dossierNotInProcess");

            return error();
        }
        // add error if dossier doesn't belong to user.
        else if (person.getUserId() != dossier.getAction().getCandidate().getUserId())
        {
            context.getFlowScope().put("viewStatusError", "cantView");

            return error();
        }

        // load dossier into the conversation scope
        context.getConversationScope().put("dossier", dossier);

        // continue loading open action
        return loadOpenAction(context);
    }


    /**
     * Load the open action attributes into the flow scope for the dossier in the conversation scope.
     *
     * There are some critical fields that are held in the FlowScope worth noting:
     * <ul>
     * <li>displayOnly
     *     <ul>
     *     <li>This prevents the links at the bottom of the page from being displayed</li>
     *     <li>it only takes one updatable appointment to make the links display</li>
     *     </ul>
     * </li>
     * <li>displayAppointmentDetails
     *     <ul>
     *     <li>tells the page to show/not show any appointment details</li>
     *     </ul>
     * <li>lastStateId
     *     <ul>
     *     <li>this is set in the methods that route the dossier</li>
     *     <li>If it is present it indicated the last action on this dossier was to route it</li>
     * </ul>
     *
     * These fields are removed every time a new dossier is selected (in {@link ManageOpenAction#resetFlowScope(RequestContext)})
     *
     * @param context Webflow request context
     * @return event status success or error
     */
    public Event loadOpenAction(RequestContext context)
    {
        MutableAttributeMap<Object> flowScope = context.getFlowScope();
        MivPerson shadowPerson = getShadowPerson(context);

        Dossier dossier = getDossier(getDossier(context) == null ? flowScope.getLong("dossierId") : getDossier(context).getDossierId());


        DossierLocation location = dossier.getDossierLocation();

        // If this dossier is ready to archive we do not need to show anything else
        // or if this dossier has just been routed, then display a confirmation message
        if (DossierLocation.READYFORPOSTREVIEWAUDIT == location
         || "routerequest".equalsIgnoreCase(flowScope.getString("lastTransaction")))
        {
            flowScope.put("displayOnly", true);         // This will prevent the display of the links on the page
            flowScope.put("displayAppointmentDetails", false);  // This will prevent display of dossier details
            return success();
        }

        // Construct URL from file path
        flowScope.put("dossierpdf",
                      pathConstructor.getUrlFromPath(dossier.getDossierPdf()));

        // See if this dossier is ready to be routed on to the next node
        List<String> routeErrors = Collections.emptyList();
        try
        {
            routeErrors = dossierService.validateRoutingState(dossier);
        }
        catch (WorkflowException e)
        {
            log.warn("retrieveDossier: failed to get dossier == {}", dossier.getDossierId());
        }

        // Dossier is Completed if there are no errors and we're not at the packet request location
        boolean dossierCompleted = routeErrors.size() == 0 && DossierLocation.PACKETREQUEST != location;

        flowScope.put("routeStatus", dossierCompleted ? "ready" : "stop");
        flowScope.put("dossierProgress", dossierCompleted ? "Completed" : (!dossierCompleted && DossierLocation.PACKETREQUEST == location ? "Unsatisfied Packet Request" : "In Progress"));

        flowScope.remove("routingErrorList");

        if (!dossierCompleted) // for testing we want to log the errors
        {
            if (log.isInfoEnabled())
            {
                String initiatorPrincipalName = dossier.getAction().getCandidate().getDisplayName();

                log.info(" **TESTING** dossier for {} dossierID == {} will not route from {} because -- {}",
                         new Object[] { initiatorPrincipalName, dossier.getDossierId(), location, routeErrors });
            }
            flowScope.put("routingErrorList", routeErrors);
        }

        return processDossierAttributes(dossier,
                                        shadowPerson,
                                        flowScope,
                                        "manageAction-flow".equals(getFlowId(context)))
             ? success()
             : error();
    }


    /**
     * Process the dossier attributes for each appointment building a list
     * of appointment along with their various attributes to be displayed
     * on the Manage Open Action page or ViewDossierStatus page.
     *
     * @param dossier
     * @param targetPerson
     * @param flowScope
     * @param isOpenActionFlow
     * @return <code>true</code>
     */
    private boolean processDossierAttributes(Dossier dossier, MivPerson targetPerson, MutableAttributeMap<Object> flowScope, boolean isOpenActionFlow)
    {
        List<DossierAppointment> results = new ArrayList<DossierAppointment>();
//        final String location = dossier.getLocation();
        final DossierLocation currentDossierLocation = dossier.getDossierLocation();//DossierLocation.mapWorkflowNodeNameToLocation(location);

        boolean displayOnly = true;  // this is used to determine if the dossier data is for "display only" or may be modified
        boolean jointRouteStatusSet = false;  // set route Status only once when multiple appointments are updateable
        boolean canSeePrimary = false;  // Used to help eliminate appointments that cannot be viewed by this user.

        // Get the attribute map for each school and department and build a DossierAppointment;
        Map <DossierAppointmentAttributeKey, DossierAppointmentAttributes> dossierAppointmentAttributes = dossier.getAppointmentAttributes();
        Integer appointmentCount = 0;

        for (DossierAppointmentAttributeKey apptKey : dossierAppointmentAttributes.keySet())
        {
            appointmentCount++;
            // Get the dossierAppointment for this appointment key
            DossierAppointment dossierAppointment = this.getAppointment(dossier, apptKey, targetPerson);

            // School and department qualifiers for authorization checks
            AttributeSet qualification = new AttributeSet();
            qualification.put(Qualifier.DEPARTMENT, Integer.toString( dossierAppointment.getDeptId() ));
            qualification.put(Qualifier.SCHOOL, Integer.toString( dossierAppointment.getSchoolId() ));
            qualification.put(Qualifier.USERID, Integer.toString( dossier.getAction().getCandidate().getUserId() ));

            // If this is a primary appointment which is being viewed,
            // make sure the viewer (targetPerson) is authorized to view it.
            if (dossierAppointment.isPrimary())
            {
                // See if the targetPerson is allowed to view the primary appointment
                canSeePrimary = authorizationService.isAuthorized(targetPerson,
                                                                  Permission.VIEW_DOSSIER,
                                                                  null,
                                                                  qualification);

                // Put the primary school/department description in the flowscope. Use to
                // show primary school appointment information at the top portion of the page
                flowScope.put("schoolDept", dossierAppointment.getAppointmentDescription());
            }

            // Open action flow has additional authorization checks
            if (isOpenActionFlow)
            {
                // Additional permission details for location
                AttributeSet permissionDetails = new AttributeSet();
                permissionDetails.put("DOSSIERLOCATION", dossier.getLocation());

                // Set update flag if the target person is allowed to edit the dossier
                dossierAppointment.setUpdate(this.authorizationService.isAuthorized(targetPerson, Permission.EDIT_DOSSIER, permissionDetails, qualification));

                // TODO: Don't like this...but will leave if for now, but it's an indication that something
                // is not quite right where this flag is being updated.
                //MIV-3401 Since the isAppointmentComplete flag was getting out of sync with the actual state
                // of the joint appointment, we will update the database if the appointment is NOT complete but
                // the JointComplete flag is TRUE But only if this is the manage-open-actions-flow.
                if (!dossierAppointment.isPrimary() &&
                    (dossierAppointment.isJointComplete() && !dossierAppointment.isComplete()))
                {
                    dossier.setIsAppointmentComplete(apptKey, false);
                    dossierAppointment.setJointComplete(false);
                    try
                    {
                        dossierService.saveDossier(dossier);
                    }
                    catch (WorkflowException e)
                    {
                        //log.warn("ViewDossierStatus: failed to save the Completed flag in dossier == " +
                        //         dossier.getDossierId() + " exception-> " + e.getMessage());
                        log.warn("ViewDossierStatus: failed to save the Completed flag in dossier == {} exception-> {}",
                                 dossier.getDossierId(), e.getMessage());
                    }
                }
                // If this is the action flow we will only fill with either viewable or modifiable appointments
                // That means if you are viewing from the joint appointment (through the actions flow) you will
                // not see the primary appointment information nor the other joint appointments outside of your area
                if (!canSeePrimary &&
                    !dossierAppointment.isUpdate() &&
                    !this.authorizationService.isAuthorized(targetPerson, Permission.VIEW_DOSSIER, null, qualification))
                {
                    // Do not add this DossierAction to the results list, the viewer isn't allowed to see it
                    continue;
                }
            }

            // Set up the links at the bottom of the page
            if (!dossierAppointment.isPrimary() && dossierAppointment.isUpdate())
            {
                if (dossier.isAppointmentComplete(apptKey) && dossierAppointment.isUpdate())
                {
                    dossierAppointment.setUpdate(false);
                }
                else if (!canSeePrimary && !jointRouteStatusSet && dossierAppointment.isComplete())
                {
                    jointRouteStatusSet = true;
                }
            }

            if (dossierAppointment.isUpdate())  // It only takes one update-able appointment to make this true
            {
                displayOnly = false;
            }

            // If this is a joint appointment and the location is 'department', 'school' or "federationschool"
            //  then it is possible to return to the joint department, show a link just for this purpose
            //  but only to the primary school/department viewer (otherwise they see a special status msg)
            if (!displayOnly && !dossierAppointment.isPrimary() &&
                    (currentDossierLocation == DossierLocation.DEPARTMENT ||
                     currentDossierLocation == DossierLocation.SCHOOL ||
                     currentDossierLocation == DossierLocation.POSTAUDITREVIEW ||
                     currentDossierLocation == DossierLocation.POSTAPPEALSCHOOL ||
                     currentDossierLocation == DossierLocation.POSTSENATESCHOOL))
            {
                dossierAppointment.setShowJointSendBackLink(
                        canSeePrimary && dossierAppointment.isJointComplete() && dossierAppointment.isComplete());

                if (!dossierAppointment.isComplete() ||
                        (!dossierAppointment.isUpdate() && !dossierAppointment.isJointComplete()))
                {
                    dossierAppointment.setJointWaitingMsg("(Has Not Been Sent to Primary " +
                            (currentDossierLocation == DossierLocation.DEPARTMENT ? "Department" : "School") + ")");
                }
            }

            // Check to add this appointment to the results list
            if (dossierAppointment.getActionList().size() > 0)   //joint appointment @ Vice Provost's location will drop
            {
                // This appointment may be updated by the targetuser, however not all attributes which are
                // available may be modified. The below method filters out those attributes which may
                // not be edited by the targetPerson based on role.
                dossierAppointment = filterAppointmentAttributes(targetPerson, dossierAppointment);

                results.add(dossierAppointment);
            }
            // There are no items in the action list, but still want to display the joint appointment header as complete
            // Right now just for SENATE locations, but may want to do this for all?
            else if (currentDossierLocation != DossierLocation.SENATE_OFFICE &&
                     currentDossierLocation != DossierLocation.FEDERATION &&
                     currentDossierLocation != DossierLocation.SENATEFEDERATION &&
                     currentDossierLocation != DossierLocation.VICEPROVOST &&
                     currentDossierLocation != DossierLocation.POSTAUDITREVIEW)
            {
                dossierAppointment.setShowJointSendBackLink(false);  // Can't "send back". Always considered complete since there are no items to act upon.
                dossierAppointment.setJointWaitingMsg("");
                dossierAppointment.setJointComplete(true);
                dossierAppointment.setUpdate(false);
                results.add(dossierAppointment);
            }

        }

        // Save a flowType for the signature action based on dossier location
        if (currentDossierLocation == DossierLocation.SCHOOL ||
            currentDossierLocation == DossierLocation.POSTSENATESCHOOL ||
            currentDossierLocation == DossierLocation.POSTAPPEALSCHOOL)
        {
            flowScope.put("decision", "dean");
        }
        else if (currentDossierLocation == DossierLocation.VICEPROVOST ||
                 currentDossierLocation == DossierLocation.POSTSENATEVICEPROVOST ||
                 currentDossierLocation == DossierLocation.POSTAPPEALVICEPROVOST)
        {
            flowScope.put("decision", "provost");
        }

        // Last of all, if this is an action display and this is not the primary department
        // show the link to allow the completion of the joint appointment (but only when everything else is done)
        boolean completeJoint = !canSeePrimary && !displayOnly;

        flowScope.put("completeJoint", completeJoint);
        flowScope.put("canSeePrimary", canSeePrimary);
        flowScope.put("displayOnly", displayOnly);
        flowScope.put("results", results);

        // Show appointment details at all locations except readytoarchive.
        if (currentDossierLocation != DossierLocation.READYFORPOSTREVIEWAUDIT)
        {
            flowScope.put("displayAppointmentDetails", true);
        }

        // Find out where the dossier goes next and if this is a link that will be enabled
        WorkflowNode previousNode = null;

        Map<String, WorkflowNode>nextNodesStatusMap = new LinkedHashMap<String, WorkflowNode>();
        WorkflowNode currentWorkflowNode = null;

        try
        {
            currentWorkflowNode = dossierService.getCurrentWorkflowNode(dossier);

            // If we are at the PacketRequest location, simulate that the dossier is at the next location (Department) location to force
            // the routing for the School location to appear. See MIV-5956 for details.
            if (currentWorkflowNode.getNodeLocation() == DossierLocation.PACKETREQUEST)
            {
                currentWorkflowNode = dossierService.getNextWorkflowNode(dossier);
            }

            // Get the next workflow nodes for this location
            for (WorkflowNode node : currentWorkflowNode.getNextWorkflowNodes())
            {
                // Add a status for this node to be used by the jsp page -  default to "stop"meaning not selectable on the jsp page or "hidden"
                node.setWorkflowNodeStatus(node.isDisplayOnlyWhenAvailable() ? "hidden" : "stop");

                // Validate that the prerequisite attributes have been satisfied for this dossier's next node
                if (dossierService.validateWorkflowNodePrerequisites(dossier, results, node))
                {
                    // Prerequisites are satisfied, now check if this is going to the archive and if it is
                    // make sure the target Person is authorized
                    if (node.getNodeLocation() != DossierLocation.ARCHIVE  ||
                       (node.getNodeLocation() == DossierLocation.ARCHIVE &&
                         this.authorizationService.isAuthorized(targetPerson, Permission.ARCHIVE_DOSSIER, null, null)))
                    {
                        // Set the routing status for this node to "ready" - it will be selectable on the jsp page
                        flowScope.put("routeStatus", "ready");
                        node.setWorkflowNodeStatus("ready");
                    }
                }
                nextNodesStatusMap.put(node.getWorkflowNodeName(), node);
            }
            previousNode = dossierService.getPreviousWorkflowNode(dossier);
        }
        catch (WorkflowException e)
        {
            log.warn("retrieveDossier: failed to get previousWorkflowNode/nextWorkflowNode for dossier == {} : {}", dossier.getDossierId(), e.getLocalizedMessage());
        }
        flowScope.put("previousNodeName", previousNode.getWorkflowNodeName());
        flowScope.put("currentNode", currentWorkflowNode);
        flowScope.put("currentNodeName", currentWorkflowNode.getWorkflowNodeName());
        flowScope.put("nextNodesStatusMap", nextNodesStatusMap);
        flowScope.put("previousNode", previousNode);
        return true;
    }


    /**
     * filterAppointmentAttributes - Each appointment has dossier attributes specified for the current
     * workflow location, however there may be roles which limited in the dossier attributes they
     * are allowed to edit for an appointment. This method gets those attribute ids which are limited for
     * a role and filters out those not included. If there are no attribute id's
     * specified for a role, all are allowed to be edited.
     *
     * @param mivPerson
     * @param dossierAppointment
     */
    private DossierAppointment filterAppointmentAttributes(MivPerson mivPerson, DossierAppointment dossierAppointment)
    {

        // Get the role of the input mivPerson
        MivRole mivRole = mivPerson.getPrimaryRoleType();


        // If this person's primary role is candidate and they are either a DEAN, DEPT_CHAIR, or VICE_PROVOST
        // if they have both roles and if they do, determine which to use.
        if (mivRole == MivRole.CANDIDATE && mivPerson.hasRole(MivRole.DEAN, MivRole.DEPT_CHAIR, MivRole.VICE_PROVOST))
        {
            // if CANDIDATE has VICE_PROVOST role, it has priority
            if (mivPerson.hasRole(MivRole.VICE_PROVOST))
            {
                mivRole = MivRole.VICE_PROVOST;
            }
            // if they have both DEAN and DEPT_CHAIR roles, determine which to use.
            else if (mivPerson.hasRole(MivRole.DEAN) && mivPerson.hasRole(MivRole.DEPT_CHAIR))
            {
                Scope scope = new Scope(dossierAppointment.getSchoolId(),dossierAppointment.getDeptId());
                MivRole tempRole = MivRole.DEPT_CHAIR;
                for (AssignedRole assignedRole : mivPerson.getAssignedRoles())
                {
                   // Check the DEAN role first
                   if (assignedRole.getRole() == MivRole.DEAN)
                   {
                       if (assignedRole.getScope().matches(scope))
                       {
                           tempRole = assignedRole.getRole();
                           break;
                       }
                   }
                }
                mivRole = tempRole;
            }
            else
            {
                mivRole = mivPerson.hasRole(MivRole.DEAN) ? MivRole.DEAN : MivRole.DEPT_CHAIR;
            }
        }

        // Get the map of roles by mivcode
        Map<String, Map<String, String>> mivRolesByMivcodeMap = MIVConfig.getConfig().getMap("mivrolesbymivcode");

        // See if any attributes specified that this role is allowed to modify.
        String editDossierAttributeIdString = mivRolesByMivcodeMap.get(mivRole.name()).get("limitedittodossierattributeids");

        // If there are no attribute id's specified, then the role is allowed to modify all
        if (editDossierAttributeIdString != null)
        {
            // Split the comma delimited string of attribute id's
            List<String> editDossierAttributeIds = Arrays.asList(editDossierAttributeIdString.split(","));
            // Get the list of DossierActions in this appointment
            List<DossierAction>dossierActionList = dossierAppointment.getActionList();
            // Check if the attribute id of each action is in the editDossierAttributeIds list
            for (DossierAction dossierAction : dossierActionList)
            {
                // If this attribute id is not in the list, disable all buttons
                // TODO: (RMH) We could also just remove the attribute, but for now
                // we will disable the buttons of those attributes which are not allowed to edit.
                if (!editDossierAttributeIds.contains(dossierAction.getAttributeId()))
                {
                    dossierAction.disableAddButton();
                    dossierAction.disableCloseButton();
                }
            }
            dossierAppointment.setShowJointSendBackLink(false);
        }
        return dossierAppointment;
    }


    /**
     * Build a DossierAppointment object by processing the input attributes for an  appointment.
     *
     * @param dossier the dossier being processed
     * @param apptKey the key to retrieve the attributes
     * @param targetPerson target, switched-to person requesting the appointment object
     * @return DossierAppointment object
     */
    private DossierAppointment getAppointment(Dossier dossier,
                                              DossierAppointmentAttributeKey apptKey,
                                              MivPerson targetPerson)
    {
        DossierAppointmentAttributes dossierAppointmentAttributes = dossier.getAttributesByAppointment(apptKey);

        DossierAppointment dossierAppointment = new DossierAppointment();
        dossierAppointment.setAppointmentDescription(dossierAppointmentAttributes.getDepartmentMap().get("description").equals(StringUtil.EMPTY_STRING) ?
                dossierAppointmentAttributes.getDepartmentMap().get("school") :
                    dossierAppointmentAttributes.getDepartmentMap().get("school")+ " - " +
                    dossierAppointmentAttributes.getDepartmentMap().get("description"));
        dossierAppointment.setPrimary(dossierAppointmentAttributes.isPrimary());
        dossierAppointment.setSchoolId(Integer.parseInt(dossierAppointmentAttributes.getSchoolId()));
        dossierAppointment.setDeptId(Integer.parseInt(dossierAppointmentAttributes.getDeptId()));

        // Get the attribute/action information for this appointment
        int missingCount = 0;  // used to determine the appropriate processing message for this appointment
        boolean dossierIsReleased = false;
        // Get the attributes associated with this dossier
        for (String attributeKey : dossierAppointmentAttributes.getAttributes().keySet())
        {
            // Get the data for each individual attribute
            DossierAttribute dossierAttribute = dossierAppointmentAttributes.getAttributes().get(attributeKey);
            // If this attribute is not for display, skip it
            if (!dossierAttribute.isDisplay())
            {
                continue;
            }
            // Create the display information for this attribute - to be used by the JSP.
            DossierAction dossierAction = new DossierAction();
            // Only fill with attributes for the current location of the dossier
            dossierAction.setAttributeId(String.valueOf(dossierAttribute.getAttributeId()));
            dossierAction.setAttributeName(dossierAttribute.getAttributeName());
            dossierAction.setAttributeStatus(dossierAttribute.getAttributeValue());
            dossierAction.setDescription(dossierAttribute.getAttributeDescription());
            dossierAction.setRequired(dossierAttribute.isRequired());
            dossierAction.setDisplayProperty(dossierAttribute.getDisplayProperty());
            // Get it once, we use it a lot - note it will always be ALL UPPER CASE
            DossierAttributeStatus value = dossierAttribute.getAttributeValue();

            dossierAction.setMissing(value == null);

            Map<String,String> documentTypeMap = documentMap.get(dossierAttribute.getAttributeName());

            int maxUploads = documentTypeMap != null ? Integer.parseInt(documentTypeMap.get("maxuploads")) : 0;
            boolean isSignable = documentTypeMap != null && "1".equals(documentTypeMap.get("signable"));

            dossierAction.enableAddButton();
            dossierAction.enableCloseButton();

            switch (dossierAttribute.getAttributeType())
            {
                // The ACTION case was assuming it's always OPEN v. CLOSED because those
                // were true for 'review' and 'vote', the only ACTION types at the time.
                // Now dean_release and vp_release change that assumption.
                case ACTION:
                    if (dossierAttribute.getAttributeName().endsWith("_release")) {
                        dossierAction.setValue(REQUIRED_RELEASE);
                    }
                    else if (dossierAttribute.getAttributeName().endsWith("_request")) {
                        dossierAction.setValue(NOT_REQUESTED);
                    }
                    else {
                        dossierAction.setValue(dossierAttribute.isRequired() ? REQUIRED_OPEN : OPTIONAL_OPEN);
                    }
                    // Handle the Voting and Reviewing periods by requiring the "Close" button
                    // if the period is now "Open". Manage button states for Release and Hold.
                    dossierAction.setActionType(DossierAttributeType.ACTION);
                    dossierAction.setUpdateButtonCount(2);
                    if (value != null)
                    {
                        switch (value)
                        {
                            case REQUESTED:                     //Packets are Requested but treated like "Open"
                            case OPEN:
                                dossierAction.setMissing(true);  // this controls the red/green display and button activation
                                dossierAction.setRequired(true); // letting the viewer know that the close action must happen.
                                dossierAction.disableAddButton();
                                break;
                            case CLOSE:
                            case CANCEL:
                                dossierAction.disableCloseButton();
                                break;
                            case RELEASE:
                                dossierIsReleased = true;
                                dossierAction.disableAddButton();
                                break;
                            case HOLD:
                                dossierAction.disableCloseButton();
                                dossierAction.setMissing(true);
                                break;
                            default: break;
                        }
                        dossierAction.setValue(value.getLabel());
                    }
                    else
                    {
                        dossierAction.disableCloseButton();
                    }

                    //Show but disable voting and reviewing at the packet location
                    if ((dossierAction.getAttributeName().equalsIgnoreCase("vote") ||
                         dossierAction.getAttributeName().equalsIgnoreCase("review")) &&
                         dossier.getLocation().equalsIgnoreCase("PACKETREQUEST"))
                    {
                        dossierAction.disableAddButton();
                        dossierAction.disableCloseButton();
                        dossierAction.setRequired(false);
                    }

                    break;

                case DOCUMENT:
                    dossierAction.setActionType(DossierAttributeType.DOCUMENT); // Treat both as DOCUMENT
                    dossierAction.setUpdateButtonCount(1);
                    if (dossierAttribute.getDocuments().size() > 0 &&
                        dossierAttribute.getDocuments().get(0).isSignable())
                    {
                        switch(dossierAttribute.getDocuments().get(0).getSignatureStatus())
                        {
                            case INVALID:
                                dossierAction.setValue(INVALID);
                                break;
                            case SIGNED:
                                dossierAction.setValue(SIGNED);
                                break;
                            default:
                                dossierAction.setValue(dossierAttribute.isRequired() ? REQUIRED_SIGNABLE : OPTIONAL_SIGNABLE);
                        }
                        dossierAction.setMissing(!dossierAttribute.getDocuments().get(0).isSigned());
                    }
                    else if (value != null)
                    {
                        dossierAction.setValue(value.getLabel());
                    }
                    else
                    {
                        dossierAction.setValue(dossierAttribute.isRequired() ?
                                (isSignable ? REQUIRED_SIGNABLE : REQUIRED_ADD) :
                                    (isSignable ? OPTIONAL_SIGNABLE : OPTIONAL_ADD));
                    }

                    // this dossier attributes document type
                    MivDocument documentType = MivDocument.get(dossierAttribute.getAttributeName());

                    switch (documentType)
                    {
                        /*
                         * Disclosure Certificate action value is DCStatus description.
                         */
                        case DISCLOSURE_CERTIFICATE:
                        case JOINT_DISCLOSURE_CERTIFICATE:

                            // Disable Add/Edit of DC at the PacketRequest location
                            if (DossierLocation.mapWorkflowNodeNameToLocation(dossier.getLocation()) == DossierLocation.PACKETREQUEST)
                            {
                                dossierAction.disableAddButton();
                                break;
                            }

                            DCStatus status = dcService.getStatus(dossier,
                                                                  dossierAppointment.getSchoolId(),
                                                                  dossierAppointment.getDeptId());

                            StringBuilder description = new StringBuilder();

                            // The above method of checking whether it is missing does not work for
                            // DC, because of the rescinding and archiving. Only not missing when there
                            // is one signed.
                            dossierAction.setMissing(false);
                            // action value denotes required or optional if not signed
                            if (status != DCStatus.SIGNED && status != DCStatus.SIGNEDIT)
                            {
                                dossierAction.setMissing(true);
                                description.append(dossierAttribute.isRequired() ? REQUIRED : OPTIONAL);
                            }

                            dossierAction.setValue(description.append(status.getDescription()).toString());

                            break;

                        /*
                         * Disable access to the Dean's/VP's decision or recommendation button until Released.
                         * Or, if the target person does not have permission to sign decisions of this document type
                         */
                        case DEANS_FINAL_DECISION:
                        case DEANS_APPEAL_DECISION:
                        case DEANS_RECOMMENDATION:
                        case JOINT_DEANS_RECOMMENDATION:
                        case JOINT_DEANS_APPEAL_RECOMMENDATION:
                        case VICEPROVOSTS_RECOMMENDATION:
                        case VICEPROVOSTS_FINAL_DECISION:
                        case PROVOSTS_RECOMMENDATION:
                        case PROVOSTS_TENURE_RECOMMENDATION:
                        case PROVOSTS_TENURE_DECISION:
                        case PROVOSTS_FINAL_DECISION:
                        case CHANCELLORS_FINAL_DECISION:
                        case VICEPROVOSTS_APPEAL_DECISION:
                        case VICEPROVOSTS_APPEAL_RECOMMENDATION:
                        case PROVOSTS_APPEAL_RECOMMENDATION:
                        case PROVOSTS_TENURE_APPEAL_RECOMMENDATION:
                        case PROVOSTS_TENURE_APPEAL_DECISION:
                        case PROVOSTS_APPEAL_DECISION:
                        case CHANCELLORS_APPEAL_DECISION:
                            AttributeSet permissionDetails = new AttributeSet();
                            permissionDetails.put(PermissionDetail.DECISION_TYPE, documentType.getAllowedSigner().name());

                            if (isSignable && !dossierIsReleased
                             || !authorizationService.hasPermission(targetPerson,
                                                                    Permission.SIGN_DECISION,
                                                                    permissionDetails))
                            {
                                dossierAction.disableAddButton();
                            }
                            break;
                        default: break;
                    }
                    break;

                case DOCUMENTUPLOAD:
                    dossierAction.setActionType(DossierAttributeType.DOCUMENTUPLOAD);
                    dossierAction.setUpdateButtonCount(2);
                    if (value == null)
                    {
                        if (dossierAttribute.getDocuments().size() == 0)
                        {
                            dossierAction.setValue(dossierAttribute.isRequired() ? REQUIRED_ADD : OPTIONAL_ADD);
                            dossierAction.disableCloseButton();
                        }
                        else
                        {
                            dossierAction.setValue(DOCUMENT_ADDED);
                            dossierAction.setMissing(false);
                        }
                    }
                    else
                    {
                        dossierAction.setValue(value.getLabel());
                    }
                    dossierAction.enableAddButton(dossierAttribute.getDocuments().size() < maxUploads);
                    break;
                case HEADER:
                    dossierAction.setActionType(DossierAttributeType.HEADER);
                    dossierAction.setUpdateButtonCount(0);
                    break;
                default: break;
            }//end switch (DossierAttributeType)

            dossierAction.setDocuments(dossierAttribute.getDocuments());
            dossierAppointment.addAction(dossierAction);
            if (dossierAction.isRequired() && dossierAction.isMissing()) {
                missingCount++;
            }

        } // end loop through attributes for this school/department
        // Now set the completion status for this appointment
        dossierAppointment.setComplete(missingCount == 0);
        dossierAppointment.setJointComplete(dossier.isAppointmentComplete(apptKey));

        return dossierAppointment;
    }

    /**
     * Get dossier for the given dossier ID
     *
     * @param dossierId dossier ID
     * @return dossier with the given ID
     */
    public Dossier getDossier(long dossierId)
    {
        try
        {
            return dossierService.getDossierAndLoadAllData(dossierId);
        }
        catch (WorkflowException e)
        {
            throw new MivSevereApplicationError("errors.dossier", e, dossierId);
        }
    }
}
