/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RebuildArchiveSnapshots.java
 */

package edu.ucdavis.mw.myinfovault.web.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.SchoolDepartmentCriteria;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.web.MIVServlet;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PDFConcatenator;


/**
 *This servlet rebuilds dossier archive snapshots from the PDF files which currently exist in the
 *archive directory for a dossier.
 *
 *There is a caveat;  There is no way to distinguish between redacted/non-redacted documents,
 *they will all be included in all snapshots built.
 *
 *If an archive snapshot already exists, the rebuild is skipped.
 *There is no menu option, invoke with ../miv/util/RebuildArchiveSnapshot 
 *
 * @author Rick Hendricks
 * @since MIV 4.7.1.1
 */
public class RebuildArchiveSnapshots extends MIVServlet
{
    private static final long serialVersionUID = 1L;
    
    private static final ThreadLocal<DateFormat> extensionFormat =
            new ThreadLocal<DateFormat>() {
                @Override protected DateFormat initialValue() {
                    return new SimpleDateFormat("yyyyMMdd_hhmmssSS");
                }
            };

    @Override
    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        ServletOutputStream out = null;
 
        try
        {
            out = response.getOutputStream();
            response.setContentType("text/html");

            prologue(out);
            MivPerson mivPerson = 
                    MivServiceLocator.getUserService().getPersonByMivId(mivSession.get().getUser().getTargetUserInfo().getLoginUser().getUserId());
            if (! mivPerson.hasRole(MivRole.SYS_ADMIN))
            {
                out.println("<p>You are not authorized to use this feature.</p>");
                epilogue(out);
                return;
            }

            out.println("<h2>Rebuild Dossier Archive Snapshot</h2>");
            out.println("<p>Rebuild snapshots for a previously archived dossier.</p>");
            out.println("<div id='queryform'>");
            out.println(" <form name='dossierquery' method='GET' action='RebuildArchiveSnapshots'>");
            out.println("  <label for='dossierid'>Dossier ID: </label>");
            out.println("  <input type='text' name='dossierid' id='dossierid'>");
            out.println("  <input type='submit' value='Rebuild'>");
            out.println(" </form>\r\n</div><!--queryform-->");

            out.println("<div id=\"results\">");

            String dossierId = request.getParameter("dossierid");
   
            // Get the dossier ID
            if (dossierId != null )
            {
                DossierService ds = MivServiceLocator.getDossierService();
                DossierCreatorService dcs = MivServiceLocator.getDossierCreatorService();
                DossierDao dossierDao = (DossierDao) MivServiceLocator.getBean("dossierDao");
                
                Dossier dossier = ds.getDossier(dossierId);
                File dossierDirectory = dossier.getArchiveDirectoryByDocumentFormat(DocumentFormat.PDF);

                if (dossier.getArchivedDate() != null)
                {
                    // Build the Candidate, Admin and Full archives
                    for (MivRole role : Arrays.asList(MivRole.CANDIDATE,MivRole.ARCHIVE_ADMIN,MivRole.ARCHIVE_USER))
                    {
                        // Get all of the documents this role is allowed to view
                        List<DossierDocumentDto> dossierFileList = dcs.getAuthorizedDocuments(role);

                        // Need to maintain the order of the files located
                        LinkedHashMap<String,String> fileMap = new LinkedHashMap<String,String>();

                        // Now go through all of the documents the role is allowed to access and try to find them on disk
                        for (DossierDocumentDto dossierFileDto : dossierFileList)
                        {
                            // Find all files which start with the dossierFileName and have a PDF extension
                            final String dossierFileName = dossierFileDto.getFileName();
                            File [] fileList = dossier.getArchiveDirectoryByDocumentFormat(DocumentFormat.PDF).listFiles(new FilenameFilter() {
                                @Override
                                public boolean accept(File dir, String name)
                                {
                                    if (name.startsWith(dossierFileName) && name.endsWith("."+DocumentFormat.PDF.getFileExtension()))
                                    {
                                        return true;
                                    }
                                    return false;
                                }});

                            if (fileList != null)
                            {
                                // Put all of the documents of the current type found into a map 
                                for (File file : fileList)
                                {
                                    fileMap.put(dossierFileDto.getDescription(), file.getAbsolutePath());
                                }
                            }
                        }
                        
                        // No files to process, get out
                        if (fileMap.isEmpty())
                        {
                            out.println("<p>No files exist for dossier "+dossierId+" from which to rebuild archive snapshots</p>");
                            break;
                        }
                        
                        PDFConcatenator pdfConcat = new PDFConcatenator();
                        
                        // The output PDF file is named according to the role.
                        final String description = role == MivRole.CANDIDATE ? "Candidate_Archive" : role == MivRole.ARCHIVE_ADMIN ? "Full_Archive" : "Admin_Archive"; 
                        
                        File outputFile = new File(dossierDirectory, description + "_snapshot_" +
                                extensionFormat.get().format(new Date()) + "." + DocumentFormat.PDF.getFileExtension());

                        // Check to make sure this archive snapshot does not exist
                        File [] fileList = dossier.getArchiveDirectoryByDocumentFormat(DocumentFormat.PDF).listFiles(new FilenameFilter() {
                            @Override
                            public boolean accept(File dir, String name)
                            {
                                if (name.startsWith(description) && name.endsWith("."+DocumentFormat.PDF.getFileExtension()))
                                {
                                    return true;
                                }
                                return false;
                            }});
                        if (fileList != null && fileList.length > 0)
                        {
                            out.println("<p>Archive snapshot exists - skipped: "+outputFile.getAbsolutePath()+"</p>");
                            continue;
                        }
                            
                        // Concatenate all of the files in the map into the output file
                        pdfConcat.concatenateFiles(fileMap, outputFile);
                        
                        // Update the database to insert an entry for the archive snapshot just created.
                        SchoolDepartmentCriteria schoolDepartmentCriteria = new SchoolDepartmentCriteria();
                        // "0:0" school department key qualifiers for all archives
                        schoolDepartmentCriteria.setDepartment(0);
                        schoolDepartmentCriteria.setSchool(0);
                        dossierDao.updateSnapshot(dossier, role, outputFile, schoolDepartmentCriteria);
                        out.println("<p>Rebuilt archive snapshot: "+outputFile.getAbsolutePath()+"</p>");
                    }

                }
            }
            epilogue(out);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (WorkflowException e)
        {
            e.printStackTrace();
        }

    }

    void prologue(ServletOutputStream out)
    {
        try {
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            out.println("<html>\r\n<head>");
            out.println(" <title>Rebuild Archive Snapshots</title>");
            out.println(" <link rel='stylesheet' type='text/css' href='../myinfovault.css'>");
            out.println(" <link rel='shortcut icon' href='../images/favicon.ico' type='image/x-icon'>");
            out.println(" <style>");
            out.println("  div.resultTable {");
            out.println("    margin-bottom: 1.5em;");
            out.println("  }");
            out.println(" </style>");
            out.println("</head>\r\n<body>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    void epilogue(ServletOutputStream out)
    {
        try {
            out.println("</body>\r\n</html>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    int beginTable(ServletOutputStream out, String... columns)
        throws IOException
    {
        int colcount = 0;
        out.println("\n<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\">");
        out.print("  <thead>\n    <tr>");
        for (String s : columns)
        {
            out.print("<th>" + s + "</th>");
            colcount++;
        }
        out.println("</tr>\n  </thead>\n  <tbody>");
        return colcount;
    }

    void finishTable(ServletOutputStream out) throws IOException
    {
        out.println("  </tbody>\n</table>\n");
    }


}
