/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RebuildEDMSArchives.java
 */

package edu.ucdavis.mw.myinfovault.web.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeType;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.archive.DossierArchiveService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService;
import edu.ucdavis.mw.myinfovault.util.FileUtil;
import edu.ucdavis.mw.myinfovault.web.MIVServlet;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PDFConcatenator;


/**
 *This servlet rebuilds dossier EDMS archives.
 *
 *There is a caveat;  There is no way to reliably distinguish between redacted/non-redacted documents,
 *so it is possible that some redacted documents will end up in the EDMS file under the non-redacted code.
 *
 *There is no menu option, invoke with ../miv/util/RebuildEDMSArchives
 *
 * @author Rick Hendricks
 * @since MIV 4.8.5
 */
public class RebuildEDMSArchives extends MIVServlet
{
    private static final long serialVersionUID = 1L;

    private final ThreadLocal<DateFormat> edmsFileDateFormatter =
            new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("MM-dd-yyyy");
            }
        };

    Logger logger = Logger.getLogger(this.getClass());

    @Override
    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        ServletOutputStream out = null;



        DossierService dossierService = MivServiceLocator.getDossierService();
        DossierArchiveService dossierArchiveService = MivServiceLocator.getDossierArchiveService();
        DossierCreatorService dossierCreatorService = MivServiceLocator.getDossierCreatorService();
        WorkflowService workflowService = MivServiceLocator.getWorkflowService();

        UserService userService = MivServiceLocator.getUserService();

        try
        {
            out = response.getOutputStream();
            response.setContentType("text/html");

            prologue(out);
            MivPerson mivPerson =
                    MivServiceLocator.getUserService().getPersonByMivId(mivSession.get().getUser().getTargetUserInfo().getLoginUser().getUserId());
            if (! mivPerson.hasRole(MivRole.SYS_ADMIN))
            {
                out.println("<p>You are not authorized to use this feature.</p>");
                epilogue(out);
                return;
            }

            out.println("<h2>Rebuild EDMS Archive</h2>");
            out.println("<p>Rebuild EDMS Archive for a previously archived dossier.</p>");
            out.println("<p>Enter the dossier ids for which to rebuild the EDMS archives separeated by a comma or space.</p>");
            out.println("<div id='queryform'>");
            out.println(" <form name='dossierquery' method='GET' action='RebuildEDMSArchives'>");
            out.println("  <label for='dossierid'>Dossier ID(s): </label>");
            out.println("  <textarea name='dossierids' id='dossierids' rows='25' cols='90' wrap='soft'/></textarea>");
            out.println("  <input type='submit' value='Rebuild'>");
            out.println(" </form>\r\n</div><!--queryform-->");

            out.println("<div id=\"results\">");

            String errorMessage = null;
            String dossierIds = request.getParameter("dossierids");

            if (dossierIds == null)
            {
                    out.println("<p>Please enter a valid dossier id or id's separated by commas or spaces.</p>");
                    epilogue(out);
                    return;
            }

            for (String dossierId : Arrays.asList(dossierIds.replaceAll(","," ").replaceAll("\\s+"," ").replaceAll(" ",",").split(",")))
            {
                out.println("<hr><p>Processing dossier "+dossierId+"</p>");
                // Get the dossier ID
                if (dossierId != null )
                {
                    Dossier dossier = null;
                    try
                    {
                        dossier = dossierService.getDossier(dossierId);
                        // dossier must be at the Archive location in order to rebuild EDMS files
                        if (workflowService.getCurrentWorkflowNode(dossier).getNodeLocation() != DossierLocation.ARCHIVE)
                        {
                            errorMessage = "Dossier "+dossierId+" has not been previously archived. Only dossiers which have been archived are eligable to have their EDMS archives rebuilt.";
                            {
                                out.println("<p>"+errorMessage+"</p>");
                                continue;
                            }
                        }
                    }
                    catch (WorkflowException wfe)
                    {
                        logger.error(wfe.getLocalizedMessage());
                        {
                            out.println("<p>"+wfe.getLocalizedMessage()+"</p>");
                            continue;
                        }

                    }

                    // Set routing person to the current user
                    dossier.setRoutingPerson(mivSession.get().getUser().getUserInfo().getPerson());

                    final PDFConcatenator pdfConcatenator = new PDFConcatenator();
                    final String edmsPathName = this.getEdmsPathName();
                    final String displayName = dossier.getAction().getCandidate().getDisplayName();
                    final String personUuid = dossier.getAction().getCandidate().getPersonId();

                    // Make sure an EDMS transfer is not in progress
                    errorMessage = dossierArchiveService.validateEdmsProcessingState();
                    if (errorMessage != null)
                    {
                        logger.error(errorMessage);
                        {
                            out.println("<p>"+errorMessage+"</p>");
                            continue;
                        }
                    }

                    // Get the MivUserInfo for this MivPerson
                    MIVUserInfo mivUserInfo = userService.getMivUserByPersonUuid( personUuid );
                    if (mivUserInfo == null)
                    {
                        errorMessage = "Unable to retrieve MivUserInfo data for " + displayName+ " ("+personUuid+") to EDMS archive dossier "
                                + dossier.getDossierId();
                        logger.error(errorMessage);
                        out.println("<p>"+errorMessage+"</p>");
                        continue;
                    }

                    // Get the employee id
                    final String employeeId = mivUserInfo.getEmployeeID();

                    if (StringUtils.isBlank(employeeId))
                    {
                        errorMessage = "Unable to retrieve employeeId for " + displayName + " ("+personUuid+") to EDMS archive dossier "
                                + dossier.getDossierId();
                        logger.error(errorMessage);
                        out.println("<p>"+errorMessage+"</p>");
                        continue;
                    }

                    this.initArchiveLog(dossier.getArchiveDirectory().getAbsolutePath());

                    logger.info("BEGIN EDMS file rebuild initiated by " + dossier.getRoutingPerson().getDisplayName()
                            + " (" + dossier.getRoutingPerson().getPersonId() + ") for " + dossier.getAction().getCandidate().getDisplayName() + " dossier "
                            + dossier.getDossierId());

                    // Get a list of documents the ARCHIVE_ADMIN role is allowed to view.
                    List<DossierDocumentDto>dossierDocumentList = dossierCreatorService.getAuthorizedDocuments(MivRole.ARCHIVE_ADMIN);

                    // There are no longer any database records pointing files which have already been archived, so we will need to look
                    // in the archiveDirectory for files on the file system with which to rebuild the EDSM files.
                    // Get the files to archive which are present for this dossier
                    Map<String, List<File>> archiveFileMap = new HashMap<String, List <File>>();
                    File archiveDirectory = dossier.getArchiveDirectoryByDocumentFormat(DocumentFormat.PDF);
                    for (DossierDocumentDto dossierDocumentDto : dossierDocumentList)
                    {

                        if (dossierDocumentDto.getDossierAttributeType() == DossierAttributeType.DOCUMENTUPLOAD
                                || dossierDocumentDto.isCandidateFile())
                        {
                            // Find all files which start with the dossierFileName and have a PDF extension
                            final String dossierFileName = dossierDocumentDto.getFileName();
                            File [] fileList = archiveDirectory.listFiles(new FilenameFilter() {
                                @Override
                                public boolean accept(File dir, String name) {
                                    return name.startsWith(dossierFileName) &&
                                            name.endsWith("."+DocumentFormat.PDF.getFileExtension());
                                }});

                            // Add all of the packet and upload files found to the archive file list by EDMS code.
                            if (fileList != null && fileList.length > 0)
                            {
                                // Is this file redactable
                                if (dossierDocumentDto.isAllowRedaction())
                                {
                                    // See if any of the files are tagged as redacted
                                    for (File file : fileList)
                                    {
                                        out.println("Redactable File processed: "+file.getName()+"<br>");

                                        // Default to standard non-redacted code
                                        String edmsCode = dossierDocumentDto.getCode();

                                        //  Use the redacted code if the file name contains "REDACTED"
                                        if (file.getName().contains("REDACTED"))
                                        {
                                            edmsCode = dossierDocumentDto.getRedactedCode();
                                        }
                                        this.addFileByEdmsCode(edmsCode, file, archiveFileMap);
                                    }
                                }
                                else
                                {
                                    this.addEdmsFiles(dossierDocumentDto.getCode(), Arrays.asList(fileList), archiveFileMap);
                                }
                            }
                        }
                        else
                        {
                            // Data based document (RAF, CDC, etc) - Cannot be redacted
                            List<File>files =  getDocuments(dossier, dossierDocumentDto, archiveDirectory);
                            if (!files.isEmpty())
                            {
                                this.addEdmsFiles(dossierDocumentDto.getCode(), files, archiveFileMap);
                            }
                        }

                    }

                    // Check that EDMS files have not been processed for this dossier today
                    if (!this.validateEDMSFiles(archiveFileMap, dossier, employeeId, out))
                    {
                        continue;
                    }

                    // Loop through the archiveFileMap and write the archive file for each code
                    ArrayList<File>edmsFiles = new ArrayList<File>();

                    if (archiveFileMap.isEmpty())
                    {
                        errorMessage = "There are no files available from which to rebuild the EDMS archive for dossier "+ dossier.getDossierId();
                        logger.error(errorMessage);
                        out.println("<p>"+errorMessage+"</p>");
                        continue;

                    }

                    for (String edmsCode : archiveFileMap.keySet())
                    {
                        String edmsFileName = this.constructEDMSFilename(edmsCode, employeeId, dossier.getAction().getActionType().getEDMSDocumentKey());
                        File edmsFile = new File(edmsPathName, edmsFileName);
                        // Log the file names making up the contents of the EDMS file
                        logger.info("======== BEGIN EDMS Code "+edmsCode+" ("+edmsFileName+") file list ========");
                        for (File file : archiveFileMap.get(edmsCode))
                        {
                            logger.info(file.getAbsolutePath());
                        }
                        logger.info("======== END EDMS Code "+edmsCode+" file list ========");
                        // Concatenate the list of files into a single pdf file
                        if (!pdfConcatenator.concatenateFiles(archiveFileMap.get(edmsCode), edmsFile))
                        {
                            logger.error("Failed to create EDMS file "+edmsFile.getAbsolutePath());
                            // If there is an error, remove any files which may have already been created
                            for (File file : edmsFiles)
                            {
                                file.delete();
                                logger.info("Deleted "+file);
                            }
                            errorMessage = "Failed to create EDMS archive file " + edmsFileName + " for " + displayName +" ("+personUuid+") dossier " + dossier.getDossierId()+".";
                        }
                        edmsFiles.add(edmsFile);
                    }

                    // Copy the edms files to the dossier archive directory
                    // A failure at this step is not considered fatal since the EDMS files have been created and placed
                    // in the directory for the transfer.
                    if (errorMessage == null)
                    {
                        // Get/create the target dossier archive directory for the edms files.
                        File edmsArchiveDirectory = new File(dossier.getArchiveDirectory(),"edms");
                        // Create the directory for the EDMS files in the MIV archive directory
                        if (edmsArchiveDirectory.exists() || edmsArchiveDirectory.mkdirs())
                        {
                            String msg = "Copying EDMS files for dossier "+dossier.getDossierId()+" to MIV archive.";
                            logger.info(msg);

                            for (File inputFile : edmsFiles)
                            {
                                File outputFile = new File(edmsArchiveDirectory,inputFile.getName());

                                if (FileUtil.copyFile(inputFile, outputFile))
                                {
                                    logger.info("Copy "+inputFile.getAbsolutePath()+" --> "+outputFile.getAbsolutePath());
                                }
                                else
                                {
                                    logger.info("*** Failed to copy "+inputFile.getAbsolutePath()+" --> "+outputFile.getAbsolutePath());
                                }
                            }
                        }
                        else
                        {
                            String msg = "Unable create a backup copy of EDMS files for dossier"+dossier.getDossierId()+" in the MIV archive."+
                                    " - Archive directory "+edmsArchiveDirectory.getAbsolutePath()+" is not present and could not be created. "+
                                    "However, the original EDMS files have been created and placed in the configured EDMS directory.";
                            logger.warn(msg);
                        }
                    }
                    // Release the archive log.
                    this.releaseArchiveLog();
                    errorMessage = errorMessage == null ? "EDMS archive rebuild complete - dossier: "+dossier.getDossierId()+", userId: "+dossier.getAction().getCandidate().getUserId()+", employeeId: "+employeeId : errorMessage;
                    out.println("<p>"+errorMessage+"</p>");
                }
            }
            epilogue(out);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    void prologue(ServletOutputStream out)
    {
        try {
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            out.println("<html>\r\n<head>");
            out.println(" <title>Rebuild Archive Snapshots</title>");
            out.println(" <link rel='stylesheet' type='text/css' href='../myinfovault.css'>");
            out.println(" <link rel='shortcut icon' href='../images/favicon.ico' type='image/x-icon'>");
            out.println(" <style>");
            out.println("  div.resultTable {");
            out.println("    margin-bottom: 1.5em;");
            out.println("  }");
            out.println(" label {display:block;}");
            out.println(" textarea { display:block;}");
            out.println(" </style>");
            out.println("</head>\r\n<body>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    void epilogue(ServletOutputStream out)
    {
        try {
            out.println("</body>\r\n</html>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    int beginTable(ServletOutputStream out, String... columns)
        throws IOException
    {
        int colcount = 0;
        out.println("\n<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\">");
        out.print("  <thead>\n    <tr>");
        for (String s : columns)
        {
            out.print("<th>" + s + "</th>");
            colcount++;
        }
        out.println("</tr>\n  </thead>\n  <tbody>");
        return colcount;
    }

    void finishTable(ServletOutputStream out) throws IOException
    {
        out.println("  </tbody>\n</table>\n");
    }


    /**
     * getEdmsPathName
     * @return String edmsLocation
     */
    private String getEdmsPathName()
    {
        // Get the edms location
        String edmsPathName = MIVConfig.getConfig().getProperty("edms-config-location");
        if (edmsPathName == null)
        {
            edmsPathName = "/var/spool/transfer"; // Default
        }
        return edmsPathName;
    }

    /**
     * Initialize the archive log appender.
     * For convenience there will be a log kept with the archive for each user.
     * If initialization fails, the archive process will continue
     * since the system level logging will still be present and contain
     * the same log entries.
     * @param archiveLogPath
     */
    private void initArchiveLog(String archiveLogPath)
    {
        Appender archiveFileAppender;
        try
        {
            archiveFileAppender =
                new FileAppender(new PatternLayout("%d{MMM dd, yyyy hh:mm:ss a} %c %M%n%-5p [%t]: %m%n"),archiveLogPath+"/archive.log");
            archiveFileAppender.setName("archiveLog");
            logger.addAppender(archiveFileAppender);
        }
        catch (IOException e)
        {
            logger.error("Archive file appender initialization failed!");
        }
    }

    /**
     * Release the appender for the archive log
     */
    private void releaseArchiveLog()
    {
        logger.removeAppender("archiveLog");
    }

    /**
     * Add files to the archiveFileMap
     * @param edmsCode
     * @param files
     * @param archiveFileMap
     */
    private void addEdmsFiles(String edmsCode, List<File>files,  Map<String, List<File>> archiveFileMap)
    {
        for (File file : files)
        {
            addFileByEdmsCode(edmsCode, file, archiveFileMap);
        }
    }


    /**
     * Add an entry to the achiveFileMap
     * @param edmsCode
     * @param file
     * @param archiveFileMap
     */
    private void addFileByEdmsCode(String edmsCode, File file,  Map<String, List<File>> archiveFileMap)
    {
        // If there is no EDMS code, don't archive
        if (StringUtils.isEmpty(edmsCode))
        {
            return;
        }

        if (!archiveFileMap.containsKey(edmsCode))
        {
            archiveFileMap.put(edmsCode, new ArrayList<File>());
        }

        if (!archiveFileMap.get(edmsCode).contains(file))
        {
            archiveFileMap.get(edmsCode).add(file);
        }

        return;
    }

    /**
     * constructEDMSFileName - Construct the EDMS file name in the form -
     * cccEEEEEEEEEMMddyyyy[edmsDocumentKey] where:
     *
     * ccc          = EDMS code - 3 digits
     * EEEEEEEEE    = Employee ID - 9 digits
     * MM           = Current Month
     * dd           = Current Day
     * yyyy         = Current Year
     * [edmsDocumentKey] = As defined in DossierActionType
     *
     * Example file name for a Disclosure Certificate for employee 123456789 in a Promotion created on 11/24/09:
     *
     * 29712345678911242009Promotion.pdf
     *
     * This file format must be followed exactly to be properly processed by the EDMS system.
     *
     * @param edmsCode
     * @param employeeId
     * @param edmsDocumentKey
     * @return String fileName
     */
    private String constructEDMSFilename(String edmsCode, String employeeId, String edmsDocumentKey)
    {
        // There is a proposed new EDMS replacement system
        // A test has been proposed which needs the spooled file names in a slightly different format.
        // This 'newEdms' flag controls that. Leave it at "false" unless you are involved with running the test.
        boolean newEdms = false;  // LEAVE THIS AS FALSE!!
        if (newEdms)
        {
            return edmsCode + "_" + employeeId + "_" + edmsFileDateFormatter.get().format(new Date()) + "_" + edmsDocumentKey;
        }
        else
        {
            DateFormat edmsDateFormat = new SimpleDateFormat("MMddyyyy");
            String edmsDateString = edmsDateFormat.format(new Date());
            return edmsCode + employeeId + edmsDateString + edmsDocumentKey + ".pdf";
        }
    }

    /**
     * Get the documents for this DossierDocument by searching for files with the
     * appropriate name on the file system.
     *
     * @param dossier Dossier
     * @param dossierDocumentDto DTO representing a distinct dossier document
     * @param dossierDirectory Directory to search
     * @return List of files located for this document
     */
    private List<File> getDocuments(Dossier dossier,
            DossierDocumentDto dossierDocumentDto,
            File dossierDirectory)
    {
        // Find all files which start with the dossierFileName_<schoolId>_<departmentId> and have a PDF extension
        final String dossierFileName = dossierDocumentDto.getFileName();
        final File [] fileList = dossierDirectory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name)
            {
//                if (name.startsWith(dossierFileName + "_") && name.endsWith("."+DocumentFormat.PDF.getFileExtension()))
//                {
//                    return true;
//                }
//                return false;
                return name.startsWith(dossierFileName + "_") &&
                       name.endsWith("." + DocumentFormat.PDF.getFileExtension());
            }});

        // Return all of the files found
        return fileList != null ? Arrays.asList(fileList) : new ArrayList<File>();
    }

    private boolean validateEDMSFiles(Map<String, List<File>>archiveFileMap, Dossier dossier, String employeeId, ServletOutputStream out) throws IOException
    {

        final String edmsPathName = this.getEdmsPathName();
        final String displayName = dossier.getAction().getCandidate().getDisplayName();
        final String personUuid = dossier.getAction().getCandidate().getPersonId();

        // Loop through the archiveFileMap and make sure none of these archive files already exist,
        // if they do, we cannot EDMS archive this dossier
        for (String edmsCode : archiveFileMap.keySet())
        {
            String edmsFileName = this.constructEDMSFilename(edmsCode, employeeId, dossier.getAction().getActionType().getEDMSDocumentKey());
            if (new File(edmsPathName, edmsFileName).exists())
            {
                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

                String errorMessage = "EDMS archive files created today ("+dateFormat.format(new Date())+") are already present for " + displayName + " ("+personUuid+"). These files must be processed before an EDMS archive is attempted.";
                logger.error(errorMessage);
                out.println("<p>"+errorMessage+"</p>");
                return false;
            }
        }
        return true;
    }

}
