/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BaseFilter.java
 */

package edu.ucdavis.myinfovault;

import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * TODO: Needs javadoc
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.8
 */
public abstract class BaseFilter implements Filter
{
    protected static final String DEBUG_PARAM = "debug";

    protected static final Pattern htmlMimeTypePattern = Pattern.compile("\\Atext/x?html(;.*)?");

    protected boolean debug = false;//true;

    /** Date formatter used by 'announce' to timestamp printlines */
    public static final ThreadLocal<DateFormat> announcer =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return new SimpleDateFormat("H:mm:ss.SSS");
        }
    };
/*
    public final ThreadLocal<DateFormat> sdf =
            new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("H:mm:ss.SSS");
            }
        };
*/

    /**
     * Write debug information to standard output.
     * @param s a String to write
     */
    protected void announce(String s)
    {
        announce(s, false);
    }


    /**
     * Write debug or forced output information to standard output.
     * @param s a String to write
     * @param force <tt>true</tt> to force output even if debug is off
     */
    protected void announce(String s, boolean force)
    {
        if (this.debug || force)
        {
            String now = announcer.get().format(new Date());
            System.out.println(" || --- " + now + " --->  " + s);
        }
    }


    /**
     * Turn debug output on or off.
     * @param shouldDebug true if debug output should be printed.
     */
    protected void setDebug(boolean shouldDebug)
    {
        this.debug = shouldDebug;
    }
    // Could also add a Guava EventBus listener here to catch changes in the state of debug,
    // but I'm trying not to couple this class with anything MIV specific.

    /**
     * CharResponseWrapper from http://www.oracle.com/technetwork/java/filters-137243.html
     *
     * This is needed to capture and modify the output from the actual servlet that
     * handles the request.
     *
     * This needs to be enhanced and moved to its own top-level class file.
     * See http://jpgmr.wordpress.com/2010/07/28/tutorial-implementing-a-servlet-filter-for-jsonp-callback-with-springs-delegatingfilterproxy/
     */
    protected class CharResponseWrapper extends HttpServletResponseWrapper
    {
        private final CharArrayWriter outputWriter;
        @SuppressWarnings("unused")  // This wrapper doesn't support getOutputStream (yet). This class may be replaced by a more generic Wrapper
        private final ByteArrayOutputStream outputStream; // Add this or something like it to begin enhancing for support of getOutputStream
        private final PrintWriter pw;

        public CharResponseWrapper(HttpServletResponse response)
        {
            super(response);

            announce("Initial encoding is [" + response.getCharacterEncoding() + "]");

            response.setCharacterEncoding("utf-8");
            this.setCharacterEncoding("utf-8");
            outputWriter = new CharArrayWriter();
            pw = new PrintWriter(outputWriter);

            // This isn't actually used yet.
            outputStream = new ByteArrayOutputStream(256); // start slightly bigger than the 32 byte default

            announce("Now encoding is [" + response.getCharacterEncoding() + "]");
        }

        @Override
        public PrintWriter getWriter()
        {
//            return new PrintWriter(output);
            //PrintWriter pw = new PrintWriter(outputWriter);
            return pw;
        }

// Must this also provide a getOutputStream() override, similar to getWriter()?
// We had to change some code that called getOutputStream to use getWriter instead,
// because it was throwing an IllegalStateException in AdditionalEditRecord.deleteRecord
// when preparing to write an AJAX response (right after isAjaxRequest() test)
        @Override
        public ServletOutputStream getOutputStream() throws IOException
        {
        //    FilterServletOutputStream fsos;
            // the "FilterServletOutputStream" was presented as part of a "GenericResponseWrapper"
            // in an article
            //   "Implementing a Servlet Filter for JSONP callback with Spring’s DelegatingFilterProxy"
            // at
            //   http://jpgmr.wordpress.com/2010/07/28/tutorial-implementing-a-servlet-filter-for-jsonp-callback-with-springs-delegatingfilterproxy/

            // For now, until we enhance to support both getWriter() and getOutputStream(),
            // just return the standard output stream that was wrapped by this Wrapper
            //announce("Someone is calling getOutputStream instead of getWriter!", true);
            return getResponse().getOutputStream();
        }


        @Override
        public String toString()
        {
            return outputWriter.toString();
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        // TODO Auto-generated method stub

    }

    void loadConfiguration(String filename)
    {
        System.out.println("TODO: need to write the loadConfiguration method");
        System.out.println("      Named configuration file is [" + filename + "]");
        File f = new File(filename);
        try {
            System.out.println("      The actual file to read is [" + f.getCanonicalPath() + "]");
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    @Override
    public void destroy()
    {
        // TODO Auto-generated method stub

    }
}
