/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVConfig.java
 */


package edu.ucdavis.myinfovault;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.events2.EventListener;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.system.MivApplicationContext;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.audit.AuditLogger;
import edu.ucdavis.myinfovault.data.FieldFormatData;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.QueryTool;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;


/**
 * Holds application level configuration information. Allows making changes to
 * the configuration of the currently running copy of MyInfoVault via JSP forms.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class MIVConfig
{
    private static final String MIV_CONFIG_FILE = "/WEB-INF/classes/mivconfig.properties";
    public static final String CONFIG_PROPERTY_NAME = "config";
    private static final String MIV_FORMATTER_CONFIG_FILE = "/WEB-INF/classes/mivformatterconfig.properties";
    private static final DateFormat df = new SimpleDateFormat("MMM dd, yyyy kk:mm:ss");

    private static final Logger log = LoggerFactory.getLogger(MIVConfig.class);

    private static Properties configProperties = new Properties();
    private static Properties formatterConfigProperties = new Properties();
    private static Properties externalProperties = new Properties();
    private static boolean initComplete = false;

    /** Maintain the loaded collection of application constant values */
    private static Map<String,List<Map<String,String>>> constants = null;
    public PropertyChangeSupport change = new PropertyChangeSupport(this);

    private static ServletContext globalContext;
    private ServletContext context;
    private File fileroot = null;
    private String webroot = "/";
    private String webserver = "http://www.example.com/";

    private boolean fDebug = false;
    private boolean gatherTimings = false;      // Set to false as default for production
                                                // It can be overridden in the mivconfig.properties
    private boolean maintenanceMode = false;
    private boolean pauseOnStartup = false;

    private DataSource dataSource = null;
    private QueryTool querytool = null;
    private int activePoolConnections = 0;


    private String trackerCode = null;


    // Finally Create the singleton AFTER all other static initialization.
    private static MIVConfig config;
    @SuppressWarnings("unused") // Don't have to use this, but it must be created. Do not remove it.
    private AuditLogger auditor = new AuditLogger();

    private EventListener evListener = null;

    private List<String> formatFields = null;
    private LinkedHashMap<Integer, LinkedHashMap<String, LinkedHashMap<String, String>>> formatFieldsMap = null;

    /**
     * Only allow creation of a Singleton MIVConfig object
     * via the {@link #getConfig()} call.
     */
    private MIVConfig(final ServletContext context)
    {
        MIVConfig.config = this;

        this.setGlobalContext(context);


        // FIXME: use a try/catch around creation of the querytool
        //        and report the error, e.g. database connection couldn't be established.
        //System.out.println("MIVConfig constructor: about to create QueryTool");

        querytool = new QueryTool(this.getDataSource());
        querytool.setNote("MIVConfig created in constructor");

        // Sleep for 5 seconds to give us a chance to attach a debugger to debug MIVConfig startup.
        // Set "pause-on-startup" to true in web.xml to do this
        if (pauseOnStartup)
        {
                System.out.println("Sleeping for 5 seconds... attach the debugger now");
                MIVUtil.pause(5000L);
        }

        reload(ConfigReloadConstants.CONSTANTS, ConfigReloadConstants.FORMAT_FIELDS);

        evListener = new EventListener();

        MIVConfig.initComplete = true;

    }

    /**
     * Only allow creation of a Singleton MIVConfig object
     * via the {@link #getConfig()} call.
     */
    private MIVConfig(final ServletContext context, DataSource datasource)
    {
        MIVConfig.config = this;

        this.setGlobalContext(context);


        // FIXME: use a try/catch around creation of the querytool
        //        and report the error, e.g. database connection couldn't be established.
        //System.out.println("MIVConfig constructor: about to create QueryTool");

        querytool = new QueryTool(datasource); // NOTE: This datasource string could be read from properties.
        querytool.setNote("MIVConfig created in constructor");

        // Sleep for 5 seconds to give us a chance to attach a debugger to debug MIVConfig startup.
        // Set "pause-on-startup" to true in web.xml to do this
        if (pauseOnStartup)
        {
                System.out.println("Sleeping for 5 seconds... attach the debugger now");
                MIVUtil.pause(5000L);
        }

        reload(ConfigReloadConstants.CONSTANTS, ConfigReloadConstants.FORMAT_FIELDS);

        evListener = new EventListener();

        MIVConfig.initComplete = true;
    }


    /**
     * Obtain the application configuration object, which will be created if necessary.
     * @return The MyInfoVault configuration object.
     */
    public static MIVConfig getConfig()
    {
        // The logger may not exist yet, so if you want to debug this uncomment the println statements.
        //System.out.println("Entering MIVConfig.getConfig()");
        if (MIVConfig.config == null) // This should never be true, but let's still check for a while
        {
            synchronized(MIVConfig.class)
            {
                System.err.println(df.format(Calendar.getInstance().getTime()) +
                        " !!! MIVConfig.getConfig() - creating the singleton!" +
                        "\n    This should never happen because the singleton is created as a static!"
                        );
            }
            final Exception e = new Exception("MIVConfig.getConfig() requested while singleton is still null!");
            e.fillInStackTrace();
            e.printStackTrace(System.err);
        }

        //System.out.println("Leaving MIVConfig.getConfig() - returning the singleton.");
        return MIVConfig.config;
    }

    /**
     * @param context servlet context
     * @return MIV configuration for the given servlet context
     */
    public static MIVConfig getConfig(final ServletContext context)
    {
        if (MIVConfig.config == null)
        {
            synchronized(MIVConfig.class)
            {
                MIVConfig.config = new MIVConfig(context);
            }
        }
        return MIVConfig.config;
    }

    /**
     * TODO: It looks like this version of getConfig is never used. Is it needed?
     * I have set it to 'private' and nothing complains that it is missing.
     */
    @SuppressWarnings("unused")
    private static MIVConfig getConfig(final ServletContext context, final DataSource datasource)
    {
        if (MIVConfig.config == null)
        {
            synchronized(MIVConfig.class)
            {
                MIVConfig.config = new MIVConfig(context, datasource);
            }
        }
        return MIVConfig.config;
    }



    /**
     * Get the Google Analytics tracker code
     * @return String trackerCode
     */
    public String getGaTrackerCode()
    {
        if (this.trackerCode == null || this.trackerCode.length() == 0) // this.setGaTrackerCode();
        {
            // This was in 'setGaTrackerCode()' but only this method should call it - so moved it inline here.
            System.out.println("Attempting to set ga Tracker code...");
            // This will often fail the first time it's called, because it's too early for isProductionServer() to work.
            // Catch the exception and make sure trackerCode stays null.  If it's not already set, any attempt to
            // get the tracker code will cause it to be set.
            try {
                final boolean production = this.isProductionServer();
                final String tracker = this.isProductionServer() ? "ga-production-trackercode" : "ga-development-trackercode";
                final String code = this.getProperty(tracker);
                System.out.println(df.format(Calendar.getInstance().getTime()) +
                                   "  Production: " + production + " : Setting tracker [" + tracker + "] to code [" + code + "]");
                this.trackerCode = code;
            }
            catch (NullPointerException e) {
                this.trackerCode = null;
                System.out.println("...Failed to set Tracker");
                e.printStackTrace(System.out);
            }
        }

        return this.trackerCode;
    }


    /**
     * Get the EventListner
     * @return EventListener
     */
    public EventListener getEventListener()
    {
        return this.evListener;
    }



    public Properties getProperties()
    {
        return MIVConfig.configProperties;
    }

    public String getProperty(final String key)
    {
        return MIVConfig.configProperties.getProperty(key);
    }

    public Properties getFormatterProperties()
    {
        return MIVConfig.formatterConfigProperties;
    }

    public Properties getExternalProperties()
    {
        return MIVConfig.externalProperties;
    }


    /**
     * Get the debug mode of the config object.
     * @return true if currently configured for debug mode
     * @deprecated use logging such as org.slf4j.Logger.debug("message {}", param) instead.
     */
    @Deprecated
    public boolean isDebug()
    {
        return fDebug;
    }

    public void setDebug(final boolean debug)
    {
        fDebug = debug;
    }


    public void setMaintenanceMode(final boolean maintenanceMode)
    {
        this.maintenanceMode = maintenanceMode;
    }

    public boolean isMaintenanceMode()
    {
        return maintenanceMode;
    }


    public boolean isProductionServer()
    {
//        if (isProductionServer == null) {
//            isProductionServer = getServer().contains(PropertyManager.getPropertySet("server", "config").get("productionname").toString());
//        }
//
//        return isProductionServer;
        return getServer().contains(PropertyManager.getPropertySet("server", "config").get("productionname").toString());
    }



    private void setGlobalContext(final ServletContext ctx)
    {
        // SDP 2007-06-21 globalContext is static: we can set it before doing getConfig
        //MIVConfig.getConfig(); // guarantee the singleton has been initialized.
        if (MIVConfig.globalContext != ctx)
        {
            if (globalContext != null && log != null) log.info("Changing Global Context from " + globalContext + " to " + ctx);
            MIVConfig.globalContext = ctx;
            this.context = ctx;
            this.initContextVars();
        }
    }


    /**
     * Initialize the data source if not done already.
     * Using a datasource to establish a database connection in this fashion
     * enables connection pooling by the application server.
     * @return returns true if datasource is initialized, false otherwise
     */
    private boolean initDataSource()
    {
        boolean isInitialized = true;

        if (this.dataSource == null)
        {
            // Check if MivApplicationContext is available...not available at boot time
            if (MivApplicationContext.getApplicationContext() != null)
            {
                this.dataSource = (DataSource)MivServiceLocator.getBean("myinfovaultDataSource");
            }
            else
            {
                ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("myinfovault-config.xml");
                this.dataSource = (DataSource)ctx.getBean("myinfovaultDataSource");
                ctx.close();
            }

            if (this.dataSource == null)
            {
                log.error("Unable to locate data source for database connection.");
                isInitialized = false;
            }
        }
        return isInitialized;
     }


    /**
     * Get the dataSource.
     * @return datasource or null
     */
    public DataSource getDataSource()
    {
        if (dataSource == null)
        {
            if (!initDataSource())
            {
                dataSource = null;
            }
        }
        return this.dataSource;
    }


    /**
     * Get a database connection. The connection will be retrieved from
     * the connection pool since it is retrieved via a datasource object.
     * @return returns the database connection or null if unable to establish a connection
     */
    public Connection getConnection()
    {
        Connection connection = null;
        try
        {
            if (initDataSource())
            {
                connection = this.dataSource.getConnection();
                connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                this.activePoolConnections++;
            }
        }
        catch (final SQLException sqle)
        {
            log.error("Unable to establish a database connection:", sqle);
            sqle.printStackTrace(System.out);
        }

        // debug
        if (this.fDebug) {
            System.out.println("MIV: MIVConfig.getConnection: Current pool connections in use=" + this.activePoolConnections);
        }
        return connection;
    }


    /**
     * Release a database connection.
     * The connection will be returned to the pool.
     * @param connection
     */
    public void releaseConnection(Connection connection)
    {
        try
        {
            if (connection != null && !connection.isClosed())
            {
                connection.commit();
                connection.close();
                if (this.activePoolConnections > 0) {
                    this.activePoolConnections--;
                }
            }
            connection = null;
        }
        catch (final SQLException sqle)
        {
            // do nothing here...just log it
            log.warn("Attempt to close database connection failed:", sqle);
        }

        // debug
        if (this.fDebug) {
            System.out.println("MIV: MIVConfig.releaseConnection: Current pool connections in use=" + this.activePoolConnections);
        }
    }


    /**
     * @return current number of active pool connections
     */
    public int getActivePoolConnections()
    {
        return this.activePoolConnections;
    }


    /**
     * Anything we might use from the ServletContext &mdash; InitParameters, Attributes, etc. &mdash;
     * should be initialized here.
     */
    void initContextVars()
    {
        /*log.info*/System.err.println("MIVConfig.initContextVars() called");
        if (this.context == null) {
            /*log.info*/System.err.println("No context set, can't run MIVConfig.initContextVars() yet");
            return;
        }

        // We want to set the file root and load the properties file
        // as early as possible so the settings in the file can be used.
        this.fileroot = new File(context.getRealPath("/"));
        System.out.println("MIV: Initialization: fileroot is " + this.fileroot);
        // We might want to pause for 5 sec in the MIVConfig constructor
        // to give us a chance to attach the debugger. Property in web.xml
        this.pauseOnStartup = Boolean.parseBoolean(context.getInitParameter("pause-on-startup"));


        loadProperties();

        loadFormatterProperties();

        loadExternalProperties();

        boolean debug = false;
        final String dbgSetting = getConfig().getProperty("debug");
        if (dbgSetting != null) {
            debug = Boolean.parseBoolean(dbgSetting);
            setDebug(debug);
        }

        final String timeSetting = getConfig().getProperty("timing");
        if (timeSetting != null) {
            setDoTimings(Boolean.parseBoolean(timeSetting));
        }

        final String maintSetting = getConfig().getProperty("maintenancemode");
        if (maintSetting != null) {
            final boolean mode = Boolean.parseBoolean(maintSetting);
            System.out.println("MIV: Initialization: Setting maintenance mode to " + mode);
            setMaintenanceMode(mode);
        }

        // Let's see what this context has to offer
        if (debug)
        {
            System.out.println("MIV: initContextVars() -- what Attributes and InitParameters are available?");
            Enumeration<?> e = context.getAttributeNames();

            System.out.println("Attributes:");
            while (e.hasMoreElements()) {
                final Object o = e.nextElement();
                System.out.println("   " + o.toString() + "==" + context.getAttribute(o.toString()));
            }

            e = context.getInitParameterNames();
            System.out.println("Init Params:");
            while (e.hasMoreElements()) {
                final Object o = e.nextElement();
                final String paramName = o.toString();
                final Object paramValue = context.getInitParameter(paramName);
                System.out.println("   " + paramName + "==" + paramValue);
            }
        }

    }
    // initContextVars()



    /**<p>
     * Returns a Map of entries keyed by the name of the constant subset.
     * Each entry is an Iterable, with elements that have an ID&lt;=&gt;number
     * and Name&lt;=&gt;string.</p>
     * <p>When made available to a JSP access becomes
     * <code>constants.<em>subset</em>.id</code> and
     * <code>constants.<em>subset</em>.name</code></p>
     * <p>The available subsets are:<ul>
     * <li>publicationstatus</li>
     * <li>publicationtype</li>
     * <li>grantstatus</li>
     * <li>granttype</li>
     * <li>grantrole</li>
     * <li>mediatype</li>
     * <li>gatheringrole</li>
     * <li>teachingtype</li>
     * <li>termtype</li>
     * <li>monthname</li>
     * <li>evaluationtype</li>
     * <li>servicetype</li>
     * <li>committeetype</li>
     * <li>schoolname</li>
     * <li>departmentname (all departments the system knows about)</li>
     * <li>activedepartments (only active departments)</li>
     * <li>documents</li>
     * <li>attributetypes</li>
     * </ul></p>
     * @return The MIV application &ldquo;constants&rdquo; loaded from the database.
     */
    public Map<String,List<Map<String,String>>> getConstants()
    {
        return constants;
    }


    /**<p>
     * Load "constant" values from the database.</p>
     * When adding to the set of available constants, be sure to document the
     * new subset names in the javadoc comment for {@link #getConstants()}.
     * @return A boolean indicating whether the constants were successfully loaded or not.
     */
    boolean loadConstants()
    {
        if (querytool == null) {
            return false;
        }

        String query;
        List<Map<String,String>> list;
        final Map<String,List<Map<String,String>>> r = new HashMap<String,List<Map<String,String>>>();

        // ** PUBLICATIONS **
        // Publication Status
        query = "SELECT ID,Description AS Name FROM PublicationStatus ORDER BY ID";
        r.put("publicationstatus", loadConstantValues(query));

        // Publication Type
        query = "SELECT ID,Description AS Name FROM PublicationType ORDER BY Description";
        r.put("publicationtype", loadConstantValues(query));


        // ** GRANTS **
        // Grant Status
        query = "SELECT ID,Name FROM GrantStatus ORDER BY ID";
        r.put("grantstatus", loadConstantValues(query));

        // Grant Type
        query = "SELECT ID,Description AS Name FROM GrantType ORDER BY ID";
        r.put("granttype", loadConstantValues(query));

        // Grant Roles
        query = "SELECT ID, Name FROM GrantRole ORDER BY Sequence";
        r.put("grantrole", loadConstantValues(query));


        // ** Extending Knowledge **
        // Media Types
        query = "SELECT ID,Description AS Name FROM MediaType ORDER BY Description";
        r.put("mediatype", loadConstantValues(query));

        // Gathering Roles
        query = "SELECT ID,Name FROM GatheringRole ORDER BY Name";
        r.put("gatheringrole", loadConstantValues(query));


        // ** Teaching **
        // Teaching Type
        query = "SELECT ID,Description AS Name FROM TeachingType ORDER BY ID"; //NOTE: may want ORDER BY Description
        r.put("teachingtype", loadConstantValues(query));

        // Term Type -- query changed to reteive terms in the new order as requested by users.
        query = "SELECT ID,Description AS Name FROM TermType ORDER BY DropDownSequence";
        r.put("termtype", loadConstantValues(query));

        // Month Names
        query = "SELECT ID,Name FROM MonthName ORDER BY ID";
        r.put("monthname", loadConstantValues(query));

        // Evaluation Types
        query = "SELECT ID,Description AS Name FROM CourseEvaluationType ORDER BY Sequence";
        r.put("evaluationtype", loadConstantValues(query));


        // ** Service **
        // Service Type
        query = "SELECT ID,Description AS Name FROM ServiceType ORDER BY Description";
        r.put("servicetype", loadConstantValues(query));


        // ** Committee **
        // Committee Type
        query = "SELECT ID,Description AS Name FROM CommitteeType ORDER BY Sequence";
        r.put("committeetype", loadConstantValues(query));


        // Biosketch Type
        query = "SELECT ID,Code AS Name FROM BiosketchType";
        r.put("biosketchtype", loadConstantValues(query));

        // School Name
        query = "SELECT ID, SchoolID, Description, Abbreviation FROM SystemSchool ORDER BY Description";
        r.put("schoolname", loadConstantValues(query));

        // Active Schools Only
        query = "SELECT ID, SchoolID, Description, Abbreviation FROM SystemSchool WHERE SystemSchool.Active=TRUE ORDER BY Description";
        r.put("activeschools", loadConstantValues(query));

        // Department Name
        query = "SELECT" +
                " SystemDepartment.ID," +
                " SystemDepartment.SchoolID," +
                " DepartmentID," +
                " SystemDepartment.Description," +
                " SystemDepartment.Abbreviation," +
                " SystemSchool.Description AS School," +
                " SystemSchool.Abbreviation AS SchoolAbbrev" +
                " FROM SystemDepartment, SystemSchool" +
                " WHERE SystemDepartment.SchoolID=SystemSchool.SchoolID" +
                " ORDER BY School, Description" +
                "";
        list = loadConstantValues(query);
        for (Map<String,String> m : list) {
            m.remove("id");
        }
        r.put("departmentname", list);
        //System.out.println("loadConstants loaded " + list.size() + " department names.");


        // Active Departments Only
        query = "SELECT" +
                " SystemDepartment.ID," +
                " SystemDepartment.SchoolID," +
                " DepartmentID," +
                " SystemDepartment.Description," +
                " SystemDepartment.Abbreviation," +
                " SystemSchool.Description AS School," +
                " SystemSchool.Abbreviation AS SchoolAbbrev" +
                " FROM SystemDepartment, SystemSchool" +
                " WHERE SystemDepartment.Active=TRUE AND SystemDepartment.SchoolID=SystemSchool.SchoolID" +
                " ORDER BY School, Description" +
                "";
        list = loadConstantValues(query);
        for (Map<String,String> m : list) {
            m.remove("id");
        }
        r.put("activedepartments", list);
        //System.out.println("loadConstants loaded " + list.size() + " ACTIVE departments.");


        // Documents
        query = "SELECT ID," +
                " Description," +
                " AlternateDescription," +
                " DossierFileName," +
                " AttributeName," +
                " Uploadable," +
                " MaxUploads," +
                " CandidateFile," +
                " ConcatUpload," +
                " AllowRedaction," +
                " CrossDepartmentPermission," +
                " Signable," +
                " UploadFirst" +
                " FROM Document" +
                " ORDER BY ID" +
                "";
        r.put("documents", loadConstantValues(query));

        // Attribute types
        query = "SELECT ID," +
                " Name," +
                " Description," +
                " Required," +
                " Optional," +
                " Display," +
                " Type," +
                " DisplayProperty,"+
                " Sequence" +
                " FROM DossierAttributeType" +
                " ORDER BY Sequence" +
                "";
        r.put("attributetypes", loadConstantValues(query));

        // Miv Roles
        query = "SELECT ID," +
                " Description," +
                " ShortDescription," +
                " MivCode," +
                " KimRoleId," +
                " ViewNonRedactedDocIds," +
                " LimitEditToDossierAttributeIds" +
                " FROM MivRole" +
                "";
        r.put("mivroles", loadConstantValues(query));

        // Workflow Routing Sequence
        query = "SELECT * from WorkflowRoutingSequence rs, " +
                        "DelegationAuthority da, " +
                        "DossierActionType at, " +
                        "WorkflowLocation wfl " +
                        "WHERE " +
                        "rs.DelegationAuthorityID=da.id and " +
                        "rs.ActionTypeId=at.ID and " +
                        "rs.WorkflowLocationID=wfl.ID ORDER BY " +
                        "DelegationAuthorityID, ActionTypeID, RoutingSequence";
        r.put("routingsequence", loadConstantValues(query));

        // Patent Jurisdiction
        query = "SELECT ID, " +
                " Jurisdiction," +
                " Abbreviation," +
                " ApplicationText," +
                " PatentText," +
                " concat(Jurisdiction,' (',Abbreviation,')') `Value`," +
                " ifnull(Tooltip,concat(Jurisdiction,' (',Abbreviation,')')) Tooltip" +
                " FROM PatentJurisdiction" +
                " WHERE Active=true" +
                " ORDER BY Jurisdiction" +
                "";
        r.put("jurisdiction", loadConstantValues(query));

        // Patent Status
        query = "SELECT ID, " +
                " Status," +
                " ifnull(Description,Status) Description" +
                " FROM PatentStatus" +
                " WHERE Active=true" +
                " ORDER BY ID" +
        "";
        r.put("patentstatus", loadConstantValues(query));

        // Patent Types
        query = "SELECT DropdownDetailID AS `ID`, "+
                " DropdownTitle AS `Value`," +
                " ifnull(Description,DropdownTitle) AS `Tip`" +
                " FROM DropdownDetails" +
                " WHERE DropdownMasterID = 1 AND Active = true" +
                " ORDER BY DisplayOrder" +
                "";
        r.put("patenttypes", loadConstantValues(query));

        // Format Data Category
        query = "SELECT DropdownDetailID AS `ID`, "+
                " DropdownTitle AS `Value`," +
                " ifnull(Description,DropdownTitle) AS `Tip`" +
                " FROM DropdownDetails" +
                " WHERE DropdownMasterID = 2 AND Active = true" +
                " ORDER BY DisplayOrder" +
                "";
        r.put("formatdatacategory", loadConstantValues(query));

        // HArCS Creative Activities : Start
        query = "SELECT ID, " +
                " Description `Value`," +
                " Description `Tip`" +
                " FROM WorkType" +
                " ORDER BY Description" +
                "";
        r.put("worktype", loadConstantValues(query));

        query = "SELECT ID, " +
                " Description `Value`," +
                " Description `Tip`" +
                " FROM EventType" +
                " ORDER BY Description" +
                "";
        r.put("eventtype", loadConstantValues(query));

        query = "SELECT ID, " +
                " Description `Value`," +
                " Description `Tip`" +
                " FROM EventCategory" +
                " ORDER BY Description" +
                "";
        r.put("eventcategory", loadConstantValues(query));

        query = "SELECT ID, " +
                " Status `Value`," +
                " Description `Tip`" +
                " FROM WorkStatus" +
                " WHERE Active = true" +
                "";
        r.put("workstatus", loadConstantValues(query));

        query = "SELECT ID, " +
                " Status `Value`," +
                " Description `Tip`" +
                " FROM EventStatus" +
                " WHERE Active = true" +
                "";
        r.put("eventstatus", loadConstantValues(query));

        // HArCS Creative Activities : End

        // Sections
        query = "SELECT ID, " +
        " Name," +
        " RecordName," +
        " SectionBaseTable, " +
        " SectionPrimaryKeyID," +
        " FromTable, " +
        " WhereClause " +
        " FROM Section" +
        " ORDER BY ID" +
        "";
        r.put("sections", loadConstantValues(query));

        // Countries
        query = "SELECT " +
        " `CountryCode` As ID, CONCAT(`CountryCode`,': ',`Name`) As Value, `Name`" +
        " FROM `GeoCountry` " +
        " ORDER BY `Name`" +
        "";
        r.put("countries", loadConstantValues(query));

        // Provinces
        query = "SELECT " +
        " `Name` As ID, CONCAT(`Name`,' [',`CountryCode`,']') As Value, `Name` " +
        " FROM `GeoState` " +
        " ORDER BY `Name`" +
        "";
        r.put("provinces", loadConstantValues(query));

        /*// Cities
        query = "SELECT " +
        " `Name` As ID, `Name` AS Value " +
        " FROM `GeoCity` " +
        " ORDER BY `Name`" +
        "";
        r.put("cities", loadConstantValues(query));*/

        constants = r;

        buildMaps();
        return true;
    }
    // loadConstants()


    /**
     * @return the list of valid fields on which formating can apply.
     */
    public List<String> getFormatFields()
    {
        return formatFields;
    }


    boolean loadFormatFields()
    {
        List<String> fieldSet = new ArrayList<String>();
        String query = "SELECT DISTINCT TRIM(SUBSTRING_INDEX(d.Description,';',1)) AS `FormatField` "+
                        " FROM DropdownDetails d, DropdownDetails m " +
                        " WHERE d.DropdownMasterID = m.DropdownDetailID AND d.Active = true AND m.Active = true " +
                        " AND m.DropdownMasterID = ? " +
                        " ORDER BY `FormatField` " +
                        "";
        String formatDataCategoryID = "2";

        List<Map<String, String>> formatFieldList = querytool.getList(query, formatDataCategoryID);

        if (formatFieldList != null)
        {
            Map<String,String> m = null;
            String field = null;
            for (int index = 0; index < formatFieldList.size(); index++)
            {
                m = formatFieldList.get(index);
                field = m.get("formatfield").toString();
                if (field != null && field.length()>0)
                {
                    for (String f : StringUtil.splitToArray(field))
                    {
                        fieldSet.add(f.toLowerCase());
                    }
                }
            }
        }
        else
        {   // Default List
            fieldSet.add("author");
            fieldSet.add("contribution");
            fieldSet.add("journal");
            fieldSet.add("title");
        }

        formatFields = fieldSet;

        return true;
    }


    /**
     * @return the list of valid fields on which formating can apply.
     */
    public LinkedHashMap<Integer, LinkedHashMap<String, LinkedHashMap<String, String>>> getFormatFieldsMap()
    {
        return formatFieldsMap;
    }

    boolean loadFormatFieldsMap()
    {
        List<Map<String, String>> formatdatacategory = constants.get("formatdatacategory");
        if (formatdatacategory != null)
        {
            this.formatFieldsMap = new LinkedHashMap<Integer, LinkedHashMap<String, LinkedHashMap<String, String>>>();
            int dataType = -1;
            for (Map<String, String> m : formatdatacategory)
            {
                dataType = Integer.parseInt(m.get("id").toString());
                formatFieldsMap.put(dataType, setFieldMap(dataType));
            }
        }
        return true;
    }

    private static final String GET_FORMATTING_FIELDS = "SELECT Description "+
                                                        " FROM DropdownDetails" +
                                                        " WHERE DropdownMasterID = ? AND Active = true" +
                                                        " ORDER BY DisplayOrder" +
                                                        "";

    /**
     * Initialize the map of field names. The order set here is maintained when
     * the descriptions are displayed on the JSP page.
     */
    private LinkedHashMap<String, LinkedHashMap<String, String>> setFieldMap(int dataType)
    {
        // Initialize the field Map
        LinkedHashMap<String, LinkedHashMap<String, String>> fieldMap = null;

        List<Map<String,String>> fields = querytool.getList(GET_FORMATTING_FIELDS, String.valueOf(dataType));

        if (fields != null)
        {
            fieldMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
            for (Map<String, String> m : fields)
            {
                String field = m.get("description").toString();

                LinkedHashMap<String, String> attrMap = new LinkedHashMap<String, String>();
                String[] attributes = field.split(";");
                if (attributes != null && attributes.length == 4)
                {
                    attrMap.put(FieldFormatData.FIELD_DESCRIPTION, attributes[1]);
                    // ALLOW_ENTIRE_FIELD indicates whether or not the user will
                    // be allowed to select a format for this entire field.
                    // A value of true will indicate that this field will be
                    // available for selection in the JSP page.
                    attrMap.put(FieldFormatData.ALLOW_ENTIRE_FIELD, attributes[2]);
                    // ALLOW_PATTERN indicates whether or not the user will be
                    // allowed to select a format pattern for this field.
                    // A value of true will indicate that this field will be
                    // available for selection in the JSP page.
                    attrMap.put(FieldFormatData.ALLOW_PATTERN, attributes[3]);

                    // Store attributes for the field - first value is the database field name
                    fieldMap.put(attributes[0], attrMap);
                }
                else
                {
                    log.info("Invalid field format attributes defined in database!");
                }
            }
        }
        else
        {
            log.info("Unable to locate format field property in database!");
        }
        return fieldMap;
    }


    /**
     * Load a set of constants from the query passed.
     * @param query
     * @return The constant values as a List of name&lt;--&gt;value Maps
     */
    private List<Map<String,String>> loadConstantValues(final String query)
    {
        List<Map<String, String>> options;
        Map<String, Map<String,String>> recordlist;

        recordlist = querytool.runQuery(false, query, "ID");
        options = new ArrayList<Map<String, String>>(recordlist.size());

        for (final Map.Entry<String,Map<String,String>> entry : recordlist.entrySet())
        {
            options.add(entry.getValue());
        }

        return options;
    }


    // TODO: move 'lookupNames()' out of MIVConfig

    private static final String LOOKUP_USER_SQL_V4 =
        "SELECT DISTINCT" +
        "        ua.UserID, Login, Surname, GivenName, Email," +
        "        s.Description as SchoolName," +
        "        d.Description as DepartmentName," +
        "        ua.Active," +
        "        r.ShortDescription as Role" +
        " FROM UserAccount ua" +
        "    LEFT JOIN RoleAssignment ra" +
        "      ON ra.UserID=ua.UserID" +
        "    LEFT JOIN MivRole r" +
        "      ON ra.RoleID=r.ID" +
        "    LEFT JOIN SystemDepartment d" +
        "      ON ua.DepartmentID=d.DepartmentID" +
        "     AND ua.SchoolID=d.SchoolID" +
        "    LEFT JOIN SystemSchool s" +
        "      ON ua.SchoolID=s.SchoolID" +
        " WHERE (" +
        "    ua.UserID = ? OR " +
        "    Login LIKE ? OR" +
        "    PreferredName LIKE ? OR" +
        "    GivenName LIKE ? OR" +
        "    Surname LIKE ? OR" +
        "    Email LIKE ?" +
        ")" +
        " AND ra.PrimaryRole=true" +
        " ORDER BY Surname ASC" +
        "";

    private static final String LOOKUP_USER_SQL = LOOKUP_USER_SQL_V4;

    public List<Map<String,String>> lookupNames(final String match)
    {
        final QueryTool qt = this.querytool;
        // When matching user id we only want exact matches, but for first/last name
        // or email address we'll take things that start with the text entered.
        final String match2 = /*"%" + */match + "%";

        final List<Map<String,String>> recordlist = qt.getList(LOOKUP_USER_SQL, match, match2, match2, match2, match2, match2);

        if (isDebug()) {
            System.out.println("MIV: MIVConfig.lookupNames found "+recordlist.size()+" records.");
            System.out.println(" List: ["+recordlist+"]");
        }

        return recordlist;
    }


    Properties loadProperties(final String propertyFilename)
    {
        // Loading properties via getResourceAsStream never re-reads an edited file.
        // This means the MIV Options > Reload Options button doesn't work anymore.
        // Reverting this to the old method of opening the file ourself.
        //return loadProperties(propertyFilename, MIVConfig.class.getResourceAsStream(propertyFilename));
        if (this.fileroot == null) {
            System.err.println("Can't load properties file; context not yet set");
            return null;
        }

        InputStream in = null;
        final File propertiesFile = new File(fileroot, propertyFilename);
        Properties p;
        try {
            in = new FileInputStream(propertiesFile);
            p = this.loadProperties(propertyFilename, in);
            try { in.close(); } catch (IOException e) { /* ignore the IOE caused by closing an already closed stream. */ }
        }
        catch (final FileNotFoundException fnfe) {
            System.err.println("Configuration file not found: " + propertiesFile);
            fnfe.printStackTrace(System.err);
            return null;
        }

        return p;
    }

    Properties loadProperties(final String propertyFilename, InputStream in)
    {

        final Properties p = new Properties();

        try
        {
            p.load(in);
            in.close();
        }
        catch (final IOException ioe)
        {
            System.err.println("Couldn't load configuration file: " + propertyFilename);
            ioe.printStackTrace(System.err);
            return null;
        }

        if (this.fDebug) {
            System.out.println("MIV: MIVConfig.loadProperties: Loaded Properties from " + propertyFilename + " :: [["+p+"]]");
        }

        return p;
    }

    boolean loadProperties()
    {
        MIVConfig.configProperties = loadProperties(MIV_CONFIG_FILE);
        return MIVConfig.configProperties != null;
    }


    boolean loadFormatterProperties()
    {
        MIVConfig.formatterConfigProperties = loadProperties(MIV_FORMATTER_CONFIG_FILE);
        return MIVConfig.formatterConfigProperties != null;
    }


    boolean loadExternalProperties()
    {
        String externalConfigLocation = this.getProperty("server-config-location");
        if (externalConfigLocation != null)
        {
            try
            {
                MIVConfig.externalProperties = loadProperties(externalConfigLocation, new FileInputStream(externalConfigLocation));
                String trackerCode = externalProperties.getProperty("tracker.code");
                System.out.println(df.format(Calendar.getInstance().getTime()) +
                                   " - - - - - --->  loadExternalProperties found a tracker code of [" + trackerCode + "] : Setting it.");
                this.trackerCode = trackerCode;
            }
            catch (FileNotFoundException e)
            {
                throw new MivSevereApplicationError("Failed to load External Properties - MIV must be shut down", e);
            }
        }
        return MIVConfig.externalProperties != null;
    }


    /**
     * Re-reads application configuration information.
     * Drops all application configuration parameters and reloads them from the configuration
     * file(s) and the database. This allows changing properties, wording, sort-order, and so on
     * without needing to restart the application.
     */
    public void reloadConfig()
    {
        System.out.println("MIV: MIVConfig RELOADING CONFIG");
        log.info("RELOADING CONFIG PROPERTIES");
        // Do we want to initContextVars again?
        // If we do, don't call loadProperties() again because initContextVars calls it.

        reload(ConfigReloadConstants.ALL);
    }

    public void reload()
    {
        this.reloadConfig();
    } // alias for reloadConfig()

    /**
     * to reload properties by ConfigReloadConstants
     * @param reloadConstants
     */
    public void reload(ConfigReloadConstants... reloadConstants)
    {
        for (ConfigReloadConstants configReloadConstants : reloadConstants)
        {
            reloadOptions(configReloadConstants);
        }
    }

    /**
     * to reload properties by ConfigReloadConstants
     * @param reloadConstant
     */
    private void reloadOptions(ConfigReloadConstants reloadConstant)
    {
        switch (reloadConstant)
        {
            case ALL:
                loadProperties();
                loadFormatterProperties();
                loadExternalProperties();
                loadConstants();
                loadFormatFields();
                loadFormatFieldsMap();
                MivServiceLocator.getUserService().refresh();
                MivServiceLocator.getGroupService().refresh();
                break;
            case PROPERTIES:
                loadProperties();
                break;
            case FORMATTER_PROPERTIES:
                loadFormatterProperties();
                break;
            case FORMAT_FIELDS:
                loadFormatFields();
                loadFormatFieldsMap();
                break;
            case CONSTANTS:
                loadConstants();
                break;
            case USER_SERVICE:
                MivServiceLocator.getUserService().refresh();
                break;
            case GROUP_SERVICE:
                MivServiceLocator.getGroupService().refresh();
                break;
            default:
                break;
        }
        // TODO: This could be finer grained now, using the passed "reloadConstant" to fire a more specific change event.
        change.firePropertyChange(CONFIG_PROPERTY_NAME, "oldConfig", "newConfig");
        EventDispatcher.getDispatcher().post(new ConfigurationChangeEvent("0", reloadConstant));
    }


    /*
     * Property Change Support - delegate everything to the PropertyChangeSupport object.
     */
    /* deprecated Register with the {@link edu.ucdavis.mw.myinfovault.events.EventDispatcher EventDispatcher} */
    /**
     * @param listener
     * @deprecated Register with the {@link edu.ucdavis.mw.myinfovault.events.EventDispatcher EventDispatcher}
     * instead and subscribe to {@link edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent ConfigurationChangeEvents}
     */
    @Deprecated
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        System.out.println("MIVConfig adding property change listener ["+listener+"]");
        this.change.addPropertyChangeListener(listener);
    }

    /**
     * @param propertyName
     * @param listener
     * @deprecated Register with the {@link edu.ucdavis.mw.myinfovault.events.EventDispatcher EventDispatcher}
     * instead and subscribe to {@link edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent ConfigurationChangeEvents}
     */
    @Deprecated
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        this.change.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * @param listener property change listener to remove for this bean
     */
    public void removePropertyChangeListern(PropertyChangeListener listener) {
        this.change.removePropertyChangeListener(listener);
    }

    /**
     * @param propertyName name of the property with the given listener
     * @param listener property change listener to remove for the given property
     */
    public void removePropertyChangeListern(String propertyName, PropertyChangeListener listener) {
        this.change.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * @param propertyName
     * @return
     */
    public boolean hasListeners(String propertyName) {
        return this.change.hasListeners(propertyName);
    }

    /**
     * @return
     */
    public PropertyChangeListener[] getPropertyChangeListeners() {
        return this.change.getPropertyChangeListeners();
    }

    /**
     * @param propertyName
     * @return
     */
    public PropertyChangeListener[] getPropertyChangeListeners(String propertyName) {
        return this.change.getPropertyChangeListeners(propertyName);
    }

    /*
     * End of Property Change Support
     */



    public boolean getDoTimings()
    {
        return this.gatherTimings;
    }

    public void setDoTimings(final boolean fGatherTimings)
    {
        this.gatherTimings = fGatherTimings;
    }



    private String findHostName()
    {
        String hostname = "unknnown";
        try
        {
            InetAddress addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        }
        catch (UnknownHostException e)
        {
            // allow hostname to remain "unknown"
        }

        return hostname;
    }


    /*========================================*/
    /* ***** Accessor / Mutator methods ***** */
    /*========================================*/

    /**
     * Get the directory of the filesystem where the root of the application lives.
     */
    public File getApplicationRoot()
    {
        return fileroot;
    }


    public String getDocumentUrlRoot()
    {
        return this.getWebApplicationPath() + "/documents";
    }

    /**
     * Returns a {@link java.io.File} object representing the work directory for a given user.
     * This does not imply the directory exists! The caller should test <code>exists()</code>
     * and create the directory or report an error as appropriate.
     *
     * @param userId the MIV user ID number
     * @return A java.io.File object representing the user's directory
     */
    public File getUserDir(final int userId)
    {
        final File f = new File(this.getProperty("document-config-baselocation"), "u" + userId);
        return f;
    }


    /**
     * Returns a {@link java.io.File} object representing the dossier PDF directory for a given user.
     * This does not imply the directory exists! The caller should test <code>exists()</code>
     * and create the directory or report an error as appropriate.
     *
     * @param userId the MIV user ID number
     * @return A java.io.File object representing the user's dossier PDF directory
     */
    public File getDossierPdfLocation(final int userId)
    {
        final Properties documentProperties = PropertyManager.getPropertySet("document", "config");
        String relativeDossierLocation = documentProperties.getProperty("location-dossier");
        if (relativeDossierLocation == null)
        {
            relativeDossierLocation = "dossier";
        }
        final File userDirectory = new File(this.getUserDir(userId),relativeDossierLocation);
        // Output directory is for PDF file extension
        final File outputDirectory = new File(userDirectory,DocumentFormat.PDF.getFileExtension());
        return outputDirectory;
    }

    public void setWebApplicationPath(final String contextPath)
    {
        if (this.fDebug) System.out.println(" --#--#--#--#-- Setting web application path to ["+contextPath+"]");
        this.webroot = contextPath;
    }

    public String getWebApplicationPath()
    {
        return this.webroot;
    }


    public void setServer(final String webserver)
    {
        if (this.fDebug) System.out.println(" --#--#--#--#-- Setting server to ["+webserver+"]");
        this.webserver = webserver;
    }

    public String getServer()
    {
        if (this.fDebug)
        {
            System.out.print("   --#--#--#--#--#--#--#-- ");
            System.out.println("Host name is " + findHostName() + " --#-- getServer() is returning " + this.webserver);
        }
        return this.webserver;
    }

    public static boolean isInitialized()
    {
        return initComplete;
    }


    public QueryTool getQueryTool()
    {
        return this.querytool;
    }



    @Override
    protected void finalize() throws Throwable
    {
        try {
            System.out.println("MIV: Releasing MIVConfig Object at finalization");
//            this.release();
            this.dataSource = null;
            this.activePoolConnections = 0;
        }
        finally {
            super.finalize();
        }
    }



/** Top level map to hold the named maps that can be requested via <code>getMap()</code> */
    private final Map<String, Map<String, Map<String, String>>> mastermap = new HashMap<String, Map<String, Map<String, String>>>();

    /**
     * "which" can be "schools", "departments", "documents", "documentsbyattributename", "attributetypesbyname", attributetypesbyid; add others as needed
     * <p>The available subsets are:<ul>
     * <li>schools</li>
     * <li>departments</li>
     * <li>documents</li>
     * <li>documentsbyattributename</li>
     * <li>attributetypesbyname</li>
     * <li>attributetypesbyid</li>
     * <br><li>mivrolesbymivcode</li>
     * <li>routingsequencebylocationname</li>
     * <li>routingsequence</li>
     * <li>jurisdiction</li>
     * <li>patentstatus</li>
     * <li>sectionidbyname</li>
     * <li>sectionbyid</li>
     * <li>tablebyrectype</li>
     * <li>countrycodes</li>
     * </ul></p>
     * @param which
     * @return the requested map, or an empty map if
     */
    public Map<String, Map<String, String>> getMap(final String which)
    {
        return mastermap.get(which);
    }

    /**<p>
     * Build the information maps that parts of MIV can use.
     * buildMaps must be called <strong><em>AFTER</em></strong> loadConstants.
     * This is only called by <code>loadConstants()</code> and it should stay that way.</p>
     *
     * <br><br>
     *
     *
     *
     * <p><strong>
     *      If you ADD ANY MORE MAPS to this you MUST add those to the Javadoc
     *      in {@link #getMap(String)} above!!!
     * </strong></p>
     *
     *
     *
     */
    private final void buildMaps()
    {
        assert constants != null : "constants have not been built yet!";

        // Create "schools" map
        final List<Map<String, String>> schools = constants.get("schoolname");
        final Map<String, Map<String, String>> schoolMap = new HashMap<String, Map<String, String>>();
        for (final Map<String, String> school : schools)
        {
            final String schoolKey = school.get("schoolid");
            schoolMap.put(schoolKey, school);
        }
        mastermap.put("schools", schoolMap);


        // Create "departments" map
        final Map<String, Map<String, String>> departmentMap = new HashMap<String, Map<String, String>>();
        final List<Map<String, String>> departments = constants.get("departmentname");
        for (final Map<String, String> dept : departments)
        {
            final String deptKey = dept.get("schoolid") + ":" + dept.get("departmentid");
            departmentMap.put(deptKey, dept);
        }
        mastermap.put("departments", departmentMap);


        // Create "documents" map
        final Map<String, Map<String, String>> documentMap = new HashMap<String, Map<String, String>>();
        final List<Map<String, String>> documents = constants.get("documents");
        for (final Map<String, String> document : documents)
        {
            final String documentKey = document.get("id");
            documentMap.put(documentKey, document);
        }
        mastermap.put("documents", documentMap);


        // Create "documentsbyattributename" map
        final Map<String, Map<String, String>> documentByAttributeNameMap = new HashMap<String, Map<String, String>>();
        for (final Map<String, String> document : documents)
        {
            final String documentKey = document.get("attributename");
            documentByAttributeNameMap.put(documentKey, document);
        }
        mastermap.put("documentsbyattributename", documentByAttributeNameMap);


        // Create "attributetypesbyname" map
        final Map<String, Map<String, String>> attributeTypeMapByName = new LinkedHashMap<String, Map<String, String>>();
        final List<Map<String, String>> attributetypes = constants.get("attributetypes");
        for (final Map<String, String> attributetype : attributetypes)
        {
            final String attributeTypeKey = attributetype.get("name");
            attributeTypeMapByName.put(attributeTypeKey, attributetype);
        }
        mastermap.put("attributetypesbyname", attributeTypeMapByName);


        // Create "attributetypesbyid" map
        final Map<String, Map<String, String>> attributeTypeMapById = new LinkedHashMap<String, Map<String, String>>();
        for (final Map<String, String> attributetype : attributetypes)
        {
            final String attributeTypeKey = attributetype.get("id");
            attributeTypeMapById.put(attributeTypeKey, attributetype);
        }
        mastermap.put("attributetypesbyid", attributeTypeMapById);


        // Create "mivrolesbymivcode" map
        final Map<String, Map<String, String>> mivRolesMap = new LinkedHashMap<String, Map<String, String>>();
        final List<Map<String, String>> mivRoles = constants.get("mivroles");
        for (final Map<String, String> mivRole : mivRoles)
        {
            final String mivRoleKey = mivRole.get("mivcode");
            mivRolesMap.put(mivRoleKey, mivRole);
        }
        mastermap.put("mivrolesbymivcode", mivRolesMap);


        // Create "routingsequencebylocationname" and "routingsequence" maps
        final Map<String, Map<String, String>> routingSequenceByLocationNameMap = new LinkedHashMap<String, Map<String, String>>();
        final Map<String, Map<String, String>> routingSequenceMap = new LinkedHashMap<String, Map<String, String>>();
        final List<Map<String, String>> routingSequences = constants.get("routingsequence");

        for (final Map<String, String> routingSequence : routingSequences)
        {
            final String key = routingSequence.get("delegationauthority") + ":" + routingSequence.get("actiontype");
            if (!routingSequenceByLocationNameMap.containsKey(key))
            {
                routingSequenceByLocationNameMap.put(key, new LinkedHashMap<String, String>());
                routingSequenceMap.put(key, new LinkedHashMap<String, String>());
            }
            routingSequenceByLocationNameMap.get(key).put(routingSequence.get("workflownodename"), routingSequence.get("routingsequence"));
            routingSequenceMap.get(key).put(routingSequence.get("routingsequence"), routingSequence.get("workflownodename"));
        }
        mastermap.put("routingsequencebylocationname", routingSequenceByLocationNameMap);
        mastermap.put("routingsequence", routingSequenceMap);


        // Create patent "jurisdiction" map
        final Map<String, Map<String, String>> jurisdictionMap = new LinkedHashMap<String, Map<String, String>>();
        final List<Map<String, String>> jurisdictions = constants.get("jurisdiction");
        for (final Map<String, String> jurisdiction : jurisdictions)
        {
            final String jurisdictionKey = jurisdiction.get("id");
            jurisdictionMap.put(jurisdictionKey, jurisdiction);
        }
        mastermap.put("jurisdiction", jurisdictionMap);


        // Create "patentstatus" map
        final Map<String, Map<String, String>> patentstatusMap = new LinkedHashMap<String, Map<String, String>>();
        final List<Map<String, String>> patentstatus = constants.get("patentstatus");
        for (final Map<String, String> status : patentstatus)
        {
            final String statusKey = status.get("id");
            patentstatusMap.put(statusKey, status);
        }
        mastermap.put("patentstatus", patentstatusMap);


        // Section ID map - get Section information by the Section Name
        final Map<String, Map<String, String>> sectionidbyname = new LinkedHashMap<String, Map<String, String>>();
        final List<Map<String, String>> sections = constants.get("sections");
        for (final Map<String, String> section : sections)
        {
            final String name = section.get("name");
            sectionidbyname.put(name, section);
        }
        // Add the special case for re-sequenceable dossier documents (uploads)
        final Map<String, String> documentUploadSection = new HashMap<String, String>(5);
        documentUploadSection.put("name", "dossier-document");
        documentUploadSection.put("recordname", "dossier-document");
        documentUploadSection.put("sectionbasetable", "UserUploadDocument");
        documentUploadSection.put("sectionprimarykeyid", "ID");
        documentUploadSection.put("fromtable", "UserUploadDocument");
        sectionidbyname.put("dossier-document", documentUploadSection);

        mastermap.put("sectionidbyname", sectionidbyname);


        final Map<String, Map<String, String>> sectionbyid = new LinkedHashMap<>();
        final Map<String, Map<String, String>> tableByRectype = new LinkedHashMap<>();
        for (final Map<String, String> section : sections)
        {
            final String id = section.get("id");
            sectionbyid.put(id, section);
            final String rectype = section.get("recordname").replace("-record", "");
            tableByRectype.put(rectype, section);
        }
        mastermap.put("sectionbyid", sectionbyid);
        mastermap.put("tablebyrectype", tableByRectype);


        final Map<String, Map<String, String>> countrycodes = new LinkedHashMap<String, Map<String, String>>();
        final List<Map<String, String>> countries = constants.get("countries");
        for (final Map<String, String> country : countries)
        {
            final String id = country.get("id");
            countrycodes.put(id, country);
        }
        mastermap.put("countrycodes", countrycodes);


        /*
         *
         *
         *      If you ADD ANY MORE MAPS to this you MUST add those to the Javadoc
         *      in {@link #getMap(String)} above!!!
         *
         *
         */

    }
}
