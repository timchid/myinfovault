/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVContextLoader.java
 */

package edu.ucdavis.myinfovault;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import edu.ucdavis.mw.myinfovault.events2.EventListener;
import edu.ucdavis.mw.myinfovault.web.MivActivityMonitor;

/**
 * Ensures the MIVConfig object is loaded as soon as a ServletContext is ready for it to use.
 * This is registered as a listener in the web.xml file.
 * @author Stephen Paulsen
 * @since MIV 2.2
 */
public class MIVContextLoader implements ServletContextListener
{
    /**
     * Load the MyInfoVault configuration ({@link MIVConfig}) as soon as the application context has been created.
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(ServletContextEvent sce)
    {
        System.out.println("MIV: Servlet context initialized, loading MIVConfig.");
        MIVConfig.getConfig(sce.getServletContext());
    }


    /* (non-Javadoc)
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce)
    {
        EventListener evListener = MIVConfig.getConfig().getEventListener();
        if (evListener != null && evListener.getMivThreadPoolExecutor() != null)
        {
            evListener.getMivThreadPoolExecutor().shutDown();
        }

        MivActivityMonitor monitor = MivActivityMonitor.getMonitor();
        if (monitor != null) monitor.shutdown();
    }
}
