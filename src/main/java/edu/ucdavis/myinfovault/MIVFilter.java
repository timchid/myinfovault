/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVFilter.java
 */

package edu.ucdavis.myinfovault;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import javax.ws.rs.core.MediaType;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.MivActivityMonitor;


/**
 * <p>Checks that a CAS-authenticated kerberos login is an authorized MIV user.</p>
 * <p>Performs a database lookup of the "remote user" and sets the login name and user ID number
 * in the HttpSession if found. Redirects to a URL if no matching ID was found in the database.
 * The database to use is configurable via a Java Naming lookup string. The query to use for
 * the lookup is configurable but is expected to accept one String argument (the login name)
 * and return a user ID number (Integer).
 * </p>
 * <p>Initialization Parameters (<code>&lt;init-param&gt;</code>) accepted are:</p><dl>
 * <dt>LookupPath</dt> <dd>JDBC Naming string</dd>
 * <dt>DatabaseQuery</dt> <dd>SQL Query to execute to verify user / retrieve user information</dd>
 * <dt>InvalidLoginURL</dt> <dd>URL to redirect to if the database query returns an "invalid user" indication</dd>
 * <dt>LogPath</dt> <dd>Location (directory) to write log file(s) in</dd>
 * <dt>LogFile</dt> <dd>Name of the log file</dd>
 * <dt>NoFilterPattern</dt> <dd>Requests that match this regular expression will be immediately passed through this filter</dd>
 * <dt>BrowserCompatibility</dt> <dd>Set the X-UA-Compatible header to the parameter value if present. Used primarily for IE8/IE7 compatibility</dd>
 * <dt>debug</dt> <dd><code>true</code> or <code>false</code> to turn debugging on or off respectively</dd>
 * <dt>AllowedUsers</dt> <dd>List of users allowed to login. Leave blank for normal operation; provide list to run in Restricted Mode</dd>
 * <dt>MaintenanceModeURL</dt> <dd>URL to redirect to if the user is not allowed to login when Restricted Mode is in force</dd>
 * </dl>
 * @author Patrick Proctor
 * @author Lawrence Fyfe
 * @author Stephen Paulsen
 * @since MIV 2.2
 */
public class MIVFilter implements Filter
{
    private UserService userService = null;

    private ServletContext context;
    private String contextPath;

//    private String lookupPath;
    private String databaseQuery;
    private String logPath;
    private String logFile;
    private String invalidLoginURL;
    private String browserCompatibility;
    private Set<String> allowedUsers = null; // for Restricted mode, which permits only the listed users to login.

    private String maintenanceModeUrl;

    private boolean debug = false;

    private final Logger logger = Logger.getLogger(MIVFilter.class.getName());
    private String unfiltered = ".*/(?:js|css|images|help)/.*"; // Overridden by the "NoFilterPattern" init-parameter if present

    private MivActivityMonitor am;

    @Override
    public void init(final FilterConfig config)
    {
        this.context = config.getServletContext();
        this.contextPath = context.getContextPath();
        System.out.println("Context Path has been set to [" + this.contextPath + "]");

        this.userService = MivServiceLocator.getUserService();

        // Read the filter params
        final String debugParam = config.getInitParameter("debug");
        debug = Boolean.parseBoolean(debugParam);

        final String applicationServer = MIVConfig.getConfig().getExternalProperties().getProperty("application.server");


        final String nofiltering = config.getInitParameter("NoFilterPattern");
        if (nofiltering != null && nofiltering.length() > 0) {
            unfiltered = nofiltering;
        }

        // User lookup in the database
//        lookupPath = config.getInitParameter("LookupPath");
        databaseQuery = config.getInitParameter("DatabaseQuery");
        invalidLoginURL = "//" + applicationServer + "/unauthorized";


        // Check for and set up Restricted Mode allowing only the listed users to login
        String allowed = config.getInitParameter("AllowedUsers");
        if (allowed != null && allowed.trim().length() > 0)
        {
            allowed = allowed.trim();
            String[] allowedList = allowed.split("\\s*[,;:]\\s*");
            allowedUsers = new HashSet<String>(Arrays.asList(allowedList));
            if (allowedUsers.size() <= 0) {
                // The param value was blank, set it back to null to treat it like it wasn't set at all.
                allowedUsers = null;
            }
        }
        maintenanceModeUrl = "//" + applicationServer + "/maintenance";

        // Set the IE compatibility mode if specified
        browserCompatibility = config.getInitParameter("BrowserCompatibility");
        // If it was set but empty, set it back to null
        if (browserCompatibility != null && browserCompatibility.trim().length() == 0) {
            browserCompatibility = null;
        }

        // Set up logging
        logPath = config.getInitParameter("LogPath");
        logFile = config.getInitParameter("LogFile");

        try {
            Handler h = null;
            h = new FileHandler(new File(logPath, logFile).toString(), /*append=*/true);
            h.setFormatter(new SimpleFormatter());
            logger.addHandler(h);
            logger.setUseParentHandlers(false);
        }
        catch (final SecurityException | IOException e) {
            // Ignore this -- use the standard Handler if our FileHandler failed.
            e.printStackTrace();
        }

        am = MivActivityMonitor.getMonitor();
    }


    /** When logging accesses, ignore requests that match the exclusions pattern */
    private static final String exclusions = ".*\\.(?:gif|jpe?g|png|ico|css|js)\\Z";
    volatile boolean dumpRequest = false;

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
        throws IOException, ServletException
    {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;


        // Task 1: Block old Internet Explorer versions.
        String userAgent = req.getHeader("User-Agent");

        // Currently rejects IE7 exactly, but should do IE <= 7
        if (userAgent.contains("MSIE 7.0")) {
            if (!userAgent.contains("Trident/")) {
                res.sendRedirect("http://" + req.getServerName());
                return;
            }
        }


        HttpSession hs = req.getSession(false);
        final String path = req.getRequestURI();

        // Task 2: Always allow access to any /status[/*] path regardless of session, or login, or user.
        /*
          This could be moved to its own filter, to handle the /miv/status/* path(s)
          It would have to appear in web.xml **Before** this MIVFilter so it would have precedence.

          Map it to handle only the status path:
          <filter-mapping>
              <filter-name>MIV Status Filter</filter-name>
              <url-pattern>/status</url-pattern>
          </filter-mapping>

         */
        if (path.startsWith(contextPath + "/status"))
        {
            System.out.println("getting status on path " + path);
            handleStatus(path, res);
            return;
        }


        // Skip filtering of resources such as images, stylesheets, etc. so
        // we don't waste cycles on the remaining tasks.  ("unfiltered")
        // Always go directly to the invalid-login URL with no filtering.
        // Doing otherwise can lead to infinite recursion when there's a
        // failure accessing the error page itself.
        if (path.matches(unfiltered) || invalidLoginURL.contains(path))
        {
            chain.doFilter(request, response);
            return;
        }


        // Task 3: Show that we're doing maintenance unless user is one of those authorized.
        if (debug) System.out.println("Checking maintenanceMode, which is " + MIVConfig.getConfig().isMaintenanceMode());
        if (MIVConfig.getConfig().isMaintenanceMode())  // No caching this, we need to check every time in case it has changed.
        {
            String user = req.getRemoteUser();
            // There may not be a remote-user if you have valid CAS and JSESSIONID cookies
            // from just prior to a restart.
            // If there's no remote-user but there *is* a login saved in the HttpSession
            // we'll use that for maintenance-mode access checking.
            if (user == null && hs != null) {
                Object u = hs.getAttribute("loginName");
                if (u != null) user = u.toString();
            }

            // If user is (still) null or not an MIV user then userID will be -1 ...
            final int userID = lookupUser(user);
            // ... and person will be null.
            MivPerson person = userService.getPersonByMivId(userID);

            System.out.println(new Date().toString() + "\tIn Maintenance Mode, checking if user is allowed... "
                             + "remote user: " + user
                             + ", user ID: " + userID
                             + ", person: " + person
                          );

            // Reject a non-miv-user (-1) right out.  Otherwise reject them if they are
            // not a SYS_ADMIN and also not on the allowed-list.
            if (
                    userID == -1 || (
                        ! person.hasRole(MivRole.SYS_ADMIN) &&
                        ! (this.allowedUsers != null && this.allowedUsers.contains( Integer.toString(userID) ))
                    )
               )
            {
                System.out.println(" -> Maintenance Mode rejecting this person from " + req.getRemoteAddr());
                res.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
                res.sendRedirect(maintenanceModeUrl);
                return;
            }

            System.out.println(" -> Maintenance Mode allowing this person to enter.");
        }


        // If the HttpSession is null is this a first contact, or did the session expire?
        // What do we want to do?  Immediately redirect to MIVLogin?  Not if the session just expired.
        if (hs == null)
        {
            hs = req.getSession(true);
        }
        else
        {
            //System.out.println("session valid? " + )
        }


        final String loginName = (String) hs.getAttribute("loginName");
        String user = req.getRemoteUser();

        if (dumpRequest) {
            dumpRequestInfo(req);
        }

        // Show the requested path - but skip the noise of graphics, css, js etc. files.
        final String msg = "request for [" + path + "] from user [" + user + "] loginName [" + loginName + "]";
        if (debug) { System.out.println(msg); }

        // Add headers on requests that aren't on the list of exclusions.
        // This will catch and add headers to HTML but skip graphics, css, js, etc.
        if (path != null && !path.matches(exclusions))
        {
            logger.finer(msg);

            if (browserCompatibility != null) {
                res.addHeader("X-UA-Compatible", browserCompatibility); // put IE versions into the mode specified
            }

            // Introduced in IE 8 RC1 2009/01/26, in FF since 3.6.9, and formalized in an Internet Draft
            //    http://tools.ietf.org/html/draft-gondrom-frame-options-02
            // the X-Frame-Options defends against some types of "click-jacking", XSS, and CSRF
            // by preventing pages from including your pages within frames & iframes.
            // Use the "SAMEORIGIN" setting to allow *ourselves* to use iframes.
            // Added here 2012-04-18 SDP
            res.addHeader("X-Frame-Options", "SAMEORIGIN"); // Can be "DENY", "SAMEORIGIN", or "ALLOW-FROM" - we probably want "SAMEORIGIN"
        }


        final String mivID = (String) hs.getAttribute("userID");
        if (loginName != null) // maybe when loginName is not-null and remote-user *IS* null?
        {
            //dumpRequestInfo(req);

            if (mivID != null)
            {
            	continueFiltering(req, res, chain);
                return;
            }
            // else what?  What if the name is set but not the ID?
        }
        // else what?
        // -----------***** What if the loginName is not set and the user (getRemoteUser) is also not set? *****


        // If we already have a login name set in the session and it matches the authenticated user name,
        // we accept that as validated and don't have to lookup the user in the database. Chain to the next filter.
        if (loginName != null && user != null) // This should never be true since the "loginName"/"userID" test was put in above.
        {
            user = user.toLowerCase();
            if (loginName.equals(user))
            {
                continueFiltering(req, res, chain);
                return;
            }
        }


        // Query the UserLogin table here
        final int userID = lookupUser(user);

        if (userID < 0)
        {
            logger.warning("No local login was found for \"" + user + "\" while requesting path [" + path + "]");
            res.sendRedirect(invalidLoginURL);
            return;
        }

        hs.setAttribute("userID", ""+userID);
        hs.setAttribute("loginName", user);

        continueFiltering(req, res, chain);
    }


    private void continueFiltering(HttpServletRequest request, HttpServletResponse response, final FilterChain chain)
            throws IOException, ServletException
    {
        final String queryString = request.getQueryString();
        am.logActivity(request.getRemoteUser(), request.getRequestURI() + (queryString != null ? "?" + queryString : ""));
        chain.doFilter(request, response);
    }


    private int lookupUser(final String user)
    {
        int userID = -1;
        DataSource source = null;
        Connection connection = null;
        PreparedStatement statement = null;

        if (MIVConfig.isInitialized())
        {
            source = MIVConfig.getConfig().getDataSource();
        }

        if (source == null)
        {
            System.out.println("*** Unable to initialize dataSource ***");
            return userID;
        }

        try {

            connection = source.getConnection();
            statement = connection.prepareStatement(databaseQuery);
            statement.setString(1, user);
            final ResultSet rs = statement.executeQuery();
            if (rs.next())
            {
                //userID = rs.getInt(1);
                userID = rs.getInt("UserID");
            }
        }
        catch (final SQLException e) { // getConnection
            // Ignore the exception, allowing userID to remain -1
        }
        finally
        {
            if (statement != null) try { statement.close(); } catch (final SQLException e) {}
            if (connection != null) try { connection.close(); } catch (final SQLException e) {}
        }

        return userID;
    }


    /**
     * Handle requests for /miv/status[/*]
     * @param res
     * @throws IOException
     */
    private void handleStatus(final String path, final HttpServletResponse res) throws IOException
    {
        final String statusPath = contextPath + "/status/";

        res.setHeader( "X-Alive", "Alive!" );
        res.setHeader( "Connection", "close" );
        res.setContentType( MediaType.TEXT_PLAIN );
        res.setCharacterEncoding( StandardCharsets.UTF_8.name() );

        if (! path.startsWith(statusPath + "echo/")) {
            res.getWriter().write("alive");
        }

        if (path.startsWith(statusPath))
        {
            String colon = " : ";
            String remainder = path.replace(statusPath, "");

            if (remainder.startsWith("echo/")) {
                remainder = remainder.replaceFirst("\\Aecho/", ""); // further strip it if it's /miv/status/echo
                colon = "";
            }

            if (remainder.trim().length() > 0) {
                res.getWriter().write(colon + remainder);
            }
        }

        res.flushBuffer();
    }


    /**
     * @param req
     */
    private void dumpRequestInfo(final HttpServletRequest req)
    {
            System.out.print("Processing a " + req.getMethod() + " request");
            System.out.println(" on getRequestURI: " + req.getRequestURI());

            System.out.println("getRequestURL [" + req.getRequestURL() + "]");
            System.out.println("getPathInfo [" + req.getPathInfo() + "]");
            System.out.println("getQueryString [" + req.getQueryString() + "]");
            System.out.println("getRequestedSessionId [" + req.getRequestedSessionId() + "]");

            System.out.println("\n Headers:");
            Enumeration<?> e = req.getHeaderNames();
            while (e.hasMoreElements())
            {
                final Object header = e.nextElement();
                final String value = req.getHeader(header.toString());
                System.out.println(header.toString() + " == " + value);
            }

            System.out.println("\n Attributes:");
            e = req.getAttributeNames();
            while (e.hasMoreElements())
            {
                final Object attribute = e.nextElement();
                final Object value = req.getAttribute(attribute.toString());
                System.out.println(attribute.toString() + " == " + value);
            }

            System.out.println("\n Parameters:");
            e = req.getParameterNames();
            while (e.hasMoreElements())
            {
                final Object pname = e.nextElement();
                final String[] pvals = req.getParameterValues(pname.toString());
                System.out.print(pname + " == ");
                for (final String val : pvals) {
                    System.out.print("[" + val + "], ");
                }
                System.out.println();
            }
    }


    /** Required for interface, but nothing to do here. */
    @Override
    public void destroy()
    {
    }
}
