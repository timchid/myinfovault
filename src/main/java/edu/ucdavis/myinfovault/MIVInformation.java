/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVInformation.java
 */

package edu.ucdavis.myinfovault;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * MIV project and build information singleton.
 *
 * @author Craig Gilmore
 * @since MIV 2.0
 */
public enum MIVInformation
{
    /**
     * Singleton instance of MIVInformation.
     */
    INSTANCE;

    // manifest path from application root
    private static final String MANIFEST_PATH = "META-INF/MANIFEST.MF";

    // manifest property keys
    private static final String BUILD_AUTHOR = "Built-By";
    private static final String BUILD_DATE = "Build-Date";
    private static final String BUILD_REVISION = "Build-Revision";
    private static final String BUILD_COMMIT = "Build-Commit";
    private static final String BUILD_MANAGER = "Created-By";
    private static final String MIV_VERSION = "Implementation-Version";
    private static final String JDK_VERSION = "Build-Jdk";

    // harvested manifest values
    private String buildAuthor;
    private String buildDate;
    private String buildRevision;
    private String buildCommit;
    private String buildManager;
    private String mivVersion;
    private String jdkVersion;

    /*
     * Creates MIV information singleton.
     */
    private MIVInformation()
    {
        Exception exception = null;

        try
        {
            // get manifest input stream from application root
            InputStream in = new FileInputStream(
                                 new File(MIVConfig.getConfig().getApplicationRoot(), MANIFEST_PATH));

            // load manifest properties
            Properties manifest = new Properties();
            manifest.load(in);
            in.close();

            // harvest build and project information from manifest properties
            buildAuthor = manifest.getProperty(BUILD_AUTHOR, StringUtil.EMPTY_STRING);
            buildDate = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm:ss z").format(new Date(Long.parseLong(manifest.getProperty(BUILD_DATE, "0"))));

            /*
             * The buildRevision and buildCommit are the abbreviated and full
             * Git commit hash for this build, respectively. This could be useful
             * in determining the commit from which the build was done.
             */
            buildRevision = manifest.getProperty(BUILD_REVISION, StringUtil.EMPTY_STRING);
            buildCommit = manifest.getProperty(BUILD_COMMIT, StringUtil.EMPTY_STRING);
            buildManager = manifest.getProperty(BUILD_MANAGER, StringUtil.EMPTY_STRING);
            mivVersion = manifest.getProperty(MIV_VERSION, StringUtil.EMPTY_STRING);
            jdkVersion = manifest.getProperty(JDK_VERSION, StringUtil.EMPTY_STRING);

        }
        catch (FileNotFoundException e) {
            exception = e;
        } catch (IOException e) {
            exception = e;
        } catch (NumberFormatException e) {
            exception = e;
        } finally {
            if (exception != null)
            {
                throw new MivSevereApplicationError("Could not load properties from webapp manifest", exception);
            }
        }
    }

    /**
     * @return MIV person initiating the build
     */
    public String getBuildAuthor()
    {
        return buildAuthor;
    }

    /**
     * @return MIV web application build date
     */
    public String getBuildDate()
    {
        return buildDate;
    }

    /**
     * @return Git abbreviated commit hash for the current build
     */
    public String getBuildRevision()
    {
        return buildRevision;
    }

    /**
     * @return Git commit hash for the current build
     */
    public String getBuildCommit()
    {
        return buildCommit;
    }

    /**
     * @return project management tool used for the build
     */
    public String getBuildManager()
    {
        return buildManager;
    }

    /**
     * @return MIV version of the this build
     */
    public String getMivVersion()
    {
        return mivVersion;
    }

    /**
     * @return version of the JDK used for the current build
     */
    public String getJdkVersion()
    {
        return jdkVersion;
    }
}
