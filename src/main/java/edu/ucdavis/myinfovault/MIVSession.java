/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVSession.java
 */

package edu.ucdavis.myinfovault;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * A MyInfoVault session.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class MIVSession
{
    private static Logger logger = LoggerFactory.getLogger(MIVSession.class);

    private static final String SESSION_ATTRIBUTE_KEY = "MIVSESSION";
    private static final String LOGIN_ATTRIBUTE_KEY = "loginName";
    private static final String USERID_ATTRIBUTE_KEY = "userID";
    private static final String CREATE_SESSION_LOG = "No MIVSession for {}; creating one and saving it in the HttpSession ({})";
    private static final String LOGIN_TIME_SQL = "UPDATE UserLogin SET LoginTimestamp=current_timestamp WHERE UserID=? AND Login=?";

    private final HttpSession httpSession;
    private final MIVUser loginUser;

    /*
     * Create a an MIV session
     */
    private MIVSession(HttpServletRequest req)
    {
        httpSession = req.getSession(false);

        MIVConfig.getConfig().setWebApplicationPath(req.getContextPath());
        MIVConfig.getConfig().setServer("http://" + req.getServerName() + "/");

        // create an instance of the logged in MIV user for this session
        loginUser = new MIVUser((String) httpSession.getAttribute(LOGIN_ATTRIBUTE_KEY),
                                (String) httpSession.getAttribute(USERID_ATTRIBUTE_KEY));

        // Set the time this user last logged in
        MIVConfig.getConfig().getQueryTool().runQuery(LOGIN_TIME_SQL, null,
                                                      Integer.toString(loginUser.getUserId()),
                                                      loginUser.getLoginName());
    }


    /**
     * Get the MIV session associated with the given HTTP servlet request.
     *
     * @param req Request associated with the session
     * @return MIV session for the given request or <code>null</code> if session is null
     */
    public static MIVSession getSession(HttpServletRequest req)
    {
        return getSession(req, false);
    }


    /**
     * Get the MIV session associated with the given HTTP servlet request and create
     * it if it doesn't not exist and the 'create' flag is <code>true</code>.
     *
     * @param req Request associated with the session
     * @param create Flag to create new MIV session if DNE
     * @return MIV session for the given request or <code>null</code> if session is null
     */
    public static MIVSession getSession(HttpServletRequest req, boolean create)
    {
        MIVSession ms = null;

        try
        {
            /*
             * Get the HTTP session and then get the MIV session from
             * it; Create an MIVSession and store it if none exists yet
             */
            HttpSession hs = req.getSession(false);

            // session must not be null
            if (hs == null) throw new IllegalStateException("HttpSession is null in MIVSession");

            // retrieve the MIV session from the HTTP session
            ms = (MIVSession) hs.getAttribute(SESSION_ATTRIBUTE_KEY);

            // create a new MIV session if it doesn't exist and the create flag is set
            if (ms == null && create)
            {
                // create new session for this request
                ms = new MIVSession(req);

                // log session creation
                logger.info(CREATE_SESSION_LOG,
                            hs.getAttribute(LOGIN_ATTRIBUTE_KEY),
                            hs.getClass().getName());

                // add MIV session object to HTTP session
                hs.setAttribute(SESSION_ATTRIBUTE_KEY, ms);
            }

            // MIV session is still null
            if (ms == null) throw new IllegalStateException();
        }
        catch (IllegalStateException e)
        {
            // a null MIV session will be returned!
            logger.warn("Returning a null MIVSession from getSession(request," + create + ")", create ? e : (Throwable)null);
        }

        return ms;
    }


    public static MIVSession getSession(HttpSession hs)
    {
        return (MIVSession) hs.getAttribute(SESSION_ATTRIBUTE_KEY);
    }


    /**
     * Get the MIV session associated with the given JSP context.
     *
     * @param context JSP servlet context
     * @return MIV session for the given context or <code>null</code> if DNE
     */
    public static MIVSession getSession(JspContext context)
    {
        return (MIVSession) context.findAttribute(SESSION_ATTRIBUTE_KEY);
    }


    /**
     * @return the HTTP session associated with this MIV session
     */
    public HttpSession getHttpSession()
    {
        return httpSession;
    }


    /**
     * Fetch the Application Configuration object. This allows a web page to get access
     * to MIv configuration information and to set the modifiable configuration items.
     *
     * @return the Application Configuration singleton.
     */
    public MIVConfig getConfig()
    {
        return MIVConfig.getConfig();
    }


    /**
     * Get the MIV application information object.
     *
     * @return the MIVInformation object singleton.
     */
    public MIVInformation getInformation()
    {
        return MIVInformation.INSTANCE;
    }


    /**
     * Get the MIV user object for the user logged in to this session.
     *
     * @return the logged-in user's MIVUser object.
     */
    public MIVUser getUser()
    {
        return loginUser;
    }


    /**
     * Does the logged-in person have any of the top MIV administrator roles?
     * — used only by disclosuremail.jsp
     * @return if logged-in person is an MIV administrator
     */
    public boolean isAdmin()
    {
        return loginUser.getUserInfo().getPerson().hasRole(VICE_PROVOST_STAFF, SYS_ADMIN);
    }


    /**
     * @return If the HTTP session exists for this MIV session
     */
    public boolean hasSession()
    {
        return httpSession != null;
    }



    /*
     * Most Recently Used (MRU) list of people that you have proxied.
     */
    private static final int MRU_SIZE = 5; // This could be configurable, or better, a user preference.
    LinkedHashMap<Integer,MivPerson> mru = new LinkedHashMap<Integer,MivPerson>(MRU_SIZE, 1.0f, true) {
        private static final long serialVersionUID = 201410271636L;
        @Override
        protected boolean removeEldestEntry(Map.Entry<Integer,MivPerson> eldest) {
            logger.info("oldest entry in MRU: {} — {}", eldest.getValue(), this.size() > MRU_SIZE ? "Dropping" : "Keeping");
            return size() > MRU_SIZE;
        }
    };
    List<MivPerson> pickList = new ArrayList<>(MRU_SIZE);
    public void updateMRU(final MivPerson target)
    {
        if (target.getUserId() == this.loginUser.getUserId()) return; // never put the logged-in person in the MRU list.

        logger.info("updateMRU({})", target);
        if (mru.containsKey(target.getUserId())) {
            logger.info("{} is already in MRU", target.getUserId());
            //mru.remove(target.getUserId()); // Removing then replacing moves a re-used person "up" in the MRU LIFO
            // ^^ .remove is not necessary by setting accessOrder "true" specified in constructor.
        }

        mru.put(target.getUserId(), target);
        pickList.clear();
        pickList.addAll(mru.values());
        // Reverse so the most recent people are at top
        Collections.reverse(pickList);
        // (or Sort alphabetically)
        /*Collections.sort(pickList, new Comparator<MivPerson>() {
            @Override
            public int compare(final MivPerson o1, final MivPerson o2) {
                return o1.getDisplayName().compareTo(o2.getDisplayName());
            }
        });*/
    }
    public List<MivPerson> getMru()
    {
        return pickList;
    }
}
