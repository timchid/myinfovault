/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVUserInfo.java
 */

package edu.ucdavis.myinfovault;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.data.DataMap;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;
import edu.ucdavis.myinfovault.data.FieldFormatData;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.QueryTool;
import edu.ucdavis.myinfovault.data.RecordHandler;
import edu.ucdavis.myinfovault.data.SectionFetcher;
import edu.ucdavis.myinfovault.data.UCDLdap;
import edu.ucdavis.myinfovault.format.HighlightPattern;

/**
 * Maintain information about a user, primarily retrieved from the MIV database.
 * Compare to {@link MIVUser} which is the logged-in user object.
 * This is the information for a particular user, where MIVUser represents the
 * currently active (logged in) user.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class MIVUserInfo
{
    private static Logger log = LoggerFactory.getLogger(MIVUserInfo.class);

    private static final int INVALID_PARENT_DOCUMENT_TYPEID = -1;

    private static UserService userService = MivServiceLocator.getUserService();

    private final MivPerson person;
    private String login;
    private MIVUser owner;
    private String fullname;
    private String employeeID;

    private List<HighlightPattern> patterns = new LinkedList<HighlightPattern>();
    private SectionFetcher sectionFetcher = null;
    private RecordHandler recordHandler = null;
    private Map<Integer,FieldFormatData> fieldFormatDataMap =  new HashMap<Integer,FieldFormatData>();

    /**
     * Create a user info instance for the currently logged-in user.
     *
     * @param owner The logged in MIVUser that is controlling the new MIVUserInfo.
     */
    public MIVUserInfo(MIVUser owner)
    {
        this(owner, owner.getUserId());
    }

    /**
     * <p>Create a user info instance for a particular user.
     * A logged in user ({@link MIVUser}) is in control of any given MIVUserInfo object.
     * Each logged in user should have their own MIVUserInfo object (see {@link #MIVUserInfo(MIVUser)}),
     * but may also have user information for a different user, for whom they may be acting.</p>
     *
     * @param owner The logged in MIVUser that is controlling the new MIVUserInfo.
     * @param userid The MIV User ID number of the user to store info about.
     * @throws IllegalArgumentException if owner is <code>null</code>
     */
    public MIVUserInfo(MIVUser owner, int userid) throws IllegalArgumentException
    {
        this(userid, owner);

        if (owner == null)
        {
            throw new IllegalArgumentException("The owner of a UserInfo object may not be null");
        }
    }

    /**
     * <p>Create a user info instance with no owner for a particular user.
     * A logged in user ({@link MIVUser}) is normally in control of any given MIVUserInfo object.
     * This case is provided when you need only the info for a userid, without necessarily
     * having a controlling logged in user.<p>
     *
     * <p><strong>NOTE:</strong> calls to getLoginUser() will return <code>null</code> in this case.</p>
     *
     * @param userid The MIV User ID number of the user to store info about.
     */
    public MIVUserInfo(int userid)
    {
        this(userid, null);
    }

    /**
     * Private constructor used to implement the public constructors.
     *
     * @param userid The MIV User ID number of the user to store info about.
     * @param owner The logged in MIVUser that is controlling the new MIVUserInfo.
     * @throws IllegalArgumentException if owner is <code>null</code>
     */
    private MIVUserInfo(int userid, MIVUser owner) throws IllegalArgumentException
    {
        if (userid <= 0)
        {
            throw new IllegalArgumentException("The User ID must be greater-than zero");
        }

        this.owner = owner;

        // set the MIV person associated with this user information
        person = userService.getPersonByMivId(userid);

        // load person details and persist
        /*
         * TODO: remove this if it isn't needed anymore if (person.loadDetail())
         * { userService.savePerson(person, owner.getUserId()); }
         */

        init();
    }

    /**
     * @return the MIV person associated with this user information
     */
    public MivPerson getPerson()
    {
        return person;
    }

    /**
     * The logged-in user may be working on someone else's data, so we can't
     * assume things like getFullName() returns the name of the current user.
     *
     * @return The logged in MIVUser object, or <code>null</code> if this has no controlling user.
     */
    public MIVUser getLoginUser()
    {
        return this.owner;
    }

    /**
     * @return Kerberos login ID
     */
    public String getLogin()
    {
        return this.login;
    }

    /**
     * @return the user's work directory.
     * @see MIVConfig#getUserDir(int)
     */
    public File getUserDir()
    {
        return MIVConfig.getConfig().getUserDir(person.getUserId());
    }

    /**
     * @return user's given name
     * @deprecated use {@link #getPerson()} to access the given name instead
     */
    @Deprecated
    public String getFirstName()
    {
        return person.getGivenName();
    }

    /**
     * @return user's surname
     * @deprecated use {@link #getPerson()} to access the surname instead
     */
    @Deprecated
    public String getLastName()
    {
        return person.getSurname();
    }

    /**
     * @return user's middle name
     * @deprecated use {@link #getPerson()} to access the middle instead
     */
    @Deprecated
    public String getMiddleName()
    {
        return person.getMiddleName();
    }

    /**
     * @return the user's full name for display
     */
    public String getFullName()
    {
        /*
         * NOTE: Don't escape the name from this call.
         * Change all callers of this to use getDisplayName()
         * instead if they need the escaped name.
         */
        return this.fullname;
    }

    /**
     * @return the user's full name for display, escaped in a manner safe for HTML and XML
     */
    public String getDisplayName()
    {
        return MIVUtil.escapeMarkup(this.fullname);
    }

    /**
     * @return employee ID
     */
    public String getEmployeeID()
    {
        return this.employeeID;
    }

    /**
     * @return preferred email address
     * @deprecated use {@link #getPerson()} to access the preferred email instead
     */
    @Deprecated
    public String getEmail()
    {
        return person.getPreferredEmail();
    }

    /**
     * @return the primary appointment department ID
     * @deprecated use a Person object to get schools and departments.
     */
    @Deprecated
    public int getDeptID()
    {
        return person.getPrimaryAppointment().getDepartment();
    }

    /**
     * @return the primary appointment school ID
     * @deprecated Use a Person object to get schools and departments.
     */
    @Deprecated
    public int getSchoolID()
    {
        return person.getPrimaryAppointment().getSchool();
    }

    /**
     * @return highlight patterns for this user
     */
    public List<HighlightPattern> getHighlightPatterns()
    {
        return this.patterns;
    }

    /**
     * @return record handler for this user
     */
    public RecordHandler getRecordHandler()
    {
        assert this.recordHandler != null : "RecordHandler for user " + getPerson().getUserId() + " is null!";
        return this.recordHandler;
    }

    /**
     * @return section fetcher for this user
     */
    public SectionFetcher getSectionFetcher()
    {
        assert this.sectionFetcher != null : "SectionFetcher for user " + getPerson().getUserId() + " is null!";
        return this.sectionFetcher;
    }

    /**
     * @param dataType data type key
     * @return field format data for this user
     */
    public FieldFormatData getFieldFormatData(int dataType)
    {
        assert this.fieldFormatDataMap != null : "FieldFormatDataMap for user " + getPerson().getUserId() + " is null!";
        assert this.fieldFormatDataMap.get(dataType) != null : "FieldFormatDataMap for dataType " + dataType + " is null!";

        return this.fieldFormatDataMap.get(dataType);
    }

    /**
     * <p>Free the resources used by this MIVUserInfo instance.
     * Once an MIVUserInfo object is released the caller should immediately drop any references to it,
     * since it will no longer be valid.</p>
     *
     * <p>It is safe to call release on the same object more than once.</p>
     */
    public void release()
    {
        if (recordHandler != null)
        {
            this.recordHandler = null;
        }

        if (sectionFetcher != null)
        {
            this.sectionFetcher = null;
        }

        if (fieldFormatDataMap != null)
        {
            this.fieldFormatDataMap = null;
        }
    }


    private static final String personalQuery =
        "SELECT UserID, Login, GivenName, Surname, MiddleName, PreferredName," +
        " SchoolID, DepartmentID, Email as EmailAddress" +
        " FROM UserAccount" +
        " WHERE UserID = ?" +
        "";

    private static final String identifierQuery =
        "SELECT UserID, Value as EmployeeID" +
        " FROM UserIdentifier" +
        " WHERE UserID = ? AND IdentifierID=1" +
        "";

    /**
     * New 'init()' for new UserFormatPattern table
     */
    private boolean init()
    {
        boolean success = false;
        boolean debug = log.isDebugEnabled();

        try {
            QueryTool qt = MIVConfig.getConfig().getQueryTool();

            qt.setNote("Created in MIVUserInfo.init() at " + java.text.DateFormat.getTimeInstance().format(new java.util.Date()) +
                       " for user " + person.getUserId() + ": closed and nulled at end of method");

            String userIdString = Integer.toString(person.getUserId());

            Map<String,Map<String,String>> records = qt.runQuery(false, personalQuery, "UserID", userIdString);

//            if (debug) System.out.println(" !!! The new MIVUserInfo init() found "+records.size()+" records: ["+records+"]");

            // records.size() == 0 means we found nothing. This is probably because no UserPersonal record has been created.
            success = records.size() > 0; // This is how we define success -- For Now.
            Map<String,Map<String,String>> identifierRecords = qt.runQuery(false, identifierQuery, "UserID", userIdString);

            if (success)
            {
                loadPatterns(); // NOTE: the patterns have to be loaded BEFORE the SectionFetcher,
                                //       which loads the FormatManager

                log.trace("Creating a SectionFetcher for {}", person.getUserId());
                this.sectionFetcher = new SectionFetcher(this);
                log.trace("Creating a RecordHandler for {}", person.getUserId());
                this.recordHandler = new RecordHandler(this);
            }

            Map<String,String> rec = records != null
                                   ? records.get(userIdString)
                                   : new HashMap<String,String>();

            Map<String,String> idRecord = identifierRecords != null
                                        ? identifierRecords.get(userIdString)
                                        : new HashMap<String,String>();

            if (debug) System.out.println("MIVUserInfo initialization, loaded personal information record: ["+rec+"]");

            this.login = rec.get("login");

            if (debug) System.out.println("found firstname: [" + person.getGivenName() + "]");

            // Use the display name from the UserPersonal table if present,
            // otherwise combine first and last name from CvMem.
            String displayName = rec.get("preferredname");
            if (displayName != null && displayName.length() > 0)
            {
                this.fullname = displayName;
            }
            else
            {
                this.fullname = person.getGivenName() + " " + person.getSurname();
            }

            if (idRecord != null)
            {
                this.employeeID = idRecord.get("employeeid");
            }

            if(this.employeeID == null || this.employeeID.trim().length() == 0)
            {
                this.employeeID = fetchEmployeeId(this.login);
            }

            if (this.employeeID != null && this.employeeID.trim().length() == 0)
            {
                this.employeeID = null;  // make sure it's null if it's not a valid ID
            }

            // getting the list of all Manage Format Options data types
            List<Map<String, String>> formatDataCategoryList = MIVConfig.getConfig().getConstants().get("formatdatacategory");

            if (formatDataCategoryList != null)
            {
                for (Map<String, String> m : formatDataCategoryList)
                {
                    if (m.get("id") != null)
                    {
                        this.fieldFormatDataMap.put(
                                Integer.parseInt(m.get("id").toString()),
                                new FieldFormatData(this, Integer.parseInt(m.get("id").toString()))); // dataType = 6 for now publications
                    }
                }
            }
            else
            {
                this.fieldFormatDataMap.put(6, new FieldFormatData(this, 6)); // dataType = 6 for now publications
            }

            if (debug) System.out.println("MIVUserInfo.init() returning success=="+success);
        }
        catch (Exception e)
        {
            log.warn("Something unexpected happened (" + e.getMessage() + ") in MIVUserInfo.init for User " + person.getUserId(), e);
            success = false;
        }

        return success;
    }

    /**
     * To get field pattern map by docIdentifier.
     *
     * @param docIdentifier
     * @return DataMap with field pattern data
     */
    public DataMap getUserFieldPatternMap(DocumentIdentifier docIdentifier)
    {
        // Get the fieldPatternMap
        DataMap fieldPatternMap = null;

        if (this.fieldFormatDataMap.get(docIdentifier.getId()) != null)
        {
            fieldPatternMap = this.fieldFormatDataMap.get(docIdentifier.getId()).getFieldPatternMap();
        }
        else if (docIdentifier.getParentDocumentId() != INVALID_PARENT_DOCUMENT_TYPEID)
        {
            fieldPatternMap = this.fieldFormatDataMap.get(docIdentifier.getParentDocumentId()).getFieldPatternMap();
        }

        return fieldPatternMap;
    }


    // The query must sort the patterns by FieldName and the length of the pattern
    // in order have the longest patterns first. We match the longest patterns for
    // a field first. Patterns which are a substrings of the longer pattern will
    // not be matched and are not supported.
    private static final String patternQuery =
            " SELECT ID, DocumentID, FieldName, ifNull(Pattern,'') AS Pattern, Bold, Italic, Underline, Display" +
            " FROM UserFormatPattern" +
            " WHERE UserID = ? AND" +
            " Display = true " +
            " ORDER BY FieldName, char_length(pattern) DESC" +
            "";

    /**
     * TODO: This should be tied in with the fieldFormatData because they are both
     * performing (essentially or exactly) the same query. We also want to reload
     * the patterns used for preview highlighting when the fieldFormatData is saved
     * or reloaded so they are always in sync and the previews get the new formatting
     * as soon as it's updated. As it is now, you have to logout and back in to get
     * the updated patterns.
     */
    public void loadPatterns()
    {
        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        String userIdString = Integer.toString(person.getUserId());
        Map<String,Map<String,String>> records = qt.runQuery(false, patternQuery, "ID", userIdString);

        //MIV-862 Empty the existing pattern list and reload it from database
        //patterns = new LinkedList<HighlightPattern>();
        patterns.clear();

        for (String recid : records.keySet())
        {
            Map<String,String> rec = records.get(recid);
            String field = rec.get("fieldname");
            int docId = Integer.parseInt(rec.get("documentid"));
            HighlightPattern p = null;
            if ("all".equalsIgnoreCase(field))
            {
                // create 4 patterns, one for each supported field
                Iterator<String> iter = MIVConfig.getConfig().getFormatFields().iterator();
                while (iter.hasNext())
                {
                    for (String f : StringUtil.splitToArray(iter.next()))
                    {
                        p = new HighlightPattern(
                                 recid,
                                 docId,
                                 f,
                                 MIVUtil.escapeMarkup(rec.get("pattern")), // Remove any markup in the pattern MIV-1260
                                 rec.get("bold"),
                                 rec.get("italic"),
                                 rec.get("underline")
                             );
                         this.patterns.add(p);
                    }
                }
            }
            else
            {
                for (String f : StringUtil.splitToArray(field))
                {
                    p = new HighlightPattern(
                              recid,
                              docId,
                              f,
                              MIVUtil.escapeMarkup(rec.get("pattern")),      // Remove any markup in the pattern MIV-1260
                              rec.get("bold"),
                              rec.get("italic"),
                              rec.get("underline")
                          );
                    this.patterns.add(p);
                }
            }
        }
        if (this.sectionFetcher != null) {
            this.sectionFetcher.getFormatManager().reload();
        }
    }


    /**
     * Get the user's Employee ID (if present) from LDAP. Used internally when the Employee ID
     * is not found in the UserIdentifier table.
     * @param loginName Kerberos login (uid) to lookup.
     * @return the user's Employee ID as a String; null or an empty string if it wasn't found.
     */
    private String fetchEmployeeId(String loginName)
    {
        String result = null;
        try {
            UCDLdap dir = new UCDLdap();
            result = dir.getEmpId(loginName);

            if (result != null && result.trim().length() > 0)
            {
                result = result.trim();
                log.info("Fetched Employee ID \"" + result + "\" from LDAP for user \"" + loginName + "\" MIV ID=" + person.getUserId());
                RecordHandler rh = this.getRecordHandler();
                if (rh != null)
                {
                    String[] fields = { "IdentifierID", "Value", "Display" };
                    String[] values = { "1", result, "1" };
                    rh.insert("UserIdentifier", fields, values);
                }
                else
                {
                    log.warn("Record Handler is null, Unable to insert Employee ID for user {} ({})", person.getUserId(), loginName);
                }
            }
            else
            {
                log.warn("No Employee ID number found in LDAP while searching for \"{}\" MIV ID={}",loginName, person.getUserId());
            }
        }
        catch (NullPointerException e) {
            log.error("NullPointerException (" + e.getMessage() + ") caught in MIVUserInfo.fetchEmployeeId for User " + person.getUserId(), e);
        }
        catch (Throwable e) {
            log.error("Something unexpected happened (" + e.getMessage() + ") in MIVUserInfo.fetchEmployeeId for User " + person.getUserId(), e);
        }
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return this.getFullName() + " / " + getPerson().getUserId();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable
    {
        try {
            log.debug("Releasing MIVUserInfo for user {} during finalization.", person.getUserId());
            this.release();
        }
        finally {
            super.finalize();
        }
    }
}
