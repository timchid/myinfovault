/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivEntity.java
 */

package edu.ucdavis.myinfovault;


/**<p>
 * Represents the Entities in MIV.</p>
 * <p><strong>Note: enum AuditAction should match with EntityType Table</strong></p>
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public enum MivEntity
{
    DEFAULT   (100, "Default"),
    USER      (101, "User"),
    DOSSIER   (102, "Dossier");

    private int entityId;
    private String description;

    private MivEntity(int entityId, String description)
    {
        this.entityId = entityId;
        this.description = description;
    }


    /**
     * @return the entityId
     */
    public int getEntityId()
    {
        return entityId;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Returns the enum corresponding to the entityId
     * @param entityId
     * @return MivEntity enum
     */
    public static MivEntity getMivEntityById(int entityId)
    {
        MivEntity[] enumlist = MivEntity.values();
        for (MivEntity mivEntity : enumlist)
        {
            if (mivEntity.getEntityId() == entityId) { return mivEntity; }
        }

        return MivEntity.DEFAULT;
    }
}
