/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivMailMessage.java
 */

package edu.ucdavis.myinfovault;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import edu.ucdavis.myinfovault.data.MIVUtil;

/**
 * Simple email message class for UC Davis. Always sends through smtp.ucdavis.edu.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class MivMailMessage extends MimeMessage
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String MIME_TYPE = "text/plain; charset=utf-8";
    private static final String RFC_SEPARATOR = ",";
    private static final String MS_SEPARATOR_RE = "[ ]*;[ ]*";
    private static final String LOG_PATTERN = "SENDING MAIL MESSAGE...\n\tSubject: {0}\n\tFrom: {1}\n\tSender: {2}\n\tTo: {3}\n\tCc: {4}";

    private static Properties props = System.getProperties();
    {
        // The mail.smtp.host should be read from properties
        // or picked up from ${java.home}/lib/javamail.X file
        // or from the META-INF/javamail.X file
        props.put("mail.smtp.host", "smtp.ucdavis.edu");
    }

    /**
     * Creates an email with empty to, from, subject.
     */
    public MivMailMessage()
    {
        super(Session.getDefaultInstance(props, null));
    }

    /**
     * Creates an email to and from the given addresses.
     *
     * @param to Address to which messages will be sent
     * @param from Address from which messages will be sent
     * @param subject The subject of the message
     * @throws MessagingException
     * @throws AddressException
     */
    public MivMailMessage(String to,
                          String from,
                          String subject) throws AddressException, MessagingException
    {
        this();

        setTo(to);
        setFrom(from);
        setSubject(subject);
    }

    /**
     * Get the comma delimited list of 'To' recipient addresses.
     *
     * @return List of 'To' addresses
     * @throws MessagingException If the 'To' header couldn't be found
     */
    public String getTo() throws MessagingException
    {
        return getAddresses(RecipientType.TO);
    }

    /**
     * Get the comma delimited list of 'Cc' recipient addresses.
     *
     * @return List of 'Cc' addresses
     * @throws MessagingException If the 'Cc' header couldn't be found
     */
    public String getCc() throws MessagingException
    {
        return getAddresses(RecipientType.CC);
    }

    /**
     * Set the 'To' address.
     *
     * @param to The address(es) to which messages will be sent
     * @throws MessagingException If the address is invalid
     */
    public void setTo(String to) throws MessagingException
    {
        setAddresses(RecipientType.TO, to);
    }

    /**
     * Set the 'From' address.
     *
     * @param from The address from which messages appears to be sent
     * @throws AddressException
     * @throws MessagingException If the address is invalid
     */
    public void setFrom(String from) throws MessagingException
    {
        setFrom(new InternetAddress(from));
    }

    /**
     * Set the 'Sender' address.
     *
     * @param sender The real address from which messages are sent
     * @throws AddressException
     * @throws MessagingException If the address is invalid
     */
    protected void setSender(String sender) throws MessagingException
    {
        setSender(new InternetAddress(sender));
    }

    /**
     * Set the 'Cc' address.
     *
     * @param cc The address(es) to which messages will be sent
     * @throws MessagingException If the address is invalid
     */
    public void setCc(String cc) throws MessagingException
    {
        setAddresses(RecipientType.CC, cc);
    }

    /**
     * Setup email with given to, from, and subject values and send.
     *
     * @param message Message body to for email to send
     * @return Date mail sent
     * @throws MessagingException
     * @throws IllegalStateException if the message is being sent before the addresses and/or subject have been set.
     * @since MIV 2.2
     */
    public Date send(String message) throws MessagingException
    {
        //log email sending
        logger.info(MessageFormat.format(LOG_PATTERN,
                                         getSubject(),
                                         StringUtils.arrayToDelimitedString(getFrom(), RFC_SEPARATOR),
                                         getSender(),
                                         getTo(),
                                         getCc()));

        // content must be UTF-8
        setContent(MIVUtil.escapeMarkup(message), MIME_TYPE);

        // send mail
        Transport.send(this);

        //return the date sent
        return getSentDate() != null ? getSentDate() : new Date();
    }

    /*
     * Replace Microsoft-style semicolon address separators with RFC standard commas. Thanks, Microsoft.
     */
    protected static String standardizeAddress(String address)
    {
        return StringUtils.hasText(address) ? address.replaceAll(MS_SEPARATOR_RE, RFC_SEPARATOR) : null;
    }

    /*
     * Set addresses for the recipient type
     */
    private void setAddresses(javax.mail.Message.RecipientType type, String addresses) throws MessagingException
    {
        setRecipients(type, InternetAddress.parse(standardizeAddress(addresses), true));
    }

    /*
     * Get addresses for the recipient type
     */
    private String getAddresses(Message.RecipientType type) throws MessagingException
    {
        return StringUtils.arrayToDelimitedString(getRecipients(type), RFC_SEPARATOR);
    }
}
