/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivWebFilter.java
 */

package edu.ucdavis.myinfovault;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * TODO: Java Doc.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.8
 */
public class MivWebFilter extends BaseFilter
{
    private static final String WARNING_CONTENT = "<div id=\"non-prod-warning\">"
                + " <p>WARNING: This is a non-production system used for MIV testing/training</p>"
                + " <p>This system may become unavailable at any time.</p>"
                + " </div>";



    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig config) throws ServletException
    {
        String debugSetting = config.getInitParameter(DEBUG_PARAM);
        // Keep debug as true if it was set to true in the source code, else set it according to the init-param.
        this.debug = this.debug || (debugSetting != null && (debugSetting.equalsIgnoreCase("true") || debugSetting.equalsIgnoreCase("on")));
    }


    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException
    {
        // Only handle HttpServletRequests - not any other kind
        if (! (request instanceof HttpServletRequest)) {
            chain.doFilter(request, response);
            return;
        }

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if (req.getMethod().equalsIgnoreCase("GET") || req.getMethod().equalsIgnoreCase("POST"))
        {
            response.setCharacterEncoding("utf-8"); // very important
            CharResponseWrapper wrapper = new CharResponseWrapper(res);

            chain.doFilter(request, wrapper);

            injectNonProductionWarning(req, res, wrapper);

            return;
        }

        // Pass anything else along
        chain.doFilter(request, response);
        return;
    }


    /**
     *
     * @param request
     * @param response
     * @param wrapper
     *
     * @throws IOException
     */
    private void injectNonProductionWarning(HttpServletRequest request, HttpServletResponse response, CharResponseWrapper wrapper)
            throws IOException
    {
        // Can't affect the response if it's already committed
        if (response.isCommitted()) return;

        PrintWriter out = null;
//        ServletOutputStream outS = null;
        try {
            out = response.getWriter();
        }
        catch (IllegalStateException e) {
            // Response is not committed, but illegal ==> OutputStream was used, not Writer
//            outS = response.getOutputStream();
            // skip the whole thing and return
            // TODO: Enhance the CharResponseWrapper and this code to deal with things
            //       that use getOutputStream() instead of only supporting getWriter()
            return;
        }

        String responseMimeType = wrapper.getContentType();
        String content = wrapper.toString();
        final boolean isProduction = MIVConfig.getConfig().isProductionServer();

        // Redirects etc. don't have a mime type; Images, text/plain, etc. don't have forms. Skip everything that's not html.
        if (responseMimeType != null && htmlMimeTypePattern.matcher(responseMimeType).find())
        {
            // Inject warning message only for non production systems
            if (!isProduction)
            {
                content = WebPatterns.BODY.getPattern().matcher(content).replaceFirst("$1 " + WARNING_CONTENT);
            }

            String htmlClass = isProduction ? "prod" : "non-prod";

            MIVSession mivSession = MIVSession.getSession(request);
            if (mivSession != null)
            {
                MIVUserInfo mui = mivSession.getUser().getTargetUserInfo();
                htmlClass = "target" + mui.getPerson().getPrimaryRoleType().getName() + " " + htmlClass;
                mui = mivSession.getUser().getUserInfo();
                htmlClass = mui.getPerson().getPrimaryRoleType().getName() + " " + htmlClass;
            }

            Matcher htmlClassMatcher = WebPatterns.HTML_CLASS.getPattern().matcher(content);

            if (htmlClassMatcher.find())
            {
                content = htmlClassMatcher.replaceFirst("$1" + htmlClass + " ");
            }
            else
            {
                content = WebPatterns.HTML_PART.getPattern().matcher(content).replaceFirst("$1 class=\"" + htmlClass + "\" $4");
            }
        }

        // If we somehow got here with no output Writer available we can't do anything.
        // Bail out now.
        // NOTE - this shouldn't happen, because catching the IllegalStateException (above) should prevent us getting here in this state.
        if (out == null) return;

        // If we created some new content, output that; otherwise output whatever came back from the filter chain
        out.write(content);
        out.close();
    }

}
