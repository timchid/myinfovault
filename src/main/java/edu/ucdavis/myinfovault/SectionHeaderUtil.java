/*
 * Copyright © 2007-2010 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SectionHeaderUtil.java
 */

package edu.ucdavis.myinfovault;

import java.util.Map;
import java.util.Set;

import edu.ucdavis.myinfovault.data.QueryTool;

/**
 * FIXME: Needs Javadoc
 *
 * @author Brij Garg
 * @since MIV 2.0
 */
public class SectionHeaderUtil
{
    private static final String GET_USER_SECTION_HEADER =
        "SELECT Header" +
        " FROM UserSectionHeader" +
        " WHERE SectionID = ? AND UserID = ?" +
        "";

    private static final String INSERT_USER_SECTION_HEADER =
        "INSERT INTO UserSectionHeader" +
        " (UserID, SectionID, Header, InsertUserID, InsertTimestamp, Display)" +
        " VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP, ?)" +
        "";

    private static final String UPDATE_USER_SECTION_HEADER =
        "UPDATE UserSectionHeader" +
        " SET Header=?," +
        " Display=?," +
        " UpdateUserID=?," +
        " UpdateTimestamp=current_timestamp" +
        " WHERE UserID = ?" +
        " AND SectionID = ?" +
        "";

    private static final String UPDATE_USER_SECTION_DISPLAY =
        "UPDATE UserSectionHeader" +
        " SET Display=?," +
        " UpdateUserID=?," +
        " UpdateTimestamp=current_timestamp" +
        " WHERE UserID = ?" +
        " AND SectionID = ?" +
        "";

    private static final String DELETE_USER_SECTION_HEADER =
        "DELETE FROM UserSectionHeader" +
        " WHERE UserID = ? AND SectionID = ?" +
        "";

    private static final String GET_SYSTEM_SECTION_HEADER =
        "SELECT Header" +
        " FROM SystemSectionHeader" +
        " WHERE SectionID = ?" +
        "";

    private static final String UPDATE_ADDITIONAL_SECTION_HEADER =
        "UPDATE AdditionalHeader" +
        " SET Value = ?" +
        " WHERE ID = ?" +
        "";

    private static final String UPDATE_ADDITIONAL_SECTION_DISPLAY =
        "UPDATE AdditionalHeader" +
        " SET Value = ?, Display = ?, UpdateUserID = ?, UpdateTimestamp=CURRENT_TIMESTAMP" +
        " WHERE ID = ?";

    private static final String GET_ADDITIONAL_SECTION_HEADER =
        "SELECT Value AS Header" +
        " FROM AdditionalHeader" +
        " WHERE ID = ? AND UserID = ?";


    public static void updateUserSectionHeader(int userId, int sectionId, String strNewHeader, int insertUserId)
    {
        updateUserSectionHeader(userId, sectionId, strNewHeader, insertUserId, true);
    }

    /**
     * UpdateUserSectionHeader is called from two places:
     *   1. While Editing the Header (Save, Restore Default)
     *   2. While saving the Design Packet.
     *
     * @param userId
     * @param sectionId
     * @param strNewHeader will be blank if updateUserSectionHeader is called while Saving Packet
     * strNewHeader will have some value when this is called while editing a header
     * @param insertUserId
     * @param display
     */
    public static void updateUserSectionHeader(int userId, int sectionId, String strNewHeader, int insertUserId, boolean display)
    {
        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        String systemHeader = getSystemSectionHeader(sectionId);
        String userHeader = getUserSectionHeader(sectionId, userId);
        boolean updateDisplayOnly = false;
        if ("".equals(strNewHeader))
        {
            updateDisplayOnly = true;
            strNewHeader = userHeader != null ? userHeader : systemHeader;
        }

        if (userHeader == null && (!systemHeader.equals(strNewHeader) || !display))
        {
            // Execute the Header Insert
            qt.runQuery(INSERT_USER_SECTION_HEADER, null, userId+"", sectionId+"", strNewHeader, insertUserId+"", display ? "1" : "0");
        }
        else if (userHeader != null && systemHeader.equals(strNewHeader) && display)
        {
            // Execute Delete Query
            deleteUserSectionHeader(sectionId, userId);
        }
        else
        {
//            qt.runQuery(updateDisplayOnly ? UPDATE_USER_SECTION_DISPLAY : UPDATE_USER_SECTION_HEADER, null,
//                        updateDisplayOnly ? (display ? "1" : "0")       : strNewHeader,
//                        insertUserId+"", userId+"", sectionId+"");
            final String displayArg = display ? "1" : "0";
            final String insertUserIdArg = Integer.toString(insertUserId);
            final String userIdArg = Integer.toString(userId);
            final String sectionIdArg = Integer.toString(sectionId);

            // Add the new header text to the head of the array if updating the section header text
            if (updateDisplayOnly) {
                qt.runQuery(UPDATE_USER_SECTION_DISPLAY, null, displayArg, insertUserIdArg, userIdArg, sectionIdArg);
            }
            else {
                qt.runQuery(UPDATE_USER_SECTION_HEADER, null, strNewHeader, displayArg, insertUserIdArg, userIdArg, sectionIdArg);
            }
        }
    }


    public static String getUserSectionHeader(int sectionId, int userId)
    {
        return getUserSectionHeader(sectionId+"", userId+"");
    }

    private static String getUserSectionHeader(String strSectionId, String strUserId)
    {
        String userHeader = null;

        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        Map<String, Map<String,String>> recordlist =
            qt.runQuery(GET_USER_SECTION_HEADER, "Header", strSectionId, strUserId);
        Set<String> userkeys = recordlist.keySet();
        java.util.Iterator<String> iterUser = userkeys.iterator();
        if (iterUser.hasNext()) {
            userHeader = iterUser.next();
        }

        return userHeader;
    }


    public static void deleteUserSectionHeader(int sectionId, int userId)
    {
        deleteUserSectionHeader(sectionId+"", userId+"");
    }

    private static void deleteUserSectionHeader(String strSectionId, String strUserId)
    {
        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        qt.runQuery(DELETE_USER_SECTION_HEADER, null, strUserId, strSectionId);
    }


    public static String getSystemSectionHeader(int sectionId)
    {
        return getSystemSectionHeader(sectionId+"");
    }

    private static String getSystemSectionHeader(String strSectionId)
    {
        String systemHeader = null;

        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        Map<String, Map<String,String>> recordlist =
            qt.runQuery(GET_SYSTEM_SECTION_HEADER, "Header", strSectionId);
        Set<String> systemkeys = recordlist.keySet();
        java.util.Iterator<String> iterSystem = systemkeys.iterator();
        if (iterSystem.hasNext()) {
            systemHeader = iterSystem.next();
        }

        return systemHeader;
    }


    public static void updateAdditionalSectionHeader(int strId, String strNewHeader)
    {
        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        qt.runQuery(UPDATE_ADDITIONAL_SECTION_HEADER, null, strNewHeader, strId+"");
    }

    public static void updateAdditionalSectionHeader(int strId, Integer updateUserId, String strNewHeader, Boolean display)
    {
        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        qt.runQuery(UPDATE_ADDITIONAL_SECTION_DISPLAY, null, strNewHeader, display ? "1" : "0", updateUserId.toString(), strId+"");
    }

    public static String getAdditionalSectionHeader(int addlId, int userId)
    {
        String addlHeader = null;

        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        Map<String, Map<String,String>> recordlist =
            qt.runQuery(GET_ADDITIONAL_SECTION_HEADER, "Header", addlId+"", userId+"");
        Set<String> addlids = recordlist.keySet();
        java.util.Iterator<String> iterAddl = addlids.iterator();
        if (iterAddl.hasNext()) {
            addlHeader = iterAddl.next();
        }

        return addlHeader;
    }
}
