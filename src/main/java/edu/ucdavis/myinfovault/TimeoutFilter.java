/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: TimeoutFilter.java
 */

package edu.ucdavis.myinfovault;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import edu.ucdavis.mw.myinfovault.util.Duration;

/**<p>
 * Originally taken almost directly from http://stackoverflow.com/a/1503459/17300
 * Then a bunch of stuff added for the "refresh" header processing.</p>
 * <p>Configuration Parameters, settable in web.xml</p>
 * <ul><li><strong>LoginPage</strong> - Tell the filter where the login page is, so it can avoid redirecting away from there.</li>
 * <li><strong>TimeoutUrl</strong> - Where to redirect when a timeout occurs; also, don't redirect <em>away</em> from there.</li>
 * <li><strong>Timeout</strong> - How long until a timeout occurs &mdash; defaults to one hour.</li>
 * <li><strong>TimeUnit</strong> - Time unit measurement for the <em>Timeout</em> parameter, e.g. <kbd>HOURS</kbd>, <kbd>SECONDS</kbd><br>
 *  &nbsp; &nbsp; An all-uppercase value taken from {@link java.util.concurrent.TimeUnit} &mdash; defaults to <kbd>MINUTES</kbd>.</li>
 * </ul>
 * @author Stephen Paulsen
 *
 */
public class TimeoutFilter implements Filter
{
//    private static final String TIMEOUT_PAGE = "logout";
    private static final String LOGIN_PAGE = "MIVLogin";

    private static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.MINUTES;
    private static final long DEFAULT_TIMEOUT_TIME = new Duration(1, TimeUnit.HOURS).as(DEFAULT_TIME_UNIT);

    /** Never redirect away from the login page */
    private String loginPath = LOGIN_PAGE;
    private String timeoutUrl;
    /** The timeoutUrl minus any url parameters. Never redirect away from the timeout/logout page, or there'll be an infinite loop. */
    private String timeoutPage;

    private TimeUnit timeUnit = DEFAULT_TIME_UNIT;
    private long timeoutValue = DEFAULT_TIMEOUT_TIME;
    private long refreshSeconds;

    private ServletContext context;


    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig config) throws ServletException
    {
        this.context = config.getServletContext();
// ---------------------------------------------
/* This block revealed that there was nothing useful available in the ServletContext to try to auto-configure some behavior.
        ServletContext context = config.getServletContext();
        String contextName = context.getServletContextName();
        System.out.println("|| --- TimeoutFilter: contextName==["+contextName+"]");
        Enumeration attnames = context.getAttributeNames();
        while (attnames.hasMoreElements())
        {
            Object el = attnames.nextElement();
            String attName = el.toString();
            Object att = context.getAttribute(attName);
            String val = att.toString();
            System.out.println("|| ----- attribute ["+attName+"] == ["+val+"]");
        }
        Enumeration paramnames = context.getInitParameterNames();
        while (paramnames.hasMoreElements())
        {
            Object el = paramnames.nextElement();
            String paramName = el.toString();
            Object param = context.getAttribute(paramName);
            String val = param.toString();
            System.out.println("|| ----- parameter ["+paramName+"] == ["+val+"]");
        }
*/
// ---------------------------------------------
        String loginPageName = config.getInitParameter("LoginPage");
        if (loginPageName != null) {
            this.loginPath = loginPageName;
        }

        String timeoutUrl = config.getInitParameter("TimeoutUrl");
        if (timeoutUrl == null || timeoutUrl.trim().length() == 0) {
            // throw new SomethingException("TimeoutUrl parameter is required");
            // ... or not.  a default of 60-MINUTES is used if not set.
        }
        this.timeoutUrl = timeoutUrl;

        int queryIndex = this.timeoutUrl.lastIndexOf('?');
        this.timeoutPage = queryIndex > -1 ? this.timeoutUrl.substring(0, queryIndex) : this.timeoutUrl;


        String timeoutString = config.getInitParameter("Timeout");
        if (timeoutString != null)
        {
            try {
                this.timeoutValue = Integer.parseInt(timeoutString);
            }
            catch (NumberFormatException e) {
                // Leave the default in place if not a valid number
                complain("Invalid value [" + timeoutString +"] given for the Timeout value. Defaulting to " + this.timeoutValue);
                context.log("Invalid value [" + timeoutString +"] given for the Timeout value. Defaulting to " + this.timeoutValue);
            }
        }

        String timeoutUnits = config.getInitParameter("TimeUnit");
        if (timeoutUnits != null)
        {
            try {
                this.timeUnit = TimeUnit.valueOf(timeoutUnits.toUpperCase());
            }
            catch (IllegalArgumentException e) {
                // Leave the units as SECONDS if this was invalid
                complain("Invalid time unit [" + timeoutUnits + "] given for the TimeUnit value. Using " + this.timeUnit);
                context.log("Invalid time unit [" + timeoutUnits + "] given for the TimeUnit value. Using " + this.timeUnit);
            }
        }

        complain("|| -- Timeout is set to " + this.timeoutValue + " " + this.timeUnit);
        context.log("|| -- Timeout is set to " + this.timeoutValue + " " + this.timeUnit);
        // Get the number of seconds that will be set in the "refresh" header
        //this.refreshSeconds = TimeUnit.SECONDS.convert(this.timeoutValue, this.timeUnit);
        this.refreshSeconds = new Duration(this.timeoutValue, this.timeUnit).as(TimeUnit.SECONDS);
    }


    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse))
        {
            HttpServletRequest requestHttp = (HttpServletRequest) request;
            HttpServletResponse responseHttp = (HttpServletResponse) response;
            final String contextPath = requestHttp.getContextPath();

            @SuppressWarnings("unused") // inspect the requestPath for debugging
            String requestPath = requestHttp.getRequestURI();

//            if (checkResource(requestHttp))
            if (sessionInvalid(requestHttp) && !ignoreTimeouts(requestHttp))
            {
//                if (sessionInvalid(requestHttp))
//                {
                    context.log("Session is invalid - redirecting to timeout url [" + this.timeoutUrl + "]");
                    String timeoutUrl = contextPath + this.timeoutUrl;
                    responseHttp.sendRedirect(timeoutUrl);
                    return;
//                }
            }

            @SuppressWarnings("unused") // check the response mime-type for debugging
            String responseType = responseHttp.getContentType();

            String headerPath = contextPath + this.timeoutUrl;
            chain.doFilter(request, new AddTimeoutHeader(responseHttp, this.refreshSeconds, headerPath));
        }
    }


    /**
     * Timeouts should be ignored when going to the Login page or the timeout page.
     * Redirecting after timeout when accessing the login page would make it impossible to login,
     * while redirecting when accessing the timeout page would loop infinitely.
     * I'm unsure about the third condition, which is "https://myinfovault.ucdavis.edu/miv/"
     * which came with the code from the StackOverflow posting. We don't use the bare URL
     * so it's being omitted here. Even if we mapped the bare URL to MIVMain we'd still
     * want to timeout on it. Maybe the original author assumed it was a splash/login page.
     * @param request
     * @return true if timeouts should be ignored for the requested resource
     */
    private boolean ignoreTimeouts(HttpServletRequest request)
    {
        String requestPath = request.getRequestURI();
        return requestPath.contains(this.timeoutPage) ||
               requestPath.contains(this.loginPath);/* ||
               requestPath.equals(request.getContextPath() + "/");*/
    }
// Renamed and reversed the test to be clearer than just "checkResource"
//    private boolean checkResource(HttpServletRequest request)
//    {
//        String requestPath = request.getRequestURI();
//        return !(requestPath.contains(this.timeoutPage) ||
//                 requestPath.contains(this.loginPath) ||
//                 requestPath.equals(request.getContextPath() + "/"));
//    }


    private boolean sessionInvalid(HttpServletRequest request)
    {
        return request.getRequestedSessionId() != null && !request.isRequestedSessionIdValid();
    }


    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy()
    {
        // TODO Auto-generated method stub
    }


    /**
     * config.getServletContext().log("message") isn't writing anything and I don't know why.
     * Workaround by using this.
     * @param message
     */
    private void complain(String message)
    {
        System.err.println(this.getClass().getSimpleName() + ": " + message);
    }


    /**
     * Adds a "refresh" response header to servlet responses for content-types that include 'html'
     * such as "text/html" or "text/xhtml"
     * @author Stephen Paulsen
     */
    public class AddTimeoutHeader extends HttpServletResponseWrapper
    {
        HttpServletResponse wrapped;
        long timeout;
        String url;


        /**
         * @param response the HttpServletResponse to wrap
         * @param timeout timeout in seconds
         * @param url the url to direct to when the timeout expires
         */
        public AddTimeoutHeader(HttpServletResponse response, long timeout, String url)
        {
            super(response);
            this.timeout = timeout;
            this.wrapped = response;
            this.url = url;
        }


        @Override
        public void setContentType(String type)
        {
            if (type.matches(".*/x?html.*"))
            {
                String value = timeout + ";url=" + url;
                super.addHeader("refresh", value);
            }
            super.setContentType(type);
        }
    }
}
