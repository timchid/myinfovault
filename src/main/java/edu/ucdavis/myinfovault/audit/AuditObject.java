/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuditObject.java
 */

package edu.ucdavis.myinfovault.audit;

import edu.ucdavis.myinfovault.MivEntity;

/**
 * Represents the Audit Trail Object.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class AuditObject
{
    int         auditID = 0;
    String      actor;
    AuditAction actionType;
    MivEntity   entityType;
    String      entityObjectID;
    String      description;
    String      remarks;
    String      actionDate;

    /**
     * constructor of AuditObject for createAuditTrail
     * @param actionType
     * @param entityType
     * @param entityObjectID
     * @param description
     * @param remarks
     * @param actor
     */
    public AuditObject(AuditAction actionType, MivEntity entityType, String entityObjectID, String description, String remarks, String actor)
    {
        this.actionType = actionType;
        this.entityType = entityType;
        this.entityObjectID = entityObjectID;
        this.description = description;
        this.remarks = remarks;
        this.actor = actor;
    }

    /**
     * constructor of AuditObject for viewAuditTrail
     * @param auditID
     * @param actionType
     * @param entityType
     * @param entityObjectID
     * @param description
     * @param remarks
     * @param actionDate
     * @param actor
     */
    public AuditObject(int auditID, AuditAction actionType, MivEntity entityType, String entityObjectID,
                       String description, String remarks, String actionDate, String actor)
    {
        this.auditID = auditID;
        this.actionType = actionType;
        this.entityType = entityType;
        this.entityObjectID = entityObjectID;
        this.description = description;
        this.remarks = remarks;
        this.actionDate = actionDate;
        this.actor = actor;
    }


    /**
     * @return the audit ID
     */
    public int setAuditID()
    {
        return auditID;
    }

    /**
     * @return the auditID
     */
    public int getAuditID()
    {
        return auditID;
    }


    /**
     * @return the actionType
     */
    public AuditAction getActionType()
    {
        return actionType;
    }

    /**
     * @return the entityType
     */
    public MivEntity getEntityType()
    {
        return entityType;
    }

    /**
     * @return the entityObjectID
     */
    public String getEntityObjectID()
    {
        return entityObjectID;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }


    /**
     * @return the remarks
     */
    public String getRemarks()
    {
        return this.remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks)
    {
        this.remarks = remarks;
    }


    /**
     * @return the currentUserID
     */
    public String getActor()
    {
        return this.actor;
    }

    /**
     * @return the actionDate
     */
    public String getActionDate()
    {
        return this.actionDate;
    }

    @Override
    public String toString()
    {
        return "[" + entityObjectID + "] : [" + description + "]";
    }

}
