package edu.ucdavis.myinfovault.data;

import java.util.ArrayList;
import java.util.List;


/**
 * This is simple java class used to store different addresses based on address types.
 * 
 * @author Venkat Vegesna
 */
public class Address
{
    /* These must match the entries in the AddressType table. */
    // TODO: make these an enum
    public static final int PERMANENT_HOME_ADDRESS = 1;
    public static final int CURRENT_HOME_ADDRESS   = 2;
    public static final int OFFICE_ADDRESS         = 3;
    public static final int POSTAL_ADDRESS         = 4;

    private String street1;
    private String street2;
    private String city;
    private String state;
    private String zipCode;
    private int addressTypeID;
    private int addressRecID;
    private String addressRecType;
    private List<Phone> phones;

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        if (city != null) city = city.replaceAll("\"", "&quot;");
        this.city = city;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        if (state != null) state = state.replaceAll("\"", "&quot;");
        this.state = state;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        if (zipCode != null) zipCode = zipCode.replaceAll("\"", "&quot;");
        this.zipCode = zipCode;
    }

    public int getAddressRecID()
    {
        return addressRecID;
    }

    public void setAddressRecID(int addressRecID)
    {
        this.addressRecID = addressRecID;
    }

    public String getAddressRecType()
    {
        return addressRecType;
    }

    public void setAddressRecType(String addressRecType)
    {
        this.addressRecType = addressRecType;
    }

    public int getAddressTypeID()
    {
        return addressTypeID;
    }

    public void setAddressTypeID(int addressTypeID)
    {
        this.addressTypeID = addressTypeID;
    }

    public String getStreet1()
    {
        return street1;
    }

    public void setStreet1(String street1)
    {
        if (street1 != null) street1 = street1.replaceAll("\"", "&quot;");
        this.street1 = street1;
    }

    public String getStreet2()
    {
        return street2;
    }

    public void setStreet2(String street2)
    {
        if (street2 != null) street2 = street2.replaceAll("\"", "&quot;");
        this.street2 = street2;
    }

    public List<Phone> getPhones()
    {
        return phones;
    }

    public void setPhones(List<Phone> phones)
    {
        this.phones = phones;
    }

    public void addPhone(Phone p)
    {
        if (this.phones == null) {
            this.phones = new ArrayList<Phone>();
        }
        this.phones.add(p);
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(this.street1).append('\n');
        if (this.street2 != null && this.street2.length() > 0) {
            sb.append(this.street2).append('\n');
        }
        sb.append(this.city).append(", ").append(this.state).append(' ').append(this.zipCode);
        return sb.toString();
    }
}
