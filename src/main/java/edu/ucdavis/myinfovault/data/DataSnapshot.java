package edu.ucdavis.myinfovault.data;

import java.util.List;


public class DataSnapshot
{
    private long dossierID = 0;
    private static String snapshotTableSuffix = "_snapshot";
    private List<TableSnapshotData>tableSnapshotDataList = null;


    public DataSnapshot(long dossierID, List<TableSnapshotData>tableSnapshotDataList)
    {
        this.dossierID = dossierID;
        this.tableSnapshotDataList = tableSnapshotDataList;
    }

    public List<TableSnapshotData> getTableSnapshotDataList()
    {
        return tableSnapshotDataList;
    }

    public long getDossierID()
    {
        return dossierID;
    }

    public static String getSnapshotTableSuffix()
    {
        return snapshotTableSuffix;
    }


}
