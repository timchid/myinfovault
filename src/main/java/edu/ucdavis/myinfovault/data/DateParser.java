/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DateParser.java
 */


package edu.ucdavis.myinfovault.data;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Parse a variety of date formats to assist in data migration.
 * Could be generally useful and may be extracted to another package later.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class DateParser
{
    /** Common standard date formats that MIV will parse. */
    private static final int[] standardDateFormats = {
        DateFormat.SHORT,
        DateFormat.MEDIUM,
        DateFormat.LONG
    };

    /** Less standard but commonly written formats that MIV will accept. */
    private static final String[] dateFormats = {
        "yyyy/MM/dd",
        // These next three are variations on DateFormat.MEDIUM
        "MMM. d, y",
        "MMM. d y",
        "MMM d y",
        "MMddyy",       // e.g. 120741
        "MMMMMMMM yy"   // e.g. December 41 (or 1941)
    };

    /** An initialized and Thread-Safe collection of date formats defining what we will accept from the client. */
    private static final ThreadLocal<DateFormat[]> parseFormats = new ThreadLocal<DateFormat[]>()
    {
        @Override protected DateFormat[] initialValue() {
            int formatCount = standardDateFormats.length + dateFormats.length;
            DateFormat[] temp = new DateFormat[formatCount];
            int pos = 0;
            for (int formatType : standardDateFormats)
            {
                temp[pos++] = DateFormat.getDateInstance(formatType);
            }
            for (String formatString : dateFormats)
            {
                temp[pos++] = new SimpleDateFormat(formatString);
            }
            return temp;
        }
    };


    /**
     *
     * @param dateString the string to parse
     * @return a parsed Date object, or <code>null</code> if the string couldn't be parsed
     */
    public static Date parse(String dateString)
    {
        return parse(dateString, (DateFormat)null);
    }


    /**
     *
     * @param dateString the string to parse
     * @param strict true for strict parsing, false for lenient parsing
     * @return a parsed Date object, or <code>null</code> if the string couldn't be parsed
     */
    public static Date parse(String dateString, boolean strict)
    {
        return parse(dateString, strict, (DateFormat)null);
    }


    /**
     *
     * @param dateString the string to parse
     * @param mine a DateFormat to try first, followed by the standard formats
     * @return a parsed Date object, or <code>null</code> if the string couldn't be parsed
     */
    public static Date parse(String dateString, DateFormat... mine)
    {
        return parse(dateString, false, mine);
    }


    /**
     *
     * @param dateString the string to parse
     * @param strict true for strict parsing, false for lenient parsing
     * @param mine a DateFormat to try first, followed by the standard formats
     * @return a parsed Date object, or <code>null</code> if the string couldn't be parsed
     */
    public static Date parse(String dateString, boolean strict, DateFormat... mine)
    {
        if (dateString == null) return null;

        Date returnDate = null;
        // Only process the caller-supplied date formatters if
        // 1. The array passed in is not null and
        // 2. It's not empty
        if (mine != null && mine.length > 0)
        {
            for (DateFormat df : mine)
            {
                if (df == null) continue; // skip any nulls passed in

                try {
                    boolean originalLenientSetting = df.isLenient();
                    df.setLenient(!strict);
                    Date d = df.parse(dateString);
                    df.setLenient(originalLenientSetting);
                    return d;
                }
                catch (ParseException e) {
                    // Couldn't parse with this provided format, keep trying with the others
                }
            }
        }

        int i = 0;
        // Caller may have provided a DateFormat that accepts dashes, but
        // ours only use slashes. Change any dash to slash before parsing.
        final String parseString = dateString.replaceAll("-", "/");
        DateFormat[] dfa = parseFormats.get();
        while (i < dfa.length)
        {
            try {
                DateFormat parser = dfa[i];
                parser.setLenient(!strict);
                Date d = parser.parse(parseString);
                returnDate = d;
                break;
            }
            catch (ParseException e) {
                i++; // try the next DateFormat
            }
        }

        return returnDate;
    }

    public DateParser()
    {
        super();
    }

}
