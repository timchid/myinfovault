package edu.ucdavis.myinfovault.data;

/**
 * Constants to use for the various document IDs we use.
 * We no longer have to (1) use magic numbers:<br>
 * <code>if (docID == 6)</code><br>
 * or (2) redefine a constant for "6" in each class that needs one.
 * Must match the ID numbers in our Document table.
 *
 * @author Stephen Paulsen
 */
public enum DocumentIdentifier
{
    INVALID(-1,-1),
    CANDIDATES_STATEMENT(1,-1),
    POSITION_DESCRIPTION(2,-1),
    EVALUATIONS(3,-1),
    TEACHING(4,-1),
    SERVICE(5,-1),
    PUBLICATIONS(6,-1),
    HONORS_AWARDS(7,-1),
    GRANTS(8,-1),
    AGSTATION(9,-1),
    EXTENDING_KNOWLEDGE(10,-1),
    CV(11,-1),
    NIH(12,-1),
    CONTRIBUTIONS_TO_JOINTLY_AUTHORED_WORKS(51,6),
    CREATIVE_ACTIVITIES(52,-1),
    CONTRIBUTIONS_TO_JOINTLY_CREATED_WORKS(53,52),
    DIVERSITY_STATEMENT(54,-1),
    ORG_CHART(17,-1),
    SHORT_BIOGRAPHY(70,-1),
    ;

    private int value;
    private int parentDocumentID;

    /**
     * Used by DocumentIdentifier internally. Also applying parent relationship between documents.
     * @param value : Document id of the MIV Document.
     * @param parentDocumentID : That ID is user to identified Document's parent ID. Use -1 in case of no parent document id exist.
     * <p> Example: <br>
     * <b>CONTRIBUTIONS_TO_JOINTLY_CREATED_WORKS(53,52)</b><br>
     * <b>53</b> : Document ID of CONTRIBUTIONS_TO_JOINTLY_CREATED_WORKS<br>
     * <b>52</b> : CREATIVE_ACTIVITIES Document ID actually CONTRIBUTIONS_TO_JOINTLY_CREATED_WORKS sharing data with CREATIVE_ACTIVITIES
     * </p>
     */
    DocumentIdentifier(int value, int parentDocumentID)
    {
        this.value = value;
        this.parentDocumentID = parentDocumentID;
    }

    /**
     * Gets a DocumentIdentifier given an integer ID value from the database.
     * @param n ID of the document
     * @return a DocumentIdentifier representing the corresponding document.
     */
    public static DocumentIdentifier convert(int n)
    {
        DocumentIdentifier value = INVALID;

        for (DocumentIdentifier current : DocumentIdentifier.values())
        {
            if (current.getValue() == n)
            {
                value = current;
                break;
            }
        }

        return value;
    }

    /**
     * Bean-style access to the value (ID number) for those places
     * that can't use an enum (like in a jsp)
     *
     * @return the numeric value of this document ID
     * @see #getId()
     */
    public int getValue()
    {
        return this.value;
    }

    public int value()
    {
        return this.getValue();
    }


    /**
     * Bean-style access to the ID number (value) for those places
     * that can't use an enum (like in a jsp)
     *
     * @return the numeric value of this document ID
     * @see #getValue()
     */
    public int getId()
    {
        return this.getValue();
    }

    public int id()
    {
        return this.getValue();
    }

    /**
     * Bean-style access to the parentDocumentID number for those places
     * that can't use an enum (like in a jsp)
     *
     * @return the numeric value of this document's parentDocumentID
     */
    public int getParentDocumentId()
    {
        return parentDocumentId();
    }

    public int parentDocumentId()
    {
        return this.parentDocumentID;
    }

}
