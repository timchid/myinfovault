package edu.ucdavis.myinfovault.data;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;

/**
 * @author bmgarg
 * @since MIV 2.1
 *
 * Updated: Pradeep on 08/03/2012 -- to resolve empty rules issue
 */
public abstract class Fetcher
{
    public static String buildQuery(String selectFields, String fromTable, String whereClause, String orderby)
    {
        return buildQuery(selectFields, fromTable, whereClause, orderby, null);
    }

    public static String buildQuery(String selectFields, String fromTable, String whereClause, String orderby, Iterable<Ruleset> ruleSet)
    {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ").append(selectFields);
        query.append(" FROM ").append(fromTable);


        if (!"".equals(whereClause) && whereClause != null)
        {
            query.append(" WHERE ").append(whereClause);
            if (ruleSet != null)
            {
                for (Ruleset rule : ruleSet)
                {
                    if(!rule.getRecType().equals(fromTable)) break;

                    if( !StringUtils.isEmpty(rule.toString()))
                    {
                        query.append(" AND ").append(rule.toString());
                    }
                }
            }
        }
        else if (ruleSet != null)
        {
            query.append(" WHERE ");
            StringBuilder ruleSetWhereClause = new StringBuilder();
            boolean first = true;
            for (Ruleset rule : ruleSet)
            {
                if(!rule.getRecType().equals(fromTable)) break;

                if( !StringUtils.isEmpty(rule.toString()))
                {
                    if(first)
                    {
                        ruleSetWhereClause.append(rule.toString());
                    }
                    else
                    {
                        ruleSetWhereClause.append(" AND ").append(rule.toString());
                    }
                    first=false;
                }
            }
            query.append(ruleSetWhereClause.toString());
        }

        if (!"".equals(orderby) && orderby != null)
        {
            query.append(" ORDER BY ").append(orderby);
        }

        return query.toString();
    }
}
