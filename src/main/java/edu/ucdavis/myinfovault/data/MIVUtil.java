/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVUtil.java
 */


package edu.ucdavis.myinfovault.data;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.ucdavis.myinfovault.htmlcleaner.HtmlCleaner;
import edu.ucdavis.myinfovault.htmlcleaner.InvalidMarkupException;


/**
 * Handle various utility functions for MIV.
 *
 * @author Rick Hendricks
 * @author Stephen Paulsen
 * @author Brij Garg
 * @since MIV 2.0
 */
public class MIVUtil
{
    /**<p>
     * Replace (Greek) characters with Numeric Character References.
     * See the <a href="http://www.w3.org/TR/html4/charset.html#h-5.3.1">HTML 4 spec</a>
     *</p>
     * @param input String to scan and replace greek characters.
     * @return A new string with characters replaced, or an empty string.
     */
    public static String greekToRef(String input)
    {
        if (input == null) return "";

        StringBuilder sb = new StringBuilder();
        int len = input.length();
        for (int i = 0; i < len; i++)
        {
            char ch = input.charAt(i);
            String replacement; // most characters will be themselves

            if (Character.UnicodeBlock.of(ch).equals(Character.UnicodeBlock.GREEK))
            {
                replacement = "&#" + Integer.toString(ch) + ";";
            }
            else
            {
                replacement = "" + ch;
            }
            sb.append(replacement);
        }
        return sb.toString();
    }

// TODO: The above charToRef() should really use codepoints, not characters.
//       Here's a start at changing it.
/*
    public static String charToRef2(String input)
    {
        if (input == null) return "";

        StringBuilder sb = new StringBuilder();
        int len = Character.codePointCount(input, 0, input.length());
        for (int i = 0; i < len; i++)
        {
            int ch = Character.codePointAt(input, i);

        }

        return sb.toString();
    }
*/

//    /**<p>
//     * Replace Character References with actual character values.
//     *</p>
//     * @param input string to scan for character reference.
//     * @return A new string with characters replaced, or an empty string.
//     * @deprecated use translateControlCharacters(replaceNumericReferences(String))
//     */
//    @Deprecated
//    public static String refToChar(String inputString)
//    {
//        return replaceCp1252(replaceNumericReferences(inputString));
//    }
//    // refToChar()



    /**
     * Provides translations for the characters in the Unicode "C0" control
     * character range. These are the classic ASCII control characters.
     * Most are simply removed - tranlated to the NUL character.
     */
    private static final char[] C0_translation = {
    /*    00        01        02        03        04        05        06        07    */
        '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000',
    /*    08        09        0a        0b        0c        0d        0e        0f    */
        '\u0000', '\u0009',  0x0a,     '\u0000', '\u0000', 0x0d,    '\u0000', '\u0000',
    /*    10        11        12        13        14        15        16        17    */
        '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000',
    /*    18        19        1a        1b        1c        1d        1e        1f    */
        '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000', '\u0000',
    };


    /**
     * Provides translations for the Windows Cp1252 characters that exist
     * in the reserved Unicode "C1" control character range with their
     * proper Unicode equivalent characters.
     */
    private static final char[] C1_translation = {
    /*    80        81        82        83        84        85        86        87    */
    //                     hex  dec  letter
        '\u20ac',       //  80  128  Euro Sign
        '\u0000',       //  81  129  none - no windows char at this code point
        '\u201a',       //  82  130  Single Low-9 Quotation Mark
        '\u0192',       //  83  131  Latin Small Letter F With Hook
        '\u201e',       //  84  132  Double Low-9 Quotation Mark
        '\u2026',       //  85  133  Horizontal Ellipsis
        '\u2020',       //  86  134  Dagger
        '\u2021',       //  87  135  Double Dagger
    /*    88        89        8a        8b        8c        8d        8e        8f    */
    //                     hex  dec  letter
        '\u02c6',       //  88  136  Modifier Letter Circumflex Accent (combining accent?)
        '\u2030',       //  89  137  Per Mille Sign
        '\u0160',       //  8a  138  Latin Capital Letter S With Caron
        '\u2039',       //  8b  139  Single Left-Pointing Angle Quotation Mark
        '\u0152',       //  8c  140  Latin Capital Ligature OE
        '\u0000',       //  8d  141  none - no windows char at this code point
        '\u017d',       //  8e  142  Latin Capital Letter Z With Caron
        '\u0000',       //  8f  143  none - no windows char at this code point
    /*    90        91        92        93        94        95        96        97    */
    //                     hex  dec  letter
        '\u0000',       //  90  144  none - no windows char at this code point
        '\u2018',       //  91  145  Left Single Quotation Mark
        '\u2019',       //  92  146  Right Single Quotation Mark
        '\u201c',       //  93  147  Left Double Quotation Mark
        '\u201d',       //  94  148  Right Double Quotation Mark
        '\u2022',       //  95  149  Bullet
        '\u2013',       //  96  150  En Dash
        '\u2014',       //  97  151  Em Dash
    /*    98        99        9a        9b        9c        9d        9e        9f    */
    //                     hex  dec  letter
        '\u02dc',       //  98  152  Small Tilde
        '\u2122',       //  99  153  Trade Mark Sign
        '\u0161',       //  9a  154  Latin Small Letter S With Caron
        '\u203a',       //  9b  155  Single Right-Pointing Angle Quotation Mark
        '\u0153',       //  9c  156  Latin Small Ligature OE
        '\u0000',       //  9d  157  none - no windows char at this code point
        '\u017e',       //  9e  158  Latin Small Letter Z With Caron
        '\u0178',       //  9f  159  Latin Capital Letter Y With Diaeresis
    };


//    /**
//     * Replace Windows Cp1252 characters that exist in the reserved Unicode "C1" control character range
//     * with their proper Unicode equivalent characters. Drop most characters in the "C0" range.
//     * @deprecated use {@link #translateControlCharacters(CharSequence)} instead.
//     */
//    @Deprecated
//    public static String replaceCp1252(CharSequence in)
//    {
//        return translateControlCharacters(in);
//    }

    /**
     * Replace Windows Cp1252 characters that exist in the reserved  Unicode "C1" control character range
     * with their proper Unicode equivalent characters. Drop most characters in the "C0" range.
     * 
     * @param in Windows Cp1252 character sequence
     * @return equivalent unicode string
     */
    public static String translateControlCharacters(CharSequence in)
    {
        if (in == null) return null;
        if ("".equals(in)) return "";

        int len = in.length();
        StringBuilder sb = new StringBuilder(len);
        char replacement;

        for (int i = 0; i < len; i++)
        {
            char ch = replacement = in.charAt(i);
            int charVal = Integer.valueOf(ch);

            // Check for low C0 control characters
            if (charVal < 0x20)
            {
                replacement = C0_translation[charVal];
            }
            // Check for high C1 control characters
            else if (charVal >= 0x80 && charVal <= 0x9f)
            {
                replacement = C1_translation[charVal - 0x80];
            }

            if (replacement != '\u0000') sb.append(replacement);
        }

        return sb.toString();
    }



    private static final Pattern NUMERIC_ENTITY_PATTERN = Pattern.compile("&#(\\d{1,5});");

    /**
     * Replace numeric entity references with the characters they represent.
     * Only works on decimal (base 10) encoded references, not on hexidecimal references;
     *
     * @param in A character sequence possibly containing numeric character entity references.
     * @return A new String with entity reference substrings replaced by single characters,
     * or <code>null</code> if <code>null</code> was passed.
     */
    public static String replaceNumericReferences(CharSequence in)
    {
        if (in == null) return null;

        StringBuffer sb = new StringBuffer();   // Matcher requires a StringBuffer, not a StringBuilder
        Matcher matcher = NUMERIC_ENTITY_PATTERN.matcher(in);

        String key = null;
        String replacement = null;

        while (matcher.find())
        {
            key = matcher.group(1);
            int charVal = Integer.valueOf(key).intValue();
            //uncomment to replace control chars with their visual symbols
            //if (charVal < 32) charVal += 0x2400;
            replacement = new Character((char) charVal).toString();
            matcher.appendReplacement(sb, replacement);
        }
        matcher.appendTail(sb);

        return sb.toString();
    }


    /**
     * Replace XML/HTML character entity references with the characters they represent.
     * Only less-than (&lt;) greater-than (&gt;) and ampersand (&amp;) are replaced.
     *
     * @param in A character sequence possibly containing character entity references.
     * @return A new String with entity reference substrings replaced by single characters,
     * or <code>null</code> if <code>null</code> was passed.
     */
    public static String replaceEntityReferences(CharSequence in)
    {
        if (in == null) return null;
        String s = in.toString();
        return s.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&amp;", "&");
    }


    /**
     * Replace newlines with a <code>&lt;br/&gt;</code> tag.
     * 
     * @param in character sequence to search
     * @return altered character sequence
     */
    public static String replaceNewline(CharSequence in)
    {
        return MIVUtil.replaceNewline(in, "<br/>");
    }


    private static final char CR = 0x0D;
    private static final char LF = 0x0A;
    private static final String strCRLF = new StringBuilder().append(CR).append(LF).toString();
    private static final String strCR = new StringBuilder().append(CR).toString();
    private static final String strLF = new StringBuilder().append(LF).toString();

    /**
     * Replace newlines with the provided character sequence.
     * 
     * @param in character sequence to search
     * @param replacement newline replacement
     * @return altered character sequence
     */
    public static String replaceNewline(CharSequence in, CharSequence replacement)
    {
        if (in == null) return null;

        String out = in.toString();
        String r = replacement.toString();
        out = out.replaceAll(strCRLF, r).replaceAll(strCR, r).replaceAll(strLF, r);

        return out;
    }


    /**
     * Replace non-printable characters with within the 00-08 range with specified value.
     * If no value is provided, replace with an empty string.
     * @param in
     * @param value
     * @return
     */
    public static String replaceNonPrintable(String in, String value)
    {
        if (in == null || in.length() < 1) return in;

        String out;
        out = in.replaceAll("[\\x00-\\x08]", value == null ? "" : value);
        return out;
    }


//    /**<p>
//     * Replace Non-Greek Numeric Character References with actual character values.
//     *</p>
//     * @param input string to scan for character reference.
//     * @return A new string with characters replaced, or an empty string.
//     * @deprecated use {@link #replaceNumericReferences(CharSequence)} instead.
//     */
//    @Deprecated
//    public static String nonGreekRefToChar(String inputString)
//    {
//        String replaceString = new String(inputString);
//        Pattern pattern = null;
//        Matcher matcher = null;
//        Integer matchStart = null;
//        Integer matchEnd = null;
//
//        if (inputString != null)
//        {
//            // Check for symbols
//            pattern = Pattern.compile("&#[0-9]\\d+;");
//            matcher = pattern.matcher(inputString);
//            matchStart = 0;
//            matchEnd = 0;
//
//            while (matcher.find())
//            {
//                matchStart = Integer.valueOf(matcher.start());
//                matchEnd = Integer.valueOf(matcher.end());
//                String replace = inputString.substring(matchStart, matchEnd);
//                // parse out the numeric portion
//                String numericEntity = inputString.substring(matchStart + 2, matchEnd - 1);
//                Character aChar = new Character((char) Integer.valueOf(numericEntity).intValue());
//                // Only replace non-greek
//                if (!Character.UnicodeBlock.of(aChar).equals(Character.UnicodeBlock.GREEK))
//                {
//                    replaceString = replaceString.replaceFirst(replace, aChar.toString());
//                }
//            }
//        }
//
//        return replaceString;
//    }
//    //nonGreekRefToChar()


    /**
     * Strong sanitation of form or url parameters that allows only letters,
     * numbers, dashes, and underscores. Use for predefined values that we
     * provide, especially URL parameters and hidden form fields, that will
     * always conform to these rules, to prevent XSS and SQL injection attacks.
     * @since MIV 3.0
     *
     * @param in String to sanitize
     * @return Safe string with disallowed characters removed.
     */
    public static String sanitize(CharSequence in)
    {
        if (in == null) return null;    // Returning "" when null was passed causes problems
        if (in.length() < 1) return ""; // !Only return a string if something was passed

        String newValue = in.toString();
        newValue = newValue.replaceAll("[^a-zA-Z0-9_-]", "");
        return newValue;
    }


    /**
     * Escape all markup in the input except for valid sub/sup pairs
     * @param in
     * @return Escaped string
     */
    public static String escapeMarkup(CharSequence in)
    {
        if (in == null || in.length() < 1) return "";

        String newValue = in.toString();
        newValue = newValue.replaceAll("&", "&amp;");
        newValue = newValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

        // Use the "reluctant" form of ".*?" to match the minimally inclusive set contained within sup/sub tags,
        // otherwise sibling tags could be missed, e.g.:
        //     this<sub>2</sub> that<sub>3</sub> end
        //        |^^^^^|                |^^^^^^|
        //   A "greedy" form ".*" match could treat the first and last as the matching pair.
        newValue = newValue.replaceAll("&lt;sup&gt;(.*?)&lt;/sup&gt;", "<sup>$1</sup>");
        newValue = newValue.replaceAll("&lt;sub&gt;(.*?)&lt;/sub&gt;", "<sub>$1</sub>");

        return newValue;
    }


    /** HTML cleaner used to make WYSIWYG fields safe, but still support the markup we allow. */
    private static HtmlCleaner wysiwygCleaner = new HtmlCleaner("edu.ucdavis.myinfovault.format.wysiwyg");

    /**
     * Fix data when editing an entry that uses the WYSIWYG editor.
     * Since this data is destined for the WYSIWYG, which ultimately places it into a div, the browser gets
     * a chance to interpret the string as HTML.  Strings the WYSIWYG already escaped for storing become the
     * characters represented so, for example, a less-than sign ('<') which was escaped as "&amp;lt;" gets
     * re-interpreted when it's put in the div and becomes an actual less-than again.
     * The sequence &amp;lt;b&amp;gt; becomes &lt;b&gt; and is then an actual tag. We need to escape the escaped
     * string so when the browser does its transformations the result is the actual string stored in the database.
     * This is accomplished by treating just three characters specially: '&lt;' '&gt;' and '&amp;'
     *
     * @param in a String to be escaped
     * @return an escaped String
     */
    public static String escapeWysiwygEdit(String in)
    {
        if (in == null || in.length() < 1) return in;

        // IMPORTANT: Ampersand needs to be done first, otherwise we'd
        //     escape the '&' we just put in when escaping '<' and '>'

        String out = in.replaceAll("&amp;", "&amp;amp;")
                       .replaceAll("&lt;", "&amp;lt;")
                       .replaceAll("&gt;", "&amp;gt;");
        try {
            out = wysiwygCleaner.removeTags(out);
        }
        catch (InvalidMarkupException e) {
            // If the HTML (XML) is not valid bludgeon it with blind escaping
            out = MIVUtil.escapeMarkup(out);
        }

        return out;
    }


    public static String escapeAmpersand(String in)
    {
        if (in == null || in.length() < 1) return in;
        return in.replaceAll("&", "&amp;");
    }


    /**
     * Escape a character in a String.
     * Notice that if the escape character is a backslash ('\') you will need to specify <strong>four</strong>
     * backslashes ("\\\\") as the escape String, due to Java string literal escaping rules and regex rules.
     *
     * @param ch The character that will be escaped.
     * @param esc The String used to escape each occurrance of the character.
     * @param in String in which characters will be escaped.
     * @return A String with all occurrances of the given character escaped.
     */
    public static String escapeChar(char ch, String esc, String in)
    {
        if (in == null || in.length() < 1) return in;

        String match = "" + ch;
        String replacement = esc + ch;
        String out = in.replaceAll(match, replacement);
        return out;
    }


    public static String decodeReqParamValue(String reqParam)
    {
        String decodedParamValue = "";
        try
        {
            // + is a Dangling meta character in Reg Exp. Escape it otherwise PatternSyntaxException will be thrown
            // ISO-8859-1 is used to decode. If we use UTF-8 the special characters with %XY hexadecimal value will be converted into junk characters
//            decodedParamValue = URLDecoder.decode(reqParam.replaceAll("\\+", "%2B"), "ISO-8859-1");

            // SDP 2008-11-14 :: NOTE the above statement re ISO-8859-1 ... during development of MIV v2.3
            //                   we were getting the problem with percent-encoded 2-byte characters being
            //                   turned to junk when using ISO-8859-1, and changing to UTF-8 cured it.
            if(reqParam!=null)
            {
                decodedParamValue = URLDecoder.decode(reqParam.replaceAll("\\+", "%2B"), "UTF-8");
            }
        }
        catch (UnsupportedEncodingException e)
        {
            // This only happens if the encoding (UTF-8 or ISO-8859-1) is not supported, which is never.
            e.printStackTrace();
        }
        return decodedParamValue;
    }


    /**
     * Compute the elapsed time from the given start and end times, and optionally display a message showing it.
     *
     * @param startTime - time when event started, in milliseconds
     * @param endTime - time when even completed, in milliseconds
     * @param display - write a message showing the elapsed time iff <code>true</code>.
     * @param message - the message to write if <code>display</code> is true.
     * @return The time elapsed from startTime to endTime in milliseconds.
     */
    public static long elapsedTime(long startTime, long endTime, boolean display, String message)
    {
        long duration = endTime - startTime;
        long seconds = duration / 1000;
        long msecs = duration % 1000;

        if (display) {
            System.out.println(message + " - " + seconds + "." + msecs + " seconds");
        }

        return duration;
    }


    /**
     * Put the current thread to sleep for the number of milliseconds given.
     * Continues waiting, even if the thread is interrupted, until the whole time has elapsed.
     * @param millis number of milliseconds to wait
     */
    public static void pause(long millis)
    {
        long start = System.currentTimeMillis();
        long end = start + millis;
        long now = System.currentTimeMillis();
        long remaining = end - start;

        do {
            try {
                Thread.sleep(remaining);
            }
            catch (InterruptedException e) {
                // do nothing here
            }
            now = System.currentTimeMillis();
            remaining = end - now;
        } while (remaining > 0);//(now < end);
    }
}
