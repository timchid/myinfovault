/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: QueryTool.java
 */

package edu.ucdavis.myinfovault.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.exception.MIVApplicationException;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class QueryTool
{
    //private static Logger log = Logger.getLogger("MIV");
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(QueryTool.class);

    /** Maximum attempts to connect to the database before tossing our cookies */
    private static final int MAX_ATTEMPTS = 5;

    DataSource source     = null;


    public QueryTool(DataSource source)
    {
        this.source = source;
    }

    public QueryTool(String datasourceName)
        //throws NamingException, SQLException
    {
        boolean connected = false;
        int attempts = 0;
        Exception lastException = null;

        do {
            attempts++;
            connected = true;
            this.source = (DataSource)MivServiceLocator.getBean(datasourceName);
            if (this.source == null)
            {
                logger.warn("QueryTool failed database naming lookup (attempt #{})", attempts);
                try { Thread.sleep(1250); } catch (InterruptedException e) { }
                connected = false;
            }
    //        catch (SQLException sqe)
    //        {
    //            log.log(Level.WARNING, "QueryTool failed to create database connection (attempt #" + attempts + ")");
    //          lastException = sqe;
    //            //throw new RuntimeException(sqe); // TODO: change this back - don't use RuntimeException
    //            /* Wait for 1 1/4 second after a failure */
    //            try { Thread.sleep(1250); } catch (InterruptedException e) { }
    //        }
        } while (!connected && attempts < MAX_ATTEMPTS);

        /* If we're still not connected re-throw the last exception encountered. */
        if (!connected)
        {
            throw new RuntimeException(lastException);
        }

        //initComplete = true;
    } //constructor


    /**
     * Get a new connection from the pool
     * @return connection
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException
    {
        Connection connection = null;
        logger.trace("Getting new connection");
        connection = source.getConnection();
        return connection;
    }


    /**
     * Release connection from the pool
     * @param connection
     * @throws SQLException
     */
    public void releaseConnection(Connection connection) throws SQLException
    {
        logger.trace("Releasing connection");
        if (connection != null)
        {
            connection.commit();
            connection.close();
            connection = null;
        }
    }


    /**
     * Get back a List instead of a Map.
     * A List is easier to deal with when all we're doing is looping through the results,
     * which we often do.
     * The original runQuery() calls can probably be rewritten to use this underneath, then
     * insert these results into a Map --- or, contrariwise, maybe this should use runQuery
     * and return the values?? But this can't know what to use as a "key"
     * @param query An SQL query to run
     * @param args The arguments to use to set any required query parameters.
     * @return a list of records
     */
    public List<Map<String,String>> getList(String query, String... args)
    {
        return getList(MIVConfig.getConfig().getDoTimings(), query, args);
    }

    /**
     * Same as {@link #getList(String, String...)} but with optional time accounting.
     * @param fReportTime A true/false flag to indicate if the query should be timed and reported.
     * @param query An SQL query to run
     * @param args The arguments to use to set any required query parameters.
     * @return a list of records
     */
    public List<Map<String,String>> getList(boolean fReportTime, String query, String... args)
    {
    // TODO: getList is (mostly) duplicated from runQuery - refactor & combine them.
        List<Map<String,String>> resultList = new LinkedList<Map<String,String>>();
        PreparedStatement statement = null;
        ResultSet results = null;
        Connection connection = null;

//        boolean debug = false;
//        if (MIVConfig.isInitialized()) debug = MIVConfig.getConfig().isDebug();

        boolean retry = false;
        int attempts = 0;

        try {
            do { // loop while retrying
                try
                {
                    connection = getConnection();
                    statement = connection.prepareStatement(query);

                    // Fill in the query parameters from the varargs passed in.
                    int lastPosition = 0;
                    int parmPosition = 0;
                    int count = 0;
                    while ((parmPosition = query.indexOf('?', lastPosition)) != -1)
                    {
                        logger.debug("setting '?' #{} to {}", count + 1, args[count]);
                        statement.setString(count + 1, args[count]);
                        lastPosition = parmPosition + 1;
                        count++;
                    }
                    logger.debug("getList: query is [" + query + "]");

                    // These 2 vars only used for time accounting
                    // They are within the retry loop because we're interested in how long the query takes,
                    // not in how long all the retries take, so we reset the start time on each iteration.
                    long startTime = System.currentTimeMillis();
                    int rowCount = 0;

                    boolean isResultSet = statement.execute();

                    if (isResultSet) //isResultSet==true means we have a ResultSet, false is an update count
                    {
                        results = statement.getResultSet();

                        ResultSetMetaData metaData = results.getMetaData();
                        int colCount = metaData.getColumnCount();

                        while (results.next())
                        {
                            rowCount++;
                            Map<String, String> fieldmap = new HashMap<String, String>();

                            for (int i = 1; i <= colCount; i++)
                            {
                                String colname = metaData.getColumnLabel(i).toLowerCase();
                                String colvalue = results.getString(i);
                                int colType = metaData.getColumnType(i);
                                //        System.out.println("Database type for column "+colname+" is "+colType);
                                switch (colType)
                                {
                                    case java.sql.Types.BOOLEAN:
                                    case java.sql.Types.BIT:
                                        logger.debug("  QueryTool: PROCESSING BIT/BOOLEAN COLUMN '{}'", metaData.getColumnLabel(i));
                                        if ("true".equals(colvalue))
                                        {
                                            colvalue = "1";
                                        }
                                        else if ("false".equals(colvalue))
                                        {
                                            colvalue = "0";
                                        }
                                        break;
                                    default:
                                        // nothing for any other kind of field
                                        break;
                                }
                                fieldmap.put(colname, colvalue);
                            }

                            logger.trace("\nPutting { [{}] } in return map", fieldmap);
                            resultList.add(fieldmap);
                        }
                        //System.out.println(" "+rowCount+" Retrieved.");
                    }

                    // Do a little accounting so we can check performance when we want to tune.
                    if (fReportTime)
                    {
                        long endTime = System.currentTimeMillis();
                        long duration = endTime - startTime;
                        long seconds = duration / 1000;
                        long msecs = duration % 1000;
                        //log.info(" " + rowCount + " rows fetched in " + seconds + "." + msecs + " seconds");
                        logger.info(" {} rows fetched in {} milliseconds", rowCount, seconds * 1000 + msecs);
                    }
                }
                catch (SQLException sqe)
                {
                    String sqlState = sqe.getSQLState();

                    logger.info("in 'catch' block for exception [{}], SQLState is [{}]", sqe, sqlState);

                    // magic state prefix "08" == Connection Exception
                    if (attempts < MAX_ATTEMPTS && sqlState.startsWith("08"))
                    {
                        // connection problem, it might be transient so retry
                        retry = true;
                        try { Thread.sleep(250); } catch (InterruptedException ie) {}
                    }
                    else
                    {
                        logger.warn("Query Failed", sqe);
                        logger.warn("  Query was: [{}]", query);
                    }
                }
            } while (retry && attempts++ < MAX_ATTEMPTS);
        }
        finally
        {
            logger.trace("QueryTool.getList() - in 'finally' block.");
            if (results    != null) try { results.close(); results = null; } catch (Exception e) {}
            if (statement  != null) try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}

            //FIXME: This must pass the exception out, so the caller can do something reasonable with it!
            // We can't do anything here because this section is so generic we don't really know what we're doing,
            // so we can't make any sensible attempt at recovery.
        }

        return resultList;
    }
    //getList()


    /**
     * Perform a query and return records mapped to a provided key field.
     * An empty map is returned if a database error occurs or if no records are found;
     * a map is always returned, never <code>null</code>.
     *
     * @param query An SQL query to run
     * @param key The name of the field to use as the map key
     * @param args The arguments to use to set any required query parameters.
     * @return a {@link java.util.LinkedHashMap LinkedHashMap} associating keys to records.
     */
    public LinkedHashMap<String,Map<String,String>> runQuery(String query, String key, String... args)
    {
        return runQuery(MIVConfig.getConfig().getDoTimings()/*true*/, query, key, args);
    }

    /**<p>
     * Perform a query and return records mapped to a provided key field.
     * An empty map is returned if a database error occurs or if no records are found;
     * a map is always returned, never <code>null</code>.</p>
     *<p>
     * A LinkedHashMap is used for the return because the order of the results must
     * be maintained. Callers may assign this to a HashMap or even a simple Map if
     * order is not important.</p>
     *
     * @param fReportTime A true/false flag to indicate if the query should be timed and reported.
     * @param query An SQL query to run
     * @param key The name of the field to use as the map key
     * @param args The arguments to use to set any required query parameters.
     * @return a {@link java.util.LinkedHashMap LinkedHashMap} associating keys to records.
     */
    public LinkedHashMap<String,Map<String,String>> runQuery(boolean fReportTime, String query, String key, String... args)
    {
    // TODO: getList is (mostly) duplicated from runQuery - refactor & combine them.
        /* NOTE: This method was copied out of the SectionFetcher class, which should be altered
         * to use this instead of its own.  Other utilities need to do queries so this method
         * was extracted to be made available as a convenience.
         */
        LinkedHashMap<String, Map<String,String>> m = new LinkedHashMap<String, Map<String,String>>();
        PreparedStatement statement = null;
        ResultSet results = null;
        Connection connection = null;

        /* DON'T DO THIS! This runQuery method is used by MIVConfig and causes an infinite
        // loop if we call back to getConfig() before the constructor is finished.
        //boolean debug = MIVConfig.getConfig().isDebug(); */

        // Just have to set it to true/false manually and recompile (for now)
//        boolean debug = false;
        // ... but it *IS* safe if MIVConfig has completed its first call to getConfig()
//        if (MIVConfig.isInitialized()) debug = MIVConfig.getConfig().isDebug();

//        if (debug) {
//            System.out.println("runQuery length of args is " + (args != null ? args.length : "(null)"));
//            System.out.println("   query is ["+query+"]");
//        }
        logger.debug("runQuery length of args is {}", args != null ? args.length : "(null)");

        boolean retry = false;
        int attempts = 0;

        try {
            do { // loop while retrying
                try
                {
                    connection = getConnection();
                    statement = connection.prepareStatement(query);

                    // Fill in the query parameters from the varargs passed in.
                    int lastPosition = 0;
                    int parmPosition = 0;
                    int count = 0;
                    while ((parmPosition = query.indexOf('?', lastPosition)) != -1)
                    {
//                        if (debug) System.out.println("setting '?' #" + (count + 1) + " to " + args[count]);
                        logger.debug("setting '?' #{} to {}", count + 1, args[count]);
                        statement.setString(count + 1, args[count]);
                        lastPosition = parmPosition + 1;
                        count++;
                    }
                    logger.trace("runQuery: query is [{}]", query);

                    // These 2 vars only used for time accounting
                    // They are within the retry loop because we're interested in how long the query takes,
                    // not in how long all the retries take, so we reset the start time on each iteration.
                    long startTime = System.currentTimeMillis();
                    int rowCount = 0;

                    boolean isResultSet = statement.execute();

                    if (isResultSet) //isResultSet==true means we have a ResultSet, false is an update count
                    {
                        results = statement.getResultSet();

                        ResultSetMetaData metaData = results.getMetaData();
                        int colCount = metaData.getColumnCount();

                        while (results.next())
                        {
                            rowCount++;
                            String reckey = results.getString(key);
                            Map<String, String> fieldmap = new HashMap<String, String>();

                            for (int i = 1; i <= colCount; i++)
                            {
                                String colname = metaData.getColumnLabel(i).toLowerCase();
                                String colvalue = results.getString(i);
                                int colType = metaData.getColumnType(i);
                                //        System.out.println("Database type for column "+colname+" is "+colType);
                                switch (colType)
                                {
                                    case java.sql.Types.BOOLEAN:
                                    case java.sql.Types.BIT:
                                        logger.debug("  QueryTool: PROCESSING BIT/BOOLEAN COLUMN '{}'", metaData.getColumnLabel(i));
                                        if ("true".equals(colvalue))
                                        {
                                            colvalue = "1";
                                        }
                                        else if ("false".equals(colvalue))
                                        {
                                            colvalue = "0";
                                        }
                                        break;
                                    default:
                                        // nothing for any other kind of field
                                        break;
                                }
                                fieldmap.put(colname, colvalue);
                            }

                            //log.finest("\nPutting { [" + reckey + "] [" + fieldmap + "] } in return map");
                            logger.trace("\nPutting { [{}] [{}] } in return map", reckey, fieldmap);
                            m.put(reckey, fieldmap);
                        }
                        //System.out.println(" "+rowCount+" Retrieved.");
                    }

                    // Do a little accounting so we can check performance when we want to tune.
                    if (fReportTime)
                    {
                        long endTime = System.currentTimeMillis();
                        long duration = endTime - startTime;
                        long seconds = duration / 1000;
                        long msecs = duration % 1000;
                        //log.info(" " + rowCount + " rows fetched in " + seconds + "." + msecs + " seconds");
                        logger.info(" {} rows fetched in {} milliseconds", rowCount, seconds * 1000 + msecs);
                    }
                }
                catch (SQLException sqe)
                {
                    logger.info("Showing SQLException (at startup?)", sqe);
                    logger.info("this.source is {}", this.source.toString());

                    String sqlState = sqe.getSQLState();

                    logger.info("in 'catch' block for exception [{}], SQLState is [{}]", sqe, sqlState);

                    // magic state prefix "08" == Connection Exception
                    if (attempts < MAX_ATTEMPTS && (sqlState != null && sqlState.startsWith("08")))
                    {
                        // connection problem, it might be transient so retry
                        retry = true;
                        try { Thread.sleep(250); } catch (InterruptedException ie) {}
                    }
                    else
                    {
                        logger.warn("Query Failed", sqe);
                        logger.warn("  Query was: [{}]", query);
                    }
                }
            } while (retry && attempts++ < MAX_ATTEMPTS);
        }
        finally
        {
            logger.trace("QueryTool.runQuery() - in 'finally' block.");
            if (results    != null) try { results.close(); results = null; } catch (Exception e) {}
            if (statement  != null) try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}

            //FIXME: This must pass the exception out, so the caller can do something reasonable with it!
            // We can't do anything here because this section is so generic we don't really know what we're doing,
            // so we can't make any sensible attempt at recovery.
        }

        return m;
    }
    //runQuery()


    /**
     * <strong>NOTE: Ripped off from RecordHandler.insert</strong>
     * If you make any changes here they may also belong in RecordHandler.insert()
     * TODO: RecordHandler.insert() should be refactored to call this method.
     * @param insertUser
     * @param table
     * @param fields
     * @param values
     * @return
     */
    public int insert(int insertUser, String table, String[] fields, String[] values)
    {
        int newRecordNumber = -1; // record number of newly inserted record
        int insertCount = 0; // temporary(?), while writing/testing
//        boolean debug = MIVConfig.getConfig().isDebug();

        assert fields.length==values.length : "Array lengths differ!";

        if (fields.length != values.length)
        {
            Exception e = new MIVApplicationException("Array lengths differ in QueryTool.insert()!");
            // If this happens it's a programming error
            System.err.println("ERROR in QueryTool.insert on " + table + " table!  Array lengths differ!\n" +
                               "   fields.length==" + fields.length+", values.length==" + values.length);
            logger.error("ERROR in QueryTool.insert on {} table - Array lengths differ!\n" +
                         "   fields.length=={}, values.length=={}", new Object[] { fields.length, values.length });
            logger.error("      Traceback:", e);
            return newRecordNumber;
        }

        // Let's see it for debugging. Remove this loop later.
        if (logger.isDebugEnabled())
        {
            System.out.println("QueryTool.insert: setting fields:");
            for (int i = 0; i < fields.length; i++)
            {
                System.out.println("  " + fields[i] + "==[" + values[i] + "]");
            }
        }

        PreparedStatement statement = null;
        Connection connection = null;


        String insert = "INSERT INTO " + table;
        StringBuilder fieldList = new StringBuilder();
        StringBuilder valueList = new StringBuilder();

        for (String f : fields)
        {
            fieldList.append(f + ",");
            valueList.append("?,");
        }


        // Add the bookkeeping columns to the insert
        fieldList.append("InsertUserID, InsertTimestamp, ");
        valueList.append("?, CURRENT_TIMESTAMP,");
        fieldList.append("UpdateUserID, UpdateTimestamp");
        valueList.append("?, CURRENT_TIMESTAMP");

        if (logger.isDebugEnabled())
        {
            System.out.println("Field List: ("+fieldList+")");
            System.out.println("Value List: ("+valueList+")");
        }

        String cmd = insert + "("+fieldList+") VALUES ("+valueList+")";
        logger.debug("QueryTool.insert: command is [{}]", cmd);

        try
        {
            logger.trace("insert statement is: [{}]", cmd);
            int position = 1;

            //statement = connection.prepareStatement(cmd);
            connection = getConnection();
            statement = connection.prepareStatement(cmd, java.sql.Statement.RETURN_GENERATED_KEYS);

            for (String v : values)
            {
                // convert control and Windows characters to sensible unicode
                // xxxx convert any entity references to actual character values
                v = MIVUtil.translateControlCharacters(v);
                logger.trace("Setting field {} to \"{}\"", position, v);
                statement.setString(position++, v);
            }

            statement.setInt(position++, insertUser); // set the Insert user
            statement.setInt(position++, insertUser); // set the Update user

            insertCount = statement.executeUpdate();
            logger.debug("QueryTool.insert() created {} records in table {}", insertCount, table);
            ResultSet keys = statement.getGeneratedKeys();
            while (keys.next())
            {
                newRecordNumber = keys.getInt(1);
                logger.info/*.debug*/("QueryTool.insert() generated key [{}]", newRecordNumber );
            }

            // Report what was done
            //report(Action.INSERT, insertCount, table, newRecordNumber);
        }
        catch (SQLException sqe)
        {
            logger.warn("RecordHandler failed to insert record in table " + table, sqe);
            sqe.printStackTrace(System.out);
        }
        finally
        {
            logger.trace("QueryTool.runQuery() - in 'finally' block.");
            if (statement  != null) try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}

            //FIXME: This must pass the exception out, so the caller can do something reasonable with it!
            // We can't do anything here because this section is so generic we don't really know what we're doing,
            // so we can't make any sensible attempt at recovery.
        }

        return newRecordNumber;
    }
    //insert()


    /** <strong>NOTE: Ripped off from RecordHandler.update</strong>
     * Do an update but without the RecordHandler's constraint of matching UserID.
     * TODO: push this back into the RecordHandler, and have the "update" that enforces UserID
     * use this, or keep it here and have the RecordHandler use a QueryTool instance and call
     * this method.
     * @param table
     * @param recid
     * @param fields
     * @param values
     * @return number of records updated
     */
    public int update(String table, int recid, String[] fields, String[] values)
    {
        assert fields.length==values.length : "Array lengths differ!";

        int updateCount = 0; // number of records updated

        if (fields.length != values.length)
        {
            Exception e = new MIVApplicationException("Array lengths differ in QueryTool.update()!");
            // If this happens it's a programming error
            System.err.println("ERROR in QueryTool.update for " + table + " table - Array lengths differ!\n" +
                               "   fields.length==" + fields.length + ", values.length==" + values.length);
            logger.error("ERROR in QueryTool.update for {} table - Array lengths differ!\n" +
                         "   fields.length=={}, values.length=={}", new Object[] { table, fields.length, values.length });
            logger.error("      Traceback:", e);
            return updateCount;
        }

        if (logger.isDebugEnabled()) {
            System.out.println("QueryTool.update: setting fields:");
            for (int i = 0; i<fields.length; i++)
            {
                System.out.println("  " + fields[i] + "==[" + values[i] + "]");
            }
        }

        PreparedStatement statement = null;
        Connection connection = null;
        String keyField = "ID";


        logger.debug(" key field {}={}", keyField, recid);

        String update = "UPDATE " + table + " SET ";
        StringBuilder assignments = new StringBuilder();
        String condition = " WHERE " + keyField + " = ?";

        for (String f : fields)
        {
            assignments.append(f + " = ?, ");
        }

        int length = assignments.length();
        assignments.delete(length-2, length-1);

        String cmd = update + assignments.toString() + condition;
        logger.debug("QueryTool.update: command is [{}]", cmd);

        try
        {
            logger.trace("update statement is: [{}]", cmd);

            int position = 1;

            connection = getConnection();
            statement = connection.prepareStatement(cmd);

            for (String v : values)
            {
                logger.trace("Setting field {} to \"{}\"", position, v);
                statement.setString(position++, v);
            }
            statement.setInt(position++, recid);

            updateCount = statement.executeUpdate();

            //report(Action.UPDATE, updateCount, table, recid);

        }
        catch (SQLException sqe)
        {
            //FIXME: This swallows the exception. Caller doesn't know to report that the update failed.
            logger.warn("QueryTool failed to update record #" + recid + " in table " + table, sqe);
            sqe.printStackTrace(System.out);
        }
        finally
        {
            logger.trace("QueryTool.runQuery() - in 'finally' block.");
            if (statement  != null) try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}

            //FIXME: This must pass the exception out, so the caller can do something reasonable with it!
            // We can't do anything here because this section is so generic we don't really know what we're doing,
            // so we can't make any sensible attempt at recovery.
        }

        return updateCount;
    }
    //update()


    /**
     * Execute a delete statement
     * This was added to deal with HArCS cross-reference association records, which the RecordHandler can't handle. (SDP: is this statement correct?)
     * @param query
     * @param args
     * @return
     */
    public int delete(String query, String... args)
    {
        PreparedStatement statement = null;
        Connection connection = null;

        int deleteCount = 0;

        try
        {
            connection = getConnection();
            statement = connection.prepareStatement(query);
            // Fill in the query parameters from the varargs passed in.
            int lastPosition = 0;
            int parmPosition = 0;
            int count = 0;
            while ((parmPosition = query.indexOf('?', lastPosition)) != -1)
            {
                logger.debug("setting '?' #{} to {}", count + 1, args[count]);
                statement.setString(count + 1, args[count]);
                lastPosition = parmPosition + 1;
                count++;
            }
            logger.trace("getList: query is [{}]", query);
            deleteCount = statement.executeUpdate();
        }
        catch (SQLException sqe)
        {
            logger.warn("QueryTool failed to delete record", sqe);
        }
        finally
        {
            logger.trace("QueryTool.delete() - in 'finally' block.");
            if (statement != null) try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }
        return deleteCount;
    }


    /** Cache for table column lookup */
    private static final Map<String,List<String>> tableColumnMap = new HashMap<String,List<String>>();


    /**<p>
     * Get the names of the columns in a table. Used to verify that data from a form
     * has a corresponding column in a table, and exclude it if not. This prevents
     * SQL exceptions from occurring when utilitarian hidden input fields are on a
     * for to assist processing, but don't belong in the database.</p>
     * <p>Caches the looked-up information to avoid repeated database access.</p>
     * @param tableName name of the table to get column information for
     * @return a List of all column names in the table.
     */
    public List<String> getTableColumns(String tableName)
    {
        List<String> columnNames = tableColumnMap.get(tableName);

        if (columnNames == null)
        {
            logger.debug("Looking up column information for table [{}]", tableName);
            columnNames = getTableColumnsInternal(tableName);
            tableColumnMap.put(tableName, columnNames);
        }
        else
        {
            logger.debug("Returning cached column information for table [{}]", tableName);
        }

        return columnNames;
    }


    /**
     * FIXME: Needs Javadoc
     *
     * @param tableName
     * @return
     */
    private List<String> getTableColumnsInternal(String tableName)
    {
        PreparedStatement statement = null;
        Connection connection = null;
        ResultSet resultset = null;
        ResultSetMetaData resMetadata = null;

        List<String> columnNames = null;

        // Query returns nothing but gets us access to the column metadata
        String query = "SELECT * FROM " + tableName + " WHERE 1=0";

        try
        {
            connection = getConnection();
            statement = connection.prepareStatement(query);
            resultset = statement.executeQuery();
            if (resultset != null)
            {
                columnNames = new ArrayList<String>();

                resMetadata = resultset.getMetaData();
                int columnCount = resMetadata.getColumnCount();
                for (int i = 1; i <= columnCount; i++)
                {
                    columnNames.add(resMetadata.getColumnLabel(i).toLowerCase());
                }
            }
        }
        catch (SQLException sqe)
        {
            logger.warn("QueryTool failed to get table metadata for: " + tableName, sqe);
        }
        finally
        {
            if (statement != null) try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        logger.debug("Columns for table {} :: {}", tableName, columnNames);
        return columnNames;
    }


    /**
     * A note to display during finalization. This can help debug where the QueryTool was created
     * and why it's being finalized.
     */
    private String note = "(no note set)";
    /** Set a note to be displayed when this instance is finalized. @param note The note text */
    public void setNote(CharSequence note)
    {
        this.note = note.toString();
    }



    @Override
    protected void finalize() throws Throwable
    {
        System.out.println("Releasing QueryTool during finalization. (" + note + ") [" + this + "]");
        try {
//            this.close();
            this.source = null;
        }
        finally {
            super.finalize();
        }
    }

}
