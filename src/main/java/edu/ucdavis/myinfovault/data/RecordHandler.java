/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RecordHandler.java
 */

package edu.ucdavis.myinfovault.data;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MarkerFactory;
import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.message.MivConstants;

/**
 * Handle fetching, updating, and deleting a single record with a known record ID.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class RecordHandler
{
    private static final Logger logger = LoggerFactory.getLogger(RecordHandler.class);

    private static /*final*/ String UPDATE_MASTER_DATA_TIME = MivConstants.getInstance().getString("UPDATE_MASTER_DATA_TIME");
    private final int contentMaxLimit = Integer.parseInt(MivConstants.getInstance().getString("WYSIWYG.CONTENT.MAX_LIMIT"));

    private final QueryTool qt = MIVConfig.getConfig().getQueryTool();

    private static final ThreadLocal<DateFormat> displaydf =
                new ThreadLocal<DateFormat>() {
                    @Override protected DateFormat initialValue() {
                        return new SimpleDateFormat(MIVConfig.getConfig().getProperty("default-config-displaydateformat"));
                    }
                };

    DataSource source = null;

    int userID = -1;
    private MIVUserInfo user = null;


    /**
     * Create a RecordHandler tied to the given user.
     * @param mui User information for...
     */
    public RecordHandler(MIVUserInfo mui)
    {
        this.user = mui;
        this.userID = mui.getPerson().getUserId();
        this.source = getDataSource();
    }



    /**
     * Gets one record based on its unique ID.
     * Assumes a unique key with column named "ID" and user ID column named "UserID" are in the table.
     * @param table Name of the table get a record from.
     * @param recid Unique record ID number, in the "ID" column.
     * @return A Map of field-name to value pairs.
     */
    public Map<String,String> get(String table, String recid)
    {
        int recordID = Integer.parseInt(recid);
        return get(table, recordID);
    }


    /**
     * Gets one record based on its unique ID.
     * Assumes a unique key with column named "ID" and user ID column named "UserID" are in the table.
     * @param table Name of the table get a record from.
     * @param recid Unique record ID number, in the "ID" column.
     * @return A Map of field-name to value pairs.
     */
    public Map<String,String> get(String table, int recid)
    {
        String get = "SELECT * FROM " + table +
                     " WHERE UserID = ? AND ID = ?";


        Map<String,String> fieldmap = new HashMap<String,String>();
        PreparedStatement statement = null;
        ResultSet results = null;
        Connection connection = null;

        try {
            logger.debug("query [{}]", get);

            connection = getConnection();
            statement = connection.prepareStatement(get);
            statement.setInt(1, this.userID);
            statement.setInt(2, recid);

            results = statement.executeQuery();

            if (results.next())
            {
                ResultSetMetaData metaData = results.getMetaData();
                int colCount = metaData.getColumnCount();

                fieldmap = new HashMap<String,String>();
                for (int i = 1; i <= colCount; i++)
                {
                    String colname = metaData.getColumnLabel(i).toLowerCase();
                    String colvalue;

                    // SDP: Booleans are coming *out* of the database as 1/0 on some
                    //      systems, but as true/false on others. We treat things as
                    //      strings and updating a boolean with the string "true"
                    //      makes it '0'/false.  This block makes sure that our
                    //      booleans always are presented to the client UI as 1/0
                    //      instead of true/false
                    int colType = metaData.getColumnType(i);
                    //        System.out.println("Database type for column "+colname+" is "+colType);
                    if (colType == java.sql.Types.DATE) {
                        java.sql.Date d = results.getDate(i);
                        colvalue = (d != null ? d.toString() : null);
                    }
                    else {
                        colvalue = results.getString(i);
                    }

                    switch (colType)
                    {
                        case java.sql.Types.BOOLEAN:
                        case java.sql.Types.BIT:
                            logger.debug("  RecordHandler: PROCESSING BIT/BOOLEAN COLUMN '{}'", metaData.getColumnLabel(i));
                            if ("true".equals(colvalue))
                            {
                                colvalue = "1";
                            }
                            else if ("false".equals(colvalue))
                            {
                                colvalue = "0";
                            }
                            break;
                        case java.sql.Types.VARCHAR:
                        case java.sql.Types.LONGVARCHAR:
                            if (colvalue != null) {
                                colvalue = MIVUtil.translateControlCharacters(colvalue);
                            }
                            break;
                        case java.sql.Types.DATE:
                            logger.debug("  -- Checking a DATE column on GET");

                            if (colvalue != null)
                            {
                                if (colvalue.trim().length() == 0 || colvalue.equals("0000-00-00"))
                                {
                                    colvalue = "";
                                }
                                else
                                // parse date into miv standard format
                                {
                                    colvalue = displaydf.get().format(results.getDate(i));
                                }
                            }
                            break;
                        default:
                            // nothing for any other kind of field
                            break;
                    }

                    // Ensure no null strings are returned
                    colvalue = (colvalue != null ? colvalue : "");
                    fieldmap.put(colname, colvalue);
                }
                report(Action.GET, 1, table, recid);
            }
        }
        catch (SQLException sqe) {
            logger.error("RecordHandler failed to get record #"+recid+" from table " + table, sqe);
        }
        finally {
            if (results != null)    try { results.close();   results = null;   } catch (Exception e) {}
            if (statement != null)  try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        return fieldmap;
    }
    //get()


    /**
     * Gets one section record based on type of record, section and recordID.
     * Fatching data from Saction table.
     * @param rectype type of the record
     * @param section name of the section
     * @param recid Unique record ID number.
     * @return A Map of field-name to value pairs.
     */
    public Map<String, String> getSectionRecord(String rectype, String section, String recid)
    {
        Map<String, String> sectionMap = null;
        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        SectionFetcher sf = this.user.getSectionFetcher();

        Map<String, String> sectionQueryMap = sf.getSectionQueryMap(rectype + "-record", section);

        if (sectionQueryMap.get("selectfields") != null)
        {
            String whereclause = sectionQueryMap.get("whereclause");
            if (sectionQueryMap.get("sectionprimarykeyid") != null
                    && sectionQueryMap.get("sectionprimarykeyid").length() > 0)
            {
                whereclause += " AND " + sectionQueryMap.get("sectionprimarykeyid") + "=? ";
            }
            else
            {
                whereclause += " AND  ID=? ";
            }

            String query = Fetcher.buildQuery(sectionQueryMap.get("selectfields"), sectionQueryMap.get("fromtable"),
                                         whereclause, "", null);
            List<Map<String, String>> dataList = qt.getList(query, String.valueOf(this.userID), recid);

            if (dataList != null)
            {
                sectionMap = dataList.get(0);
            }
        }
        return sectionMap;
    }


    /**
     * Add a new record in a table.
     * NOTE: This method has been largely duplicated as QueryTool.insert()
     * If you make any changes here they may also belong in QueryTool.insert()
     * TODO: RecordHandler.insert() should be refactored to call QueryTool.insert()
     *
     * @param table Name of the table to add a record to.
     * @param fields Array of field names to insert.
     * @param values Array of field values to match field names.
     * @return The unique key ID of the inserted record, or <code>-1</code> if the insert failed.
     */
    public int insert(String table, String[] fields, String[] values)
    {
        int newRecordNumber = -1; // record number of newly inserted record
        int insertCount = 0; // temporary(?), while writing/testing

        assert fields.length==values.length : "Array lengths differ!";

        if (fields.length != values.length)
        {
            // If this happens it's a programming error
            System.err.println("ERROR in RecordHandler.insert!  Array lengths differ!\n"+
                               "   fields.length=="+fields.length+", values.length=="+values.length);
            logger.error("RecordHandler.insert - Array lengths differ!\n" +
                         "    fields.length=={}, values.length=={}", fields.length, values.length);
            return newRecordNumber;
        }

        // Let's see it for debugging. Remove this loop later.
        System.out.println("RecordHandler.insert: setting fields:");

        if (!validateContentLimit(fields, values)) {
            // MySql ERROR Code - 1406 : Data truncation: Data too long
            return Integer.parseInt(MivConstants.getInstance().getString("MYSql.ErrorCode.DataTruncation"));
        }

        PreparedStatement statement = null;
        String userIdField = "UserID";


        int insertUser = -1;
        MIVUser u = this.user.getLoginUser();
        if (u != null) {
            insertUser = this.user.getLoginUser().getUserID();
        }
        else {
            insertUser = this.user.getPerson().getUserId();
        }

        String insert = "INSERT INTO "+table;
        StringBuilder fieldList = new StringBuilder();
        StringBuilder valueList = new StringBuilder();

        fieldList.append(userIdField+",");
        valueList.append(this.userID+",");

        List<String> columns = qt.getTableColumns(table);

        // set updated columns
        for (String f : fields)
        {
            // check the valid columns for the table
            if (columns.contains(f.toLowerCase()))
            {
                fieldList.append(f+",");
                valueList.append("?,");
            }
        }

/*
//Tests.....
     // A table that has no Sequence column
        System.out.println("   TEST:   No Sequence column");
        this.getNextSequence("CvJournalsHTML");
     // A non-existent table
        System.out.println("   TEST:   Bad Table name");
        this.getNextSequence("NoTableHere");
*/

        // Try to get the next sequence number, and add it to the insert
        // statement if we get a valid one.
        int nextSequence = this.getNextSequence(table);
        if (nextSequence > 0)
        {
            fieldList.append("Sequence, ");
            valueList.append(nextSequence+",");
        }

        // Add the bookkeeping columns to the insert
        fieldList.append("InsertUserID, InsertTimestamp, ");
        valueList.append("?,CURRENT_TIMESTAMP,");
        fieldList.append("UpdateUserID, UpdateTimestamp");
        valueList.append("?,CURRENT_TIMESTAMP");

        System.out.println("Field List: ("+fieldList+")");
        System.out.println("Value List: ("+valueList+")");

        String cmd = insert + "("+fieldList+") VALUES ("+valueList+")";
        System.out.println("RecordHandler.insert: command is ["+cmd+"]");

        Connection connection = null;

        try {
            logger.debug("insert statement is: [{}]", cmd);

            connection = getConnection();

            int position = 1;
            int paramIndex = 1;
            Map<String,Integer> columnTypes = getColumnTypeMap(table, connection);

            statement = connection.prepareStatement(cmd, java.sql.Statement.RETURN_GENERATED_KEYS);

            for (String v : values)
            {
                String fieldName = fields[position-1];

                // check the valid columns for the table
                if (!columns.contains(fieldName.toLowerCase()))
                {
                    logger.debug("{} :: Skip column", fieldName);
                    position++;
                    continue;
                }

                Integer columnType = columnTypes.get(fieldName.toLowerCase());

                logger.debug("Setting field [{}] which is type [{}] in position [{}] to value [{}]", new Object[] { fieldName, columnType, position, v });

                v = fixFieldValue(v, columnType);

                logger.debug("Setting field {} to \"{}\"", paramIndex, v);
                statement.setString(paramIndex++, v);
                position++;
            }

            statement.setInt(paramIndex++, insertUser); // set the Insert user
            statement.setInt(paramIndex++, insertUser); // set the Update user

            insertCount = statement.executeUpdate();
            ResultSet keys = statement.getGeneratedKeys();
            while (keys.next())
            {
                newRecordNumber = keys.getInt(1);
                System.out.println("RecordHandler.insert() generated key ["+newRecordNumber+"]");
            }

            // Report what was done
            report(Action.INSERT, insertCount, table, newRecordNumber);
        }
        catch (SQLException sqe) {
            logger.error("RecordHandler failed to insert record in table " + table, sqe);
        }
        finally {
            if (statement != null)  try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        if (newRecordNumber > 0) this.setUpdateTime(Action.INSERT);

        return newRecordNumber;
    }
    //insert()


    /**
     * Update the record with the specified ID.
     * @param table Name of the table to update a record in.
     * @param recid Unique record ID number, in the "ID" column.
     * @param fields Array of field names to update.
     * @param values Array of field values to match field names.
     * @return the number of records updated, which <em>should</em> always be 0 or 1.
     */
    public int update(String table, int recid, String[] fields, String[] values)
    {
        assert fields.length==values.length : "Array lengths differ!";

        logger.trace("update( table, recid, fields, values )");
        int updateCount = 0; // number of records updated

        if (fields.length != values.length)
        {
            // If this happens it's a programming error
            System.err.println("ERROR in RecordHandler.update!  Array lengths differ!\n"+
                               "   fields.length=="+fields.length+", values.length=="+values.length);
            logger.error("RecordHandler.insert - Array lengths differ!\n" +
                         "    fields.length=={}, values.length=={}", fields.length, values.length);
            return updateCount;
        }


        if (!validateContentLimit(fields, values)) {
            // MySql ERROR Code - 1406 : Data truncation: Data too long
            return Integer.parseInt(MivConstants.getInstance().getString("MySql.ErrorCode.DataTruncation"));
        }


        PreparedStatement statement = null;
        String keyField = "ID";
        String userIdField = "UserID";
        String update = "UPDATE " + table + " SET ";

        StringBuilder assignments = new StringBuilder();
        /* Checking 'UserID=?' along with ID is redundant since ID is unique,
         * but doing so provides a safety so a given user can't accidentally
         * (or maliciously) change a record for a different user.         */
        String condition = " WHERE " + keyField + " = ? AND " + userIdField + " = ?";

/* TODO: Document it -- Why was "getTableColumns()" added, and why is the "check the valid columns"
 *       being done before adding the field to the 'assignments' string?  This didn't have to be
 *       done before, because there was no way it could go wrong.  I assume that's changed in some
 *       way, so how can it go wrong now, why does this check have to be made?
 */
        List<String> columns = qt.getTableColumns(table);

        // set updated columns
        for (String f : fields)
        {
            // check the valid columns for the table
            if (columns.contains(f.toLowerCase()))
            {
                assignments.append(f + " = ?, ");
            }
        }

        // Add the audit columns and values.
        assignments.append("UpdateUserID = ?, UpdateTimestamp = current_timestamp");

        String cmd = update + assignments.toString() + condition;
        logger.debug("RecordHandler.update: command is [{}]", cmd);

        Connection connection = null;

        try {
            logger.debug("update statement is: [{}]", cmd);

            //checkConnect();

            connection = getConnection();
            int updateUser = this.user.getLoginUser().getUserID();
            int position = 1;
            int paramIndex = 1;

            Map<String,Integer> columnTypes = getColumnTypeMap(table, connection);

            statement = connection.prepareStatement(cmd);

            for (String v : values)
            {
                String fieldName = fields[position-1];

                // check the valid columns for the table
                if (!columns.contains(fieldName.toLowerCase()))
                {
                    logger.debug("{} :: Skip column", fieldName);
                    position++;
                    continue;
                }

                Integer columnType = columnTypes.get(fieldName.toLowerCase());

                logger.debug("Setting field [{}] which is type [{}] in position [{}] to value [{}]", new Object[] { fieldName, columnType, position, v });

                v = fixFieldValue(v, columnType);
                //log.finer/*info*/("Setting field "+position+" to \""+v+"\"");
                logger.debug("Setting field {} to \"{}\"", paramIndex, v);
                statement.setString(paramIndex++, v);
                position++;
            }

            statement.setInt(paramIndex++, updateUser);

            statement.setInt(paramIndex++, recid);
            statement.setInt(paramIndex++, this.userID);

            updateCount = statement.executeUpdate();

            report(Action.UPDATE, updateCount, table, recid);

        }
        catch (SQLException sqe) {
            //FIXME: This swallows the exception. Caller doesn't know to report that the update failed.
            logger.error("RecordHandler failed to update record #" + recid + " in table " + table, sqe);
        }
        finally {
            if (statement != null)  try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        if (updateCount > 0) this.setUpdateTime(Action.UPDATE);

        return updateCount;
    }
    //update()


    /**
     * Update the record with the specified script and values.
     * @param script - update script
     * @param values Array of field values to match field names.
     * @return the number of records updated
     */
    public int update(String script, String... values)
    {
        int fieldsLength = StringUtils.countOccurrencesOf(script, "?");

        assert values.length == fieldsLength : "Array lengths differ!";

        logger.trace("update( script, values )");
        int updateCount = 0; // number of records updated

        if (fieldsLength != values.length)
        {
            // If this happens it's a programming error
            System.err.println("ERROR in RecordHandler.update!  Array lengths differ!\n"+
                               "   fields.length=="+fieldsLength+", values.length=="+values.length);
            logger.error("RecordHandler.insert - Array lengths differ!\n" +
                         "    fields.length=={}, values.length=={}", fieldsLength, values.length);
            return updateCount;
        }

        PreparedStatement statement = null;
        String auditFields = "UpdateUserID = ?, UpdateTimestamp = current_timestamp";
        script = script.replaceFirst("(?i)Set", "SET "+auditFields+", ");

        logger.debug("RecordHandler.update: command is [{}]", script);

        Connection connection = null;

        try {
            logger.debug("update statement is: [{}]", script);

            connection = getConnection();
            int updateUser = this.user.getLoginUser().getUserID();
            int paramIndex = 1;

            statement = connection.prepareStatement(script);

            // Setting value for audit param, which is always at first position
            statement.setInt(paramIndex++, updateUser);
            for (String v : values)
            {
                statement.setString(paramIndex++, v);
            }

            updateCount = statement.executeUpdate();
        }
        catch (SQLException sqe) {
            //FIXME: This swallows the exception. Caller doesn't know to report that the update failed.
            logger.error("RecordHandler failed to update record for command " + script, sqe);
        }
        finally {
            if (statement != null)  try { statement.close(); statement = null; } catch (SQLException e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        if (updateCount > 0) this.setUpdateTime(Action.UPDATE);

        return updateCount;
    }


    /**
     * Delete the record with the specified ID.
     * The record with the given ID is only deleted if it is owned by the user the RecordHandler belongs to.
     * Assumes a unique key with column named "ID" and user ID column named "UserID" are in the table.
     * @param table Name of the table to delete record from.
     * @param recid Unique record ID number, in the "ID" column.
     * @return the number of records deleted, which <em>should</em> always be 0 or 1.
     */
    public int delete(String table, int recid)
    {
        PreparedStatement statement = null;
        String keyField = "ID";
        String userIdField = "UserID";
        boolean useTempDelete = false;  // change to true or false to use the temp 'mark as deleted' or the real delete.

        /* Checking 'UserID=?' along with ID is redundant since ID is unique,
         * but doing so provides a safety so a given user can't accidentally
         * delete a record for a different user.         */
        String del = "DELETE FROM " + table +
                     " WHERE " + keyField + " = ? AND " + userIdField + " = ?"
        ;
        String tmpdel = "UPDATE "+table+
                       " SET "+keyField+" = ?, "+userIdField+" = ?, UpdateUserID = ?, UpdateTimestamp = current_timestamp"+
                       " WHERE "+keyField+" = ? AND "+userIdField+" = ?"
        ;

        if (useTempDelete) del = tmpdel;

        //int newRecid = Integer.parseInt( "909"+Integer.toString(recid) );
        int newRecid = -recid;

        int count = 0;

        Connection connection = null;

        try {
            logger.debug("delete statement is: [{}]", del);

            connection = getConnection();
            int updateUser = this.user.getLoginUser().getUserID();

            statement = connection.prepareStatement(del);
            if (useTempDelete)
            {
                // These are the parms for marking as deleted, rather than deleting.
                statement.setInt(1, newRecid);
                statement.setInt(2, -this.userID);
                statement.setInt(3, updateUser);
                statement.setInt(4, recid);
                statement.setInt(5, this.userID);
            }
            else
            {
                // Only these two are needed when we switch to real deletion.
                statement.setInt(1, recid);
                statement.setInt(2, this.userID);
            }

            count = statement.executeUpdate();
            report(Action.DELETE, count, table, recid);
        }
        catch (SQLException sqe) {
            logger.error("RecordHandler failed to delete record #" + recid + " from table " + table, sqe);
        }
        finally {
            if (statement != null)  try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        if (count > 0) this.setUpdateTime(Action.DELETE);

        return count;
    }
    //delete()


    /**
     * Validate content limit
     * @param fields
     * @param values
     * @return
     */
    private boolean validateContentLimit(String[] fields, String[] values)
    {
        for (String field : fields)
        {
            int count = 0;
            if (field.trim().equals("content"))
            {
                int contentSize = StringUtil.getSizeInBytes(values[count++]);
                if (contentSize > contentMaxLimit) {
                    return false;
                }
            }
        }
        return true;
//        for (int i = 0; i<fields.length; i++)
//        {
//            if (fields[i].toString().trim().equals("content"))
//            {
//                int contentSize = StringUtil.getSizeInBytes(values[i]);
//                if (contentSize > contentMaxLimit) {
//                    return false;
//                }
//            }
//        }
//        return true;
    }


    /**
     * Data type storage for table metadata.
     * Used by update() to twiddle the String data field values passed in for certain types of columns.
     */
    private static final Map<String,Map<String,Integer>> tableColumnTypeMap = new HashMap<String,Map<String,Integer>>();

    /**
     * @param table
     * @param connection
     * @throws SQLException
     */
    private Map<String,Integer> getColumnTypeMap(String table, Connection connection) throws SQLException
    {
        Map<String,Integer> columnTypeMap = tableColumnTypeMap.get(table);

        // Fetch the table metadata if we don't have it yet
        if (columnTypeMap == null)
        {
            if (logger.isDebugEnabled()) {
                System.out.println(" - Loading Column Type Map for table ["+table+"]");
            }
            DatabaseMetaData meta = connection.getMetaData();
            ResultSet rsColumns = meta.getColumns(null, null, table, null);

            columnTypeMap = new HashMap<String,Integer>();
            while (rsColumns.next())
            {
                String columnName = rsColumns.getString("COLUMN_NAME").toLowerCase();
                int dataType = rsColumns.getInt("DATA_TYPE");
                columnTypeMap.put(columnName, dataType);

           // for debugging
                if (logger.isDebugEnabled())
                {
                    String typeName = rsColumns.getString("TYPE_NAME");
                    int nullability = rsColumns.getInt("NULLABLE");
                    System.out.println(" - - - Column [" + columnName + "] is type [" + typeName +
                                       "] which has nullable value [" + nullability + "] --> " +
                                       (nullability == DatabaseMetaData.columnNoNulls ? " column NOT nullable" :
                                        nullability == DatabaseMetaData.columnNullable ? " column NULLABLE" : " unknown nullability")
                                      );
                    if (dataType == Types.DATE) System.out.println("     - - got a date");
                }
            }
            tableColumnTypeMap.put(table, columnTypeMap);
        }
        else {
            if (logger.isDebugEnabled()) {
                System.out.println(" - Returning cached Column Type Map for table ["+table+"]");
            }
        }

        return columnTypeMap;
    }


    /**
     * Correct the value of a field so it can be safely stored in the type of database column given.
     * @param fieldValue proposed new value for a field
     * @param columnType the java.sql.Types of the column in which this value will be stored.
     * @return the new value for the field altered, if necessary, to be safely stored in the given column type.
     */
    private String fixFieldValue(String fieldValue, Integer columnType)
    {
        if (fieldValue != null)
        {
            // Twiddle with the new field value as needed, based on the column Type
            switch (columnType)
            {
                case Types.DATE:
                    if (fieldValue.trim().equals("") || fieldValue.equals("0000-00-00")) fieldValue = null;
                    break;
            }

            // convert control and Windows characters to sensible unicode
            fieldValue = MIVUtil.translateControlCharacters(fieldValue);
        }
        return fieldValue;
    }


    /**
     * Set the master Time That This User's Data was Last Touched.
     */
    private int setUpdateTime(Action action)
    {
        int updateCount = 0;

        // set the UserAccount master data update timestamp to CURRENT_TIMESTAMP where ua.UserID=this.userID
        logger.info("{}: Setting the UpdateTimestamp TO {}", action.toString(), new java.util.Date());

        try ( Connection con = getConnection();
              PreparedStatement ps = con.prepareStatement(UPDATE_MASTER_DATA_TIME) ) {
            ps.setInt(1, this.userID);
            updateCount = ps.executeUpdate();
            con.commit();
//            try { this.releaseConnection(con); } catch (SQLException e) {}
            if (updateCount != 1) {
                logger.warn("{} timestamp records updated instead of 1", updateCount);
            }
        }
        catch (SQLException e) {
            logger.warn("Failed to set the master data update time", e);
        }

        return updateCount;
    }


    /**
     * Get the DataSource that Connections will use.
     * @return The DataSource
     */
    private DataSource getDataSource()
    {
        DataSource ds = null;
        if (MIVConfig.isInitialized())
        {
            ds = MIVConfig.getConfig().getDataSource();
            if (ds == null)
            {
                logger.error("RecordHandler failed to retrieve the dataSource");
            }
        }
        return ds;
    }


    /**
     * Get a new connection from the pool
     * @return connection
     * @throws SQLException
     */
    protected Connection getConnection() throws SQLException
    {
        if (this.source == null) this.source = this.getDataSource();
        Connection connection = null;
        logger.debug("Getting new connection");
        connection = source.getConnection();
        return connection;
    }


    /**
     * Release connection from the pool
     * @throws SQLException
     */
    protected void releaseConnection(Connection connection) throws SQLException // FIXME: This should be private or protected
    {
        logger.debug("Releasing connection");
        if (connection != null)
        {
            connection.commit();
            connection.close();
            connection = null;
        }
    }


    /**
     * Find the next Sequence number to use in a table.
     * If the table doesn't exist or there is no Sequence number column a value
     * of -1 is returned; the call shouldn't try to insert a sequence number.
     * Used internally by insert().
     * @param table Name of the table
     * @return An available sequence number, or -1 to indicate nothing appropriate.
     */
    private int getNextSequence(String table)
    {
        int seq = -1;

        String query = "SELECT MAX(Sequence) FROM "+table+" WHERE UserID=?";

        PreparedStatement statement = null;
        ResultSet results = null;

        Connection connection = null;

        try
        {
            connection = getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, this.userID);
            results = statement.executeQuery();
            if (results.next())
            {
                seq = results.getInt(1);
                seq += 10;
            }
        }
        catch (SQLException e)
        {
            //table doesn't exist or there is no Sequence number column a value of -1 is returned
            seq = -1;
            // Ignore the exception, allow the default (10) to be returned
            //e.printStackTrace();
        }
        finally
        {
            if (results != null)    try { results.close();   results = null;   } catch (Exception e) {}
            if (statement != null)  try { statement.close(); statement = null; } catch (Exception e) {}
            try { this.releaseConnection(connection); } catch (SQLException e) {}
        }

        logger.debug("Returning {} from getNextSequence()", seq);
        return seq;
    }



    private enum Action
    {
        GET("got", "from"),
        INSERT("inserted", "in"),
        UPDATE("updated", "in"),
        DELETE("deleted", "from");

        String verb = null;
        String location = null;

        Action(String verb, String location)
        {
            this.verb = verb;
            this.location = location;
        }
    }
    /** TODO: Move this method out to an "Audit" class, which can log like this does now
     * and later expand to write records in an audit table.
     */
    private void report(Action action, int count, String table, int recid)
    {
        int actor = user.getLoginUser().getUserID();
        int role = user.getPerson().getUserId();
        String roleName = "";
        if (actor != role) {
            roleName = " for user "+role;
        }

        logger.info(MarkerFactory.getDetachedMarker("_AUDIT"),
                    "_AUDIT : User {} {} {} record(s) {} {} with ID {}{}",
                    new Object[] { actor, action.verb, count, action.location, table, recid, roleName } );
    }


    @Override
    protected void finalize() throws Throwable
    {
        //System.out.println("Releasing RecordHandler for user "+userID+" during finalization. ["+this+"]");
        try {
 //           this.close();
            this.source = null;
        }
        finally {
            super.finalize();
        }
    }
}
