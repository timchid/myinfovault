package edu.ucdavis.myinfovault.data;

import java.util.List;

public class TableSnapshotData
{
    public String recordName;
    public List recordIdList;
    public String columnNames;
    public String sourceTable;
    public String snapshotTable;

    public TableSnapshotData(String recordName, List recordIdList, String columnNames, String sourceTable, String snapshotTable)
    {
        this.recordName = recordName;
        this.recordIdList = recordIdList;
        this.columnNames = columnNames;
        this.sourceTable = sourceTable;
        this.snapshotTable = snapshotTable;
    }
}
