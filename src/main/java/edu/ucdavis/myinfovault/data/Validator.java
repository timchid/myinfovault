/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Validator.java
 */

package edu.ucdavis.myinfovault.data;

import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.exception.OutOfRangeException;
import edu.ucdavis.myinfovault.data.exception.ValidationException;


/**<p>
 * Validates the field data against the constraints passed to it.
 * </p>
 * <p>
 * This can be used two ways
 * <ol>
 * <li>Create a Validator with a set of Constraints that can be used to repeatedly validate against those constraints.</li>
 * <li>Create a Validator and validate several fields, each against its own constraints, building up a list of errors.</li>
 * </ol>
 * </p>
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class Validator
{
    private static final Logger logger = LoggerFactory.getLogger(Validator.class);

    private String[] constraints = {};
    private Map <String, String[]> reqData = null;
    private boolean valid = true;
    private List<String> errorFields = new ArrayList<String>();
    private List<String> errorMsgs = new ArrayList<String>();
    private static Gson gson = new Gson();


    public Validator()
    {
    }

    public Validator(Map <String, String[]> reqData)
    {
        this.reqData = reqData;
    }

    public Validator(String constraints)
    {
        if (constraints == null || constraints.length() == 0) {
            throw new IllegalArgumentException("list of constraints can not be empty");
        }

        String[] c = constraints.split("\\s*[,]\\s*");
        this.constraints = c;
    }

    public Validator(String... constraints)
    {
        if (constraints == null || constraints.length == 0) {
            throw new IllegalArgumentException("list of constraints can not be empty");
        }

        this.constraints = constraints;
    }


    public String validate(String fname, String value)
    {
        return validate(fname, value, this.constraints);
    }


    public String validate(String fname, String value, String constraints)
    {
        return validate(fname, value, constraints.split("\\s*[,]\\s*"));
    }


    public String validate(String fname, String value, String[] constraints)
    {
        for (String c : constraints)
        {
            // Break the constraint into its Type and its Params
            // e.g. "float:9999.99"  "length:9"  "int:1:100"  "int::1000"
            // Note the first array element will be the Type itself
//            String[] params = c.split(":");
            String[] params = c.split("\\s*:\\s*"); // trying out optional whitespace so args[] don't have to be trim()med
            Constraints constraint = Constraints.INVALID;
            try {
                constraint = Constraints.valueOf(params[0].toUpperCase());
                value = constraint.validate(this.reqData, fname, value, params);
            }
            catch (ValidationException e) {
                //System.out.println("Caught ValidationException [" + e + "]");
                logger.debug("Caught ValidationException", e);
                value = e.getValue();
                this.addError(e.getField(), e.getMessage());
            }
            // The enum .valueOf() operation throws IllegalArgumentException
            // if the string passed doesn't match an enum value. In that case,
            // the constraint we're looking at could be a number, indicating
            // maximum field length. If it's not a number it's an error.
            catch (IllegalArgumentException iae)
            {
                //System.out.println("Caught IllegalArgumentException [" + e + "]");
                // if this isn't a number (a max-length) log a warning that an invalid constraint was specified
                try {
                    int maxlength = new Integer(c).intValue();
                    //System.out.println("Constraint is [" + c + "] Checking field \"" + fname + "\" for max length of " + maxlength);
                    logger.debug("Constraint is [{}] Checking field \"{}\" for max length of {}", new Object[] { c, fname, maxlength });
                    if (value != null && value.length() > maxlength)
                    {
                        String s = "Too many characters have been added to this field (" + maxlength + " characters allowed).";
                        this.addError(fname, s);
                    }
                }
                catch (NumberFormatException nfe) {
                    String s = "An invalid constraint \"" + c + "\" has been placed on this field. Please notify the miv-help list.<br>"
                             + "Please copy this message and email it to miv-help@ucdavis.edu";
                    this.addError(fname, s);
                }
                catch (Exception e) {
                    String s = "An Internal Error occurred trying to validate the field \"" + fname + "\" against the constraint \"" + c + "\"<br>"
                             + "Please copy this message and email it to miv-help@ucdavis.edu";
                    this.addError(fname, s);
                    logger.error(s, e);
                }
            }
        }

        return value;
    }


    /**
     * Get the status of this validation: it is either valid (<code>true</code>) or invalid (<code>false</code>)
     * @return <code>true</code> if the current validation is considered valid.
     */
    public boolean isValid()
    {
        return this.valid;
    }


    /**
     * Returns the error fields.
     *
     * @return Error fields as a JSON string
     */
    public String getErrors()
    {
        return gson.toJson(this.errorFields);
    }


    /**
     * Returns the error messages.
     *
     * @return Error Messages as a JSON string
     */
    public String getErrorMsgs()
    {
        return gson.toJson(this.errorMsgs);
    }

    /**
     * Return list of error fields
     * @return
     */
    public List<String> getErrorFieldList()
    {
        return this.errorFields;
    }

    /**
     * Return list of error messages
     * @return
     */
    public List<String> getErrorMessageList()
    {
        return this.errorMsgs;
    }

    /**
     * Reset the validator. Clears the messages and the error fields of the prior validation.
     */
    public void reset()
    {
        this.valid = true;
        this.errorFields.clear();
        this.errorMsgs.clear();
    }


    /**
     * Add error
     *
     * @param field
     * @param errormsg
     */
    private void addError(String field, String errormsg)
    {
        this.valid = false;
        this.errorFields.add(field);
        this.errorMsgs.add(errormsg);
    }

    /**<p>
     * Defines the types of constraints that can be enforced.
     * </p>
     * <p>In general, a constraint will have the form <code>constraint[:param]*</code> that is,
     * the constraint name followed by zero or more constraint parameters, each introduced
     * by a colon.
     * </p>
     * <p>An example constraint String is <code>"required,length:4,int:100:1000"</code>
     * which indicates a field is required, can have no more than four characters, and
     * must be an integer with a value of at least 100 but no more than 1,000.
     * Three constraints are specified, with zero, one, and two constraint parameters
     * respectively.
     * </p>
     * <p>Note when implementing Constraints &mdash; the "args" being passed to
     * the <code>validate</code> method <em>includes</em> the name of the
     * Constraint itself as the first argument, <code>arg[0]</code>
     * </p>
     *
     * @author Stephen Paulsen
     * @since MIV 3.2
     */
    private static enum Constraints
    {
        /** This is not a valid Constraint */
        INVALID,

        /**
         * Indicates the field is required. It must be non-blank after trimming leading and trailing spaces.
         * The original untrimmed value is returned whether or not the field fails validation.
         * No parameters are used.
         */
        REQUIRED
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                value = super.validate(reqData, field, value, args);

                if (value == null || value.trim().length() < 1)
                {
                    throw new ValidationException(field, value, "This field is required.");
                }
                return value;
            }
        },

        /**
         * Indicates the maximum allowed length of the field contents.
         * One parameter, the maximum allowed length, is required.
         * Uses the form "length:12" to indicate a max length of 12 characters.
         */
        LENGTH
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                value = super.validate(reqData, field, value, args);

                Integer maxlength = null;
                try {
                    maxlength = Integer.parseInt(args[1]);
                }
                catch (NumberFormatException nfe) {
                    // the specification of the max-length was not an int!
                    throw new IllegalArgumentException("length specified was not an integer", nfe);
                }
                if (value.length() > maxlength.intValue()) {
                    throw new ValidationException(field, value, "Too many characters have been added to this field (" + maxlength + " characters allowed).");
                }
                return value;
            }
        },

        /**<p>
         * Indicates the field must be an integer, with optional minimum and maximum range.
         * Zero, one, or two parameters may be specified using the form "int[:min[:max]]".
         * Note for a maximum to be specified without a minimum the colon introducing the minimum
         * must still be present.</p>
         * Examples:<ul>
         * <li>int &ndash; the field can be any integer.</li>
         * <li>int:6 &ndash; an integer with a minimum value of 6</li>
         * <li>int:2:12 &ndash; an integer with a minimum of 2 and maximum of 12</li>
         * <li>int::42 &ndash; an integer with no minimum value, and a maximum of 42</li>
         * </ul>
         */
        INT
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                value = super.validate(reqData, field, value, args);
                // MIV-3636 added "0" as default value to conform with how the old validation worked.
                // Better would be to add the capability of providing an arbitrary default in the
                // configuration string for a field.
                return validateInteger(field, value, 0, args);
            }
        },

        /**<p>
         * Indicates the field must be an float, with optional minimum and maximum range.
         * Zero, one, or two parameters may be specified using the form "float[:min[:max]]".
         * Note for a maximum to be specified without a minimum the colon introducing the minimum
         * must still be present.</p>
         * Examples:<ul>
         * <li>float &ndash; the field can be any float.</li>
         * <li>float:0.0 &ndash; an float with a minimum value of 0</li>
         * <li>float:1.0:999.99 &ndash; an float with a minimum of 1.0 and maximum of 999.99</li>
         * <li>float::999.99 &ndash; an float with no minimum value, and a maximum of 999.99</li>
         * </ul>
         */
        FLOAT
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                // MIV-3896 added "0.0" as default value.
                if (value == null || value.isEmpty())
                {
                    value = "0.0";
                }

                value = super.validate(reqData, field, value, args);

                Float floatValue = null;
                Float max = null;
                Float min = null;

                // Arranged to avoid duplication by taking advantage of case fall-throughs
                switch (args.length)
                {
                    case 3:     // both min and max specified

                        // Convert the "picture"/max-val arg to a float. It's a programmer error if this conversion fails.
                        if (args[2] != null && args[2].trim().length() > 0) {
                            try {
                                max = new Float(args[2]);
                            }
                            catch (NumberFormatException e) {
                                throw new ValidationException(field, value, "An invalid constraint \"float:"+args[2]+"\" has been placed on this field. Please notify the miv-help list.");
                            }
                        }
                        // fall-through, since there are 2 args now get the min

                    case 2:     // only a min was specified
                        if (args[1] != null && args[1].trim().length() > 0) {
                            // Convert the "picture"/min-val arg to a float. It's a programmer error if this conversion fails.
                            try {
                                min = new Float(args[1]);
                            }
                            catch (NumberFormatException e) {
                                throw new ValidationException(field, value, "An invalid constraint \"float:"+args[1]+"\" has been placed on this field. Please notify the miv-help list.");
                            }
                        }
                        // fall-through, since there's at least 1 arg now check the value is an float

                    case 1:     // only 1 arg - the validation type "float" itself

                        // Convert the field content to a float. It's a data entry error if this conversion fails.
                        try {
                            floatValue = new Float(value);
                        }
                        catch (NumberFormatException e) {

                            // not a proper float, that's an error, unless the field is blank
                            if (value.length() != 0) {
                                throw new ValidationException(field, value, "This field is restricted to numbers and decimals only (no letters or punctuation allowed).");
                            }
                            else {
                                return value;
                            }
                        }
                    case 0:     // illegal, this shouldn't be possible
                        break;
                    default:    // too many args
                        break;
                }

                // We got here, so it's a valid float -- is it within the allowed range?

                if (min != null && floatValue.floatValue() < min.floatValue())
                {
                    throw new OutOfRangeException(field, value,
                            min.floatValue() == 0.0 ? "This value can not be negative" : "The minimum allowed value is " + min.toString());
                }

                if (max != null && floatValue.floatValue() > max.floatValue()) {
                    throw new OutOfRangeException(field, value, "The maximum allowed value is " + max.toString());
                }

                // The database doesn't like trailing decimals. The old code added a zero after the decimal, let's try trimming the decimal off.
                if (value.endsWith(".")) {
                    value = value.substring(0, value.lastIndexOf('.'));
                }

                return value;
            }
        },

        YEAR
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                value = super.validate(reqData, field, value, args);
                int maxYear = 0;
                try
                {
                    int currentYear = Calendar.getInstance().get(Calendar.YEAR);
                    maxYear = currentYear;
                    // check for current year validation
                    if (args.length == 2)
                    {
                        String limit = args[1].toUpperCase();
                        if (limit.indexOf("Y") != -1)
                        {
                            limit = limit.replace("Y", "").replace("+", "");
                            if (isInteger(limit.replace("-", "")))
                            {
                                maxYear = currentYear + (new Integer(limit));
                            }
                        }
                    }

                    value = validateInteger(field, value, "INT", "1900", String.valueOf(maxYear));
                }
                catch (OutOfRangeException e)
                {
                    // Change the standard INT out-of-range message to one that is YEAR specific
                    throw new OutOfRangeException(field, value, "must be from 1900 to " + maxYear);
                }
                return value;
            }
        },

        DAY
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                value = super.validate(reqData, field, value, args);
                if (value != null && value.trim().length() > 0)
                {
                    try
                    {
                        value = validateInteger(field, value, "INT", "1", "31");
                    }
                    catch (OutOfRangeException e)
                    {
                        // Change the standard INT out-of-range message to one that is DAY specific
                        throw new OutOfRangeException(field, value, "must be 1 to 31");
                    }
                }
                else
                {
                    // Changes an empty "day" string to a NULL string.
                    // An empty string can't be saved to the INT column,
                    // and a zero (0) would actually display in the field
                    // which we don't want. Making the DB field NULL
                    // leaves the field blank.
                    value = null;
                }

                return value;
            }
        },

        /**<p>
         * Indicates a field must be a valid date, with an enforced minimum of January 1, 1900.
         * One optional parameter specifying a format string may be provided. If the date entered
         * is determined to be valid the input will be reformatted per the format string if provided.</p>
         * <p>
         * first optional parameter specifying a format-string.
         * second optional parameter specifying a maxdate-parameter.
         * </p>
         * <p>
         * <strong>maxdate-parameter description</strong><br>
         * <strong>Y</strong>: max date till last date of Y year<br>
         * <strong>M</strong>: max date till last date of M month<br>
         * <strong>y</strong>: max date till the date of y year<br>
         * <strong>m</strong>: max date till the date of m month<br>
         * <strong>d or D</strong>: max date till the date of d/D days<br>
         * </p>
         * <p>
         * Example - date:yyyy-MM-dd:Y+1<br>
         * Output - input and accepted as "Apr 12, 2011" and would be returned as "2011-12-04"
         * </p>
         */
        DATE
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                if (value.trim().length() > 0)
                {
                    value = super.validate(reqData, field, value, args);

                    // TODO: Test this in a real situation. Date validation was never implemented in the old code.
                    Date inputDate = DateParser.parse(value, /*strict=*/true);

                    if (inputDate == null) {
                        throw new ValidationException(field, value, "\"" + value + "\" is not a valid date");
                    }

                    // Minimum Date validation
                    Calendar c = Calendar.getInstance();
                    // Set up Jan 1, 1900 as the minimum date
                    c.set(1900, 0, 1, 0, 0, 0);
                    Date minDate = c.getTime();

                    if (inputDate.before(minDate))
                    {
                        throw new OutOfRangeException(field, value, "must be greater than 1900-01-01");
                    }
                    boolean isParsed = false;
                                        // The minimum date has been checked, what's going on starting here??
                    DateFormat df = null;
                    if (args.length >= 2)
                    {
                        String dateFormat = args[1];
                        if (dateFormat.trim().length() > 0)
                        {
                            try
                            {
                                df = new SimpleDateFormat(dateFormat);
                                value = df.format(inputDate);
                                isParsed = true;
                            }
                            catch (IllegalArgumentException e)
                            {
                                logger.error("DATE parsed [{}] as [{}] with format [{}]", new Object[] { value, inputDate, dateFormat });
                                // Invalid date format try to format with default format
                                df = sdf.get();
                                value = df.format(inputDate);
                                isParsed = true;
                                //e.printStackTrace(); // FIXME: No. Don't. Stop. Log it if that's needed.
                            }
                        }
                        else
                        {
                            df = sdf.get();
                            value = df.format(inputDate);
                            isParsed = true;
                        }
                    }

                    /* Checking for third argument - Its used to decide maximum date  */
                    if (args.length >= 3)
                    {
                        String maxLimit = args[2].toUpperCase();
                        if (maxLimit.trim().length() > 0)
                        {
                            // setting the default maxdate
                            c = Calendar.getInstance();
                            //c.set(c.get(Calendar.YEAR), Calendar.DECEMBER, 31);
                            Date maxDate = c.getTime();

                            if (maxLimit.indexOf("Y") != -1) // Create max date till last date of Y year
                            {
                                maxLimit = maxLimit.replace("Y", "").replace("+", "");
                                if (isInteger(maxLimit.replace("-", "")))
                                {
                                    c.set(c.get(Calendar.YEAR) + (new Integer(maxLimit)) , Calendar.DECEMBER, 31);
                                }
                                maxDate = c.getTime();
                            }
                            else if (maxLimit.indexOf("M") != -1) // Create max date till last date of M month
                            {
                                maxLimit = maxLimit.replace("M", "").replace("+", "");
                                if (isInteger(maxLimit.replace("-", "")))
                                {
                                    c.add(Calendar.MONTH, (new Integer(maxLimit)));
                                    c.add(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                                }
                                maxDate = c.getTime();
                            }
                            else if (maxLimit.indexOf("y") != -1) // Create max date till the date of y year
                            {
                                maxLimit = maxLimit.replace("y", "").replace("+", "");
                                if (isInteger(maxLimit.replace("-", "")))
                                {
                                    c.add(Calendar.YEAR, (new Integer(maxLimit)));
                                }
                                maxDate = c.getTime();
                            }
                            else if (maxLimit.indexOf("m") != -1) // Create max date till the date of m month
                            {
                                maxLimit = maxLimit.replace("m", "").replace("+", "");
                                if (isInteger(maxLimit.replace("-", "")))
                                {
                                    c.add(Calendar.MONTH, (new Integer(maxLimit)));
                                }
                                maxDate = c.getTime();
                            }
                            else if (maxLimit.indexOf("D") != -1 || maxLimit.indexOf("d") != -1) // Create max date till the date of d/D days
                            {
                                maxLimit = maxLimit.toUpperCase().replace("D", "").replace("+", "");
                                if (isInteger(maxLimit.replace("-", "")))
                                {
                                    c.add(Calendar.DATE, (new Integer(maxLimit)));
                                }
                                maxDate = c.getTime();
                            }
                            else
                            {
                                maxLimit = maxLimit.replace("C", "").replace("c", "");
                                maxDate = DateParser.parse(value, /* strict= */true);
                            }

                            if (maxDate != null && inputDate.after(maxDate))
                            {
                                throw new OutOfRangeException(field, value, "must be less than "+sdf.get().format(maxDate));
                            }
                        }
                    }

                    if (!isParsed && inputDate != null) // now this reformats the input date?
                    {
                        value = sdf.get().format(inputDate);
                    }

                    logger.debug("DATE parsed [{}] as [{}]", value, inputDate);
                }

                return value;
            }
        },

        TEXT
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                value = value != null ? value.trim() : "";
                if (value.trim().length() > 0)
                {
                    value = super.validate(reqData, field, value, args);

                    if (args.length == 2)
                    {
                        String limit = args[1];
                        if (value.getBytes().length > Integer.parseInt(limit)) {
                            throw new ValidationException(field, value, "Maximum field length(" + limit + " bytes) exceeded.");
                        }
                    }
                }

                return value;
            }
        },

        /**<p>
         * Indicates a field must be a valid dropdown.
         * first optional parameter specifying a value to be check for required.
         * second optional parameter specifying an type of validation operation to be perform like suggestions etc.
         * third optional parameter specifying a value on which validation takes place.(second optional parameter required)
         * forth optional parameter specifying a field name to be validate,
         * if its empty automatically creates that parameter by adding prefix txt to the field.
         * Examples- dropdown:0:suggestions:0:suggestworktypeid or dropdown:0
         *</p>
         */
        DROPDOWN
        {
            @Override
            String validate(Map <String, String[]> reqData,String field, String value, String... args) throws ValidationException
            {
                value = value != null ? value.trim() : "";
                if (value.length() > 0)
                {
                    value = super.validate(reqData, field, value, args);
                    String dropdownid = "0";
                    String suggestionsselectid = "0";
                    boolean suggestions = false;
                    String suggestionfield = "";
                    String suggestionfievalue = "";

                    if (args.length > 2) {
                        dropdownid = args[1];
                    }

                    if (args.length == 2)
                    {
                        if (value.equals(dropdownid))
                        {
                            throw new ValidationException(field, value, "This field is required.");
                        }
                        return value;
                    }


                    if (args.length > 3)
                    {
                        if (args[2].equalsIgnoreCase("suggestions"))
                        {
                            suggestions = true;
                            // setting the default text field for suggestions
                            suggestionfield = "txt" + field;
                        }
                    }

                    if (args.length > 4)
                    {
                        if (args[3].length() > 0) {
                            suggestionsselectid = args[3];
                        }
                    }

                    if (args.length > 5)
                    {
                        if (args[4].length() > 0) {
                            suggestionfield = args[4];
                        }
                    }

                    if (suggestions)
                    {
                        if (reqData != null && reqData.get(suggestionfield) != null)
                        {
                            suggestionfievalue = reqData.get(suggestionfield)[0];
                        }
                    }

                    if (value.equals(suggestionsselectid) && suggestionfievalue.length() == 0)
                    {
                        throw new ValidationException(field, value, "This field is required.");
                    }

                    if (!dropdownid.equalsIgnoreCase(suggestionsselectid) && value.equals(dropdownid))
                    {
                        throw new ValidationException(field, value, "This field is required.");
                    }
                }

                return value;
            }
        },

        URL
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                if (StringUtils.isNotEmpty(value))
                {
                    value = super.validate(reqData, field, value, args);

                    try {
                        if (StringUtils.isEmpty((new URL(value)).getHost())) {
                            throw new MalformedURLException("No host");
                        }
                    }
                    catch (MalformedURLException e) {
                        throw new ValidationException(field, value, "must be a valid web address (e.g. <em>http://example.com/some+page/some_document.pdf</em>)");
                    }
                }

                return value;
            }
        },

        /**<p>
         * To provide custom validation.
         * first optional parameter specifying a class name.
         * second optional parameter specifying a validation method name.
         * i.e custom:{Class_Name}:{Method_Name}
         * Example - custom:edu.ucdavis.mw.myinfovault.web.CreativeActivitiesEditRecord:validateEventDate
         *</p>
         */
        CUSTOM
        {
            @Override
            String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
            {
                if (logger.isDebugEnabled())
                {
                    System.out.println("================== CUSTOM Validation ===========================");
                    System.out.println("reqData :: " + reqData);
                    System.out.println("field :: " + field);
                    System.out.println("value :: " + value);
                    System.out.println("args :: " + args);
                    System.out.println("args :: " + args.length);
                }

                ResultVO resultVO = null;
                String finalValue = value;
                List<String> fields = Arrays.asList(field);

                if (args.length == 3)
                {
                    String className = args[1];
                    String methodName = args[2];

                    try
                    {
                        Class<?> dynaclass = Class.forName( className );
                        Object classInstance = dynaclass.newInstance(); // Calling default constructor
                        // Method must have two arguments userId as String and reqData as Map
                        Method method = dynaclass.getDeclaredMethod(methodName, String.class, Map.class);

                        logger.debug("method :: {}", method);

                        if (method != null)
                        {
                            Object resultObject = method.invoke(classInstance, value, reqData);
                            if (resultObject instanceof ResultVO)
                            {
                                resultVO = (ResultVO) resultObject;
                            }
                        }

                        if (resultVO.getValue() == null)
                        {
                            finalValue = null;
                        }
                        else if (!resultVO.getValue().equals(ResultVO.DEFAULT_VALUE))
                        {
                            finalValue = resultVO.getValue();
                        }


                        if (resultVO != null && !resultVO.isSuccess())
                        {
                            // if you set a custom field name
                            if (resultVO.getErrorfields() != null && !resultVO.getErrorfields().isEmpty())
                            {
                                fields = resultVO.getErrorfields();
                            }
                            throw new ValidationException(StringUtils.join(fields, ','), value, StringUtils.join(resultVO.getMessages(), ", "));
                        }
                    }
                    catch (ValidationException ve) // FIXME: if we're just re-throwing it, why catch it in the first place?
                    {
                        throw ve;
                    }
                    catch (Exception e)
                    {
                        // FIXME: Why is plain 'Exception' being caught here?  Isn't there something more specific to look for?
                        //        There may be a legitimate reason to catch everything here, because a reflective method call
                        //        is taking place, but that should be documented.
                        //        The reflection based exceptions, e.g. ClassNotFoundException, InvocationTargetException, etc.
                        //        should be handled separately, as configuration/programmer errors.
                        logger.error("The 'CUSTOM' validation constraint caught an exception [" + e.getClass().getSimpleName() + "]", e);
                        logger.error("The 'CUSTOM' validator class/method was [{}]/[{}]", className, methodName);
                        //e.printStackTrace(); // FIXME: do not use printStackTrace()
                        throw new ValidationException(field, finalValue, "An invalid constraint \"custom\" has been placed on this field. Please notify the miv-help list.");
                    }
                }

                return finalValue;
            }
        };

        /* End of the ENUM Values */



        /**<p>
         * Base implementation of validate. Every Constraint should override this.
         * This could be made abstract, but for now is used to aid debugging. Each
         * Constraint can call <code>super.validate(field, value, args)</code> in
         * its own implementation of validate so information is printed.</p>
         * <p>Rather than being abstract, there may be some reason to have this base
         * method do some kind of standard basic validation.</p>
         */
        @SuppressWarnings("unused")
        String validate(Map <String, String[]> reqData, String field, String value, String... args) throws ValidationException
        {
            // For debugging...
            if (logger.isDebugEnabled())
            {
                System.out.println("Called [ validate(" + field + ", " + value + ", {args}) ]");
                System.out.print("Args are:");
                for (String arg : args) {
                    System.out.print("  [" + arg + "]");
                }
                System.out.println();
            }
            // Done debugging.

            return value;
        }


        String validateInteger(String field, String value, Integer defaultValue, String... args) throws ValidationException
        {
            if (value == null || value.length() == 0) {
                value = defaultValue.toString();
            }
            return validateInteger(field, value, args);
        }


        /** Used by INT, YEAR, and DAY */
        String validateInteger(String field, String value, String... args) throws ValidationException
        {
            Integer min = null;
            Integer max = null;
            Integer intValue = null;

            // Arranged to avoid duplication by taking advantage of case fall-throughs
            switch (args.length)
            {
                case 3:     // both min and max specified
                    if (args[2] != null && args[2].trim().length() > 0) {
                        max = new Integer(args[2]);
                    }
                    // fall-through, since there are 2 args now get the min

                case 2:     // only a min was specified
                    if (args[1] != null && args[1].trim().length() > 0) {
                        min = new Integer(args[1]);
                    }
                    // fall-through, since there's at least 1 arg now check the value is an int

                case 1:     // only 1 arg - the validation type "int" itself
                    try {
                        intValue = new Integer(value);
                    }
                    catch (NumberFormatException nfe) {
                        // not a proper int, that's an error, unless the field is blank
                        if (value.length() != 0) {
                            throw new ValidationException(field, value,
                                    "This field is restricted to numbers only (no letters or punctuation allowed).");
                        }
                        else {
                            return value;
                        }
                    }
                    break;

                case 0:     // illegal, this shouldn't be possible
                    break;

                default:    // too many args
                    break;
            }


            // We got here, so it's a valid integer -- is it within the allowed range?

            if (min != null && intValue.intValue() < min.intValue())
            {
                throw new OutOfRangeException(field, value,
                            min.intValue() == 0 ? "This value can not be negative" : "The minimum allowed value is " + min.toString());
            }

            if (max != null && intValue.intValue() > max.intValue()) {
                throw new OutOfRangeException(field, value, "The maximum allowed value is " + max.toString());
            }

            return value;
        }
    }


    /**
     * Integer validity checker
     *
     * @param intStr String to test
     * @return {@code true} if the given String is a valid integer value
     */
    public static boolean isInteger(String intStr)
    {
        try
        {
            Integer.parseInt(intStr);
        }
        catch (NumberFormatException exception)
        {
            return false;
        }

        return true;
    }


    /**
     * Float validity checker
     *
     * @param floatStr String to test
     * @return {@code true} if the given String is a valid floating-point value
     */
    public static boolean isFloat(String floatStr)
    {
        try
        {
            Float.valueOf(floatStr);
        }
        catch (NumberFormatException exception)
        {
            return false;
        }

        return true;
    }


    /** Used by {@link #isDate(String, String, String)} for date validation. */
    private static final ThreadLocal<DateFormat> sdf =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return new SimpleDateFormat(MIVConfig.getConfig().getProperty("default-config-dateformat"));
            //return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    /**
     * Check the validity of a date broken down into year-month-day components.
     * The Strings must be parseable as date components and the represented date
     * must be valid - e.g. 2011, 2, 30 (Feb 30 2011) is not a valid date
     * @param year
     * @param month
     * @param day
     * @return {@code true} if the Y-M-D represents a valid date
     */
    public static boolean isDate(String year, String month, String day)
    {
        if (month.length() < 2) month = "0" + month;
        if (day.length() < 2) day = "0" + day;
        String date = year + "-" + month + "-" + day;

        DateFormat df = sdf.get();
        try
        {
            df.setLenient(true);
            df.parse(date);
        }
        catch (ParseException exception)
        {
            return false;
        }

        return true;
    }


    /**
     * Test driver program
     * @param args
     */
    public static void main(String[] args)
    {
        System.out.println("\nStarting tests\n");

        Validator v = new Validator();

        String[] test0 = { "duh:42" };      // specify an invalid constraint name

        // REQUIRED Tests
        String[] test1a = { "REQUIRED:yes:no:maybe" };
        String[] test1b = { "REQUIRED" };
        String[] test1c = { "required" };

        // LENGTH Tests
        String[] test2a = { "Length:7" };
        String[] test2b = { "6" }; // old-style "bare" length
        String[] test2c = { "length:3A" }; // invalid length given

        // INT Tests
        String[] test3a = { "int:2:12" };    // int min=2, max=12
        String[] test3b = { "int:6" };       // int min=6, no max
        String[] test3c = { "int::13" };     // int no min, max=13
        String[] test3d = { "int" };         // any int, no limits

        // FLOAT Tests
//        String[] test4a = { "float" };
        String[] test4b = { "float:0.0:999.99" };

        // YEAR Tests
        String[] test5a = { "year" };

        // DAY Tests
        String[] test6a = { "day" };

        // DATE Tests
        String[] test7a = { "date:yyyy-MM-dd" };
        String[] test7b = { "date" };
        String[] test7c = { "required", "date:yyyy-MM-d" };



        // Combination Tests
        String[] test20 = { "required", "length:2", "int:7:11" };

        String[] test21 = {"text:10"};

        runtest(v, "invalid-constraint-name", "x", test0);

        runtest(v, "required-extra-args", "x", test1a);
        runtest(v, "required-absent",     "",  test1b);
        runtest(v, "required-present",    "x", test1c);

        runtest(v, "length-valid",    "x", test2a);         // valid
        runtest(v, "length-too-long", "123456789", test2a); // too long
        runtest(v, "length-valid",    "x", test2b);         // valid
        runtest(v, "length-too-long", "123456789", test2b); // too long
        runtest(v, "length-bad-spec", "123", test2c);      // bad length

        runtest(v, "int-valid",    "7",  test3a); // valid
        runtest(v, "int-too-high", "33", test3a); // out of range high
        runtest(v, "int-too-low",  "-2", test3a); // out of range low
        runtest(v, "int-not-int",  "7mary3",  test3a); // not an integer

        runtest(v, "int-valid", "7", test3b);  // valid
        runtest(v, "int-too-low", "3", test3b);  // out of range low
        runtest(v, "int-not-int", "sum41", test3b);  // not an integer

        runtest(v, "int-valid", "-4", test3c); // valid
        runtest(v, "int-too-high", "33", test3c); // out of range high
        runtest(v, "int-not-int", "x", test3c);  // not an integer

        runtest(v, "int-valid", "1776", test3d); // valid
        runtest(v, "int-not-int", "5-4", test3d);  // not an integer

        runtest(v, "float-valid", "987.54", test4b);
        runtest(v, "float-valid-trailing-dot", "22.", test4b);
        runtest(v, "float-too-big", "1234.56", test4b);
        runtest(v, "float-not-float", "123+45", test4b);
        runtest(v, "float-min-invalid", "-987.54", test4b);

        runtest(v, "year-valid", "2001", test5a);
        runtest(v, "year-too-low", "1899", test5a);
        runtest(v, "year-too-high", "2020", test5a);
        runtest(v, "year-not-numeric", "200x", test5a);

        runtest(v, "day-valid", "12", test6a);
        runtest(v, "day-too-high", "34", test6a);
        runtest(v, "day-too-low", "0", test6a);
        runtest(v, "day-blank", "", test6a);
        runtest(v, "day-not-int", "x", test6a);


        runtest(v, "date-valid-short-slashed", "12/4/61", test7a);
        runtest(v, "date-valid-short-dashed", "12-4-1961", test7a);
        runtest(v, "date-invalid-short-slashed", "2/31/2002", test7a);
        runtest(v, "date-invalid-short-slashed", "12/99/2009", test7a);

        runtest(v, "date-valid-long-1", "Feb. 7, 1927", test7a); // Parser doesn't like the period after the abbreviation
        runtest(v, "date-valid-long-2", "Feb 7, 1927", test7a);
        runtest(v, "date-valid-long-3", "Feb. 7 1927", test7a);
        runtest(v, "date-valid-long-4", "Feb 7 1927", test7a);
        runtest(v, "date-valid-long-5", "December 7, 1941", test7a);
        runtest(v, "date-valid-long-6", "2011-3-13", test7a);


        runtest(v, "combo-valid", "7", test20);
        runtest(v, "combo-absent", "", test20);
        runtest(v, "combo-blank", " ", test20);
        runtest(v, "combo-too-long", "123", test20);
        runtest(v, "combo-too-high", "42", test20);
        runtest(v, "combo-too-low", "3", test20);
        runtest(v, "combo-not-int", "xx", test20);

        runtest(v, "date-valid-long-09", "Feb. 7 1927", test7b);
        runtest(v, "date-valid-long-10", "Feb. 7 1927", test7c);
        runtest(v, "date-valid-long-11", "", test7c);
        runtest(v, "text-content", "test the text constraint", test21);

        String[] test5b = { "year:Y-1" };    // int min=1900, max= (Current Year) -1
        String[] test5c = { "year:Y+1" };    // int min=1900, max= (Current Year) +1
        runtest(v, "int-year-validation-1", "2000", test5b);
        runtest(v, "int-year-validation-2", "2014", test5c);

        runtest(v, "date-valid-long-7", "2011-3-13", new String[]{ "date::Y+1" });
        runtest(v, "date-valid-long-8", "2011-3-13", new String[]{ "date:yyyy-MM-dd:Y+1" });
        runtest(v, "date-valid-long-9", "2020-3-13", new String[]{ "date:yyyy-MM-dd:Y-1" });
        runtest(v, "date-valid-long-10", "2012-4-13", new String[]{ "date:yyyy-MM-dd:M+1" });
        runtest(v, "date-valid-long-11", "2012-2-13", new String[]{ "date:yyyy-MM-dd:M-1" });
        runtest(v, "date-valid-long-12", "2012-3-13", new String[]{ "date:yyyy-MM-dd:y+1" });
        runtest(v, "date-valid-long-13", "2012-3-13", new String[]{ "date:yyyy-MM-dd:y-1" });

        runtest(v, "date-valid-long-10", "2012-4-13", new String[]{ "date:yyyy-MM-dd:m+1" });
        runtest(v, "date-valid-long-11", "2012-2-13", new String[]{ "date:yyyy-MM-dd:m-1" });
        runtest(v, "date-valid-long-12", "2012-3-16", new String[]{ "date:yyyy-MM-dd:d+1" });
        runtest(v, "date-valid-long-13", "2011-3-16", new String[]{ "date:yyyy-MM-dd:d-1" });

        runtest(v, "date-valid-long-12", "2011-3-13", new String[]{ "date:yyyy-MM-dd:2010-12-31" });

        runtest(v, "dropdown-valid-1", "1", new String[]{ "dropdown:0:suggestions:0:txtworktypeid" });
        runtest(v, "dropdown-valid-2", "0", new String[]{ "dropdown:0" });
        runtest(v, "date-valid-long-8", "May 02, 2012", new String[]{ "date:" });

        System.out.println("Tests complete");
    }

    /**
     * Run a test case from the {@link #main(String[])} driver program.
     * @param v a Validator to use for the test.
     * @param field name of a (fake) field, used for reporting.
     * @param value the Value that the field (allegedly) contains.
     * @param pconstraints field constraints that would have been read from a configuration.
     */
    private static void runtest(Validator v, String field, String value, String[] pconstraints)
    {
        v.reset();
        String testValue = value;

        if (pconstraints == null) {
            testValue = v.validate(field, value);
        }
        else {
            testValue = v.validate(field, testValue, pconstraints);
        }

        String errFields = v.getErrors();
        String errMsgs = v.getErrorMsgs();
        boolean hasErrors = !errMsgs.equals("['']");

        StringBuilder sb = new StringBuilder();
        for (String s : pconstraints) {
            sb.append(s).append("  ");
        }
        System.out.println("Field: [" + field + "]  Value: [" + value + "]  Constraints: ["+sb.toString().trim()+"]");

        System.out.println("  Produced value [" + testValue + "] with " + (hasErrors ? "" : "no ") + "errors");
        if (hasErrors)
        {
            System.out.println("    Fields [" + errFields + "]");
            System.out.println("  Messages [" + errMsgs + "]");
        }
        System.out.println("\n");
    }

}
