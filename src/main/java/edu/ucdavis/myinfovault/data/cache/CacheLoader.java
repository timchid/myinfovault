/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CacheLoader.java
 */

package edu.ucdavis.myinfovault.data.cache;


/**
 * When a Cache has a miss it needs to load the requested object from
 * the key given. The cache doesn't know how to do that so it calls
 * back you an object you provide, which must implement the CacheLoader
 * interface, to retrieve the object.
 * 
 * @author Stephen Paulsen
 * @since MIV 3.0
 * @param <K> 
 * @param <T> 
 */
public interface CacheLoader<K, T>
{
    /**
     * Users of a {@link Cache} must provide a CacheLoader to retrieve objects
     * when there is a cache miss.
     */
    public T load(K key);

}
