package edu.ucdavis.myinfovault.data.exception;

import edu.ucdavis.myinfovault.exception.MIVApplicationException;

public class MIVSaveRecordException extends MIVApplicationException
{
    private static final long serialVersionUID = 200804091020L;

    public MIVSaveRecordException()
    {
        super();
    }

    public MIVSaveRecordException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MIVSaveRecordException(String message)
    {
        super(message);
    }

    public MIVSaveRecordException(Throwable cause)
    {
        super(cause);
    }
}
