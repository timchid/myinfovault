/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: OutOfRangeException.java
 */


package edu.ucdavis.myinfovault.data.exception;


/**
 * Represents an error due to a value being out of an acceptable range.
 *
 * @author Stephen Paulsen
 * @since MIV 3.3
 */
public class OutOfRangeException extends ValidationException
{
    private static final long serialVersionUID = 201003100930L;

    public OutOfRangeException(String field, String value, String message)
    {
        super(field, value, message);
    }
}
