package edu.ucdavis.myinfovault.data.mover;

/*
 * Things in the new GrantSummary table.
 */
public class Grant
{
    // Completed, Active, Other grants
    // CvRcomgrants, CvRactgrants, CvRothgrants are the same
    String CvRcomgrants =
        "select RGCID,SSN,Title,GtDesc,GtNum,Agency,Amount,PiName,Seq,PkPrt,Type,Startyear,Endyear,Parms,Prt,PkGtPrt"+
        " from CvRcomgrants";
    String CvRactgrants =
        "select RGAID,SSN,Title,GtDesc,GtNum,Agency,Amount,PiName,Seq,PkPrt,Type,Startyear,Endyear,Parms,Prt,PkGtPrt"+
        " from CvRactgrants";
    String CvRothgrants =
        "select RGOID,SSN,Title,GtDesc,GtNum,Agency,Amount,PiName,Seq,PkPrt,Type,Startyear,Endyear,Parms,Prt,PkGtPrt"+
        " from CvRothgrants";

    // Pending, Unawarded grants
    // CvRpendgrants, CvUnawarded are the same
    String CvRpendgrants =
        "select RGPID,SSN,Title,GtDesc,GtNum,Agency,Amount,PiName,Seq,PkPrt,Type,Subyear,Parms,Prt,PkGtPrt"+
        " from CvRpendgrants";
    String CvUnawarded =
        "select RGUID,SSN,Title,GtDesc,GtNum,Agency,Amount,PiName,Seq,PkPrt,Type,Subyear,Parms,Prt,PkGtPrt"+
        " from CvUnawarded";

    // Gift grants
    String CvRgiftgrants =
        "select RGGID,SSN,Title,GtDesc,GtNum,Source,Amount,PiName,Seq,PkPrt,Year,Parms,Prt,PkGtPrt"+
        " from CvRgiftgrants";

    // Teaching/Training grants
    String CvTgrants =
        "select TGID,SSN,Title,GtDesc,GtNum,Agency,Amount,PiName,Seq,PkPrt,StartYear,EndYear,SubYear,Status,Parms,Prt,PkGtPrt"+
        " from CvTgrants";

    // Grant Types:
    // 1 Research
    // 2 Gift
    // 3 Teaching and Training
    // 4 Other

    // Grant Statuses:
    // 1 Completed
    // 2 Active
    // 3 Pending
    // 4 Unawarded

    // Grant Roles:
    // 1 Principal Investigator
    // 2 Co-Investigator
    // 3 Assistant Researcher
    // 4 Collaborator
    // 5 Instructor

    int RecordID = 0;
    int UserID;
    int TypeID;
    int StatusID;
    String Title;
    String Description;
    String Number;
    String Source;
    String Amount;
    String SubmitDate;
    String StartDate;
    String EndDate;
    int RoleID;
    String PrincipalInvestigator;
    int Sequence;
    boolean Display;
    boolean DisplayDescription;

    @Deprecated
    public void setAmount(String amount)
    {
        Amount = amount;
    }

    public void setDescription(String description)
    {
        Description = description;
    }

    public void setDisplay(boolean display)
    {
        Display = display;
    }

    public void setDisplayDescription(boolean displayDescription)
    {
        DisplayDescription = displayDescription;
    }

    public void setEndDate(String endDate)
    {
        EndDate = endDate;
    }

    public void setNumber(String number)
    {
        Number = number;
    }

    public void setPrincipalInvestigator(String principalInvestigator)
    {
        PrincipalInvestigator = principalInvestigator;
    }

    public void setRecordID(int recordID)
    {
        RecordID = recordID;
    }

    public void setRoleID(int roleID)
    {
        RoleID = roleID;
    }

    public void setSequence(int sequence)
    {
        Sequence = sequence;
    }

    public void setSource(String source)
    {
        Source = source;
    }

    public void setStartDate(String startDate)
    {
        StartDate = startDate;
    }

    public void setStatusID(int statusID)
    {
        StatusID = statusID;
    }

    public void setSubmitDate(String submitDate)
    {
        SubmitDate = submitDate;
    }

    public void setTitle(String title)
    {
        Title = title;
    }

    public void setTypeID(int typeID)
    {
        TypeID = typeID;
    }

    public void setUserID(int userID)
    {
        UserID = userID;
    }
}
