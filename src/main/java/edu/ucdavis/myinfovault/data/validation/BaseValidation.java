/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BaseValidation.java
 */

package edu.ucdavis.myinfovault.data.validation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import edu.ucdavis.myinfovault.MIVConfig;

/**
 * To provide most common methods and function to help validation.
 *
 * @author pradeeph
 * @since MIV 4.4.2
 */
public class BaseValidation
{
    // DateFormat is not thread safe. Always use a ThreadLocal for a DateFormat
    protected static final ThreadLocal<DateFormat> sdf =
            new ThreadLocal<DateFormat>() {
                @Override
                protected DateFormat initialValue()
                {
                    return new SimpleDateFormat(MIVConfig.getConfig().getProperty("default-config-dateformat"));
                }
            };
    protected static final ThreadLocal<DateFormat> yf =
            new ThreadLocal<DateFormat>() {
                @Override
                protected DateFormat initialValue()
                {
                    return new SimpleDateFormat("yyyy");
                }
            };
}
