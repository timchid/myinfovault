package edu.ucdavis.myinfovault.designer;


public class BaseDisplayState implements DisplayState
{   
    String cbName;
    String recID;
    String recType;
    String displayField;
    boolean displayStatus;

    public BaseDisplayState(String cbName, String recID, String recType, String displayFieldName, boolean displayFlag)
    {
        this.setCbName(cbName);
        this.setRecID(recID);
        this.setRecType(recType);
        this.setDisplayField(displayFieldName);
        this.setDisplayStatus(displayFlag);
    }

    public BaseDisplayState(String cbName, String recID, String recType, String displayFieldName, String displayFlag)
    {
        this(cbName, recID, recType, displayFieldName, "true".equals(displayFlag) || "1".equals(displayFlag));
    }

    public String toString()
    {
        return cbName + "," + recID + "," + recType + "," + displayField + "," + (displayStatus ? "1" : "0");
    }

    public void setCbName(String cbName)
    {
        this.cbName = cbName;
    }

    public String getCbName()
    {
        return this.cbName;
    }

    public void setDisplayStatus(boolean displayStatus)
    {
        this.displayStatus = displayStatus;
    }

    public boolean getDisplayStatus()
    {
        return this.displayStatus;
    }

    private void setRecID(String recID)
    {
        this.recID = recID;
    }

    public String getRecID()
    {
        return this.recID;
    }

    public String getID() // an alias for getRecID()
    {
        return this.getRecID();
    }

    private void setRecType(String recType)
    {
        this.recType = recType;
    }

    public String getRecType()
    {
        return this.recType;
    }

    public void setDisplayField(String displayField)
    {
        this.displayField = displayField;
    }

    public String getDisplayField()
    {
        return this.displayField;
    }
}
