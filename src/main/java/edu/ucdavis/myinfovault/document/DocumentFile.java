package edu.ucdavis.myinfovault.document;

import java.io.File;

public class DocumentFile
{
    boolean createSuccess = false;
    File outputFile = null;
    String outputUrl = null;
    DocumentFormat documentFormat = DocumentFormat.PDF;
    
    DocumentFile(boolean createSuccess, File outputFile, String outputUrl, DocumentFormat documentFormat)
    {
        this.createSuccess = createSuccess;
        this.outputFile = outputFile;
        this.outputUrl = outputUrl;
        this.documentFormat = documentFormat;
    }

    public File getOutputFile()
    {
        return outputFile;
    }

    public void setOutputFile(File outputFile)
    {
        this.outputFile = outputFile;
    }

    public boolean getCreateSuccess()
    {
        return createSuccess;
    }

    public String getOutputUrl()
    {
        return outputUrl;
    }

    public DocumentFormat getDocumentFormat()
    {
        return documentFormat;
    }

}
