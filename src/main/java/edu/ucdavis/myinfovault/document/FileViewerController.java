/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FileViewerController.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.SchoolDepartmentCriteria;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.document.Builder.BuilderDocumentType;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * TODO: Add Javadoc comments!
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
@Controller
@RequestMapping("/**")
public class FileViewerController
{
    private static final int BUFFER_SIZE = 4 * 1024; // 4K buffer

    private static Logger log = LoggerFactory.getLogger(FileViewerController.class);

    private final DocumentAccess documentAccess;
    private final UserService userService;
    private final DossierCreatorService dossierCreator;

    /** Associate file extensions with MIME types. */
    private static final Map<String,String> mimeTypeMap = new HashMap<String,String>();
    static {
        mimeTypeMap.put("pdf", "application/pdf");
        mimeTypeMap.put("doc", "application/msword");
        mimeTypeMap.put("rtf", "application/rtf");
        mimeTypeMap.put("txt", "text/plain");
        mimeTypeMap.put("jpg", "image/jpeg");
        mimeTypeMap.put("jpeg", "image/jpeg");
        mimeTypeMap.put("png", "image/png");
        mimeTypeMap.put("gif", "image/gif");
        mimeTypeMap.put("xls", "application/ms-excel");
        mimeTypeMap.put("csv", "text/plain");
        mimeTypeMap.put("ppt", "application/ms-powerpoint");
        // missing / unknown extension default to octet-stream
        mimeTypeMap.put("", "application/octet-stream");
        mimeTypeMap.put(null, "application/octet-stream");
    }

    @Autowired
    public FileViewerController(UserService userService,
                                DossierCreatorService dossierCreator,
                                DocumentAccess documentAccess)
    {
        this.documentAccess = documentAccess;
        this.userService = userService;
        this.dossierCreator = dossierCreator;
    }


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getDocument(HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        log.trace("enter handleRequestInternal");

        /*
         * Because we override handleRequestInternal, which is the _first_ method called
         * in the lifecycle, we have to check for the mivSession ourselves.
         * This is the same code that is in the BiosketchFormController.
         * The alternative is to rework this class so it can be a subclass and
         * inherit this session check.
         */
        MIVSession ms = MIVSession.getSession(request);
        if (ms == null || ! ms.hasSession())
        {
            String className = this.getClass().getSimpleName();
            if (ms == null) System.out.println("\n\n" + className + ": MIVSession is NULL");
            else if (! ms.hasSession()) System.out.println("\n\n" + className + ": MIVSession.hasSession() is FALSE");
            System.out.println("" + className + ": Redirecting to /logout.html\n");
//            URL logoutTargetUrl = new URL("http", request.getServerName(), "/logout.html");
//            response.sendRedirect(logoutTargetUrl.toString());
//            return null;
            return new ModelAndView(new RedirectView("/logout.html"));
        }

        MIVSession mivSession = ms;

        String url = request.getPathInfo();
        String roleId = request.getParameter("roleId");

        // The concatenateFile parameter may contain a comma separated list of
        // files to be concatenated to the file specified in the URL for viewing
        /*
         * cd to the root of the workspace, then run:
         * $ find src -type f -exec fgrep -n concatenateFiles {} +
         * There is no place where the request parameter "concatenateFiles" is ever set.
         */
        String concatenateFiles = request.getParameter("concatenateFiles");

        if (url == null)
        {
            throw new MivSevereApplicationError("URL was missing in FileViewer");
        }

        PathConstructor pathConstructor = new PathConstructor();

        File filePath = pathConstructor.getPathFromUrl(url);
        boolean isDossierFile = pathConstructor.getFileName().toLowerCase().endsWith("dossier.pdf");
        boolean isPacketFile = pathConstructor.getFileName().toLowerCase().endsWith("packet.pdf");

        // If the file name is dossier.pdf or candidatedossier.pdf, it will not exist and must be created on the fly
        if (!filePath.exists() && !isDossierFile && !isPacketFile)
        {
            /*
             * MIV-4836
             *
             * The dean's final decision signature has been removed from the RAF
             * and added to its own document. PDFs generated before this change
             * will still have the dean's final decision signature on the RAF.
             *
             * Redirecting requests for missing "deansfinaldecision_x_y.pdf" to "raf_x_y.pdf".
             */
            if (pathConstructor.getFileName().startsWith(MivDocument.DEANS_FINAL_DECISION.getDossierFileName()))
            {
                return new ModelAndView(new RedirectView(
                    pathConstructor.getUrlFromPath(filePath)
                                   .replace(MivDocument.DEANS_FINAL_DECISION.getDossierFileName(),
                                            MivDocument.RAF.getDossierFileName())));
            }
            response.setContentType(MediaType.TEXT_HTML);
            response.sendError(HttpServletResponse.SC_NOT_FOUND, "The requested file is not found in the file repository.");

            return null;
        }

        // Get the documentId which equates to the dossierId
        long dossierId = pathConstructor.getDocumentId();

        // get the target userId from the session
        int userId = mivSession.getUser().getTargetUserInfo().getPerson().getUserId();

        MivPerson mivPerson = userService.getPersonByMivId(userId);

        // If the roleId is passed in, use it (this would be the case for a derived reviewer role), otherwise get the primary role of the
        // current target user
        MivRole mivRole = roleId != null ? MivRole.valueOf(roleId) : mivPerson.getPrimaryRoleType();

        /*
         * DO NOT REMOVE -- This is important for functionality in IE.
         * This line removes the pragma "no-cache" response header
         */
        response.setHeader("Pragma", "");

        // If this is a "dossier", create and stream
        if (isDossierFile)
        {
            Dossier dossier = MivServiceLocator.getDossierService().getDossier(dossierId);

            // Check if role is authorized to view the dossier at its current location
            if (documentAccess.isAuthorized(mivRole, mivPerson, dossier))
            {
                if (mivRole.isReviewerRole())
                {
                    // A reviewer role has been specified, we need to get the review scope for this person
                    Scope scope = documentAccess.getReviewScope(mivPerson, dossier);

                    // Deny access if there is no review scope for this person.
                    if (scope.matches(Scope.None))
                    {
                        throw new AccessDeniedException("No review scope for user " + mivPerson.getUserId());
                    }

                    // Set the schoolDepartmentCriteria and stream the dossier for the reviewer
                    SchoolDepartmentCriteria schoolDepartmentCriteria = new SchoolDepartmentCriteria();
                    schoolDepartmentCriteria.setDepartment(scope.getDepartment());
                    schoolDepartmentCriteria.setSchool(scope.getSchool());
                    Map<String, SchoolDepartmentCriteria>schoolDepartmentCriteriaMap = new HashMap<String, SchoolDepartmentCriteria>();
                    schoolDepartmentCriteriaMap.put(schoolDepartmentCriteria.getSchool()+":"+schoolDepartmentCriteria.getDepartment(), schoolDepartmentCriteria);
                    dossierCreator.createDossier(response.getOutputStream(), dossier, mivRole, schoolDepartmentCriteriaMap);
                }
                else
                {
                    // Create and stream the dossier based on the mivPerson
                    dossierCreator.createDossier(response.getOutputStream(), dossier, mivPerson);
                }
                return null;
            }
            else
            {
                throw new AccessDeniedException("User " + mivPerson.getUserId() + "not authorized to view dossier " + dossier.getDossierId() + " in the " + mivRole + " capacity");
            }
        }
        // If this is a "packet", concatenate existing packet files and stream to browser
        else if (isPacketFile)
        {
            if (!documentAccess.isAuthorized(userId, filePath))
            {
                throw new AccessDeniedException("User " + userId + "not authorized to view " + filePath);
            }

            Packet packet = pathConstructor.getDocumentId() <= 0 ? new Packet(mivPerson.getUserId(), "Master") : MivServiceLocator.getPacketService().getPacket(pathConstructor.getDocumentId());

            PacketBuilder packetBuilder = new PacketBuilder(BuilderDocumentType.valueOf(pathConstructor.getDocType().toUpperCase()), packet, mivPerson);
            // List existing packet documents
            List<Document>documents = packetBuilder.getDocumentList();
            // Make sure there is something to concatenate
            if (!documents.isEmpty())
            {
                List<File>outputFileList = new ArrayList<>();
                for (Document document : documents)
                {
                    for(DocumentFile documentFile : document.getOutputDocuments())
                    {
                        outputFileList.add(documentFile.getOutputFile());
                    }
                }
                // Concatenate the list of files found into a single pdf file and stream to the browser
                PDFConcatenator pdfConcatenator = new PDFConcatenator();
                pdfConcatenator.concatenateFiles(outputFileList, response.getOutputStream());
            }
            return null;
        }
        else // Stream a single file
        {
            if (!documentAccess.isAuthorized(userId, filePath))
            {
                throw new AccessDeniedException("User " + userId + "not authorized to view " + filePath);
            }

            // If this is a deans final decision, deans or joint deans recommendation, check to concatenate with any files
            // which may be in the concatenateFiles variable
            if ((pathConstructor.getFileName().toLowerCase().startsWith("raf") ||
                  pathConstructor.getFileName().toLowerCase().startsWith("deansrecommendation") ||
                 pathConstructor.getFileName().toLowerCase().startsWith("j_deansrecommendation")) &&
                concatenateFiles != null)
            {
                // Split the comma separated list of files to concatenate
                String [] fileListArr =  concatenateFiles.split(",");
                ArrayList<File>fileList =  new ArrayList<File>();
                // Add the remaining files from the concatenate files string
                for (String filename : fileListArr)
                {
                    fileList.add(new File(filename));
                }
                // Add the URL filepath last
                fileList.add(filePath);
                File tempOutput = File.createTempFile(pathConstructor.getFileName(), null);
                PDFConcatenator pdfConcatenator = new PDFConcatenator();
                pdfConcatenator.concatenateFiles(fileList, tempOutput);
                filePath = tempOutput;
            }
            return streamFile(request, response, filePath);
        }
    }


    /**
     * Display Access Denied page
     * @return ModelAndView
     */
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    private ModelAndView whenAccessDenied(Exception e)
    {
        log.info("ACCESS DENIED", e);

        return new ModelAndView("accessdenied");
    }


    private ModelAndView streamFile(HttpServletRequest request, HttpServletResponse response, File filePath)
    {
        ServletOutputStream out = null;

        try ( InputStream in = new BufferedInputStream(new FileInputStream(filePath)) )
        {
            out = response.getOutputStream();
            byte[] buf = new byte[BUFFER_SIZE];
            int bytesRead;

            while ((bytesRead = in.read(buf)) != -1)
            {
                out.write(buf, 0, bytesRead);
            }

            out.flush();
        }
        catch (IOException ioex)
        {
            log.error("I/O Error while streaming file [" + filePath.getAbsolutePath() + "]", ioex);
            throw new MivSevereApplicationError("errors.FileViewer.IOError", "FileViewerController");
        }

        return null;
    }
}
