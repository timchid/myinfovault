package edu.ucdavis.myinfovault.document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.data.DataFetcher;
import edu.ucdavis.myinfovault.data.DataMap;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;
import edu.ucdavis.myinfovault.data.SectionFetcher;

public class PacketBuilder extends Builder implements DocumentBuilder
{

    private static final String DOCUMENTQUERY = "SELECT Document.ID, Description, FileName, DossierFileName, ConcatUpload"
        + " FROM CollectionDocument, Document" + " WHERE CollectionDocument.DocumentID = Document.ID" + " AND CollectionID = ?"
        + " ORDER BY Sequence";

    private static final String PRINTCONTRIBUTIONSQUERY = "SELECT *  FROM PublicationSummary WHERE UserId = ? AND Contribution != '' "+
                                " AND Display = 1 AND DisplayContribution = 1";

    private static final String PRINTPATENTCONTRIBUTIONSQUERY = "SELECT *  FROM PatentSummary WHERE UserId = ? AND Contribution != '' "+
                                " AND Display = 1 AND DisplayContribution = 1";

    private static final String PRINTWORKSCONTRIBUTIONSQUERY = " SELECT *  FROM Works  WHERE Contributors != '' "+
                                                        " AND UserID = ? AND Display = 1 AND DisplayContribution = 1 "+
                                                        " AND ( "+
                                                        "     ID IN (select WorkID from WorksEventsAssociation) "+
                                                        "  OR ID IN (select WorkID from WorksPublicationEventsAssociation)"+
                                                        " )";

    protected static final int combinedPdfId = 999;

    private static Logger log = Logger.getLogger("MIV");
    private int collectionId;
    private boolean displayContributions = true;
    private boolean displayPubContributions = true;
    private boolean displayWorksContributions = true;
    private MIVUserInfo mui = null;
    private LinkedHashMap<Integer, PacketDocument> documents = new LinkedHashMap<Integer, PacketDocument>();
    DataFetcher fetcher = null;
    long packetId = 0;

    public PacketBuilder(MIVUserInfo mui, int collectionId, long packetId)
    {
        super(BuilderDocumentType.PACKET);
        this.setUserId(mui.getPerson().getUserId());
        this.collectionId = collectionId;
        this.mui = mui;
        this.packetId = packetId;
        this.init();
    }

    public PacketBuilder(Packet packet, MivPerson mivPerson)
    {
        super(BuilderDocumentType.PACKET);
        this.setUserId(mivPerson.getUserId());
        this.collectionId = 1;
        this.mui = new MIVUser(mivPerson.getPrincipal().getPrincipalName(), mivPerson.getUserId()).getTargetUserInfo();
        this.packetId = packet.getPacketId();
        this.init();
    }

    public PacketBuilder(BuilderDocumentType buildDocumentType, Packet packet, MivPerson mivPerson)
    {
        this(buildDocumentType, packet.getPacketId(), mivPerson);
    }

    public PacketBuilder(BuilderDocumentType buildDocumentType, long packetId, MivPerson mivPerson)
    {
        super(buildDocumentType);
        this.setUserId(mivPerson.getUserId());
        this.collectionId = 1;
        this.mui = new MIVUser(mivPerson.getPrincipal().getPrincipalName(), mivPerson.getUserId()).getTargetUserInfo();
        this.packetId = packetId;
        this.init();
    }

    /**
     * Initialize the document locations and data for this packet builder
     * @param baseFileLocation - the location on the file system where the packet files will be created
     */
    private void init()
    {
        //init file paths
        this.setFilePaths(mui.getUserDir());

        // Get the dataFetcher for this user
        this.fetcher = new DataFetcher(this.mui);

        //set the documentFormats
        this.setDocumentFormats();

        // If this is a master packet, check the database for any contributions present.
        // Note that packet specific contributions will default to display and be resolved by the PacketContent filter.
        if (this.packetId == 0)
        {
            // MIV-4840 - check for patent contributions as well as publications
            this.displayPubContributions = this.getDisplayContributions(PRINTCONTRIBUTIONSQUERY) ||
                    this.getDisplayContributions(PRINTPATENTCONTRIBUTIONSQUERY);

            //get the displayWorksContributions flag
            this.displayWorksContributions = this.getDisplayContributions(PRINTWORKSCONTRIBUTIONSQUERY);
        }

        //get the document data
        this.gatherDocumentData();

    }

    /**
     * Gather the document data for all of the documents that could potentially be produced
     * initializing a map of document Ids/documents.
     */
    private void gatherDocumentData()
    {
        Connection connection = MIVConfig.getConfig().getConnection();
        PreparedStatement documentStatement = null;
        ResultSet documentSet = null;
        try
        {
            documentStatement = connection.prepareStatement(DOCUMENTQUERY);
            documentStatement.setInt(1, this.collectionId);
            documentSet = documentStatement.executeQuery();
            String baseFileName = null;
            String dossierFileName = null;
            String description = null;
            int documentID;

            // Get the document data from the database
            while (documentSet.next())
            {
                documentID = documentSet.getInt(1);
                description = documentSet.getString(2);
                baseFileName = documentSet.getString(3);
                dossierFileName = documentSet.getString(4);
                boolean concatenateUploads = documentSet.getBoolean(5);

                DocumentIdentifier documentIndentifier = DocumentIdentifier.convert(documentID);

                switch(documentIndentifier)
                {
                    case CONTRIBUTIONS_TO_JOINTLY_AUTHORED_WORKS:
                        if (!this.displayPubContributions)
                        {
                            continue;
                        }
                        this.displayContributions = this.displayPubContributions;
                        break;
                    case CONTRIBUTIONS_TO_JOINTLY_CREATED_WORKS:
                        if (!this.displayWorksContributions)
                        {
                            continue;
                        }
                        this.displayContributions = this.displayWorksContributions;
                        break;
                     default:
                     this.displayContributions = true;
                }

                // Put the info for this doc into the document map
                PacketDocument document = new PacketDocument(
                                                this.getUserId(),
                                                this.collectionId,
                                                documentID,
                                                description,
                                                baseFileName,
                                                dossierFileName,
                                                this.buildXsltFileObj(baseFileName),
                                                this.buildXmlFileObj(baseFileName),
                                                this.getOutputFilePath(),
                                                this.getUploadFilePath(),
                                                concatenateUploads,
                                                this.displayContributions,
                                                this.getDocumentFormats(),
                                                this.packetId);
                documents.put(documentID, document);
            }
            // If there are PDF document formats present, add a document for the combined PDF file
            if (this.getDocumentFormats().containsKey(DocumentFormat.PDF))
            {
                PacketDocument document = new PacketDocument(
                        this.getUserId(),
                        this.collectionId,
                        PacketBuilder.combinedPdfId,
                        "View My Packet as One PDF File",
                        "bk",
                        "dossiercombined",
                        null,
                        null,
                        this.getOutputFilePath(),
                        "",
                        false,
                        false,
                        null,
                        this.packetId);
                documents.put(PacketBuilder.combinedPdfId, document); // Document ID this.combinedPdfId is used for combined PDF packet
            }

        }
        catch (SQLException sqle)
        {
            log.log(Level.SEVERE, "Unable to gather document information to produce packet document(s):", sqle);
            sqle.printStackTrace(System.out);
        }
        finally
        {
            try
            {
                if (documentSet != null)
                {
                    documentSet.close();
                }
                if (documentStatement != null)
                {
                    documentStatement.close();
                }
                MIVConfig.getConfig().releaseConnection(connection);
            }
            catch (SQLException sqle)
            {
                // Ignore
            }
        }
    }

    /**
     * Iterate through the list of documents and create each
     * returning a list of document objects
     * @return List of documents
     */
    @Override
    public List<Document> generateDocuments()
    {
        //generate the XML  data
        List<Document>documentList = this.generateXML();

        // Iterate thought the list of documents for which there is XML data
        for (Document document : documentList)
        {
            // Skip combined pdf document
            if (document.documentId == PacketBuilder.combinedPdfId)
            {
                continue;
            }
            // Create each document format
            document.create();
        }

        // If there are pdf files present, build the URL for the combined PDF file and add to the list
        documentList = buildCombinedFileURL(documentList);

        // this.fetcher.close();
        return documentList;
    }

    /**
     * Generate the XML document for each document in the list of documents for which there is data.
     * Only documents which contain data are returned in the list
     * @return List of documents containing data
     */
    private List<Document>generateXML()
    {
        Map<String, Map<String, String>> sectionHeaderMap = null;
        XMLBuilder builder = null;
        BufferedWriter writer = null;
        DataMap documentSectionMap = null;

        // Get the sectionHeaderMap
        SectionFetcher sf = this.mui.getSectionFetcher();
        sectionHeaderMap = sf.getHeaderMap();
        // Get the display name
        String displayName = sf.getPersonalData().getDisplayName();
        // Get the full name...First and Last
        DataMap wholeNameMap = this.fetcher.getWholeNameMap();

        // Get the datamap for each document
        Map<Document, DataMap>documentList = this.getDocuments();

        // We want to use the display name on all documents if it exists.
        if (displayName != null && displayName.length() > 0)
        {
            displayName = displayName.replaceAll("&quot;", "\"");
            wholeNameMap.put("fname", displayName);
            wholeNameMap.put("lname", "");
        }

        // Get the fieldPatternMap
        DataMap fieldPatternMap = null;
        DocumentIdentifier docIdentifier = DocumentIdentifier.INVALID;

        // Iterate through each document and build the XML file for each
        for (Document document : documentList.keySet())
        {
            fieldPatternMap = null;

            // Skip combined PDF document
            if (document.documentId == PacketBuilder.combinedPdfId)
            {
                continue;
            }

            docIdentifier = DocumentIdentifier.convert(document.documentId);

            // skip invalid documents
            if(docIdentifier == DocumentIdentifier.INVALID)
            {
                continue;
            }

            fieldPatternMap = this.mui.getUserFieldPatternMap(docIdentifier);

            // get the document section map for this document
            documentSectionMap = documentList.get(document);

            // Build the XML output
            builder = new XMLBuilder("packet", fieldPatternMap, wholeNameMap, sectionHeaderMap,
                    fetcher.getDepartment(), docIdentifier);
            builder.buildPacketXmlDoc(documentSectionMap);
            try
            {
                writer = new BufferedWriter(new FileWriter(document.getXmlFile()));
                writer.write(builder.getXMLString());
                writer.close();
            }
            catch (IOException ioe)
            {
                log.log(Level.SEVERE, "Unable create XML output file to produce packet document(s):", ioe);
                ioe.printStackTrace(System.out);
            }
        }

        // Return the list of documents
        return new ArrayList<Document>(documentList.keySet());
    }

    /**
     * Get the documents and document map for each document in the packet. Only
     * documents which contain data are returned in the list
     *
     * @return Map - DataMaps by Document
     */
    public Map<Document, DataMap>getDocuments()
    {
        Document document = null;
        DataMap documentSectionMap = null;
        LinkedHashMap<Document, DataMap>documentMap = new LinkedHashMap<Document, DataMap>();

        // Get the document section map for each document
        for (Integer documentId : documents.keySet())
        {
            document = documents.get(documentId);

            documentSectionMap = this.fetcher.fetch(documentId, this.packetId);

            if (this.includeDocument(documentId, documentSectionMap))
            {
                documentMap.put(document,  documentSectionMap);
            }
        }
        return documentMap;
    }

    /**
     * Check to include document in the document map for the packet.
     *
     * Master packet (packetId = 0) contributions records have already had the display status checked via a direct
     * database query, so only a basic check is done to include the document.
     *
     * Packet specific records must have a further check of contributions documents for
     * the display status which has been set based on the PacketContent record for a Packet.
     *
     * @param documentId
     * @param documentSectionMap
     * @return boolean
     */
    private boolean includeDocument(Integer documentId, DataMap documentSectionMap)
    {
        // Master packet has already qualified contributions, so just do a basic that there is something which will be displayed
        if (packetId == 0 || !(DocumentIdentifier.convert(documentId) == DocumentIdentifier.CONTRIBUTIONS_TO_JOINTLY_AUTHORED_WORKS ||
                DocumentIdentifier.convert(documentId) == DocumentIdentifier.CONTRIBUTIONS_TO_JOINTLY_CREATED_WORKS))
        {
            return documentSectionMap != null && documentSectionMap.size() > 0 || documentId == PacketBuilder.combinedPdfId;
        }

        // If processing any of the contributions documents, make sure there will actually be something to display.
        // If not, do not add to documents map
        else if ((DocumentIdentifier.convert(documentId) == DocumentIdentifier.CONTRIBUTIONS_TO_JOINTLY_AUTHORED_WORKS ||
                DocumentIdentifier.convert(documentId) == DocumentIdentifier.CONTRIBUTIONS_TO_JOINTLY_CREATED_WORKS) &&
                documentSectionMap != null && documentSectionMap.size() > 0)
        {
            // Check if there will be any contributions to print
            for (String key : documentSectionMap.keySet())
            {
                DataMap map = (DataMap)documentSectionMap.get(key);
                for (String mapKey : map.keySet())
                {
                    // Skips annotations-legends records
                    if (!(map.get(mapKey) instanceof LinkedList))
                    {
                        continue;
                    }

                    List<DataMap> recordList = (List<DataMap>)map.get(mapKey);
                    for (DataMap recordMap : recordList)
                    {
                        String displayContribution = (String) recordMap.get("displaycontribution");
                        if (displayContribution != null && displayContribution.equals("true"))
                        {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Iterate through the list of documents for there is data present
     * and check for the presence of each document file returning a list
     * of document objects
     * @return List of documents
     */
    @Override
    public List<Document> getDocumentList()
    {
        ArrayList<Integer>removeDocuments = new ArrayList<Integer>();

        // Iterate through the list of documents for which there is data
        // and get the files
        for (Integer documentId : this.documents.keySet())
        {
            // Skip the combined document
            if (documentId == PacketBuilder.combinedPdfId)
            {
                continue;
            }

            // Get the files for all documents
            documents.get(documentId).collectDocumentFiles();

            // If there are no output documents, save the document to remove to a list
            if (documents.get(documentId).getOutputDocuments().isEmpty())
            {
                removeDocuments.add(documentId);
            }
        }

        // Remove any documents identified above
        for (Integer documentId : removeDocuments)
        {
            documents.remove(documentId);
        }

        // If not 2 or more, remove the combined PDF file document
        if (this.documents.values().size() <=2 )    // 2 accounts for the combined PDF file doc itself
        {
            this.documents.remove(PacketBuilder.combinedPdfId);
        }

        return new ArrayList<Document>(this.documents.values());
    }

    /**
     * buildCombinedFileURL - Build the document URLfor the combined pdf output files
     * @param documentList list of documents to check for pdf files
     * @return documentList updated with new combined file if needed
     */
    private List<Document>buildCombinedFileURL(List<Document>documentList)
    {
        LinkedList<String>pdfFileList = new LinkedList<String>();

        // Check for output pdf files and concatenate
        // together into a single packet document
        for (Document document : documentList)
        {
            for (DocumentFile documentFile : document.getOutputDocuments())
            {
                if (documentFile.documentFormat == DocumentFormat.PDF)
                {
                    pdfFileList.add(documentFile.outputFile.getAbsolutePath());
                }
            }
        }

        // Add a document for the combined PDF file
        if (!pdfFileList.isEmpty() && pdfFileList.size() > 1)
        {
            PacketDocument document = this.documents.get(PacketBuilder.combinedPdfId);

            String outputFileUrl = new StringBuilder().append("/miv/documents/")
                    .append(document.userId)
                    .append("/")
                    .append(this.getDocumentType())
                    .append("/")
                    .append(document.packetId)
                    .append("/")
                    .append("packet")
                    .append(".")
                    .append(DocumentFormat.COMBINED_PDF.getFileExtension())
                    .toString();

                    // Add the document to the output file list
                    DocumentFile documentFile = new DocumentFile(true, null, outputFileUrl, DocumentFormat.COMBINED_PDF);
                    document.getOutputDocuments().add(documentFile);
        }
        // If only one or none, remove the combined PDF file document
        else
        {
            this.documents.remove(PacketBuilder.combinedPdfId);
            // Return the modified list
            documentList = this.getDocumentList();
        }
        return documentList;
    }

    /**
     * Check to see if there are any contributions set to print. This flag will be used to determine if the
     * contributions document will be produced.
     *
     * Get the display contributions flag from the database for the current user
     * @return boolean true to display contributions
     */
    private boolean getDisplayContributions(String query)
    {
        Connection connection = MIVConfig.getConfig().getConnection();
        PreparedStatement documentStatement = null;
        ResultSet documentSet = null;
        boolean display = true;
        try
        {
            documentStatement = connection.prepareStatement(query);
            documentStatement.setInt(1, this.getUserId());
            documentSet = documentStatement.executeQuery();
            // No row returned indicates do not display contributions
            if (!documentSet.next())
            {
                display = false;
            }
        }
        catch (SQLException sqle)
        {
            log.log(Level.WARNING, "Unable to check DisplayContributions flag", sqle);
            sqle.printStackTrace(System.out);
        }
        finally
        {
            try
            {
                if (documentSet != null)
                {
                    documentSet.close();
                }
                if (documentStatement != null)
                {
                    documentStatement.close();
                }
                MIVConfig.getConfig().releaseConnection(connection);
            }
            catch (SQLException sqle)
            {
                // Ignore
            }
        }
        return display;
    }

    public void setCollectionId(int collectionId)
    {
        this.collectionId = collectionId;
    }

    public void setMui(MIVUserInfo mui)
    {
        this.mui = mui;
    }
}
