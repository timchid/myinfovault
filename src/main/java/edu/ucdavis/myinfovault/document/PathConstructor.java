/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PathConstructor.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Logger;

import edu.ucdavis.myinfovault.MIVConfig;

public class PathConstructor
{
    private static Logger log = Logger.getLogger("MIV");
    private static MIVConfig cfg = MIVConfig.getConfig();

    private static final String ARCHIVE_URL_PATH = "/archive/";
    /** A path that will (likely) cause a 404 error */
    private static final File BAD_PATH = new File("/../../x$$");

    private String documentType = "dossier";

    private String fileName = "";
    private String fileExt = "";
    private int documentId;
    private int userId = -1;
//    private String fileLocDir="";


    /** Get the document directory prefix for a document type */
    private static final Map<String,String> documentPrefixMap = new HashMap<String,String>();
    static {
        documentPrefixMap.put("dossier", "d");
        documentPrefixMap.put("packet", "");
        documentPrefixMap.put("preview", "");
        documentPrefixMap.put("biosketch", "");
        // if no type was found, no prefix is used
        documentPrefixMap.put("", "");
        documentPrefixMap.put(null, "");
    }



    /**
     * Constructor for Spring to use.
     */
    public PathConstructor()
    {
        // Better call setUrlPathPrefix() later if you use this constructor
    }

    /**
     * Most Non-Spring callers should use this constructor until aop/spring-injection is all working.
     * ---maybe not. this is targetted too closely to getUrlFromPath() and ignores what getPathFromUrl needs.
     * @param urlPathPrefix
     */
    public PathConstructor(String urlPathPrefix)
    {
        this.urlPrefix = urlPathPrefix;
    }

    /**
     * Constructs physical path for the given URL.
     *
     * URLs are
     *    userDir/docType/file.ext
     * or
     *    userDir/docType/docNumber/file.ext
     *
     * @param urlStr url of the document
     * @return physical file path
     * @throws Exception
     */
    public File getPathFromUrl(String urlStr) throws Exception
    {
        log.entering("PathConstructor", "getPathFromUrl");

        urlStr = urlStr.replace(this.urlPrefix, "");

        boolean isArchive = urlStr.startsWith(ARCHIVE_URL_PATH);


        // Strip off the leading "/archive/" or "/"
        // What remains starts with NNNN which is the user ID number
        String urlPath = urlStr.replaceFirst(
                    isArchive ? ARCHIVE_URL_PATH : "/", "");


        int parsePos = 0;
        String[] pathParts = urlPath.split("/");
//        int pathLength = pathParts.length;

        // All cases will result in either 3 or 4 elements after the split, or always 2 for archives.
        // Return a bad path that will cause a 404 if this is not the case.
//        if ((isArchive && pathLength != 3) || pathLength < 3 || pathLength > 4)
//        {
//            return BAD_PATH;
//        }


        File path ;


//        if (isArchive)
//        {
//            path = new File(cfg.getProperty("document-config-archivelocation"));
//        }
//        else
//        {
            String userNumber = pathParts[parsePos++];
            if (userNumber.matches("\\d+"))
            {
                this.userId = Integer.parseInt(userNumber);
                path = cfg.getUserDir(this.userId);
            }
            else
            {
                // Wasn't a valid user number
                return BAD_PATH;
            }

            if (isArchive)
            {
                path =
                    new File(path.getAbsolutePath().replace(cfg.getProperty("document-config-baselocation"), cfg.getProperty("document-config-archivelocation")));
            }
//        }

        // We've got the base path at this point, the next part is the doctype
        String docType = pathParts[parsePos++];
        this.documentType = docType;

        // Get the configured dir for the doctype found
        String docDir = cfg.getProperty("document-config-location-" + docType);
        path = new File(path, docDir);


        // Doc type is followed by the document itself or the doc ID
//        if (isArchive)
//        {
//            String docNumber = pathParts[parsePos++];
//            if (docNumber.matches("\\d+"))
//            {
//                int docId = Integer.parseInt(docNumber);
//                path = new File(path, documentPrefixMap.get(docType) + docId);
//            }
//            else
//            {
//                return BAD_PATH;
//            }
//        }
//        else
//        {
            // If we see a number it's a document ID (dossier ID)
            String docNumber = pathParts[parsePos]; // don't increment parsePos yet
            if (docNumber.matches("\\d+"))
            {
                this.documentId = Integer.parseInt(docNumber);
                path = new File(path, documentPrefixMap.get(docType) + this.documentId);
                parsePos++; // now advance from the token we've used
            }
//        }

        // This should be the filename.ext
        String filename = pathParts[parsePos++];
        int extensionIndex = filename.lastIndexOf('.');

        // Get the extension or an empty string
        String fileext = extensionIndex > -1 ?
                filename.substring(extensionIndex + 1) : "";

        this.fileName = filename;
        this.fileExt = fileext;

//        if (!isArchive) {
            path = new File(path, fileext);
//        }

        path = new File(path, filename);

        log.exiting(this.getClass().getSimpleName(), "getPathFromUrl", path);

        return path;
    }
    // getPathFromUrl()



    /* The Other Direction --- given a path generate the appropriate URL */

    public String getUrlFromPath(String path)
    {
        return this.getUrlFromPath(new File(path));
    }

    /**
     * You hand me a physical file path and I'll build a URL for you.
     * I only know how to build URLs for two (2) paths:
     *  1. Files is a user directory
     *  2. Files in the MIV archive directory
     *
     * Paths may be in one of these forms: <ul>
     * <li>Absolute<ol>
     *   <li>/configuredRoot/userDir/docType/docNumber/fileExt/file.ext</li>
     *   <li>/configuredRoot/userDir/docType/fileExt/file.ext</li>
     *   <li>/configuredRoot/userDir/docType/file.ext</li>
     * </ol></li>
     * <li>Relative<ol>
     *   <li>userDir/docType/ (etc)</li>
     * </ol></li></ul>
     * @param f
     * @return
     */
    public String getUrlFromPath(File f)
    {
        String basePath    = cfg.getProperty("document-config-baselocation");
        String archivePath = cfg.getProperty("document-config-archivelocation");

        if (this.urlPrefix == null || this.urlPrefix.equals(""))
        {
            this.urlPrefix = cfg.getDocumentUrlRoot();
        }

        boolean isArchive = false;
        String url = null;

        // If the file they've passed doesn't exist, maybe it's a path/file
        // that's relative to the configured base location.
        // Prepend the base path and see if _that_ path/file exists. If it
        // does, that's the path we'll use to construct the URL; otherwise
        // make a URL from the path given to us even though it will
        // probably be a 404
        if (! f.exists())
        {
            File test = new File(basePath, f.getPath());
            if (test.exists())
            {
                f = test;
            }
        }


        // Strip off the configured base/archive path location leaving just the
        // relative path/file from there. THAT is what a URL is based upon.
        String path = f.getPath();

        if (f.isAbsolute())
        {
            String replacePath = "";
            if (path.startsWith(archivePath))
            {
                isArchive = true;
                replacePath = archivePath;
            }
            else if (path.startsWith(basePath))
            {
                replacePath = basePath;
            }

            path = path.replaceFirst(replacePath + File.separatorChar, "");
            f = new File(path);
        }

        // Sanity check:
        // This path better start with a user dir, else something's wrong
        if (/*!isArchive  && */ !path.matches("u\\d+/.*"))
        {
            String msg = "Relative physical path doesn't start with a user directory: ["+path+"]";
            log.warning(msg);
        }

        // Sanity check:
        // Paths that we can build URLs for have either 3 or 4 path components
        // FIXME: put a check in here,
        //    maybe parts[] = path.split(File.separatorChar)
        //      then parts.length better be 3 or 4


        // We'll push the path components as we determine them, starting
        // with the filename.
        Stack<String> pathParts = new Stack<String>();

        // At this point, "f" represents the partial relative path AFTER
        // the configured base location. The first component better be
        // the user's directory: u12345

        // Get the file name and figure out the file extension if there is one

        String filename = f.getName();          // get the filename
        File remainder = f.getParentFile();     // and trim it from the path
        pathParts.push(filename);


        // Get the extension or an empty string
        int extensionIndex = filename.lastIndexOf('.');
        String fileext = extensionIndex > -1 ?
                filename.substring(extensionIndex + 1) : "";


        // Last component of path -- will be <file-ext> or "dossier", "biosketch" etc. OR d#### etc.
        String subdir = remainder.getName();
        // if subdir==fileext we'll leave it out of the url

        if (fileext.equalsIgnoreCase(subdir))
        {
            // Don't push this; the URL doesn't include the filetype.
            // Move on to the next component instead
            remainder = remainder.getParentFile();
            subdir = remainder.getName();
        }

        // Now deal with d#### or doctype
        if (subdir.matches("[A-Za-z]+\\d+"))
        {
            // It's a document number
            // save it for later because it _precedes_ the doctype in the URL !NO it doesn't. hmmmmmmm
            Integer documentID = Integer.parseInt(subdir.replaceFirst("[A-Za-z]+", ""));
            pathParts.push(documentID.toString());
            remainder = remainder.getParentFile();
            subdir = remainder.getName();
        }

        // This should be the doctype now in any case
        pathParts.push(subdir);

        // If this is an archive we're done, otherwise there should be a user dir left.
//        if (!isArchive)
//        {
            remainder = remainder.getParentFile();
            subdir = remainder.getName();

            // This better be the user dir
            if (subdir.matches("[A-Za-z]+\\d+"))
            {
                Integer userID =  Integer.parseInt(subdir.replaceFirst("[A-Za-z]+", ""));
                pathParts.push(userID.toString());
            }
            else
            {
                // something went wrong !?!
                log.warning("getUrlFromPath parsing of ["+f.getPath()+"] was wrong somehow");
            }
//        }

        StringBuffer b = new StringBuffer(this.urlPrefix);
        if (isArchive) {
            b.append("/archive");
        }
        while (! pathParts.empty())
        {
            b.append("/").append(pathParts.pop());
        }
        url = b.toString();

        return url;
    }
    // getUrlFromPath()


    private String urlPrefix = "";
    public void setUrlPathPrefix(String prefix)
    {
        this.urlPrefix = prefix;
    }



    /*
     * getFileName
     * getter for the fileName
     * @return String - file name
     */
    public String getFileName()
    {
        return this.fileName;
    }

    /*
     * getFileExt
     * getter for the fileExt
     * @return String - file extension
     */
    public String getFileExt()
    {
        return this.fileExt;
    }

    /*
     * getUserId
     * getter for the userId
     * @return String - user id
     */
    public int getUserId()
    {
        return this.userId;
    }

    /*
     * getDocumentId
     * getter for the documentId (dossierId)
     * @return int - document id
     */
    public int getDocumentId()
    {
        return this.documentId;
    }

    /*
     * getDocType
     * getter for the docType
     * @return String - document type
     */
    public String getDocType()
    {
        return documentType;
    }

    /*
     * getFileLocDir
     * getter for the fileLocDir
     * @return String - directory name where file is located
     */
//    public String getFileLocDir()
//    {
//        return fileLocDir;
//    }


    // Unit Testing ...
    public void testUrls()
    {
        class Pair
        {
            String input = "";          // given this input string
            String expected = "";       // this is the expected output

            Pair(String input, String expected)
            {
                this.input = input;
                this.expected = expected;
            }

            String getInput() {
                return this.input;
            }

            String getExpectedOutput() {
                return this.expected;
            }

            @Override
            public String toString() {
                return "[" + this.input + ", " + this.expected + "]";
            }
        }

        // These test paths depend on the configuration being set as:
        //   document-config-baselocation=/opt/ucd/myinfovault
        //   document-config-archivelocation=/opt/ucd/mivarchives
        String[] paths = {
                "/opt/ucd/myinfovault/u18099/dossier/d2706/pdf/raf.pdf",
                "/opt/ucd/myinfovault/u18099/dossier/pdf/PkGt18099.pdf",
                "/opt/ucd/myinfovault/u18099/biosketch/pdf/biosketch1104_18099.pdf",
                "/u18099/dossier/pdf/PkPub18099.pdf",
                "u18099/dossier/pdf/PkPub18099.pdf",
                "u18099/dossier/d2706/pdf/raf.pdf",
                "/opt/ucd/mivarchives/dossier/d2706/snap71.pdf"
        };

        Pair[] pathPairs = new Pair[paths.length];
        int pos = 0;
        pathPairs[pos] = new Pair(paths[pos], "/miv/documents/18099/dossier/2706/raf.pdf"); pos++;
        pathPairs[pos] = new Pair(paths[pos], "/miv/documents/18099/dossier/PkGt18099.pdf"); pos++;
        pathPairs[pos] = new Pair(paths[pos], "/miv/documents/18099/biosketch/biosketch1104_18099.pdf"); pos++;
        pathPairs[pos] = new Pair(paths[pos], "/miv/documents/18099/dossier/PkPub18099.pdf"); pos++;
        pathPairs[pos] = new Pair(paths[pos], "/miv/documents/18099/dossier/PkPub18099.pdf"); pos++;
        pathPairs[pos] = new Pair(paths[pos], "/miv/documents/18099/dossier/2706/raf.pdf"); pos++;
        pathPairs[pos] = new Pair(paths[pos], "/miv/documents/archive/dossier/2706/snap71.pdf"); pos++;

        for (Pair p : pathPairs)
        {
            String path = p.getInput();
            String url = this.getUrlFromPath(path);
            String expectedUrl = p.getExpectedOutput();

            if (url.equals(expectedUrl)) {
                System.out.printf("URL for path [%s] is [%s]%n", path, url);
            }
            else {
                System.out.printf("!! for path [%s] expected [%s] but got [%s]%n", path, expectedUrl, url);
            }
        }
    }
    // done for testing only
}
