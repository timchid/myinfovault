/**
 * Copyright
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: XSLTBuilder.java
 */
package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao;
import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchStyleDao;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchType;
import edu.ucdavis.myinfovault.data.DisplaySection;
// Deprecated classes
//import org.apache.xml.serialize.OutputFormat;
//import org.apache.xml.serialize.XMLSerializer;

/**
 * This class dynamically builds the XSLT file which will be used to transform XML
 * into XSL-FO output in preparation for generating biosketch document.
 */
public class XSLTBuilder
{
    private Biosketch biosketch = null;;
    private BiosketchStyle biosketchStyle = null;;
    private Iterable<BiosketchAttributes> biosketchAttributes = null;;
    private Document document = null;
    private File filepath = null;
    private File xsltTemplate = null;
    private List<String> includeFileList = null;
    private static Logger log = Logger.getLogger("MIV");

    /**
     * @param bio 
     * @param path 
     *
     */
    public XSLTBuilder(Biosketch bio, File path)
    {
        biosketch = bio;
        biosketchStyle = biosketch.getBiosketchStyle();
        biosketchAttributes = biosketch.getBiosketchAttributes();
        filepath = path;
        this.setUpDocument();
    }

    /**
     * @param bio 
     * @param path 
     * @param xsltTemplate 
     *
     */
    public XSLTBuilder(Biosketch bio, File path, File xsltTemplate)

    {
        biosketch = bio;
        biosketchStyle = biosketch.getBiosketchStyle();
        biosketchAttributes = biosketch.getBiosketchAttributes();
        filepath = path;
        this.xsltTemplate = xsltTemplate;
        this.setUpDocument(this.xsltTemplate);
        this.includeFileList = this.gatherXsltIncludeFiles(xsltTemplate);

    }

    /**
     * Creates an empty XML document
     */
    private void setUpDocument()
    {
        try
        {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            this.document = documentBuilder.newDocument();
        }
        catch (ParserConfigurationException pcex)
        {
            log.log(Level.SEVERE, "Unable create DOM document to produce XLST", pcex);
            pcex.printStackTrace();
        }
    }

    /**
     * Creates a document from an existing XML file
     *
     * @param xsltTemplate xml file
     */
    private void setUpDocument(File xsltTemplate)
    {
        try
        {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            this.document = documentBuilder.parse(xsltTemplate);
            this.document.getDocumentElement().normalize();
        }
        catch (ParserConfigurationException pcex)
        {
            log.log(Level.SEVERE, "Unable create DOM document to produce XLST", pcex);
            pcex.printStackTrace();
        }
        catch (FileNotFoundException fnfe)
        {
            log.log(Level.SEVERE, "Unable to open input XSLT template to produce XLST", fnfe);
            fnfe.printStackTrace();
        }
        catch (IOException ioe)
        {
            log.log(Level.SEVERE, "Unable to open input XSLT template to produce XLST", ioe);
            ioe.printStackTrace();
        }
        catch (SAXException se)
        {
            log.log(Level.SEVERE, "Unable parse XML output file to produce XLST", se);
            se.printStackTrace();
        }

    }

    /**
     * Builds the output xslt file and writes to the specified file path
     */
    public void buildBiosketchOptions()
    {
        Element rootElement = this.setXsltDocumentHeader();

        // Build biosketch options based on biosketchType
        if (biosketch.getBiosketchType() == BiosketchType.CV)
        {
            rootElement = generateCVOptions(rootElement);
        }
        else
        {
            rootElement = generateNIHOptions(rootElement);
        }

        // If this was a new document (no nodes) , add the root element
        if (!this.document.hasChildNodes())
        {
            this.document.appendChild(rootElement);
        }

        try
        {

//            // Below commented code uses depreacted classes XMLSerializer and OutputFormat
//            FileWriter fos = new FileWriter(this.filepath.getAbsolutePath());
//            OutputFormat of = new OutputFormat("XML", "UTF-8", true);
//            of.setIndent(1);
//            of.setIndenting(true);
//            XMLSerializer serializer = new XMLSerializer(fos, of);
//            // As a DOM Serializer
//            serializer.asDOMSerializer();
//            serializer.serialize(this.document.getDocumentElement());

            // Below code uses replacement for depreacted classes XMLSerializer and OutputFormat
            DOMImplementationRegistry registry;
                registry = DOMImplementationRegistry.newInstance();

            DOMImplementationLS impl =
                (DOMImplementationLS)registry.getDOMImplementation("LS");

            LSSerializer writer = impl.createLSSerializer();
            LSOutput output = impl.createLSOutput();
            output.setEncoding("UTF-8");
            FileOutputStream fos = new FileOutputStream(this.filepath.getAbsolutePath());
            output.setByteStream(fos);
            writer.write(this.document.getDocumentElement(), output);
            fos.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Add NIH specific biosketch options to the XSLT document
     */
    private Element generateNIHOptions(Element rootElement)
    {
        // Add specifiic formatting for this biosketch
        // Document title
        Element e = buildNode("param", biosketch.getTitle(), "document.title");
        rootElement.appendChild(e);

        // Process Biosketch attributes
        for (BiosketchAttributes attribute : biosketchAttributes)
        {
            // Only add the attribute if it is to be displayed
            if (attribute.isDisplay())
            {
                e = buildNode("param", attribute.getValue(), attribute.getName());
                rootElement.appendChild(e);
            }
        }
        rootElement.appendChild(e);

        return rootElement;
    }

    /**
     * Add CV specific biosketch options to the XSLT document
     */
    private Element generateCVOptions(Element rootElement)
    {
        // Add specifiic formatting for this biosketch
        // Document title
        Element e = buildNode("param", biosketch.getTitle(), "document.title");
        rootElement.appendChild(e);
        // Formatting for document title Bold,Italic,Underline;Lines above and
        // below;ALignment
        e = buildNode("attribute-set", null, "document.title.format");
        if(biosketchStyle.isTitleRules())
        {
            e.appendChild(buildNode("attribute", "thin solid black", "border-top"));
            e.appendChild(buildNode("attribute", "thin solid black", "border-bottom"));
        }
        if(biosketchStyle.isTitleBold())
        {
            e.appendChild(buildNode("attribute", "bold", "font-weight"));
        }
        if(biosketchStyle.isTitleItalic())
        {
            e.appendChild(buildNode("attribute", "italic", "font-style"));
        }
        if(biosketchStyle.isTitleUnderline())
        {
            e.appendChild(buildNode("attribute", "underline", "text-decoration"));
        }
        e.appendChild(buildNode("attribute", biosketchStyle.getTitleAlign().toString().toLowerCase(), "text-align"));
        rootElement.appendChild(e);

        e = buildNode("attribute-set", null, "header.format");
        if(biosketchStyle.isHeaderBold())
        {
            e.appendChild(buildNode("attribute", "bold", "font-weight"));
        }
        if(biosketchStyle.isHeaderItalic())
        {
            e.appendChild(buildNode("attribute", "italic", "font-style"));
        }
        if(biosketchStyle.isHeaderUnderline())
        {
            e.appendChild(buildNode("attribute", "underline", "text-decoration"));
        }
        e.appendChild(buildNode("attribute", biosketchStyle.getHeaderAlign().toString().toLowerCase(), "text-align"));
        if(!biosketchStyle.getHeaderAlign().toString().toLowerCase().equals("center"))
        {
            e.appendChild(buildNode("attribute", String.valueOf(biosketchStyle.getHeaderIndent()) + "pt", "margin-left"));
        }
        rootElement.appendChild(e);


        // DisplayName "displayname.indent"
        e = buildNode("attribute-set", null, "display.name.format");
        e.appendChild(buildNode("attribute", biosketchStyle.getDisplayNameAign().toString().toLowerCase(), "text-align"));
        rootElement.appendChild(e);

        // Headers Formatting "header.format"
        // Bold,Italic,Underline;Indent;ALignment

        // Content formatting "content.indent" Indent
        e = buildNode("param", String.valueOf(biosketchStyle.getBodyIndent()) + "pt", "content.indent");
        rootElement.appendChild(e);
        // Footer
        e = buildNode("param", String.valueOf(biosketchStyle.isFooterPageNumbers()), "pagenumbers");
        rootElement.appendChild(e);
        e = buildNode("attribute-set", null, "footer.format");
        if (biosketchStyle.isFooterRules())
        {
            e.appendChild(buildNode("attribute", "thin solid black", "border-top"));
            rootElement.appendChild(e);
        }
        // Process Biosketch attributes
        for (BiosketchAttributes attribute : biosketchAttributes)
        {
            // Add the display flag for each attribute
            e = buildNode("param", attribute.isDisplay() ? "true" :"false", attribute.getName());
            rootElement.appendChild(e);

            // Add the value for the employee_id
            if (attribute.getName().equals("employee_id") && attribute.isDisplay())
            {
                e = buildNode("param", attribute.getValue(), "employeeID");
                rootElement.appendChild(e);
            }
        }

        // Process section headers and store the state in the XSLT as gobal parameters
        Iterable<BiosketchSection>biosketchSections = biosketch.getBiosketchSections();

        // Build a map of DisplaySection names and record counts
        Iterable<DisplaySection>biosketchDisplaySections = biosketch.getBiosketchData().getDisplaySectionList();
        LinkedHashMap<String, String>displaySectionMap = new LinkedHashMap<String, String>();
        for (DisplaySection displaySection : biosketchDisplaySections)
        {
            displaySectionMap.put(displaySection.getKey(),Integer.toString(displaySection.getRecords().size()));
        }
        for (BiosketchSection biosketchSection : biosketchSections)
        {
            // Only process biosketch header sections or sections which have entries in the displaySectionMap
            if (displaySectionMap.containsKey(biosketchSection.getSectionName()) || biosketchSection.getSectionName().endsWith("-header"))
            {
                if (biosketchSection.isDisplay())
                {
                    // Don't check record count for headers and additional information
                    if (biosketchSection.getSectionName().endsWith("-header") || biosketchSection.getSectionName().endsWith("-additional"))
                    {
                        e = buildNode("param", biosketchSection.isDisplayHeader() ? "true" :"false", biosketchSection.getSectionName());
                    }
                    else
                    {
                        //String recordCount = displaySectionMap.get(biosketchSection.getSectionName());
                        //e = buildNode("param", biosketchSection.isDisplayHeader() && recordCount != null && Integer.parseInt(recordCount) > 0 ? "true" :"false", biosketchSection.getSectionName());
                        e = buildNode("param", biosketchSection.isDisplayHeader() ? "true" :"false", biosketchSection.getSectionName());
                    }
                }
                rootElement.appendChild(e);
            }
        }
        return rootElement;
    }

    /**
     * Builds an attribute map
     *
     * @param key
     * @param value
     * @return A map with key value pair attributes
     */
    public HashMap<String, String> buildArributeMap(String key, String value)
    {
        HashMap<String, String> attributeMap = new HashMap<String, String>();
        attributeMap.put(key, value);
        return attributeMap;
    }

    /**
     * Creates an XML node
     *
     * @param type
     * @param text
     * @param attributeName
     * @return Element the element created
     */
    public Element buildNode(String type, String text, String attributeName)
    {
        // builds an xsl node
        Element e = null;
        Node n = null;
        e = this.document.createElement("xsl:" + type);
        if (attributeName != null)
        {
            e.setAttribute("name", attributeName);
        }

        if (text != null)
        {
            n = this.document.createTextNode(text);
            e.appendChild(n);
        }
        return e;
        // document.appendChild(e);
    }

    /**
     * Set up the XML document header removing any current import or include
     * nodes which will be replaced with imports
     *
     * @return Element the root element of the document
     */
    private Element setXsltDocumentHeader()
    {
        Element rootElement = null;

        if (!document.hasChildNodes())
        {
            rootElement = document.createElement("xsl:stylesheet");
            rootElement.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
            rootElement.setAttribute("xmlns:fo", "http://www.w3.org/1999/XSL/Format");
            rootElement.setAttribute("version", "1.0");
        }

        rootElement = this.document.getDocumentElement();

        // Remove any exisiting import statements in the current document and
        // add new ones from the list of include files. If there are no include
        // files in the list, skip the removal.
        if (this.includeFileList != null)
        {

            NodeList nodeList = rootElement.getChildNodes();
            // Remove any imports which may currently be present
            for (int i = 0; i < nodeList.getLength(); i++)
            {
                Node node = nodeList.item(i);
                if (node.getNodeName().equalsIgnoreCase("xsl:import") || node.getNodeName().equalsIgnoreCase("xsl:include"))
                {
                    rootElement.removeChild(node);
                }

            }
            // Add the include files
            for (String includeFile : this.includeFileList)
            {
                Element newElement = this.document.createElement("xsl:import");
                newElement.setAttribute("href", includeFile);

                Node node = rootElement.getFirstChild();
                rootElement.insertBefore(newElement, node);
            }
        }

        return rootElement;
    }

    /**
     * Gathers a list of all xslt files recursively in the given file path
     * @param directory
     * @return List of all files
     */
    private List<String> gatherXsltIncludeFiles(File path)
    {
        String parent = null;

        if (path.isFile())
        {
            parent = path.getParent();
        }
        else
        {
            parent = path.getAbsolutePath();
        }

        String [] files = new File(parent).list();
        List<String>fileList = new ArrayList<String>();

        for (String file : files)
        {
            File fileObj = new File(parent,file);
            if (fileObj.isDirectory())
            {
                fileList.addAll(gatherXsltIncludeFiles(fileObj));
            }
            else
            {
                // Only add xslt files and skip the template itself
                if (file.endsWith(".xslt") &&
                   (this.xsltTemplate != null && !this.xsltTemplate.getName().equalsIgnoreCase(file)))
                {
                    fileList.add(new File(parent,file).getAbsolutePath());
                }
            }
        }
        return fileList;
    }

    /**
     * For testing Main.
     * @param args command-line arguments
     */
    public static void main(String[] args)
    {
        ClassPathXmlApplicationContext util = new ClassPathXmlApplicationContext(
                new String[] { "/edu/ucdavis/mw/myinfovault/dao/biosketch/test-myinfovault-data.xml" });

        BiosketchDao dao = (BiosketchDao) util.getBean("biosketchDao");
        BiosketchStyleDao styledao = (BiosketchStyleDao) util.getBean("biosketchStyleDao");
        try
        {
            int id = 2;
            Biosketch bio = dao.findBiosketch(id);
            bio.setBiosketchStyle(styledao.findBiosketchStyleById(id));
            XSLTBuilder xmlclass = new XSLTBuilder(bio, new File("xslt/CV/style-options.xslt"));
            xmlclass.buildBiosketchOptions();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }
}
