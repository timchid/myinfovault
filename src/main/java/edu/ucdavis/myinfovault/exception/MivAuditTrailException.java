/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivAuditTrailException.java
 */
package edu.ucdavis.myinfovault.exception;

/**
 * Audit Trails Exception Class
 * @author pradeeph
 * @since  MIV 4.0
 */
public class MivAuditTrailException extends Exception
{
    private static final long serialVersionUID = 201106171152L;

    public MivAuditTrailException()
    {
        super();
    }

    public MivAuditTrailException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MivAuditTrailException(String message)
    {
        super(message);
    }

    public MivAuditTrailException(Throwable cause)
    {
        super(cause);
    }
}
