/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ServiceUnavailableException.java
 */


package edu.ucdavis.myinfovault.exception;

/**
 * TODO: Add Javadoc comments!
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class ServiceUnavailableException extends MivSevereApplicationError
{
    private static final long serialVersionUID = 200910011716L;

    private static final String DEFAULT_MESSAGE = "Service Unavailable";

    public ServiceUnavailableException()
    {
        super(DEFAULT_MESSAGE);
    }

    public ServiceUnavailableException(Throwable cause)
    {
        super(DEFAULT_MESSAGE, cause);
    }

    public ServiceUnavailableException(String message)
    {
        super(message);
    }

    public ServiceUnavailableException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
