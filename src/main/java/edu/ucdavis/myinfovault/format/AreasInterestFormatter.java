package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class AreasInterestFormatter extends FormatAdapter implements RecordFormatter
{
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder resequencePreview = new StringBuilder();

        preview .append( insertValue("header", m.get("header"), "<h3>", "</h3>") )
                .append( insertValue("content", m.get("content")) );

        resequencePreview.append( insertValue("content", m.get("content")) );

        String sequencePreview = resequencePreview.toString().replaceAll(COMMA+SINGLE_SPACE+COMMA, COMMA+SINGLE_SPACE);

        m.put("sequencepreview", sequencePreview);
        m.put("preview", preview.toString());

        return m;
    }
}
