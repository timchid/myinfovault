/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BookFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class BookFormatter extends PreviewBuilder implements RecordFormatter
{
    public BookFormatter()
    {
        this.add( new TitleFormatter(COMMA, PUB_GROUP_BOOKS) )
            .add( new PublicationNameFormatter() )
            .add( new BookVolumeIssueFormatter() )
            .add( new AuthorFormatter(COLON))
            .add( new URLFormatter(URLFormatter.PreviewType.PUBLICATION) )
        ;
    }

    @Override
    protected String buildPreview(Map<String, String> m)
    {
        //System.out.println("Book FORMATTER :: running format()");
        this.applySubformats(m);
        StringBuilder preview = new StringBuilder();

        String s = m.get("citation");
        if ( ! isEmpty(s) ) {
            preview.append(s);
        }
        else
        {
            //MIV 610 - Make sure there is a period and no COMMA after title.
            //Also there should be a period at the end.
            /*boolean editorBlank = true;
            boolean journalBlank = true;
            boolean VolumeIssueBlank = true;
            boolean publisherBlank = true;
            boolean cityBlank = true;
            boolean pagesBlank = true;*/
            preview.append( insertValue("author", m.get("author")))
                   .append( insertValue("title", m.get("title"), SINGLE_SPACE, ""))
                   .append( insertValue("editor", m.get("editor"), SINGLE_SPACE, COMMA_SPACE + "(ed)" + COMMA))
                   .append( insertValue("journal", m.get("journal"), SINGLE_SPACE, COMMA))
                   .append( insertValue("volume", m.get("VolumeIssue"), SINGLE_SPACE, ""))
                   .append( insertValue("publisher", m.get("publisher"), SINGLE_SPACE, COMMA))
                   .append( insertValue("city", m.get("city"), SINGLE_SPACE, PERIOD))
                   //.append( insertValue("pages", m.get("pages"), " pp. ", ""))
                   ;

            /*if (!isEmpty(m.get("editor"))) {
                editorBlank = false;
                preview.append(SINGLE_SPACE + m.get("editor") + COMMA_SPACE + "(ed)" + COMMA);
            }

            if (!isEmpty(m.get("journal")))
            {
                journalBlank = false;
                if (editorBlank) {
                    preview.append(SINGLE_SPACE + m.get("journal") );
                }
                else {
                    preview.append(COMMA_SPACE + m.get("journal") );
                }
            }

            if (!isEmpty(m.get("VolumeIssue")))
            {
                VolumeIssueBlank = false;
                if (editorBlank && journalBlank) {
                    preview.append(SINGLE_SPACE + m.get("VolumeIssue") );
                }
                else {
                    preview.append(COMMA_SPACE + m.get("VolumeIssue") );
                }
            }

            if (!isEmpty(m.get("publisher")))
            {
                publisherBlank = false;
                if (editorBlank && journalBlank && VolumeIssueBlank) {
                    preview.append(SINGLE_SPACE + m.get("publisher") );
                }
                else {
                    preview.append(COMMA_SPACE + m.get("publisher") );
                }
            }

            if (!isEmpty(m.get("city")))
            {
                cityBlank = false;
                if (editorBlank && journalBlank && VolumeIssueBlank && publisherBlank) {
                    preview.append(SINGLE_SPACE + m.get("city") );
                }
                else {
                    preview.append(COMMA_SPACE + m.get("city") );
                }
            }

            if (!isEmpty(m.get("pages")))
            {
                pagesBlank = false;
                if (editorBlank && journalBlank && VolumeIssueBlank && publisherBlank && cityBlank) {
                    preview.append(" pp. " + m.get("pages") );
                }
                else {
                    preview.append(". pp. " + m.get("pages") );
                }
                preview.append(PERIOD);
            }*/

            if (!isEmpty(s=m.get("pages")))
            {
                if(!isEmpty(m.get("VolumeIssue")))
                {
                    preview.append(insertValue("pages", m.get("pages"), " pp. ", ""));
                }
                else
                {
                    preview.append(insertValue("pages", m.get("pages"), " ", ""));
                }
                preview.append(PERIOD);
            }
            else
            {
                int l = preview.length();
                char lastchar = preview.charAt(l-1);

                if (lastchar == ',') {
                    preview.deleteCharAt(l-1);
                }

                if(!hasPunctuation(preview)){
                    preview.append(PERIOD);
                }
            }
        }

        //MIV - 651 - Add ** IN PRESS ** or ** SUBMITTED ** at the end.
        if (!isEmpty(m.get("statusid")))
        {
            if (m.get("statusid").equals("2"))
            {
                preview.append(" ** IN PRESS ** ");
            }
            else if (m.get("statusid").equals("3"))
            {
                preview.append(" ** SUBMITTED ** ");
            }
        }

//        if (!isEmpty(m.get("link")))
//        {
//            preview.append(m.get("link"));
//        }
        //System.out.println("preview:::"+preview.toString()+":::");
        return preview.toString();
    }
}
