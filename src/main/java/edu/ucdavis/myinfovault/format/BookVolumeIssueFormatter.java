package edu.ucdavis.myinfovault.format;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class BookVolumeIssueFormatter extends FormatAdapter implements RecordFormatter
{
    private static Logger log = Logger.getLogger("MIV");
    private static final String ALTERNATEKEY = "VolumeIssue";
    private static final Collection<String> addedFields = Arrays.asList(new String[] {ALTERNATEKEY});

    /**
     * @return The original Map with the new key "VolumeIssue" added.
     */
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        String volume, issue, combined;

        volume = m.get("volume");
        issue = m.get("issue");
        boolean hasVolume = volume != null && volume.length() > 0;
        boolean hasIssue  = issue  != null && issue.length() > 0;

        if (hasVolume && hasIssue) {
            combined = "Vol. " + volume + "(" + issue + ")" + COMMA;
        }
        else if (hasVolume) {
            combined = "Vol. " + volume + COMMA;
        }
        else if (hasIssue) {
            combined = "(" + issue + ")" + COMMA;
        }
        else {  // neither volume nor issue available
            combined = "";
        }

        log.finest("Added combined Volume/Issue string \""+combined+"\" under the key VolumeIssue");
        m.put(ALTERNATEKEY, combined);

        return m;
    }

    @Override
    public Iterable<String> getAddedFields()
    {
        return addedFields;
    }
}
