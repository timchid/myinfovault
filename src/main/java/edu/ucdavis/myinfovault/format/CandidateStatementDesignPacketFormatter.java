package edu.ucdavis.myinfovault.format;

import java.io.File;
import java.util.Map;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.document.PathConstructor;

public class CandidateStatementDesignPacketFormatter extends PreviewBuilder
{
//    private String linkPath = null;

    public CandidateStatementDesignPacketFormatter(String fileLocation)
    {
//        this.linkPath = fileLocation;
    }
    public CandidateStatementDesignPacketFormatter()
    {
    }

    @Override
    public String buildPreview(Map<String, String> m)
    {
        StringBuilder preview = new StringBuilder();

        String uploadName = "PDF Document";
        String readOnly;
        String s;

        if (m.containsKey("content"))
        {
            return "Candidate's Statement content is not displayed here, but will be in the dossier.";
        }
        else
        {
            readOnly = "true";
            if (!isEmpty(s = m.get("uploadname")))
            {
                uploadName = s;
            }

            if (!isEmpty(s = m.get("uploadfile")))
            {
//                String link = "<a href=\"" + linkPath + "/CV/" + s + "\" title=\"PDF Document\">" + uploadName + "</a>";
                // Build a URL from the file name
                int userId = Integer.parseInt(m.get("userid"));
                // Full upload file path
                File uploadFile = new File(MIVConfig.getConfig().getDossierPdfLocation(userId),s);
                // Convert the file path to a URL
                String uploadFileUrl  = new PathConstructor().getUrlFromPath(uploadFile.getAbsoluteFile());
                String link = "<a href=\"" +  uploadFileUrl + "\" title=\""+uploadName+"\">" + uploadName + "</a>";
                preview .append( isEmpty(s=m.get("uploadfile")) ? "" : link );
            }
            else
            {
                s = "";
            }

            m.put("readonly", readOnly);
        }

        return preview.toString();
    }
}
