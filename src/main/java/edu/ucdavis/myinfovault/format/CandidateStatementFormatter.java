/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CandidateStatementFormatter.java
 */


package edu.ucdavis.myinfovault.format;

import java.io.File;
import java.util.Map;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.document.PathConstructor;

public class CandidateStatementFormatter extends PreviewBuilder
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        String preview = buildPreview(m);

        m.put("preview", preview);
        m.put("sequencepreview", preview);
        m.put("year", insertValue("year", m.get("year")));

        return m;
    }

    @Override
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        String s;
        String readOnly;
        String uploadName = "PDF Document";

        m.put("year", insertValue("year", m.get("year")));
        if (m.containsKey("content"))
        {
            preview .append( insertValue("content", m.get("content")));
        }
        else
        {
            readOnly = "true";
            if ( ! isEmpty(s=m.get("uploadname")) )
            {
                uploadName = s;
            }

            if ( ! isEmpty(s=m.get("uploadfile")) )
            {
                // Build a URL from the file name
                int userId = Integer.parseInt(m.get("userid"));
                // Full upload file path
                File uploadFile = new File(MIVConfig.getConfig().getDossierPdfLocation(userId),s);
                // Convert the file path to a URL
                String uploadFileUrl  = new PathConstructor().getUrlFromPath(uploadFile.getAbsoluteFile());
                String link = "<a href=\"" +  uploadFileUrl + "\" title=\""+uploadName+"\">" + uploadName + "</a>";
                preview .append( isEmpty(s=m.get("uploadfile")) ? "" : link );
            }
            else
            {
                s = "";
            }
            m.put("readonly", readOnly);
        }

        return preview.toString();
    }
}
