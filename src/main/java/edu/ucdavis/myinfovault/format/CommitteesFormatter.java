package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class CommitteesFormatter extends PreviewBuilder
{
    @Override
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();

    //    m.put("year", insertValue("year", m.get("year")));

        // All the fields used in a preview are required - shout out if any are missing
       // preview .append( isEmpty(s=m.get("year")) ? missing(" YEAR MISSING ") : s )
        preview .append( insertValue("role", m.get("role"), NOTHING, COMMA_SPACE))
                .append( insertValue("description", m.get("description")))
                ;
        return preview.toString();
    }
}
