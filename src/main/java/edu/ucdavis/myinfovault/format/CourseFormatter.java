package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class CourseFormatter extends PreviewBuilder implements RecordFormatter
{
    @Override
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();

        // All the fields used in a preview are required - shout out if any are missing
        preview .append( insertValue("description", m.get("description")))
                .append( insertValue("coursenumber", m.get("coursenumber"), ", Course Number=", ""))
                .append( insertValue("title", m.get("title"), COMMA_SPACE, ""))
                .append( insertValue("units", m.get("units"), COMMA_SPACE+"Units=", ""))
                .append( insertValue("undergraduatecount", m.get("undergraduatecount"), COMMA_SPACE+"Undergraduate Count=", ""))
                .append( insertValue("graduatecount", m.get("graduatecount"), COMMA_SPACE+"Graduate Count=", ""))
                .append( insertValue("effort", m.get("effort"), COMMA_SPACE+"Percentage Effort=", ""))
                ;

        return preview.toString();
    }
}
