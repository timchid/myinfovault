/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DiversityStatementFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class DiversityStatementFormatter extends PreviewBuilder implements RecordFormatter
{

    /**
     * A formatter for PublicationEvents.
     * @param names An Iterable list of names to highlight in the citation. An empty list is allowed, <code>null</code> is not.
     */
    public DiversityStatementFormatter(Iterable<String> names)
    {

    }

    public DiversityStatementFormatter()
    {
        this(null);
    }

    @Override
    protected String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);
        StringBuilder preview = new StringBuilder();
        boolean hasPreview = false;

        if (!isEmptyOrBlank(m.get("teachingcontent")))
        {
            preview.append( insertValue("teachinghead", "Description/Elaboration of Diversity Activities in Teaching", "<div class=\"statementhead\">", "</div>") )
                .append( insertValue("teachingcontent", m.get("teachingcontent"), "<div class=\"content\">", "</div>"));
            hasPreview = true;
        }

        if (!isEmptyOrBlank(m.get("servicecontent")))
        {
            preview.append( insertValue("servicehead", "Description/Elaboration of Diversity Activities in University and Public Service", (hasPreview ? "<br>":"") +"<div class=\"statementhead\">", "</div>") )
                .append( insertValue("servicecontent", m.get("servicecontent"), "<div class=\"content\">", "</div>"));
            hasPreview = true;
        }

        if (!isEmptyOrBlank(m.get("researchcontent")))
        {
            preview.append( insertValue("researchhead", "Description/Elaboration of Diversity Activities in Scholarly and Creative Activities", (hasPreview ? "<br>":"") +"<div class=\"statementhead\">", "</div>") )
                .append( insertValue("researchcontent", m.get("researchcontent"), "<div class=\"content\">", "</div>"));
            hasPreview = true;
        }

        // to avoid empty [] on item list page.
        if(!hasPreview)
        {
            preview.append("<span class=\"ERR_MISSING\">At least one Description/Elaboration of Diversity Activities is required.</span>");
        }

        final String previewString = preview.toString();
        m.put("preview", previewString);
        m.put("sequencepreview", previewString);
        return previewString;
    }

    @Override
    public java.util.Map<String,String> format(java.util.Map<String,String> m)
    {
        String preview = buildPreview(m);
        m.put("preview", preview);
        m.put("sequencepreview", preview);
        m.put("year", insertValue("year", m.get("year")));

        return m;
    }
}
