package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class EKMediaFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder resequencePreview = new StringBuilder();

        // Media Type ("description") is from a drop-down and should always be present.
        preview
            .append( insertValue("title", m.get("title"), "<span class=\"prefix\">", "</span>, "))
            ;

        resequencePreview
                .append( insertValue("description", m.get("description")))
                .append( insertValue("datespan", m.get("datespan"), COMMA_SPACE, ""))
                .append( insertValue("publisher", m.get("publisher"), COMMA_SPACE, ""))
                ;

        if (!hasPunctuation(resequencePreview.toString())) {
            resequencePreview.append(PERIOD);
        }

        m.put("sequencepreview", resequencePreview.toString());
        m.put("preview", preview.append(resequencePreview).toString());

        return m;
    }
}
