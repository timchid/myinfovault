package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class EducationTrainingFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder yearfield = new StringBuilder();
        String startDate, endDate;

        startDate = insertValue("startdate", m.get("startdate"));
        endDate = insertValue("enddate", m.get("enddate"));
        m.put("remark", insertValue("remark", m.get("remark"),COMMA_SPACE, ""));
        if (!isEmpty(m.get("field"))) {
            m.put("sponsor", insertValue("sponsor", m.get("sponsor"), SINGLE_SPACE+OPEN_PARENTHESIS, CLOSE_PARENTHESIS));
        }
        else
        {
            m.put("sponsor", "");
        }

        yearfield.append(startDate).append(DASH);

        if (startDate.length() > 4 || endDate.length() > 4) {
            yearfield.append("<br>");
        }
        yearfield.append(endDate);
        m.put("year", yearfield.toString());

        preview .append( insertValue("institution", m.get("institution")))
                .append( insertValue("location", m.get("location"), COMMA_SPACE, ""))
                .append( insertValue("degree", m.get("degree"), COMMA_SPACE, ""))
                .append( insertValue("field", m.get("field"), COMMA_SPACE, ""));
//        if (!isEmpty(m.get("field"))) {
//            preview.append( insertValue("sponsor", m.get("sponsor"), SINGLE_SPACE+OPEN_PARENTHESIS, CLOSE_PARENTHESIS));
//        }

        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());

        return m;
    }
}
