package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class EmploymentHistoryFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder yearfield = new StringBuilder();
        String startDate, endDate;

        String prefix = "";

        startDate = insertValue("startdate", m.get("startdate"));
        endDate = m.get("enddate");
        m.put("remark", insertValue("remark", m.get("remark"),COMMA_SPACE, ""));

        /*if (isEmptyOrBlank(startDate)) {
            startDate = missingField("startdate");
        }*/
        // end date is not a required field, but we need to guarantee it's not null
        if (isEmpty(endDate)) {
            endDate = "";
        }

        yearfield.append(startDate);

        if (startDate.length() > 4 || endDate.length() > 4) {
            prefix = "<br>";
        }
        yearfield.append(insertValue("enddate", endDate, "-"+prefix, ""));

        m.put("year", yearfield.toString());

        preview .append( insertValue("institution", m.get("institution")))
                .append( insertValue("location", m.get("location"), COMMA_SPACE, ""))
                .append( insertValue("title", m.get("title"), COMMA_SPACE, ""))
       //MIV-1739 removing the salary info from the preview
       //       .append( insertValue("salary", m.get("salary"), COMMA_SPACE, ""))
        ;

        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());
        return m;
    }
}
