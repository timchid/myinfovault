package edu.ucdavis.myinfovault.format;

import java.util.Collections;
import java.util.Map;

/**<p>
 * A formatter that performs no formatting, to use as a container for sub-formatters.
 * This class will run the added sub-formatters in the order they were added &mdash; a behaviour
 * not guaranteed by the {@link RecordFormatter interface definition}.
 *</p>
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public final class EmptyFormatter extends FormatAdapter implements RecordFormatter
{
    public Map<String, String> format(Map<String, String> m)
    {
        return applySubformats(m);
    }

    /**
     * The EmptyFormatter does not add any fields, though subformatters may.
     */
    @SuppressWarnings({"unchecked"})
    public Iterable<String> getAddedFields()
    {
        return Collections.EMPTY_LIST; // the empty formatter never adds fields.
    }
}
