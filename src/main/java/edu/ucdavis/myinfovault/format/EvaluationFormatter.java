package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Binhtri Huynh
 * @since MIV 2.0
 */
public class EvaluationFormatter extends PreviewBuilder implements RecordFormatter
{
    // Add the Subformatters here. For Evaluation we need to format URL so add URLFormatter here.
    public EvaluationFormatter()
    {
        this.add( new URLFormatter(URLFormatter.PreviewType.EVALUATION) );
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        String preview = buildPreview(m);

        m.put("preview", preview);
        m.put("sequencepreview", preview);
        m.put("year", insertValue("year", m.get("year")));

        return m;
    }

    @Override
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);
        StringBuilder preview = new StringBuilder();

        preview
           .append( insertValue("course", m.get("course"), "", COLON + SINGLE_SPACE ))
           .append( insertValue("description", m.get("description")))
           .append(insertValue("enrollment", m.get("enrollment"), COMMA_SPACE + "Total Enrollment: ", ""))
           .append( insertValue("responsetotal", m.get("responsetotal"), COMMA_SPACE + "Total Responses: ", ""))
           .append( insertValue("percentageofreturn", m.get("percentageofreturn"), COMMA_SPACE + "% of Return: ", "%"))
           .append( insertValue("typeid", m.get("type"), SINGLE_SPACE + OPEN_PARENTHESIS, CLOSE_PARENTHESIS))
           .append( insertValue("instructorscore", m.get("instructorscore"), COMMA_SPACE + "Instructor Score: ", ""))
           .append( insertValue("coursescore", m.get("coursescore"), COMMA_SPACE + "Course Score: ", ""))
           //.append( insertValue("link", m.get("link")))
           ;

        return preview.toString();
    }
}
