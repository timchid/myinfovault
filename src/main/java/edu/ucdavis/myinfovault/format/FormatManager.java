/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FormatManager.java
 */


package edu.ucdavis.myinfovault.format;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.util.Duration;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.htmlcleaner.HtmlCleaner;
import edu.ucdavis.myinfovault.htmlcleaner.InvalidMarkupException;


/**
 * FIXME: All of FormatManager needs Javadoc
 * @author Stephen Paulsen
 * @since MIV 2.0
 *
 */
public class FormatManager
{
    private static final String thisPackageName = FormatManager.class.getPackage().getName();
    private static Logger log = LoggerFactory.getLogger(FormatManager.class);

    private MIVUserInfo mui;
    private HtmlCleaner htmlCleaner = null;
    private HtmlCleaner wysiwygCleaner = null;
    private Map<String,List<RecordFormatter>> allformatters = new HashMap<String,List<RecordFormatter>>();


    public FormatManager(MIVUserInfo userInfo)
    {
        this.mui = userInfo;
        htmlCleaner = new HtmlCleaner(thisPackageName);
        wysiwygCleaner = new HtmlCleaner(thisPackageName+".wysiwyg");
        initFormatters();
        EventDispatcher.getDispatcher().register(this);
    }


    public void reload()
    {
        log.debug("reload() -- Reloading formatters");
        allformatters.clear();
        initFormatters();
    }


    private void initFormatters()
    {
        List<HighlightPattern> patterns = mui.getHighlightPatterns();

        List<RecordFormatter> tmplist;


        /*
         * IMPORTANT!
         *
         * Notice a new ArrayList is created for each record type below.
         * The ArrayList "tmplist" CAN NOT just be re-used. Each tmplist.add()
         * would then be adding another element to an already existing List,
         * and that one-and-only list would be added to the "allformatters"
         * Map under each record-type key.
         * This would waste buckets of time because BookFormatters would be
         * run against trainee records. It would also hose the formatting
         * done where different record types used the same column names but
         * required special formatting.
         */

        // Formatter for Journals (journal-record)
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.PUBLICATIONS, patterns));
        RecordFormatter journalFormatter = new PeriodicalFormatter(/*highlights*/);
        tmplist.add(journalFormatter);

        allformatters.put("journal", tmplist);
        allformatters.put("review", tmplist);
        allformatters.put("letter-to-editor", tmplist);

        allformatters.put("abstract", tmplist);
        allformatters.put("limited", tmplist);


        // Formatter for Books (book-*-record and media-record)
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.PUBLICATIONS, patterns));
        RecordFormatter bookFormatter = new BookFormatter(/*highlights*/);
        tmplist.add(bookFormatter);

        allformatters.put("book-author", tmplist);
        allformatters.put("book-editor", tmplist);
        allformatters.put("book-chapter", tmplist);
        // Alternative Media is formatted the same as books
        allformatters.put("media", tmplist);


        // Formatter for NIH-Publications
        NIHPublicationFormatter publicationFormatter = new NIHPublicationFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.PUBLICATIONS, patterns));
        tmplist.add(publicationFormatter);
        allformatters.put("publication", tmplist);


        // Formatter for Grants (grant-record)
        GrantFormatter grantFormatter = new GrantFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(grantFormatter);

        allformatters.put("grant", tmplist);
        allformatters.put("grantnih", tmplist);


        // Formatter for Honors and Awards (honor-record)
        RecordFormatter honorFormatter = new HonorAwardFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(honorFormatter);

        allformatters.put("honor", tmplist);


        // Formatter for Trainees (trainee-record)
        RecordFormatter traineeFormatter = new TraineeFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(traineeFormatter);

        allformatters.put("trainee", tmplist);


        // Formatter for Extending Knowledge: Media [EKBPEM] (knowledge-media-record)
        RecordFormatter ekmediaFormatter = new EKMediaFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.EXTENDING_KNOWLEDGE, patterns));
        tmplist.add(ekmediaFormatter);

        allformatters.put("knowledge-media", tmplist);


        // Formatter for Extending Knowledge: Gatherings [Meetings, EKWCPSC] (knowledge-gathering-record)
        RecordFormatter ekmeetingFormatter = new EKMeetingFormatter();
        tmplist.add(new PatternFormatter(DocumentIdentifier.EXTENDING_KNOWLEDGE, patterns));
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.EXTENDING_KNOWLEDGE, patterns));
        tmplist.add(ekmeetingFormatter);

        allformatters.put("knowledge-gathering", tmplist);


        // Formatter for Extending Knowledge: Other (knowledge-other-record)
        RecordFormatter ekotherFormatter = new EKOtherFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.EXTENDING_KNOWLEDGE, patterns));
        tmplist.add(ekotherFormatter);

        allformatters.put("knowledge-other", tmplist);

        // Formatter for Extending Knowledge: Upload (knowledge-upload-record)
        RecordFormatter ekuploadFormatter = new PdfUploadFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(ekuploadFormatter);

        allformatters.put("knowledge-upload", tmplist);


        // Formatter for Teaching: Courses (course-record)
        RecordFormatter courseFormatter = new CourseFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(courseFormatter);

        allformatters.put("course", tmplist);


        // Formatter for Teaching: Lecture, Seminar, et.al. (supplement-record)
        RecordFormatter supplementFormatter = new SupplementFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(supplementFormatter);

        allformatters.put("supplement", tmplist);


        // Formatter for Presentations
        RecordFormatter presentationFormatter = new PresentationFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.PUBLICATIONS, patterns));
        tmplist.add(presentationFormatter);

        allformatters.put("presentation", tmplist);


        // Formatter for Service types (service-record)
        RecordFormatter serviceFormatter = new ServiceFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(serviceFormatter);
        allformatters.put("service", tmplist);


        // Formatter for Service types (committee-record)
        RecordFormatter committeesFormatter = new CommitteesFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(committeesFormatter);

        allformatters.put("committee", tmplist);
        allformatters.put("committee-all", tmplist);


        // Formatter for Service types (administrative activities-record)
        RecordFormatter administrativeActivitiesFormatter = new AdministrativeActivitiesFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(administrativeActivitiesFormatter);

        allformatters.put("administrativeactivity", tmplist);


        // Formatter for Service types (editorial and advisory boards-record)
        RecordFormatter editorialAndAdvisoryBoardsFormatter = new EditorialAndAdvisoryBoardsFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(editorialAndAdvisoryBoardsFormatter);

        allformatters.put("advisoryboard", tmplist);


        // Formatter for Position Description
        RecordFormatter positionFormatter = new PositionDescFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(positionFormatter);

        allformatters.put("position", tmplist);


        // Formatter for Candidate Statemnt
        RecordFormatter statementFormatter = new CandidateStatementFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(statementFormatter);

        allformatters.put("statement", tmplist);


        // Formatter for Evaluations
        RecordFormatter evaluationFormatter = new EvaluationFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(evaluationFormatter);

        allformatters.put("evaluation", tmplist);


        // Formatter for Ag Experiment Station (agstation)
        RecordFormatter agstationFormatter = new AgstationFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(agstationFormatter);

        allformatters.put("agstation", tmplist);


        // Formatter for Thesis Supervision
        RecordFormatter thesisFormatter = new ThesisFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(thesisFormatter);

        allformatters.put("thesis", tmplist);


        // Formatter for University Extension
        RecordFormatter extensionFormatter = new UniversityExtFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(extensionFormatter);

        allformatters.put("extension", tmplist);


        // Formatter for Student Advising
        RecordFormatter studentFormatter = new StudentAdvFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(studentFormatter);

        allformatters.put("student-advising", tmplist);


        // Formatter for Special Advising
        RecordFormatter specialFormatter = new SpecialAdvFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(specialFormatter);

        allformatters.put("special-advising", tmplist);


        // Formatter for Curricular Development
        RecordFormatter curriculumFormatter = new CurricularDevFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(curriculumFormatter);

        allformatters.put("curriculum", tmplist);

        // Formatter for DESII Report: Upload (desii-upload-record)
        RecordFormatter desiiuploadFormatter = new PdfUploadFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(desiiuploadFormatter);

        allformatters.put("desii-upload", tmplist);

        // Formatter for Contact Hours
        RecordFormatter contactFormatter = new ContactHourFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(contactFormatter);

        allformatters.put("contact-hours", tmplist);


        // Formatter for Education & Training
        RecordFormatter educationFormatter = new EducationTrainingFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(educationFormatter);

        allformatters.put("education", tmplist);


        // Formatter for Areas of Interest
        RecordFormatter interestFormatter = new AreasInterestFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(interestFormatter);

        allformatters.put("interest", tmplist);


        // Formatter for Licenses and Certifications
        RecordFormatter credentialFormatter = new LicensesCertificationFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(credentialFormatter);

        allformatters.put("credential", tmplist);


        // Formatter for Employment History
        RecordFormatter employmentFormatter = new EmploymentHistoryFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(employmentFormatter);

        allformatters.put("employment", tmplist);


        // Formatter for Additional Information records
        RecordFormatter noChangeFormatter = new EmptyFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(noChangeFormatter);

        allformatters.put("additional", tmplist);


        // Formatter for Additional Information Headers
        // re-use the noChangeFormatter above
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(noChangeFormatter);

        allformatters.put("additionalheader", tmplist);


        // Formatter got Patents */
        RecordFormatter patentFormatter = new PatentFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.PUBLICATIONS, patterns));
        tmplist.add(patentFormatter);
        allformatters.put("patent", tmplist);


        // Formatter for Works, alone and as associated lines
        RecordFormatter worksFormatter = new WorksFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.CREATIVE_ACTIVITIES, patterns));
        tmplist.add(worksFormatter);

        allformatters.put("works", tmplist);
        allformatters.put("eventsworks", tmplist);
        allformatters.put("publicationeventsworks", tmplist);
        allformatters.put("reviewsworks", tmplist);


        // Formatter for Events, alone and as associated lines
        RecordFormatter eventsFormatter = new EventsFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.CREATIVE_ACTIVITIES, patterns));
        tmplist.add(eventsFormatter);

        allformatters.put("events", tmplist);
        allformatters.put("worksevents", tmplist);
        allformatters.put("reviewsevents", tmplist);


        // Formatter for Reviews
        RecordFormatter reviewsFormatter = new ReviewsFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.CREATIVE_ACTIVITIES, patterns));
        tmplist.add(reviewsFormatter);

        allformatters.put("reviews", tmplist);


        // Formatter for  Publication Events
        PublicationEventsFormatter publicationEventsFormatter = new PublicationEventsFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(new PatternFormatter(DocumentIdentifier.CREATIVE_ACTIVITIES, patterns));
        tmplist.add(publicationEventsFormatter);

        allformatters.put("publicationevents", tmplist);
        allformatters.put("workspublicationevents", tmplist);


        // Formatter for  Candidate's Diversity Statement
        DiversityStatementFormatter diversityStatementFormatter = new DiversityStatementFormatter();
        tmplist = new ArrayList<RecordFormatter>();
        tmplist.add(diversityStatementFormatter);

        allformatters.put("diversitystatement", tmplist);
    }
    //initFormatters()


    public List<RecordFormatter> getFormatters(String rectype)
    {
        return allformatters.get(rectype);
    }


    /**
     * Apply formatting to a single record.
     *
     * @param rectype record type name of the record being passed in the input map.
     * @param record the record being formatted.
     */
    public void applyFormats(String rectype, Map<String, String>record)
    {
        applyFormats(rectype, record, (RecordFormatter[])null);
    }


    /**
     * Apply formatting to a single record.
     *
     * @param rectype record type name of the record being passed in the input map.
     * @param record the record being formatted.
     * @param postFormat optional formatters to be run after the standard formatters, or <code>null</code>.
     */
    public void applyFormats(String rectype, Map<String, String>record, RecordFormatter... postFormat)
    {
        LinkedHashMap<String, Map<String, String>> m = new LinkedHashMap<String, Map<String, String>>();
        m.put("1", record);
        this.applyFormats(rectype, m, postFormat);
    }


    /**
     * Apply formatting to a set of records.
     * The record type name is used to select the formatter(s) to run.
     *
     * @param rectype record type name of the records being passed in the input map.
     * @param m the input map which is modified by the formatters
     */
    public void applyFormats(String rectype, LinkedHashMap<String, Map<String, String>> m)
    {
        applyFormats(rectype, m, (RecordFormatter[])null);
    }


    /**
     * Apply formatting to a set of records.
     * The record type name is used to select the formatter(s) to run.
     *
     * @param rectype record type name of the records being passed in the input map.
     * @param m the input map which is modified by the formatters
     * @param postFormat optional formatters to be run after the standard formatters, or <code>null</code>.
     */
    public void applyFormats(String rectype, LinkedHashMap<String, Map<String, String>> m, RecordFormatter... postFormat)
    {
        log.trace("Applying formatting for record type \"{}\"", rectype);
        List<RecordFormatter> formats = this.getFormatters(rectype);

        if (formats == null)
        {
            log.warn("No formatters found for record type [{}]: using generic record dump.", rectype);
            formats = new LinkedList<RecordFormatter>();
            formats.add(genericFormatter);
        }

        long startTime = System.currentTimeMillis();

        Properties p = PropertyManager.getPropertySet(rectype, "config");
        Collection<String> markupFieldsSet = Collections.emptyList();
        Collection<String> translateFieldsSet = Collections.emptyList();

        boolean allowMarkup = Boolean.parseBoolean(p.getProperty("allowmarkup"));
        if (allowMarkup)
        {
            String markupFields = p.getProperty("markupfields");
            markupFieldsSet = markupFields == null ? null : convertToList(markupFields);
        }

        String translateFields = p.getProperty("translatenewline");

        boolean showFormat = log.isDebugEnabled();

        for (String key : m.keySet())
        {
            Map<String,String> record = m.get(key);

            cleanMarkup(record, markupFieldsSet); // 2007-12-04 SDP
            if (translateFields != null && translateFields.length() > 0)
            {
                translateFieldsSet = translateFields == null ? null : convertToList(translateFields);
                translateNewLines(record, translateFieldsSet);
            }
            for (RecordFormatter f : formats)
            {
                if (showFormat) {
                    log.debug("Running formatter [{}][{}]", f.getClass().getSimpleName(), f.getClass().getName());
                }
                Map<String,String> fmap = Collections.emptyMap();
                try {
                    f.setRecType(rectype);
                    fmap = f.format(record);
                }
                catch (Exception e) {
                    // Catch *anythying* that goes wrong, log it, and display an error to the user.
                    log.error("An error occurred while formatting this \"" + rectype + "\" record: [" + record + "]" +
                               " with the formatter [" + f.getClass().getSimpleName() + "]\n" +
                               "    " + e + "\n" +
                               " **** THIS IS A PROGRAMMING ERROR! ****" + "\n" +
                               " **** " + f.getClass().getSimpleName() + " MUST BE FIXED! ****"
                               , e);
                    fmap = errorFormatter.format(record);
                }
                m.put(key, fmap);
            }
        }

        if (postFormat != null)
        {
            for (RecordFormatter postFormatter : postFormat)
            {
                if (postFormatter != null)
                {
                    if (showFormat) {
                        log.debug("Applying post-formatter [{}][{}]", postFormat.getClass().getSimpleName(), postFormat.getClass().getName());
                    }
                    for (String key : m.keySet())
                    {
                        Map<String,String> record = m.get(key);
                        Map<String,String> fmap = postFormatter.format(record);
                        m.put(key, fmap);
                    }
                }
            }
        }

        if (MIVConfig.getConfig().getDoTimings())
        {
            Duration duration = Duration.elapsed(startTime, System.currentTimeMillis());
            long seconds = duration.as(TimeUnit.SECONDS);
            long msecs = duration.as(TimeUnit.MILLISECONDS) % 1000;
            log.trace(" formatting records took {}.{} seconds", seconds, msecs);
        }
    }
    //applyFormats()


    private Collection<String> convertToList(String markupFields)
    {
        String[] fields = markupFields.split(",");
        Collection<String> fieldSet = java.util.Arrays.asList(fields);
        return fieldSet;
    }


    /**
     * Escape HTML tags so they are seen, rather than acted on.
     *
     * @param record the record Map of field-name / value pairs to be modified.
     * @param markupFields a Collection keyed with field names that allow markup.
     *  <code>null</code> indicates all fields allow markup.
     */
    public boolean cleanMarkup(Map<String, String> record, Collection<String> markupFields)
    {
        boolean debug = log.isDebugEnabled();
        boolean validated = true;
        HtmlCleaner cleaner = htmlCleaner;

        for (String key : record.keySet())
        {
            String value = record.get(key);
            if (value == null || value.length() == 0) continue;

            String newValue = new String(value);
            // Decide whether the current field allows markup: if the list is 'null' then *all* fields
            // in this record allow markup, otherwise if the list of fields contains the current field
            // name then this field allows markup.
            if (markupFields == null || markupFields.contains(key))
            {
                cleaner = wysiwygCleaner;
            }
            else
            {
                // Current field doesn't allow markup: escape everything then put sup/sub tags back.
                newValue = MIVUtil.escapeMarkup(newValue);
            }


            try
            {
                newValue = cleaner.removeTags(newValue);
            }
            catch (InvalidMarkupException e)
            {
                log.info("InvalidMarkupException while handling field [{}] with contents [{}]", key, value);
                log.info("     {}", e.getMessage());
                if (debug) {
                    log.info("Backtrace follows ---", e);
                }
                validated = false;
                // Invalid XML, escape any remaining tags.
                newValue = value.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
            }

            record.put(key, newValue);
        }

        return validated;
    }
    //cleanMarkup()


    /**
     * Escape HTML tags so they are seen, rather than acted on.
     *
     * @param record the record Map of field-name / value pairs to be modified.
     * @param translateFields a Collection keyed with field names that allow markup
     * <code>null</code> indicates all fields allow markup.
     */
    public void translateNewLines(Map<String, String> record, Collection<String> translateFields)
    {
        for (String key : record.keySet())
        {
            if (translateFields.contains(key))
            {
                String value = record.get(key);
                if (value == null || value.length() == 0) continue;

                value = MIVUtil.replaceNewline(value);
                record.put(key, value);
            }
        }
    }
    //translateNewLines()


    private RecordFormatter genericFormatter = new RecordDumpFormatter();
    private class RecordDumpFormatter extends FormatAdapter
    {
        @Override
        public Map<String, String> format(Map<String, String> m)
        {
            StringBuilder preview = new StringBuilder();
            for (Map.Entry<String, String> e : m.entrySet())
            {
                String value = e.getValue();
                if (value == null) value = "";
                preview.append(e.getKey() + "&nbsp;==&gt;&nbsp;&ldquo;<strong>" + value + "</strong>&rdquo;, ");
            }
            m.put("preview", preview.toString());
            m.put("sequencepreview", preview.toString());
            return m;
        }
    }

/*
A warning should display in the preview when the formatter cannot create it:

ERROR: This record cannot be displayed due to missing/invalid data.
Please edit this record or
<a href="https://panthro.ucdavis.edu:8443/{myinfovault}/help/contact.html">contact the MIV project team</a>
for further assistance.

The message should display with a style the same as: <span class="ERR_MISSING">
*/
    private RecordFormatter errorFormatter = new FormatErrorFormatter("/help/contact.html");
    private class FormatErrorFormatter extends FormatAdapter
    {
        private static final String ERROR_MESSAGE_START_TAG = "<span class=\"" + ERROR_CSS_CLASS + "\">";
        private static final String ERROR_MESSAGE_END_TAG = "</span>";

        private static final String ERROR_MESSAGE_A = "ERROR: This record cannot be displayed due to missing/invalid data.";
        private static final String ERROR_MESSAGE_B = "Please edit this record or link()";
        private static final String ERROR_LINK_TEXT = "contact the MIV project team";
        private static final String ERROR_MESSAGE_C = "for further assistance.";

        private String error_url = null;
        private String MESSAGE_B = ERROR_MESSAGE_B;

        public FormatErrorFormatter(String url)
        {
            String path = "/" + url;
            this.error_url = MIVConfig.getConfig().getWebApplicationPath() + path.replaceAll("//", "/");
            MESSAGE_B = ERROR_MESSAGE_B
                    .replaceAll("link\\(\\)",
                            "<a class=\"attention\" href=\"" + this.error_url + "\">" +
                            ERROR_LINK_TEXT + "</a>");
        }

        @Override
        public Map<String, String> format(Map<String, String> m)
        {
            StringBuilder errMessage = new StringBuilder();

            errMessage.append(ERROR_MESSAGE_START_TAG);
            errMessage.append(ERROR_MESSAGE_A).append(" ");
            errMessage.append(this.MESSAGE_B).append(" ");
            errMessage.append(ERROR_MESSAGE_C);
            errMessage.append(ERROR_MESSAGE_END_TAG);

            m.put("preview", errMessage.toString());
            m.put("sequencepreview", errMessage.toString());
            return m;
        }
    }


    @Subscribe
    public void reloadFormatters(ConfigurationChangeEvent e)
    {
        log.debug("Reloading formatters because a ConfigurationChangeEvent was received");
        this.reload();
    }
}
