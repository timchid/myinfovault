package edu.ucdavis.myinfovault.format;

import java.util.Map;

import edu.ucdavis.myinfovault.data.MIVUtil;

public class GreekFixer extends FormatAdapter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        for (String key : m.keySet())
        {
            String value = m.get(key);
            value = MIVUtil.greekToRef(value);
            m.put(key, value);
        }
        return m;
    }
}
