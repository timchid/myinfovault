/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: HighlightPattern.java
 */

package edu.ucdavis.myinfovault.format;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;

/**<p>
 * Holds a pattern and the attributes used for highlighting it when the pattern is found.
 * Quickly hacked together to use for Author Name highlighting. More work may be needed
 * to use it for the general match-and-highlight strategy for MIV formatting.
 * </p>
 * <p>Constructors are provided for boolean flagging of the attributes, i.e. bold is true
 * or false to indicate whether this pattern should be bold when found; and constructors
 * for integer 1 or 0, and String "1" or "0", as a convenience since the fields can come
 * out of the database as integers (tinyint) or as Strings when using very generic fetch
 * methods.
 * </p>
 * <p>This class and methods refer to a "pattern" to match. While it's theoretically
 * possible to actually use regular expression ("RE", "regex") pattern matching in our
 * highlighting we aren't currently (2007-Jul-24) supporting it. The matched string is
 * generally (in old existing code) replaced by the pattern used to match it &mdash;
 * backreferences aren't used on the actual match.  The "patterns" given to this class
 * should, for now, be plain strings to look for.
 * Note that the pattern provided is "quoted" so no characters will have their regex
 * meaning, and this would have to change if we support RE matching in the future.
 * </p>
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class HighlightPattern
    implements Serializable, Cloneable
{
    private static final long serialVersionUID = 200707240955L;
    private static MIVConfig mivConfig = MIVConfig.getConfig();

    private static String patternPrefix = mivConfig.getProperty("miv-config-pattern-prefix");
    private static String patternSuffix = mivConfig.getProperty("miv-config-pattern-suffix");

    /* Default pattern match strings in case we don't find any in the config properties. */
    private static final String DEFAULT_MATCH_PREFIX = "(?<=\\A|[^\\p{javaUpperCase}\\p{javaLowerCase}0-9])(";
    private static final String DEFAULT_MATCH_SUFFIX = ")(?=[^\\p{javaUpperCase}\\p{javaLowerCase}0-9]|\\Z)";

    static {
        if (!StringUtils.hasText(patternPrefix)) patternPrefix = DEFAULT_MATCH_PREFIX;
        if (!StringUtils.hasText(patternSuffix)) patternSuffix = DEFAULT_MATCH_SUFFIX;
        // Is this something we really need to worry about changing on-the-fly like this?
        mivConfig.addPropertyChangeListener(MIVConfig.CONFIG_PROPERTY_NAME, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent e)
            {
                HighlightPattern.patternPrefix = mivConfig.getProperty("miv-config-pattern-prefix");
                HighlightPattern.patternSuffix = mivConfig.getProperty("miv-config-pattern-suffix");
                if (!StringUtils.hasText(patternPrefix)) patternPrefix = DEFAULT_MATCH_PREFIX;
                if (!StringUtils.hasText(patternSuffix)) patternSuffix = DEFAULT_MATCH_SUFFIX;
            }
        });
    }

    private String ID = null;
//    private String documentID = null;
    private DocumentIdentifier documentType = DocumentIdentifier.INVALID;
    private String field = null;
    private String pattern = null;
    private boolean bold = false;
    private boolean italic = false;
    private boolean underline = false;
    private String cssClass = "";


    /**<p>
     * Create a pattern with highlighting information.
     * </p>
     * @param ID Unique identifier for this pattern (often a record number)
     * @param documentID type of the document that this highlighting will apply to
     * @param field The name of a field to which this pattern should be applied.
     * @param pattern The fixed String or pattern to match.
     * @param bold Flag to indicate using <strong>bold</strong> to highlight this pattern when found (true/false).
     * @param italic Flag to indicate using <em>italic</em> to highlight this pattern when found (true/false).
     * @param underline Flag to indicate using <u>underline</u> to highlight this pattern when found (true/false).
     */
    public HighlightPattern(String ID, int documentID, String field, String pattern, boolean bold, boolean italic, boolean underline)
    {
        if (ID == null || "".equals(ID))
            throw new IllegalArgumentException("Pattern ID may not be null or empty");

        this.ID = ID;
        this.documentType = DocumentIdentifier.convert(documentID);
        this.field = field != null ? field : "author";
        this.pattern = pattern;
        this.bold = bold;
        this.italic = italic;
        this.underline = underline;
        setCssClass();
    }


    /**<p>
     * Create a pattern with highlighting information. Any non-zero value for the flags indicates
     * true&mdash;use the indicated attribute to highlight the pattern&mdash;while 0 means false.
     * Most commonly 1 and 0 will be used to indicate true and false respectively.
     * </p>
     * @param ID Unique identifier for this pattern (often a record number)
     * @param documentID type of the document
     * @param field The name of a field to which this pattern should be applied.
     * @param pattern The fixed String or pattern to match.
     * @param bold An integer flag to indicate using <strong>bold</strong> to highlight this pattern when found.
     * @param italic An integer flag to indicate using <em>italic</em> to highlight this pattern when found.
     * @param underline An integer flag to indicate using <u>underline</u> to highlight this pattern when found.
     */
    public HighlightPattern(String ID, int documentID, String field, String pattern, int bold, int italic, int underline)
    {
        this(ID, documentID, field, pattern,!(bold==0),!(italic==0),!(underline==0));
    }


    /**<p>
     * Create a pattern with highlighting information. Any non-"0" value for the flags indicates
     * true&mdash;use the indicated attribute to highlight the pattern&mdash;while "0" means false.
     * Most commonly "1" and "0" will be used to indicate true and false respectively.
     * </p>
     * @param ID Unique identifier for this pattern (often a record number)
     * @param documentID type of the document
     * @param field The name of a field to which this pattern should be applied.
     * @param pattern The fixed String or pattern to match.
     * @param bold A numeric String flag to indicate using <strong>bold</strong> to highlight this pattern when found.
     * @param italic A numeric String flag to indicate using <em>italic</em> to highlight this pattern when found.
     * @param underline A numeric String flag to indicate using <u>underline</u> to highlight this pattern when found.
     */
    public HighlightPattern(String ID, int documentID, String field, String pattern, String bold, String italic, String underline)
    {
        this(ID, documentID, field, pattern,
             ! "0".equals(bold),
             ! "0".equals(italic),
             ! "0".equals(underline)
        );
    }

    /**<p>
     * Get the DocumentIdentifier of the field.
     * </p>
     * @return the DocumentIdentifier of the field.
     */
    public DocumentIdentifier getDocumentType()
    {
        return this.documentType;
    }


    /**<p>
     * Get the name of the field this HighlightPattern applies to.
     * </p>
     * @return the field this HighlightPattern applies to.
     */
    public String getFieldName()
    {
        return this.field;
    }


    /**<p>
     * Get the pattern String for this HighlightPattern
     * </p>
     * @return the pattern String for this HighlightPattern
     */
    public String getPattern()
    {
        return this.pattern;
    }


    /**<p>
     * Indicates whether or not this HightlightPattern should use
     * <strong>bold</strong> to highlight the pattern when found.
     * </p>
     * @return true if bold should be used, otherwise false
     */
    public boolean useBold()
    {
        return this.bold;
    }


    /**<p>
     * Indicates whether or not this HightlightPattern should use
     * <em>italics</em> to highlight the pattern when found.
     * </p>
     * @return true if italics should be used, otherwise false
     */
    public boolean useItalic()
    {
        return this.italic;
    }


    /**<p>
     * Indicates whether or not this HightlightPattern should use
     * <u>underlining</u> to highlight the pattern when found.
     * </p>
     * @return true if underline should be used, otherwise false
     */
    public boolean useUnderline()
    {
        return this.underline;
    }


    /**
     * To generate the style css class name based on Bold, Italic and Under rules.
     */
    private void setCssClass()
    {
        StringBuilder buf = new StringBuilder();
        buf.append("style");
        buf.append(useBold() ? "B" : "");
        buf.append(useItalic() ? "I" : "");
        buf.append(useUnderline() ? "U" : "");
        this.cssClass = buf.toString();
    }


    /**
     * To the style css class name
     * @return
     */
    public String getCssClass()
    {
        return this.cssClass;
    }


    /**
     * Tag any matches to this pattern found in the input string.
     * @param in
     * @return
     */
    public String tag_0(String in)
    {
//        String out="";
//
//        if (this.getPattern() != null && this.getPattern().length() > 0)
//        {
//            String match = Pattern.quote(this.getPattern());
//
//            String pattern = patternPrefix + match + patternSuffix;
//            String replacement = "<span class=\"" + this.getCssClass() + "\">$1</span>";
//
//            out = in.replaceAll(pattern, replacement);
//
//
//              // Matching of substrings within a pattern are not supported
//              // check that there are no nested spans
//             /* String spanPattern= "\\<span .*?\\>.*?\\<span .*?\\>\\</span\\>"; if
//              (Pattern.compile(spanPattern).matcher(out).matches()) { out = in; }
//             */
//        }
//        else
//        {
//            out = "<span class=\"" + this.getCssClass() + "\">" + in + "</span>";
//        }
//
//        return out;
  // Commented out the (above) re-written version and restoring the original version (below)
  // FIXME: Note this causes a problem, in that both Publications and Creative Activities makes
  //        a CSS rule that just says "span.title { ... }" with no further qualifications.
  //        Whichever appears last takes effect, so, if setting Creative Activities titles to Bold
  //        and Publication titles to Italics, both types of titles will instead appear the same,
  //        either both bold or both italic, depending on which record happens to appear last.
        String out;
        String match = Pattern.quote(this.getPattern());

        String pattern = patternPrefix + match + patternSuffix;
        String replacement = "<span class=\""+this.getCssClass()+"\">$1</span>";

        out = in.replaceAll(pattern, replacement);

        // Matching of substrings within a pattern are not supported
        // check that there are no nested spans
        String spanPattern= "\\<span .*?\\>.*?\\<span .*?\\>\\</span\\>";
        if (Pattern.compile(spanPattern).matcher(out).matches())
        {
            out = in;
        }

        return out;
    }

    /**
     * Tag any matches to this pattern found in the input string.
     * @param in
     * @return
     */
    public String tag(String in)
    {
        String out="";

        if (this.getPattern() != null && this.getPattern().length() > 0)
        {
            String match = Pattern.quote(this.getPattern());

            String pattern = patternPrefix + match + patternSuffix;
            String replacement = "<span class=\"" + this.getCssClass() + "\">$1</span>";

            out = in.replaceAll(pattern, replacement);
        }
        else
        {
            out = "<span class='" + this.getCssClass() + "'>" + in + "</span>";
        }

        return out;
    }


    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb
          .append(this.getPattern()).append(" ")
          .append("field=").append(this.getFieldName()).append(" ")
          .append("bold=").append(this.bold).append(" ")
          .append("italic=").append(this.italic).append(" ")
          .append("underline=").append(this.underline).append(" ")
          ;
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ID == null) ? 0 : ID.hashCode());
        result = prime * result + (bold ? 1231 : 1237);
        result = prime * result + ((field == null) ? 0 : field.hashCode());
        result = prime * result + (italic ? 1231 : 1237);
        result = prime * result + ((pattern == null) ? 0 : pattern.hashCode());
        result = prime * result + (underline ? 1231 : 1237);
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final HighlightPattern other = (HighlightPattern) obj;
        if (ID == null)
        {
            if (other.ID != null) return false;
        }
        else if (!ID.equals(other.ID)) return false;
        if (bold != other.bold) return false;
        if (field == null)
        {
            if (other.field != null) return false;
        }
        else if (!field.equals(other.field)) return false;
        if (italic != other.italic) return false;
        if (pattern == null)
        {
            if (other.pattern != null) return false;
        }
        else if (!pattern.equals(other.pattern)) return false;
        if (underline != other.underline) return false;
        return true;
    }
}
