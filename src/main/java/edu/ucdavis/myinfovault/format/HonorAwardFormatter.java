package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class HonorAwardFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        m.put("year", insertValue("year", m.get("year")));
        m.put("remark", insertValue("remark", m.get("remark"),COMMA_SPACE, ""));

        StringBuilder preview = new StringBuilder();
        preview .append(insertValue("description", m.get("description")));

        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());

        return m;
    }
}
