package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class LicensesCertificationFormatter extends PreviewBuilder implements RecordFormatter
{
    @Override
    protected String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);
        StringBuilder preview = new StringBuilder();
        m.put("remark", insertValue("remark", m.get("remark"),COMMA_SPACE, ""));

        m.put("year", insertValue("yearspan", m.get("yearspan")));

        preview .append( insertValue("content", m.get("content")));

        return preview.toString();
    }
}
