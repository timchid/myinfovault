/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PatentFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

public class PatentFormatter extends PreviewBuilder implements RecordFormatter
{
    /**
     * A formatter for patents, such as patent title, patent authors etc.
     * @param names An Iterable list of names to highlight in the title. An empty list is allowed, <code>null</code> is not.
     */
    public PatentFormatter(Iterable<String> names)
    {
        this.add( new TitleFormatter(PERIOD, "") )
            .add( new AuthorFormatter(PERIOD, "author") )
            .add( new GrantDateFormatter("year"))
            .add( new URLFormatter(URLFormatter.PreviewType.PATENT).add(new QuoteCleaner("link")))
            ;
    }

    public PatentFormatter()
    {
        this(null);
    }

    @Override
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();

//        String patentTypeID = m.get("typeid");

        preview
            .append(insertValue("author", m.get("author")))
            .append(insertValue("title", m.get("title"), SINGLE_SPACE, "")).append(LINE_BREAK)
            .append(insertValue("patentdetail", m.get("patentdetail"), "", SINGLE_SPACE))
            .append(insertValue("patentinfo", m.get("patentinfo"), "", ""));


//        PatentType patentType = PatentType.getPatentTypeById(Integer.parseInt(patentTypeID));
//        if (patentType == PatentType.PATENT && m.get("licensing") != null
//                && m.get("licensing").toString().trim().length() > 0)
//        {
//            preview.append(insertValue("jurisdiction", m.get("licensing"), "<br><br><span class=\"licensing\">","</span>"));
//        }

        return preview.toString();
    }
}
