package edu.ucdavis.myinfovault.format;

import java.io.File;
import java.util.Map;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.document.PathConstructor;

/**
 * @author Binhtri Huynh
 * @since MIV 2.0
 */
public class PositionDescFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        String s;
        String readOnly;
        String uploadName = "PDF Document";
        
        if (m.containsKey("pdcontent"))
        {
            // Move the percenttime into a "year" pseudo-field for consistency w/other rec types
            m.put("year", insertValue("percenttime", m.get("percenttime"), "", " %"));
            preview.append( insertValue("content", m.get("pdcontent")) );
        }
        else
        {
        	readOnly = "true";
            if ( ! isEmpty(s=m.get("uploadname")) )
            {
                uploadName = s;
            }

            if ( ! isEmpty(s=m.get("uploadfile")) )
            {
                // Build a URL from the file name
                int userId = Integer.parseInt(m.get("userid"));
                // Full upload file path
                File uploadFile = new File(MIVConfig.getConfig().getDossierPdfLocation(userId),s);
                // Convert the file path to a URL
                String uploadFileUrl  = new PathConstructor().getUrlFromPath(uploadFile.getAbsoluteFile());
                String link = "<a href=\"" +  uploadFileUrl + "\" title=\""+uploadName+"\">" + uploadName + "</a>";
                preview .append( isEmpty(s=m.get("uploadfile")) ? "" : link );
            }
            else
            {
                s = "";
            }
            m.put("readonly", readOnly);
        }

        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());
        return m;
    }
}
