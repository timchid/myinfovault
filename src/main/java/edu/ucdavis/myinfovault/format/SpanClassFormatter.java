/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SpanClassFormatter.java
 */
package edu.ucdavis.myinfovault.format;

import java.util.Map;
import java.util.logging.Logger;

/**
 * <p>
 * Format the field strings by enclosing within a.
 * HTML <code>&lt;span class="styleClasses"&gt;&lt;/span&gt;</code> tag.</p>
 * @author pradeeph
 * @since MIV 4.4.2
 */
public class SpanClassFormatter extends FormatAdapter implements RecordFormatter
{
    private static Logger log = Logger.getLogger("MIV");
    private String[] styleClasses = null;
    private String fieldName = null;

    /**
     * Create an SpanClassFormatter to surrounding the complete field with span tag and css styles,
     * HTML <code>&lt;span class="styleClasses"&gt;&lt;/span&gt;</code> tag.
     * 
     * @param fieldName
     * @param styleClasses
     */
    public SpanClassFormatter(String fieldName, String... styleClasses)
    {
        this.fieldName = fieldName;
        this.styleClasses = styleClasses;
    }

    /**
     * Format the field strings by enclosing within a
     * HTML <code>&lt;span class="styleClasses"&gt;&lt;/span&gt;</code> tag.
     * @return The provided Map.
     */
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        log.entering(this.getClass().getSimpleName(), "format");

        if (!isEmpty(fieldName))
        {
                String fieldValue = m.get(this.fieldName);
                String styleClassesString = "";

                if(this.styleClasses!=null && this.styleClasses.length>0)
                {
                    for(String style: this.styleClasses)
                    {
                        if(isEmpty(styleClassesString))
                        {
                            styleClassesString = this.fieldName;
                        }
                        styleClassesString += " "+style;
                    }
                }
                m.put(this.fieldName, "<span class=\""+styleClassesString+"\">" + fieldValue + "</span>");
        }
        log.exiting(this.getClass().getSimpleName(), "format");
        return m;
    }
}
