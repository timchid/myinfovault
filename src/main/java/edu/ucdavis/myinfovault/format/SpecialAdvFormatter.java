package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Binhtri Huynh
 * @since MIV 2.0
 */
public class SpecialAdvFormatter extends PreviewBuilder implements RecordFormatter
{
    @Override
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();

        preview
 //          .append( insertValue("year", m.get("year"), "Academic Year: ", " "))
           .append( insertValue("content", m.get("spcontent")))
           ;
        return preview.toString();
    }
}
