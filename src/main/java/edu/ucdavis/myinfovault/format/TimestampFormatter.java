/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: TimestampFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

public class TimestampFormatter extends FormatAdapter implements RecordFormatter
{
    private static final ThreadLocal<DateFormat> formatter =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return new SimpleDateFormat("MM-dd-yyyy, hh:mm aaa");
        }
    };
//    private static final DateFormat formatter = new SimpleDateFormat("MM-dd-yyyy, hh:mm aaa");

    private String customField = null;

    // A field *must* be specified so there shouldn't be a no-arg constructor.
//    public TimestampFormatter()
//    {
//    }

    public TimestampFormatter(String field)
    {
        if (isEmptyOrBlank(field)) {
            throw new IllegalArgumentException("A field to format must be specified");
        }
        customField = field;
    }


    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        String current = m.get(customField);

        if (hasText(current))
        {
            java.sql.Timestamp ts = java.sql.Timestamp.valueOf(current);
//            synchronized(formatter) {
//                String dateString = formatter.format(ts);
//                m.put(customField, dateString);
//            }
            String dateString = formatter.get().format(ts);
            m.put(customField, dateString);
        }

        return m;
    }
}
