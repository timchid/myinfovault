/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: TitleFormatter.java
 */


package edu.ucdavis.myinfovault.format;

import java.util.Map;
import java.util.logging.Logger;

import edu.ucdavis.mw.myinfovault.util.HtmlUtil;

/**
 * Format the title of a record for the publication citation previews.
 * This formatter modifies the value of the existing <em>title</em> field.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class TitleFormatter extends FormatAdapter implements RecordFormatter
{
    private static Logger log = Logger.getLogger("MIV");
    private static final String FIELDKEY = "title";
    private String punctuationChar = PERIOD;
    private String punctuationGroup = PUB_GROUP_JOURNALS;

    public TitleFormatter(String punctuationChar, String punctuationGroup)
    {
        this.punctuationChar = punctuationChar;
        this.punctuationGroup = punctuationGroup;
    }

    @Override
    public RecordFormatter add(RecordFormatter formatter)
    {
        return this;
    }

    @Override
    public Map<String,String> format(Map<String,String> m)
    {
        log.entering(this.getClass().getSimpleName(), "format");
        String orig, newval="";


        /* Add a period (.) to the end of the title unless it already ends with punctuation. */
        orig = m.get(FIELDKEY);
        if (orig != null && orig.length() > 0)
        {
            // if (MIVConfig.getConfig().isDebug()) spin(orig);

            newval = orig;
            String terminator = punctuationChar;//SDP 2007-05-11
            //if (!orig.matches(".*[.,!?]\\z"))     // look for certain punctuation at the end
            if (PUB_GROUP_JOURNALS.equals(punctuationGroup) && hasPunctuation(orig))
            {
                terminator = "";
            }

            // Mark this with a class
            newval = "<span class=\"title\">"+newval+"</span>"+terminator;//SDP 2007-05-11
            log.finest(" title: orig=["+orig+"] new=["+newval+"]");

            m.put(FIELDKEY, newval);
        }

        log.exiting(this.getClass().getName(), "format");
        return m;
    }


    /**
     * Determine whether the provided string ends with punctuation.
     * Characters considered to be punctuation
     * @param field  string to check for punctuation
     * @return true if the provided string ends with a punctuation character.
     */
    private boolean hasPunctuation(String field)
    {
        field = HtmlUtil.removeHTMLTags(field.trim());

        // SDP 2011-11-08 MIV-4059 : Added endsWith(PERIOD) back in to the punctuation
        return (field.endsWith(".") || field.endsWith("!") || field.endsWith("?") || field.endsWith("\"") ||
                field.matches(".*\\.\\.\\.$") || field.matches(".*[A-Z]\\.$"));
    }

    // A Temporary method to test some stuff about unicode blocks.
    // remove this and the call to it when testing is done.
    /*@SuppressWarnings("unused")
    private void spin(String s)
    {
        char ch;        // char
        int cp;         // codepoint
        int len = s.length();
        StringBuilder info = new StringBuilder();
        StringBuilder orig = new StringBuilder(s);
        System.out.println("----------------------->8--------------------------------------");
        System.out.println("TitleFormatter -- String:["+s+"] Length:["+len+"/"+orig.length()+"] CodepointCount:["+orig.codePointCount(0, len)+"]");
        System.out.println("[ char/codept ch==/ch.eq/cp==/cp.eq ]");
        for (int i=0; i<len; i++)
        {
            ch = s.charAt(i);
            cp = s.codePointAt(i);
            Character.UnicodeBlock charBlock = Character.UnicodeBlock.of(ch);
            Character.UnicodeBlock codeBlock = Character.UnicodeBlock.of(cp);
            boolean fCharInBlock1 = (charBlock == Character.UnicodeBlock.GREEK);
            boolean fCharInBlock2 = (charBlock.equals(Character.UnicodeBlock.GREEK));
            boolean fCodepointInBlock1 = (codeBlock == Character.UnicodeBlock.GREEK);
            boolean fCodepointInBlock2 = (codeBlock.equals(Character.UnicodeBlock.GREEK));

            info
                .append("[")
                .append(ch) .append("/") .append(cp)
                .append(" ")
                .append(fCharInBlock1) .append("/") .append(fCharInBlock2)
                .append("/")
                .append(fCodepointInBlock1) .append("/") .append(fCodepointInBlock2)
                .append("]")
                ;
            System.out.println(info.toString());
            info.setLength(0);
        }
        System.out.println("----------------------->8--------------------------------------");
        System.out.println();
        info.setLength(0);
    }*/
}
