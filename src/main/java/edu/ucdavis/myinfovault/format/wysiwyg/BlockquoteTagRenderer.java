package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class BlockquoteTagRenderer extends SimpleTagRenderer
{
    public BlockquoteTagRenderer(Node n)
    {
        super(n);
    }
}
