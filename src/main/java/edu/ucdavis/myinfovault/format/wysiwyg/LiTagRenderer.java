package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.TagAttributeRenderer;

public class LiTagRenderer extends TagAttributeRenderer
{
    public LiTagRenderer(Node n)
    {
        super(n,"class","style");
    }
}
