package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class OlTagRenderer extends SimpleTagRenderer
{
    public OlTagRenderer(Node n)
    {
        super(n);
    }
}
