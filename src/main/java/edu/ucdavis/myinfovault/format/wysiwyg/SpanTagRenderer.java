package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.TagAttributeRenderer;

public class SpanTagRenderer extends TagAttributeRenderer
{
    public SpanTagRenderer(Node n)
    {
        super(n, "class", "title", "style");
    }


    public String toString()
    {
        String startTag = makeStartTag();
        String endTag = makeEndTag();
        String content = processChildren();

        if (this.node.hasAttributes())
        {
            if (startTag.startsWith("<span style=\"text-decoration: underline;\">"))
            {
                startTag = "<u>";
                endTag = "</u>";
            }
            return startTag + content + endTag;
        }

        // No attributes: don't write the tag at all, just the contents.
        return content;
    }
}
