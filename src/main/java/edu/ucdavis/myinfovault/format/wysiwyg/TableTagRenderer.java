package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.TagAttributeRenderer;

public class TableTagRenderer extends TagAttributeRenderer
{
    public TableTagRenderer(Node n)
    {
        super(n, "width", "border");
    }
}
