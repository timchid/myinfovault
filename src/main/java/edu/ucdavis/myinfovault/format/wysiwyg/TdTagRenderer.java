package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.TagAttributeRenderer;

public class TdTagRenderer extends TagAttributeRenderer
{
    public TdTagRenderer(Node n)
    {
        super(n, "width", "valign", "align","style");
    }

    @Override
    public String toString()
    {
        String startTag = makeStartTag();
        String endTag = makeEndTag();
        String content = processChildren();
        // The table data element must not be empty
        content = content != null && content.length() != 0 ? content : " ";
        return startTag + content + endTag;
    }
}
