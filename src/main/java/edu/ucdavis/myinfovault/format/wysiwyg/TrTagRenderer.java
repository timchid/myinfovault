package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class TrTagRenderer extends SimpleTagRenderer
{
    public TrTagRenderer(Node n)
    {
        super(n);
    }
}
