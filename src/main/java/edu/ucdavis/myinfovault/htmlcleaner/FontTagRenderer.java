package edu.ucdavis.myinfovault.htmlcleaner;

import org.w3c.dom.Node;

class FontTagRenderer extends NodeRenderer
{
    // for <font face="symbol"> tags
    public FontTagRenderer(Node n)
    {
	super(n);
    }
}
