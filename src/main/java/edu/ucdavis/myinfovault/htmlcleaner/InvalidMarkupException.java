package edu.ucdavis.myinfovault.htmlcleaner;

public class InvalidMarkupException extends Exception
{
    private static final long serialVersionUID = 1L;

    private int columnNumber = -1;

    public InvalidMarkupException()
    {
        super();
    }

    public InvalidMarkupException(String message)
    {
        super(message);
    }

    public InvalidMarkupException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InvalidMarkupException(Throwable cause)
    {
        super(cause);
    }

    public InvalidMarkupException(int columnNumber)
    {
        super();
        this.columnNumber = columnNumber;
    }

    public InvalidMarkupException(String message, int columnNumber)
    {
        super(message);
        this.columnNumber = columnNumber;
    }

    public InvalidMarkupException(String message, int columnNumber, Throwable cause)
    {
        super(message, cause);
        this.columnNumber = columnNumber;
    }

    public InvalidMarkupException(int columnNumber, Throwable cause)
    {
        super(cause);
        this.columnNumber = columnNumber;
    }

    public int getColumnNumber()
    {
        return columnNumber;
    }
}
