package edu.ucdavis.myinfovault.htmlcleaner;

//import java.lang.reflect.Constructor;
//import java.util.WeakHashMap;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**<p>
 * Render the contents of a DOM Node.
 * </p>
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class NodeRenderer
{
    /** The node to be rendered by this renderer */
    protected Node node = null;
    /** The factory used to create this node, and used to create other nodes. */
    protected NodeRendererFactory factory = NodeRendererFactory.getDefaultFactory();


    /**
     * Create a generic NodeRenderer for the given node.
     * @param n a {@link org.w3c.dom.Node Node} to be rendered by this renderer.
     * @throws IllegalArgumentException is a <code>null</code> Node is passed.
     */
    public NodeRenderer(Node n)
    {
        assert n != null : "nodes cannot be null";
        if (n == null) throw new IllegalArgumentException("nodes cannot be null");
        this.node = n;
    }


    public NodeRenderer(Node n, NodeRendererFactory factory)
    {
        this(n);
        if (factory != null) this.factory = factory;
    }


    void setFactory(NodeRendererFactory factory)
    {
        this.factory = ( factory != null ? factory : NodeRendererFactory.getDefaultFactory() );
    }


    /**
     * Render the contents (value) of the node.
     * The generic NodeRenderer ignores its own value and just renders its children.
     * It reduces multiple spaces to a single space and trims trailing spaces.
     */
    public String toString()
    {
        return processChildren()/*.replaceAll("[ ][ ]*", " ")*/.replaceFirst("[ ]*$", "");
    }


    /**
     * Render the contents of each child node of this node.
     * Most, if not all, subclasses can use this implementation to render their child nodes.
     * @return The rendered String from all DOM subtrees.
     */
    protected String processChildren()
    {
        StringBuilder sb = new StringBuilder();
        NodeList l = node.getChildNodes();
        int len = l.getLength();
        for (int i=0; i<len; i++)
        {
            NodeRenderer nr = this.factory.createNodeRenderer(l.item(i));
            sb.append(nr.toString());
        }

        return sb.toString();
    }
}
