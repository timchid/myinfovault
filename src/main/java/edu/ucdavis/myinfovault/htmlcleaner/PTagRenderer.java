package edu.ucdavis.myinfovault.htmlcleaner;

import org.w3c.dom.Node;

/**
 * Replace the paragraph tag by the contents of the paragraph followed by a newline.
 * @author Stephen Paulsen
 */
public class PTagRenderer extends NodeRenderer
{
    public PTagRenderer(Node n) { super(n); }

    public String toString()
    {
        return "\n\n" + processChildren() + "\n\n";
    }
}
