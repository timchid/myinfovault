package edu.ucdavis.myinfovault.htmlcleaner;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class TagSourceRenderer extends NodeRenderer
{
    private String startTag = null;
    private String endTag = null;

    public TagSourceRenderer(Node n)
    {
        super(n);
        //System.out.println(n.toString());

        String nodeName = n.getNodeName();
        StringBuilder s = new StringBuilder();

        // build the start tag
        s.append("&lt;").append(nodeName);
        if (n.hasAttributes())
        {
            NamedNodeMap attrMap = n.getAttributes();
            int attrCount = attrMap.getLength();
            for (int i=0; i<attrCount; i++)
            {
                s.append(" ").append(attrMap.item(i).toString());
            }
        }

        if (!n.hasChildNodes())
        {
            endTag = "";
            s.append("/");
        }
        else
        {
            endTag = "&lt;/" + nodeName + "&gt;";
        }
        s.append("&gt;");
        startTag = s.toString();
    }

    @Override
    public String toString()
    {
        return startTag + processChildren() + endTag;
    }
}
