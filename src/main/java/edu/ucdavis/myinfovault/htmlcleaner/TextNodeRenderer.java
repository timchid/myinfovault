package edu.ucdavis.myinfovault.htmlcleaner;

import org.w3c.dom.Node;

public class TextNodeRenderer extends NodeRenderer
{
    public TextNodeRenderer(Node n)
    {
	super(n);
    }
    public String toString()
    {
        return node.getNodeValue();
    }
}
