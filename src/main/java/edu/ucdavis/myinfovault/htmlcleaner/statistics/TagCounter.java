package edu.ucdavis.myinfovault.htmlcleaner.statistics;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;
import edu.ucdavis.myinfovault.htmlcleaner.NodeRendererFactory;
import edu.ucdavis.myinfovault.htmlcleaner.TagInfoRecord;
import edu.ucdavis.myinfovault.htmlcleaner.statistics.StatisticsTester.TagStatisticsFactory;

public class TagCounter extends NodeRenderer
{
    // for testing only...
    private static int tagCount = 0;

    private String tableName;
    private String recordID;
    private String userID;
    private String fieldName;

    public TagCounter(Node n)
    {
        super(n);
    }

    public TagCounter(Node n, TagStatisticsFactory factory)
    {
        super(n, factory);
    }

    /**
     * This is the one we'll be using.
     * @param n
     * @param factory
     * @param tableName
     * @param recordID
     * @param userID
     * @param fieldName
     */
    public TagCounter(Node n, NodeRendererFactory factory, String tableName, String recordID, String userID, String fieldName)
    {
        super(n, factory);
        this.tableName = tableName;
        this.recordID = recordID;
        this.userID = userID;
        this.fieldName = fieldName;
    }

    @Override
    protected String processChildren()
    {
        StringBuilder sb = new StringBuilder();
        NodeList l = node.getChildNodes();
        int len = l.getLength();
        for (int i=0; i<len; i++)
        {
            NodeRenderer nr = ((TagStatisticsFactory)this.factory).createNodeRenderer(tableName, recordID, userID, fieldName, l.item(i));
            sb.append(nr.toString());
        }

        return sb.toString();
    }

    @Override
    public String toString()
    {
        String tagName = this.node.getNodeName();
        //System.err.println("TagCounter.toString() on tag ["+tagName+"]");

        // Don't bother with these tags
        if ("br".equalsIgnoreCase(tagName)) return "";  // ignore the <br>, and it has no children

        if ("htmlTestWrapper".equalsIgnoreCase(tagName) ||
            "font".equalsIgnoreCase(tagName)
        )
        {
            return processChildren();
        }

        // Anything else, we'll create and display the tag information
        TagInfoRecord tagInfo = new TagInfoRecord(tableName, recordID, userID, fieldName, tagName, node.getTextContent());
        System.out.println(tagInfo);

        return "(" + (++tagCount) + ":" + tagName + ") " + processChildren();// + super.toString();
    }
}
