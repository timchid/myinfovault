package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;


/** Not really a subclass; an exact clone of the ATagRenderer */
// we just need another one in the wysiwyg package so the package-based introspection will find it.
public class ATagRenderer extends edu.ucdavis.myinfovault.htmlcleaner.ATagRenderer
{
    public ATagRenderer(Node n)
    {
        super(n);
    }
}
