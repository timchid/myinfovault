package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;

public class BrTagRenderer extends NodeRenderer
{
    public BrTagRenderer(Node n)
    {
        super(n);
    }

    public String toString()
    {
        return "<br/>";
    }
}
