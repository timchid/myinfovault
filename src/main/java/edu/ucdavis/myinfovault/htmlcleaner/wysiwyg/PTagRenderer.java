package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

public class PTagRenderer extends TagAttributeRenderer
{
    public PTagRenderer(Node n)
    {
        super(n, "align","style");
    }

    @Override
    public String toString()
    {
        //return "<br/>"+super.toString()+"<br/>";
        return super.toString().replaceAll("\\A<p>", "<p><br/>") + "<br/>";
    }
}
