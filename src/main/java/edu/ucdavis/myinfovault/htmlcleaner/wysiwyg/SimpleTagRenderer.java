package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;

public abstract class SimpleTagRenderer extends NodeRenderer
{
    public SimpleTagRenderer(Node n)
    {
        super(n);
    }

    public String toString()
    {
        String nodeName = this.node.getNodeName().toLowerCase();
//        String startTag = "<" + nodeName + ">";
//        String endTag = "</" + nodeName + ">";
//        return startTag + processChildren() + endTag;
        return "<" + nodeName + ">" + processChildren() + "</" + nodeName + ">";
    }
}
