package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

public class StrongTagRenderer extends SimpleTagRenderer
{
    public StrongTagRenderer(Node n)
    {
        super(n);
    }
}
