/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivRequestAdapter.java
 */

package edu.ucdavis.myinfovault.http;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class MivRequestAdapter extends HttpServletRequestWrapper
{
    private Map<String,String[]> newParams;
    public MivRequestAdapter(HttpServletRequest req)
    {
        super(req);
    }

    @SuppressWarnings("unchecked")
    public MivRequestAdapter(HttpServletRequest req, Map params)
    {
        super(req);
        this.newParams = new LinkedHashMap<String,String[]>();
        newParams.putAll(params);
    }

    @Override
    public Map<String,String[]> getParameterMap() {
//        System.out.println(" +@ returning ["+newParams+"]");
        return newParams;
    }

    @Override
    public Enumeration<String> getParameterNames()
    {
        Vector<String> v = new Vector<String>(newParams.keySet());
//        System.out.println(" +@  returning ["+v.elements()+"]");
        return v.elements();
    }

    @Override
    public String[] getParameterValues(String name)
    {
//        System.out.println(" +@ name requested: "+name);
//        System.out.println(" +@@  returning ["+java.util.Arrays.toString(newParams.get(name))+"]");
        return newParams.get(name);
    }

    @Override
    public String getParameter(String name)
    {
//        System.out.println(" +@ name requested: "+name);
//        System.out.println(" +@@  returning ["+newParams.get(name)[0]+"]");
        String[] values = this.getParameterValues(name);
        if (values != null && values.length > 0) {
            return values[0];
        }
        else {
            return null;
        }
    }

    public void addParam(String name, String... value)
    {
        newParams.put(name, value);
    }

    public void removeParam(String name)
    {
        newParams.remove(name);
    }

    @Override
    public BufferedReader getReader()
    {
        StringBuilder sbRequestData = new StringBuilder();
        Iterator<String> reqParams = newParams.keySet().iterator();
        while (reqParams.hasNext())
        {
            String reqParam = reqParams.next();
            String[] reqParamValues = newParams.get(reqParam);
            for (String reqParamValue : reqParamValues)
            {
                sbRequestData.append(reqParam).append("=");
                sbRequestData.append(reqParamValue);
                sbRequestData.append("&");
            }
        }
        BufferedReader br = new BufferedReader(new StringReader(sbRequestData.toString()));
        return br;
    }
}
