/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MessageBundle.java
 */

package edu.ucdavis.myinfovault.message;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Provides access to messages from properties file.
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public abstract class MessageBundle
{
    /**
     * Concrete classes must provide a {@code getBundle()} method
     * to provide <em>their</em> ResourceBundle to the {@code getString} methods.
     * @return A {@link java.util.ResourceBundle ResourceBundle} to use as the source for messages.
     */
    public abstract ResourceBundle getBundle();

    /**
     * Gets an externally defined message for a given message ID.
     * Returns the message ID string itself if no message was found.
     * This allows a message <em>or</em> a message ID to be passed
     * and the return value used whether or not it's an externally
     * defined message.
     *
     * @param messageId the <em>Identifier</em> of an externalized message
     * @return The message for the given message ID
     */
    public final String getString(String messageId)
    {
        try {
            return getBundle().getString(messageId);
        }
        catch (MissingResourceException e) {
            return messageId;
        }
    }


    /**
     * Gets an externally defined message for a given message ID, with replaceable parameters filled in.
     * Returns the message ID string itself if no message was found.
     * This allows a message <em>or</em> a message ID to be passed
     * and the return value used whether or not it's an externally
     * defined message.
     *
     * @param messageId the <em>Identifier</em> of an externalized message
     * @param messageParameters as many parameters as necessary to fill "<code>{0}</code>" style placeholders in the message.
     * @return The message for the given message ID
     */
    public final String getString(String messageId, Object... messageParameters)
    {
        try {
            String template = getBundle().getString(messageId);
            String output = MessageFormat.format(template, messageParameters);

            return output;
        }
        catch (MissingResourceException e) {
            return messageId;
        }
    }
}
