/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivLabels.java
 */

package edu.ucdavis.myinfovault.message;

import java.util.ResourceBundle;

/**
 * Provides access to Miv Labels from properties file.
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class MivLabels extends MessageBundle
{
    private static final String BUNDLE_NAME = "mivlabels";
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
    private static MessageBundle instance = new MivLabels();


    /** This is a static class that should never be instantiated. */
    private MivLabels() { } // don't allow instantiation


    /**
     * Get the single instance of the MivConstants object.
     * @return MivLabels
     */
    public static MessageBundle getInstance()
    {
        return instance;
    }


    @Override
    public ResourceBundle getBundle()
    {
        return RESOURCE_BUNDLE;
    }

}
