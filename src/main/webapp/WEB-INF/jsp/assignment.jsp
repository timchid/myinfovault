<%--
    NOTE: Webflow form backing object must extend edu.ucdavis.mw.myinfovault.web.spring.assignreviewers.AssignmentForm
          in order to  make use of this JSP

    DESCRIPTION:
        * Defines two tables of MIV persons/groups with tag IDs 'assigned' and 'available'
        * A MivPerson and Group IDs binds to personId and groupId, respectively
        * Triggers events 'search,' 'add,' 'remove,' 'activate,' 'deactivate,' 'editMembers,' and 'viewMembers'
        * 'assigned,' 'inactive,' and 'available' MivPerson/Group collections must be defined in the request scope

    INPUT:
        * "param.isReview"       : Is this page being called by the assign reviewers JSP?
        * "param.canSwitch"      : Can the the acting person switch to edit mode?
        * "param.editMembership" : Editing the assigned membership is allowed
        * "param.editMembers"    : Editing the status of the individual group members is allowed
        * "param.editGroups"     : Editing the statuses of the member group constituents is allowed

--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
    TODO: This should not be done this way.
--%><c:set var="mivPersonClass" value="edu.ucdavis.mw.myinfovault.service.person.builder.BuiltPersonImpl"/>
<script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/assignment.js'/>"></script>
<script type="text/javascript">
    /* <![CDATA][ */
        var mivConfig = new Object();
        mivConfig.isIE7 = false;
    /* ]]> */

    $(document).ready( function() {
        fnInitAssignment($("#assigned"),
                         $("#available"),
                         ${param.isReview},
                         ${param.editMembership},
                         ${param.editMembers},
                         ${param.editGroups});
    });
</script>

<%-- default event --%>
<input type="hidden" name="_eventId_update"/>

<fieldset class="${param.editMembership and not param.isReview ? 'assignmentStep' : 'mivfieldset'} columns">
    <c:if test="${param.editMembership and not param.isReview}">
        <legend>${strings.assignmentLegend}</legend>
    </c:if>

    <%-- if editing the membership is allowed --%>
    <c:if test="${param.editMembership}">
        <c:if test="${not param.isReview}">
            <jsp:include page="/help/group_assignment.html"/>
            <%--div class="helpicon">?</div--%>
            <div class="helpicon"><img src="<c:url value='/images/help.png'/>" alt="Help for Step 3"></div>
        </c:if>

        <div>
            <button type="button"
                    class="button"
                    id="searchtoggle"
                    title="Select for advanced search fields">
                Advanced Search
            </button>
        </div>
        <jsp:include page="searchcriteriaform.jsp">
            <jsp:param name="searchGroups" value="true"/>
        </jsp:include>

        <div class="formline groupblock">
            <table class="display" id="available" title="${strings.availableSummary}">
                <caption>${strings.availableCaption}</caption>

                <thead>
                    <tr>
                        <th scope="col">${strings.nameHeader}</th>
                        <th scope="col" class="hidden">Name</th>
                        <th scope="col" class="hidden">Surname</th>
                        <th scope="col" class="hidden">School</th>
                        <th scope="col" class="hidden">Department</th>
                        <th scope="col" class="hidden">school:department</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>${strings.availableResults}${fn:length(available)}</th>
                    </tr>
                </tfoot>

                <tbody>
                    <c:choose>
                        <%-- no search results to display --%>
                        <c:when test="${empty available}">
                            <tr class="odd">
                                <td class="dataTables_empty">${strings.nothingAvailable}</td>
                            </tr>
                        </c:when>

                        <c:otherwise>
                            <%-- iterate or all available people and groups for this request --%>
                            <c:forEach items="${available}" var="item" varStatus="row">
                                <c:set var="isPerson" value="${item['class'].name == mivPersonClass}"/>
                                <c:set var="school" value="${isPerson ? item.primaryAppointment.schoolAbbreviation : ''}"/>
                                <c:set var="department" value="${isPerson ? item.primaryAppointment.departmentAbbreviation : ''}"/>
                                <c:set var="scope" value="${isPerson ? item.primaryAppointment.scope : ''}"/>
                                <c:set var="name" value="${isPerson ? item.sortName : item.name}"/>
                                <c:set var="surname" value="${isPerson ? item.surname : ''}"/>

                                <tr class="${row.count % 2 == 0 ? 'even' : 'odd'}">
                                    <td class="selectable">
                                        <c:choose>
                                            <c:when test="${not isPerson}">
                                                <div class="group description">
                                                    <span>${item.name}</span>

                                                    <div>${item.description}</div>
                                                    <%-- editing group membership only available to the group creator if read-only --%>
                                                    <c:set var="canEditGroup" value="${param.editGroups
                                                                                    and (not item.readOnly
                                                                                    or item.creator.userId == form.actingPerson.userId)}"/>
                                                    <%-- viewing confidential group membership only available to the group creator --%>
                                                    <c:if test="${item.creator.userId == form.actingPerson.userId
                                                               || not item.hidden
                                                               && not item.confidential}">
                                                        <ul style="display: none;">
                                                        <c:forEach items="${item.members}" var="members" varStatus="cnt">
                                                            <li id="${cnt.count}" class="${(param.editMembers && canEditGroup) ? 'selectable' : ''}">
                                                                ${members.sortName}
                                                                <input type="hidden" name="memberid" value="${members.userId}">
                                                                <input type="hidden" name="type" value="member">
                                                                <c:if test="${param.editMembers && canEditGroup}">
                                                                    <button name="deactivatePeople"
                                                                            type="button"
                                                                            class="memberedit">
                                                                        ${strings.deactivateLabel}
                                                                    </button>
                                                                </c:if>
                                                            </li>
                                                        </c:forEach>
                                                        </ul>
                                                    </c:if>
                                                </div>

                                                <div>
                                                    <span>${item.creator.displayName}</span>

                                                    <div><fmt:formatDate pattern="M/d/yyyy" value="${item.created}"/></div>
                                                </div>

                                                <fieldset class="groupbuttons">
                                                    <input type="hidden" name="id" value="${item.id}">
                                                    <input type="hidden" name="type" value="group">
                                                    <button name="addGroups"
                                                            type="button"
                                                            class="addGroup"
                                                            title="${strings.addGroupTitle}">
                                                        ${strings.addLabel}
                                                    </button>
                                                    <c:if test="${item.creator.userId == form.actingPerson.userId
                                                               || not item.hidden
                                                               && not item.confidential}">
                                                        <button title="View group members"
                                                                type="button"
                                                                class="view">
                                                            View members
                                                        </button>
                                                    </c:if>
                                                </fieldset>
                                            </c:when>

                                            <c:otherwise>
                                                <div class="description">
                                                    <span>${item.sortName}</span>

                                                    <div><abbr title="${item.primaryAppointment.schoolName}">${item.primaryAppointment.schoolAbbreviation}</abbr>${not empty item.primaryAppointment.departmentAbbreviation ? '&nbsp;-&nbsp;' : ''}${item.primaryAppointment.departmentAbbreviation}</div>
                                                </div>

                                                <fieldset>
                                                    <input type="hidden" name="id" value="${item.userId}">
                                                    <input type="hidden" name="type" value="person">
                                                    <button name="addPeople"
                                                            class="addPerson"
                                                            type="button"
                                                            title="${strings.addPersonTitle}">
                                                        ${strings.addLabel}
                                                    </button>
                                                </fieldset>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td class="hidden">${name}</td>
                                    <td class="hidden">${surname}</td>
                                    <td class="hidden">${school}</td>
                                    <td class="hidden">${department}</td>
                                    <td class="hidden">${scope}</td>
                                    <td class="hidden">${isPerson ? "PERSON" : "GROUP"}</td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </tbody>
            </table>
        </div>
    </c:if>

    <div class="formline groupblock">
        <table class="display${empty assigned ? ' jsVisible' : ''}" id="assigned" title="${strings.assignedSummary}">
            <caption>${strings.assignedCaption}</caption>

            <thead>
                <tr>
                    <th scope="col">${strings.nameHeader}</th>
                    <th scope="col" class="hidden">Name</th>
                    <th scope="col" class="hidden">Surname</th>
                    <th scope="col" class="hidden">School</th>
                    <th scope="col" class="hidden">Department</th>
                    <th scope="col" class="hidden">school:department</th>
                </tr>
            </thead>

            <c:if test="${not param.editMembership}">
                <tfoot>
                    <tr>
                        <th>
                            ${strings.assignedResults}${fn:length(assigned)}
                        </th>
                    </tr>
                </tfoot>
             </c:if>

            <tbody>
                <%-- iterate or all assigned people and groups for this request --%>
                <c:forEach items="${assigned}" var="item" varStatus="row">
                    <c:set var="isPerson" value="${item['class'].name == mivPersonClass}"/>
                    <c:set var="school" value="${isPerson ? item.primaryAppointment.schoolAbbreviation : ''}"/>
                    <c:set var="department" value="${isPerson ? item.primaryAppointment.departmentAbbreviation : ''}"/>
                    <c:set var="scope" value="${isPerson ? item.primaryAppointment.scope : ''}"/>
                    <c:set var="name" value="${isPerson ? item.sortName : item.name}"/>
                    <c:set var="surname" value="${isPerson ? item.surname : ''}"/>
                    <c:set var="isInactive" value="false"/>

                    <%-- find this assigned user in the inactive pile --%>
                    <c:forEach items="${inactive}" var="i">
                        <c:if test="${i.userId == item.userId}">
                            <c:set var="isInactive" value="true"/>
                        </c:if>
                    </c:forEach>

                    <tr class="${row.count % 2 == 0 ? 'even' : 'odd'}">
                        <td class="selectable">
                            <c:choose>
                                <c:when test="${not isPerson}">
                                    <div class="group description">
                                        <span>${item.name}</span>

                                        <div>${item.description}</div>

                                        <%-- viewing hidden/confidential group membership only available to the group creator --%>
                                        <c:set var="canViewMembers" value="${item.creator.userId == form.actingPerson.userId ||
                                                                             not item.hidden &&
                                                                             not item.confidential}" />
                                        <%-- editing group membership only available to the group creator if read-only --%>
                                        <c:set var="canEditGroup" value="${canViewMembers and param.editGroups
                                                                            and (not item.readOnly
                                                                            or item.creator.userId == form.actingPerson.userId)}"/>
                                        <c:if test="${canViewMembers}">
                                            <ul style="display: none;">
                                            <c:forEach items="${item.members}" var="members" varStatus="cnt">
                                                <c:set var="memberInactive" value="${item.inactiveMap.containsKey(members.userId)}" />
                                                <li id="${cnt.count}"
                                                    class="${memberInactive ? 'inactive ': ''}${(canEditGroup && param.editMembers) ? 'selectable' : ''}">
                                                    ${members.sortName}
                                                    <input type="hidden" name="memberid" value="${members.userId}">
                                                    <input type="hidden" name="type" value="member">
                                                    <c:if test="${canEditGroup && param.editMembers}">
                                                        <button name="${memberInactive
                                                                      ? 'activatePeople'
                                                                      : 'deactivatePeople'}"
                                                                type="button"
                                                                title="${strings.activateTitle}"
                                                                class="memberedit">
                                                            ${memberInactive
                                                            ? strings.activateLabel
                                                            : strings.deactivateLabel}
                                                        </button>
                                                    </c:if>
                                                </li>
                                            </c:forEach>
                                            </ul>
                                        </c:if>
                                    </div>

                                    <div>
                                        <span>${item.creator.displayName}</span>

                                        <div><fmt:formatDate pattern="d MMM yyyy" value="${item.created}"/></div>
                                    </div>

                                    <c:if test="${param.editMembership}">
                                        <fieldset>
                                            <input type="hidden" name="id" value="${item.id}">
                                            <input type="hidden" name="type" value="group">
                                            <button name="removeGroups"
                                                    class="removeGroup"
                                                    type="button"
                                                    title="${strings.removeGroupTitle}">
                                                ${strings.removeLabel}
                                            </button>
                                            <c:if test="${canViewMembers}">
                                                <button title="${canEditGroup ? strings.editMembersTitle : strings.viewMembersTitle}"
                                                        type="button"
                                                        class="view">
                                                    ${canEditGroup ? strings.editMembersLabel : strings.viewMembersLabel}
                                                </button>
                                            </c:if>
                                        </fieldset>
                                    </c:if>
                                </c:when>

                                <c:otherwise>
                                    <div class="description">
                                        <span class="${isInactive ? 'inactive' : ''}">${item.sortName}</span>

                                        <div><abbr title="${item.primaryAppointment.schoolName}">${item.primaryAppointment.schoolAbbreviation}</abbr>${not empty item.primaryAppointment.departmentAbbreviation ? '&nbsp;-&nbsp;' : ''}${item.primaryAppointment.departmentAbbreviation}</div>
                                    </div>

                                    <c:if test="${param.editMembership}">
                                        <fieldset>
                                                <input type="hidden" name="id" value="${item.userId}">
                                                <input type="hidden" name="type" value="person">
                                                <button name="removePeople"
                                                        class="removePerson"
                                                        type="button"
                                                        title="${strings.removePersonTitle}">
                                                    ${strings.removeLabel}
                                                </button>
                                        </fieldset>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="hidden">${name}</td>
                        <td class="hidden">${surname}</td>
                        <td class="hidden">${school}</td>
                        <td class="hidden">${department}</td>
                        <td class="hidden">${scope}</td>
                        <td class="hidden">${isPerson ? "PERSON" : "GROUPS"}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</fieldset>
