<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
<%----%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <c:set var="pagetitle" value="Add Dossier Reviewers: Confirmation"/>
        <title>MIV &ndash; ${pagetitle}</title>
        <script type="text/javascript">
           var mivConfig = new Object();
           mivConfig.isIE = false;
        </script>
        <%@ include file="/jsp/metatags.html" %>
        
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
            <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->

        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

	    <jsp:include page="breadcrumbs.jsp">
	        <jsp:param value="${pagetitle}" name="pageTitle"/>
	    </jsp:include>

        <!-- MIV Main Div -->
        <div id="main">
            <h1> ${pagetitle}</h1>

            <c:if test="${empty currentAssignedReviewers}">
                <div>No users have been selected to be updated.</div>
            </c:if>

            <c:if test="${!empty currentAssignedReviewers}">
                <div>The following users have been added/removed as reviewers for <strong>${userName}</strong></div><br>
				<ul class="itemlist">
                   	<c:forEach var="usr" items="${currentAssignedReviewers}">
                    	<li><strong>${usr.key}:</strong> ${usr.value}</li>
                   	</c:forEach>
                </ul>                
            </c:if>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
