<%--

    Webflow bind/validation errors message box.
    
--%><%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" buffer="none"%><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%>
<spring:hasBindErrors name="form">
    <c:set var="hasErrors" value="${errors.errorCount > 0}"/>
</spring:hasBindErrors>
<div id="messagebox"${hasErrors ? ' class="haserrors"' : ''}>
    <c:if test="${hasErrors}">
        <spring:hasBindErrors name="form">
            <div id="errorbox">
                <strong>${errors.errorCount == 1? "Error":"Errors"}</strong>
                <ul>
                    <c:forEach var="errMsgObj" items="${errors.allErrors}">
                        <li><spring:message htmlEscape="false" text="${errMsgObj.defaultMessage}"/></li>
                    </c:forEach>
                </ul>
            </div>
        </spring:hasBindErrors>
    </c:if>
</div>