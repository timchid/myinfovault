<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
     ONLY INCLUDE THE TAGLIBS YOU ACTUALLY NEED!
     Don't leave all these in your page just because they're here in the boilerplate.
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash;
    OOPS! TITLE NOT SET
    </title>

    <script type="text/javascript">
      var mivConfig = new Object();
      mivConfig.isIE = false;
    </script>
    <%@ include file="/jsp/metatags.html" %>
    
<%-- Don't blindly include mivEnterData.css just because it's here.  Only keep it if you need it. --%>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
<%-- <script type="text/javascript" src="<c:url value='/js/((yourscript)).js'/>"></script> --%>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>
    <!-- MIV Header Div -->
    <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; <a href="<c:url value='somewhere'/>">there</a>
        &gt; here
    </div><!-- breadcrumbs -->

    <!-- MIV Main Div -->
    <div id="main">

        <pre>
            THIS IS WHERE YOUR CONTENT GOES
            The only other places that should be touched are
            - the title attribute in the page head
            - page specific breadcrumbs
            - additional CSS links
            - additional Javascript links
            - change to a different miv footer.jsp if needed
        </pre>

    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
vi: se ts=8 sw=4 sr et:
--%>
