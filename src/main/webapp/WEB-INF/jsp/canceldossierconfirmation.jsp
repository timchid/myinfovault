<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><!DOCTYPE html>
<html>
    <head>
        <c:set var="pagetitle" value="Cancel Action: Verification"/>
        <title>MIV &ndash; ${pagetitle}</title>
        <script type="text/javascript">
           var mivConfig = new Object();
           mivConfig.isIE = false;
        </script>
        <%@ include file="/jsp/metatags.html" %>
        
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/postauditreview.css'/>">

        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
            <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->

        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />


    <!-- Breadcrumbs Div -->
 
  <div id="breadcrumbs">
      <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
          &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='root'/>
            </c:url>" title="Cancel Action: Search">Cancel Action: Search</a>
          &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='cancel'/>
            </c:url>" title="Cancel Action: Search">Cancel Action: Search Results</a>
         &gt; ${pagetitle}
    </div><!-- breadcrumbs -->

        <!-- MIV Main Div -->
        <div id="main">
            <h1>${pagetitle}</h1>
            <form:form name="verifyDossiers" cssClass="mivdata" method="post" commandName="searchCriteria" >

            <c:if test="${empty dossiersToCancelMap}">
                <div>No dossiers have been selected to route to Cancel.</div>
            </c:if>

            <c:if test="${!empty dossiersToCancelMap}">
            	<c:set var="dossierCount" scope="page" value="${fn:length(dossiersToCancelMap)}"/>
            	
                <p>Select the "Cancel Selected Actions" button to cancel the ${dossierCount>1?'actions':'action'} selected actions,
                     or the "Cancel" button to stop the cancellation of these ${dossierCount>1?'actions':'action'} at this time.</p>
                <p class="attention"><strong>Canceling an action will *NOT* remove any of the candidates data from the system. Only the data created as a result of submitting the action will be removed from the system.</strong></p>
                
                <div class="buttonset">
                    <input type="submit" title="Cancel Selected Actions" name="_eventId_cancelDossiers" value="Cancel Selected Actions">
                    <input type="submit" title="Cancel" name="_eventId_cancel" value="Cancel">
                    <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
                </div>

                <div id="listarea" class="verifydossiers">
                	<div class="dossiercount">Total&nbsp;Action${dossierCount>1?'s':''}&nbsp;=&nbsp;${dossierCount}</div>
                    <ul class="itemlist"><%--
                    --%><c:forEach var="dossier" items="${dossiersToCancelMap}" varStatus="rowIndex">
                        <li class="records ${(rowIndex.count)%2==0?'even':'odd'}">
                        	<strong>${dossier.key}:</strong> ${dossier.value}
                        </li><%--
                    --%></c:forEach>
                    </ul>
                </div>

                <div class="buttonset">
                    <input type="submit" title="Cancel Selected Actions" name="_eventId_cancelDossiers" value="Cancel Selected Actions">
                    <input type="submit" title="Cancel" name="_eventId_cancel" value="Cancel">
                    <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
                </div>

            </c:if>
          </form:form>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
