<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%--
--%>

<c:set var="signatures" value="${requestScope[param.signatureListKey]}"/>

<table class="display" id="${param.tableId}">
    <thead>
        <tr>
            <th class="datatable-column-filter" title="MIV User">MIV User</th>
            <th class="datatable-column-filter" title="School/College">School/College</th>
            <th class="datatable-column-filter" title="Department">Department</th>
            <th class="datatable-column-filter" title="Action">Action</th>
            <th class="datatable-column-filter" title="Release Date">Release Date</th>
            <th class="datatable-column-filter" title="Review Committee">Review Committee</th>                                
        </tr>
    </thead>

    <tbody>
        <c:forEach var="signature" items="${signatures}" varStatus="decRow">
            <c:url var='link' value='${context.request.contextPath}'>
                <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                <c:param name='_eventId' value='choose'/>
                <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                <c:param name="dossierId" value="${signature.document.dossier.dossierId}"/>
                <c:param name="schoolId" value="${signature.document.schoolId}"/>
                <c:param name="departmentId" value="${signature.document.departmentId}"/>
                <c:param name="attributeName" value="${signature.document.documentType.attributeName}"/>
            </c:url>
            
            <tr class="${(decRow.count)%2==0?'even':'odd'}">
                <td>
                    <a href="<c:out escapeXml='true' value='${link}'/>"
                       title="${signature.document.dossier.candidate.sortName}">${signature.document.dossier.candidate.sortName}</a>
                </td>

                <td>${signature.document.schoolName}</td>
                <td>${signature.document.departmentName}</td>
                <td>${signature.document.dossier.action.description}</td>
                <td><fmt:formatDate type="both" value="${signature.createdDate}" /></td>
                <td>${signature.document.dossier.reviewCommittees}</td>                                        
            </tr>                                                    
        </c:forEach>
    </tbody>
    
    <tfoot>
        <tr>
            <th colspan="6">${fn:length(signatures)} Entries found</th>
        </tr>
    </tfoot>
</table>