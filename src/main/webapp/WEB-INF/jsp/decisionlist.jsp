<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
--%><spring:message var="pageTitle" text="${strings.pageTitle}" arguments="${capacity.description}"/><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; ${pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">

    <style type="text/css" media="screen">
        /*
         * Override styles needed due to the mix of different CSS sources!
         */
        .dataTables_info { padding-top: 0; }
        .dataTables_paginate { padding-top: 0; }
        .css_right { float: right; }
        #theme_links span { float: left; padding: 2px 10px; }
    </style>

    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/decision.js'/>"></script>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${pageTitle}" name="pageTitle"/>
    </jsp:include>

    <!-- MIV Main Div -->
    <div id="main">
        <h1>${pageTitle}</h1>

        <c:choose>
            <c:when test="${fn:length(appointmentSignatures) > 0 || fn:length(otherSignatures) > 0}">
                <p><spring:message text="${strings.selectDirection}" arguments="${capacity.description}"/></p>

                <p>${strings.sortDirection}</p>

                <div class="full_width">
                    <div class="appointments">
                        <h2>New Appointment(s):</h2>
                        <jsp:include page="decisionlist-table.jsp">
                            <jsp:param name="tableId" value="appointmentlist"/>
                            <jsp:param name="signatureListKey" value="appointmentSignatures"/>
                        </jsp:include>
                    </div>

                    <div class="otheractions">
                        <h2>All Other Actions:</h2>
                        <jsp:include page="decisionlist-table.jsp">
                            <jsp:param name="tableId" value="userlist"/>
                            <jsp:param name="signatureListKey" value="otherSignatures"/>
                        </jsp:include>
                    </div>
                </div><!-- full_width -->
            </c:when>

            <c:otherwise>
                <spring:message text="${strings.noDecisions}" arguments="${capacity.description}"/>
            </c:otherwise>
        </c:choose>
    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
vi: se ts=8 sw=4 et:
--%>
