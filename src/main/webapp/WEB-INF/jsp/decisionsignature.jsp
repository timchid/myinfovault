<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><c:set var="wetSignatureUrl" value="${form.wetSignatureUrl}"/><%--
--%><spring:message var="pageTitle" text="${strings.pageTitle}" arguments="${form.decision.documentType.alternateDescription}"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" media="screen" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" media="screen" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" media="screen" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" media="screen" href="<c:url value='/css/decision.css'/>">
    <link rel="stylesheet" type="text/css" media="print" href="<c:url value='/css/printdecision.css'/>">

    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <jwr:script src="/bundles/mivvalidation.js" />
    <script type="text/javascript" src="<c:url value='/js/decisionsignature.js'/>"></script>
</head>
<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${pageTitle}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
        <h1>${pageTitle}</h1>

        <jsp:include page="binderrors.jsp" />

        <div id="headingbox" class="formline">
            <div><strong>${strings.candidateLabel}</strong> ${form.decision.dossier.candidate.displayName}</div>
            <div>
                <strong>${strings.viewPdfLabel}</strong>
                <a href="<c:out escapeXml="true" value="${form.dossierUrl}"/>"
                   title="${form.decision.dossier.action.description}">${form.decision.dossier.action.description}</a>
            </div>
            <div>
                <strong>${strings.primaryDeptLabel}</strong>
                ${form.decision.dossier.primaryDepartment.school}${!empty form.decision.dossier.primaryDepartment.description ? ' &ndash; ' : ''}
                ${form.decision.dossier.primaryDepartment.description}
            </div>

            <%-- if not appeal decision --%>
            <c:if test="${form.decision.documentType != 'DEANS_APPEAL_DECISION'
                       && form.decision.documentType != 'VICEPROVOSTS_APPEAL_DECISION'
                       && form.decision.documentType != 'JOINT_DEANS_APPEAL_RECOMMENDATION'}">
                <div><strong>${strings.presentRankLabel}</strong> ${form.decision.present}</div>
                <div><strong>${strings.proposedRankLabel}</strong> ${form.decision.proposed}</div>
            </c:if>
            <div style="margin-top: 0.6em;">
                <strong>${strings.effectiveDateLabel}</strong>
                <fmt:formatDate value="${form.decision.dossier.action.effectiveDate}" pattern="M/d/yyyy" />
            </div>
        </div><!-- #headingbox -->

        <form:form id="signatureForm" cssClass="mivdata" method="post" commandName="form">
            <%-- wet signature upload and print buttons only available to provost/chancellor signable decisions/recommendations --%>
            <c:if test="${form.decision.documentType.allowedSigner == 'PROVOST'
                       || form.decision.documentType.allowedSigner == 'CHANCELLOR'}">
               <div class="formline">
                    <miv:permit roles="SYS_ADMIN|VICE_PROVOST_STAFF|OCP_STAFF" usertype="REAL">
                        <button id="printPdf"
                                title="<spring:message text="${tooltips.printWetSignature}"
                                                       arguments="${form.decision.documentType.description}"/>">${labels.printWetSignature}</button>
                    </miv:permit>

                    <%-- OCP staff may not upload/remove wet signatures --%>
                    <miv:permit roles="!OCP_STAFF">
                        <c:choose>
                            <%-- wet signature uploaded --%>
                            <c:when test="${not empty wetSignatureUrl}">
                                <a href="<c:out escapeXml='true' value='${wetSignatureUrl}'/>"
                                   title="<spring:message text="${tooltips.viewWetSignature}"
                                                          arguments="${form.decision.documentType.description}"
                                                          htmlEscape="false"/>">${form.decision.documentType.description}</a>

                                <button type="submit" formnovalidate
                                        name="_eventId_remove"
                                        title="<spring:message text="${tooltips.removeWetSignature}"
                                                               arguments="${form.decision.documentType.description}"/>">${labels.removeWetSignature}</button>
                            </c:when>

                            <%-- no wet signature uploaded --%>
                            <c:otherwise>
                                <miv:permit roles="SYS_ADMIN|VICE_PROVOST_STAFF" usertype="REAL">
                                    <button type="submit" formnovalidate
                                            name="_eventId_add"
                                            title="<spring:message text="${tooltips.addWetSignature}"
                                                                   arguments="${form.decision.documentType.description}"/>">${labels.addWetSignature}</button>
                                </miv:permit>
                            </c:otherwise>
                        </c:choose>
                    </miv:permit>
                </div>
            </c:if>

            <%-- OCP staff may not sign --%>
            <miv:permit roles="!OCP_STAFF">
                <fieldset class="mivfieldset formline">
                    <legend><spring:message text="${labels.action}"
                                            arguments="${form.decision.dossier.delegationAuthorityDescription}"/></legend>

                    <div>
                        <fieldset class="mivfieldset decision">
                            <legend><spring:message text="${form.decision.documentType.decision ? labels.decision : labels.recommendation}"
                                                    arguments="${form.decision.dossier.delegationAuthorityDescription}"/></legend>

                            <ul>
                                <form:radiobuttons path="decision.decisionType"
                                                   items="${form.decisionTypes}"
                                                   element="li"/>
                            </ul>
                        </fieldset>

                        <c:if test="${not empty form.decisionTypes.OTHER}">
                            <div id="ifother">
                            <spring:message text="${strings.ifOther}" arguments="${form.decision.documentType.allowedSigner.description}"/>
                            </div>
                        </c:if>

                        <div>
                            <form:checkbox path="decision.contraryToCommittee"
                                           label="${labels.contraryToCommittee}"
                                           title="${tooltips.contraryToCommittee}"/>
                        </div>
                    </div>
                </fieldset>

                <div class="formline">
                    <spring:bind path="decision.comments">
                        <div><label for="${status.expression}"><spring:message text="${strings.commentsLabel}"
                                                                               arguments="${form.decision.documentType.allowedSigner.description}"
                                                                               htmlEscape="false"/></label></div>

                        <textarea id="${status.expression}"
                                  name="${status.expression}"
                                  rows="5" cols="80" autofocus>${status.value}</textarea>
                    </spring:bind>
                </div>
                <div id="lineBreakNote">${strings.commentsDagger}</div>

                <p><spring:message text="${strings.agreement}" arguments="${pageTitle}" htmlEscape="false"/></p>

                <div class="buttonset">
                    <input type="submit"
                           title="${pageTitle}"
                           name="_eventId_sign"
                           value="${pageTitle}">
                    <input type="submit"
                           title="${tooltips.cancelButton}"
                           name="_eventId_breadcrumb1"
                           value="${labels.cancelButton}"
                           formnovalidate>
                </div>
            </miv:permit>
        </form:form>
    </div>

    <div id="printpage">
        <div class="decisiontype">${form.decision.documentType.alternateDescription}</div>
        <div class="candidate">${form.decision.dossier.candidate.displayName},&nbsp;${form.decision.dossier.action.delegationAuthority.description}:&nbsp;${form.decision.dossier.action.description}</div>

        <ul class="selections">
        <c:forEach var="decisionType" items="${form.decisionTypes}">
                        <li>${decisionType["value"]}</li>
        </c:forEach>

        </ul>

        <c:if test="${not empty form.decisionTypes.OTHER}">
        <div id="ifotherPrint">
            <spring:message text="${strings.ifOtherPrint}" arguments="${form.decision.documentType.allowedSigner.description}"/>
        </div>
        </c:if>

        <ul class="selections">
                <li>${labels.contraryToCommittee}</li>
        </ul>

        <div class="commentslabel"><spring:message text="${strings.commentsLabel}" arguments="${form.decision.documentType.allowedSigner.description}"/></div>
        <div class="commentsarea">&nbsp;</div>
        <div class="signature">Signed:<span class="signatureline">&nbsp;</span></div>
        <div class="date">Date:<span class="dateline">&nbsp;</span></div>
        <div class="signer">${form.signingPerson.displayName}</div>
    </div>

    <jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html><%--
vi: se ts=8 sw=4 et:
--%>
