<%--
  JSP fragment to display an unordered list of categories for an event
  log entry. For use in report.jsp. Configured in mivconfig.properties.

  Request Scope  | Description
  ---------------+---------------------------------------------------
  fieldValue     | set of event categories for the parent log entry
  category       | event category filtering the event log parent page

--%><%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <ul><%--
    --%><c:forEach var="eventCategory" items="${fieldValue}"><%--
        --%><%-- not showing category by which this log is filtered --%><%--
        --%><c:if test="${category != eventCategory}">
            <li>${eventCategory.description}</li><%--
        --%></c:if><%--
    --%></c:forEach>
    </ul>
