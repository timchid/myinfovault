<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:set var="isCreator" scope="request" value="${form.group.creator.userId == form.actingPerson.userId}"/><%--
--%><!DOCTYPE html>
<html>
    <%-- set the page title --%>
    <c:choose>
        <%-- group already exists --%>
        <c:when test="${form.group.id > 0}">
            <c:set var="title" value="${isEdit ? strings.editPageTitle : (isConfirmation ? strings.confirmationPageTitle : strings.viewPageTitle)} ${form.group.name}"/>
        </c:when>

        <%-- group is new --%>
        <c:otherwise>
           <c:set var="title" value="${strings.addPageTitle}"/>
        </c:otherwise>
    </c:choose>

    <head>
        <title>MIV &ndash; ${title}</title>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/jquery.qtip.min.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/group.css'/>">

        <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/jquery.qtip.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/qtip_config.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <jwr:script src="/bundles/mivvalidation.js" />
        <script type="text/javascript" src="<c:url value='/js/groupedit.js'/>"></script>
    </head>

    <body>
        <c:set var="helpPage" scope="request" value="manage_groups.html"/>
        <c:set var="helpTip" scope="request" value="Help with creating and managing groups"/>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${title}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${title}</h1>

            <jsp:include page="binderrors.jsp" />

            <form:form id="assignForm" method="post" commandName="form">
                <c:choose>
                    <%-- user has permission to edit the group details --%>
                    <c:when test="${isEdit and form.group.type == 'GROUP'}">
                        <div id="enterdataguide">
                            <div id="requiredfieldlegend">${strings.requiredKey}</div>
                        </div>

                        <fieldset class="assignmentStep columns">
                            <legend>${strings.attributesLegend}</legend>
                            <jsp:include page="/help/group_identity.html"></jsp:include>
                            <%--div class="helpicon">?</div--%>
                            <div class="helpicon"><img src="<c:url value='/images/help.png'/>" alt="Help for Step 1"></div>
                            <div class="formline">
                                <div>
                                    <form:label path="group.name" cssClass="f_required">
                                        ${strings.nameLabel}
                                    </form:label>
                                </div>

                                <div>
                                    <form:input path="group.name"
                                                title="${strings.nameTitle}"
                                                maxlength="25"/>
                                </div>
                            </div>

                            <div class="formline">
                                <div>
                                    <form:label path="group.description">
                                        ${strings.descriptionLabel}
                                    </form:label>
                                </div>

                                <div>
                                    <form:input path="group.description"
                                                title="${strings.descriptionTitle}"
                                                size="50" maxlength="100"/>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="assignmentStep" id="permission">
                            <legend>${strings.restrictionLegend}</legend>
                            <jsp:include page="/help/group_sharing.html"></jsp:include>
                            <%--div class="helpicon">?</div--%>
                            <div class="helpicon"><img src="<c:url value='/images/help.png'/>" alt="Help for Step 2"></div>

                            <div class="formline">
                                <c:if test="${isCreator}">
                                    <fieldset class="mivfieldset">
                                        <form:radiobutton path="group.hidden"
                                                          label="${strings.sharedLabel}"
                                                          title="${strings.sharedTitle}"
                                                          value="false"/>
                                        <form:radiobutton path="group.hidden"
                                                          label="${strings.notSharedLabel}"
                                                          title="${strings.notSharedTitle}"
                                                          value="true"/>
                                    </fieldset>
                                </c:if>

                                <fieldset class="mivfieldset">
                                    <c:forEach items="${form.assignedRoles}" var="assignedRole">
                                        <c:set var="role" value="${assignedRole.key}"/>

                                        <fieldset class="mivfieldset">
                                            <c:if test="${role == 'SCHOOL_STAFF' or role == 'DEPT_STAFF'}">
                                                <legend>${role.description}s</legend>
                                            </c:if>

                                            <ul>
                                                <c:forEach items="${assignedRole.value}" var="assignment" varStatus="assignmentRow">
                                                    <li class="${assignment.disabled ? 'disabled' : ''}">
                                                        <form:checkbox path="assignedRoles[${role}][${assignmentRow.index}][checked]"
                                                                       value="true"
                                                                       label="${assignment.label}"
                                                                       title="${strings.restrictionLabel} ${assignment.label}"
                                                                       disabled="${assignment.disabled}"/>
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                        </fieldset>
                                    </c:forEach>

                                    <c:if test="${isCreator}">
                                        <form:checkbox path="group.readOnly"
                                                       label="${strings.readonlyLabel}"
                                                       title="${strings.readonlyTitle}"/>
                                    </c:if>
                                </fieldset>
                            </div>
                        </fieldset>
                    </c:when>

                    <%-- user doesn not have permission to edit details --%>
                    <c:otherwise>
                        <div class="assignmentSummary">
                            <div>
                                <span>${strings.nameLabel}</span>
                                ${form.group.name}
                            </div>

                            <c:if test="${not empty form.group.description}">
                                <div>
                                    <span>${strings.descriptionLabel}</span>
                                    ${form.group.description}
                                </div>
                            </c:if>

                            <div>
                                <span>${strings.creatorLabel}</span>
                                ${form.group.creator.displayName}
                            </div>
                            <div>
                                <span>${strings.createdLabel}</span>
                                <fmt:formatDate pattern="M/d/yyyy" value="${form.group.created}"/>
                            </div>
                        </div>

                        <c:if test="${isConfirmation}">
                            <div class="hasmessages" id="CompletedMessage">
                                 <div id="successbox">${strings.confirmationMessage}</div>
                            </div>
                            <script type="text/javascript">
                                $("#CompletedMessage").removeClass("hasmessages");
                                showMessage("CompletedMessage");
                            </script>
                        </c:if>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${isCreator or not form.group.confidential}">
                        <jsp:include page="assignment.jsp">
                            <jsp:param name="isReview" value="false"/>
                            <jsp:param name="canSwitch" value="${canSwitch}"/>
                            <jsp:param name="editMembership" value="${isEdit and form.group.type == 'GROUP'}"/>
                            <jsp:param name="editMembers" value="${isEdit}"/>
                            <jsp:param name="editGroups" value="${isEdit}"/>
                        </jsp:include>
                    </c:when>

                    <c:otherwise>
                        <p>${strings.membershipConfidential}</p>
                    </c:otherwise>
                </c:choose>

                <%-- use buttonset if available table is shown so that buttons appear under the assigned table --%>
                <div class="${(isCreator or not form.group.confidential) and isEdit and form.group.type == 'GROUP' ? 'buttonset' : ''}">
                    <c:choose>
                        <c:when test="${isEdit}">
                            <button type="submit"
                                    name="_eventId"
                                    value="save"
                                    class="button"
                                    title="${not empty form.group and form.group.id > 0
                                           ? strings.saveTitle
                                           : strings.createTitle}">
                                ${not empty form.group and form.group.id > 0
                                ? strings.saveLabel
                                : strings.createLabel}
                            </button>
                        </c:when>

                        <c:when test="${canSwitch}">
                            <button type="submit"
                                    name="_eventId"
                                    value="edit"
                                    class="button"
                                    title="${strings.editTitle}"
                                    formnovalidate>
                                ${strings.editLabel}
                            </button>
                        </c:when>
                    </c:choose>

                    <button type="submit"
                            name="_eventId"
                            value="breadcrumb1"
                            class="button"
                            title="${strings.backTitle}"
                            formnovalidate>
                        ${isEdit
                        ? strings.cancelLabel
                        : strings.doneLabel}
                    </button>
                </div>
            </form:form>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
