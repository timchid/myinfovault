<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
     ONLY INCLUDE THE TAGLIBS YOU ACTUALLY NEED!
     Don't leave all these in your page just because they're here in the boilerplate.
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash;
    Hello World test page
    </title>

    <script type="text/javascript">
      var mivConfig = new Object();
      mivConfig.isIE = false;
    </script>
    <%@ include file="/jsp/metatags.html" %>
    
<%-- Don't blindly include mivEnterData.css just because it's here.  Only keep it if you need it. --%>
    <%--link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>"-->
<%-- <script type="text/javascript" src="<c:url value='/js/((yourscript)).js'/>"></script> --%>

    <style>
    span.var {
        font-family: Andale Mono, Consolas, Courier, monospace;
    }
    #main ol, #main ul {
        margin-left: 2em;
    }
    </style>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>
    <!-- MIV Header Div -->
    <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; <a href="<c:url value='somewhere'/>">there</a>
        &gt; here
    </div><!-- breadcrumbs -->

    <!-- MIV Main Div -->
    <div id="main">

        <h2>This is <em>helloWorld.jsp</em><small> — based on <em>boilerplate.jsp</em></small></h2>

        <p><span class="var">message</span> is: ${message}<br>
           <span class="var">user</span> is: ${user.userInfo}</p>

        <pre>
            THIS IS WHERE YOUR CONTENT GOES
            The only other places that should be touched are
            - the title attribute in the page head
            - page specific breadcrumbs
            - additional CSS links
            - additional Javascript links
            - change to a different miv footer.jsp if needed
        </pre>


        <c:if test="${requests != null}">
        <p>Packet Requests:</p>
        <ol>
        <c:forEach var="pktreq" items="${requests}" varStatus="row">
            <li>${pktreq}</li>
        </c:forEach>
        </ol>
        </c:if>

        <c:if test="${packets != null}">
        <p>Packets:</p>
        <ol>
        <c:forEach var="pkt" items="${packets}" varStatus="row">
            <li>${pkt}</li>
        </c:forEach>
        </ol>
        </c:if>

    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
vi: se ts=8 sw=4 sr et:
--%>
