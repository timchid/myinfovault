<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; <%--Manage Open Actions: Search Results
    --%>${strings.description}<c:if test="${fn:length(strings.subtitle)>0}">:
    ${strings.subtitle}</c:if>
    </title>
    <script type="text/javascript">
    var mivConfig = new Object();
    mivConfig.isIE = false;
  </script>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/dossierAction.css'/>">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
    <![endif]-->
    <script type="text/javascript" src="<c:url value='/js/openactions.js'/>"></script>
    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>

</head>
<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${strings.description}" name="pageTitle"/>
    </jsp:include>

  <!-- Dossier Data -->
  <div id="main">
      <h1>Open Action</h1>
      <div id="errormessage">
           <c:if test="${errors.errorCount > 0}"><%--
       --%><strong>ERROR - The following ${errors.errorCount > 1 ? 'errors were' : 'error was'} found:</strong>
          <ul><%--
          --%><c:forEach var="errMsgObj" items="${errors.allErrors}">
              <li><spring:message htmlEscape="false" text="${errMsgObj.defaultMessage}"/></li><%--
          --%></c:forEach>
          </ul><%--
           --%></c:if>
      </div>

    <c:if test="${!empty actionMsg}">
      <c:if test="${!empty actionError}">
      <div id="dossiermsg"><span class="attention">${actionMsg}</span>
          <p>If you have problems routing a dossier, please contact the MIV Project Team at
          <a href="mailto:miv-help@ucdavis.edu" title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>
          </p>
      </div>
      </c:if>
      <c:if test="${empty actionError}">
      <div id="dossiermsg">${actionMsg}</div>
      </c:if>
    </c:if>

<jsp:include page="dossierdetail.jsp" />

  <c:if test="${!displayOnly}">
<!--     <div id="dossierpdflink"> -->
<%--       <a title="View The Dossier as One PDF File" href="${dossierpdf}">View The Dossier as One PDF File</a> --%>
<!--     </div> -->

<!-- nextNodeEnum ${nextNodeEnum} completeJoint ${completeJoint}  dossierLocation ${dossierLocation}  -->
<!-- routingErrorList ${routingErrorList} -->

  <c:if test="${!completeJoint}">

   <!-- Only these roles may route a dossier -->
<%--    <miv:permit roles="DEPT_CHAIR|DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF|SENATE_STAFF|VICE_PROVOST|SYS_ADMIN"> --%>
        <miv:permit action="Route Dossier" location="${currentNode.workflowNodeName}" qualifier="${schoolDeptQualifier}">
    <div id="returnit"><%--
    --%><c:if test="${previousNode.workflowNodeName != 'Unknown'}"><%--
       --%><c:set var="tagMesg" value="" /><%--
       --%><c:choose><%--
        --%><c:when test="${currentNode.workflowNodeName == 'Senate' || currentNode.workflowNodeName == 'Federation' || currentNode.workflowNodeName == 'SenateFederation'}"><%--
            --%><c:set var="tagText" value="Return This Dossier to the ${previousNode.workflowNodeDesc} for Review" scope="page" /><%--
            --%><c:set var="tagMesg" value="(ALL recommendations will be deleted)" /><%--
        --%></c:when><%--
        --%><c:when test="${currentNode.workflowNodeName == 'ViceProvost' || currentNode.workflowNodeName == 'ViceProvostPostSenate'}"><%--
            --%><c:set var="tagText" value="Return This Dossier to the ${previousNode.workflowNodeDesc} for Review" scope="page" /><%--
            --%><c:set var="tagMesg" value="(ALL recommendations as well as the Vice Provost's Final Decision will be deleted)" /><%--
        --%></c:when><%--
        --%><c:when test="${currentNode.workflowNodeName == 'Department'}"><%--
            --%><c:set var="tagText" value="Request a new packet for this dossier from the candidate" scope="page" /><%--
            --%><c:set var="tagMesg" value="(No documents/letters currently present for this dossier will be visible to the candidate nor will they be deleted from the dossier)"/><%--
        --%></c:when><%--
        --%><c:otherwise><%--
          --%><c:set var="tagText" value="Return This Dossier to the ${previousNode.workflowNodeDesc}" scope="page" /><%--
            --%><c:if test="${currentNode.workflowNodeName == 'School' || currentNode.workflowNodeName == 'PostSenateSchool'}"><%--
              --%><%--c:set var="tagMesg" value="(No documents/letters will be visible to the Department, nor will they be deleted from the dossier; Dean’s Recommendation/Final Decision will be deleted)"/--%><%--
              MIV-4319 : Warn people to remove documents before returning the Dossier to the Department. (The c:set line above can be removed - it is replaced with this next line)
              --%><c:set var="tagMesg" value="(All documents/recommendations/decisions added at the School/College will be deleted, including any Joint Dean documents/recommendations/decisions.)"/><%--
            --%></c:if><%--
        --%></c:otherwise><%--
        --%></c:choose><%--
     --%><c:url var='backLink' value='OpenActions'><%--
          --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/><%--
          --%><c:param name='_eventId' value='return'/><%--
          --%><c:param name='dossierId' value='${dossier.dossierId}'/><%--
          --%></c:url>
         <!-- Do not show any return links at the Department location -->
         <c:if test="${currentNode.workflowNodeName != 'Department'}">
           <ul class="itemlist">
             <li><a href="<c:out value='${backLink}' escapeXml='true'/>"
               title="${tagText}">${tagText}</a>&nbsp;<span>${tagMesg}</span></li>
             <%-- Display any additional text lines following the return links --%><%--
             --%><c:forEach var="additionalText" items="${currentNode.additionalReturnTextList}"><%--
               --%><li><em>${additionalText}</em></li><%--
             --%></c:forEach>
           </ul>
         </c:if>
         <!--.itemlist--><%--
    --%></c:if>
    </div>

    <div id="routeit" class="${routeStatus}"><%--
    --%><c:url var='nextLink' value='OpenActions'><%--
        --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/><%--
        --%><c:param name='_eventId' value='route'/><%--
        --%><c:param name='dossierId' value='${dossier.dossierId}'/><%--
    --%></c:url><%--
    --%><c:set var="tagNote" value="" scope="page" /><%--
    <%-- Decide on active vs. disabled link, based on dossier status --%><%--
    --%><ul class="itemlist"><%--
    --%><c:forEach var="node" items="${nextNodesStatusMap}"><%--
         --%><c:choose><%--
             <%-- Decide on active vs. disabled link, based on dossier status --%><%--
             --%><c:when test="${node.value.workflowNodeStatus == 'ready'}"><%--
                 --%><c:choose><%--
                     --%><c:when test="${node.value.workflowNodeName == 'ReadyToArchive'}"><%--
                         --%><c:set var="tagText" value="Send The Completed Dossier to the ${node.value.workflowNodeDesc}" scope="page" /><%--
                         --%><c:set var="tagMesg" value="(ALWAYS remove CRC recommendations before returning the dossier)" /><%--
                         --%><c:if test="${node.value.workflowNodeName == 'ViceProvost'}"><%--
                             --%><c:set var="tagNote" value=" <span class='${routeStatus}'></span>" scope="page" /><%--
                         --%></c:if><%--
                     --%></c:when><%--
                     --%><c:when test="${node.value.workflowNodeName == 'FederationSchool'}"><%--
                         --%><c:set var="tagText" value="Send This Dossier to the ${node.value.workflowNodeDesc} for Final Decision" scope="page" /><%--
                     --%></c:when><%--
                     --%><c:when test="${node.value.workflowNodeName == 'FederationViceProvost'}"><%--
                         --%><c:set var="tagText" value="Send This Dossier to the ${node.value.workflowNodeDesc} for CRC recommendation" scope="page" /><%--
                     --%></c:when><%--
                     --%><c:otherwise><%--
                         --%><c:set var="tagText" value="Send This Dossier to the ${node.value.workflowNodeDesc}" scope="page" /><%--
                     --%></c:otherwise><%--
                 --%></c:choose><%--
                 --%><c:url var='nextLink' value='OpenActions'><%--
                     --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/><%--
                     --%><c:param name='_eventId' value='route'/><%--
                     --%><c:param name='dossierId' value='${dossier.dossierId}'/><%--
                     --%><c:param name='nextNode' value="${node.value.workflowNodeName}"/><%--
                 --%></c:url><%--
                 --%><c:set var="tagTitle" value="Send This Dossier to the ${node.value.workflowNodeDesc}" scope="page" /><%--
                 --%><c:set var="tagClass" value="${node.value.workflowNodeStatus}" scope="page" /><%--
                 --%><c:set var="tagType" value="a" scope="page" /><%--
                   --%><c:set var="tagHref" value=" href=\"${nextLink}\"" scope="page" /><%--
              --%></c:when><%--
              --%><c:otherwise><%--
                   --%><c:set var="tagClass" value="${node.value.workflowNodeStatus}" scope="page" /><%--
                   --%><c:set var="tagType" value="span" scope="page" /><%--
                   --%><c:set var="tagText" value="Send This Dossier to the ${node.value.workflowNodeDesc}" scope="page" /><%--
                   --%><c:set var="tagTitle" value="This Dossier is Not Ready to Send to the ${node.value.workflowNodeDesc}" scope="page" /><%--
                   --%><c:set var="tagHref" value="" scope="page" /><%--
              --%></c:otherwise><%--
              --%></c:choose><%--
              --%><li class="${tagClass}"><${tagType} class="${tagClass}" title="${tagTitle}"${tagHref}>${tagText}</${tagType}>${tagNote}</li><%--
               --%><c:if test="${node.value.workflowNodeName == 'Archive' && node.value.workflowNodeStatus != 'ready'}"><%--
                 --%><li><em><${tagType} class="ready">Please contact the Vice Provost's office when this dossier is ready to archive</${tagType}></em></li><%--
              --%></c:if><%--
              --%></c:forEach><%--
               --%><%-- Display any additional text lines following the routing links --%><%--
              --%><c:forEach var="additionalText" items="${currentNode.additionalRoutingTextList}"><%--
                --%><li><em><${tagType} class="ready">${additionalText}</${tagType}></em></li><%--
              --%></c:forEach><%--   
         --%></ul><%--
   --%></div>
   </miv:permit>

  </c:if>
  </c:if><%-- end if test="${!displayOnly}" --%>

  </div><!-- main -->


  <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>

</html>
