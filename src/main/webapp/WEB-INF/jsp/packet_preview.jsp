<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" href="<c:url value='/css/packets.css'/>">

    <style>
    div.X_types {
        padding-left: 1.5em;
        background-color: lightyellow;
        border-left: 2px solid blue;
    }

    div.X_doc {
        max-width: 80%;
        margin-left: 2.0em;
        padding-left: 1.5em;
        background-color: lightgreen;
        border-left: 2px dotted green;
    }
    </style>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="/WEB-INF/jsp/breadcrumbs.jsp">
        <jsp:param value="${pageTitle}" name="pageTitle"/>
    </jsp:include>

    <main id="main" role="main">

        <h1>Preview <span class="documentname">${packet.packetName}</span></h1>

        <p>
        Your Packet documents are listed below.
        If you add new data in your account you will have to choose to include it in this packet by
        <a href="<c:url value='/packet/design'>
                 <c:param name='packetId' value='${packet.packetId}'/>
                 </c:url>">editing this packet</a>
        and selecting the new data.
        </p>

        <div><%-- container --%><%-- Is this needed? Remove it? --%>

            <c:if test="${not empty combinedPacketDocument.outputUrl}">
            <div id="combined">
            <a title="View My Packet as One PDF File"
               href="${combinedPacketDocument.outputUrl}">View
               <span class="documentname">${packet.packetName}</span>
               as One PDF File</a>
            </div>
            </c:if>

            <div class="types">
                <header>
                    <h2>View <span class="documentname">${packet.packetName}</span> as individual PDF files</h2>
                </header>
                
                <c:set var="damagedFiles" value="0"/>
                <ul class="itemlist">
                <c:forEach var="detail" items="${document_types}">
                    <li>
                        <div class="doc">
                    
                        <c:forEach var="document" items="${detail.outputDocuments}">
                             <c:choose>
                                    <c:when test="${document.createSuccess}">
                                        <a title="${detail.description} ${document.documentFormat}"
                                           href="${document.outputUrl}">${document.documentFormat}</a>
                                    </c:when>

                                    <c:otherwise>
                                        <c:set var="damagedFiles" value="${damagedFiles+1}"/>
                                            
                                        <strong class="warning">ERROR</strong>
                                    </c:otherwise>
                                </c:choose>
                            ${detail.description}
                        </c:forEach>
                        </div><%-- .doc --%>
                    </li>
                </c:forEach>
                </ul>
                <c:if test="${damagedFiles > 0}">
                    <p class="warning"><spring:message text="ERROR: {0} {0,choice,1#file |1<files } could not
                                       be created for this packet. Please review the data
                                       for the PDF files with errors to correct the invalid data."
                                       arguments="${damagedFiles}"/></p>
                </c:if>
        
            </div><%-- .types --%>

        </div><%-- container --%>

    </main>

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>

</html><%--
vi: se ts=8 sw=4 sr et:
--%>
