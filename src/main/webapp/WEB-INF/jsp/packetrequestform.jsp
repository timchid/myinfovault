<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><c:set var="pageTitle" value="${strings.pageTitle}"/><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${pageTitle}&nbsp;${requestAction}</title>

        <%@ include file="/jsp/metatags.html"%>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <jwr:script src="/bundles/mivvalidation.js" />
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${pageTitle}&nbsp;${requestAction}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${pageTitle}&nbsp;${requestAction}</h1>

            <jsp:include page="binderrors.jsp" />

            <form:form id="packetRequest" cssClass="mivdata" method="post" commandName="form">
                <div id="enterdataguide">
                    <div id="requiredfieldlegend">* = Required Field</div>
                </div>

                <div id="maindialog" class="pagedialog">
                    <div class="mivDialog wideDialog">
                        <h1>${form.dossier.action.candidate.displayName}</h1>

                        <div id="summary" class="summary">
                            <div class="formline">
                                <jsp:useBean id="now" class="java.util.Date" scope="page" />
                                <strong>Date:</strong>&nbsp;<fmt:formatDate type="date" dateStyle="long" value="${now}"/>
                            </div>
                            <div class="formline">
                                <strong>Action:</strong>&nbsp;${form.dossier.action.description}
                            </div>
                        </div><!-- #summary -->

                         <div class="buttonset">
                           <input type="submit" name="_eventId_mail" id="mail" value="Email Notification to Candidate" title="Email Notification to Candidate">
                           <input type="submit" name="_eventId_cancelNotification" id="cancel" value="Cancel Email Notification" title="Cancel Email Notification">
                         </div>
                    </div><!-- mivDialog -->
                </div><!-- mainDialog -->
            </form:form>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html><%--
 vi: se ts=8 sw=4 et:
--%>
