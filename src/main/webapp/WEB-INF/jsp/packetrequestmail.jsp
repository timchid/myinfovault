<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; ${strings.pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEmail.css'/>">
    
    <script type="text/javascript" src="<c:url value='/js/disclosuremail.js'/>"></script>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${strings.pageTitle}&nbsp;${requestAction}" name="pageTitle"/>
    </jsp:include>

    <!-- MIV Main Div -->
    <div id="main">
        <h1>${strings.pageTitle}&nbsp;${requestAction}</h1>

        <spring:hasBindErrors name="form">
            <c:set var='errorClass' scope='page' value=' class="haserrors"'/>
            <div id="errormessage"${errorClass}>
            <div id="errorbox">
                <strong>
                    Error:
                <ul>
                        <li>Failed to send packet request email to one or more recipients. Make sure the email address(es) are correct.</li>
                </ul>
                </strong>
            </div>
        </div>
        </spring:hasBindErrors>
        <!-- Leaving in the next set of errormessage divs just in case the errors object ever gets passed into the page -->
        <div id="errormessage"${errorClass}>
                <c:if test="${errors.errorCount > 0}">
            <div id="errorbox">
                <strong>
                    ${errors && errors.errorCount > 1 ? "Errors:" : "Error:"}
                </strong>
                
                <ul>
                    <c:forEach var="errObj" items="${errors.allErrors}">
                        <li><spring:message htmlEscape="false" text="${errObj.defaultMessage}"/></li>
                    </c:forEach>
                </ul>
            </div>
            </c:if>
        </div>

        <p>${strings.instruction}</p>

        <p>
              <strong>${requestAction == 'CANCELED' ? 'Cancel Packet Request:' : 'Submit Packet Request:'}&nbsp;</strong>${form.dossier.action.description}<br/>
        </p>

        <form:form cssClass="mivdata" method="post" commandName="form" >
            <div class="fakemail">
                <div class="headerline">
                    <form:label path="emailFrom" cssClass="prompt">${strings.fromLabel}</form:label>
                    <form:input path="emailFrom" cssClass="field" size="62" maxlength="60" disabled="true"/>
                </div>
    
                <div class="headerline">
                    <form:label path="emailTo" cssClass="prompt">${strings.toLabel}</form:label>
                    <form:input path="emailTo" cssClass="field" size="62" maxlength="60" disabled="${not MIVSESSION.admin}"/>
                </div>
    
                <div class="headerline">
                    <form:label path="emailCc" cssClass="prompt">${strings.ccLabel}</form:label>
                    <form:input path="emailCc" cssClass="field" size="62" maxlength="60"/>
                </div>
    
                <div class="headerline">
                    <form:label path="emailSubject" cssClass="prompt">${strings.subjectLabel}</form:label>
                    <form:input path="emailSubject" cssClass="field" size="62" maxlength="60" disabled="true"/>
                </div>
                
                <div class="headerline">
                    <form:label path="emailMessage" cssClass="prompt">${strings.contentLabel}</form:label>
                    <form:textarea path="emailMessage" cssClass="field" cols="80" rows="9" disabled="true"/>
                </div>
                
                <div class="headerline">
                    <spring:bind path="form.emailBody">
	                    <label for="${status.expression}" class="prompt">${strings.infoLabel}</label>
	                    <textarea id="${status.expression}" name="${status.expression}"
	                              class="field" cols="80" rows="4"
	                              placeholder="${strings.infoPlaceholder}">${status.value}</textarea>
                    </spring:bind>
                </div>
                
                <div class="buttonset">
                     <c:set var='action' value="${requestAction == 'CANCELED' ? 'Cancel' : 'Submit '}"/>   
                    <input type="submit" name="_eventId_save" id="save"
                           value="${action}&nbsp;${strings.sendButtonLabel}" title="${action}&nbsp;${strings.sendButtonTitle}"/>
                    <input type="submit" name="_eventId_back" id="cancel"
                           value="${strings.cancelButtonLabel}" title="${strings.cancelButtonTitle}"/>
                </div>
            </div><!-- fakemail -->
        </form:form>
    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vi: se ts=8 sw=4 et sr:
--%>