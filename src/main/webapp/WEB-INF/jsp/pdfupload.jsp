<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><c:set var="title" value="Add &quot;${uploadDescription}&quot; PDF Upload"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
  <title>MIV &ndash; ${title}</title>

  <%@ include file="/jsp/metatags.html" %>

  <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

  <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/js/validation.js'/>"></script>
</head>

<body class="tundra">
  <jsp:include page="/jsp/mivheader.jsp" />

    <c:choose>
        <c:when test="${(!empty returnTo || !empty returnurl) and fn:length(breadcrumbs) == 0}">
        
        <div id="breadcrumbs">
            <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
            <c:choose>
                <c:when test="${!empty returnurl}" >
                    &gt; <a href="<c:url value='${returnurl}'/>" title="${uploadDescription} List">${uploadDescription} List</a>
                </c:when>
                <c:otherwise>
                    &gt; <a href="${returnTo}" title="${uploadDescription} List">${uploadDescription} List</a>
                </c:otherwise>
            </c:choose>
            &gt; ${title}
        </div>
        
        </c:when>

        <c:otherwise>
        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${title}" name="pageTitle"/>
        </jsp:include>
        </c:otherwise>
    </c:choose>

    <!-- MIV Main Div -->
    <div id="main">

    <h1>${title}</h1>

    <c:if test="${fn:length(additionalTextList)>0}">
    <div id="listarea">
        <ul class="mivrecords">
            <c:forEach var="additionalText" items="${additionalTextList}">
            <li>
            ${additionalText}
            </li>
            </c:forEach>
        </ul>
    </div>
    </c:if>

    <jsp:include page="binderrors.jsp" />

    <div id="enterdataguide">
        <div id="requiredfieldlegend">* = Required Field</div>
    </div>

    <div id="maindialog" class="pagedialog">
        <div class="mivDialog wideDialog">
            <div id="userheading">
                <span class="standout">${displayName}</span>
            </div>
            
            <c:if test="${not empty formhelp}">
                <p class="formhelp">${formhelp}</p>
            </c:if>
            
            <form id="pdfuploadform" commandName="form" method="post" class="mivdata" enctype="multipart/form-data">
            <!-- Only add this additional text for redactable uploads which ar no ad hoc or shadow committee recommendations -->
            
            <c:if test="${isRedactable == 'true' && !fn:startsWith(documentAttributeName,'adhoc_committee') && !fn:startsWith(documentAttributeName,'shadow_committee')}">
                <div class="formline">
                    <br><strong>Confidential Chair's letters should not be uploaded in MIV.</strong>&nbsp;Please submit Confidential Chair's letters via hardcopy outside of MIV.<br>
                </div>
            </c:if>
            
            <c:if test="${isYearField}">
                <div class="formline">
                    <span class="textfield" title="Year">
                        <label for="year" class="f_required">Year:</label>
                          <input type="text" id="year" name="year" value="${form.year}" maxlength="4" size="5" autofocus/>
                    </span>
                </div><!-- formline -->
            </c:if>
            
            <div class="formline">
                <span class="textfield" title="Upload PDF File">
                    <label for="pdfUpload" class="f_required">Upload PDF File:</label><br>
                    <input type="file" id="pdfupload" name="uploadFileName" size="50" maxlength="256" value="${form.uploadFileName}">
                </span>
            </div>

            <div class="formline">
                <span class="textfield" title="Optional custom name">
                    <label for="uploadFileDescription">Create an optional custom name for this file
                        (default will be &quot;${uploadDescription}&quot; if no custom name is entered):</label><br>
                    <input type="text" id="uploadFileDescription" name="uploadFileDescription"
                           value="${form.uploadFileDescription}" size="50" maxlength="50">
                </span>
            </div>

            <!-- If this is a redactable document, show the buttons with no default to force a selection -->
            <c:if test="${isRedactable}">

            <div class="formline">
                <label for="redacted" class="f_required">Letter Type:</label>
                &nbsp;
                <input type="radio" name="letterType" value="redacted" id="redacted"  title="Redacted" ${form.letterType == 'redacted' ? 'checked="checked"' : ''}>
                &nbsp;&nbsp;<a href="<c:url value='/help/redacted_non-redacted_letters.html'/>" title="Redacted" target="mivhelp">Redacted</a>
                &nbsp;
                &nbsp;
                <input type="radio" name="letterType" value="nonredacted" id="nonredacted" title="Non-Redacted" ${form.letterType == 'nonredacted' ? 'checked="checked"' : ''}>
                &nbsp;&nbsp;<a href="<c:url value='/help/redacted_non-redacted_letters.html'/>" title="Non-Redacted" target="mivhelp">Non-Redacted</a>
            </div>

            <!-- Only add this additional text for redactable uploads which ar no ad hoc or shadow committee recommendations -->
            <c:set var="redactedWarningMessage" value='<strong>Candidates view "Redacted" letters. </strong>Reviewers and Administrators view "Non-Redacted" letters.<br>'/>

            <c:if test="${fn:startsWith(documentAttributeName,'adhoc_committee') || fn:startsWith(documentAttributeName,'shadow_committee')}">
                <c:set var="redactedWarningMessage" value='<strong>Candidates, Department Administrators, Department Chairs, School/College Administrators and Deans view only the “Redacted” recommendations.</strong> Assigned reviewers in the Senate Office and/or Vice Provost locations, as well as the Senate Administrators and MIV Administrators, view the “Non-redacted” recommendations.<br>'/>
            </c:if>

            <div class="formline">
                ${redactedWarningMessage}
                <a href="<c:url value='/help/redacted_non-redacted_letters.html'/>" title="Definition of &quot;Redacted&quot; and &quot;Non-Redacted&quot; letters" target="mivhelp">Definition of "Redacted" and "Non-Redacted" documents</a>
            </div>

            </c:if><%-- isRedactable --%>

            <div class="buttonset">
                <input type="submit" name="_eventId_upload" value="Upload PDF File" title="Upload PDF File">
                <input type="submit" name="_eventId_breadcrumb1" value="Cancel" title="Cancel">
            </div><!-- buttonset -->

            <p>
            <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
            <input type="hidden" name="_flowId" value="${flowRequestContext.activeFlow.id}">
            </p>

            </form>
        </div><!-- mivDialog -->
    </div><!-- maindialog -->

    </div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html><%--
vi: se ts=8 sw=4 et sr:
--%>
