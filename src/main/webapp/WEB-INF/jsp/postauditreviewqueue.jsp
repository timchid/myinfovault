<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; <%--Manage Open Actions: Search Results
    --%>${strings.pagetitle}<c:if test="${fn:length(strings.subtitle)>0}">:
    ${strings.subtitle}</c:if>
    </title>
    <script type="text/javascript">
        var mivConfig = new Object();
        mivConfig.isIE = false;
    </script>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">

    <!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
	<script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
	<script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->

    <style type="text/css" media="screen">
    	#main { min-width: 63em; }
        /*
         * Override styles needed due to the mix of different CSS sources!
         */
        .dataTables_info { padding-top: 0; }
        .dataTables_paginate { padding-top: 0; }
        .css_right { float: right; }
        #theme_links span { float: left; padding: 2px 10px; }
    </style>


    <!-- <script type="text/javascript" src="<c:url value='/js/datatable/data_table.js'/>"></script> -->
    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>

    <script type="text/javascript">
        $(document).ready( function() {
        	$('#userlist tr td input:checkbox').each(function( index )
            {
            	if( $(this).is(':checked') )
           		{
            		tr = this.parentNode.parentNode;
            		if ( !$(tr).hasClass('row_active') )
            		{
            			$(tr).addClass('row_active');
            		}
           		}
            });

            // for more details look into /js/datatable/data_table_config.js
            var customizeSettingMap = new Object();
            customizeSettingMap["aoColumns"] = [{ "sSortDataType": "dom-checkbox" },null,null,null,null,null,null,null,null];
            customizeSettingMap["aaSorting"] = [[ 7, 'desc' ]];
            customizeSettingMap["aFilterColumns"] = [2,3,4,5,6,7,8];
            customizeSettingMap["aoColumnDefs"] = [{ "bSortable": false, "aTargets": [ 1 ] }];
            oTable = fnInitDataTable('userlist', customizeSettingMap);

            // Add all checked checkboxes from the hidden pages to the page for submit
            $('#selectDossiers').submit ( function() {
                $(oTable.fnGetHiddenTrNodes()).find('input:checked').hide().appendTo(this);
            });

            /* Add a checkbox click handler */
            $('#userlist tr td').on('click', 'input:checkbox', function () {
                tr = this.parentNode.parentNode;
                $(tr).toggleClass('row_active')
            } );
        } );
    </script>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${strings.description}" name="pageTitle"/>
    </jsp:include>

    <div id="main">

      <h1>${strings.description}</h1>

      <form:form name="selectDossiers" id="selectDossiers" cssClass="DELETEmivdataME" method="post" commandName="PostAuditDossiersForm"><%-- shouldn't need the "mivdata" class, and it causes problems with the checkboxes --%>

    <c:if test="${empty results}">
    <p>There are no available dossiers to send Post Audit at this time.</p>
    </c:if>

    <c:if test="${!empty results}">
    <p>${strings.message}</p>
    <c:if test="${!empty strings.message1}">
    <p>${strings.message1}</p>
    </c:if>
    <p>Select a column header to sort by that column.</p>

    <div class="searchresults">Search Results = ${fn:length(results)}</div>

    <div class="buttonset">
        <input type="submit" title="Send Dossier to Post Audit" name="_eventId_confirmPostAuditReview" value="Send Dossier to Post Audit">
        <input type="submit" title="Cancel" name="_eventId_cancel" value="Cancel">
        <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
    </div>

    <div class="full_width">
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="userlist" width="100%">
        <thead>
            <tr>
              <%-- TODO: These hrefs need to actually point to something that will do sorting if no Javascript is available --%>
              <th class="datatable-column-filter" title="Post Audit">Post Audit</th>
              <th class="datatable-column-filter" >&nbsp;</th>
              <th class="datatable-column-filter" title="MIV User">MIV User</th>
              <th class="datatable-column-filter" title="School/College">School/College</th>
              <th class="datatable-column-filter" title="Department">Department</th>
              <th class="datatable-column-filter" title="Delegation Authority">Delegation Authority</th>
              <th class="datatable-column-filter" title="Action">Action</th>
              <th class="datatable-column-filter" title="Date Submitted to Dept.">Date Submitted to Dept.</th>
              <th class="datatable-column-filter" title="Completed Date">Completed Date</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="dossier" items="${results}" varStatus="row">
            <tr class="${(row.count)%2==0?'even':'odd'} ${fn:contains(PostAuditDossiersForm.toPostAuditReview, dossier.dossierId) ? 'row_active':''}">
              <td>
                <input type="checkbox" name="toPostAuditReview" id="cb${dossier.dossierId}"
                       value="${dossier.dossierId}"${fn:contains(PostAuditDossiersForm.toPostAuditReview, dossier.dossierId) ? ' checked="checked"':''}
                       title="select the dossier to Post Audit.">&nbsp;&nbsp;&nbsp;&nbsp;
              </td>
              <td>
              	<c:url var='nextLink' value='PostAuditDossiers'>
	                <c:param name='_flowExecutionKey' value='${flowExecutionKey}'/>
	                <c:param name='_eventId' value='returnDossier'/>
	                <c:param name='dossierId' value='${dossier.dossierId}'/>
                </c:url>
                <a href="<c:out value='${nextLink}' escapeXml='true'/>" class="linktobutton"
                   title="Return ${dossier.sortName} Dossier.">Return&nbsp;Dossier</a>
              </td>
              <td>
                <a href="<c:out value='${dossier.dossierUrl}' escapeXml='true'/>"
                   title="View ${dossier.sortName} dossier.">${dossier.sortName}</a>
              </td>
              <td>${dossier.schoolName}</td>
              <td>${!empty dossier.departmentName ? dossier.departmentName : '&nbsp;'}</td>
              <td>${dossier.delegationAuthority}</td>
              <td>${dossier.actionType}</td>
              <td>${dossier.submitDate}</td>
              <td>${dossier.completedDate}</td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
    </div>
    <div class="buttonset">
        <input type="submit" title="Send Dossier to Post Audit" name="_eventId_confirmPostAuditReview" value="Send Dossier to Post Audit">
        <input type="submit" title="Cancel" name="_eventId_cancel" value="Cancel">
        <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
    </div>
    </c:if>
    </form:form>

    </div><!--main-->
    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
