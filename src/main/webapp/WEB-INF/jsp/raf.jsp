<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><spring:message var="pageTitle" text="${strings.pageTitle}" arguments="${form.actionType.description},${form.delegationAuthority.description}"/><%--
--%><c:set var="isReadOnly" value="${form.viceProvost}" /><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${pageTitle}</title>

        <script type="text/javascript">
        /* <![CDATA][ */
            //debugger;
            //alert("editform.jsp setting djConfig");
            djConfig = {
                isDebug: false,
                parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
            };
            var mivConfig = new Object();
            mivConfig.isIE = false;
        /* ]]> */
        </script>

        <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojo/resources/dojo.css'/>">
        <%-- <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dijit/themes/tundra/tundra.css'/>" --%>
        <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/FloatingPane.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/ResizeHandle.css'/>">

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/raf.css'/>">

        <!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->

        <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/dojo/dojox/layout/FloatingPane.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <jwr:script src="/bundles/mivvalidation.js" />
        <script type="text/javascript" src="<c:url value='/js/rafform.js'/>"></script>

        <style type="text/css">
            .disabled {
                color:grey;
            }
        </style>
    </head>

    <body>
        <jsp:scriptlet><![CDATA[
            Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
            pageContext.setAttribute("currentYear", d, PageContext.REQUEST_SCOPE);
        ]]></jsp:scriptlet>

        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${pageTitle}" name="pageTitle"/>
        </jsp:include>

        <!-- MIV Main Div -->
        <div id="main">
            <h1>${pageTitle}</h1>

            <jsp:include page="binderrors.jsp" />

            <div id="enterdataguide">
                <div id="requiredfieldlegend">* = Required Field</div>
            </div>

            <div id="maindialog" class="pagedialog">

                <%-- Do not allow edit of any fields except Retroactive Date when the RAF is at the ViceProvost or FederationViceProvost location --%>
                <c:set var="disabled" value=""/>
                <c:set var="dateClass" value='class="datepicker"'/>
                <c:if test="${isReadOnly}">
                    <c:set var="disabled" value='disabled class="disabled"'/>
                    <c:set var="dateClass" value='disabled class="disabled"'/>
                </c:if>

                <form:form  id="rafForm" cssClass="mivdata" method="post" commandName="form">
                    <div class="mivDialog wideDialog">
                        <span class="standout">${form.raf.dossier.candidate.displayName}</span>

                        <p class="formhelp">
                        <c:if test="${form.delegationAuthority == 'REDELEGATED'
                                   && form.raf.dossier.dossierLocation == 'SCHOOL'}"><%--
                    --%>${strings.deanResignExplanation}
                        </c:if>
                        </p><%--

                        <p><strong>Recommended Action:</strong>&nbsp;${form.actionType.description}</p>

                    --%><div class="formline">
                                <spring:bind path="effectiveDate">
                                    <label for="${status.expression}"><strong>${labels.effectiveDate}:</strong></label>

                                    <c:set var="effectiveDate" value="${form.effectiveDateAsDate}"/>
                                    <c:if test="${empty status.errorCodes}">
                                        <fmt:formatDate type="date" dateStyle="long" value="${effectiveDate}" var="effectiveDate" />
                                    </c:if>

                                    <input type="text" ${dateClass}
                                        id="${status.expression}" name="${status.expression}"
                                        value="${effectiveDate}" size="15"
                                        title="${tooltips.effectiveDate}"
                                        required="required" trim="true"
                                        promptMessage="Effective Date is required">
                                </spring:bind>
                        </div><!-- formline -->

                        <%-- Only show Retroactive Date prompt when the raf is at the ViceProvost, PostSenateViceProvost or PostSenateSchool location --%>
                        <c:if test="${isReadOnly}">
                        <div class="formline">
                            <spring:bind path="retroactiveDate">
                                <label for="${status.expression}"><strong>${labels.retroactiveDate}:</strong></label>
                                <c:set var="retroactiveDate" value="${form.retroactiveDateAsDate}"/>
                                    <c:if test="${empty status.errorCodes}">
                                        <fmt:formatDate type="date" dateStyle="long" value="${retroactiveDate}" var="retroactiveDate" />
                                    </c:if>
                                    <input type="text" class="datepicker"
                                       id="${status.expression}" name="${status.expression}"
                                       value="${retroactiveDate}" size="15"
                                       title="${tooltips.retroactiveDate}" trim="true">
                            </spring:bind>
                        </div>
                        </c:if>

                        <div class="formline">
                            <spring:bind path="endDate">
                                <label for="${status.expression}"><strong>${labels.endDate}:</strong></label>
                                <c:set var="endDate" value="${form.endDateAsDate}"/>
                                <c:if test="${empty status.errorCodes}">
                                    <fmt:formatDate type="date" dateStyle="long" value="${endDate}" var="endDate" />
                                </c:if>
                                <input type="text" ${dateClass}
                                       id="${status.expression}" name="${status.expression}"
                                       value="${endDate}" size="15"
                                       title="${tooltips.endDate}" trim="true">
                            </spring:bind>
                        </div>

                        <div class="formline">
                            <fieldset class="mivfieldset">
                                <legend><span class="f_required">Appointment Type:</span></legend>

                                <form:checkbox path="raf.appointmentType9mo"
                                               id="appointmentType9mo"
                                               label="9 Months"
                                               title="9 Months"
                                               disabled="${isReadOnly}"/>

                                <form:checkbox path="raf.appointmentType11mo"
                                               id="appointmentType11mo"
                                               label="11 Months"
                                               title="11 Months"
                                               disabled="${isReadOnly}"/>
                            </fieldset>
                        </div><!-- formline -->

                        <div class="formline">
                            <fieldset class="mivfieldset">
                                <legend><span class="f_required">Acceleration:</span></legend>

                                <form:radiobutton id="accelerationNormal"
                                                  path="raf.acceleration"
                                                  value="NORMAL"
                                                  label="Normal"
                                                  title="Normal"
                                                  disabled="${isReadOnly}"/>

                                <form:radiobutton id="accelerationAccel"
                                                  path="raf.acceleration"
                                                  value="ACCELERATION"
                                                  label="Accel."
                                                  title="Acceleration"
                                                  disabled="${isReadOnly}"/>

                                <form:label path="raf.accelerationYears">(Years):</form:label>
                                <form:input id="accelerationYears"
                                            path="raf.accelerationYears"
                                            title="Acceleration Years"
                                            size="3" maxlength="2"/>

                                <form:radiobutton id="accelerationDecel"
                                                  path="raf.acceleration"
                                                  value="DECELERATION"
                                                  label="Decel."
                                                  title="Deceleration"
                                                  disabled="${isReadOnly}"/>

                                <form:label path="raf.decelerationYears">(Years):</form:label>
                                <form:input id="decelerationYears"
                                            path="raf.decelerationYears"
                                            title="Deceleration Years"
                                            size="3" maxlength="2"/>
                            </fieldset>
                        </div><!-- formline -->

                        <div class="formline">
                            <spring:bind path="form.raf.yearsAtRank">
                            <label for="yearsAtRank" class="f_required">Years at Rank:</label>
                            <input type="text" id="yearsAtRank" name="${status.expression}"
                                   ${disabled} value="${status.value}" size="3" maxlength="2" title="Years at Rank" autofocus><%--
                        --%></spring:bind>

                            <spring:bind path="form.raf.yearsAtStep">
                            <label for="yearsAtStep" class="f_required">Years at Step:</label>
                            <input type="text" id="yearsAtStep" name="${status.expression}"
                                   ${disabled} value="${status.value}" size="3" maxlength="2" title="Years at Step"><%--
                        --%></spring:bind>
                        </div><!-- formline -->

                        <c:set var="showDepartmentLabelRowText" value="${fn:length(form.raf.departments)>1}"/>

                        <div id="departmentBlock" class="formline">
                        <c:forEach var="department" items="${form.raf.primaryAndJointDepartments}" varStatus="deptRow">
                            <spring:bind path="form.raf.primaryAndJointDepartments[${deptRow.index}].percentOfTime">
                                <label errorlabel="Department ${showDepartmentLabelRowText ? ', Row ' : ''}${showDepartmentLabelRowText ? deptRow.count : ''} – % of Time" for="departmentPercentOfTime${deptRow.count}"  class="f_required">% of Time:</label>
                                <input type="text" id="departmentPercentOfTime${deptRow.count}" name="${status.expression}"
                                       ${disabled} value="${status.value}" size="3" maxlength="3" title="Percent of Time"><%--
                        --%></spring:bind>&nbsp;
                            <strong>${deptRow.first ? '' : 'Joint '}Department:</strong> ${department.schoolName}
                            <c:if test="${not empty department.departmentName}">
                            &ndash; ${department.departmentName}
                            </c:if><br><%--
                    --%></c:forEach>
                        <p class="formhelp">
                            <em>Note: If the above department information is incorrect,
                            send the dossier back to the Candidate,
                            update his/her account and send the dossier to the Department.</em>
                        </p>
                        </div><!-- formline -->

                        <c:forEach var="rafStatus" items="${form.raf.statuses}" varStatus="statRow">
                            <c:set var='statusType' scope='page' value="${fn:replace(rafStatus.statusType.description,' Status','')}"/>
                            <div class="formline">
                                <span class="f_required">
                                        <strong>${rafStatus.statusType.description}</strong>
                                </span>
                                <table class="rafStatusTable">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Rank, Title &amp; Step</th>
                                            <th>% of<br>Time</th>
                                            <th>Title<br>Code</th>
                                            <th>Monthly Salary</th>
                                            <th>Annual Salary</th>
                                        </tr>
                                    </thead>

                                    <tbody><%--
                                --%><spring:bind path="form.raf.statuses[${statRow.index}].ranks"><%--
                                --%><c:forEach items="${status.value}" varStatus="rankRow">
                                        <tr>
                                            <td>
                                                <label class="sequence">${rankRow.count}.</label>
                                            </td>
                                            <td>
                                                <label class="hidden" errorlabel="${rafStatus.statusType.description} Status, Row ${rankRow.count}" for="${statusType}${rankRow.count}RankAndStep">${rafStatus.statusType.description} Status, Row ${rankRow.count} - Rank &amp; Step:</label><%--
                                            --%><spring:bind path="form.raf.statuses[${statRow.index}].ranks[${rankRow.index}].rankAndStep">
                                                <input type="text" id="${statusType}${rankRow.count}RankAndStep" name="${status.expression}"
                                                    ${disabled} value="${status.value}" size="50" maxlength="100" title="Rank and Step"><%--
                                            --%></spring:bind>
                                            </td>

                                            <td>
                                                <label class="hidden" for="${statusType}${rankRow.count}PercentOfTime">${rafStatus.statusType.description} Status, Row ${rankRow.count} - % of Time:</label><%--
                                            --%><spring:bind path="form.raf.statuses[${statRow.index}].ranks[${rankRow.index}].percentOfTime">
                                                <input type="text" id="${statusType}${rankRow.count}PercentOfTime" name="${status.expression}"
                                                    ${disabled} value="${status.value}" size="3" maxlength="3" title="Percent of Time must be numeric or three characters: WOS"><%--
                                            --%></spring:bind>
                                            </td>

                                            <td>
                                                <label  class="hidden" for="${statusType}${rankRow.count}TitleCode">${rafStatus.statusType.description} Status, Row ${rankRow.count} - Title Code:</label><%--
                                            --%><spring:bind path="form.raf.statuses[${statRow.index}].ranks[${rankRow.index}].titleCode">
                                                <input type="text" id="${statusType}${rankRow.count}TitleCode" name="${status.expression}"
                                                    ${disabled} value="${status.value}" size="5" maxlength="4" title="Title Code must be numeric"><%--
                                        -   --%></spring:bind>
                                            </td>

                                            <td>
                                                <label  class="hidden" for="${statusType}${rankRow.count}MonthlySalary">${rafStatus.statusType.description} Status, Row ${rankRow.count} - Monthly Salary:</label><%--
                                            --%><spring:bind path="form.raf.statuses[${statRow.index}].ranks[${rankRow.index}].monthlySalary">
                                                <input type="text" id="${statusType}${rankRow.count}MonthlySalary" name="${status.expression}"
                                                    ${disabled} value="${status.value == 0.0 ? "" : status.value}" size="12" maxlength="10"
                                                    title="Monthly Salary must be numeric from 0 to 9,999,999.99"><%--
                                            --%></spring:bind>
                                            </td>

                                            <td>
                                                <label  class="hidden" for="${statusType}${rankRow.count}AnnualSalary">${rafStatus.statusType.description} Status, Row ${rankRow.count} - Annual Salary:</label><%--
                                            --%><spring:bind path="form.raf.statuses[${statRow.index}].ranks[${rankRow.index}].annualSalary">
                                                <input type="text" id="${statusType}${rankRow.count}AnnualSalary" name="${status.expression}"
                                                    ${disabled} value="${status.value == 0.0 ? "" : status.value}" size="12" maxlength="10"
                                                    title="Annual Salary must be numeric from 0 to 9,999,999.99"><%--
                                            --%></spring:bind>
                                            </td>
                                        </tr>
                                    </c:forEach><%--
                                --%></spring:bind>
                                    </tbody>
                                </table>
                            </div>
                        </c:forEach>

                        <div class="buttonset">
                            <input type="submit" id="rafSubmit" title="Save" name="_eventId_save" value="Save">
                            <input type="submit" id="cancel" title="Cancel" name="_eventId_cancel" value="Cancel">
                        </div><!-- buttonset -->
                    </div><!-- wideDialog -->
                </form:form>
            </div><!-- maindialog -->
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>

</html>
