<%--
  JSP fragment to display an iterable bean as an unordered list.
  For use in report.jsp. Configured in mivconfig.properties.

  Request Scope | Description
  --------------+-----------------------
  fieldValue    | iterable bean

--%><%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <ul><%--
    --%><c:forEach var="item" items="${fieldValue}">
        <li>${item}</li><%--
    --%></c:forEach>
    </ul>
