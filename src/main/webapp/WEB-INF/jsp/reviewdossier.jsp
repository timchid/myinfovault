<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <c:set var="pagetitle" value="Review Dossier"/>
    <title>MIV &ndash; ${pagetitle}</title>
    <script type="text/javascript">
        var mivConfig = new Object();
        mivConfig.isIE = false;
    </script>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        <c:url var='nextLink' value='/ReviewDossiers'><%--
     --%><c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/><%--
     --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/><%--
     --%><c:param name='_eventId' value='root'/><%--
     --%></c:url>
         &gt; <a href="<c:out value='${nextLink}' escapeXml='true'/>" title="Review Other Candidate's Dossier List">Review Other Candidate's Dossier List</a>
         &gt; ${pagetitle}
    </div><!-- breadcrumbs -->

    <div id="main">

      <h1>${pagetitle}</h1>

      <p>
         Dossier documents for <strong>${userName}</strong> are listed below.<br>
      </p>
      <h2><a title="View Dossier as One PDF File" href="${combinedDossierDocument}">View Dossier as One PDF File</a></h2>
      <h2>View Dossier as Individual PDF files</h2>
      <ul class="itemlist">
      <c:forEach var="pdfUrl" items="${dossierFileUrlMap}">
        <li><a title="PDF: ${pdfUrl.value}" href="${pdfUrl.key}">PDF</a> ${pdfUrl.value}</li>
      </c:forEach>
      </ul>
      
      <p>
        If you have any problems viewing this dossier, please contact the MIV Project Team at
        <a href="mailto:miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>.
      </p>
    </div><!--main-->
    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
