<%--
    DESCRIPTION:
        Forms fields for posting/binding to the SearchCriteria form.

--%><%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="searchRole" value="${user_role!=null ? user_role.shortDescription : 'user'}"/>

<fieldset id="searchFields">
    <p class="placeholder">${strings.instruction}</p>

    <div class="formline">
        <form:radiobuttons items="${displayCriteria.checkboxes}"
                           title="${strings.searchRequestTitle}"
                           path="searchRequests"
                           itemValue="selectKey"
                           itemLabel="selectText"
                           element="div" />
    </div>

    <div class="buttonset">
        <input type="submit"
               name="_eventId_${displayCriteria.rptType}"
               id="${displayCriteria.rptSearch}"
               value="${strings.searchButtonLabel}"
               title="${strings.searchButtonTitle}"/>
    </div>
</fieldset>
