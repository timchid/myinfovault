<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
  <title>MIV &ndash; ${strings.pageTitle}</title>

  <%@ include file="/jsp/metatags.html" %>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
        <h1>${strings.pageTitle}</h1>

        <spring:hasBindErrors name="form">
            <div id="errormessage" class="errorbox haserrors">
                <spring:message htmlEscape="false" text="${errors.allErrors[0].defaultMessage}"/>
            </div>
        </spring:hasBindErrors>
        
        <c:choose>
            <c:when test="${!empty signatures}">
                <p>${strings.explanation}</p>

                <c:forEach var="signature" items="${signatures}">
                    <div class="formline">
                        <a href="<c:url value='${context.request.contextPath}'>
                                 <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                                 <c:param name='_eventId' value='choose'/>
                                 <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                                 <c:param name="id" value="${signature.recordId}"/>
                                 </c:url>" title="${strings.reviewLink}">${strings.reviewLink}</a>
                        for ${signature.document.schoolName}
                            ${!empty signature.document.departmentName ? '&ndash;' : ''}
                            ${signature.document.departmentName}
                    </div>
                </c:forEach>
            </c:when>
            
            <c:otherwise><p>${strings.noRequests}</p></c:otherwise>
        </c:choose>
    </div>

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
vi: se ts=8 sw=2 et:
--%>
