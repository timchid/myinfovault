<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; Javascript Unit Tests
    </title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/qunit.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">

    <script type="text/javascript" src="<c:url value='/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/test/jquery.mockjax.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/test/qunit.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/test/blanket.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/miv.dom.js'/>" data-cover></script>
    <script type="text/javascript" src="<c:url value='/js/miv.errors.js'/>" data-cover></script>
    <script type="text/javascript" src="<c:url value='/js/miv.validate.js'/>" data-cover></script>
    <script type="text/javascript" src="<c:url value='/js/miv.api.js'/>" data-cover></script>
    <script type="text/javascript" src="<c:url value='/js/test/test_miv.dom.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/test/test_miv.errors.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/test/test_miv.validate.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/test/test_miv.api.js' />"></script>
</head>

<body>
    <!-- MIV Header Div -->
    <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; Javascript Unit Tests
    </div><!-- breadcrumbs -->

    <!-- MIV Main Div -->
    <div id="main">

        <div id="qunit"></div>
    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
