<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><%--
--%><c:set var="successcount" scope="page" value="${upload.importResult.persisted}" /><%--
--%><c:set var="failedcount" scope="page" value="${fn:length(upload.importResult.errors)}" /><%--
--%><c:set var="processedcount" scope="page" value="${upload.importResult.processed}" /><%--
--%><c:set var="duplicatecount" scope="page" value="${upload.importResult.duplicates}" />
<html>
<head><c:set var="title" scope="page" value="${strings.title}"/>
  <title>MIV &ndash; ${title}</title>

  <script type="text/javascript">
  /* <![CDATA[ */
  var mivConfig = new Object();
  mivConfig.isIE = false;
  /* ]]> */
  </script>
  <%@ include file="/jsp/metatags.html" %>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
  <script type="text/javascript">mivConfig.isIE=true;</script>
  <![endif]-->
<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
<div id="breadcrumbs">
  <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
  &gt; <a href="<c:url value='/Imports'>
				  <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
				  <c:param name='_flowExecutionKey' value='${flowExecutionKey}'/>
				  <c:param name='_eventId' value='select'/>
				  </c:url>" title="${strings.title}">${strings.title}</a>
  &gt; ${title}
</div><!-- breadcrumbs -->

<!-- MIV Main Div -->
<div id="main">
	<h1>${title}</h1>
	
	<p>
	    <c:if test="${successcount==processedcount}">Y</c:if><%--
	--%><c:if test="${successcount<=0}">None of y</c:if><%--
	--%><c:if test="${successcount>0 && successcount<processedcount}">${successcount} of ${processedcount} ${processedcount!=1 ? 'records' : 'record'} from y</c:if><%--
	--%>our publications data ${successcount!=1 ? 'have' : 'has'} been imported.
	</p>
	
	<p>
	    <c:if test="${successcount>0}"><div>
	        <strong>${successcount} ${successcount!=1 ? 'records have been added and are' : 'record has been added and is'} now available in your publications data.</strong>
	    </div></c:if>
	    <c:if test="${duplicatecount>0}"><div>
            <strong class="warning">${duplicatecount} duplicate ${duplicatecount!=1 ? 'records' : 'record'} ${duplicatecount!=1 ? 'were' : 'was'} detected and skipped.</strong>
        </div></c:if>
	    <c:if test="${failedcount>0}"><div>
	        <strong class="warning">${failedcount} ${failedcount!=1 ? 'records' : 'record'} ${failedcount!=1 ? 'were' : 'was'} skipped due to errors:</strong>
	        <ul class="warning">
	           <c:forEach items="${upload.importResult.errors}" var="error">
	               <li>${error}</li>
	           </c:forEach>
	        </ul>
	    </div></c:if>
	</p>
	
	<p>
	   If you have any questions or problems with importing data, please contact the MIV Project Team at <a href="mailto:miv-help@ucdavis.edu?subject=Importing MIV Data" title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>
	</p>
</div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vi: se ts=8 sw=4 et:
--%>
