<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>MIV &ndash; ${strings.title}</title>
  <script type="text/javascript">
  /* <![CDATA[ */
  var mivConfig = new Object();
  mivConfig.isIE = false;
  /* ]]> */
  </script>
  <%@ include file="/jsp/metatags.html" %>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
  <script type="text/javascript">mivConfig.isIE=true;</script>
  <![endif]-->
<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<jsp:include page="breadcrumbs.jsp">
    <jsp:param value="${strings.title}" name="pageTitle"/>
</jsp:include>

<!-- MIV Main Div -->
<div id="main">

<h1>${strings.title}</h1>

<p>
Publication records can be imported from an XML file created by the
<a href="<c:url value='/help/import_data.html'/>" target="mivhelp" title="EndNote">EndNote</a> program
directly into MIV.
If you do not want to import your publication records, you can manually
<a href="<c:url value='/help/enter_data.html'/>" target="mivhelp"
   title="Entering MIV Data">enter data</a> instead.
</p>

<p>
<strong>MIV supports publication data imports only from
<a href="<c:url value='/help/import_data.html'/>" target="mivhelp" title="EndNote">EndNote</a>
XML files.</strong>
</p>

<p>
If you are a new user that has a list of publications you would like to
import into MIV, follow the
<a href="<c:url value='/help/onetime_data_imports.html'/>" target="mivhelp"
   title="Instructions for a one-time file data import"
   >instructions for a one-time file data import</a>.
</p>

<spring:hasBindErrors name="upload">
    <c:set var='errorClass' scope='page' value=' class="haserrors"'/>
</spring:hasBindErrors>

<div id="errormessage"${errorClass}>
<spring:hasBindErrors name="upload">
<c:if test="${errors.errorCount > 0}"><%--
--%>
<strong>${errors.errorCount == 1? "Error:":"Errors:"}</strong>
    <ul>
        <c:forEach var="errMsgObj" items="${errors.allErrors}"><%--
    --%><li><spring:message htmlEscape="false" text="${errMsgObj.defaultMessage}"/></li><%--
    --%></c:forEach>
    </ul><%--
--%></c:if>
</spring:hasBindErrors>
</div>

<div id="maindialog" class="pagedialog">
    <div class="dojoDialog mivDialog">
        <p>
            <a class="standout" href="<c:url value='/help/import_data.html'/>"
               target="mivhelp" title="Instructions for creating an EndNote XML file"
               >Instructions for creating an EndNote XML file</a>
        </p>
        <form:form  id="uploadform" cssClass="mivdata" enctype="multipart/form-data" method="post" commandName="upload">
            <div class="formline">
                <spring:bind path="upload.uploadFile">
                <label for="${status.expression}">Import EndNote XML file
                (that contains your publication records):</label>
                <input type="file" id="${status.expression}" name="${status.expression}" value="${status.value}" size="50">
                </spring:bind>
            </div>
            <miv:permit roles="SYS_ADMIN" usertype="REAL">
            <div class="formline">
            <spring:bind path="upload.source">
	            <fieldset class="mivfieldset">
	                <legend><strong>Upload Type</strong></legend>
	                <c:forEach var="type" items="PUBMED,ENDNOTE,CSV">
	                <input type="radio" id="${status.expression}${type}" name="${status.expression}" value="${type}" <c:if test="${status.value == type}"> checked="checked"</c:if>>
	                <label for="${status.expression}">${type}</label>&nbsp;
	                </c:forEach>
	            </fieldset>
            </spring:bind>
            </div>
            </miv:permit>
            <div class="buttonset">
                <input type="submit" id="sendxml" name="_eventId_import" value="Import Now" title="Import Now">
            </div>
        </form:form>
    </div><!-- mivDialog -->
</div><!-- maindialog -->

<p>
If you have any questions or problems with importing data, please contact the MIV Project Team
at <a href="mailto:miv-help@ucdavis.edu?subject=Importing MIV Data"
     title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>
</p>

</div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html><%--
 vi: se ts=8 sw=4 et:
--%>
