<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:choose><%--
--%><c:when test="${not empty dossierInProcess}"><%--
--%><c:set var="pageTitle" value="${strings.errorTitle}"/><%--
--%></c:when><%--
--%><c:when test="${flowRequestContext.activeFlow.id == 'manageUsers-account-flow'}"><%--
--%><c:set var="pageTitle" value="${strings.editUserTitle}"/><%--
--%></c:when><%--
--%><c:when test="${flowRequestContext.activeFlow.id == 'manageUsers-add-flow' || flowRequestContext.activeFlow.id == null}"><%--
--%><c:set var="pageTitle" value="${strings.addUserTitle}"/><%--
--%></c:when><%--
--%><c:otherwise><%--
--%><c:set var="pageTitle" value="${strings.editSelfTitle}"/><%--
--%></c:otherwise><%--
--%></c:choose><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${pageTitle}</title>

        <script type="text/javascript">
        /* <![CDATA][ */
            djConfig = {
                isDebug: false,
                parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
            };
            var mivConfig = new Object();
            mivConfig.isIE = false;
        /* ]]> */
        </script>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${pageTitle}" name="pageTitle"/>
        </jsp:include>

        <!-- MIV Main Div -->
        <div id="main">
            <h1>${pageTitle}</h1>
            <c:choose><%--
            --%><c:when test="${not empty dossierInProcess}">
                <p>
                <strong>We're sorry, but you cannot edit this user's account at this time because he/she has an active dossier in progress.</strong>
                </p>
                <p>An active dossier must be either archived or returned to the Candidate if you want to edit his/her account.</p>
                <p>
                <em>NOTE: Editing a user's account after his/her dossier has been
                    returned to the Candidate will delete all PDF uploads and remove
                    all assigned reviewers that were added to that user's dossier.
                    If a dossier has been archived no documents will ever be deleted.</em>
                </p>
                </c:when><%--
            --%><c:otherwise>
                <p>
                The following user information has been updated.
                These changes may take up to two hours to appear updated everywhere in MIV.
                </p>
                </c:otherwise><%--
        --%></c:choose>
            <div class="summary">
                <div class="formline"><strong>Name:</strong> ${form.displayName}</div>
                <div class="formline"><strong>MIV Role:</strong> ${form.selectedRole.description}</div>
                <div class="formline"><strong>Email:</strong> ${form.selectedEmail}</div>
            </div>

            <c:set var="joint" scope="page" value="${form.selectedRole=='CANDIDATE' ? 'Joint ' : 'Additional '}"/>
            <div class="listings">
                <ul><%-- MIV-4027
                When confirming changes from editing a user the primary "home" department has been merged
                into the list of formLines as the first line, so we loop over all lines and just mark the
                first one specially as the "Primary".
                But when editing fails due to a Dossier being In-Process none of that merging has had a
                chance to happen. In that case we need to show the "home" department as primary and
                loop over any _remaining_ rows.  All of those, including the first, are joint/additional
                assignments.
            --%><c:set var="jointStart" value="1"/>
                <c:if test="${not empty dossierInProcess}">
                    <li>
                        <strong>Primary School/College - Department:</strong> ${form.homeDepartment.description}
                        
                        <c:if test="${form.homeDepartment.dean or form.homeDepartment.deptChair}"><%-- show if dean or department chair --%>
                        (${form.homeDepartment.dean ? 'Dean' : ''}${form.homeDepartment.dean and form.homeDepartment.deptChair ? ', ' : ''}${form.homeDepartment.deptChair ? 'Dept. Chair' : ''})
                        </c:if>
                    </li>
                    <c:set var="jointStart" value="0"/>
                </c:if>
                <c:forEach var="department" items="${form.formLines}" varStatus="deptRow">
                    <c:if test="${department.scope != '0:0'}">
                    <li>
                        <strong>${deptRow.count > jointStart ? joint : 'Primary'} School/College - Department:</strong> ${department.description}
                        
                        <c:if test="${department.dean or department.deptChair}"><%-- show if dean or department chair --%>
                        (${department.dean ? 'Dean' : ''}${department.dean and department.deptChair ? ', ' : ''}${department.deptChair ? 'Dept. Chair' : ''})
                        </c:if>
                    </li>
                    </c:if>
                </c:forEach>
                </ul>
            </div>

        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
