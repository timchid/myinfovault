<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><c:set var="pageTitle" value="${selfEdit ? strings.selfPageTitle : strings.pageTitle}"/><%--
--%><c:set var="appointment" scope="page" value="${form.selectedRole == 'CANDIDATE' ? 'Appointment' : 'Assignment'}"/><%--
--%><c:set var="joint" scope="page" value="${form.selectedRole == 'CANDIDATE' ? 'Joint ' : 'Additional '}"/><%--
--%><c:set var="cancelmsg" scope="page" value="${roleEditAllowed ? strings.specialmsgrole : strings.specialmsgnorole}"/><%--
--%><c:set var="exectooltip" scope="page" value="${isExecutive ? strings.executive : '' }"/><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${pageTitle}</title>

        <script type="text/javascript">
        /* <![CDATA][ */
            djConfig = {
                isDebug: false,
                parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
            };
            var mivConfig = new Object();
            mivConfig.isIE = false;
        /* ]]> */
        </script>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/useredit.css'/>">

        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->
        <script type="text/javascript" src="<c:url value='/js/useredit.js'/>"></script>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${pageTitle}" name="pageTitle"/>
        </jsp:include>

        <!-- MIV Main Div -->
        <div id="main">
            <h1>${pageTitle}</h1>

            <spring:hasBindErrors name="form">
                <c:set var='errorClass' scope='page' value=' class="haserrors"'/>
            </spring:hasBindErrors>

            <div id="messagebox"${errorClass}>
                <spring:hasBindErrors name="form">
                    <c:if test="${errors.errorCount > 0}">
                        <div id="errorbox">
                            <strong>${errors.errorCount == 1 ? "Error:" : "Errors:"}</strong>
                            <ul>
                                <c:forEach var="errMsgObj" items="${errors.allErrors}">
                                    <li><spring:message htmlEscape="false" text="${errMsgObj.defaultMessage}"/></li>
                                </c:forEach>
                            </ul>
                        </div>
                   </c:if>
                </spring:hasBindErrors>
            </div>

            <div id="maindialog" class="pagedialog">
                <form:form id="editUserForm" cssClass="mivdata ${form.originalRole}" method="post" modelAttribute="form">
                    <div class="mivDialog wideDialog">
                        <div class="standout">${canEdit ? 'Editing: ' : ''}${form.displayName}</div>

                        <div class="formline">
                            <c:choose>
                                <c:when test="${roleEditAllowed}">
                                    <form:label path="selectedRole">MIV Role:</form:label>
                                    <form:select path="selectedRole" items="${form.availableRoles}" itemValue="name" itemLabel="description" multiple="false"/>
                                </c:when>

                                <c:otherwise>
                                    <div id="displayrole" title="${exectooltip}">
                                    MIV Role: <strong>${form.originalRole.description}</strong>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                            <c:if test="${packetRequested}">
                                  <div class="specialmsg">
                                      ${cancelmsg}
                                  </div>
                            </c:if>
                        </div>

                        <div class="formline">
                            <c:choose>
                                <%-- editing email allowed if more than one available and can either edit users or is editing self --%>
                                <c:when test="${fn:length(form.availableEmails) > 1 && (canEdit || selfEdit)}">
                                    <fieldset class="mivfieldset">
                                        <legend>Email:</legend>

                                        <form:radiobuttons items="${form.availableEmails}" path="selectedEmail" element="div"/>
                                    </fieldset>
                                </c:when>

                                <c:otherwise>
                                    Email: <strong>${form.originalEmail}</strong>
                                </c:otherwise>
                            </c:choose>
                        </div>

                        <h2>Primary ${appointment}</h2>

                        <div class="formline">
                            <c:choose>
                                <c:when test="${!canEdit}">
                                    School/College &ndash; Department: <strong>${form.homeDepartment.description}</strong>

                                    <c:if test="${form.homeDepartment.dean or form.homeDepartment.deptChair}"><%-- show if dean or department chair --%>
                                    (${form.homeDepartment.dean ? 'Dean' : ''}${form.homeDepartment.dean and form.homeDepartment.deptChair ? ', ' : ''}${form.homeDepartment.deptChair ? 'Dept. Chair' : ''})
                                    </c:if>
                                </c:when>

                                <c:when test="${form.homeDepartment.readonly}">
                                    <strong title="This ${appointment} can not be edited because it is not within your assignments">${form.homeDepartment.description}</strong>
                                </c:when>

                                <c:otherwise>
                                    <span class="minBox">
                                        <form:select path="homeDepartment.scope" items="${userscopeddepartments}" itemLabel="description" multiple="false"/>
                                    </span>

                                    <!-- Home Dept is ${form.homeDepartment.description} -->
                                    <div class="appointment">
                                        <div class="buttonset"><%--Need the buttonset, but no delete button on primary appointment --%>
                                        </div>

                                        <span class="options ${form.selectedRole}">
                                            <span class="minBox">
                                                <form:checkbox path="homeDepartment.candidate" disabled="true"/>
                                                <form:label cssClass="heading" path="homeDepartment.candidate">Candidate</form:label>
                                            </span>

                                            <miv:permit action="Assign Dept Chair">
                                                <span class="minBox">
                                                    <form:checkbox path="homeDepartment.deptChair"/>
                                                    <form:label cssClass="heading" path="homeDepartment.deptChair">Dept. Chair</form:label>
                                                </span>
                                            </miv:permit>

                                            <miv:permit action="Assign Dean">
                                                <span class="minBox">
                                                    <form:checkbox path="homeDepartment.dean"/>
                                                    <form:label cssClass="heading" path="homeDepartment.dean">Dean</form:label>
                                                </span>
                                            </miv:permit>
                                        </span>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </div><!-- formline -->

                        <h2>${joint}${appointment}s</h2>

                        <c:forEach var="line" items="${form.formLines}" varStatus="row">
                            <%-- If we are editing a candidate and the candidate checkbox is not checked we may not want to show the line at all. --%>
                            <div class="formline jointline">
                                <c:choose>
                                    <c:when test="${!canEdit}">
                                        ${joint} School/College &ndash; Department: <strong>${line.description}</strong>

                                        <c:if test="${line.dean or line.deptChair}"><%-- show if dean or department chair --%>
                                        (${line.dean ? 'Dean' : ''}${line.dean and line.deptChair ? ', ' : ''}${line.deptChair ? 'Dept. Chair' : ''})
                                        </c:if>
                                    </c:when>

                                    <c:when test="${line.readonly}">
                                            <strong title="This ${appointment} can not be edited because it is not within your assignments">${line.description}</strong>
                                    </c:when>

                                    <c:otherwise>
                                        <form:select cssClass="additionaldept" path="formLines[${row.index}].scope" items="${alldepartments}" itemLabel="description" multiple="false"/>

                                        <div class="appointment">
                                            <div class="buttonset">
                                                <input type="submit"
                                                       class="deleteAppointment"
                                                       id="delete${row.index}" name="_eventId_delete${row.index}"
                                                       value="Delete" title="Delete This ${appointment}">
                                            </div>

                                            <span class="options ${form.selectedRole}">
                                                <form:checkbox path="formLines[${row.index}].candidate"/>
                                                <form:label cssClass="heading" path="formLines[${row.index}].candidate">Candidate</form:label>

                                                <miv:permit action="Assign Dept Chair">
                                                    <form:checkbox path="formLines[${row.index}].deptChair"/>
                                                    <form:label cssClass="heading" path="formLines[${row.index}].deptChair">Dept. Chair</form:label>
                                                </miv:permit>

                                                <miv:permit action="Assign Dean">
                                                    <form:checkbox path="formLines[${row.index}].dean"/>
                                                    <form:label cssClass="heading" path="formLines[${row.index}].dean">Dean</form:label>
                                                </miv:permit>
                                            </span>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </div><!-- formline -->
                        </c:forEach>

                        <c:if test="${canEdit}">
                                <input type="submit" id="additem" name="_eventId_addAppointment"
                                       value="Add ${appointment}" title="Add Another ${appointment}">
                        </c:if>

                        <p>
                            <em>Note: The name above is populated from the UC Davis online directory.
                            If you would like your name to appear differently in your MIV documents (Dossier, CV, etc.),
                            update your "Display Name" in the "Enter&nbsp;Data&nbsp;&gt;&nbsp;
                            <a href="<c:url value='/PersonalForm'/>" title="Personal Information">Personal&nbsp;Information</a>" page.</em>
                        </p>

                        <div class="buttonset">
                            <c:if test="${canEdit || (selfEdit && fn:length(form.availableEmails) > 1)}">
                                <input type="submit" name="_eventId_save" value="Save" title="Save">
                            </c:if>
                            <input type="submit" name="_eventId_breadcrumb1" value="Cancel" title="Cancel">
                        </div><!-- buttonset -->
                    </div><!-- wideDialog -->
<%-- This block holds the replacement drop-down lists for department selections.
     These are used to dynamically replace the dept lists when the role is changed.
     The full list is always used for candidates, who can be assigned to anywhere by anyone.
     The scoped list is based on the person doing the editing, and lists only what they can assign.

     This block was moved to be within the form because the code below the main div was not providing the needed lists (missing description).
--%>
                    <div class="hidden">
                    <form:select id="alldepts" path="jspBind" items="${alldepartments}" itemLabel="description" multiple="false"/>
                    <form:select id="scopeddepts" path="jspBind" items="${scopeddepartments}" itemLabel="description" multiple="false"/>
                    <form:select id="userscopeddepts" path="jspBind" items="${userscopeddepartments}" itemLabel="description" multiple="false"/>
                    </div>
                </form:form>
            </div><!-- maindialog -->
        </div><!-- main -->

<%-- This block holds the replacement drop-down lists for department selections.
     These are used to dynamically replace the dept lists when the role is changed.
     The full list is always used for candidates, who can be assigned to anywhere by anyone.
     The scoped list is based on the person doing the editing, and lists only what they can assign.
MOVED TO THE FORM ABOVE TO GET THE LIST CORRECTLY

        <div class="hidden">
            <select id="alldept">
                <c:forEach var="row" items="${alldepartments}" varStatus="cnt">
                    <option value="${row}">${row.description}</option>
                </c:forEach>
            </select>
            <select id="scopeddept">
                <c:forEach var="department" items="${scopeddepartments}" varStatus="cnt">
                    <option value="${department}">${department.description}</option>
                </c:forEach>
            </select>
       </div>
--%>   <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
