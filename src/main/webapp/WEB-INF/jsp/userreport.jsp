<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; <%--View MIV Users: Search Results
        --%>${strings.description}<c:if test="${fn:length(strings.subtitle)>0}">:
            ${strings.subtitle}</c:if>
        </title>
        <script type="text/javascript">
            /* <![CDATA][ */
            var mivConfig = new Object();
            mivConfig.isIE = false;
            /* ]]> */
        </script>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">

        <!--[if lt IE 8]>
	        <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
	        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->
	<!--[if IE 8]>
	    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
	    <script type="text/javascript">mivConfig.isIE=true;</script>
	<![endif]-->

        <style type="text/css" media="screen">
            /*
             * Override styles needed due to the mix of different CSS sources!
             */
            .dataTables_info { padding-top: 0; }
            .dataTables_paginate { padding-top: 0; }
            /*.css_right { float: right; }*/
            #theme_links span { float: left; padding: 2px 10px; }
        </style>

        <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>

        <script type="text/javascript">
            $(document).ready( function() {
                var filterColumns = "${results.individualColumnFilters}";
                var aFilterColumns = undefined;
                if( filterColumns.toUpperCase() === 'ALL') {
                    aFilterColumns = [];
                }
                else {
                    aFilterColumns = filterColumns.split(",");
                }
                var oTable = fnInitDataTable('userlist', {"bHelpBar" : true,"aFilterColumns": aFilterColumns});
            } );
        </script>

        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>
    <body>
        <!-- Adding report help file link -->
        <c:set var="helpPage" scope="request" value="reports.html"/>
        <jsp:include page="/jsp/mivheader.jsp" />

        <!-- Breadcrumbs Div -->
        <div id="breadcrumbs">
            <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>

            &gt; <a href="<c:url value='${context.request.contextPath}'>
                    <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                    <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                    <c:param name='_eventId' value='search'/>
                    </c:url>" title="${strings.prev}">${strings.prev}</a>

            &gt; ${strings.description}
        </div>
        <!-- breadcrumbs -->

        <div id="main">

            <h1>${strings.description} for ${searchCriteriaText}</h1>

            <c:if test="${!empty selectErrorMessage}">
            <div id="errormessage">
                <strong>ERROR -</strong>
                <ul>
                    <li>${selectErrorMessage}</li>
                </ul>
            </div>
            </c:if>

            <c:if test="${empty results || empty results.resultList}">
            <p>We're sorry, no matching results were found.</p>
            </c:if>


            <c:if test="${!empty results && !empty results.resultList}">
            <p>${strings.message}</p>
            <p>Select a column header to sort by that column.</p>

            <div class="searchresults">Search Results=${fn:length(results.resultList)}</div>

            <div class="full_width">
                <table class="display" id="userlist">
                    <thead>
                        <tr>
                            <c:forEach var="headTitle" items="${results.headerList}" varStatus="headerIndex">
                            <th title="${headTitle}" class="datatable-column-filter">
                            ${!empty headTitle ? headTitle : '&nbsp;'}</th>
                            </c:forEach>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach var="row" items="${results.resultList}" varStatus="rowIndex">
                        <tr class="${(rowIndex.count)%2==0?'even':'odd'}">
                            <c:forEach var="colKey" items="${results.keyList}" varStatus="colIndex">
                            <td>
                                <c:choose>
                                    <c:when test="${!empty row[colKey]}">
                                        <c:set var="cell" scope="page" value="${row[colKey]}"/>
                                        <c:choose>
                                            <c:when test="${cell.linkRequired}">
                                                <a href="${cell.linkUrl}&amp;_flowExecutionKey=${flowExecutionKey}" title="${cell.linkTitle}">
                                                    ${cell.value}
                                                </a>
                                            </c:when>
                                            <c:otherwise>
                                                ${cell.value}
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        &nbsp;
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            </c:forEach>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            </c:if>


        </div><!--main-->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
