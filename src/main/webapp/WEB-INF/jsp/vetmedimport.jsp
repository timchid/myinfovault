<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${strings.pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">

    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/vetmedimport.js'/>"></script>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
        <h1>${strings.pageTitle}</h1>

        <form:form id="upload" method="post" commandName="form" enctype="multipart/form-data">
            <label for="csv">${strings.csvLabel}</label>
            <input id="csv"
                   type="file"
                   name="upload"
                   title="${strings.csvTitle}">

            <input type="submit"
                   name="_eventId_upload"
                   title="${strings.uploadTitle}"
                   value="${strings.uploadLabel}">
        </form:form>

        <c:if test="${not empty form.uploaded}">
            <h2>${strings.uploadedHeader}</h2>

            <ul>
                <c:forEach var="file" items="${form.uploaded}">
                    <li>${file.name}</li>
                </c:forEach>
            </ul>
        </c:if>

        <c:if test="${not empty form.processed}">
            <h2>${strings.processedHeader}</h2>

            <form:form id="persist" method="post" commandName="form">
                <fieldset class="mivfieldset">
                    <table id="processed" class="display">
                        <thead>
                            <tr>
                                <th></th>
                                <th title="${strings.userHeader}">${strings.userHeader}</th>
                                <th title="${strings.termHeader}">${strings.termHeader}</th>
                                <th title="${strings.courseHeader}">${strings.courseHeader}</th>
                                <th title="${strings.responseHeader}">${strings.responseHeader}</th>
                                <th title="${strings.instructorScoreHeader}">${strings.instructorScoreHeader}</th>
                                <th title="${strings.courseScoreHeader}">${strings.courseScoreHeader}</th>
                            </tr>
                        </thead>

                        <tbody>
                            <c:forEach var="evaluation" items="${form.processed}" varStatus="row">
                                <tr>
                                    <td>
                                        <form:checkbox path="processed[${row.index}].selected"
                                                       value="true"
                                                       title="${strings.selectTitle}"/>
                                    </td>

                                    <td>${evaluation.user.displayName}</td>
                                    <td>${evaluation.term.description} ${evaluation.year}</td>
                                    <td>${evaluation.course}, ${evaluation.description}</td>
                                    <td>${evaluation.responseTotal}</td>
                                    <td>${evaluation.instructorScore}</td>
                                    <td>${evaluation.courseScore}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </fieldset>

                <input type="submit"
                       name="_eventId_persist"
                       title="${strings.persistTitle}"
                       value="${strings.persistLabel}">
            </form:form>
        </c:if>

        <c:if test="${not empty form.invalid}">
            <h2>${strings.invalidHeader}</h2>

            <p><a id="invalidLink"
                  download="${strings.invalidFilename}"
                  title="${strings.invalidTitle}">${strings.invalidLabel}</a></p>

            <table id="invalid" class="display">
                <thead>
                    <tr>
                        <th title="${strings.userHeader}">${strings.userHeader}</th>
                        <th title="${strings.termHeader}">${strings.termHeader}</th>
                        <th title="${strings.courseHeader}">${strings.courseHeader}</th>
                        <th title="${strings.descriptionHeader}">${strings.descriptionHeader}</th>
                        <th title="${strings.responseHeader}">${strings.responseHeader}</th>
                        <th title="${strings.instructorScoreHeader}">${strings.instructorScoreHeader}</th>
                        <th title="${strings.courseScoreHeader}">${strings.courseScoreHeader}</th>
                        <th title="${strings.linkHeader}">${strings.linkHeader}</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="evaluation" items="${form.invalid}">
                        <tr>
                            <td>${evaluation.mothraid}</td>
                            <td>${evaluation.term_code}</td>
                            <td>${evaluation.course_name}</td>
                            <td>${evaluation.course_title}</td>
                            <td>${evaluation.response}</td>
                            <td>${evaluation.instructor_score}</td>
                            <td>${evaluation.course_score}</td>
                            <td>${evaluation.url}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>

        <c:if test="${not empty form.persisted}">
            <h2>${strings.persistedHeader}</h2>

            <table id="persisted"  class="display">
                <thead>
                    <tr>
                        <th title="${strings.userHeader}">${strings.userHeader}</th>
                        <th title="${strings.termHeader}">${strings.termHeader}</th>
                        <th title="${strings.courseHeader}">${strings.courseHeader}</th>
                        <th title="${strings.responseHeader}">${strings.responseHeader}</th>
                        <th title="${strings.instructorScoreHeader}">${strings.instructorScoreHeader}</th>
                        <th title="${strings.courseScoreHeader}">${strings.courseScoreHeader}</th>
                    </tr>
                </thead>


                <tbody>
                    <c:forEach var="evaluation" items="${form.persisted}">
                        <tr>
                            <td>${evaluation.user.displayName}</td>
                            <td>${evaluation.term.description} ${evaluation.year}</td>
                            <td>${evaluation.course}, ${evaluation.description}</td>
                            <td>${evaluation.responseTotal}</td>
                            <td>${evaluation.instructorScore}</td>
                            <td>${evaluation.courseScore}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
    </div>

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
