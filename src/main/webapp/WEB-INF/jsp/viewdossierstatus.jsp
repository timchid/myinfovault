<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
--%><c:set var="pageTitle" value="View My Dossier Status"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; View My Dossier Status</title>

    <script type="text/javascript">
        var mivConfig = new Object();
        mivConfig.isIE = false;
    </script>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/dossierAction.css'/>">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
</head>
<body class="tundra">
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${pageTitle}" name="pageTitle"/>
    </jsp:include>

    <!-- Dossier Data -->
    <div id="main">
        <h1>${pageTitle}</h1>

        <spring:hasBindErrors name="viewDossier">
            <c:set var='errorClass' scope='page' value=' class="haserrors"'/>
        </spring:hasBindErrors>

        <div dojotype="dijit.layout.ContentPane" id="errormessage"${errorClass}>
           <spring:hasBindErrors name="viewDossier">
           <c:if test="${errors.errorCount > 0}"><%--
           --%><div id="errorbox">
                    <strong>${errors.errorCount == 1? "Error":"Errors"} - The following ${errors.errorCount > 1 ? 'errors were' : 'error was'} found:</strong>
                    <ul>
                       <c:forEach var="errMsgObj" items="${errors.allErrors}"><%--
                       --%><li><spring:message htmlEscape="false" text="${errMsgObj.defaultMessage}"/></li><%--
                       --%></c:forEach>
                    </ul>
                </div><%--
        --%></c:if>
           </spring:hasBindErrors>
        </div>

        <c:choose>
            <c:when test="${not empty viewStatusError and viewStatusError == 'dossierNotInProcess'}">
                You do not have a dossier in process at this time.
            </c:when>
            <c:when test="${not empty viewStatusError and viewStatusError == 'cantView'}">
                You are not allowed to view this dossier.
            </c:when>
            <c:otherwise>
                <jsp:include page="dossierdetail.jsp" />
            </c:otherwise>
        </c:choose>
    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
