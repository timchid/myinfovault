<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>MIV &ndash; View Kuali Document</title>

  <script type="text/javascript">
  /* <![CDATA[ */
      var mivConfig = new Object();
      mivConfig.isIE = false;
  /* ]]> */
  </script>
  <%@ include file="/jsp/metatags.html" %>
  
  <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>
  <!-- MIV Header Div -->
  <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="View Kuali Document" name="pageTitle"/>
    </jsp:include>

  <!-- MIV Main Div -->
  <div id="main">
        <h1>View Kuali Document</h1>
        <c:choose>
        <c:when test="${not empty archived}">
                <p>This dossier has been archived. Please select <a href="<c:url value='/ViewDossierArchive?_flowId=viewdossierarchive-flow'/>" title="View Dossier Archive">View Dossier Archive</a> to locate this dossier in the archive.</p>
        </c:when>
        <c:when test="${not empty canceled}">
                <p>We're sorry, but this dossier has been canceled and is no longer available in MIV. Please select <a href="<c:url value='/MIVMain'/>" title="Home">Home</a> for the MIV Main Menu</p>
        </c:when>
        <c:otherwise>
                <p>We're sorry, but you do not have permission to access this dossier. Please select <a href="<c:url value='/MIVMain'/>" title="Home">Home</a> for the MIV Main Menu</p>
        </c:otherwise>
        </c:choose>
  </div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html><%--
vi: se ts=8 sw=2 et:
--%>
