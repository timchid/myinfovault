'use strict';
/* global miv */

function Packet(packetId, packetName, userId, packetItems)
{
    this.userId = userId;
    this.packetId = packetId;
    this.packetName = packetName;
    this.packetItems = packetItems;
}

(function()
{
    Packet.prototype.toString = function()
    {
        return JSON.stringify(this);
    };

    Packet.prototype.addPacketItem = function(sectionId, recordId, includedParts) {
        if (this.packetItems === undefined) {
            this.packetItems = [];
        }

        this.packetItems.push({
            sectionId: sectionId,
            recordId: recordId,
            displayParts: includedParts instanceof Array ? includedParts.join(',') : includedParts
        });

        return this;
    };

    Packet.prototype.clearPacketItems = function() {
        this.packetItems = [];

        return this;
    };

    Packet.prototype.save = function(callbacks) {
        return savePacket(this, callbacks);
    };

    function savePacket(packet, callbacks)
    {
        var fSuccess = $.noop();
        if (callbacks && callbacks.success) {
            fSuccess = callbacks.success;
        }

        callbacks.success = function(data, status) {
            updatePacketFromResponse(packet, data);

            fSuccess(data, status);
        };


        miv.api.post('/packets', callbacks, packet);

        return packet;
    }

    function updatePacketFromResponse(packet, serverPacket)
    {
        packet.packetId = serverPacket.packetId;
        packet.userId = serverPacket.userId;
        packet.packetName = serverPacket.packetName;
        packet.packetItems = serverPacket.packetItems ? serverPacket.packetItems : [];
    }
})();
