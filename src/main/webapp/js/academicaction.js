'use strict';
/* global trim, detect, stripCurrency, formatAsMoneyFromString, isNotDepartment */
/* global getClickedButton, validateForm, setErrorLabel, isNumber, getElementById */
/* exported customValidationResult */
/*
 * Academic action creating/editing script for academicaction.jsp.
 */

var _navigator = {
    ua : detect.parse(navigator.userAgent)
};

var scopedSchoolDepartmentSource = 'school/department/scoped';
var nonScopedSchoolDepartmentSource = 'school/department';

var ASSIGNMENT_TEMPLATE;

var MINIMUM = 0;
var MAX_ACCELERATION_YEARS = 20;
var MAX_RANKSTEP_YEARS = 50;
var MAX_QUARTER_COUNT = 24;

var sDefaultPrimaryAssignmentHeader = 'Home Department';
var sDefaultJointAssignmentHeader = 'Joint Department';
var sDefaultTitleHeader = 'New Title';

var mivFormProfile = {
        required: [ 'effectiveDate', 'givenName', 'surname' ],
        trim: ['effectiveDate', 'retroactiveDate', 'givenName', 'middleName', 'surname'],
        constraints: {
            effectiveDate : validateDateField,
            retroactiveDate : function(value) {
                if (value.trim().length === 0) { return true; }
                return validateDateField(value);
            },
            ':not(.REAPPOINTMENT, .APPOINTMENT_INITIAL_CONTINUING) input[id$=yearsAtRank]' : validateYearsAtRankStep,
            '.REAPPOINTMENT input[id$=yearsAtRank], .APPOINTMENT_INITIAL_CONTINUING input[id$=yearsAtRank]'
                : validateQuarterCount,
            'input[id$=yearsAtStep]' : validateYearsAtRankStep,
            '#accelerationYears' : validateAccelerationYears
        },
        errormap: { 'INVALID_PERCENT_TIME_RANGE':'Must be from 0 to 100.',
                    'INVALID_PERIOD_SALARY_RANGE':'Must be numeric from 0 to 99,999.99',
                    'INVALID_ANNUAL_SALARY':'Must be a whole dollar amount.',
                    'INVALID_ANNUAL_SALARY_RANGE':'Must be numeric from 1 to 999,999',
                    'INVALID_ANNUAL_SALARY_RANGE_WOS':'Must be numeric from 0 to 999,999',
                    'INVALID_PERCENT_TIME_DISTRIBUTION':'Must be equal to sum of Department\'s - % of Time.',
                   /* "INVALID_ASSIGNMENT_PERCENT_TIME_DISTRIBUTION":"Must be 100% or less.", */
                    'INVALID_ASSIGNMENT_PERCENT_TIME_DISTRIBUTION':'The total percentage of time in all departments cannot exceed 100%',
                    'DUPLICATE_DEPARTMENTS': 'Must be unique.',
                    'INVALID_ACCELERATION_YEARS':'Must be numeric from ' + MINIMUM + ' to ' + MAX_ACCELERATION_YEARS,
                    'INVALID_RANKSTEP_YEARS':'Must be numeric from ' + MINIMUM + ' to ' + MAX_RANKSTEP_YEARS,
                    'INVALID_QUARTER_COUNT':'Must be numeric from ' + MINIMUM + ' to ' + MAX_QUARTER_COUNT }
};

/*
 * Renders an appointment.
 *
 * @param {map} item suggestion map
 */
var fnRenderAppointment = function(item) {
    /*
     * School
     */
    var appointment = $('<span>').append(item.schoolabbrev && item.description ?
                                         $('<abbr>').prop('title', item.school).text(item.schoolabbrev) :
                                         item.school);

    /*
     * Add person department if available
     */
    if (item.description) {
        appointment.append(' - ')
                   .append(item.abbreviation ?
                           $('<abbr>').prop('title', item.description).text(item.abbreviation) :
                           item.description);
    }

    return appointment;
};

/*
 * Renders a department suggestion.
 *
 * @param {dom} ul suggestion list
 * @param {map} item suggestion map
 */
var fnRenderDepartment = function(ul, item) {
    var anchor = $('<a>').append(fnRenderAppointment(item));

    return $('<li>').append(anchor).appendTo(ul);
};

/*
 * Renders a user suggestion.
 *
 * @param {dom} ul suggestion list
 * @param {map} item suggestion map
 */
var fnRenderUser = function (ul, item) {
    //Thank you StackOverflow: http://stackoverflow.com/a/18459157/3907538
    //Add the .ui-state-disabled class and don't wrap in <a> person is already active
    if (item.active === '1' &&
       (item.roleid === '20' ||//person is an appointee (20) or candidate (6)
        item.roleid === '6')) {
        var reason = item.roleid === '20' ?
                     'Appointment in Progress' :
                     'Active Candidate';
        return $('<li class="ui-state-disabled">' + item.label + ' - ' + reason + '</li>').appendTo(ul);
    }
    else {
        return $('<li>')
        .append('<a>' + item.label + '</a>')
        .appendTo(ul);
    }
};

$(document).ready(function() {
    initFormSubmit();

    // make sticky the academic action form edit warning
    $('#warning .sticky').containedStickyScroll({
        duration: _navigator.ua.browser.family === 'IE' ? 1 : 0,
        hideClose: false
    });

    // form fields
    var usersearch = $('#usersearch');
    var surname = $('input[name=surname]');
    var givenName = $('input[name=givenName]');
    var middleName = $('input[name=middleName]');
    var delegationAuthority = $('input[name=delegationAuthority]');
    var effectiveDate = $('input[name=effectiveDate]');
    var retroactiveDate = $('input[name=retroactiveDate]');
    var endDate = $('input[name=endDate]');

    // section header spans
    var actionInformationDelegationAuthority = $('.sections > h2:first-of-type > span:first-of-type');
    var actionInformationEffectiveDate = $('.sections > h2:first-of-type > span:last-of-type');
    var candidateInformationHeader = $('.sections > h2:nth-of-type(2) > span');

    /*
     * Update candidate information accordion header
     */
    var fnNameChange = function() {
        var name = givenName.val();

        if (middleName.val().trim() !== '') {
            name = name + ' ' + middleName.val();
        }

        if (surname.val().trim() !== '') {
            name = name + ' ' + surname.val();
        }

        candidateInformationHeader.text(trim(name) ?
                                        'Appointee: ' + name :
                                        'Appointee Information');
    };
    surname.change(fnNameChange);
    middleName.change(fnNameChange);
    givenName.change(fnNameChange);

    // user search auto-complete
    var suggestions = $('.userSuggestion').suggestion({
        suggestionSource : [
                                {label : 'MyInfoVault', name : 'mivUserID',      path : 'person/name', minLength : 2},
                                {label : 'Campus',      name : 'ldapPersonUUID', path : 'ldap/cn',     minLength : 4}
                           ],
        select           : function(event, ui) {
                               // update the name input fields on select
                               surname.val(ui.item.surname);
                               givenName.val(ui.item.givenname);
                               middleName.val(ui.item.middlename);

                               // update candidate information accordion header
                               candidateInformationHeader.text(ui.item.label);
                           },
        autoFocus        : true
    }).data('custom-suggestion');

    //check if suggestions exists, otherwise causes errors rendoring.
    if (suggestions !== null && suggestions !== undefined) {
        suggestions._renderItem = fnRenderUser;
    }

    // correct effectiveDate format
    effectiveDate.change(function(event) {
        var $self = $(this);

        if ($self.val().trim() !== '') {
            if (validateDateField($self.val()) === true) {
                $self.val(Date.parse($self.val()).toString('MMMM d, yyyy'));
            }
            else {
                $self.val('').focus();
            }
        }
    });
    // correct retroactive and end date formats (must allow blank)
    retroactiveDate.add(endDate).change(function(event) {
        var $self = $(this);
        if ($self.val().trim() === '') {
            $self.val('');
        }
        else {
            if (validateDateField($self.val()) === true) {
                $self.val(Date.parse($self.val()).toString('MMMM d, yyyy'));
            }
        }
    });


    // update action information header on delegation of authority change
    delegationAuthority.change(function(event) {
        actionInformationDelegationAuthority.text($(this).next('label').text());
    });

    // update action information header on effective date change
    effectiveDate.change(function(event) {
        actionInformationEffectiveDate.text($(this).val());
    });


    var fnFormatAsMoneyHandler = function (event) {
        fnFormatAsMoney(0, event.currentTarget);
    };

    var fnFormatAsMoney = function (index, element) {
        var upperlimit = 999999999999999.00;

        var useDecimal = element.value.indexOf('.') !== -1;

        var nStr = stripCurrency(element.value);
        element.value = formatAsMoneyFromString(nStr, upperlimit, useDecimal);
    };

    var fnStripCommaFromMoneyHandler = function (event) {
        event.currentTarget.value = stripCurrency(event.currentTarget.value);
    };

    var fnTitleCodePadding = function (event) {
        var returnValue = trim(event.currentTarget.value);

        if (trim(returnValue) !== '' && !isNaN(returnValue)) {
            var numberOfDigits = 4;
            returnValue = trim(event.currentTarget.value);

            for (var i=0; i < numberOfDigits - trim(event.currentTarget.value).length; i++) {
                returnValue = '0'+returnValue;
            }

            event.currentTarget.value = returnValue;
        }
    };

    var fnRefreshWidgets = function() {
        $('input[id$=code]').focusout(fnTitleCodePadding);
        $('input[id$=periodSalary], input[id$=annualSalary]').submit(fnStripCommaFromMoneyHandler).focusout(fnFormatAsMoneyHandler).each(fnFormatAsMoney);

        // enable or disable withoutSalary checkbox
        $('input[id$=".percentOfTime"][id*="titles"]').change(function() {
            var $self = $(this);
            var value = $self.val();
            var result = validatePercentOfTime(value) && parseInt(value) === 0;
            var $title = $self.closest('div.title');

            // select form checkbox insted of spring provided place holder
            var $withoutSalary = $title.find('[name$=".withoutSalary"]:not([name^="_"])');
            var withoutSalaryCell = $withoutSalary.closest('.cell');

            // unchecked and disabled the checkbox when the percentOfTime is ZERO
            $withoutSalary
                .prop('checked', ($withoutSalary.prop('checked') && result))
                .prop('disabled', !result);

            withoutSalaryCell.toggleClass('disabled', !result);
        });
    };
    fnRefreshWidgets();

    /* Clear/reset template forms and headers. */
    var resetAssignmentAndTitleTemplates = function(assignmentTemplate, titleTemplate)
    {
        titleTemplate.nextAll('.title').remove();
        titleTemplate.removeAttr('data-path');
        assignmentTemplate.removeAttr('data-path').removeClass('primary').addClass('joint');

        $('input:not(:radio):not(:checkbox)', assignmentTemplate).val('');
        $('input:radio', assignmentTemplate).prop('checked', false);
        $('input:checkbox', assignmentTemplate).prop('checked', false);
        $('select option', assignmentTemplate).removeAttr('selected');
        $('h3 > span', assignmentTemplate).text(sDefaultJointAssignmentHeader);
        $('h4 > span', assignmentTemplate).text(sDefaultTitleHeader);
    };

    /*
     * Save assignment as templates for adding.
     */
    ASSIGNMENT_TEMPLATE = $('.assignment').first().clone();
    resetAssignmentAndTitleTemplates(ASSIGNMENT_TEMPLATE, $('.title', ASSIGNMENT_TEMPLATE).first());

    /*
     * Removing appointmentDuration field from present status for appointment action
     */
    var bAppointment = $('div.sections').hasClass('APPOINTMENT');
    if (bAppointment) {
        $('#PRESENT select[name$="appointmentDuration"]').closest('div.cell').remove();
    }

    /*
     * Title salary fields selector strings.
     */
    var SALARY_PERIOD_SELECTOR = 'input[name$=salaryPeriod]';
    var PERIOD_SALARY_SELECTOR = 'input[name$=periodSalary]';
    var ANNUAL_SALARY_SELECTOR = 'input[name$=annualSalary]';

    /*
     * Detach-and-stow or restore the period and annual salary
     * fields depending on the the state of the salary period
     * selection. Additionally alter the period salary label.
     */
    var fnResetSalaryFields = function(title) {
        // currently selected salary period for this title
        var selectedSalaryPeriod = $(SALARY_PERIOD_SELECTOR, title).filter(':checked');

        // is 'MONTHLY' selected?
        var bMonthly = selectedSalaryPeriod.val() === 'MONTHLY';

        // select the salaries block for this title
        var salaries = $('.salaries', title);

        // restore salaries block if missing
        if (!salaries.length) {
            salaries = title.data('salaries');

            // insert after the salary period cell
            salaries.insertAfter(selectedSalaryPeriod.closest('.cell'));
        }

        var $periodSalary = $(PERIOD_SALARY_SELECTOR, title);

        /*
         * Salary period selection changed:
         *  1. stow current period salary value
         *  2. restore saved period salary value
         */
        var saved = $periodSalary.data('saved') || '';
        $periodSalary.data('saved', $periodSalary.val());
        $periodSalary.val(saved);

        var annualSalary = $(ANNUAL_SALARY_SELECTOR, title).closest('.cell');

        // if monthly salary period selected
        if (bMonthly) {
            // update period salary label
            $periodSalary.prev('label').text('Monthly Salary');

            // restore annual salary block if missing
            if (!annualSalary.length) {
                salaries.append(title.data('annualSalary'));
            }
        }
        // else, hourly-rate selected
        else {
            // update period salary label
            $periodSalary.prev('label').text('Hourly Rate');

            // detach and stow the annual salary if present
            if (annualSalary.length) {
                title.data('annualSalary', annualSalary.detach());
            }
        }
    };

    /*
     * Set the title salary fields based on the initial
     * state of the salary period selection.
     */
    var fnSetSalaryFields = function(title) {
        // selected salary period (MONTHLY, HOURLY, or undefined)
        var sPeriod = $(SALARY_PERIOD_SELECTOR, title).filter(':checked').val();

        // if monthly not selected
        if (sPeriod !== 'MONTHLY') {
            // detach and stow the annual salary
            title.data('annualSalary', $(ANNUAL_SALARY_SELECTOR, title).closest('.cell').detach());
        }

        // if nothing selected
        if (!sPeriod) {
            // detach and stow the entire salaries cell
            title.data('salaries', $(PERIOD_SALARY_SELECTOR, title).closest('.cells').detach());
        }
    };

    /*
     * Title salary fields:
     *  - Set initially
     *  - Reset when the salary period changes
     */
    $('.title').each(function(index, title) {
        fnSetSalaryFields($(title));
    });
    $(document).on('change', SALARY_PERIOD_SELECTOR, function(event) {
        fnResetSalaryFields($(event.currentTarget).closest('.title'));
    });

    /*
     * Add assignment or title
     */
    $(document).on('click', '.APPOINTMENT button.add, .assignment button.add', function(event) {
        event.preventDefault();

        var button = $(event.currentTarget);
        var bTitle = button.prev().is('.title');
        var $status = button.closest('fieldset.status');
        var bPresent = $status.closest('div').is($('#PRESENT'));
        var bJoint = $status.find('div.assignment').length > 0;

        var dTemplate = bTitle ? $('.title', ASSIGNMENT_TEMPLATE).first() : ASSIGNMENT_TEMPLATE;
        var newElement = $(dTemplate.clone());

        // Remove Appointment Duration for Appointment action
        if (bPresent && bAppointment) {
            newElement.find('select[name$=appointmentDuration]').closest('div.cell').remove();
        }

        // if it's not a title and not a joint (i.e. it's first assignment)
        if (!bTitle && !bJoint) {
            // Add 'Home Department' header
            $('h3 > span', newElement).text(sDefaultPrimaryAssignmentHeader);

            // remove 'Remove Department' button
            newElement.find('> button.remove').remove();
        }

        if (!bPresent && bTitle) {
            //remove years at rank and step fields from proposed titles
            newElement.find('div.cell:has([id$=yearsAtRank], [id$=yearsAtStep])').remove();
        }

        button.before(newElement);

        // add expand/collapse ability to new element
        newElement.find('.title').andSelf().accordion({
            header : '> :header',
            collapsible : true,
            heightStyle: 'content'
        });
        // given first input focus after creating
        $('input:first', newElement).focus();

        // school-department auto-complete for new assignment
        if (!bTitle) {
            $('input[name$="scope"]', newElement).suggestion({
                suggestionSource : bJoint || (bAppointment && bPresent) ? nonScopedSchoolDepartmentSource : scopedSchoolDepartmentSource,
                autoFocus        : true,
                sDefaultValue    : '0:0'
            }).data('custom-suggestion')._renderItem = fnRenderDepartment;
        }

        fnRefreshWidgets();

        fnUpdateIndexes(newElement);

        // initialize salary fields for new title
        fnSetSalaryFields(bTitle ? newElement : newElement.find('.title'));
    });

    /*
     * Remove assignment or title
     */
    $(document).on('click', '.APPOINTMENT button.remove, .title button.remove', function(event) {
        event.preventDefault();

        var container = $(event.currentTarget).parent();
        var sContainerClass = container.is('.title') ? 'title' : 'assignment';
        var sContainerPath = container.data('path');

        // if assignment or title to remove exists server-side
        if (sContainerPath) {
            /*
             * Insert hidden selected field. This informs the
             * form that this element should be removed.
             */
            container.after($('<input>').prop('type', 'hidden')
                                        .addClass(sContainerClass)
                                        .prop('name', sContainerPath + '.selected')
                                        .val(true));

            /*
             * We may want to change this to $.detach()
             * in the future to allow users to undo the
             * errant removal of an assignment or title.
             */
            container.empty().remove();
        }
        // otherwise, removing from client-side only
        else {
            // following title or assignment containers
            var followingContainers = container.nextAll('.' + sContainerClass);

            container.empty().remove();

            /*
             * We need to shift the indexes of the following containers
             * back by one. This ensures that no indexes are skipped
             * when binding new assignments and/or titles.
             */
            followingContainers.each(function(index, element) {
                fnUpdateIndexes($(element));
            });
        }
    });

    // remove first (primary) assignment's remove button
    $('.status').each(function(index, element) {
        $('.assignment', element).first().find('> button.remove').remove();
    });

    /*
     * Assignment and title collapse/expand
     */
    $('.title, .assignment').accordion({
        header : '> :header',
        collapsible : true,
        heightStyle: 'content'
    });

    // school-department auto-complete
    $('#status-block > div').each(function(index, element) {
        var bPresent = $(element).is('#PRESENT');

        $('.assignment', element).each(function(index, assignment) {
            var bJoint = index > 0;

            var search = $('input[name$="scope"]', assignment);

            if (search.is(':enabled'))
            {
                /*
                 * Use NON scoped school/department suggestion handler if either:
                 *  - assignment is joint
                 *  - status is present
                 */
                search.suggestion({
                    suggestionSource : bJoint || (bAppointment && bPresent) ? nonScopedSchoolDepartmentSource : scopedSchoolDepartmentSource,
                    autoFocus        : true,
                    sDefaultValue    : '0:0'
                }).data('custom-suggestion')._renderItem = fnRenderDepartment;
            }
            else
            {
                // set dept field to the 'data-initial' attribute
                search.val(search.data('initial'));
            }
        });
    });

    var showHideAppointmentDetails = function (value) {
        var toggle = value === 'NO';

        $('p.proceeding-error', $('#questions')).remove();
        $('#usersearch').parent().toggleClass('hidden', ! $('#currentEmployee1').is(':checked'));
        $('#status-block').prev('.sections > :header').toggleClass('hidden', toggle);
        $('#save').toggleClass('hidden', toggle);

        if (toggle) {
            $('#questions').append( '<p class="proceeding-error">Contact labor relations and/or your dean\'s office prior to proceeding with this appointment.</p>' );
        }
    };

    /*
     * Disable/enable new appointee questions.
     */
    var fnEnabled = function(dFieldset) {
        if (dFieldset.length !== 0) {
            var dPreviousField = $('input:checked', dFieldset.prev());

            /*
             * The form fields in this field set will be disabled if:
             *  - the previous field is disabled; or
             *  - the previous field selection is not 'YES'
             */
            var bDisabled = dPreviousField.is(':disabled') ||
                            dPreviousField.val() !== 'YES';


            // remove checks from radio options
            if (bDisabled) {
                $('input', dFieldset).prop('checked', false);
            }

            // enable/disable the radio form fields
            $('input', dFieldset).prop('disabled', bDisabled);

            // add/remove fieldset 'disabled' class
            dFieldset.toggleClass('disabled', bDisabled);

            // add/remove legend 'f_required' class
            $('legend', dFieldset).toggleClass('f_required', !bDisabled);

            showHideAppointmentDetails('YES');

            // enable/disable next fieldset
            fnEnabled(dFieldset.next());
        }
    };

    // remove disabled property to trigger onchange events
    $('#questions input').prop('disabled', false);

    // trigger enabled function
    var presentStatus;// = undefined;
    $('#questions > fieldset:first-of-type input').change(function() {
        var $self = $(this);
        fnEnabled($self.closest('fieldset').next());
        var toggle = $self.is(':checked') && $self.val() === 'NO';

        $('.cell.usersearch').toggleClass('hidden', ! $('#currentEmployee1').is(':checked'));

        // toggle disabled on canidate name fields
        usersearch.prop('disabled', isNotDepartment);
        surname.prop('disabled', !toggle || isNotDepartment);
        givenName.prop('disabled', !toggle || isNotDepartment);
        middleName.prop('disabled', !toggle || isNotDepartment);

        // detach and stow present status element
        if (toggle) {
            presentStatus = $('#PRESENT').detach();
        }
        // restore present status element
        else if (presentStatus) {
            $('#PROPOSED').before(presentStatus);
            presentStatus = undefined;
        }

//        $('#PROPOSED').toggleClass("full", toggle);

        // THIS SHOULD BE DEALT WITH SERVER-SIDE! When the form-backing is being contructed?
        // if appointee is UC Davis employee and No Present assignments
        // (it only happen when edit the AAF)
        if ($self.val() === 'YES' && $('#PRESENT').find('div.assignment').length === 0) {
            $('#PRESENT').find('.add:last').click();
        }
    });

    // trigger change event for currentEmployee checked radio box
    var $checkedRadio = $('input[type="radio"][name="currentEmployee"]:checked');

    if ($checkedRadio.length > 0) {
        $checkedRadio.trigger('change');
    }
    else {
        $('input[type="radio"][name="currentEmployee"]').trigger('change');
    }

    // all question inputs except the last
    $('#questions > fieldset:not(:last-of-type) input').change(function(event) {
        fnEnabled($(this).closest('fieldset').next());
    });

    // Have you notified labor relations about this proposed change in appointment?
    // if no can't proceed on action
    $('#questions > fieldset:last-of-type input').change(function() {
        var $self = $(this);
        showHideAppointmentDetails($self.val());
    });

    // disabled all question when if its not department location
    if (isNotDepartment) {
        $('#questions input:not(:disabled)').prop('disabled', true);
    }

    /*
     * Section accordions.
     */
    $('.sections').accordion({
        heightStyle : 'content',
        activate : function(event, ui) {
            // allow sibling accordion headers to be tabbed to
            $('> :header', ui.newHeader.parent()).prop('tabindex', 0);

            // remove current header from tab order
            ui.newHeader.prop('tabindex', -1);
        },
        create : function(event, ui) {
            // allow sibling accordion headers to be tabbed to
            $('> :header', ui.header.parent()).prop('tabindex', 0);

            // remove default header from tab order
            ui.header.prop('tabindex', -1);
        }
    });

    /*
     * Effective date picker.
     */
    $('.datepicker:not(:disabled)').datepicker({
        dateFormat : 'MM d, yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        constrainInput : false,
        yearRange: 'c-10:c+2',
        currentText: 'Find Today',
        closeText: 'Close',
        buttonImage: '/miv/images/buttons/datepicker.png',
        buttonImageOnly: true,
        showOn: 'both'
    });

    // end date range: last year to fifty years from now
    endDate.datepicker('option', 'yearRange', 'c-1:c+50');

    $('img[class="ui-datepicker-trigger"]').each(function() {
        $(this).attr('title', 'Open the datepicker.');
    });

    /*
     * Update assignment and title headers on content change.
     */
    $(document).on('change', '.title input[name$="description"],' +
                             '.title input[name$="percentOfTime"]', function(event) {
        var title = $(event.currentTarget).closest('.title');

        updateHeader(title,
                     $('input[name$="description"]', title).val(),
                     sDefaultTitleHeader);
    });
    $(document).on('change', '.assignment input[name$="scope"],' +
                             '.assignment input[name$="percentOfTime"]', function(event) {
        var assignment = $(event.currentTarget).closest('.assignment');
        var sDefaultAssignmentHeader = assignment.is(':first-of-type') ?
                                       sDefaultPrimaryAssignmentHeader :
                                       sDefaultJointAssignmentHeader;

        updateHeader(assignment,
                     $('input[type="search"]', assignment).val(),
                     sDefaultAssignmentHeader);
    });
});


/**
 * initialize form submit
 */
function initFormSubmit() {

    var $form = $('#form');

    /*
     * Activate the accordion section containing a required field with missing value.
     */
    $(':input').on('invalid', function() {
        /*
         * For each accordion section in which the required field resides.
         */
        $(this).parents('.ui-accordion-content').each(function(i, section) {
            // this section's accordion
            var accordion = $(section).parent();

            // this section's index among its sibling sections
            var index = accordion.find('ui-accordion-content').index(section);

            // activate (open) this accordion section
            accordion.accordion('option', 'active', index);
        });
    });

    /*
     * Form input fields using the html5 pattern attribute
     * must provide a 'data-customvalidity' attribute
     * containing a message for a pattern mismatch.
     */
    $(':input[pattern]').on('change', function() {
        this.setCustomValidity(this.validity.patternMismatch ?
                               $(this).data('customvalidity') :// add custom message
                               '');// remove custom message
    });

    $form.submit(function(event) {
        setErrorLabels();
        var $clickedButton = getClickedButton($(this));

        if ($clickedButton.prop('id') !== 'cancel') {
            var $message = $('#messagebox');
            var formValidation = jQuery.extend(true, {}, mivFormProfile); // to get fresh copy of mivFormProfile

            /*
             * Adding constraints dynamically for validation
             */

            $('#questions fieldset').each(function(index, element) {
                var $self = $(element);

                if (!$self.hasClass('disabled') && !$('input', element).is(':checked')) {
                    formValidation.required.push($('input:first', $self).prop('id'));
                }
            });

            // FIXME: Will a fieldset.status ever be hidden? We detach/restore this element?
            $('#status-block fieldset.status:not(.hidden)').each(function(statusIndex, statusElement) {
                $('div.assignment', statusElement).each(function(assignmentIndex, assignmentElement) {
                    var $assignmentElement = $(assignmentElement);

                    // department
                    var $element = $assignmentElement.find('[id$=".scope"]');
                    var id = $element.prop('id');
                    if (jQuery.type(formValidation.constraints[id]) === 'undefined') {
                        formValidation.constraints[id] = validateDepartment;
                    }


                    // percentOfTime
                    $element = $assignmentElement.find('[id$=".percentOfTime"]:not([id*="titles"])');
                    id = $element.prop('id');
                    formValidation.required.push(id);
                    formValidation.trim.push(id);

                    if (jQuery.type(formValidation.constraints[id]) === 'undefined') {
                        formValidation.constraints[id] = validateAssignmentPercentOfTime;
                    }


                    $('div.title', assignmentElement).each(function(titleIndex, titleElement) {
                        var $titleElement = $(titleElement);

                        // Title Code
                        $element = $titleElement.find('[id$=".code"]');
                        id = $element.prop('id');
                        formValidation.required.push(id);
                        formValidation.trim.push(id);

                        if (jQuery.type(formValidation.constraints[id]) === 'undefined') {
                            formValidation.constraints[id] = validateTitleCode;
                        }


                        // Rank and Title
                        $element = $titleElement.find('[id$=".description"]');
                        id = $element.prop('id');
                        formValidation.required.push(id);
                        formValidation.trim.push(id);


                        // % of Time
                        $element = $titleElement.find('[id$=".percentOfTime"]');
                        id = $element.prop('id');
                        formValidation.required.push(id);
                        formValidation.trim.push(id);

                        if (jQuery.type(formValidation.constraints[id]) === 'undefined') {
                            formValidation.constraints[id] = validatePercentOfTime;
                        }


                        // Appointment Duration
                        $element = $titleElement.find('[id$=".appointmentDuration"]');
                        id = $element.prop('id');
                        if (jQuery.type(formValidation.constraints[id]) === 'undefined') {
                            formValidation.constraints[id] = validateAppointmentDuration;
                        }

                        // salaryPeriod
                        $element = $titleElement.find('[name$="salaryPeriod"]');
                        id = $element.prop('id');
                        var $checkedElement = $element.filter(':checked');

                        // salaryPeriod is required
                        if (typeof $checkedElement.val() === 'undefined') {
                            formValidation.required.push(id);
                        }
                        else { // if something checked apply checks on salaries
                            var $periodSalary = $titleElement.find('[id$=".periodSalary"]');
                            var periodSalaryId = $periodSalary.prop('id');

                            // Period Salary is required
                            formValidation.required.push(periodSalaryId);
                            formValidation.trim.push(periodSalaryId);

                            if (jQuery.type(formValidation.constraints[periodSalaryId]) === 'undefined') {
                                formValidation.constraints[periodSalaryId] = validatePeriodSalary;
                            }

                            // if this is monthly salary type make annualSalary required
                            if ($checkedElement.val() === 'MONTHLY') {
                                var $annualSalary = $titleElement.find('[id$=".annualSalary"]');
                                var annualSalaryId = $annualSalary.prop('id');

                                formValidation.required.push(annualSalaryId);
                                formValidation.trim.push(annualSalaryId);
                                if (jQuery.type(formValidation.constraints[annualSalaryId]) === 'undefined') {
                                    formValidation.constraints[annualSalaryId] = validateAnnualSalary;
                                }
                            }
                        }
                    }); // $("div.title", assignmentElement)
                }); // $("div.assignment", statusElement)
            }); // $("#status-block fieldset.status")

            // validate form
            if (!validateForm($form, formValidation, $message)) {
                event.preventDefault();
            }
            else {
                // Re-enable the name fields in case they were disabled when a person was chosen from the pick-list
                // They need to be sent to the server and used until the new User Service can load non-MIV users.
                $('input[name=surname]').prop('disabled', false);
                $('input[name=givenName]').prop('disabled', false);
                $('input[name=middleName]').prop('disabled', false);
            }
        }
    });
}
// initFormSubmit


/**
 * Open errored sections accordion first than apply focus
 *
 * @Override function of form.validation.js
 */
function customValidationResult(field)
{
    try {
        var $parent = field.closest('div.sections > div').prev(':header');

        $('.sections').accordion('option', 'active', $('div.sections > :header').index($parent));
    }
    catch (e) {
        console.warn('exception "e" is: ' + e);
    }
}
// customValidationResult


/**
 * Generate dynamic error labels for all input fields
 */
function setErrorLabels()
{
    // Adding id's to field sets for radio validations
    $('#questions fieldset').each(function(index, element) {
        var $self = $(element);
        var $questionOption1 = $('input:first', $self);
        $self.prop('id', $questionOption1.prop('name'));
    });

    $('#status-block fieldset.status:not(.hidden)').each(function(statusIndex, statusElement) {
        var statusLabel = $('legend:first', statusElement).text();
        var label = '';

        $('div.assignment', statusElement).each(function(assignmentIndex, assignmentElement) {
            var assignmentLabel = ', ' + (assignmentIndex === 0 ? sDefaultPrimaryAssignmentHeader : sDefaultJointAssignmentHeader +'['+assignmentIndex+']');

            var department = $(assignmentElement).find('[id$=".scope"]').val().trim();
            if (department !== '') {
                assignmentLabel = ', ' + department;
            }

            label = statusLabel + assignmentLabel + ' -';

            setErrorLabel(assignmentElement, '.scope', label);
            setErrorLabel(assignmentElement, '.percentOfTime', label);

            $('div.title', assignmentElement).each(function(titleIndex, titleElement) {
                var $titleElement = $(titleElement);
                var titleLabel = 'Title['+(titleIndex+1)+']';
                var description = $titleElement.find('[id$=".description"]').val().trim();
                if (description !== '') {
                    titleLabel = description;
                }

                label = statusLabel + assignmentLabel + ', ' + titleLabel + ' -';

                setErrorLabel(titleElement, '.code', label);
                setErrorLabel(titleElement, '.description', label);
                setErrorLabel(titleElement, '.step', label);
                setErrorLabel(titleElement, '.percentOfTime', label);
                setErrorLabel(titleElement, '.periodSalary', label);
                setErrorLabel(titleElement, '.annualSalary', label);
                setErrorLabel(titleElement, '.appointmentDuration', label);

                var $element = $titleElement.find('[id$=".salaryPeriod1"]');
                var $radioGroup = $element.closest('fieldset.cell');
                $radioGroup.prop('id', $element.prop('name'));
                setErrorLabel(titleElement, '.salaryPeriod', label);
            });
        });
    });
}
// setErrorLabels

/**
 * validate date field
 */
function validateDateField(value)
{
    var date = Date.parse(value);

    if (date === null) {
        return 'INVALID_DATE_FORMAT';
    }

    if (date instanceof Date && date.compareTo(Date.parse('01/01/1900')) === -1) {
        return 'INVALID_DATE_MINRANGE';
    }

    return true;
}
// validateDateField


/**
 * validate period salary field
 * period salary can't be negative and must be lesser than 99999.99
 */
function validatePeriodSalary(value)
{
    value = stripCurrency(value);
    if (!isNumber(value)) {
        return 'INVALID_NUMBER';
    }

    value = parseFloat(value);
    if (value < 0 || value > 99999.99) {
            return 'INVALID_PERIOD_SALARY_RANGE';
    }

    return true;
}
// validatePeriodSalary


/**
 * validate annual salary field
 * annual salary can't be negative and must be lesser than 999999
 */
function validateAnnualSalary(value, id)
{
    value = stripCurrency(value);
    var bWithoutSalary = getElementById(id).closest('.title')
                                           .find('input[name$="withoutSalary"]')
                                           .is(':checked');

    if (!isNumber(value)) {
        return 'INVALID_NUMBER';
    }
    value = parseFloat(value);

    var decimal = value - Math.floor(value);
    if (decimal > 0.0) {
        return 'INVALID_ANNUAL_SALARY';
    }

    // minimum is zero if WOS, one otherwise
    var minAnnualSalary = bWithoutSalary ? 0 : 1;

    if (value < minAnnualSalary || value > 999999.00) {
            return bWithoutSalary ?
                   'INVALID_ANNUAL_SALARY_RANGE_WOS' :
                   'INVALID_ANNUAL_SALARY_RANGE';
    }

    return true;
}
// validateAnnualSalary


/**
 * validation used for acceleration
 */
function validateAccelerationYears(field)
{
    return validateRange(field, 'INVALID_ACCELERATION_YEARS', MINIMUM, MAX_ACCELERATION_YEARS);
}

/**
 * validation used for rank and step years fields.
 */
function validateYearsAtRankStep(field)
{
    return validateRange(field, 'INVALID_RANKSTEP_YEARS', MINIMUM, MAX_RANKSTEP_YEARS);
}

/**
 * Validation used for quarter count fields (years at rank for reappointments.
 */
function validateQuarterCount(field)
{
    return validateRange(field, 'INVALID_QUARTER_COUNT', MINIMUM, MAX_QUARTER_COUNT);
}

function validateRange(field, sErrorMessage, nMiniumum, nMaximum)
{
    var value = field.val();

    // must be numeric and within the proper range
    return !$.isNumeric(value) || value < nMiniumum || value > nMaximum ?
           sErrorMessage :
           true;
}

/**
 * validate department field
 * department can't be empty
 */
function validateDepartment(value, id)
{
    var $element = getElementById(id);
    var $cell = $element.closest('div.cell');

    // get the name from id because actual value stored in hidden field
    var $hiddenField = $('input[type="hidden"]', $cell);
    value = $hiddenField.val();

    if (trim(value) === '0:0' || trim(value) === '') {
        return 'REQUIRED';
    }

    // Validate duplicate departments
    var $status = $element.closest('.status');
    var result = true;

    // get all assignments's scope fields
    $('input[name$=".scope"]', $status).not($hiddenField).each(function(i, e) {
        if (value === $(e).val()) {
            result = 'DUPLICATE_DEPARTMENTS';
            return false; // break the each loop
        }
    });

    return result;
}
// validateDepartment


function validateAppointmentDuration(value) {
    if (trim(value) === 'NA') {
        return 'REQUIRED';
    }

    return true;
}

/**
 * validate assignment PercentOfTime field
 * percent of time must be lesser or equals to 100 % also
 *                 assignment PercentOfTime = sum of title PercentOfTime
 */
function validateAssignmentPercentOfTime(value, id)
{
    var result = validatePercentOfTime(value);
    var $element = getElementById(id);
    var $assignment = $element.closest('div.assignment');

    var error = false;

    if (result === true) {
        // get all title's Percent Of Time fields
        $('input[id$=percentOfTime][id*=titles]', $assignment).each(function(index, element) {
            var $self = $(this);
            var pVal = $self.val();

            if (validatePercentOfTime(pVal) === true) {
                value = value - parseInt(pVal);
            }
            else { // skip validation since there is some error on title's PercentOfTime
                error = true;
                return false; // return false to break out of each() loops.
            }
        });

        if (!error && value !== 0) {
            return 'INVALID_PERCENT_TIME_DISTRIBUTION';
        }

        // validate only if its belongs to home department
        if ($assignment.hasClass('primary')) {
            var $status = $assignment.closest('.status');
            value = 0;

            // get all assignments's Percent Of Time fields
            $('input[id$=percentOfTime]:not([id*=titles])', $status).each(function(index, element) {
                var $self = $(this);
                var pVal = $self.val();

                // comparing boolean true because validatePercentOfTime may return error message code.
                if (validatePercentOfTime(pVal) === true) {
                    value = value + parseInt(pVal);
                }
                else { // skip validation since there is some error on assignment's PercentOfTime
                    error = true;
                    return false; // return false to break out of each() loops.
                }
            });

            // Assignment's - % of Time must be 100% or less
            if (!error && value > 100) {
                /* MIV-5400 Trying to turn off the field name & focus and provide only a message.
                 * This didn't really work as it rippled through validation.js and form.validation.js
                 * Much bigger changes are needed, but a change from string-based to object-based
                 * validation reporting is the right way to go, so leaving this in place.
                 */
                return { 'fld':null, 'message':'INVALID_ASSIGNMENT_PERCENT_TIME_DISTRIBUTION' };
            }
        }
    }

    return result;
}
// validateAssignmentPercentOfTime


/**
 * validate PercentOfTime field
 * PercentOfTime can't be empty,
 *               must be numeric and
 *               must be lesser or equals to 100 %
 */
function validatePercentOfTime(value)
{
    if (trim(value) === '') {
        return 'REQUIRED';
    }

    if (isNaN(value)) {
        return 'INVALID_NUMBER';
    }

    if (value < 0 || value > 100) {
        return 'INVALID_PERCENT_TIME_RANGE';
    }

    return true;
}
// validatePercentOfTime


/**
 * validate title code field
 * title code must be numeric
 *
 */
function validateTitleCode(value) {

    if (isNaN(value)) {
        return 'INVALID_NUMBER';
    }

    return true;
}
// validateTitleCode

/**
 * Update the immediate child header of the given title or assignment element.
 *
 * @param element title or assignment element
 * @param sLabelSelector selector of input for label value
 * @param sDefaultLabel default label if not value at given selector
 */
function updateHeader(element, sLabel, sDefaultLabel)
{
    var sPercentOfTime = trim($('input[name$="percentOfTime"]', element).val());

    if (!sLabel) {
        sLabel = sDefaultLabel;
    }

    if (!sPercentOfTime) {
        sPercentOfTime = '0';
    }

    $('> :header span:nth-child(2)', element).text(sLabel + ' – ' + sPercentOfTime + '%');
}

/**
 * Regular expression matches the title index in an input attribute.
 */
var reTitleIndex = new RegExp(/\d+(?=\D+\d?$)/);

/**
 * Regular expression matches the assignment index in an input attribute.
 */
var reAssignmentIndex = new RegExp(/\d+/);

/**
 * Regular expression matches the assignment index in an input attribute.
 */
var reStatusKey = new RegExp(/PRESENT|PROPOSED/);

/**
 * Updates status key and assignment and title indexes
 * on inputs and labels of a new assignment or title.
 *
 * @param event assignment or title add button click event
 */
var fnUpdateIndexes = function(element)
{
    // is this container an assignment (otherwise, a title)
    var bAssignment = element.is('.assignment');

    var dAssignment = bAssignment ? element : element.closest('.assignment');

    // get the index of the target assignment and the map key of the target status
    var nAssignmentIndex = dAssignment.prevAll('.assignment').length;
    var sStatusKey = dAssignment.closest('.status').closest('div').prop('id');

    // update status key
    updateIndexes(element, reStatusKey, sStatusKey);

    //update assignment index
    updateIndexes(element, reAssignmentIndex, nAssignmentIndex);

    // if container is a title
    if (!bAssignment) {
        var nTitleIndex = element.prevAll('.title').length;

        // update title index
        updateIndexes(element, reTitleIndex, nTitleIndex);
    }
};

/**
 * Replace indexes matched in the given regular expression for the
 * labels and inputs of the given element with the given index.
 *
 * @param element DOM element to update
 * @param re regular expression matching indexes to replace
 * @param index replacement index
 */
function updateIndexes(element, re, index)
{
    // no need to re-index buttons, beacuse they dosen't need to know about datapath.
    $(':input:not(:button)', element).each(function() {
        updateIndex(this, 'id', re, index);
        updateIndex(this, 'name', re, index);
    });

    $('label', element).each(function() {
        updateIndex(this, 'for', re, index);
    });
}

function updateIndex(element, sProperty, re, index)
{
    var e = $(element);

    e.prop(sProperty, e.prop(sProperty).replace(re, index));
}

/*<span class="vim">
 vim:ts=8 sw=4 et sr:
</span>*/
