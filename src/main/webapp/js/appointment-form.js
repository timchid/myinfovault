'use strict';
/* global htmlDecode */
/* exported departments */
/**
 *
 */

/*Appointee Form (new version) */
/*var data = [
{ label: "Anders, Kelly - Academic Affairs", category: "" },
{ label: "Arbulesco, Nick - IET - PMO", category: "" },
{ label: "Aya, Pradeep", category: "" },
{ label: "Anderson, Lisa", category: "--- ldap ---" },
{ label: "Andrews, Jackson", category: "--- ldap ---" },
{ label: "Arbor, James", category: "--- ldap ---" }
];*/

var scopedSchoolDepartmentSource = '/miv/suggest/scopedschool/scopeddepartment';
var nonScopedSchoolDepartmentSource = '/miv/suggest/school/department';


var data = [
{'personid':'00453738','lname':'Bray','fname':'Curtis','mname':'','label':'Bray, Curtis: Information and Educational Technology - Application Development','category':''},
{'personid':'00281994','lname':'Gilmore','fname':'Craig','mname':'Robert','label':'Gilmore, Craig Robert: Information and Educational Technology - Application Development','category':''},
{'personid':'00526526','lname':'Bailey','fname':'Patricia','mname':'A','label':'Bailey, Patricia A: School of Medicine - Family \u0026 Community Medicine','category':''},
{'personid':'00600824','lname':'Lee','fname':'Thomas','mname':'(Chun Man)','label':'Lee, Thomas (Chun Man): LS: MPS - Statistics','category':''},
{'personid':'00229512','lname':'Savageau','fname':'Michael','mname':'A','label':'Savageau, Michael A: Engineering - Biomedical Engineering','category':''},
{'personid':'00002292','lname':'Groza','fname':'Joanna','mname':'R','label':'Groza, Joanna R: Engineering - Chemical Engineering and Materials Science','category':''},
{'personid':'00033294','lname':'Whitten','fname':'Jedidiah','mname':'S','label':'Whitten, Jedidiah S: Information and Educational Technology - Application Development','category':'--- ldap ---'},
{'personid':'00277673','lname':'Hillman','fname':'Steven','mname':'','label':'Hillman, Steven: Information and Educational Technology - Inactive','category':'--- ldap ---'},
{'personid':'00017778','lname':'Peterson','fname':'Pete','mname':'','label':'Peterson, Pete: Information and Educational Technology - Application Development','category':'--- ldap ---'},
{'personid':'00407581','lname':'Garcia','fname':'Brian','mname':'','label':'Garcia, Brian: School of Medicine - Anesthesiology \u0026 Pain Medicine','category':'--- ldap ---'},
{'personid':'00472302','lname':'Giang','fname':'Phat','mname':'','label':'Giang, Phat: School of Medicine - Anesthesiology \u0026 Pain Medicine','category':'--- ldap ---'}
];

function getDepartmentAutocomplete(element, autoCompleteSource)
{

	$(element).autocomplete({
        // Append the scopes for the current logged in user to the search term
		source: autoCompleteSource,
        minLength: 2,
        focus : function(event, ui) {
                    $('#'+event.target.id).val(htmlDecode(ui.item.label));
                    return false;
                },
        select: function(event, ui) {
                    $('#'+event.target.id).val(htmlDecode(ui.item.label));
                    return false;
                },
        change: function (event, ui) {
                    // The item selected from the menu, if any.
                    // Otherwise the property is null
                    //so clear the item for force selection
                    if (!ui.item){
                        $('#'+event.target.id).val('');
                    }
                }
    }).data( 'ui-autocomplete' )._renderItem = function( ul, item ) {
        return $( '<li>' )
        .append( '<a>' + htmlDecode(item.label) + '</a>' )
        .appendTo( ul );
    };
}

function getJointDepartment(status, deptType, deptId)
{
    return '<div class="department-block joint-department '+status+'" id="joint-department-'+status+'-'+deptId+'">'+
            '  <div class="header">'+
            '    <div class="formlines">'+
            '       <div class="inline-formline">'+
            '           <label for="'+status+'_joint_dept_'+deptId+'">Joint Department: </label>'+
            '           <input type="text" id="'+status+'_joint_dept_'+deptId+'" name="'+status+'_joint_dept" class="departments" value="">'+
            '       </div>'+
            '       <div class="inline-formline">'+
            '           <label for="'+status+'_joint_percentage_time_'+deptId+'">% of time: </label>'+
            '           <input type="text" maxlength="3" size="4" id="'+status+'_joint_percentage_time_'+deptId+'" name="'+status+'_joint_percentage_time_'+deptId+'" value="">'+
            '    </div>'+
            '  </div>'+
            '</div>  <!-- end .header -->  '+
            ''+
            '<div class="title-blocks '+status+'">'+
            getTitleBlock(status, deptId, 1) +
            '    <div class="buttonset">'+
            '        <input type="submit" id="joint_add_title_'+deptId+'" name="btnAddTitle" value="Add Title" title="Add Title">'+
            '    </div>        '+
            '</div> <!-- End title-blocks--> '+
            '</div>';
}

function addJointDepartment(status, deptType, deptId)
{

    if ($('div.'+deptType+'-department.'+status).size() === 0) {
        $('div.'+deptType+'-departments.'+status).append(getJointDepartment(status, deptType, deptId));
    }
    else {
        $('div.'+deptType+'-department.'+status).last().after( getJointDepartment(status, deptType, deptId));
    }

    // alert($('#'+status+'_'+deptType+'_dept_'+deptId).prop('id'));
    getDepartmentAutocomplete($('#'+status+'_'+deptType+'_dept_'+deptId),nonScopedSchoolDepartmentSource);

    $('#'+deptType+'_add_title_'+deptId).click(function() {
        var id =$('div.title-block.'+status,$('#'+deptType+'-department-'+status+'-'+deptId)).last().prop('id').replace('title-block-'+status+'-','');
        id = parseInt(id) + 1;

        addTitleBlock(status, deptType, deptId, id);
    });

    var $jointDeptBlocks = $('div.joint-department.'+status);

    $('input.close.joint-dept.'+status, $jointDeptBlocks).remove();

    $('div.joint-department.'+status+' div.header div.formlines:first-child', $('div.joint-departments.'+status)).append('<input type="button" id="close-joint-dept-'+status+'-1" name="btnClose" value="X" title="Delete joint department" class="close joint-dept '+status+'">');

    $('input.close.joint-dept.'+status, $jointDeptBlocks).click(function() {
        $(this).parent('div').parent('div').parent('div').remove();
    });

    setUpSalaryTypeSelectEvent();
}

function getTitleBlock(status, deptId, titleId)
{
    return '<div class="title-block '+status+'" id="title-block-'+status+'-'+titleId+'">'+
            '            <div class="formlines">'+
            '               <div class="inline-formline">'+
            '                   <label for="'+status+'_title_rank_'+titleId+'">Rank and Title: </label>'+
            '                   <input type="text" id="'+status+'_title_rank_'+titleId+'" name="'+status+'_title_rank" value="">'+
            '               </div>'+
            '               <div class="inline-formline">'+
            '                   <label for="'+status+'_step_'+titleId+'">Step: </label>'+
            '                   <input type="text" maxlength="2" size="3" id="'+status+'_step_'+titleId+'" name="'+status+'_title_rank" value="">'+
            '               </div>'+
            '            </div>'+
            '        '+
            '            <div class="formlines">'+
            '               <div class="inline-formline">'+
            '                   <label for="'+status+'_titlecode_'+titleId+'">Title Code: </label>'+
            '                   <input type="text" maxlength="4" size="5" id="'+status+'_titlecode_'+titleId+'" name="'+status+'_titlecode" value="">'+
            '               </div>'+
            '               <div class="inline-formline">'+
            '                   <label for="'+status+'_percentage_time_'+titleId+'">% of time: </label>'+
            '                   <input type="text" maxlength="3" size="4" id="'+status+'_percentage_time_'+titleId+'" name="'+status+'_percentage_time" value="">'+
            '               </div>'+
            '            </div>'+
            '        '+
            '            <div class="formline">'+
            '            Appointment Type:  '+
            '            <input type="radio" id="'+status+'_appttype_9mo_'+titleId+'" name="'+status+'_appttype_'+titleId+'" value="9mo">'+
            '            <label for="'+status+'_appttype_9mo_'+titleId+'">9 Months</label>'+
            '        '+
            '            <input type="radio" id="'+status+'_appttype_11mo_'+titleId+'" name="'+status+'_appttype_'+titleId+'" value="11mo">'+
            '            <label for="'+status+'_appttype_11mo_'+titleId+'">11 Months</label>    '+
            '            </div>'+
            '            <div class="formlines">'+
            '               <div class="inline-formline">'+
            '                   <select id="'+status+'_salarytype_'+titleId+'" name="'+status+'_salarytype" class="salarytype">'+
            '                       <option value="H">Hourly</option>'+
            '                       <option value="M" selected="selected">Monthly</option>'+
            '                   </select>&nbsp;'+
            '                   <label for="'+status+'_salary_'+titleId+'">Salary: </label> '+
            '                   <input type="text" maxlength="5" size="6" id="'+status+'_salary_'+titleId+'" name="'+status+'_salary" value="">'+
            '               </div>'+
            '               <div class="inline-formline annual_salary">'+
            '                   <label for="'+status+'_annual_salary_'+titleId+'">Annual Salary: </label>'+
            '                   <input type="text" maxlength="7" size="8" id="'+status+'_annual_salary_'+titleId+'" name="'+status+'_annual_salary" value="">'+
            '               </div>'+
            '            </div>'+
            '    </div> <!-- End title-block-->   ';
}

function addTitleBlock(status, deptType, deptId, id)
{
    var $titleBlocks = $('#'+deptType+'-department-'+status+'-'+deptId);

    $('div.title-block.'+status,$titleBlocks).last().after(getTitleBlock(status, deptId, id));

    setUpSalaryTypeSelectEvent();

    $('input.close.title-block.'+status,$titleBlocks).remove();

    $('div.title-block.'+status+' div.formlines:first-child', $titleBlocks).append('<input type="button" id="close-'+status+'-1" name="btnClose" value="X" title="Delete Rank, Title & Step" class="close title-block '+status+'">');

    $('input.close.title-block.'+status,$titleBlocks).click(function() {
        var $self = $(this);
        $self.parent('div').parent('div').remove();

        var $closeButtons = $('input.close.title-block.'+status,$titleBlocks);
        if ($closeButtons.size() === 1) {
            $closeButtons.remove();
        }
    });
}

function setUpSalaryTypeSelectEvent()
{
    $('.salarytype').change(function() {
        var $self = $(this);
        $self.parent().siblings('div.annual_salary').toggle($self.val() !== 'H');
    });
}

function validateForm() {
    var result = true;

    var $lname = $('#lname');
    if ($lname.val().length === 0) {
        $lname.addClass('errorfield').focus();
        result = false;
    }
    else {
        $lname.removeClass('errorfield');
    }

    var $fname = $('#fname');
    if ($fname.val().length === 0) {
        $fname.addClass('errorfield').focus();
        result = false;
    }
    else {
        $fname.removeClass('errorfield');
    }

    if (!result) {
        $('.errorfield').first().focus();
    }

    return result;
}

$.widget( 'custom.catcomplete', $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
        var that = this,
        currentCategory = '';
        $.each( items, function( index, item ) {
            if ( item.category !== currentCategory ) {
                ul.append( '<li class="ui-autocomplete-category">' + item.category + '</li>' );
                currentCategory = item.category;
            }
            that._renderItemData( ul, item );
        });
    }
});

function setAppointeeDisplayname()
{
    var displayname = $('#lname').val();
    if ($('#fname').val().length > 0) {
        displayname = displayname + ', '+$('#fname').val();
    }

    if ($('#mname').val().length > 0) {
        displayname = displayname + ' '+$('#mname').val();
    }
    $('#appointee_displayname').val(displayname);
}

$(document).ready(function() {

    var $lname = $('#lname');
    var $fname = $('#fname');
    var $mname = $('#mname');
    var $appointee_displayname = $('#appointee_displayname');

    $( '#lname, #fname' ).each(function(index, element) {
        var $self = $(element);

        $self.catcomplete({
            delay: 0,
            minLength: 0,
            source: function( request, response ) {
                    var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), 'i' );
                    response( $.grep( data, function( item ) {
                        var value = item[$self.prop('id')];
                        return matcher.test( value );
                    }) );
            },
            focus: function(event, ui) {
                 $lname.val(htmlDecode(ui.item.lname));
                 $fname.val(htmlDecode(ui.item.fname));
                 $mname.val(htmlDecode(ui.item.mname));
                 return false;
            },
            select: function(event, ui) {
                 $lname.val(htmlDecode(ui.item.lname));
                 $fname.val(htmlDecode(ui.item.fname));
                 $mname.val(htmlDecode(ui.item.mname));
                 return false;
            },
            change: function (event, ui) {
                $mname.prop('disabled',false);
                // The item selected from the menu, if any.
                // Otherwise the property is null
                //so clear the item for force selection
                if (ui.item) {
                    $appointee_displayname.val(htmlDecode(ui.item.label));
                    if (ui.item.mname.length > 0) {
                        $mname.prop('disabled',false).val(htmlDecode( ui.item.mname));
                    }
                    else {
                        $mname.prop('disabled',true).val('');
                    }
                }
                else {
                    setAppointeeDisplayname();
                }
            }
        });
    });

    $mname.change(function(){
        setAppointeeDisplayname();
    });

    getDepartmentAutocomplete($('#present_home_dept_1'),scopedSchoolDepartmentSource);
    getDepartmentAutocomplete($('#proposed_home_dept_1'),scopedSchoolDepartmentSource);

    $('input[name="q2"], input[name="q3"], input[name="q4"]')
            .prop('disabled',true)
            .prop( 'checked', false );

    $('#present_add_title').click(function(){
        var id =$('div.title-block.present').last().prop('id').replace('title-block-present-','');
        id = parseInt(id) + 1;
        addTitleBlock('present','home',1,id);
    });

     $('#proposed_add_title').click(function(){
        var id = $('div.title-block.proposed').last().prop('id').replace('title-block-proposed-','');
        id = parseInt(id) + 1;
        addTitleBlock('proposed','home',1, id);
    });

    $('#present_add_joint_department').click(function(){
        var deptId = $('div.joint-department.present').size() === 0 ? 0 : $('div.joint-department.present').last().prop('id').replace('joint-department-present-','');

        deptId = parseInt(deptId) + 1;
        addJointDepartment('present', 'joint', deptId);
    });

    $('#proposed_add_joint_department').click(function(){
        var deptId = $('div.joint-department.proposed').size() === 0 ? 0 : $('div.joint-department.proposed').last().prop('id').replace('joint-department-proposed-','');
        deptId = parseInt(deptId) + 1;
        addJointDepartment('proposed', 'joint', deptId);
    });

    var $present = $('#present');
    var $proposed = $('#proposed');
    //var $q1 = $("#span-q1");
    var $q2 = $('#span-q2');
    var $q3 = $('#span-q3');
    var $q4 = $('#span-q4');


    $('input[name="q1"]').change(function(){
        if ($(this).val() === 'Yes') {
            $('input[name="q2"]').prop('disabled',false);
            $q2.addClass('f_required');

            $present.removeClass('hidden');
            $proposed.removeClass('full');
        }
        else {
            $present.addClass('hidden');
            $proposed.addClass('full');

            $q2.removeClass('f_required');
            $q3.removeClass('f_required');
            $q4.removeClass('f_required');

            $('input[name="q2"], input[name="q3"], input[name="q4"]')
                    .prop('disabled',true)
                    .prop( 'checked', false )
                    .removeClass('f_required');
        }
    });

    $('input[name="q2"]').change(function(){
        if($(this).val() === 'Yes') {
            $('input[name="q3"]').prop('disabled',false);
            $q3.addClass('f_required');
        }
        else {
            $('input[name="q3"], input[name="q4"]').prop('disabled',true).prop( 'checked', false );

            $q3.removeClass('f_required');
            $q4.removeClass('f_required');
        }
    });

    $('input[name="q3"]').change(function(){
        if($(this).val() === 'Yes') {
            $('input[name="q4"]').prop('disabled',false);
            $q4.addClass('f_required');
        }
        else {
            $('input[name="q4"]').prop('disabled',true).prop( 'checked', false );

            $q4.removeClass('f_required');
        }
    });

    $('#save-1').click(function(){
        if(!validateForm()) { return; }

        $('#step-1').addClass('hidden');
        $('#step-2').removeClass('hidden');
        $('div.appointee-info').addClass('active').empty().append('Appointee\'s Name: '+$('#appointee_displayname').val());
    });

    $('#save-2').click(function(){
        $('#step-1, #step-2').addClass('hidden');

        $('#main .mivDialog').append('<div class="goodbye">Do the Next Step</div>');
        $('div.appointee-info').removeClass('active').empty();
    });

    $('#popupformcancel-1').click(function(){
        $('#step-1, #step-2').addClass('hidden');

        $('#main .mivDialog').append('<div class="goodbye">Goodbye</div>');
        $('div.appointee-info').removeClass('active').empty();
    });

    $('#popupformcancel-2').click(function(){
        $('#step-2').addClass('hidden');
        $('#step-1').removeClass('hidden');
        $('div.appointee-info').removeClass('active').empty();
    });

    setUpSalaryTypeSelectEvent();

});
