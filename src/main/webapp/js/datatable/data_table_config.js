'use strict';
/* global Modernizr */
/* exported fnInitDataTable, UP, DOWN, TAB */
//event key codes
var ENTER = 13;
var UP = 38;
var DOWN = 40;
var TAB = 9;

var datatable_help_html = location.protocol + '//' + location.host + '/miv/help/datatable_features.html';

var sSearch = '<span>Find in all :</span>&nbsp;' +
          '<span class="inputtext" title="Enter text to find within displayed result">_INPUT_</span>' +
          '<input class="reset-button ui-icon ui-icon-circle-close" title="Reset the Search" type="button">';

var sSortBy = 'Sort by ';

/**
 * Initializes a datatable object from a DOM table.
 *
 * @param sDatatableId
 *   Unique id of the table to which a datatable should be applied
 *
 * @param oCustomSettings
 *   customize datatable by providing your own configMap
 *   i.e {"iDisplayLength":25}
 *
 *   *Custom parameters*
 *     bHelpBar, boolean, if true, help bar is added to the bottom of the table
 *     aFilterColumns, integer array, provide column filtering set empty array for all columns.
 *
 * @returns
 *   datatable table Object
 */

function fnInitDataTable(sDatatableId, oCustomSettings)
{
    var dtObj;
    if (typeof sDatatableId !== 'string') {
        dtObj = sDatatableId;
        sDatatableId = sDatatableId.attr('id');
    }
    else {
        dtObj = $('#' + sDatatableId);
    }
    oCustomSettings = oCustomSettings || {};

    //default settings for data table
    var oSettings = {
        'bJQueryUI'        : true, /* Enable jQuery UI ThemeRoller support */
        'sPaginationType'  : 'full_numbers',
        'bAutoWidth'       : true, /* Enable or disable automatic column width calculation */
        'bProcessing'      : true, /* Enable or disable the display of a 'processing' indicator when the table is being processed */
        'bSort'            : true, /* Enable or disable sorting of columns */
        'aaSorting'        : [[ 0, 'asc' ]],
        'oLanguage'        : {'sSearch' : sSearch},
        'aLengthMenu'      : [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        'iDisplayLength'   : 25, /* choose the default visible rows count */
        'stateSave'        : Modernizr.history,
        'stateSaveCallback': Modernizr.history ? function(settings, data) {
            var state = window.history.state || {};

            state[sDatatableId] = {
                data : data,
                page : this.api().page()
            };

            window.history.replaceState(state, '');
        } : $.noop,
        'stateLoadCallback': Modernizr.history ? function(settings, data) {
            // returns the state object, page is set in fnInitComplete, search
            // terms are set in fnSetColumnFilter
            if (window.history.state !== null &&
                window.history.state[sDatatableId] !== undefined)
            {
                return window.history.state[sDatatableId].data;
            }

            return null;
        } : $.noop,
        'fnHeaderCallback' : function (nHead) {
                                // add 'sort by' to sortable columns
                                if (oSettings.bSort) {
                                     //add 'sort by' to sortable columns
                                     $('th', nHead).each(function(index, element) {
                                            //header sortable by default
                                            var bSortable = true;

                                            for (var i in oCustomSettings.aoColumnDefs) {
                                                //find column definition for this header index
                                                if (jQuery.inArray(index, oCustomSettings.aoColumnDefs[i].aTargets) !== -1) {
                                                    //get if this column is sortable
                                                    bSortable = oCustomSettings.aoColumnDefs[i].bSortable;

                                                    break;
                                                }
                                            }

                                            //if column is sortable
                                            if (bSortable) {
                                                var sTitle = $(element).attr('title');

                                                //prepend 'Sort by' to title
                                                if (sTitle && sTitle.indexOf(sSortBy) < 0) {
                                                    $(element).attr('title', sSortBy + sTitle);
                                                }
                                            }
                                     });
                                }

                                 //move caption text above table head
                                 var sCaption = $('caption', this).remove().text();
                                 if (sCaption) {
                                       $(nHead).closest('div').children().first().prepend($('<div/>').text(sCaption).addClass('caption'));
                                 }
                             },
        'fnInitComplete'   : function () {
                                this.fnSetFilter();
                                this.fnSetColumnFilter(oSettings , oCustomSettings);
                                if (oCustomSettings.bHelpBar) { this.fnAddHelpBar(); }
                                if (Modernizr.history && window.history.state !== null &&
                                    window.history.state[sDatatableId] !== undefined) {
                                    this.api().page(window.history.state[sDatatableId].page).draw(false);
                                }
                                else {
                                    this.fnDraw();
                                }
                             }
    };

    //recursive merge the custom settings onto the default settings
    if ($.isPlainObject(oCustomSettings))
    {
        $.extend( true, oSettings, oCustomSettings );
    }

    // remove odd and even class because it will automaticly added by datatable plugin
    $('tr', dtObj).removeClass('odd even');

    //make datatable object available to caller
     var oTable = dtObj.dataTable( oSettings );

     return oTable;
}

var sSearchableColumns = 'filter_column';

//events to govern table columns filtering
$.fn.dataTableExt.oApi.fnSetColumnFilter = function( oSettings , oCustomSettings)
{

    if (oCustomSettings.aFilterColumns === undefined) { return; }

    var self = this;
    var dataTableObj = $('#'+self.attr('id'));
    var api = dataTableObj.dataTable().api();

    var tfoot;

    if ($('tfoot', self).size() === 0)
    {
        tfoot = $('<tfoot><tr class="filterrow"></tr></tfoot>');
    }
    else
    {
        tfoot = $('tfoot', self);
        tfoot.prepend('<tr class="filterrow"></tr>');
    }

    $('thead tr th.datatable-column-filter', dataTableObj).each(function(index) {
        //var id = $(this).attr('id').split('-')[1];
        var id = index;

        var headtitle = jQuery.trim($(this).text());
        var filterText = api.column(id*1).search();
        var th = '<input type="text" name="searchcolumn-'+id+'" id="searchcolumn-'+id+'" value="'+(filterText || 'Filter '+headtitle)+'"'+(filterText ==='' ? '' : ' class="active"')+' title="Filter '+headtitle+'">';
        th += '<input type="button" name="resetcolumn-'+id+'" id="resetcolumn-'+id+'" title="Reset the Search for '+headtitle+'" class="reset-button ui-icon ui-icon-circle-close" value="">';
        tfoot.children('tr.filterrow').append('<th><div class="filterdiv">'+th+'</div></th>');
    });
    dataTableObj.append(tfoot);

    var validFiltersCount=0;
    /*
    * Support Filtering by adding class sSearchableColumns
    */
    $('tfoot input[id^="searchcolumn"]', dataTableObj).each(function(i) {
        var id = $(this).attr('id').split('-')[1];
        if (oCustomSettings.aFilterColumns.length === 0 ||
            jQuery.inArray( !isNaN(id)? parseInt(id) : -1 , oCustomSettings.aFilterColumns) !== -1 ||
            jQuery.inArray( id, oCustomSettings.aFilterColumns) !== -1)
        {
            $(this).addClass(sSearchableColumns);
            validFiltersCount++;
        }
        else
        {
            $(this).parents('th').html('');
        }
    });

    // if no filter column exist delete footer columns row
    if(validFiltersCount === 0)
    {
        $('tfoot tr.filterrow', dataTableObj).remove();
        return;
    }

    $('tfoot input[id^="searchcolumn"]', dataTableObj).focus(function() {
        if (this.className === sSearchableColumns) {
            $(this).addClass('active');
            $(this).val('');
        }
    });
    $('tfoot input[id^="searchcolumn"]', dataTableObj).blur(function(i) {
        if (this.value === '') {
            $(this).removeClass('active');
            $(this).val($(this).prop('title'));
        }
    });

    $('tfoot input[id^="searchcolumn"]', dataTableObj).keyup(function() {
        var id = $(this).attr('id').split('-')[1];
        /* Filter on the column (the index) of this element */
        api.column(id * 1).search(this.value).draw();
    });

    $('tfoot input[id^="resetcolumn"]', dataTableObj).click(function() {
        var id = $(this).attr('id').split('-')[1];
        var targatedSearch = $('#searchcolumn-' + id, dataTableObj);
        targatedSearch.removeClass('active');
        targatedSearch.addClass(sSearchableColumns);
        targatedSearch.val(targatedSearch.attr('title'));

        /* Clear the Filter on the column (the index) of this element */
        api.column(id * 1).search('').draw();
    });
};


// events to govern table filtering
$.fn.dataTableExt.oApi.fnSetFilter = function( oSettings )
{
    var self = this;

    var api = $('#' + self.attr('id')).dataTable().api();

    //filter on double clicked word
    $('tbody :not(button):not(:has(*))', this).on('dblclick', function() {
        api.search($.trim($(this).text())).draw();

        //mouse may leave the table when the redraw occurs, but not register the event
        $(this).trigger('mouseleave');
    } );

    //the restet button for this datatable
    var resetButton = $('#' + $(this).attr('id') + '_filter input.reset-button');

    var fnReset = function(event) {
        //stops input submission in forms
        event.preventDefault();

        api.search('').draw();

        //after reset, change focus to find search box
        resetButton.parent().find('input:not(.reset-button)').focus();
    };

    //reset filter on reset button click or focus enter
    resetButton.click(fnReset).keydown(function(event) {
        if (event.keyCode === ENTER) {
            fnReset(event);
        }
    });
};

//adds a help bar on the bottom of each table
$.fn.dataTableExt.oApi.fnAddHelpBar = function(oSettings)
{
    //the data table wrapping div
    var wrapper = this.closest('div');

    //remove rounded corners from bottom tool bar
    var toolbar = wrapper.children().last();
    toolbar.removeClass('ui-corner-bl ui-corner-br');

    //load table help and append it to the wrapper
    wrapper.append(
        $('<div/>').load(datatable_help_html, function() {
            var features = $('.datatable_features', this);

            //Initialize expand/collapse event for help features/tips.
            $('.expand_collapse', features).click(function(event) {
                var featuresButton = $(event.currentTarget);
                var featuresHeader = $('.featuresHeader', features);
                var featuresDescription = $('.featuresDescription', features);
                var featuresTitle = $('.featuresTitle label', features);

                if (featuresButton.hasClass('ui-icon-plusthick')) // do the expand
                {
                    //change plus to minus and change its title
                    featuresButton.removeClass('ui-icon-plusthick');
                    featuresButton.addClass('ui-icon-minusthick');
                    featuresButton.attr('title','Hide Tips and Color Codes');

                    //change the features header title
                    featuresTitle.html('Tips / Color codes:');

                    //remove the rounded corners from the header
                    featuresHeader.removeClass('ui-corner-bl ui-corner-br');

                    //make description visible
                    featuresDescription.removeClass('display-none');
                }
                else if (featuresButton.hasClass('ui-icon-minusthick')) // do the collapse
                {
                    //change minus to plus and change its title
                    featuresButton.removeClass('ui-icon-minusthick');
                    featuresButton.addClass('ui-icon-plusthick');
                    featuresButton.attr('title','Show Tips and Color Codes');

                    //change the features header title
                    featuresTitle.html('Expand to get Tips and Color code details');

                    //add rounded corners to the header
                    featuresHeader.addClass('ui-corner-bl ui-corner-br');

                    //hide the description
                    featuresDescription.addClass('display-none');
                }
            });
        })
    );
};

/* Create an array with the values of all the checkboxes in a column */
$.fn.dataTableExt.afnSortData['dom-checkbox'] = function(oSettings, iColumn)
{
    var aData = [];
    //TODO: This doesn't work more than once. The changes I made just make it work
    // at a level equal to what it currently does in production.
    $( 'td:eq('+iColumn+') input', this.api().rows().nodes() ).each(function() {
        aData.push( this.checked === true ? '1' : '0' );
    });
    return aData;
};

/* Custom sort by html5 "data-raw" attribute */
$.fn.dataTableExt.afnSortData['custom-sort'] = function(oSettings, iColumn)
{
    var aData = [];
    $( 'td:eq('+iColumn+')', oSettings.oApi._fnGetTrNodes(oSettings) ).each(function() {
        aData.push( $(this).data('raw') );
    });
    return aData;
};

/*
 * Function: fnGetHiddenTrNodes
 * Purpose:  Get all of the hidden TR nodes (i.e. the ones which aren't on display)
 * Returns:  array:
 * Inputs:   object:oSettings - DataTables settings object
 */
$.fn.dataTableExt.oApi.fnGetHiddenTrNodes = function(oSettings)
{
    /* Note the use of a DataTables 'private' function thought the 'oApi' object */
    var anNodes = this.oApi._fnGetTrNodes( oSettings );
    var anDisplay = $('tbody tr', oSettings.nTable);

    /* Remove nodes which are being displayed */
    for (var i=0; i<anDisplay.length; i++ )
    {
        var iIndex = jQuery.inArray( anDisplay[i], anNodes );
        if (iIndex !== -1)
        {
            anNodes.splice( iIndex, 1 );
        }
    }
    return anNodes;
};


$.fn.dataTableExt.oApi.fnReloadAjax = function(oSettings) {
    var that = this;

    //perform data replacement after server call
    var callback = function(json) {
        //clear old table data
        that.oApi._fnClearTable(oSettings);

        //load new data
        var aData =  that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ); //jshint ignore:line

        //add new data row-by-row
        for ( var i=0 ; i<json[oSettings.sAjaxDataProp].length ; i++ ) {
            that.oApi._fnAddData( oSettings, json[oSettings.sAjaxDataProp][i] );
        }

        oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
        that.fnDraw();
    };

    //make call to the server
    oSettings.fnServerData(oSettings.sAjaxSource, [], callback, oSettings);
};
