'use strict';
/* global getClickedButton, validateForm */
/*
 * For use with decisionsignature.jsp
 */

var mivFormProfile = {
    constraints: {
        'fieldset.decision' : function(element) {
            return $('input:checked', element).exists() ? true : 'REQUIRED';
        }
    }
};
$(document).ready(function() {
	// print button handler
	$('#printPdf').click(function(event) {
		event.preventDefault();
		window.print();
	});

    var form = $('#signatureForm');

    // form validation
    form.submit(function(event) {
        /*
         * "formnovalidate" attribute on selected submit button, do not validate.
         */
        if (getClickedButton(form).attr('formnovalidate') !== undefined) { return; }

        var formValidation = jQuery.extend(true, {}, mivFormProfile); // to get fresh copy of mivFormProfile

        if (!validateForm(form, formValidation, $('#messagebox'))) {
            event.preventDefault();
        }
    });
});
