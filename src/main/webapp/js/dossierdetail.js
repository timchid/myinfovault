'use strict';
/* global detect, showMessage, qTipObject */
var _navigator = {
    ua : detect.parse(navigator.userAgent)
};
var alertResequencMsg = 'You have made changes on resequencing of letters that you have not yet confirmed.\nIf you navigate away from this page you will lose your unsaved changes.';
var saveConfirmationMsg = 'Do you want to save resequenced letters?';

$(document).ready(function() {
    setContainedStickyScroll('#sticky');
    setupResequence();
    initToolTips();
});


function setContainedStickyScroll(selector, vactive)
{
    vactive = (vactive === null || typeof vactive === 'undefined' || vactive);

    $(selector).containedStickyScroll({
        duration: _navigator.ua.browser.family === 'IE' ? 1 : 0,
        hideClose: false,
        active: vactive
    });
}


/*
 * Setup re-sequencing on Extramural Letter(s).
 * @author Pradeep Haldiya
 * @since MIV 4.6.2
 */
function setupResequence()
{
    // save re-sequenced data
    $('.SaveResequence').click(function() {
        if ( confirm(saveConfirmationMsg) )
        {
            $('.SaveResequence').attr('disabled', 'disabled');
            $('.CancelResequence').addClass('hidden');

            var postData = {
                actiontype : 'save',
                rectype : 'dossier-document',
                subtype : 'none',
                keyVal :  $('form#searchCriteria input[name=dossierId]').val(),
                sectionCount : 0,
                sections : []
             // headers : undefined // headers will never be used for dossier-document reordering
            };

            $( '.miv-sortable' ).each(function(index) {
                var recNumbers = $(this).sortable('toArray');
                if (recNumbers.length > 0) {
                    var sectionSet = {};
                    sectionSet.section = 'dossier-document';
                    sectionSet.records = recNumbers;
                    postData.sections.push(sectionSet);
                }
            });
            postData.sectionCount = postData.sections.length;

            var QryStrValue = 'OpenActions?_eventId=reload&_flowExecutionKey=' + $('#_flowExecutionKey').val();

            // prepare save re-sequencing url
            var url = '/miv/Resequence';

            // Send the data using post and put the results in CompletedMessage div
            $.post(url, postData, function(data) {
              //var resultObj = eval('(' + data + ')');         // FIXME: DANGER Will Robinson! ... Do NOT use eval() !!!
                var resultObj = $.parseJSON(data);              // Use JSON.parse() or $.parseJSON() instead.

                if (resultObj !== undefined && resultObj !== null) {
                    var result = resultObj.success;
                    if (result !== undefined && resultObj !== null /*&& result*/) {
                        showMessage(undefined, '<div id="successbox">' + resultObj.message + '</div>', $('#appointments').position().top);
                        cancelResequence();

                        // modifying history using pushState
                        if (typeof history.pushState === 'function') {
                            var stateObj = {};
                            history.pushState(stateObj, 'dummy', QryStrValue);
                        }
                    }
                }
            });
        }
    });

    // FIXME: Document why this is done for only Mozilla and Opera browsers; and document what the heck this is doing.
    if (_navigator.ua.browser.family === 'Firefox' || _navigator.ua.browser.family === 'Opera') {
        $('input[type="submit"],input[type="button"]:not(.SaveResequence, .recorddel),a:not(.popuplink,.CancelResequence)').click(function(e) {
            // check for resequencing
            if ($('.SaveResequence').is(':visible') && !$('.SaveResequence').is(':disabled'))
            {
                if ( !confirm(alertResequencMsg + '\n\nAre you sure you want to leave this page?') ) {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                }
            }
        });
    }
    else {
        $(window).bind('beforeunload', function(e) {
            // check for resequencing
            if ($('.SaveResequence').is(':visible') && !$('.SaveResequence').is(':disabled')) {
                return alertResequencMsg;
            }
        });
    }

    // Hide the re-sequence button if miv-sortable not exist
    // SDP: If we're hiding the button why did we bother adding a click-handler to it?
    //      Maybe this block should be the first thing done.
    if ($( '.miv-sortable' ).size() === 0) {
        $('.buttonset', $('#summary')).addClass('hidden');
    }
    else {
        // SDP: This seems overcooked.  The page structure is
        //      #summary .... #ResequenceButtons.buttonset > input.ActiveResequence
        //  so $('#ResequenceButtons') and $('.buttonset, $('#summary')) select exactly the same single element.
        //  The .ActiveResequence button is within that and gets shown just by removing 'hidden' from its parent,
        //  but I'll leave that in separately since the button gets hidden if you Resequence, Cancel, and Resequence again.
//        $('#ResequenceButtons').removeClass('hidden');
//        $('.buttonset', $('#summary')).removeClass('hidden');
//        $('.ActiveResequence').removeClass('hidden');
// Replaced the above three lines with the single line here - select both elements at once
// and removeClass on the whole selected list.
        $('#ResequenceButtons, .ActiveResequence').removeClass('hidden');
        // init re-sequencing
        $('.ActiveResequence').click(function() {
            initResequence();
        });
    }
} // setupResequence()


function setSortableBlock()
{
    var winHeight = $(window).height();
    var headerHeight = $('#summary').height();
    var finalHeight = winHeight - headerHeight - 75;

    $('.ui-sortable-block').each(function(index) {
        $(this).css('max-height', finalHeight);
        $(this).css('min-width', '30em');
    });
}


function removeSortableBlock()
{
    $('.ui-sortable-block').each(function(index) {
        $(this).css('max-height', '');
        $(this).css('min-width',  '');
    });
}


function initResequence()
{
    $('.ui-state-disabled').removeClass('ui-state-disabled');

    // arranging section records to perform re-sequencing
    $( '.miv-sortable' ).each(function(index) {
        var key = $(this).attr('id');
        var keyrecords = 'li[rel="' + key + '"]';
        $(this).addClass('ui-sortable-block');

        if ($(keyrecords).size() > 1)
        {
            $(keyrecords).each(function(index) {
                $(this).addClass('ui-state-default');
            });
        }
        else
        {
            $(keyrecords).each(function(index) {
                $(this).addClass('ui-state-disabled');
            });
        }
    });

    // arranging section records to perform re-sequencing
    $( '.miv-sortable li.ui-state-default' ).each(function(index) {
        $(this).addClass('active');
    });

    // applying sorting on section records
    $( '.miv-sortable' ).sortable({
        items: 'li.active:not(.ui-state-disabled)',
        axis: 'y',
        disabled: false,
        placeholder: 'ui-state-highlight',
        update: function(event, ui) {
            $('.SaveResequence').removeAttr('disabled');
        }
    });

    setSortableBlock();

    $(window).resize(function() {
        setSortableBlock();
    });

    $('.ActiveResequence').addClass('hidden');
    $('.CancelResequence').removeClass('hidden');
    $('.SaveResequence').removeClass('hidden');
    $('.SaveResequence').attr('disabled', 'disabled');
} // initResequence()


function cancelResequence()
{
    $( '.miv-sortable' ).each(function(index) {
        var key = $(this).attr('id');
        var keyrecords = 'li[rel="' + key + '"]';

        $(keyrecords).removeClass('ui-state-default active').removeAttr('title').addClass('ui-state-disabled');
    });

    // disabled sorting on section records
    $( '.miv-sortable' ).sortable({
        items: 'li.active:not(.ui-state-disabled)',
        disabled: true
    });

    removeSortableBlock();

    $(window).unbind('resize');

    $('.active').removeClass('active');
    $('.SaveResequence').removeAttr('disabled');
    $('.ActiveResequence').removeClass('hidden');
    $('.CancelResequence').addClass('hidden');
    $('.SaveResequence').addClass('hidden');
} // cancelResequence()


/**
 * Initialization for dossierdetail.jsp tool tips in list elements.
 *
 * @author Craig Gilmore
 * @since MIV 4.7
 */
function initToolTips()
{
    $('span.helpicon').each(
        function() {
            var li = $(this).closest('li');
            // add specific content for the list element
            // qTipObject is available at qtip_config.js
            qTipObject.content = {
                title: { text: li.children('.lastmsg').text() },
                text: li.find('span.help')
            };

            $(this).qtip(qTipObject);
        });
}

/*<span class="vim">
 vim:ts=8 sw=4 et sr:
</span>*/
