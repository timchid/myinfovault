'use strict';
/* global escape, getLabel, enable, disable, validateForm, setDefaultFocus, mivFormProfile */
// SDP Updated 2011-08-17 17:39
// Pradeep K Haldiya (2013-10-22) - removing dojo calls and replacing with jquery calls

/**
 * setup edit form
 */
$(document).ready(function() {
    setTimeout(runEditInit, 300);
});
// ready()


/**
 * get form cancel button, but make sure selected element is actually a button.
 */
function getFormCancelButton()
{
    var $cancelBtn = $('#popupformcancel');
    if (!$cancelBtn.exists()) { $cancelBtn = $('#formcancel'); }
    if (!$cancelBtn.exists()) { $cancelBtn = $('#cancel'); }

    if ($cancelBtn.exists()  && $cancelBtn.prop('nodeName') === 'INPUT') {
        return $cancelBtn;
    }

    return null;
}
// getFormCancelButton()


/**
 * clear url by removing parameters
 */
function cleanUrl(url)
{
    var cleaned = url;

    // if there's a leftover "recid=NNN" strip it off
    cleaned = cleaned.replace(/&recid=\d+/, '');

    // strip any previous completed-message
    //  ? but what to look for to end the pattern? anything not an '&'
    cleaned = cleaned.replace(/&CompletedMessage=[^&]+/, '');
    cleaned = cleaned.replace(/zremoved=[^&]+&/g, '');
    cleaned = cleaned.replace(/&timestamp=[^&]+/, '');
    cleaned = cleaned.replace(/&success=[^&]+/, '');

    return cleaned;
}
// cleanUrl()


/**
 * cancel the form and return back to list page
 */
function cancelled(e)
{
    e.preventDefault();

    var button = e.target;

    if (button.setAttribute) {
        button.setAttribute('disabled', 'disabled');
    }

    var backurl = document.referrer;
    backurl = cleanUrl(backurl);

    if ((backurl.lastIndexOf('PersonalForm') === (backurl.length - 12)) || backurl === '') {
        backurl =  'MIVMain';
    }

    window.location = backurl;
}
// cancelled()


/**
 * Builds the new url.
 * this function knows about the hidden referer node in the edit page.
 */
function buildUrl(recnum, msg, timestamp)
{
    if (typeof recnum === 'undefined' || recnum.length === 0) {
        recnum = 0;
    }

    var $node = $('#referer');
    var href = $node.prop('href');
    var separator = '&';

    if (href.match('.\\?.') === null) {
        separator = '?';
    }

    href = cleanUrl(href);

    if (recnum > 0) {
        href = href + separator + 'recid=' + recnum;
    }

    if (msg !== undefined) {
        href = href + '&CompletedMessage=' + msg + '&timestamp=' + timestamp + '&success=true';
    }

    return href;
}
// buildUrl()


/**
 * perform action after save call
 */
function saveFinished(data, evt)
{
        var saveMsg;
        var timeStamp;
        var resultObj = JSON.parse( data );
        var recid = resultObj.recnum;

        if (recid !== undefined && recid.length === 0) {
            recid = 0;
        }

        if (resultObj.success) {
            saveMsg = escape(resultObj.message);
            timeStamp = resultObj.timestamp;
            var url = '';

            if (resultObj.resultUrl !== undefined) {
                url = resultObj.resultUrl +
                      '?CompletedMessage=' + saveMsg +
                      '&timestamp=' + timeStamp +
                      '&success=true';
            }
            else {
                url = buildUrl(recid, saveMsg, timeStamp);
            }

            window.location = url;
        }
        else {
            if (resultObj.resultUrl) {
               window.location = resultObj.resultUrl;
            }
            else {
                var $saveButton = $('#save');
                var $errorMessage = $('#errormessage');
                var content = '';

                if (resultObj.errorfields) {
                    content = resultObj.errorfields.length > 1 ? 'Errors :' : 'Error :';

                    var errors = resultObj.errorfields;
                    var errormsgs = resultObj.message;
                    content += '<ul>';

                    for (var p=0; p<errors.length; p++) {
                        content += '<li>' + getLabel(errors[p]) + ': ' + errormsgs[p] + '</li>';
                    }

                    content += '</ul>';
                }
                else {
                    content = 'Error(s) :';
                    if ( resultObj.isCustomMsg ) {
                        saveMsg = '<br />'+ resultObj.message + '<br />';
                    }
                    else {
                        saveMsg = '<br>There was an unknown system error while trying to save this record!<br>"' +
                        resultObj.message + '"';
                    }

                    content += saveMsg;
                }

                $errorMessage.html('<div id="errorbox">'+ content + '</div>');
                $errorMessage.css('display','block');
                scrollTo('errormessage');
                enable($saveButton);
                return;
            }
        }
}
// saveFinished()

/**
 * init the enter data form
 */
function runEditInit()
{
    var $findForm = $('form#mytestform');
    var $saveButton = $('#save');
    var $cancelBtn = getFormCancelButton();

    if ($findForm.exists())
    {
        disable($cancelBtn);
        var action = $findForm.attr( 'action' );

        try {
            $findForm.submit(function( event ) {
                event.preventDefault();
                // force to hit save button one time
                disable($saveButton);
                var $errormessage = $('#errormessage');

                if (typeof mivFormProfile !== 'undefined')
                {
                    if (validateForm($findForm, mivFormProfile, $errormessage)) {

                        $.post(action, $findForm.serialize())
                            .done(function(data, status, xhr) {
                                saveFinished(data, event );
                            })
                            .fail(function(jqXHR, status, error) {
                                if (jqXHR.status === 403) {
                                    var reason = /Token [a-z]+/i.exec(jqXHR.responseText);
                                    var s = reason;
                                    if (reason === 'Token Expired') {
                                        s = 'Sorry, your session expired and your data could not be saved. Re-load this form and re-enter your data.';
                                    }
                                    else if (reason === 'Token Missing') {
                                        s = 'Sorry, token is missing and your data could not be saved. Re-load this form and re-enter your data.';
                                    }

                                    if ($errormessage.exists()) {
                                        $errormessage.html('<div id="errorbox">Error : <ul><li>'+s+'</li></ul></div>');
                                    }
                                    $errormessage.css('display','block');
                                    scrollTo('errormessage');
                                }

                                enable($saveButton);
                            });
                    }
                    else {
                        enable($saveButton);
                    }
                }
                else {
                    enable($saveButton);
                }
            });
        }
        catch (exp) {
            enable($saveButton);
        }

        // Hook the cancel button handler for any of 3 ID names
        if ($cancelBtn.exists()) {
            switch ($cancelBtn.prop('type')) {
                case 'submit':
                case 'reset':
                case 'button':
                    $cancelBtn.click(cancelled);
                    break;
                default:
                    // do nothing; the element is not a button.
                    break;
            }
            enable($cancelBtn);
        }

        if ($saveButton.exists()) {
            enable($saveButton);
            setDefaultFocus();
        }
    }
}
// runEditInit()

/*<span class="vim">
 vim:ts=8 sw=4 et sr:
</span>*/
