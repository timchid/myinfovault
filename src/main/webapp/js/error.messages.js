'use strict';
/* exported getErrorMessage */

var errormap = {'INVALID':'error on field.',
                'INVALID_NUMBER':'This is not a valid number.',
                'INVALID_YEAR':'This is not a valid year.',
                'INVALID_MONTH' : 'This is not a valid month.',
                'INVALID_DAY':'This is not a valid day.',
                'REQUIRED':' This field is required.',
                'INVALID_LINK': 'must be a valid web address (e.g. http://example.com/some+page/some_document.pdf).',
                'INVALID_TERM_TYPE': 'This is not a valid term type.',
                'INVALID_INTEGER':'This is not a valid integer.',
                'INVALID_MIN_VALUE':'Field value below the minimum allowed value.',
                'INVALID_MAX_VALUE':'Field value exceeds the maximum allowed value.',
                'INVALID_DATE_FORMAT':'This is not a valid date.',
                'INVALID_DATE_MINRANGE':'must be greater than or equal to January 1, 1900.'
                };

function getErrorMessage(errorkey) {
    if (typeof errorkey !== 'undefined')
    {
        var message = errormap[errorkey];
        if (typeof message!== 'undefined') { return message; }
    }

    return errormap.INVALID;
}
