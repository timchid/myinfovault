// http://blog.echoenduring.com/wp-content/uploads/demos/echo_ContainedStickyScroll/

(function( $ ){

$.fn.containedStickyScroll = function( options ) {
    /*
     * JQuery custom functions are still called on selectors returning no matches.
     */
	if (!this.length) return;
    
    var defaults = {  
            oSelector : this.selector,
            active : true,
            unstick : true,
            easing: 'linear',
            duration: 500,
            queue: false,
            hideClose: false, // to providing option to hide close button.
            closeTop: 0,
            zIndex : 20000,
            closeRight: 0 ,
            borderColor: "#555555"
        };
                  
    options =  $.extend(defaults, options);
    var $getObject = $(options.oSelector);
    
    // wrap the sticky container
    $getObject.wrap('<div class="sticky-wrapper"/>');
    var $stickyWrapper = $getObject.parent('.sticky-wrapper');
    
    // create close link
    $getObject.append('<a class="popuplink scrollFixIt ui-icon ui-icon-closethick ui-state-default ui-corner-all" title="close sticky header">&nbsp;</a>');
    var $closeLink = $getObject.find('a.scrollFixIt');
    
    if (options.active == true) {
        if (options.unstick == true) {
            
            $closeLink.css('position','absolute');
            $closeLink.css('top',options.closeTop + 'px');
            $closeLink.css('right',options.closeTop + 'px');
            $closeLink.css('cursor','pointer');
            $closeLink.css('margin','0.25em');
            $closeLink.css('display','none');
            
            $closeLink.click(function() {
                $getObject.css('box-shadow','');
                $getObject.css('border','');
                $getObject.css('position','');
                $getObject.css('padding','');
                $getObject.removeClass('active');
                $getObject.animate({ top: "0px" },
                    { queue: options.queue, easing: options.easing, duration: options.duration });
                jQuery(window).unbind();
                $getObject.css('top','');
                $closeLink.remove();
            });
        }
        jQuery(window).scroll(function() {
            if (jQuery(window).scrollTop() > ($stickyWrapper.offset().top)) { 
                $closeLink.css('display',options.hideClose == true ? 'none':'block');               
                if (!options.hideClose) {
                    $getObject.css('padding','0.25em 1.5em 0.25em 0.5em');
                }               
                $closeLink.css('z-index',options.zIndex);
                $getObject.css('box-shadow','1px 1px 3px 1px '+options.borderColor);
                $getObject.css('border','1px solid '+options.borderColor);
                $getObject.css('position','relative');
                $getObject.css('z-index',options.zIndex);
                $getObject.addClass('active');
                $getObject.css('top',$stickyWrapper.offset().top);
                $getObject.animate({ top: (jQuery(window).scrollTop() - $stickyWrapper.offset().top) + "px" }, 
                { queue: options.queue, easing: options.easing, duration: options.duration });
            }
            else if (jQuery(window).scrollTop() < ($stickyWrapper.offset().top)) {
                $closeLink.css('display','none');               
                if (!options.hideClose) {
                    $getObject.css('padding','');
                }
                $getObject.css('box-shadow','');
                $getObject.css('border','');
                $getObject.css('position','');
                $getObject.css('z-index','');
                $getObject.removeClass('active');
                $getObject.animate({ top: "0px" },
                { queue: options.queue, easing: options.easing, duration: options.duration });
                $getObject.css('top','');
            }
        });
    }
    else {
        $closeLink.css('display','none');
        if (!options.hideClose) {
            $getObject.css('padding','0.25em 1.5em 0.25em 0.5em');
        }
        $getObject.css('box-shadow','');
        $getObject.css('border','');
        $getObject.css('z-index','');
        $getObject.css('position','');
        $getObject.removeClass('active');
        $getObject.animate({ top: "0px" },
                { queue: options.queue, easing: options.easing, duration: options.duration });
        jQuery(window).unbind();
        $getObject.css('top','');
        $closeLink.remove();
    }   

  };
})( jQuery );