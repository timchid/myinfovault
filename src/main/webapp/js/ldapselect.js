'use strict';
/* global Modernizr, fnDetails, fnGetColumns */
/* exported ldapSelect */

/**
 * @namespace ldapSelect
 * @author Jacob Saporito
 * @author Craig Gilmore
 * @description LDAP DataTable module, for embedding, manipulating, and validating searchldap.jspf
 * @version 1.0
 * @since 4.8.5
 */
var ldapSelect = (function() {
/**
 * @description contains a reference to the outward-facing API.
 * @inner
 */
var self;

var sLdapSelectSingle = 'One user is associated with that email.';
var sLdapSelectMultiple = 'Multiple users are associated with that email. Which were you searching for?';
var sLdapFoundNone = 'No matches were found in the campus directory for the email address ';
var ADDING_AS_STAFF = 'The selected user is an appointee. Appointees may not be added as staff.';

var dtSection;
var ldapData;
var searchSection;
var emailSearchDT;

return {
    /**
     * @description The form profile which can be passed to the MIV validation code to validate the datatable section.
     * @memberof ldapSelect
     * @instance
     * @todo This part is NOT decoupled from the user add account type select, but should be safe to run without the other parts.
     */
    dataTableProfile : {
        constraints : {
            '.ldap-datatable' : function(contents) {
                if ($('tbody > tr.selected', contents).length !== 1) {
                    return 'ERROR_SELECT_USER';
                }
                else {
                    var rowData = $('.dataTable', dtSection).DataTable().row('.selected').data();
                    var primaryRoleType = rowData.user.primaryRoleType || { name : '', description : '' };

                    if (rowData && rowData.user.userId > 0 && primaryRoleType.name !== 'APPOINTEE') {
                        return 'ERROR_USER_EXISTS';
                    }
                    else if (rowData &&
                             primaryRoleType.name === 'APPOINTEE' &&
                             $('#accountTypeSelect input:checked').val() === 'STAFF')
                    {
                        return 'ERROR_ADDING_APPOINTEE_AS_STAFF';
                    }
                }

                return true;
            }
        },
        errormap : {
            'ERROR_SELECT_USER' : 'You must select one user to add.',
            'ERROR_USER_EXISTS' : 'User already exists as <span></span> in <span></span>.',
            'ERROR_ADDING_APPOINTEE_AS_STAFF' : ADDING_AS_STAFF
        }
    },
    /**
     * @description The form profile which can be passed to the MIV validation code to validate the search box section.
     * @memberof ldapSelect
     * @instance
     */
    emailSearchProfile : {
        required : '#email'
    },
    /**
     * @function buildTable
     * @description Builds the data table used to display the ldap search results.
     * @memberof ldapSelect
     * @param {?ldapSelect~select} [select] - Callback called when a row in the ldap search results is selected.
     * @param {?ldapSelect~deselect} [deselect] - Callback called when a row in the ldap search results is deselected.
     * @instance
     */
    buildTable : function(select, deselect) {
        // destroy current email search data table
        if (emailSearchDT) {
            emailSearchDT.serverside('destroy');
        }

        // build email search data table
        emailSearchDT = dtSection.serverside({
            selectMultiple : false,
            ordering : false,
            source : '/miv/DataTables/user/email',
            type : 'user',
            params : {
                email : self.getSearchTerm()
            },
            columns : fnGetColumns(),
            select : function(event, ui) {
                ldapData = ui.data.user;

                if (select instanceof Function) {
                    /**
                     * @callback ldapSelect~select
                     * @description Called when LDAP search results table row is selected
                     */
                    select(event, ui);
                }
            },
            deselect : function(event, ui) {
                if (deselect instanceof Function) {
                    /**
                     * @callback ldapSelect~deselect
                     * @description Called when LDAP search results table row is deselected
                     */
                    deselect(event,ui);
                }
            },
            details : fnDetails,
            init : function(api) {
                // show details for all rows if three or less
                if (api.page.info().recordsTotal < 4) {
                    dtSection.serverside('showDetails', 'tr');
                }

                var statusColumn = api.column('status:name');

                // status column is visible if non-empty data is found
                var found = false;
                $.each(statusColumn.data(), function(index, status) {
                    if (!found) { found = status && status.trim() !== 0; }
                });
                statusColumn.visible(found);
            }
        });
    },
    /**
     * @function getUserData
     * @description Gets the data associated with the selected datatable row.
     * @memberof ldapSelect
     * @instance
     * @returns {Object}
     */
    getUserData : function() {
        return ldapData;
    },
    /**
     * @function getDTSection
     * @description Gets a jQuery object containing the datatable section of searchldap.jspf
     * @memberof ldapSelect
     * @instance
     * @returns {jQuery#Object}
     */
    getDTSection : function() {
        return dtSection;
    },
    /**
     * @function getSearchSection
     * @description Gets a jQuery object containing the search box section of searchldap.jspf
     * @memberof ldapSelect
     * @instance
     * @returns {jQuery#Object}
     */
    getSearchSection : function() {
        return searchSection;
    },
    /**
     * @function getSearchTerm
     * @description Gets the string entered into the search box by the user.
     * @memberof ldapSelect
     * @instance
     * @returns {string}
     */
    getSearchTerm : function() {
        return $(':input', self.getSearchSection()).val();
    },
    /**
     * @function emailValid
     * @description Checks to see whether the text entered into the search box is a valid email address.
     * @memberof ldapSelect
     * @instance
     * @returns {boolean} True if email is valid, else false.
     */
    emailValid : Modernizr.formvalidation ?
        function() {
            return $('#email', searchSection)[0].validity.valid;
        }
        :
        function() {
            return $('#email', searchSection)[0].value.match(/.+@[^.@]+\..+/) !== null;
        },
    /**
     * Initializes the form, attaches event handlers, and prepares local variables for use by the API.
     * Must be called after the document is ready, and before the rest of the API is used.
     * @function initialize
     * @memberof ldapSelect
     * @instance
     */
    initialize : function() {
        self = this;
        dtSection = $('#ldapSelect');
        searchSection = $('#emailSearch');

        // input PLACEHOLDER not supported
        if (!Modernizr.input.placeholder) {
            $(':input', self.getSearchSection()).after('&nbsp;(e.g., name@ucdavis.edu or first.last@ucdmc.ucdavis.edu)');
        }

        /**
         * Handle the ldapSelect data tables draw event. Attached when initialize is called.
         *
         * Note: this handler could also be implemented in the data tables
         * draw callback for the ldapSelect step, but this method was chosen
         * because the draw event handler is passed the $.Event object.
         *
         * @function ldapSelect~anonymous
         * @listens {jQuery.dataTables#draw}
         */
        dtSection.on('draw.dt', function(event, settings) {
            var table = new $.fn.dataTable.Api(settings);
            var info = table.page.info();

            //Add class so that this datatable can be referred to by validation code.
            //For some reason '#ldapSelect .dataTable' doesn't work in the validation code
            //and just a pure .dataTable gets ALL dataTables on the page.
            $('#ldapSelect .dataTable').addClass('ldap-datatable');

            // LDAP selection text based on records found
            $('p.formhelp', dtSection).text(info.recordsTotal > 1 ?
                                        sLdapSelectMultiple :
                                            info.recordsTotal > 0 ?
                                            sLdapSelectSingle :
                                            sLdapFoundNone+'<'+$('input[name=email]').val()+'>');

            // If there are no entries in the datatable, hide the table and disable next.
            if (info.recordsTotal === 0) {
                $('#ldapSelect .dataTable').hide();
                $('button.next', dtSection).prop('disabled', true);
            }
            // If there is only one entry in the table, preselect
            else if (info.recordsTotal === 1 && !table.row('.selected').node()) {
                dtSection.serverside('select', 0, event);
            }

            //manually set the error label. It's not a "real" input field, so a simple label tag isn't correct.
            //https://html.spec.whatwg.org/multipage/forms.html#category-label
            var tableLegend = $('legend', dtSection).text();
            $('table.dataTable', dtSection).data('errorLabel', tableLegend);

            // only show table controls if more than one page
            var controls = $('.dataTables_length,\
                              .dataTables_filter,\
                              .dataTables_info,\
                              .dataTables_paginate', dtSection);
            controls.toggle(info.pages > 1);
        });
    }
};
})();
