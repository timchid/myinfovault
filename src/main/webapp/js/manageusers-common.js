'use strict';
/* exported fnDetails, fnGetColumns */

/**
 * @fileoverview Functions used in multiple manage users scripts.
 * @author Craig Gilmore
 * @since 4.8.5
 */

/**
 * Data tables 'details' callback. Display reason row
 * is unselectable when row details are generated.
 * @function
 * @param {jQuery#Event} event - The event triggering the call of this function.
 * @param {serverside~ui} ui
 * @param {jQuery#Object} ui.container - jQuery object containing the row details DOM elements.
 */
function fnDetails(event, ui) {
    var reasonDisabled = ui.row.data('disabled');

    // display reason the row is disabled if so
    if (reasonDisabled) {
        ui.container.prepend('<p>' + reasonDisabled + '</p>');
    }
}

/**
 * @function
 * @description Data Tables columns definition for the LDAP and fuzzy match tables.
 * @returns {Object[]} The column definition used for user selection tables for the user management pages.
 */
function fnGetColumns() {
    return [
    // visible columns
        { title : 'Last Name', data : 'user.surname', render : function(data) { return '<span>' + data + '</span>'; } },
        { title : 'First Name', data : 'user.givenName' },
        { title : 'Department', data : 'primaryDepartment', defaultContent : 'None', render : function(data) {
            return data === '' ? 'None' : data;
        } },
        { title : 'Action Status', data : 'status', name : 'status' },

        // hidden detail columns
        { title : 'Display Name', data : 'user.displayName', visible : false },
        { title : 'Email', data : 'user.preferredEmail', visible : false },
        { title : 'Action Location', data : 'location', visible : false },
        { title : 'Role', data : 'user', visible : false, defaultContent : 'None', render : function(data, type) {
            if (type === 'display' && data.primaryRoleType) {
                // roles to display
                var roles = [];

                // add primary role
                roles.push(data.primaryRoleType.description);

                // find and add appointee role if haven't already
                if (data.primaryRoleType.name !== 'APPOINTEE') {
                    $.each(data.assignedRoles, function(index, assignedRole) {
                        if (assignedRole.role.name === 'APPOINTEE') {
                            roles.push(assignedRole.role.description);
                            return false;// break
                        }
                    });
                }

                // display comma-delimited
                return roles.join(', ');
            }
        } },

        // hidden columns (retrieved for data only)
        { data : 'user.userId', visible : false },
        { data : 'user.personId', visible : false },
        { data : 'user.primaryRoleType.name', visible : false },
        { data : 'user.primaryRoleType.description', visible : false },
        { data : 'user.primaryRoles[].scope.description', visible : false },
        { data : 'user.primaryRoles[].scope.scopeString', visible : false },
        { data : 'user.assignedRoles[].role.name', visible : false},
        { data : 'user.assignedRoles[].role.description', visible : false},
        { data : 'user.assignedRoles[].scope.scopeString', visible : false},
        { data : 'user.appointments[].scope.description', visible : false},
        { data : 'user.appointments[].scope.scopeString', visible : false}
    ];
}
