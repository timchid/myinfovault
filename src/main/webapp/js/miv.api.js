'use strict';
/* global authHeader */

var miv = miv || {};
var authHeader = authHeader || 'NOT AUTHORIZED'; //jshint ignore:line

(function() {
    function MivAPI() {
        var options = {
            'errorHandling' : true
        };

        var API_PREFIX = '/miv/api';
        var AJAX_HEADERS = {
            'Authorization': authHeader,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        function doAjax(method, apipath, callbacks, data)
        {
            var ajaxOptions = $.extend(callbacks, {
                'method': method,
                'url': API_PREFIX + apipath,
                'headers': AJAX_HEADERS,
                'data' : JSON.stringify(data)
            });

            $.ajax(ajaxOptions)
             .done(function() {
                 miv.errors.clearErrors();
             })
             .fail(function(jqXHR, textStatus, errorText) {
                 if (options.errorHandling) {
                     if (jqXHR.responseJSON && jqXHR.responseJSON.errors) {
                         miv.errors.error(jqXHR.responseJSON.errors);
                     }
                     else {
                        miv.errors.error([
                            method.charAt(0).toUpperCase() +
                            method.slice(1).toLowerCase() +
                            ' failed. Reason: ' + errorText
                        ]);
                     }
                 }
             });
        }

        /**
         * Perform an HTTP POST to the api.
         * @function post
         * @memberof miv.api
         * @param {String} apipath - The path to POST to relative to the api root ('/miv/api')
         * @param {Object} callbacks - An object of callbacks where the property name corresponds to the $.ajax() callback name and the value is the callback itself.
         * @param {Object} data - The data to POST.
         */
        this.post = function(apipath, callbacks, data) {
            doAjax('POST', apipath, callbacks, data);
        };

        /**
         * Perform an HTTP GET to the api.
         * @function get
         * @memberof miv.api
         * @param {String} apipath - The path to GET to relative to the api root ('/miv/api')
         * @param {Object} callbacks - An object of callbacks where the property name corresponds to the $.ajax() callback name and the value is the callback itself.
         * @param {Object} data - The data to GET.
         */
        this.get = function(apipath, callbacks, data) {
            doAjax('GET', apipath, callbacks, data);
        };

        /**
         * Perform an HTTP DELETE to the api.
         * @function delete
         * @memberof miv.api
         * @param {String} apipath - The path to DELETE to relative to the api root ('/miv/api')
         * @param {Object} callbacks - An object of callbacks where the property name corresponds to the $.ajax() callback name and the value is the callback itself.
         * @param {Object} data - The data to DELETE.
         */
        this['delete'] = function(apipath, callbacks, data) {
            doAjax('DELETE', apipath, callbacks, data);
        };

        /**
         * Perform an HTTP PUT to the api.
         * @function put
         * @memberof miv.api
         * @param {String} apipath - The path to PUT to relative to the api root ('/miv/api')
         * @param {Object} callbacks - An object of callbacks where the property name corresponds to the $.ajax() callback name and the value is the callback itself.
         * @param {Object} data - The data to PUT.
         */
        this.put = function(apipath, callbacks, data) {
            doAjax('PUT', apipath, callbacks, data);
        };

        /**
         * Set a configuration option.
         * @function config
         * @memberof miv.api
         * @param {String} option - The name of the option we are setting.
         * @param value - The value to which we wish to set the option.
         */
        this.config = function(option, value) {
            options[option] = value;
        };
    }

    /**
     * Namespace for api interactions. Consolidates api ajax configuration and error handling into one place.
     * @namespace miv.api
     * @memberof miv
     * @author Jacob Saporito
     * @requires {@link miv.errors}
     * @since 5.0
     * @version 1.0
     */
    miv.api = new MivAPI();
})();
