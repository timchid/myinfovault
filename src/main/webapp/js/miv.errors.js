'use strict';

var miv = miv || {};

(function() {
    function MivErrors() {
        var messageElement;
        function getMessageBox() {
            /* message element is either undefined, or not part of the dom anymore */
            if (messageElement === undefined ||
                !$.contains(document.documentElement, messageElement[0])) {
                var contentBox = $('#main, main').first();
                messageElement = contentBox.find('#messagebox');
                if (messageElement.size() === 0) {
                    //messageBox does not already exist.
                    messageElement = miv.dom.tag('div', {
                        'id' : 'messagebox',
                        'style' : 'display: none;'
                    });

                    //Intelligent guess where to put the messagebox
                    var contents = contentBox.children('h1, h2, h3, h4, h5, h6, #form, form');
                    if (contents.size() === 0) {
                        // We have none of the usual elements to put the box under,
                        // so we'll just throw it in at the top.
                        contentBox.prepend(messageElement);
                    }
                    else if (contents.first().is('form, #form')) {
                        //The first relevant element was a form, put the message box before.
                        contents.first().before(messageElement);
                    }
                    else {
                        //The first relevant thing was a header, put the message box after.
                        contents.first().after(messageElement);
                    }
                }
            }

            return messageElement;
        }

        /**
         * Clear the list of errors on the page.
         * @function clearErrors
         * @memberof miv.errors
         */
        this.clearErrors = function()
        {
            getMessageBox().empty().css('display', 'none');
        };

        /**
         * Display a list of errors on the page.
         * @function error
         * @memberof miv.errors
         * @param {string[]} aErrors - An array of error messages to display.
         */
        this.error = function(aErrors)
        {
            var messageBox = getMessageBox();

            //clear errors
            this.clearErrors();

            if (aErrors.length > 0) {
                // Add the errors to the list element
                var errorList = miv.dom.tag('ul');
                for (var i = 0; i < aErrors.length; i++) {
                    errorList.append(miv.dom.tag('li').text(aErrors[i]));
                }

                //create the errorbox structure and append to the message box
                var errorBox = miv.dom.tag('div', {'id' : 'errorbox'})
                              .text('Error: ')
                              .append(errorList);
                messageBox.append(errorBox);

                //make errors visible
                messageBox.css('display', 'block');
            }
        };
    }

    /**
     * Namespace for displaying errors.
     * @namespace miv.errors
     * @memberof miv
     * @author Jacob Saporito
     * @requires {@link miv.dom}
     * @since 5.0
     * @version 1.0
     */
    miv.errors = new MivErrors();
})();
