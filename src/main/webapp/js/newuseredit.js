'use strict';
/* exported editForm */

/**
 * @namespace editForm
 * @author Jacob Saporito
 * @author Craig Gilmore
 * @description User Edit Form module, for embedding, manipulating, and validating usereditform.jspf
 * @version 1.0
 * @since 4.8.5
 */
var editForm = (function() {
//private vars
/**
 * @description After initialization, contains a reference to the outward facing API.
 * @name self
 * @memberof ldapSelect
 * @private
 * @inner
 */
var self = null;
var addAppt = null;
var assignments = null;
var roles;
var roleChecks = null;
var form = null;
var roleText = null;
var roleOptions = null;

//start private functions
/**
 * @function enableJoint
 * @memberof editForm
 * @description Enable editing/viewing of joint appointments.
 * @private
 * @inner
 */
function enableJoint() {
    //enable and show role checkboxes, re-disable candidate checkbox for home dept.
    //.show sets it to display: block, so setting it to inline here.
    if ($('option:selected', roles).is('.FACULTY'))
    {
        roleChecks.css('display', 'inline');
        $('input:not(#candidate0)', roleChecks).prop('disabled', false);
    }

    //reattach joint assignments for Candidates.
    if ($('div.jointline').length === 0)
    {
        $('#assignments').append(assignments)
        .after(addAppt);
    }
}

/**
 * @function disableJoint
 * @memberof editForm
 * @description Disable editing/viewing of joint appointments.
 * @private
 * @inner
 */
function disableJoint() {
    //hide and disable checkboxes. Most staff roles don't need joint assignments, so detaching.
    assignments.detach();
    addAppt.detach();
    roleChecks.hide();
    $('input', roleChecks).prop('disabled', true);
}


/**
 * @function resetLine
 * @memberof editForm
 * @description Clear assignment line
 * @inner
 * @private
 */
function resetLine(line) {
    $(':input:not(:button)', line).val('')
                                  .removeAttr('checked');

    //candidate should be checked for primary
    $('input[name$=".candidate"]', line).prop('checked', true);
}

/**
 * Fill the role select backs with the option elements passed in.
 * If only one option is passed in, instead show it just as text
 *
 * @function fillRoleSelect
 * @memberof editForm
 * @inner
 * @private
 */
function fillRoleSelect(options) {
    var onlyOneOption = options.length === 1;
    var optionDescription = '';

    //show role selection if there are more than 1 option
    roles.empty()
              .toggle(!onlyOneOption)
              .append(options);

    var selectedOption = $(':checked', options);
    if (selectedOption.size === 1) {
        optionDescription = selectedOption.text();
    }
    else if(onlyOneOption) {
        optionDescription = options.text();
    }

    //show the role in normal text only if there is exactly one option
    roleText.text(optionDescription)
            .toggle(onlyOneOption);

    roles.trigger('change');
}
//end private functions

return {
    /**
     * @memberof editForm
     * @type {boolean}
     * @description True if the edit form is initialized, else false.
     * @instance
     */
    initialized : false,
    /**
     * @memberof editForm
     * @type {Object}
     * @description The form profile used to validate the user edit form with the MIV validation code.
     * @instance
     */
    formProfile : {
        required : [ 'role', 'input[name="formLines[0].scope"]', 'scope0' ],
        constraints : {
            'fieldset.roles:visible' : function(contents) {
                if ($(':checked', contents).length === 0) {
                    contents = $(contents);

                    contents.data('errorLabel', 'Role Selection');
                    return 'MUST_MAKE_SELECTION';
                }

                return true;
            }
        },
        errormap : {
            'MUST_MAKE_SELECTION' : 'You must select at least one checkbox for <span class="errorDept"></span>.'
        }
    },
    /**
     * @function filterRoles
     * @description Filters roles in the role dropdown, only showing roles which match the selector.
     * @memberof editForm
     * @param {jQuery#selector} [optionSelector='*'] All roles which match the selector will be shown. If no selector has been passed to the function, it will show all roles.
     * @instance
     */
    filterRoles : function(optionSelector) {
        if (optionSelector === undefined) {
            fillRoleSelect(roleOptions);
        }

        fillRoleSelect(roleOptions.filter(optionSelector));
    },
    /**
     * @function getRoleLabel
     * @description Get the text of the selected role.
     * @memberof editForm
     * @returns {string}
     * @instance
     */
    getRoleLabel : function () {
        return $(':selected', roles).text();
    },
    /**
     * @function getForm
     * @description Get a jQuery object containing the fieldset with the user edit form.
     * @memberof editForm
     * @returns {jQuery#Object}
     * @instance
     */
    getForm : function() {
        return form;
    },
    /**
     * Updates the edit user step with selected user data when user
     * row selected from a DataTables server-side widget table.
     *
     * @function fnEditStep
     * @memberof editForm
     * @param {!Object} data - The data describing the user being edited.
     * @param {string} data.displayName - The edited user's name, as it should be displayed.
     * @param {string} data.userId - The MIV user id of the edited user, or -1 if the person is not a current MIV user.
     * @param {Object} [data.primaryRoleType] - An object describing the primary role of the edited user.
     * @param {string} data.primaryRoleType.name - The name of the edited user's primary role (as listed in the MivRole enum).
     * @param {Object[]} [data.assignedRoles] - An array containing objects describing the roles assigned to the edited user.
     * @param {Object[]} [data.primaryRoles] - An array containing objects describing the primary roles assigned to the edited user.
     * @instance
     */
    fnEditStep : function(data) {
        enableJoint(); //so that appointments will be attached and filled out
        $('> legend', form).text(data.displayName);

        // set hidden user ID in form
        $('input[name$="userId"]').val(data.userId);

        // double pipe these variables as they may be undefined
        var primaryRoleType = data.primaryRoleType || { name : '' };
        var assignedRoles = data.assignedRoles || [];

        roles.val(primaryRoleType.name);
        roles.trigger('change');

        var editingAppointee = primaryRoleType.name === 'APPOINTEE';

        roles.prop('disabled', editingAppointee);
        $('input[type=button].deleteAppointment').toggle(!editingAppointee);
        addAppt.toggle(!editingAppointee);

        $('.assignment', form).each(function(index, element) {
            var assignment = $(element);
            var scope = $('input[name$=".scope"]', assignment);

            var assignedRole = data.primaryRoles[index];
            var appointment = data.appointments[index];

            var scopeDescription = '';
            var scopeValue = '0:0';

            if (assignedRole) {
                scopeDescription = assignedRole.scope.description;
                scopeValue = assignedRole.scope.scopeString;
            }

            if (appointment) {
                scopeDescription = appointment.scope.description;
                scopeValue = appointment.scope.scopeString;
            }

            resetLine(assignment);
            $('input[name$=".candidate"]:not(#candidate0)').prop('disabled', editingAppointee);
            scope.prev().val(scopeDescription).prop('disabled', editingAppointee);
            scope.val(scopeValue);
            scope.closest('div.jointline').toggle(assignment.is('.primary') || assignedRole !== undefined);
        });

        for (var i = 0; i < assignedRoles.length; i++)
        {
            var role = assignedRoles[i].role.name;
            var scope = assignedRoles[i].scope.scopeString;

            switch(role)
            {
            case 'DEAN':
                $('input[value^="'+scope.replace(':ANY', ':')+'"] ~ fieldset.roles input[name$=".dean"]')
                .prop('checked', true);
                break;
            case 'DEPT_CHAIR':
                $('input[value="'+scope+'"] ~ fieldset.roles input[name$=".deptChair"]')
                    .prop('checked', true);
                break;
            case 'CANDIDATE':
                $('input[value="'+scope+'"] ~ fieldset.roles input[name$=".candidate"]')
                .prop('checked', true);
                break;
            default:
                break;
            }
        }
    },

    /**
     * Initializes the form, attaches event handlers, and prepares local variables for use by the API.
     * Must be called after the document is ready, and before the rest of the API is used.
     * @function initialize
     * @memberof editForm
     * @instance
     */
    initialize : function() {
        if (this.initialized) { return; }

        //private var initialization
        self = this;
        addAppt = $('#additem');
        assignments = $('div.jointline');
        roleChecks = $('fieldset.roles');
        roleText = $('#roleText');
        roles = $('#role');
        roleOptions = $('option', roles);
        form = $('#editForm');

        var jointFields = $('div.jointline.assignment input[name$=".scope"]');
        var jointSelector = 'div.jointline:not(:visible)';

        //properly display it, in case it isn't filtered at any point.
        fillRoleSelect(roleOptions);

        $('div.primary.assignment input[name$=".scope"]').suggestion({
            suggestionSource : 'school/department/scoped',
            sDefaultValue    : '0:0'
        });

        jointFields.suggestion({
            suggestionSource : function() {
                if ($('option:selected', roles).is('.FACULTY')) {
                    return 'school/department';
                }

                return 'school/department/scoped';
            },
            sDefaultValue    : '0:0'
        });

        /**
         * @function editForm~_deleteApptClick
         * @description Deletes appointments/assignments and enables the addAppt button.
         * @listens {jQuery#click}
         */
        $('input[type=button].deleteAppointment').click(function() {
            var line = $(this).closest('div.jointline');
            line.slideToggle(function() {
                //move current line to the bottom
                line.detach();
                $('div#assignments').append(line);

                //and clear it's input fields on deleted appointment line.
                resetLine(line);

                addAppt.prop('disabled', $(jointSelector).length === 0);
            });
        });

        /**
         * Handles revealing a new joint line and disabling the add appointment button.
         * @function editForm~_addApptClick
         * @listens {jQuery#click}
         */
        addAppt.click(function() {
            var hiddenLines = $(jointSelector);

            $(this).prop('disabled', (hiddenLines.length === 1));

            hiddenLines.first().slideToggle(true);
        });

        /**
         * Show/hide checkboxes, assignments, buttons, etc. based on the selected role.
         * Set default primary assignments for select roles.
         *
         * @function editForm~_rolesChange
         * @listens {jQuery#change}
         */
        roles.change(function() {
            var scopeDescription;
            var scopeValue;

            //if nothing is selected, select the first option
            //fixes problem where if something was selected and then subsequently removed
            //value would be null, causing the form to render incorrectly.
            if ($(this).val() === null)
            {
                $(this).val($('option:first', roles).val());
                roleText.text(self.getRoleLabel());
            }

            disableJoint();

            switch($(this).val()) {
            case 'SENATE_STAFF':
                scopeDescription = 'Academic Senate';
                scopeValue = '42:500';
                break;
            case 'SYS_ADMIN':
                scopeDescription = 'IET - Application Development';
                scopeValue = '38:313';
                break;
            case 'VICE_PROVOST_STAFF':
                scopeDescription = 'Provost\'s Office - Academic Affairs';
                scopeValue = '24:232';
                break;
            case 'OCP_STAFF':
                scopeDescription = 'Offices of the Chancellor and Provost';
                scopeValue = '41:500';
                break;
            case 'SCHOOL_STAFF':
                //don't enable joint
                break;
            default:
                enableJoint();
                break;
            }

            var scope = $('input[name="formLines[0].scope"]');
            $('#scope0').prop('disabled', false);

            if (scopeDescription && scopeValue) {
                scope.prev().val(scopeDescription);
                scope.val(scopeValue);
                // For those role with a single predefined scope, do not allow any edits
                $('#scope0').prop('disabled', true);
            }

            $('span.appointment').text($('option:selected', roles).is('.STAFF') ?
                                       'Assignment' :
                                       'Appointment');
        });

        self.initialized = true;
    }
};})();
