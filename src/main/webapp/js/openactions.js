'use strict';
/*
 Manage Open Actions page
 Add a confirmation dialog to all of the "Hold" buttons
 Dean's RELEASE/HOLD is attribute #32 so the button IDs are
 all D32-<loopNumber> e.g. if there are 5 appointments
 they will be D32-1 through D32-5 though some could be
 skipped because the joint has returned control to the
 primary.
 VP's RELEASE/HOLD is attribute #33
 Packet Request OPEN/CANCEL is attribute #122
*/

$(document).ready(function() {
	initialize();
	checkDCconfirmation();
});

function initialize()
{
	var documentsToDelete = 0;
	var inputs = document.getElementsByTagName('input');
	var i;
	for (i=0; i<inputs.length; i++)
	{
		var control = inputs[i];
		if (control.id.match(/^D3[23]|^D4[78]|^D7[68]|^D8[0258]|^D9[0247]-\d+$/))
		{
			// IE doesn't even like the "if (console)" part of this.
			//if (console) console.log("attach handler to " + control.id);
			control.onclick = confirmHold;
		}
		else if (control.id.match(/^D122-\d+$/))
		{
			control.onclick = confirmCancel;
		}
		else if (control.id.match(/^A122-\d+$/))
		{
			control.onclick = confirmOpen;
		}
	}

	// limiting to check within the main div
	var links = $('a[href]:visible',$('#main'));

	// Count any pdf documents referenced on the page besides the RAF and dossier itself
	for (i=0; i<links.length; i++)
	{
		var link = links[i];
		if (!link.title.match(/.*Action Form.*/) &&
			!link.href.match(/dossier.pdf/) &&
			link.href.match(/.*\.pdf$/))
		{
			documentsToDelete++;
		}
	}

	// See if we are returning the dossier to the Department, School/College or Academic Senate or
	// sending the dossier to the Archive
	// If we are, then require a confirmation from the user.
	for (i=0; i<links.length; i++)
	{
		var returnControl = links[i];

		if (returnControl.title.match(/^Return.*(Department|School\/College|Vice.Provost|Senate|Post Audit)/) && documentsToDelete > 0)
		{
			returnControl.onclick = confirmReturn;
		}
		else if (returnControl.title.match(/^Send.*Archive/))
		{
			returnControl.onclick = confirmArchive;
		}
		else if (returnControl.title.match(/^Send.*Post Audit/))
		{
			returnControl.onclick = confirmSendToPostAudit;
		}
	}
}

function confirmOpen()
{
	var msg = 'Initiating a new packet request will invalidate any signed Disclosure Certificates. Uploaded documents will remain intact.\n\n' +
		  'Are you sure you want to initiate a new packet request?';
	return confirm(msg);
}

function confirmCancel()
{
	var msg = 'Cancelling the packet request will place this dossier back in process at the Department and cancel the current packet request.\n\n' +
		  'Are you sure you want to continue?';
	return confirm(msg);
}

function confirmHold()
{
	var msg = 'Holding for signature will delete any corresponding decision and signature!\n\n' +
		  'Are you sure you want to continue?';
	return confirm(msg);
}

function confirmReturn()
{
	var msg = 'Returning this dossier will delete any documents/recommendations/decisions added at the current location!\n\n' +
		  'Are you sure you want to continue?';
    return confirm(msg);
}

function confirmArchive()
{
	var msg = 'Sending this dossier to the Archive will finalize the dossier and send documents to EDMS!\n\n' +
		  'Are you sure you want to continue?';
	return confirm(msg);
}

function confirmSendToPostAudit()
{
	var msg = 'Once this dossier is sent to Post Audit it cannot be returned or edited!\n\n'+
		  'Are you sure you want to continue?';
	return confirm(msg);
}

function checkDCconfirmation()
{
	var signedmsg = 'Confirm changes to the dossier\n\n' +
			'A candidate disclosure certificate (CDC) has already been signed.\n' +
			'Changing the dossier by adding or deleting uploaded documents will automatically invalidate the CDC signature\n' +
			'and a new CDC will have to be signed before the dossier can be routed.\n\n' +
			'Are you sure you want to continue?';

	var requestedmsg = 'Confirm changes to the dossier\n\n' +
			'A candidate disclosure certificate has been released for signature. ' +
			'Changing the dossier by adding or deleting uploaded documents will automatically cancel the signature request.\n\n' +
			'A notification email will be sent to the candidate alerting them to the rescission of the signature request.\n\n' +
			'Are you sure you want to continue?';

	// for each appointment check the state of the disclosure certificate to determine if a confirmation dialog is needed
	$('ul.dossieractions').each(function(index, appointment)
	{
		// attribute in this appointment has status of 'SIGNED' or 'SIGNEDIT'
		var bSigned = $('.actions[data-status^="SIGNED"]', appointment).size();

		// attribute in this appointment has status of 'REQUESTED' or 'REQUESTEDIT'
		var bRequested = $('.actions[data-status^="REQUESTED"]', appointment).size();

		if (bSigned)
		{
			$('div.confirm', appointment).click(function (event) {
				return confirm(signedmsg);
			});
		}
		else if (bRequested)
		{
			$('div.confirm', appointment).click(function (event) {
				return confirm(requestedmsg);
			});
		}
	});
}
