'use strict';

/* Used for the Packet Management page, 'packets.jsp' */
$(document).ready(function() {

    // Make all the document titles the same width so
    // the floating buttons appear in the same spot.
	var packetNames = $('#packetlist ol li.file .entry .documentname');
	var widest = packetNames.first().width();
	if (widest === null) widest = 0;
	packetNames.each(function(i, el) {
		if ($(el).width() > widest) {
			widest = $(el).width();
		}
	});
	widest = (1 * widest) + 2; // 2 pixels wider than the widest, and make sure it is typeof 'number'
	packetNames.width(widest);


    // Attach button handlers for Packet

	// Define the confirmation dialog
	var confirmDlg = $('#confirmDialog').dialog({
		modal : true,
		resizable : false,
		autoOpen : false,
		closeOnEscape : false,
		title : 'Generic Title',
		dialogClass : 'no-close',
		open : function(e, ui) {
				var jdlg = $(this).closest('.ui-dialog');
				var actionText = $(e.target).find('#actionName').text();
				jdlg.find('button.action-class .ui-button-text').text( actionText );
				jdlg.find('span.ui-dialog-title').text('Confirm ' + actionText);
			},
		buttons : [
			{
				text    : 'action',
				id      : 'action-button',
				name    : 'action-name',
				'class' : 'action-class',
				click   : function(e) {
						var form = $(e.target).closest('.ui-dialog').find('form');
						var submitter = $(e.target).closest('.ui-dialog').find('form button[type=submit].action-button');
						var top = $(document).scrollTop();
						var left = $(document).scrollLeft();
						form.append( $('<input type="hidden" name="scrollTop" value="' + top + '">') );
						form.append( $('<input type="hidden" name="scrollLeft" value="' + left + '">') );
						form.append( $('<input type="hidden" name="confirm" value="true">') );
						submitter.click();
						$(this).dialog('close');
					}
			},
			{
				text  : 'Cancel',
				click : function(e) {
						$(this).dialog('close');
						$('.packetlist.section ul.mivrecords li').removeClass('selected');
					}
			}
		]
	});

	var addButtons = [
		{
			text : 'Add Packet',
			click : function(e) {
					var form = $(e.target).closest('.ui-dialog').find('form');
					var input = form.find('input[name=packetname]')[0];
					if (Modernizr.formvalidation) {
						input.setCustomValidity('');
						if (form[0].checkValidity()) {
							var submitter = form.find('button[type=submit].action-button');
							if (submitter) submitter.click(); else form.submit();
							$(this).dialog('close');
						}
						else {
							if (input.validity.patternMismatch) {
								input.setCustomValidity("Packet name can not be blank");
							}
							form[0].reportValidity();
						}
					}
				},
			create : function(event) {
					$('form#add').off('submit');
				}
		},
		{
			text  : 'Cancel',
			click : function(e) {
					$(this).dialog('close');
				}
		}
	];

	var submitRename = function(e) {
		e.preventDefault();
		var form = $(e.target).closest('.ui-dialog').find('form');
		var input = $('input[name=packetname]')[0];
		if (Modernizr.formvalidation) {
			input.setCustomValidity('');
			if (form[0].checkValidity()) {
				var packet = new Packet(renameId, input.value);
				packet.save({
					success : function() {
							$('li[data-pkid=' + renameId + '] .documentname').text(packet.packetName);
							$('li[data-pkid=' + renameId + '] .preview em').text('"' + packet.packetName + '"');
							input.value = '';
							addDialog.dialog('close');
						}
				});
			}
			else {
				if (input.validity.patternMismatch) {
					input.setCustomValidity("Packet name can not be blank");
				}
				form[0].reportValidity();
			}
		}
	};

	var renameButtons = [
		{
			text   : 'Rename',
			click  : submitRename,
			create : function(event, ui) {
					$('form#add').submit(function(event) {
						event.preventDefault();
						submitRename(event);
					});
				}
		},
		{
			text  : 'Cancel',
			click : function(e) {
					$('form#add input').val('');
					$(this).dialog('close');
				}
		}
	];

	var addDialog = $('#namePacket').dialog({
		modal     : true,
		resizable : false,
		autoOpen  : false,
		open : function(e, ui) {
				//var form = $(e.target).find('form');
				//var input = form.find('input[name=packetname]')[0];
				//if (input.validity.patternMismatch) {
				//    input.setCustomValidity("Packet name can not be blank");
				//}
				//form[0].reportValidity();
			},
		buttons : addButtons
	});

	var renameId;

	// Attach the handler for the Delete and Duplicate buttons
	$('.file.entry button.delete, .file.entry button.duplicate').click(function(evt) {
		var $this = $(this);
		var pkid = $this.val();
		var pname = $this.closest('.file.entry').find('.packet.documentname').text();

		confirmDlg.find('#confirmed').val(pkid).prop('name', $this.prop('name'));
		confirmDlg.find('#actionName').text($this.text());
		confirmDlg.find('#packetName').text(pname);

		evt.preventDefault();
		confirmDlg.dialog('open');
	});

	// Attach a handler for renaming a packet in-place.
	$('.file.entry button.rename').click(function(e) {
		e.preventDefault();

		renameId = $(e.target).closest('li').data('pkid');

		addDialog.dialog('option', {
			buttons : renameButtons,
			title   : 'Rename the Packet'
		});
		addDialog.dialog('open');
	});

	$('button#addPacket').click(function(e) {
		e.preventDefault();

		addDialog.dialog('option', {
			buttons : addButtons,
			title   : 'Add Packet: Name the Packet'
		});
		addDialog.dialog('open');
	});


	// Make a clicked record 'selected'
	$('.packetlist.section li.selectable').click(function (e) {
		var clearSelection = $(this).hasClass('selected');
		$('.packetlist.section li.selectable').removeClass('selected');
		if (!clearSelection) $(this).addClass('selected');
	});

	$('.viewSubmitted').click(function() {
		window.location.href = $(this).data('href');
	});
});
/* vim:ts=8 sw=8 noet sr:
   EOF -- Don't put anything after this line. */
