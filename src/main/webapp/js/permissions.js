'use strict';
/* exported scopeMatch, permissionMatch, ALL_ROLES */

var ALL_ROLES = [ 'MIV_USER', 'SYS_ADMIN', 'VICE_PROVOST', 'VICE_PROVOST_STAFF', 'PROVOST', 'CHANCELLOR', 'OCP_STAFF',
                  'SENATE_STAFF', 'DEAN', 'SCHOOL_STAFF', 'DEPT_CHAIR', 'DEPT_STAFF', 'DEPT_ASSISTANT', 'CANDIDATE',
                  'DEPT_REVIEWER', 'CRC_REVIEWER', 'ARCHIVE_ADMIN', 'SCHOOL_REVIEWER', 'J_SCHOOL_REVIEWER',
                  'CANDIDATE_SNAPSHOT', 'ARCHIVE_USER', 'APPOINTEE', 'J_DEPT_REVIEWER', 'INVALID_ROLE' ];

function scopeMatch(scope1, scope2)
{
    scope1 = scope1.split(':');
    scope2 = scope2.split(':');
    var length = scope1.length < scope2.length ? scope1.length : scope2.length; //takes the shorter length
    var MATCH_ALL = [ 'ANY', '*' ];

    for (var i = 0; i < length; i++) {
        if ($.inArray(scope1[i], MATCH_ALL) > -1 ||
            $.inArray(scope2[i], MATCH_ALL) > -1) {
                return true;
        }
        else if (scope1[i] !== scope2[i]) {
            return false;
        }
    }

    return true;
}

function permissionMatch(assignedRoles, rolesAndScopes)
{
    var inRole, scope;

    for(var i = 0; i < assignedRoles.length; i++)
    {
        inRole = $.inArray(assignedRoles[i].role, rolesAndScopes.roles) > -1;
        scope = assignedRoles[i].scope;

        for(var j = 0; j < rolesAndScopes.scopes.length; j++)
        {
            if (inRole && scopeMatch(scope, rolesAndScopes.scopes[j])) {
                return true;
            }
        }
    }

    return false;
}
