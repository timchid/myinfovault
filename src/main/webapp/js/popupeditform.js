'use strict';
/* global trim */
/* exported checkLength, checkRequired, checkRegexp, showResponseError */
function updateTips( o, t )
{
    appendError(o.attr('id'),getFieldLabel(o.attr('id')),t);
}

function appendError(id,label,message)
{
    var $popuperrors = $('#popuperrors');

    if ($popuperrors === undefined ||
        $popuperrors === null ||
        $('#popuperrors').html() === null ||
        $('#popuperrors').html() === undefined) {
        $('#tipsbox').before($('<div id="popuperrors" class="mivbox"><div id="errorbox">Error(s):<ul></ul></div></div>'));
    }

    $('#popuperrors ul').append('<li class="'+id+'">'+label+' : '+message+'</li>');
    $('#popuperrors').focus();
}

function removeErrorDiv()
{
    var $popuperrors = $('#popuperrors');

    if ($popuperrors !== undefined &&
        $popuperrors !== null &&
        $('#popuperrors').html() !== null &&
        $('#popuperrors').html() !== undefined) {
        $('#popuperrors').remove();
    }
}

function checkLength( o, n, min, max )
{
    if ( (o.val()).length > max || (o.val()).length < min ) {
        o.addClass( 'errorfield' );
        updateTips(o, 'Length of ' + n + ' must be between ' + min + ' and ' + max + '.' );
        return false;
    }
    else {
        return true;
    }
}

function checkRequired( o )
{
    if ( trim($(o).val()).length <= 0) {
        o.addClass( 'errorfield' );
        updateTips(o, 'This field is required.');
        return false;
    }
    else {
        return true;
    }
}

function checkRegexp( o, regexp, n )
{
    if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( 'errorfield' );
        updateTips(o, n );
        return false;
    }
    else {
        return true;
    }
}

function focusFirstError(fields)
{
    if (typeof fields === 'object') {
        var firstError = false;
        fields.each(function(index) {
            if ( !firstError && $(this).hasClass('errorfield')) {
                firstError = true;
                $(this).focus();
            }
        });
    }
}

function getFieldLabel(objId)
{
    var $label = $('#addelement').contents().find('label[for='+ objId +']');
    var label = objId;

    if ( $label !== undefined &&
         $label !== null)
    {
        if ( $label.attr('errorlabel') !== undefined &&
             $label.attr('errorlabel') !== null) {
            label = $label.attr('errorlabel').replace(':', '');
        }
        else if ( $label.html() !== undefined &&
                  $label.html() !== null &&
                  $label.html() !== '' ) {
            label = $label.html().replace(':', '');
        }
    }
    return label;
}

function showResponseError(fields,errormsgs,allFields)
{
    removeErrorDiv();
    var obj = null;
    for (var p=0; p<fields.length; p++) {
        obj = $('#addelement').contents().find('#'+fields[p]);
        updateTips(obj,errormsgs[p]);
        obj.addClass( 'errorfield' );
    }
    focusFirstError(allFields);
}
