'use strict';
/* exported aTipSize, qTipObject, qTipSelectObject */
var aTipSize = {
		'small'  : ' qtip-small',
		'medium' : ' qtip-medium',
		'large'  : ' qtip-large',
		'xlarge' : ' qtip-xlarge'
};

/* qTip Base config (Don't use this Object it just basic configuration )*/
var qTipBaseObject = {
	prerender: false,
	id: false,
	overwrite: true,
	metadata: {
		type: 'class'
	},
	content: {
		title: {
			button: false
		}
	},
	style: {
		classes: 'miv-qtip qtip-shadow qtip-bright-gold',
		widget: false,
		tip: {
			corner: true,
			mimic: false,
			method: true,
			width: 16,
			height: 16,
			border: 1,
			offset: 0
		}
	},
	position: {
		target: false,
		container: false,
		viewport: $(window),
		adjust: {
			x: 0,
			y: 0,
			method: 'flip flip',
			mouse: true,
			resize: true
		},
		my: 'right center',
		at: 'left center'
	},
	show: {
		target: false,
		event: 'mouseenter',
		effect: true,
		delay: 90,
		solo: true,
		ready: false,
		modal: {
			on: false,
			effect: true,
			blur: true,
			keyboard: true
		}
	},
	hide: {
		target: false,
		event: 'mouseleave',
		effect: true,
		delay: 100,
		fixed: false,
		inactive: false,
		leave: 'window',
		distance: true
	},
	events: {
		render: function(event, api) {
			// Grab the tooltip element from the API
			var tooltip = api.elements.tooltip;

			// creating qtip-content-box over qtip-content div
			var content = $('.qtip-content',tooltip).html();
			$('.qtip-content',tooltip).html('');

			$('<div/>').addClass('qtip-content-box').html(content).appendTo($('.qtip-content',tooltip));
		}
	}
};


/* Use this object if qTip have only text */
var qTipObject = jQuery.extend( true, {}, qTipBaseObject);

/* Use this object if qTip has/have some thing to be clickable like links */
var qTipSelectObject = jQuery.extend( true, {}, qTipBaseObject, { hide: { event: 'unfocus' } });
