'use strict';
/* exported randomImage */
// Randomly loaded image on pages

/*
  Randomly choose an image to use as an element background.
  Images must be named randomImage_<n>.jpg
  where <n> is a number from 1 to 9, or the number provided in the count parameter
  parameters
  - count: number of images to choose from (optional; default is 9)
  - el: id of element to set background (optional; default is "randomImage"
*/
function randomImage(count, el)
{
    if (!count) {
        count = 9;
    }
    if (!el) {
        el = 'randomImage';
    }

    /* Don't change below!! */
    var randomnumber = Math.random();
    var rand = Math.round((count - 1) * randomnumber) + 1;
    var element = document.getElementById(el);
    var imgURL = 'url(/miv/images/randomImage_' + rand +'.jpg)';
    element.style.backgroundImage = imgURL;
    element.style.backgroundRepeat = 'no-repeat';
}

/*<span class="vim">
 vim:ts=8 sw=4 et sm:
</span>*/
