'use strict';
/* global detect, dojo, djConfig, viewTime, dijit */
/* exported setupResequence */
// Handle events on the 'recordlist.jsp' based pages.


// *** Declare and Init global variables
var editDialog = null;
var _navigator = {
    ua : detect.parse(navigator.userAgent)
};

var trapSubmit = false; // use either onSubmit or onClick to trigger dialog
// Trap submit except on IE, then trap click
// This may have to be changed to "trap submit on Mozilla, click on all others"
trapSubmit = ! dojo.isIE;


// We'll get MIV prefs from somewhere real at some point.
// Code it here for now.
var mivPrefs = {};
mivPrefs.usePopups = false;

var alertResequencMsg = 'You have made changes on resequencing of records or sections that you have not yet confirmed.\nIf you navigate away from this page you will lose your unsaved changes.';
var saveConfirmationMsg = 'Do you want to save resequenced records?';
// *** Attach the page load event handler
dojo.addOnLoad( function() { setTimeout(runListInit, 200); } );


function runListInit()
{
    //if (djConfig.isDebug) {
    //    alert('recordlist.js loaded and init() called!');
        var startTime = new Date();
    //}

    /* ------------------------- */
    if (mivPrefs.usePopups) // This is never true, as of v2.0 to v2.3
    {
        // Attach handler to respond to 'Add Record' button clicks
        dojo.connect( dojo.byId('E0000'), 'onclick', doAdd );

        // Attach handler to respond to 'Edit' button clicks
        if (trapSubmit) {
            dojo.connect( dojo.byId('recordlistform'), 'onsubmit', doEdit );
        } else {
            dojo.query('#listarea .recordedit').onclick(doEdit);
        }
    }
    /* ------------------------- */

    // Attach handler to respond to 'Delete' button clicks
    dojo.query('#listarea .recorddel').onclick(doDelete);

    if (djConfig.isDebug) {
        dojo.log.debug('Time to connect events using ' +
                (trapSubmit ? 'submit' : 'click') + ': ' +
                (new Date() - startTime));
    }

    // Clear the highlighting from the "previous" record if one was set.
    setTimeout(
        function() {
            dojo.query('#listarea .previous').removeClass('previous');
        },
        7000 // clear the highlighting after 7 seconds
    );

} // runListInit()


function doEdit(e)      // doEdit never gets called because mivPrefs.usePopups is never true.
{
    // Added as first item Jan 4 2007 because IE was submitting the form even
    // though preventDefault() was called below.
    e.preventDefault();

    var btnPressed;// = undefined;

    if (trapSubmit) {
        // This explicitOriginalTarget is Mozilla only .. need a cross platform method.
        btnPressed = e.explicitOriginalTarget.id;
    }
    else {
        btnPressed = e.target.id;
    }


//    var btnEl = dojo.byId(btnPressed);
    //var recItem = dojo.getParentByType(btnEl, 'LI');
/***
        The creation and use of editedEl is not correct, creating a new DOM node,
        but what we want is to attach a new class "editing" to an existing node
        as in doDelete() below.
        Not going to fix this now because doEdit() is never used, since we're not
        using pop-up editor windows.
 ***/
    var editedEl = document.createElement('editing');
    dojo.query(dojo.byId('li#P' + btnPressed.substring(1))).place(editedEl, 'first');
    //dojo.addClass(recItem, 'editing');

    // later: dojo.removeClass(dojo.getParentByType(btnEl, 'LI'), 'editing');

    /* Create the Dialog widget object.  The createDialog() function
       takes care of creating it only the first time in here. */
    createDialog();

    //why are these here?
    /*var underlay =*/ dojo.query('.dialogUnderlay')[0]; //jshint ignore:line

    //recItem is... undefined here?
    /*var editconnect =*/ dojo.connect(editDialog, 'hide', getDeleteClassFunction(recItem, 'editing')); //jshint ignore:line

    // Mozilla/FF doesn't get scroll events, so resize the dialogUnderlay
    // whenever dialog.checkSize() mucks with it.
//    if (dojo.isMozilla) {
//        var resizeconnect = dojo.connect(editDialog, 'checkSize', getResizer(underlay, dojo.body()));
//    }

    var editorServlet =  dojo.query( dojo.byId('EditorServlet') )[0].className;
    var rectype = dojo.query( dojo.byId('rectype'), 'value' )[0];
    var mivRecEditURL = '/miv' + editorServlet +
        '?' + btnPressed + '=edit' +
        '&rectype='+rectype
        /*+ '&js=true'*/
    ;

    editDialog.setUrl(mivRecEditURL);
    editDialog.show();
} // doEdit()

function doDelete(e)
{
    e.preventDefault();

    var btnPressed = e.target.id;
    //alert('doDelete called with event [' + e + '], button [' + btnPressed + ']');
    var btnEl = dojo.byId(btnPressed);

    var recItem = null;
    var recItemForPdfs = null;
    var preItem = dojo.byId('P'+btnPressed.substring(1));
    if (preItem !== null && preItem !== undefined)
    {
        recItem = dojo.query('li#P' + btnPressed.substring(1))[0];

        // get the section ul node for deleted record's section
        recItemForPdfs = $('#' + 'P' + btnPressed.substring(1)).closest('ul.mivrecords');
    }

    if (preItem === null || preItem === undefined) {
        preItem = btnEl;
    }

    var curtop = 0;

    if (btnPressed.match('^DH')) { // Delete Header Record
        curtop = $('#'+btnPressed).position().top;
    }
    else if (btnPressed.match('^D')) { // Delete Record
        curtop = $('#P' + btnPressed.substring(1)).position().top;
    }

    // var userUpload = testRecItem.id;
    var deleteMessage = 'This record will be permanently deleted.';
    if (recItem === null) {
        var btnClass = btnEl.className;
        var tokens = btnClass.split(' ');
        for (var i=0; i<tokens.length; i++) {
            var className = tokens[i];
            if (className === 'sectionhead') {
                var sectionNode = getParentByClass(btnEl, 'section');
                if (sectionNode !== null && sectionNode !== undefined) {
                    recItem = sectionNode;
                    deleteMessage = 'Header and corresponding records will be permanently deleted.';
                }
                break;
            }
        }
    }

    dojo.addClass(recItem, 'editing');

    var confirmed = confirm(deleteMessage);
    //alert('answer was ' + confirmed);

    if (!confirmed) {
        var removeHighLight = getDeleteClassFunction(recItem, 'editing');
        removeHighLight();
        return;
    }

    // Remove the DOM node, then send a message to the server to delete the actual record.
    recItem.parentNode.removeChild(recItem);

    var deleteServlet =  dojo.query( dojo.byId('DeleteServlet') )[0].className;
    var rectype = dojo.query( dojo.byId('rectype'))[0].value;
    var subtype = dojo.query( dojo.byId('subtype'))[0].value;
    var mivRecDeleteURL = '/miv' + deleteServlet +
        '?' + btnPressed + '=delete' +
        '&rectype=' + rectype +
        '&subtype=' + subtype
    ;

    if (recItemForPdfs !== null && recItemForPdfs !== undefined) {
        if (recItemForPdfs.attr('id').lastIndexOf('upload-records') !== -1 ) { // no need of double checking
            mivRecDeleteURL += '&uploaded=true';
        }
    }

    dojo.xhrGet({
        url: mivRecDeleteURL,
        load: function(response, ioArgs) {
                  showMessage('CompletedMessage', '<div id="deletebox">' + response + '</div>', curtop, true);
              },
        handleAs: 'text'
    });

} // doDelete()


/**
 * Show a message in the given messagediv with given message
 * messageDivID : ID of the div, for null or undefined id's a Dynamic Message div will be created.
 * message : set your custom message or set null to don't replace existing message.
 * curtop : set the current top position of message in numbers (default 160px).
 * fixHeader : true to handle the header otherwise not.
 */
function showMessage(messageDivID, message, curtop, fixHeader)
{
    var distanceFromTop = 300;
    var animateDuration = 1000;

    fixHeader = !(fixHeader === null || typeof fixHeader === 'undefined' || fixHeader === false);

    if (fixHeader) {
        setContainedStickyScroll('#header', false);
    }

    if (curtop === null || typeof curtop === undefined || curtop === 0) {
        curtop = 160;
    }

    if (curtop + 10 > $('#footer').position().top) {
        curtop = curtop - 50;
    }

    // set focus to curtop position
    jQuery('html, body').animate({
        scrollTop : (curtop - distanceFromTop) + 'px'
    }, animateDuration, 'linear');

    var msgDiv = $('#' + messageDivID);

    var dynamicMessage = (msgDiv.size() === 0);
    // create a dynamic message div
    if (dynamicMessage) {
        msgDiv = $('<div/>').addClass('DynamicMessage').appendTo('body');
    }

    msgDiv.removeAttr('style');

    if (message !== null && typeof message !== 'undefined') {
        msgDiv.html(message);
    }

    // set the style to the message div
    msgDiv.css('display', 'block');
    msgDiv.css('opacity', '1');
    msgDiv.css('position', 'absolute');
    if (curtop > 0) {
        msgDiv.css('top', curtop + 'px');
    }

    // create close link
    msgDiv.append('<a class="popuplink message-box ui-icon ui-icon-closethick ui-state-default ui-corner-all" title="close">&nbsp;</a>');
    var $closeLink = msgDiv.find('a.message-box');

    // click on message to close the message
    $closeLink.click(function() {
        hideMessage(msgDiv, dynamicMessage, fixHeader);
    });

    msgDiv.show('slow', setTimeout(function() {
        hideMessage(msgDiv, dynamicMessage, fixHeader);
    }, viewTime)); // viewTime set in mivcommon.js

} // showMessage()


function hideMessage(messageDiv, dynamicMessage, fixHeader)
{
    messageDiv.removeAttr('style').hide();

    // clear the message if its available
    messageDiv.html('');

    // remove a dynamic message div
    if (dynamicMessage) {
        messageDiv.remove();
    }

    if (fixHeader) {
        setContainedStickyScroll('#header');

        jQuery('html, body').animate({
            scrollTop : '-=50px'
        }, 'linear');
    }

    messageDiv.unbind( 'click' );
}


function setContainedStickyScroll(selector, vactive)
{
    vactive = (vactive === null || typeof vactive === 'undefined' || vactive);
    $(selector).containedStickyScroll({
        duration: _navigator.ua.browser.family === 'IE' ? 1 : 0,
        hideClose: false,
        active: vactive
    });
}


function doAdd(e)
{
    e.preventDefault();
    //alert('Add pressed with event [' + e + '][' + e.originalTarget.id + ']');

    createDialog();

    var editorServlet =  dojo.query( dojo.byId('EditorServlet') )[0].className;
    var rectype = dojo.query( dojo.byId('rectype'))[0].value;
    var mivRecAddURL = '/miv' + editorServlet +
        '?E0000=edit' +
        '&rectype='+rectype
    ;

    //alert('mivRecAddURL:[' + mivRecAddURL + ']');
    editDialog.setUrl(mivRecAddURL);
    editDialog.show();

    e.preventDefault();
} // doAdd()


/*
 * Returns a function that will remove the given cssClass from the item passed.
 */
function getDeleteClassFunction(item, cssClass)
{
    function _deleteCssClass() {
        dojo.removeClass(item, cssClass);
    }
    return _deleteCssClass;
}


/*
 * Returns a function that sets the size of the element 'resizeEl'
 * to match the size of 'matchEl'
 */
// function getResizer(resizeEl, matchEl)
// {
//     function _resizer() {
//         var box = dojo.marginBox(matchEl);  //mmn <-- this may not be right
//         dojo.marginbox(resizeEl, box);
//         dojo.style(resizeEl, 'top:0px; left:0px;');
//     }
//     return _resizer;
// }


function createDialog()
{
    if (!editDialog) {
        editDialog = new dijit.Dialog(
                       'dojo:Dialog', {
                           toggle:         'fade',
                           toggleDuration: 200,
                           executeScripts: true,
                           preload:        false,
                           cacheContent:   false,
                           followScroll:   true
                       },
                       dojo.byId('DialogNode')
                    );
    } // !editDialog

} // createDialog()


function getParentByClass(node, classname)
{
    var returnNode = null;
    var myParent = node.parentNode;

    if (myParent === null || myParent === undefined) { return null; }

    //var classes = dojo.getClasses(myParent);
    var classes = dojo.query('.' + classname);
    for (var i=0; i<classes.length; i++) {
        if (dojo.isDescendant(node, classes[i])) {
            returnNode = classes[i];
            break;
        }
    }
    return returnNode;
}


/*
 * Setup re-sequencing on sections and section records.
 * sections [Optional] : boolean --> set true to apply re-sequencing on section headers
 * @author Pradeep Haldiya
 * @since MIV 4.6.2
 */
function setupResequence(sortSections)
{
    sortSections = sortSections || false;
    var keys = [];
    $('li[rel*="sequencekey:"]').each(function(index) {
        if (jQuery.inArray($(this).attr('rel'), keys) === -1) {
            keys.push($(this).attr('rel'));
        }
    });

    // Arranging section records to perform re-sequencing
    $.each(keys, function(index, key) {
        var value = key.replace('sequencekey:', '');
        var $parentUL = $( '#' + value.split(':').shift() + '-records' );
        var $keyrecords = $( 'li[rel="' + key + '"]' );

        var listClass = 'section-records';
        // If there's more than one record in the group then this is a sortable block of records.
        if ($keyrecords.size() > 1) {
            listClass += ' ui-sortable-block';
        }

        $('<li/>').append($('<ul/>').attr('id', value).addClass(listClass).append($keyrecords)).appendTo($parentUL);
    });

    // If sections flag is true and numbers of sections > 1 than we're applying sorting on sections
    if (sortSections && $('li.section', $('.sections')).size() > 1) {
        $('.sections').addClass('ui-sortable-block');

        $('li.section', $('.sections')).each(function(index) {
            $(this).addClass('miv-sortable');
        });
    }


    // Save re-sequenced data
    $('.SaveResequence').click(function() {
        if (confirm(saveConfirmationMsg)) {
            $('.SaveResequence').attr('disabled', 'disabled');
            showHideElements('.CancelResequence', false);

            // Changed from a long URL with GET parameters to a short URL with POST data.
            var postData = {
                    actiontype : 'save',
                    rectype : $('#rectype').val(),
                    subtype : $('#subtype').val(),
                    sectionCount : 0,
                    sections : [],
                    headers : []  // will be filled below only if sections are being rearranged
            };


            // Collect section records to re-sequence
            $('.section-records').each(function(index) {
                var recNumbers = $(this).sortable('toArray');
                if (recNumbers.length > 0) {
                    var sectionSet = {};
                    var sectionName = $(this).attr('id').split(':')[0];

                    sectionSet.section = sectionName;
                    sectionSet.records = recNumbers;

                    postData.sections.push(sectionSet);
                }
            });
            postData.sectionCount = postData.sections.length;


            // Collect sections header to re-sequence
            if (sortSections && $( 'li.section', $('.sections') ).size() > 1)
            {
                $('.sections').each(function(index) {
                    var sectionsRecords = $(this).sortable('toArray');
                    if (sectionsRecords.length > 0) {
                        postData.headers = sectionsRecords;
                    }
                });
            }

            // Prepare save re-sequencing url
            var url = '/miv/Resequence';

            // Send the data using post and put the results in CompletedMessage div
            $.post(url, postData, function(data) {
              //var resultObj = eval('(' + data + ')');         // FIXED: DANGER Will Robinson! ... Do NOT use eval() !!!
                var resultObj = $.parseJSON(data);              // Use native JSON.parse() or jQuery $.parseJSON() instead.

                if (resultObj !== undefined && resultObj !== null) {
                    var result = resultObj.success;
                    if (result !== undefined && resultObj !== null && result)  {
                        showMessage('CompletedMessage', '<div id="successbox">' + resultObj.message + '</div>');

                        // Refresh the page after saving the re-sequence records for IE version less than 9.
                        if (_navigator.ua.browser.family === 'IE' && parseInt(_navigator.ua.browser.major, 10) < 9) {
                            window.location = window.location.href;
                            // window.location = $('#CancelResequence1').attr('href');
                        }
                        else {
                            cancelResequence(sortSections);
                        }
                    }
                }
            });
        }
    });

    // FIXME: Document why this is done for only Mozilla and Opera browsers; and document what the heck this is doing.
    if (_navigator.ua.browser.family === 'Firefox' || _navigator.ua.browser.family === 'Opera') {
        $('input[type="submit"],input[type="button"]:not(.SaveResequence, .recorddel),a:not(.popuplink,.CancelResequence)').click(function(e) {
            // check for resequencing
            if ($('.SaveResequence').is(':visible') && !$('.SaveResequence').is(':disabled')) {
                if (!confirm(alertResequencMsg + '\n\nAre you sure you want to leave this page?')) {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                }
            }
        });
    }
    else {
        window.onbeforeunload = function(e) {
            // check for resequencing
            if ($('.SaveResequence').is(':visible') && !$('.SaveResequence').is(':disabled')) {
                return alertResequencMsg;
            }
        };
    }

    // init re-sequencing
    $('.ActiveResequence').click(function() {
        initResequence(sortSections);
    });

} // setupResequence()


function initResequence(doSections)
{
    doSections = doSections || false;
    // hide the header first
    setContainedStickyScroll('#header', false);

    $('.ui-state-disabled').removeClass('ui-state-disabled');

    $('ul.section-records').each(function(index) {
        if ($('li.records', $(this)).size() > 1) {
            $( 'li.records', $(this)).addClass('miv-sortable ui-state-default').attr('title', 'Drag the record to resequence.');
        }
        else {
            $('li.records', $(this)).addClass('miv-sortable ui-state-disabled');
        }
    });

    // applying sorting on section records
    $('.section-records').sortable({
        items: 'li.records:not(.ui-state-disabled)',
        axis: 'y',
        placeholder: 'ui-state-highlight',
        disabled: false,
        update: function(event, ui) {
            $('.SaveResequence').removeAttr('disabled');
        }
    });

    // if section's flag is true and numbers of sections > 1 than applying sorting on sections
    if (doSections) {
        $('li.section', $('.sections')).each(function(index) {
            if ($(this).hasClass('miv-sortable')) {
                $(this).addClass('ui-state-default').attr('title', 'Drag the section to resequence.');
            }
            else {
                $(this).addClass('ui-state-disabled');
            }
        });

        $('.sections').sortable({
            items: 'li.section:not(.ui-state-disabled)',
            axis: 'y',
            placeholder: 'ui-state-highlight',
            disabled: false,
            update: function(event, ui) {
                $('.SaveResequence').removeAttr('disabled');
            }
        });

        $('li.section').addClass('active');
    }

    setSortableBlock(doSections);

    $(window).resize(function() {
        setSortableBlock(doSections);
    });

    showHideElements('.helplinks');
    $('.ui-sortable').addClass('active');
    $('.miv-sortable').addClass('active');
    showHideElements('.ActiveResequence', false);
    showHideElements('.SaveResequence');
    showHideElements('.CancelResequence');
    $('.SaveResequence').attr('disabled', 'disabled');

    // show the header if required by scroll the screen with the height of header + 5px
    setContainedStickyScroll('#header');
    var headerHeight = $('#header').height() + 5;
    jQuery('html, body').animate({
        scrollTop : '-=' + headerHeight + 'px'
    }, 'linear');
} // initResequence()



function setSortableBlock(sections)
{
    var winHeight = $(window).height();
    var headerHeight = $('#header').height();
    var finalHeight = winHeight - headerHeight - 75;

    if (sections) {
        $('.ui-sortable-block:not(.sections)').each(function(index) {
            $(this).css('max-height', (finalHeight+1) * 3 / 4);
        });

        $('.sections').css('max-height', finalHeight);
    }
    else {
        $('.ui-sortable-block').each(function(index) {
            $(this).css('max-height', finalHeight);
        });
    }
}


function removeSortableBlock()
{
    $('.ui-sortable-block').each(function(index) {
        $(this).css('max-height', '');
    });
    $('.sections').css('max-height', '');
}


function cancelResequence(doingSections)
{
    doingSections = doingSections || false;
    $( 'ul.section-records' ).each(function(index) {
        if ($('li.records', $(this)).size() > 1) {
            $('li.records', $(this)).removeClass('miv-sortable ui-state-default active').removeAttr('title').addClass('ui-state-disabled');
            $(this).removeClass('miv-sortable').addClass('ui-state-disabled');
        }
        else {
            $('li.records', $(this)).removeClass('miv-sortable ui-state-disabled');
        }
    });

    $('li.section', $('.sections')).each(function(index) {
        if ($(this).hasClass('miv-sortable')) {
            $(this).removeClass('ui-state-default active').removeAttr('title').addClass('ui-state-disabled');
        }
    });


    // disabled sorting on sections
    if (doingSections) {
        $('.sections').sortable({
            items: 'li.ui-state-default:not(.ui-state-disabled)',
            disabled: true
        });
    }

    // disabled sorting on section records
    $('.section-records').sortable({
        items: 'li.records:not(.ui-state-disabled)',
        disabled: true
    });

    removeSortableBlock();

    $(window).unbind('resize');

    $('.active').removeClass('active');
    $('.SaveResequence').removeAttr('disabled');
    showHideElements('.SaveResequence', false);
    showHideElements('.CancelResequence', false);
    showHideElements('.ActiveResequence');
    showHideElements('.helplinks', false);
} // cancelResequence()


// function to show or hide DOM elements
// SDP: Shouldn't this just use jQuery ".toggle()" instead?
//      Shouldn't the *callers* just call .show() or .hide() or .toggle() instead of calling this?
//      Is it important that this is done by using the ".hidden" class instead of "display:none"?
function showHideElements(selector, show)
{
    if (show === undefined) { show = true; }
    $(selector).each(function(index) {
        if (show) {
            $(this).removeClass('hidden');
        }
        else {
            $(this).addClass('hidden');
        }
    });
}

/*<span class="vim">
 vim: tabstop=8 shiftwidth=4 expandtab shiftround:
</span>*/
