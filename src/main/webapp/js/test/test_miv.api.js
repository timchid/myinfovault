'use strict';
/* global QUnit, miv */

var errorState = [];
var originalErrorFn = miv.errors.error;

QUnit.module('The miv.api library', {
    beforeEach: function() {
        $.mockjax({
            url: '/miv/api/success'
        });
        $.mockjax({
            url: '/miv/api/error',
            status: 403,
            statusText: 'Bad Request',
            responseText: {
                errors: [
                    'This request failed validation'
                ],
                type: 'error'
            }
        });
        $.mockjax({
            url: '/miv/api/timeout',
            isTimeout: true,
            responseText: {
                type: 'timeout'
            }
        });
    },
    afterEach: function() {
        $.mockjax.clear();
        miv.errors.error = originalErrorFn;
    }
});

QUnit.test('Api Completeness', function(assert) {
    assert.ok(miv.api.post instanceof Function, 'contains a function for posting to our API');
    assert.ok(miv.api.get instanceof Function, 'contains a function for getting from our API');
    assert.ok(miv.api.put instanceof Function, 'contains a function for putting to our API');
    assert.ok(miv.api.delete instanceof Function, 'contains a function for deleting from our API');
});

QUnit.test('Basic functionality (Putting, Posting, Getting, Deleting)', function(assert) {
    var putDone = assert.async();
    var postDone = assert.async();
    var getDone = assert.async();
    var deleteDone = assert.async();

    var fakeData = {
        answer : 42,
        string : 'test string',
        trueFalse : !true
    };

    miv.api.put('/success',
                {
                    success : function() {
                        assert.ok(true, 'put completed and called success callback');
                        putDone();
                    }
                },
                fakeData);
    miv.api.post('/success',
                 {
                     success: function() {
                         assert.ok(true, 'post completed and called success callback');
                         postDone();
                     }
                 },
                 fakeData);
    miv.api.get('/success',
                {
                    success : function() {
                        assert.ok(true, 'get completed and called success callback');
                        getDone();
                    }
                },
                fakeData);
    miv.api.delete('/success',
                   {
                       success : function() {
                           assert.ok(true, 'delete completed and called success callback');
                           deleteDone();
                       }
                   },
                   fakeData);

    assert.expect(4);
});

QUnit.test('Error handling', function(assert) {
    var errorDone = assert.async();
    var timeoutDone = assert.async();

    miv.errors.error = function(errors) {
        errorState = errors;
    };

    miv.api.get('/error', {
        complete : function() {
            assert.deepEqual(errorState,
                             ['This request failed validation'],
                             'API code correctly reporting normal errors from the error object.');

            errorState = [];
            errorDone();
        }
    });
    miv.api.get('/timeout', {
        complete : function() {
            assert.deepEqual(errorState,
                             ['Get failed. Reason: timeout'],
                             'API code correctly reporting timeout errors.');

            errorState = [];
            timeoutDone();
        }
    });

    assert.expect(2);
});
