'use strict';
/* global QUnit, miv */

QUnit.module('The miv.errors library');
QUnit.test('Api Completeness', function(assert) {
    assert.ok(miv.errors.error instanceof Function, 'has a function which shows errors');
    assert.ok(miv.errors.clearErrors instanceof Function, 'has a function which clears errors');
});

QUnit.test('The errors function', function(assert) {
    var messageBox = $('<div id="messagebox" />');
    $('#main').prepend(messageBox);
    miv.errors.error(['First error message', 'Second error message']);

    assert.ok($('div#errorbox').length === 1,
                'creates an errorbox div');
    assert.ok($('#errorbox li').length === 2,
                'with a list of the errors passed as an argument');
    assert.ok($('div#errorbox').parent().is(messageBox),
              'appends it to an existing messagebox div');

    var secondError = 'Error testing round two.';
    miv.errors.error([secondError]);
    assert.ok($('#errorbox li').text() === secondError,
              'contains only the errors from the most recent function call');

    messageBox.remove();
    miv.errors.error(['Error test 3']);

    assert.ok($('#messagebox > #errorbox').length === 1,
                'creates a messagebox div if it doesn\'t exist and appends the errorbox');

    assert.ok($('#main > :first-child').is('#messagebox'),
              'messagebox is at the top of #main, if no forms or headers are present');

    $('#messagebox').remove();
    $('#main').prepend($('<form />'))
              .prepend($('<div id="divider" />'));
    miv.errors.error(['Error test 4']);

    assert.ok($('#divider').next().is('#messagebox'),
              'messagebox is immediately above the first form if the form element \
              exists and precedes any headers that may exist');

    $('#messagebox').remove();
    $('#main').prepend('<h1 id="testheader">A Header</h1>');
    miv.errors.error(['Error test 5']);

    assert.ok($('#testheader').next().is('#messagebox'),
              'messagebox is immediately below the first header if the header \
              element precedes any forms that may exist');

    //clean up our mess
    $('#messagebox, #divider, #testheader, form').remove();
});

QUnit.test('the clearErrors function', function(assert) {
    //initialize the error box
    miv.errors.error(['First error', 'Second error']);
    //and immediately clear it.
    miv.errors.clearErrors();

    assert.ok($('#errorbox').length === 0, 'removes the errorbox from the messagebox, if it exists');
    assert.ok($('#messagebox').is(':not(:visible)'), 'hides the messagebox');

    //clean up
    $('#messagebox').remove();
});
