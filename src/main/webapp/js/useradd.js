'use strict';
/* global editForm, ldapSelect, validateForm, fnDetails, fnGetColumns,
   permissionMatch, removeErrorClasses, editorAssignedRoles */

var ADDING_AS_STAFF = 'The selected user is an appointee. Appointees may not be added as staff.';

var mivAccountTypeProfile = {
    required : 'input[name="accountType"]'
};

var mivFuzzySelectProfile = {
    constraints : {
        'table.dataTable' : function(/*contents*/) {
            var data = $('#fuzzyMatches .dataTable').DataTable().row('.selected').data();

            if ($('#fuzzyMatches tr').is('.selected') &&
                $('#accountTypeSelect input:checked').val() === 'STAFF') {
                return 'ERROR_ADDING_APPOINTEE_AS_STAFF';
            }
            else if (data && !permissionMatch(editorAssignedRoles, {
                'roles' : [ 'SYS_ADMIN', 'DEPT_STAFF', 'SCHOOL_STAFF', 'VICE_PROVOST_STAFF', 'SENATE_STAFF' ],
                'scopes' : new Array(data.user.appointments[0].scope.scopeString)
            })) {
                return 'ERROR_OUTSIDE_SCOPE';
            }

            return true;
        }
    },
    errormap : {
        'ERROR_ADDING_APPOINTEE_AS_STAFF' : ADDING_AS_STAFF,
        'ERROR_OUTSIDE_SCOPE' : 'User is outside of your school or department.'
    }
};

/**
 * Sets up useradd.jsp form validation and Modernizr alternatives.
 *
 * @author Craig Gilmore
 * @since MIV 4.6.1
 */
$(document).ready(function() {
    editForm.initialize();
    ldapSelect.initialize();

    var fuzzyMatchDT;
    var accountType = '';

    //if page is loaded with an option pre-selected, trigger the change handler.
    $('input[name=accountType]:checked').trigger('change');

    var isEditorSenateAdmin = editorAssignedRoles.some(function(entry) {
        if (entry.role === 'SENATE_STAFF') {
            return true;
        }
    });

    var summary  = $('ul#summary');
    //When errors are caught serverside, it reloads the page with the account type
    //pre-selected. Everything else is cleared, but it does not force the account
    //type to be re-selected. Role select isn't filled because the event doesn't fire.
    $('input[type=radio]').prop('checked', false);

    /*
     * Workflow form and step selectors
     */
    var accountTypeSelect = $('#accountTypeSelect');
    var fuzzySelect = $('#fuzzyMatches');
    var formConfirm = $('#confirmation');
    var form = $('#manageUsersForm');
    var cancel = $('button[name="cancel"]', form);
    var errors = $('#errormessages');
    var userData;

    var dialog = $('#fuzzyDialog').dialog({
        modal : true,
        autoOpen : false,
        buttons: [
            {
                text : 'Ok',
                disabled : true,
                click : function(event) {
                    $(this).dialog('close');

                    //deselect the previous row, triggering the deselect callback
                    fuzzyMatchDT.serverside('deselect', 'tr.selected', event);

                    $('tr.dummy', fuzzySelect).addClass('selected');
                    $('button.next').prop('disabled', false);
                }
            },
            {
                text : 'Cancel',
                click : function() {
                    $(this).dialog('close');
                }
            }
        ]
    });
    $('#dialogCheck', dialog).change(function() {
        var isChecked = $(this).is(':checked');
        $('button:contains(Ok)').prop('disabled', !isChecked)
                                .prop('aria-disabled', !isChecked)
                                .toggleClass('ui-state-disabled', !isChecked)
                                .toggleClass('ui-button-disabled', !isChecked);
    });

    function addFakeRowToTable(table, rowContent, clickHandler) {
        var numRows = $('tbody > tr[role=row]', table).length;

        var row = $(
        '<tr class="dummy">' +
            '<td class="dummy-control"></td>' +
            '<td colspan="4">' +
                '<span>' + rowContent + '</span>' +
            '</td>' +
        '</tr>');

        row.addClass(numRows % 2 ? 'even' : 'odd')
           .click(clickHandler)
           .toggle(accountType === 'FACULTY');

        $('tbody', table).append(row);

        return row;
    }

    function fnEditStepWrapper(data) {
        editForm.fnEditStep(data);

        /*
         * If the selected user is an appointee, disable role selection and update
         * the confirmation step form help text to inform the administrator.
         */
        if (data.isAppointee)
        {
            $('.appointeeConversionWarning', formConfirm).html(' Submitting this form will convert this <span>' +
                                                               data.primaryRoleType.description +
                                                               '</span> to a <span></span>.');
        }
        else {
            $('.appointeeConversionWarning', formConfirm).empty();
        }
    }

    function copyToConfirm()
    {
        var rowData = $('.dataTable:first').DataTable().row('.selected').data();

        $('#confirmName').text(rowData.user.displayName);
        $('#confirmRole').text($('#role option:checked').text());
        $('#confirmEmail').text(ldapSelect.getSearchTerm());
        var confirmAppointments = $('#confirmAppointments');
        confirmAppointments.html('');
        var assignmentMap = {};
        
        $('.assignment:visible').each(function(i, assignment) {
            var primaryJoint = i === 0 ? 'Primary' : 'Joint';
            var deanMsg = $('#dean'+i).is(':checked') ? ' (Dean)' : '';
            var chairMsg = $('#deptChair'+i).is(':checked') ? ' (Department Chair)' : '';
            var scope = $('[id^="scope"]', assignment).val();
            
            // Any add to appointments if scope is present and has not already been added
            if (scope !== '' && !(scope in assignmentMap)) {
            confirmAppointments.append(
                '<li>' + primaryJoint + ' School/College - Department: ' +
                scope + chairMsg + deanMsg + '</li>'
            );
            assignmentMap[scope] = scope; 
            }
            // Get rid of the duplicate 
            else {
            	$('[id^="scope"]', assignment).closest('div.jointline').find('input[type=button].deleteAppointment').first().trigger('click');
            }
        });
    }

    /*
     * Workflow container.
     */
    form.workflow({
        create : function(event, ui) {
            // create a summary list item for each step
            ui.steps.each(function(index, step) {
                var summaryItem = $('<li>').text($('legend', step).text())
                                           .appendTo(summary);

                // zeroth is initially the current
                if (!index) {
                  summaryItem.addClass('current');
                }
                // initially hide fuzzy select as it may not be needed
                else if ($(step).is(fuzzySelect)) {
                    summaryItem.hide();
                }
            });

            /*
             * Only allow form submission on final step
             * unless caused by the cancel button.
             */
            form.submit(function(event) {
                if (!ui.current.is(ui.last) && !cancel.is('[clicked]')) {
                    event.preventDefault();
                }

                // So can submit disabled fields
                $('input').prop('disabled', false);
                $('select').prop('disabled', false);
            });
        },
        beforeNext : function(event, ui) {
            //if going forward to the edit step, reset to the datatables data so
            //that you can't go backwards and forwards to avoid scope restrictions.
            if ($(ui.newStep).is(editForm.getForm())) {
                fnEditStepWrapper(userData);
            }

            var mivFormProfile;

            switch($(ui.formerStep).prop('id'))
            {
            case 'accountTypeSelect':
                mivFormProfile = mivAccountTypeProfile;
                break;
            case 'emailSearch':
                mivFormProfile = ldapSelect.emailSearchProfile;
                break;
            case 'ldapSelect':
                mivFormProfile = ldapSelect.dataTableProfile;
                break;
            case 'fuzzyMatches':
                mivFormProfile = mivFuzzySelectProfile;

                // hide fuzzy selection summary list item if none selected (skipped)
                if (!$('tbody > tr', fuzzySelect).is('.selected')) {
                    getSummaryItem(ui.formerCount).hide();
                }

                break;
            case 'editForm':
                mivFormProfile = editForm.formProfile;
               /*
                * Update the form confirmation appointee conversion warning message
                * (if exists) with the role to which the appointee will be converted.
                */
                $('.appointeeConversionWarning > span:nth-of-type(2)', formConfirm).text(editForm.getRoleLabel());
                copyToConfirm();
                break;
            default:
                return true;
            }

            var valid = validateForm(ui.formerStep, mivFormProfile, errors);

            //Fill out the error message if the user they are trying to add already exists.
            if (!valid)
            {
                var errorDepts = $('fieldset.roles.errorfield').siblings('[id^=scope]');
                if (errorDepts.length > 0) {
                    $('span.errorDept').each(function(index, element) {
                        $(element).text(errorDepts[index].value || 'Joint Department');
                    });
                }

                var rowData = $('.dataTable', ldapSelect.getDTSection()).DataTable().row('.selected').data();

                if (rowData)
                {
                    $('.ERROR_USER_EXISTS span:first').text(rowData.user.primaryRoleType.description);
                    $('.ERROR_USER_EXISTS span:nth-of-type(2)').text(rowData.primaryDepartment);
                }
            }

            return valid;
        },
        beforePrevious : function(event, ui) {
            // remove/hide any errors from the step we're leaving
            removeErrorClasses();
            var errormessage = $('#errormessages');
            if (errormessage.exists()) {
                errormessage.html('');
                errormessage.css('display', 'none');
            }
        },
        beforeStep : function(event, ui) {
            // always show summary line for current step
            getSummaryItem(ui.newCount).show();
        },
        step : function(event, ui) {
            var nextButton = $('button.next', this);

            if (ui.newStep.is(accountTypeSelect)) {
                // enable next button if an account type is selected
                nextButton.prop('disabled', !$(':input', accountTypeSelect).is(':checked'));
            }
            else if (ui.newStep.is(ldapSelect.getSearchSection()))
            {
                nextButton.prop( 'disabled', !ldapSelect.emailValid() );
            }
            else if (ui.newStep.is(ldapSelect.getDTSection()))
            {
                nextButton.prop('disabled', !$('tbody tr').is('.selected'));
            }
            else if (ui.newStep.is(fuzzySelect)) {
                /*
                 * If no fuzzy matches are selected, next button will say "Skip".
                 */
                if ($('tbody > tr.selected', ui.newStep).length === 0 &&
                    accountType === 'STAFF') {
                    nextButton.text('Skip');
                }

                $('tr.dummy', fuzzySelect).toggle(accountType === 'FACULTY')
                                          .removeClass('selected');

                nextButton.prop('disabled',
                                !$('tbody tr', fuzzySelect).is('.selected') &&
                                accountType !== 'STAFF');
            }
            else if (ui.newStep.is(editForm.getForm()))
            {
                /*
                 * If the user has the SENATE_STAFF role, force the role selection to SENATE_STAFF
                 */
                if (isEditorSenateAdmin) {
                    editForm.filterRoles('[value="SENATE_STAFF"]');
                }
            }
            else if (ui.newStep.is(formConfirm))
            {
                nextButton.text('Save')
                          .prop('disabled', false);
            }

            // add 'current' class to current step
            getSummaryItem(ui.newCount).addClass('current');
            getSummaryItem(ui.formerCount).removeClass('current');

            // focus to the first enabled field in the current step
            ui.newStep.find('input:enabled, textarea:enabled, select:enabled').first().focus();
            // $(':input:enabled', ui.newStep).not(':hidden').first() might be better, or
            // $(':input:enabled:not(:hidden)', ui.newStep).first()
        },
        change : function(event, ui) {
            var bAppointee;

            // account type selection changed
            if (ui.step.is(accountTypeSelect))
            {
                var selectedAccountType = $(':input:checked', accountTypeSelect);
                accountType = selectedAccountType.val().toUpperCase();

                // update summary list item
                setSummaryItem(ui.count, fnGetLabel(selectedAccountType).text());

                // enable next button if an account type is selected
                $('button.next', this).prop('disabled', !selectedAccountType.size());

                editForm.filterRoles('.' + accountType);
            }
            // email search changed
            else if (ui.step.is(ldapSelect.getSearchSection())) {
                // update summary list item
                setSummaryItem(ui.count, ldapSelect.getSearchTerm());

                ldapSelect.buildTable(
                    //select callback
                    function() {
                        //setup mailto miv-help with name of the person being converted
                        var email = 'miv-help@ucdavis.edu';
                        var subject = 'Cannot Convert Appointee: {name}';
                        var body = '{name} was not matched as an appointee in MIV, but has a current, completed appointment action. ' +
                                   'I need assistance converting {name} to a candidate.';
                        subject = subject.replace(/\{name\}/g, ldapSelect.getUserData().displayName);
                        body = body.replace(/\{name\}/g, ldapSelect.getUserData().displayName);
                        $('a', dialog).prop('href',
                                            'mailto:' + email +
                                            '?subject=' + encodeURIComponent(subject) +
                                            '&body=' + encodeURIComponent(body)
                        );

                        /*
                         * Determine if campus user is an appointee.
                         */
                        bAppointee = false;
                        $.each(ldapSelect.getUserData().assignedRoles, function(index, assignedRole) {
                            bAppointee = assignedRole.role.name === 'APPOINTEE';
                            return !bAppointee;///continue if not found to be an appointee
                        });

                        // update edit step with selected LDAP person data
                        userData = ldapSelect.getUserData();
                        userData.isAppointee = bAppointee;

                        // abandon/skip appointee fuzzy match selection if an appointee is already selected
                        if (bAppointee) {
                            fuzzySelect.addClass('skip');
                            getSummaryItem(fuzzySelect.data('order')).hide();
                            $('button.next', form).prop('disabled', false);
                            return;
                        }

                        // update summary list item
                        setSummaryItem(ldapSelect.getDTSection().data('order'), ldapSelect.getUserData().displayName);

                        // destroy fuzzy matches data table
                        if (fuzzyMatchDT) {
                            fuzzyMatchDT.serverside('destroy');
                        }

                        // build fuzzy matches data table
                        fuzzyMatchDT = fuzzySelect.serverside({
                            selectMultiple : false,
                            ordering : false,
                            source : '/miv/DataTables/user/decisions',
                            params : {
                                personUUID : ldapSelect.getUserData().personId,
                                email      : ldapSelect.getUserData().preferredEmail,
                                role       : 'APPOINTEE'
                            },
                            columns : fnGetColumns(),
                            select : function(event, ui) {
                                // LDAP names supersede those of the selected fuzzy match
                                var fuzzyData = $.extend({}, ui.data.user, {
                                    displayName : ldapSelect.getUserData().displayName,
                                    surname     : ldapSelect.getUserData().surname,
                                    givenName   : ldapSelect.getUserData().givenName
                                });

                                // update summary list item
                                setSummaryItem(fuzzySelect.data('order'), ui.data.user.displayName);

                                // appointee fuzzy match next button will say "Next", and never be disabled when something is selected
                                $('button.next', form).text('Next')
                                                      .prop('disabled', false);

                                // update edit step with selected fuzzy-matched, appointee person data
                                userData = fuzzyData;
                                userData.isAppointee = true;
                            },
                            deselect : function(/*event, ui*/) {
                                // reset summary list item
                                setSummaryItem(fuzzySelect.data('order'), '');

                                // appointee fuzzy match next button will say "Skip"
                                if (accountType === 'STAFF') {
                                    $('button.next', form).text('Skip');
                                }
                                else {
                                    $('button.next', form).prop('disabled', true);
                                }

                                // update edit step with selected LDAP person data
                                userData = ldapSelect.getUserData();
                                userData.isAppointee = bAppointee;
                            },
                            details : fnDetails,
                            draw : function(api) {
                                var info = api.page.info();

                                $('button.next', form).prop('disabled', false);

                                var tableLegend = $('legend', fuzzySelect).text();
                                $('table.dataTable', fuzzySelect).data('errorLabel', tableLegend);

                                // only show table controls if more than one page
                                var controls = $('.dataTables_length,\
                                                  .dataTables_filter,\
                                                  .dataTables_info,\
                                                  .dataTables_paginate', fuzzySelect);
                                controls.toggle(info.pages > 1);

                                addFakeRowToTable($('.dataTable', fuzzySelect),
                                    ldapSelect.getUserData().displayName + (api.data().length === 0 ?
                                                                            ' doesn’t appear to be an appointee in MIV.' :
                                                                            ' is not one of the people listed above.'),
                                    function() {
                                        if ($(this).is('.selected')) {
                                            $(this).removeClass('selected');
                                            $('button.next').prop('disabled', true);
                                            $('input[type=checkbox]', dialog).click();
                                        }
                                        else {
                                            dialog.dialog('open');
                                        }
                                    }
                                );
                            },
                            init : function(api) {
                                var recordsTotal = api.page.info().recordsTotal;

                                // skip appointee fuzzy selection if none
                                if (recordsTotal === 0 &&
                                    accountType === 'STAFF') {
                                        fuzzySelect.addClass('skip');
                                        getSummaryItem(fuzzySelect.data('order')).hide();
                                    }
                                    else {
                                        fuzzySelect.removeClass('skip');
                                        getSummaryItem(fuzzySelect.data('order')).show();

                                        // show details for all rows if three or less
                                        if (recordsTotal < 4) {
                                            fuzzySelect.serverside('showDetails', 'tr');
                                        }
                                    }
                                }
                            }
                        );
                    },//end select callback
                    //start deselect callback
                    function() {
                        // reset summary list item
                        setSummaryItem(fuzzySelect.data('order'), '');

                        // appointee fuzzy match next button will say "Skip"
                        if (accountType === 'STAFF') {
                            $('button.next', form).text('Skip');
                        }
                        else {
                            $('button.next', form).prop('disabled', true);
                        }

                        // update edit step with selected LDAP person data
                        userData = ldapSelect.getUserData();
                        userData.isAppointee = bAppointee;
                    }
                );
            }
        }
    });

    form.on('click', 'button.next:contains("Save")', function() {
        form.submit();
    });

    // disable/enable next button on invalid/valid email address
    $('#email').on('keyup paste',
                   function() { $('button.next').prop( 'disabled', !ldapSelect.emailValid() ); });

    /*
     * Get the summary list item for the given step index.
     */
    function getSummaryItem(index) {
      return $('li:eq(' + index + ')', summary);
    }

    /*
     * Update the summary list item at the given index with the given text.
     */
    function setSummaryItem(index, text) {
        var summaryLine = getSummaryItem(index);

        $('span', summaryLine).remove();

        if (text) {
            summaryLine.append($('<span>').text(': ' + text));
        }
    }

    editorAssignedRoles.some(function(entry) {
        if (entry.role === 'SENATE_STAFF') {
            $('input[name=accountType][value=STAFF]').click();
            var facRadio = $('input[name=accountType][value=FACULTY]');
            var facLabel = fnGetLabel(facRadio);
            facLabel.html('Senate Admins can only create other Senate Admin <em>Staff</em> accounts.<br>Please select <em>"Next"</em><br>');
            /*var oldFacRad =*/ facRadio.detach();
            return true;
        }
    });
});

/*
 * Get the label corresponding to the given form field.
 */
function fnGetLabel(element) {
    if (!element || !element.size()) { return $(); }

    var label = element.parent().find('label[for="' + element.prop('id') + '"]');

    return label.size() > 0 ? label : element.closest('label');
}
