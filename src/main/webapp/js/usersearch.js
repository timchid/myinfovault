/* global key */
'use strict';

$(document).ready(function() {
    var container = $('.account-search');
    var searchBox = $('input[name=account]', container);
    var results = $('<div>').appendTo(container);
    var resultsDT;

    function initiateDT(event) {
        event.preventDefault();
        var term = $('input[name=account]').val();

        if (term === '' && miv && miv.errors) {
            searchBox.addClass('errorfield');
            miv.errors.error(['You must enter a search term to find the relevant candidates.']);
            $('#errorbox li').addClass('searchError');

            return;
        }
        else if ($('#errorbox li').hasClass('searchError')) {
            miv.errors.clearErrors();
            searchBox.removeClass('errorfield');
        }

        if (resultsDT) {
            resultsDT.serverside('destroy');
            resultsDT = false;
        }

        resultsDT = results.serverside({
            bDetails : false,
            selectMultiple: false,
            source : '/miv/DataTables/candidates',
            columns : [
                { title : 'Account', data : 'sortName', name : 'sortName' },
                { title : 'School/College', data : 'primaryAppointment.scope.schoolDescription' },
                { title : 'Department', data : 'primaryAppointment.scope.departmentDescription' },
                { data : 'userId', name : 'userId', visible : false }
            ],
            params : {
                name : term,
                scoped: true
            },
            draw : function (api) {
                $('.dataTable', api.table().container()).data('errorLabel', 'Candidate Selection');
            }
        });
    }

    searchBox.keypress(function(event) {
        if (event.which === key.ENTER) {
            initiateDT(event);
        }
    });

    $('#searchbutton').click(initiateDT);
});
