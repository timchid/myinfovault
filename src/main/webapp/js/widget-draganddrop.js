'use strict';

(function() {
    /**
     * @fileOverview Drag and drop widget widget.
     * @author Jacob Saporito
     * @version 1.0
     * @namespace dragdrop
     * @memberof jQuery.custom
     * @example
     * //apply drag and drop widget to 'div.container'. All container children
     * //will be draggable.
     * $('div.container').dragdrop();
     *
     * @see {@link http://jqueryui.com/widget/|jQuery UI Widget Factory}
     */

    var outside = 0;
    var draggables = [],
        dropzones = [];

    var dragOpts = {
        revert: true,
        revertDuration: 50
    };

    var self;
    /**
     * @function
     * @memberof dragdrop
     * @param {jQuery#Object} draggable - jQuery object containing the draggable.
     * @param {jQuery#Object} target - jQuery object containing the droppable container to check against.
     * @returns {boolean} True if target contains parent, else false.
     * @private
     * @inner
     */
    function isOriginalSource(draggable, target) {
        return draggable.closest(target).length >= 1;
    }

    /**
     * @function
     * @memberof dragdrop
     * @param {jQuery#Object} draggable - jQuery object containing the draggable.
     * @param {jQuery#Object} target - jQuery object containing the droppable container to check against.
     * @returns {boolean} True if target contains the draggable, or a copy of the draggable.
     * @private
     * @inner
     */
    function draggableInTarget(draggable, target) {
        var isContained = false;

        target.children().each(function(_index, element) {
            isContained = $(element).text() === draggable.text();

            //Break out of loop when a match is found.
            return !isContained;
        });

        return isContained;
    }

    /**
     * @function
     * @memberof dragdrop
     * @description Make a clone of the draggable and append it to the target.
     * @param {jQuery#Object} draggable - jQuery object containing the draggable.
     * @param {jQuery#Object} target - jQuery object containing the droppable container to append the clone to.
     * @private
     * @inner
     */
    function cloneDraggableTo(draggable, target) {
        var clone = draggable.clone();

        clone.removeClass('ui-draggable-dragging')
             .css('left', '')
             .css('top', '')
             .draggable(dragOpts)
             .data(draggable.data())
             .appendTo(target);

        draggables.push(clone);
    }

    /**
     * @function
     * @memberof dragdrop
     * @description Remove draggable from the DOM.
     * @param {jQuery#Object} draggable - jQuery object containing the draggable to remove.
     * @private
     * @inner
     */
    function removeDraggable(draggable) {
        var index = draggables.indexOf(draggable);
        draggable.remove();

        if (index !== -1) {
            //delete the draggable from draggables
            draggables.splice(index, 1);
        }
    }

    /**
     * @function
     * @memberof dragdrop
     * @description Remove draggable from the DOM.
     * @param {jQuery#Object} draggable - jQuery object containing the draggable.
     * @private
     * @inner
     */
    function getData(draggable) {
        return draggable.data();
    }

    $.widget('custom.dragdrop', {
        /**
         * @description widget options
         * @memberof dragdrop
         * @property {boolean} [allowDuplicates=false] Allow multiples of the same item to be dropped into a single dropzone
         * @property {boolean} [duplicate=true] Duplicate draggable if dropped into a different dropzone than it originated in.
         * @property {boolean} [deleteOutside=true] Delete draggable if it is dropped outside of a dropzone.
         * @property {boolean} [placeholderGlow=false] Experimental. Show a placeholder glow where the item will be dropped.
         * @property {Function} [isDuplicate=custom.dragdrap~draggableInTarget] Callback used to determine if a draggable is being dropped into a container where it already exists.
         * @property {Function} [getData=custom.dragdrap~getData] Callback used to extract data from a draggable.
         * @property {Function} [beforeDrop] Callback called before draggable is dropped. Return false to prevent dropping.
         * @property {Function} [drop] Callback called after draggable is dropped.
         * @property {Function} [beforeDelete] Callbock called before draggable is deleted. Return false to prevent deleting.
         * @property {Function} [delete] Callback called after draggable is deleted.
         */
        options: {
            allowDuplicates: false,
            duplicate: true,
            deleteOutside: true,
            placeholderGlow: false,
            isDuplicate: draggableInTarget,
            getData: getData,
            beforeDrop: $.noop,
            drop: $.noop,
            beforeDelete: $.noop,
            'delete': $.noop,
            dragstart: $.noop
        },
        /**
         * Drag and drop widget initialization.
         * @function _create
         * @memberof dragdrop
         * @private
         */
        _create: function() {
            self = this;

            var destinationOnly = this.element.hasClass('destinationonly');

            dropzones.push(this.element);

            this.element
                // note: even if sourceonly, we need to make it a droppable, so we can
                // monitor for over and out events. We may not want to delete the
                // draggable if a user attempts to drag it onto a source only.
                // Instead, the cursor should reflect that it can't be dropped there.
                .droppable({
                    drop: function(event, ui) {
                        var draggable = $(ui.draggable),
                            target = $(event.target);

                        $('.placeholder-glow', target).remove();

                        draggable.css('cursor', '');

                        if (self.options.beforeDrop(draggable, target) === false) { return; }

                        //if target is not the original source of the draggable
                        // and the target accepts draggables
                        // and the draggable is not being dragged from a destinationonly
                        // and either duplicates are allowed, or the item is not already in the target.
                        if (!isOriginalSource(draggable, target) &&
                            !target.hasClass('sourceonly') &&
                            !draggable.parent().hasClass('destinationonly') &&
                            (self.options.allowDuplicates ||
                            !self.options.isDuplicate(draggable, target))) {
                            cloneDraggableTo(draggable, target);

                            if (!self.options.duplicate) {
                                removeDraggable(draggable);
                            }

                            self.options.drop(draggable, target);
                        }
                    },
                    over: function(event, ui) {
                        outside = 0;

                        var draggable = $(ui.draggable),
                            target = $(event.target);

                        draggable.css('cursor', '');

                        if (!isOriginalSource(draggable, target)) {
                            if (self.options.placeholderGlow &&
                                !target.hasClass('sourceonly')) {
                                $('<div></div>').css('height', draggable.outerHeight())
                                                .css('width', draggable.outerWidth())
                                                .addClass('placeholder-glow')
                                                .appendTo(target)
                                                .css('opacity', '1');
                            }

                            if (target.hasClass('sourceonly') ||
                                draggable.parent().hasClass('destinationOnly') ||
                                (!self.options.allowDuplicates &&
                                self.options.isDuplicate(draggable, target))) {
                                draggable.css('cursor', 'no-drop');
                            }
                            else if (self.options.duplicate) {
                                draggable.css('cursor', 'copy');
                            }
                        }
                    },
                    out: function(event, ui) {
                        outside = 1;

                        var target = $(event.target),
                            draggable = $(ui.draggable);

                        $('.placeholder-glow', target).remove();

                        if (!self.options.deleteOutside) {
                            draggable.css('cursor', 'no-drop');
                        }
                        else {
                            draggable.css('cursor', '');
                        }
                    },
                    deactivate: function(event, ui) {
                        var draggable = $(ui.draggable);

                        draggable.css('cursor', '');

                        if (self.options.beforeDelete(draggable) === false) { return; }

                        //delete if dragged out of drop zone, if that option is enabled
                        //AND if it isn't coming from a sourceonly zone.
                        if (self.options.deleteOutside &&
                            outside === 1 &&
                            !draggable.parent().hasClass('sourceonly')) {
                            removeDraggable(draggable);
                            self.options.delete(draggable);
                        }
                    }
            });

            if (!destinationOnly) {
                draggables.push(this.element.children().draggable(dragOpts));
            }
        },

        /**
         * @function _destroy
         * @description Destroys this widget.
         * @memberof dragdrop
         * @private
         */
        _destroy: function() {
            $.each(draggables, function(_index, draggable) {
                draggable.draggable('destroy');
            });
            draggables = [];

            $.each(dropzones, function(_index, droppable) {
                droppable.droppable('destroy');
            });
            dropzones = [];
            outside = 0;
        },
        /**
         * @function getData
         * @description Gets javascript object representing the drop zones and data contained therein.
         * @memberof dragdrop
         * @returns {Object[]} Array of objects representing the drop zones,
         * which has the id of the drop zone, and an array containing the data
         * attached to the draggables contained within the drop zone.
         */
        getData: function() {
            var data = [];

            $.each(dropzones, function(_index, droppable) {
                droppable = $(droppable);
                var droppableData = {
                    dragdropId: droppable.data('dragdropId'),
                    id: droppable.attr('id'),
                    data: []
                };

                $('.ui-draggable', droppable).each(function(_index, draggable) {
                    droppableData.data.push(self.options.getData($(draggable)));
                });

                data.push(droppableData);
            });

            return data;
        }
    });
})(jQuery);
