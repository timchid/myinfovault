'use strict';
/* globals htmlDecode */

$(function() {
    var sCategoryClass = 'custom-suggestion-category';

    var searchType = 'text';

    // See if this browser supports input[type=search]
    // Could use Modernizr for this but that creates a depency that we don't want.
    try {
        var node = document.createElement('input');
        node.setAttribute('type', 'search');
        if (node.type !== 'text') {
            searchType = 'search';
        }
    }
    catch (e) {
        // leave searchType as "text"
        console.warn('Exception when testing browser support: ' + e);
    }

    /**
     * @fileOverview Extension to JQuery auto-complete widget for getting suggestions from paths relative to "/miv/suggest/".
     * @author Craig Gilmore
     * @version 1.0
     * @namespace suggestion
     * @memberof jQuery.custom
     * @extends jQuery.ui.autocomplete
     * @example
     * // Enhance an MIV user ID input field with a user name auto-complete.
     * $("input[name=mivUserID]").suggestion({
     *      suggestionSource : "person/name",
     *      minLength        : 3,
     *  });
     *
     * // Enhance an input field with multiple suggestion categories
     * $("input#personID").suggestion({
     *     suggestionSource : [{ label     : "MyInfoVault",
     *                           name      : "mivUserID",
     *                           path      : "person/name",
     *                           minLength : 3 },
     *
     *                         { label     : "Campus",
     *                           name      : "ldapPersonUUID",
     *                           path      : "ldap/cn",
     *                           minLength : 4 }]
     * });
     */
    $.widget('custom.suggestion', $.ui.autocomplete, {
        /**
         * @member suggestion#options
         * @description Widget options
         * @property {Number}          [minLength=2]          Minimum input text length retrieving a suggestion set
         * @property {String}          [sDefaultValue]        Default value bound to the form field upon suggestion selection
         * @property {String|Object[]} source                 Suggestion server path relative to "/miv/suggest/" or define suggestion groups with an array of maps
         * @property {String}          source[].label         Group label
         * @property {String}          [source[].name]        Form field binding name for group
         * @property {String}          source[].path          Suggestion server path relative to "/miv/suggest/"
         * @property {Number}          [source[].minLength=0] Minimum input text length retrieving a suggestion set for the group
         */
        options  : {
            suggestionSource : [],
            minLength        : 2,
            sDefaultValue    : '0'
        },

        /**
         * @private
         * @function suggestion#_create
         * @description Initialize this widget.
         */
        _create : function() {
            var opts = this.options;
            var search = $(this.element);

            // create/insert hidden value field to hold selected value
            var value = $('<input/>').prop('type', 'hidden')
                                     .prop('name', search.prop('name'))
                                     .val(search.val())
                                     .insertAfter(search);

            // remove name from element as value will be form posted via the hidden field
            search.prop('type', searchType)
                  .prop('name', '');


            search.on('change', function(/*event*/) {
            	// reset value text if search text is empty
            	if (!$.trim(search.val())) {
            		value.val(opts.sDefaultValue);
            	}
            	// otherwise, remove search text if no value was selected
                else if (!$.trim(value.val()) || value.val() == opts.sDefaultValue) { //jshint ignore:line
            		search.val('');
                }
            });

            // set initial suggestion to the 'data-initial' attribute
            if (search.val()) {
                search.val(search.data('initial'));
            }

            // widget caller custom select in options
            var fnCustomSelect = opts.select ? opts.select : $.noop;

            if (opts.suggestionSource instanceof Array) {
                /*
                 * Each suggestion source has its own minimum length
                 */
                opts.minLength = 0;

                opts.source = function(request, response) {
                    var suggestionGroups = [];
                    var nGroupsRemaining = opts.suggestionSource.length;

                    /*
                     * To be executed when finished getting suggestions for each group.
                     */
                    var fnComplete = function() {
                        nGroupsRemaining--;

                        // if all groups have reported in
                        if (!nGroupsRemaining) {
                            // if suggestions from the server
                            if (!$.isEmptyObject(suggestionGroups)) {
                                // respond with suggestion groups
                                response(suggestionGroups);
                            }
                            // else, respond with nothing
                            else {
                                response([]);
                            }
                        }
                    };

                    $.each(opts.suggestionSource, function(index, oSource) {
                        // create a group entry for this suggestion source
                        var group = {label : oSource.label,
                                     name  : oSource.name};

                        /*
                         * Include this source if the minimum search
                         * term length requirement has been met.
                         */
                        if (request.term.length >= oSource.minLength) {
                            $.ajax('/miv/suggest/' + oSource.path, {
                                dataType : 'json',
                                data     : { term : request.term },
                                success  : function(data) {
                                               if (data.length) {
                                                   group.suggestions = data;

                                                   // push group entry into combined suggestion map
                                                   suggestionGroups.push(group);
                                               }
                                           },
                                complete : fnComplete
                            });
                        }
                        // otherwise, complete
                        else {
                            fnComplete();
                        }
                    });
                };

                /*
                 * Set search field to selected label and
                 * set hidden value field to proper name
                 * and value on select.
                 */
                opts.select = function(event, ui) {
                    // if custom select doesn't explicitly return false
                    if (fnCustomSelect(event, ui) !== false)
                    {
                        search.val(htmlDecode(ui.item.label));

                        value.val(htmlDecode(ui.item.value));
                        value.prop('name', ui.item.name);
                    }

                    return false;
                };

                /**
                 * @method suggestion#_renderMenu
                 * @description Render menu for multiple suggestion sources
                 * @param {jQuery#Element} ul                 Suggestion list
                 * @param {Object[]}       items              Suggestion groups
                 * @param {Object[]}       item[].suggestions Group suggestions
                 * @param {String}         item[].label       Group label
                 * @param {String}         item[].name        Form field name for binding selection from this group
                 */
                this._renderMenu = function(ul, items) {
                    var that = this;

                    // groups
                    $.each(items, function(index, group) {
                        // skip this iteration if group has no suggestions
                        if (!group.suggestions || group.suggestions.length <= 0) { return true; }

                        // group label
                        $('<li>').addClass(sCategoryClass)
                                 .text(group.label)
                                 .appendTo(ul);

                        // group items
                        $.each(group.suggestions, function(index, suggestion) {
                            // set group name as suggestions binding name
                            suggestion.name = group.name;

                            that._renderItemData(ul, suggestion);
                        });
                    });
                };
            }
            else {
                // override source for suggestion server path
                opts.source = function(request, response) {
                    $.ajax('/miv/suggest/' + (opts.suggestionSource instanceof Function ?
                                              opts.suggestionSource() :
                                              opts.suggestionSource), {
                        dataType : 'json',
                        data     : {term : request.term},
                        success  : function(data) {
                                       response(data);
                                   }
                    });
                };

                /*
                 * Set search field to selected label and
                 * set hidden value field to selected value.
                 */
                opts.select = function(event, ui) {
                    // if custom select doesn't explicitly return false
                    if (fnCustomSelect(event, ui) !== false)
                    {
                        search.val(htmlDecode(ui.item.label));
                        value.val(htmlDecode(ui.item.value)).trigger('change');
                    }

                    return false;
                };
            }

            /*
             * Reset the hidden value before searching so the
             * result of any previous search does not remain.
             */
            opts.search = function(event, ui) {
                value.val(opts.sDefaultValue);
            };

            // do nothing on suggestion focus
            opts.focus = function(event, ui) {
                return false;
            };

            // call parent widget create
            this._super();
        },
        /**
         * @method suggestion#_renderItem
         * @description Render menu items HTML decoded
         * @param {jQuery#Element} ul         Suggestion list
         * @param {Object}         item       Suggestion to render
         * @param {String}         item.label Suggestion label
         * @param {String}         item.value Suggestion value
         */
        _renderItem : function(ul, item) {
            return $('<li>').append(
                       $('<a>').text(htmlDecode(item.label))
                   ).appendTo(ul);
        }
    });
});
