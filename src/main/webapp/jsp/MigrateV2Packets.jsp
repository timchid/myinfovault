<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%--<%@ taglib prefix="spring" uri="/spring"%>--%><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; v2 To v3 Migration</title>
    <%@ include file="/jsp/metatags.html"%>
    <script type="text/javascript">
    /* <![CDATA][ */
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>

    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
<c:set var="user" scope="request" value="${MIVSESSION.user}" />
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; v2 To v3 Migration
    </div><!-- breadcrumbs -->

    <div id="main">

    <!-- START TEMP -->
    <p>This is a placeholder for the v2 to v3 Migration page.</p>


    <div style="background-color: #f7fff7; border: 1px solid black; padding: 5ex; padding-top: 0;">
        <h2>Query Params</h2>
        <table border="1" cellspacing="0" cellpadding="3">
            <thead><tr><th>param</th><th>value</th></tr></thead>
            <tbody>
            <c:forEach var="p" items="${param}">
                <tr><td>${p.key}</td><td>${p.value}</td></tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <!-- END TEMP -->

    <miv:permit roles="SYS_ADMIN">
    <c:if test="${user.userId==18099 || user.userId==19044}">
    <form method="post" action="<c:url value='/migration/MigrateV2Packets'/>">
        <div class="formline">
        <input type="text" name="thePacId" maxlength="6" width="7">
        <div class="buttonset">
        	<input type="submit" name="start" value="Start" title="Migrate one Packet">
        </div>
        </div>

        <div class="formline">
        <div class="buttonset">
        	<input type="submit" name="all" value="Migrate All" title="Start the Packet Migration">
        </div>
        </div>
    </form>
    </c:if>
    </miv:permit>

    </div><!-- #main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>

