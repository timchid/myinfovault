<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>UC Davis: MyInfoVault &ndash; Active Other Letters</title>
</head>
<body>
<h1>
List Of Documents To Choose
</h1>
<ul class="linkblock">
   <c:forEach var="item" items="${letters}">
    <li>
    <a href="<c:url value='/OpenLetters'>
    <c:param name='actionId' value='2'/>
    <c:param name='docid' value='${item.otherletterid}'/>
    <c:param name='tabid' value='${item.documentid}'/>
    <c:param name='packetid' value='${item.packetid}'/>
    <c:param name='userid' value='${item.userid}'/>
    </c:url>" >${item.filename}</a>
    </li>
   </c:forEach>
</ul>
</body>
</html>