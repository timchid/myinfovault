<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
       required: [ ${constants.config.required} ],
       trim: ["title", "startyear", "endyear", "percenteffort"],
       constraints: {
    	   startyear: function(contents) {
               var curYear = new Date().getFullYear();
               
               if(isNaN(contents)){
               	return "INVALID_NUMBER";
               }
               
               var numyear = contents * 1;
               
               if( numyear < 1900 || numyear > (curYear +1) ){
               	return "INVALID_YEAR";
               }
               
               return true;
           },
           endyear: function(contents) {
               var curYear = new Date().getFullYear();
               
               if( contents.toLowerCase() != 'present' && isNaN(contents)){
               	return "INVALID_END_YEAR";
               }
               
               /* if( contents.toLowerCase() == 'present' ){
            	   contents = "${thisYear}";            	   
               } */
               
               var numyear = contents * 1;
               
               var startyearObj = dojo.byId('startyear');
               
               if( startyearObj && trim(startyearObj.value).length > 0 && !isNaN(startyearObj.value) 
            		   && numyear < startyearObj.value){
               	return "INVALID_END_YEAR_RANGE";
               }
               
               return true;
           }/* ,
           percenteffort: function(contents) {
               
        	   if(isNaN(contents)){
               	return "INVALID_NUMBER";
               }
               
               var percent = contents * 1;
               
               if( percent < 0 || percent > 100 ){
               	return "INVALID_PERCENT";
               }
               
               return true;
           } */
         },
         errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}.",
        	 		/* "INVALID_PERCENT":"Percent must be from 0 to 100.", */
        	 		"INVALID_END_YEAR":"Enter 4 digit year or word &quot;present&quot;.",
        	 		"INVALID_END_YEAR_RANGE":"End Year must be greater than or equal to Start Year."}
    };
// -->    
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>
<p class="formhelp">
	Administrative Assignments are things such as Department Chair,Division Chief, Medical/Clinical Service Director, Graduate Group Director, etc.
</p>

<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<div class="formline">
<span class="textfield" title="${constants.tooltips.title}">
  <label for="title" class="f_required">${constants.labels.title}</label>
  <input type="text"
         id="title" name="title"
         size="54" maxlength="255"
         value="${rec.title}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<span class="textfield" title="${constants.tooltips.startyear}">
  <label for="startyear" class="f_required">${constants.labels.startyear}</label>
  <input type="text"
         id="startyear" name="startyear"
         size="5" maxlength="4"
         value="${rec.startyear}"         
         >
</span>
<span class="textfield" title="${constants.tooltips.endyear}">
  <label for="endyear"  class="f_required">${constants.labels.endyear}</label>
  <input type="text"
         class="f_numeric"
         id="endyear" name="endyear"
         size="8" maxlength="7"
         value="${rec.endyear}"         
         >
</span>
</div><!-- formline -->

<div class="formline">
<span class="textfield" title="${constants.tooltips.percentageeffort}">
  <label for="percenteffort">${constants.labels.percentageeffort}</label>
  <input type="text"
         id="percenteffort" name="percenteffort"
         size="8" maxlength="7"
         value="${rec.percenteffort}"
         >
   <span>%</span>      
</span>
</div><!-- formline -->

<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <input type="submit"  id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>
</form>
