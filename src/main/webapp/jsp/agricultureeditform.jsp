<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript">
<!--
    mivFormProfile = {
       required: [ ${constants.config.required} ],
       trim: ["datespan", "reporttitle", "reportnumber" ]
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<div id="uploadlink"><%--
--%><c:url var="nextLink" value="/PdfUpload"><%--
--%>    <c:param name="_flowId" value="pdfupload-external-flow"/><%--
--%>    <c:param name="documentAttributeName" value="aes"/><%--
--%>    <c:param name="isYearField" value="true"/><%--
--%></c:url>
<a href="<c:out value='${nextLink}' escapeXml='true'/>" title="Upload a PDF">Upload a PDF</a> or enter data in the fields below.
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Date(s) -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.datespan}">
  <label for="datespan" class="f_required">${constants.labels.datespan}</label>
  <input type="text"
         id="datespan" name="datespan"
         size="20" maxlength="50"
         value="${rec.datespan}"
         >
</span>
</div><!-- formline -->

<!-- Project Title -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.reporttitle}">
  <label for="reporttitle"  class="f_required">${constants.labels.reporttitle}</label>
  <input type="text"
         id="reporttitle" name="reporttitle"
         size="54" maxlength="255"
         value="${rec.reporttitle}"
         >
</span>
</div><!-- formline -->

<!-- Investigator Names -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.investigatorname}">
  <label for="investigatorname">${constants.labels.investigatorname}</label>
  <input type="text"
         id="investigatorname" name="investigatorname"
         size="54" maxlength="255"
         value="${rec.investigatorname}"
         >
</span>
</div><!-- formline -->

<!-- Number -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.reportnumber}">
  <label for="reportnumber">${constants.labels.reportnumber}</label>
  <input type="text" class="f_numeric"
         id="reportnumber" name="reportnumber"
         size="25" maxlength="50"
         value="${rec.reportnumber}"
         >
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
