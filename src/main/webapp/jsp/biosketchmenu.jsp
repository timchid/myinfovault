<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="spring" uri="/spring"%><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; ${constants.strings.menuheading}</title>
    <%@ include file="/jsp/metatags.html"%>
    <script type="text/javascript">
    /* <![CDATA[ */
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>

    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
<c:set var="user" scope="request" value="${MIVSESSION.user}" />
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; <a href="<c:url value='/biosketch/BiosketchPreview?biosketchtypeid=${biosketchtypeId}'/>"
                title="${constants.strings.description} List">${constants.strings.description} List</a>
        &gt; ${constants.strings.description} Menu
    </div><!-- breadcrumbs -->
    <div id="main">
        <h1 id="recordtypedescription" style="float: left;">${constants.strings.menuheading}</h1>
        <c:choose>
        <c:when test="${biosketchname == null}">
        <p>
        No data has been entered or no data has been set to print for your
        ${constants.strings.description}. Please enter data or set some data to
        print, and then you will be able to create your document. Print options
        are available on the "${constants.strings.designmybiosketch}" page.
        </p>
        </c:when>
        <c:otherwise>
        <div>&nbsp;</div><%-- This div fixes an IE7 rendering bug. --%>
        <div style="clear: both; padding-bottom: 1em;">
            <strong>${biosketchname}</strong>
        </div>
        <form method="post">
            <%-- this is a List of Links, so it should be a <ul> with no <br> between items --%>
            <a href="<c:url value='/biosketch/BiosketchDisplayOptions'></c:url>"
               title="${constants.strings.displayselectoptionstitle}">${constants.strings.displayselectoptionstitle}</a>
            <br/>

            <a href="<c:url value='/biosketch/SelectBiosketchData'></c:url>"
               title="${constants.strings.selectbiosketchdata}">${constants.strings.selectbiosketchdata}</a>
            <br/>

            <a href="<c:url value='/biosketch/DesignMyBiosketch'></c:url>"
               title="${constants.strings.designmybiosketch}">${constants.strings.designmybiosketch}</a>
            <br/>

            <a href="<c:url value='/biosketch/CreateMyBiosktech'></c:url>"
               title="${constants.strings.createmybiosketch}">${constants.strings.createmybiosketch}</a>
        </form>
        </c:otherwise>
        </c:choose>

    </div><!-- main -->
    <jsp:include page="/jsp/mivfooter.jsp" />
</body>
</html>


