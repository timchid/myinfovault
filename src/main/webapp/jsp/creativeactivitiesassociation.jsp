<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <%@ include file="/jsp/metatags.html" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Associate Items</title>
        <script type="text/javascript" src="<c:url value='/js/creativeactivities/creativeactivitiesassociation.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/creativeactivities/creativeactivities.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/association.css'/>">

        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>

    <body class="useiframes">

        <div id="main" class="nopadding mivbackground">
<%--
    This page should be re-structured as follows:
        The top is the association rule, just as it is now.
        Next comes three divs

        1. The "Available" div, which contains:
            (a) The "Available X" heading
            (b) The "Find" box for available X items
            (c) The select list (multi-select) for the listed available items
         ** This is a UNIT, with three pieces that should be kept together.

        2. The action buttons, which are the "Add" and "Remove" buttons

        3. The "Selected" div, which contains:
            (a), (b), (c) The same three pieces that are in the "Available" div
--%>
        <c:if test="${not empty associationRule}">
        <div class="associationrule">
            <span class="ruletitle">Association Rule :</span> <span class="ruledescription">${associationRule}</span>
        </div>
        </c:if>

        <input type="hidden" name="associationListJson" id="associationListJson" value='<c:out value='${associationListJson}' escapeXml='true'/>'>

        <div class="availableblock">
            <!-- FIXME: The next ilne is NOT a label, because it doesn't label a control. It is a HEADING -->
            <label class="legend"> Available ${ not empty associatetitle ? associatetitle : ""}</label>
            <div title="Find into available ${ not empty associatetitle ? associatetitle : ""}" class="searchbox">
                <form action="<c:url value='/Associate?selectedids=${selectedIds}&rectype=${recordTypeName}&recid=${recordID}&year=${year}'/>" method="post" >
                    <label for="searchavailable">Find:</label>
                    <input type="text" name="txtsearchavailable" id="txtsearchavailable" size="22"  value="${txtsearchavailable}">
                    <input type="submit" class="unscripted" name="searchavailable" id="searchavailable" value="Go" title="Click to find into available ${ not empty associatetitle ? associatetitle : ""}">
                </form>
            </div>
        </div>

        <div class="togglespace"> &nbsp;</div><%--This "spacing div" wouldn't be needed if the page were structured as I describe above. --%>

        <div class="selectedblock">
            <!-- FIXME: The next ilne is NOT a label, because it doesn't label a control. It is a HEADING -->
            <label class="legend">Selected ${ not empty associatetitle ? associatetitle : ""}</label>
            <div title="Find into selected ${ not empty associatetitle ? associatetitle : ""}" class="searchbox">
                <form action="<c:url value='/Associate?selectedids=${selectedIds}&rectype=${recordTypeName}&recid=${recordID}&year=${year}'/>" method="post" >
                    <label for="searchselected">Find:</label>
                    <input type="text" name="txtsearchselected" id="txtsearchselected" size="22" value="${txtsearchselected}">
                    <input type="submit" class="unscripted" name="searchselected" id="searchselected" value="Go" title="Click to find into selected ${ not empty associatetitle ? associatetitle : ""}">
                </form>
            </div>
        </div>

        <form action="<c:url value='/Associate'/>" method="post" >
            <input type="hidden" name="rectype" id="rectype" value="${recordTypeName}">
            <input type="hidden" name="recid" id="recid" value="${recordID}">
            <input type="hidden" name="selectedids" id="selectedids" value="${selectedIds}" id="selectedids">
            <input type="hidden" name="year" id="year" value="${year}">

            <div class="availableblock">
                <select name="available" id="available" multiple="multiple" size="10">
                <c:forEach var="opt" items="${availableList}" varStatus="availableIndex"><%--
                --%>
                <option value="${opt.id}" title="${opt.value}" class="${(availableIndex.count)%2==0?'even':'odd'} ${recordTypeName}">${opt.value}</option>

                <%-- <c:choose>
                        <c:when test="${empty year || empty opt.isvalid || opt.isvalid == 'true'}">
                                <option value="${opt.id}" title="${opt.value}" class="${(availableIndex.count)%2==0?'even':'odd'} ${recordTypeName}">${opt.value}</option>
                        </c:when>
                        <c:when test="${!opt.isvalid || opt.isvalid == 'false'}">
                                <option value="${opt.id}" title="${opt.value}" class="${(availableIndex.count)%2==0?'even':'odd'}error ${recordTypeName}">${opt.value}</option>
                        </c:when>
                </c:choose> --%>

                </c:forEach><%--
            --%></select>
            </div>

            <div class="togglebuttons">
                <input type="submit" name="add" id="add" value=">>" title="Add"><p/><!-- FIXME: What's with the empty P tag? Why is it here? This is Invalid in HTML 5 -->
                <input type="submit" name="remove" id="remove" value="<<" title="Remove">
            </div>

            <div id="selectedblock">
                <select name="selected" id="selected" multiple="multiple" size="10">
                <c:forEach var="opt" items="${selectedList}" varStatus="selectedIndex"><%--
                --%>
                <option value="${opt.id}" title="${opt.value}" class="${(selectedIndex.count)%2==0?'even':'odd'} ${recordTypeName}">${opt.value}</option>

                <%-- <c:choose>
                        <c:when test="${empty year || empty opt.isvalid || opt.isvalid == 'true'}">
                                <option value="${opt.id}" title="${opt.value}" class="${(selectedIndex.count)%2==0?'even':'odd'} ${recordTypeName}">${opt.value}</option>
                        </c:when>
                        <c:when test="${!opt.isvalid || opt.isvalid == 'false'}">
                                <option value="${opt.id}" title="${opt.value}" class="${(selectedIndex.count)%2==0?'even':'odd'}error ${recordTypeName}">${opt.value}</option>
                        </c:when>
                </c:choose>
                --%>
                </c:forEach><%--
            --%></select>
            </div>

        </form>

        </div><!-- #main -->

        <script type="text/javascript">
        jQuery('body').addClass("${user.userInfo.person.primaryRoleType}").addClass(${MIVSESSION.config.productionServer} ? "prod" : "non-prod");
        </script>
    </body>
</html>
