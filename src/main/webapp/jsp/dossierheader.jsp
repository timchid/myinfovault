<%--
    NOTE: dossier object must exist with scope="request" in order to  make use of this JSP

    DESCRIPTION:
        * Extracting dossier object to display dossier header

    INPUT:
        * "dossier"       : must be exist with scope="request"

--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet" type="text/css" href="<c:url value='/css/dossierheader.css'/>">

<!-- Show the dossier header only when param dossier exist -->
<c:if test="${not empty dossier}">
<!-- dossier header : start -->
<c:set var="schoolDepartment" scope="page" value="${dossier.primaryDepartment.school}"/>
<c:if test="${not empty dossier.primaryDepartment.description}">
        <c:set var="schoolDepartment" scope="page" value="${schoolDepartment} - ${dossier.primaryDepartment.description}"/>
</c:if>

<c:set var="delegationAuthority" scope="page" value="${dossier.action.delegationAuthority.description}"/>
<c:if test="${not empty dossier.reviewType}">
        <c:set var="delegationAuthority" scope="page" value="${delegationAuthority} (${dossier.reviewType.description})"/>
</c:if>

<div class="summary">
        <div class="displayName">${dossier.candidate.displayName}</div>
        <div class="schoolDepartment">${schoolDepartment}</div>
        <div class="actionDescription">${dossier.action.fullDescription}
        <c:if test="${not empty dossier.submittedDate}">
              <div class="submitDate"><span class="routedDate">Submitted on
              <fmt:formatDate pattern="M/d/yyyy, h:mm a" value="${dossier.submittedDate}"/></span></div>
        </c:if>
        </div>

        <c:if test="${not empty dossier.submittedDate}">
              <div title="Current location of this Dossier" class="location">
                <span class="dossierLocation">Location: <strong>${dossier.dossierLocation.description}</strong></span> &mdash;
                <span class="routedDate">Last Routed on <fmt:formatDate pattern="M/d/yyyy, h:mm a" value="${dossier.lastRoutedDate}"/></span>
              </div>
        </c:if>
</div>
<!--.summary-->
<!-- dossier header : end -->
</c:if>
