<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
       required: [ ${constants.config.required} ],
       trim: ["description","years"]
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<div class="formline">
<span class="textfield" title="${constants.tooltips.year}">
  <label for="years" class="f_required">${constants.labels.years}</label>
  <input type="text"
         id="years" name="years"
         size="10" maxlength="20"
         value="${rec.years}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<span class="textfield" title="${constants.tooltips.description}">
  <label for="description" class="f_required">${constants.labels.description}</label><br />
  <textarea
         id="description"
         name="description"
         rows="3"
         cols="52"
         wrap="soft"
     >${rec.description}</textarea>
</span>
</div><!-- formline -->

<div class="formline">
<span class="textfield" title="${constants.tooltips.remark}">
  <label for="remark">${constants.labels.remark}</label><br />
  <textarea
         id="remark"
         name="remark"
         rows="3"
         cols="52"
         wrap="soft"
     >${rec.remark}</textarea>
</span>
</div><!-- formline -->



<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save">
  <input type="submit"  id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>
</form>
