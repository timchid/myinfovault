<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<script type="text/javascript" src="<c:url value='/js/evaluation.js'/>"></script>

<script type="text/javascript"><!--
    mivFormProfile = {
       required: [ ${constants.config.required} ],
       trim: ["year", "course", "description", "responsetotal","enrollment",
              "instructorscore", "coursescore", "link" ],
       constraints: {
    	   link: miv.validation.isLink
       }
    };    
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- <!-- constants: ${constants} --> --%>
<%-- <!-- constants.labels: ${constants.labels} --> --%>
<%-- <!-- constants.options: ${constants.options} --> --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">


<!-- Quarter & Year -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  <input type="text"
         id="year" name="year"
         size="22" maxlength="40"<%-- big enough to show "Summer Semester 2222" --%>
         value="${rec.year}"
         >
</span>
</div><!-- formline -->

<!-- Program/Course -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.course}">
  <label for="course" class="f_required">${constants.labels.course}</label>
  <input type="text"
         id="course" name="course"
         size="25" maxlength="200"
         value="${rec.course}"
         >
</span>
</div><!-- formline -->


<!-- Description -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.description}">
  <label for="description" class="f_required">${constants.labels.description}</label><br />
<%--  <input type="text"
         id="description" name="description"
         size="54" maxlength="255"
         value="${rec.description}"
         >--%>
  <textarea
            id="description" name="description"
            rows="3" cols="50" wrap="soft"
            >${rec.description}</textarea>
</span>
</div><!-- formline -->


<!-- Total Responses -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.enrollment}">
  <label for="enrollment">${constants.labels.enrollment}</label>
  <input type="text" class="f_numeric"
         id="enrollment" name="enrollment"
         size="5" maxlength="4"
         value="${rec.enrollment}"
         >
</span>

<span class="textfield" title="${constants.tooltips.responsetotal}">
  <label for="responsetotal">${constants.labels.responsetotal}</label>
  <input type="text" class="f_numeric"
         id="responsetotal" name="responsetotal"
         size="5" maxlength="4"
         value="${rec.responsetotal}"
         >
</span>

<span class="percentageofreturn hidden">
<span class="textfield" title="${constants.tooltips.percentageofreturn}">
  <label for="percentageofreturn">${constants.labels.percentageofreturn}</label>
  <input type="text" class="f_numeric"
         id="percentageofreturn" name="percentageofreturn"
         size="7" readonly="readonly"
         value=""
         >
</span>
</span>
</div><!-- formline -->

<!-- Instructor Score -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.instructorscore}">
  <label for="instructorscore">${constants.labels.instructorscore}</label>
  <input type="text" class="f_numeric"
         id="instructorscore" name="instructorscore"
         size="5" maxlength="10"
         value="${rec.instructorscore}"
         >
</span>

<!-- Course Score -->
<span class="textfield" title="${constants.tooltips.coursescore}">
  <label for="coursescore">${constants.labels.coursescore}</label>
  <input type="text" class="f_numeric"
         id="coursescore" name="coursescore"
         size="5" maxlength="10"
         value="${rec.coursescore}"
         >
</span>
</div><!-- formline -->


<!-- Eval Type: summaries or complete evaluations -->
<div class="formline">
<span class="dropdown" title="${constants.tooltips.typeid}">
  <label for="typeid" class="f_required">${constants.labels.typeid}</label>
  <select id="typeid" name="typeid">
  <c:forEach var="opt" items="${constants.options.evaluationtype}"><%--
--%>    <option value="${opt.id}"${rec.typeid==opt.id?' selected="selected"':''}>${opt.name}</option>
  </c:forEach><%--
--%></select>
</span>
</div><!-- formline -->


<!-- Link -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.link}">
  <label for="link">${constants.labels.link}</label>
  <input type="text"
         id="link" name="link"
         size="54" maxlength="200"
         value="${rec.link}"
         >
</span>
</div><!-- formline -->

<%--
        NEED RADIO BUTTONS OR SELECT LIST FOR "Summary" vs. "Complete" eval type.
--%>

<!-- Evaluations Submitted: Summary/Completed (RADIO Boxes) -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
