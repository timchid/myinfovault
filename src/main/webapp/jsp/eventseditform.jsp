<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${usepopups || constants.config.usepopups}"><%--
--%><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache, must-revalidate">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="expires" content="0">

        <%@ include file="/jsp/metatags.html" %>

        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/creativeactivities.css'/>">
        <script type="text/javascript" src="<c:url value='/js/location.js'/>"></script>

    </head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
    if (typeof _container_ != 'undefined') {
        var btn = document.getElementById("popupformcancel");
        _container_.setCloseControl(btn);
    }
</script>
</c:if><%-- usepopups --%>

<c:if test="${ !(usepopups || constants.config.usepopups) }">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/creativeactivities.css'/>">
        
        <script type="text/javascript" src="<c:url value='/js/popupeditform.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/creativeactivities/events.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/creativeactivities/creativeactivities.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/location.js'/>"></script>
        <%--<script type="text/javascript" src="<c:url value='/js/lwconsole.js'/>"></script>--%>
</c:if>
<c:if test="${ (usepopups || constants.config.usepopups) }"><%--SDP added: always use events.js --%>
    <script type="text/javascript" src="<c:url value='/js/creativeactivities/events.js'/>"></script>
</c:if>

<c:if test="${ !(usepopups || constants.config.usepopups) }">
<script type="text/javascript">
//<![CDATA[
    var mivFormProfile = {
        required: [${constants.config.required}],
        trim: [${constants.config.required}],
        constraints: {
            eventtypeid: miv.validation.isDropdown,
            statusid: miv.validation.isDropdown,
            url: miv.validation.isLink
        }
    };
//]]>
</script>
</c:if>

<c:if test="${usepopups || constants.config.usepopups}">
<body class="usepopups ">
    <div id="main">
        <div class="pagedialog" id="maindialog">
            <div class="dojoDialog mivDialog"><!--might want to be more flexible with dialog widths-->
</c:if>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>

<div id="userheading">
    <span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<!-- dummy hidden values -->
<input type="hidden" id="usepopups" value="${ (usepopups || constants.config.usepopups) }">
<input type="hidden" id="thisYear" value="${thisYear}">

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

    <input type="hidden" id="class" name="recordClass" value="${recordClass}">
    <input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
    <input type="hidden" id="recid" name="recid" value="${rec.id}">
    <input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

    <div class="formline">
        <%--span class="textfield" title="Start Date"--%>
        <span class="textfield" title="${constants.tooltips.eventstartdate}">
            <label for="eventstartdate" errorlabel="Event start date" class="f_required">${constants.labels.eventstartdate}</label>
            <input type="text" id="eventstartdate" name="eventstartdate" size="11" maxlength="20"  class="datepicker" value="${rec.eventstartdate}">
        </span>
        <span class="textfield" title="${constants.tooltips.eventsenddate}">
            <label for="eventenddate" errorlabel="Event end date" >${constants.labels.eventsenddate}</label>
            <input type="text" id="eventenddate" name="eventenddate" size="11" maxlength="20"  class="datepicker" value="${rec.eventenddate}">
        </span>
    </div><!-- formline -->

    <div class="formline">
        <span class="dropdown" title="${constants.tooltips.eventtypeid}">
            <label for="eventtypeid" class="f_required">${constants.labels.eventtypeid}</label>
            <%-- <input type="text" id="eventtypeid" name="eventtypeid" value="${rec.eventtypeid}"> --%>
            <select id="eventtypeid" name="eventtypeid">
                <option value="0" selected="selected">--- Select ---</option><%--
            --%><c:forEach var="opt" items="${constants.options.eventtype}">
                <option value="${opt.id}"${rec.eventtypeid==opt.id?' selected="selected"':''} title="${opt.tip}">${opt.value}</option><%--
            --%></c:forEach>
            </select>
        </span>
        <a href="<c:url value='/help/eventtype.html'/>" target="mivhelp" class="mivlink">Help! My event type is not listed</a>
    </div><!-- formline -->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.statusid}">
            <label for="statusid" class="f_required">${constants.labels.statusid}</label>
            <select id="statusid" name="statusid">
            <c:forEach var="opt" items="${constants.options.eventstatus}"><%--
            --%>  <option value="${opt.id}"${ ((rec.statusid==null && opt.id==1)?' selected="selected"': (( rec.statusid==opt.id)?' selected="selected"':''))} title="${opt.tip}">${opt.value}</option>
            </c:forEach><%--
        --%> </select>
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.eventdescription}">
            <label for="eventdescription">${constants.labels.eventdescription}</label><br>
            <textarea id="eventdescription" name="eventdescription"
                      rows="2" cols="51" wrap="soft"
                      >${rec.eventdescription}</textarea>
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.venue}">
            <label for="venue">${constants.labels.venue}</label>
            <input type="text" id="venue" name="venue" value="${rec.venue}" size="50" maxlength="255">
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.venuedescription}">
            <label for="venuedescription">${constants.labels.venuedescription}</label><br>
            <textarea id="venuedescription" name="venuedescription"
                      rows="2" cols="51" wrap="soft"
                      >${rec.venuedescription}</textarea>
        </span>
    </div><!--formline-->

    <div id="locationblock"> <!-- Location Block -->
        <c:set var="tempcountry" value="${(not empty rec.id ? ( not empty rec.country ? rec.country : '') : constants.config.contrycode)}"/>
        <c:set var="tempprovince" value="${(not empty rec.id ? ( not empty rec.province ? rec.province : '') : constants.config.province)}"/>

        <div class="formline">
            <span class="textfield" title="${constants.tooltips.country}">
                <label for="country">${constants.labels.country}</label>
                <select id="country" name="country">
                    <option value="" selected="selected">--- Select Country ---</option><%--
                --%><c:forEach var="opt" items="${constants.options.countries}">
                    <option value="${opt.id}"${tempcountry==opt.id?' selected="selected"':''} title="${opt.value}">${opt.value}</option><%--
                --%></c:forEach>
                </select>
            </span>
        </div><!--formline-->

        <div class="formline">
            <div class="textfield" title="${constants.tooltips.province}" id="provinceblock">
                <label for="province">${constants.labels.province}</label>
                <select id="province" name="province">
                    <option value="" selected="selected">--- Select State/Province ---</option><%--
                --%><c:forEach var="opt" items="${constants.options.provinces}">
                    <option value="${opt.id}"${tempprovince==opt.id?' selected="selected"':''} title="${opt.value}">${opt.value}</option><%--
                --%></c:forEach>
                </select>
            </div>

            <div class="textfield" title="${constants.tooltips.city}" id="cityblock">
                <label for="city">${constants.labels.city}</label>
                <input type="text" id="city" name="city" value="${rec.city}">
            </div>
        </div><!--formline-->
    </div><!-- Location Block -->

    <!-- URL / Link -->
    <div class="formline" id="urlformline">
        <span class="textfield" title="${constants.tooltips.url}">
        <label for="url">${constants.labels.url}</label>
        <input type="url" pattern="(https?|ftp):\/\/.+"
               placeholder="http://example.com/some+page"
               id="url" name="url"
               size="52" maxlength="255"
               value="${rec.url}"
               >
        </span>
    </div><!-- formline -->

<c:if test="${ !(usepopups || constants.config.usepopups) }">
<input type="hidden" id="selectedids" name="selectedids" value="">
<fieldset id="associate_fieldset" name="associate_fieldset" class="collapsible ">
    <legend>Associate Works with this Event</legend>
    <div class="hint">expand to see association form</div>

    <div id="association">
        <!-- association error field -->
        <label for="association" class="hidden">Associate Works</label>

        <div class="associationbuttons">
            <div class="buttonset">
                <!-- <a title="Add a new event" href="/miv/EditRecord?rectype=events&pubtype=95&documentid=&sectionname=&subtype=&E0000=Add+a+New+Record" class="linktobutton mivbutton">Add&nbsp;A&nbsp;New&nbsp;Event</a> -->
                <input type="button" id="newevent" name="newevent" value="Add a New Work" title="Add a New Work" onclick="">
            </div>
        </div>

        <div class="formline">
            <%-- <iframe src="<c:url value='pages/creativeactivitiesassociation.jsp'/>" height="350px" width="590px" frameborder="0" scrolling="yes" style="overflow:auto;">
            </iframe> --%>
            <iframe id="associationframe" name="associationframe" src="<c:url value='/Associate?requestType=init&rectype=${recordTypeName}&recid=${rec.id}&year=${rec.eventstartdate}'/>" frameborder="0" scrolling="yes">
            </iframe>
        </div><!--formline-->
    </div><!-- association -->

</fieldset>
</c:if>
<%--
<div id="output_console">
</div>
<script type="text/javascript">
var lwcon =  new Console('output_console');
lwcon.write('console is loaded');
</script>
--%>
<c:if test="${ !(usepopups || constants.config.usepopups) }">
    <div class="buttonset">
        <input type="submit" id="save" name="save" value="Save" title="Save">
        <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
        <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
    </div>
</c:if>

<c:if test="${ !(usepopups || constants.config.usepopups) }">
<div id="dialogBox">
    <div id="dialog-form" title="Add a New Work" >
        <%-- Add form at runtime --%>
    </div>
</div>
</c:if>

</form>

<c:if test="${usepopups || constants.config.usepopups}">
</div>
</div>
</div>
</body>
</html>
</c:if>

