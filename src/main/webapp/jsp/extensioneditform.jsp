<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
      required: [ ${constants.config.required} ],
      trim: ["year","coursenumber","title","units","duration","locationdescription"],
      constraints: {
          year: function(contents) {
              var curYear = new Date().getFullYear();
              
              if(isNaN(contents)){
              	return "INVALID_NUMBER";
              }
              
              var numyear = contents * 1;
              
              if( numyear < 1900 || numyear > (curYear +1) ){
              	return "INVALID_YEAR";
              }
              
              return true;
          },
		  termtypeid: function(contents) {
			
			  if(contents == 0){
				  return "REQUIRED";
			  }
				
			  return true;			
		  }
        },
        errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}" }
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>
<p class="formhelp">
Enter the university extension teaching load carried by the candidate during the review period.
</p>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="typeid" name="typeid" value="6"><%-- 6==University Extension --%>
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Academic Year -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  <input type="text" id="year" name="year" size="5" maxlength="4"
		value="${rec.year}" title="Year must be from 1900 to ${thisYear+1}">
  </span>
<!-- Quarter -->
<span class="dropdown" title="${constants.tooltips.termtypeid}">
   <label for="termtypeid" class="f_required">${constants.labels.termtypeid}</label>
   <select id="termtypeid" name="termtypeid">
   <c:forEach var="opt" items="${constants.options.termtype}"><%--
--%> <option value="${opt.id}"${rec.termtypeid==opt.id?' selected="selected"':''}>${opt.name}</option>
   </c:forEach><%--
--%></select>
</span>
</div><!-- formline -->

<!-- Course Number -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.coursenumber}">
  <label for="coursenumber">${constants.labels.coursenumber}</label>
  <input type="text"
         id="coursenumber" name="coursenumber"
         size="10" maxlength="20"
         value="${rec.coursenumber}"
         >
</span>
</div><!-- formline -->

<!-- Course Title  -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.title}">
  <label for="title" class="f_required">${constants.labels.title}</label>
  <input type="text"
         id="title" name="title"
         size="54" maxlength="255"
         value="${rec.title}"
         >
</span>
</div><!-- formline -->

<!-- Units -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.units}">
  <label for="units">${constants.labels.units}</label>
  <input type="text" class="f_decimal"
         id="units" name="units"
         size="4" maxlength="4"
         value="${rec.units}"
         >
</span>
</div><!-- formline -->

<!-- Duration of Course -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.duration}">
  <label for="duration">${constants.labels.duration}</label>
  <input type="text"
         id="duration" name="duration"
         size="30" maxlength="30"
         value="${rec.duration}"
         >
</span>
</div><!-- formline -->

<!-- Location -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.locationdescription}">
  <label for="locationdescription">${constants.labels.locationdescription}</label>
  <input type="text"
         id="locationdescription" name="locationdescription"
         size="54" maxlength="255"
         value="${rec.locationdescription}"
         >
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
