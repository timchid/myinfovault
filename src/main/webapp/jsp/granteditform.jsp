<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<script type="text/javascript" src="<c:url value='/js/grantedit.js'/>"></script>

<script type="text/javascript">
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: ["title", "source", "number", "startdate", "enddate","submitdate",
               "effort", "principalinvestigator", "description"],
        constraints: {
            startdate : function(contents) {
                var nStatusID = trim(dojo.byId('statusid').value);

                if (trim(contents).length <= 0) {
                    if (nStatusID == 1 || nStatusID == 3) {
                        return "MISSING_STARTDATE";
                    }
                    else if (nStatusID == 5 && trim(dojo.byId('submitdate').value) <= 0) {
                        return "POSSIBLY_MISSING_STARTDATE";
                    }
                }

                return true;
            },
            submitdate : function(contents) {
                var nStatusID = trim(dojo.byId('statusid').value);

                if (trim(contents).length <= 0) {
                    if (nStatusID == 2 || nStatusID == 4) {
                        return "MISSING_SUBMITDATE";
                    }
                    else if (nStatusID == 5 && trim(dojo.byId('startdate').value) <= 0) {
                        return "POSSIBLY_MISSING_SUBMITDATE";
                    }
                }

                return true;
            }
        },
        errormap: {
            "MISSING_STARTDATE"           : "This field is required for a status of 'Active' or 'Completed'.",
            "MISSING_SUBMITDATE"          : "This field is required for a status of 'Pending' or 'Not Awarded'.",
            "POSSIBLY_MISSING_STARTDATE"  : "This field or the Submittted/Resubmitted Date is required for a status of 'None'.",
            "POSSIBLY_MISSING_SUBMITDATE" : "This field or the Start Date must be entered is required for a status of 'None'."
        }
    };

</script>

<div id="userheading">
    <span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">
<!-- Grant Type and Grant Status dropdowns -->
<!--  Type: Research, Gift, Teaching and Training, Other (typeid) -->
<!--  Status: Active, Pending, Completed, Unawarded (statusid) -->
<div class="formline">
<span class="dropdown" title="${constants.tooltips.typeid}">
  <label for="typeid" class="f_required">${constants.labels.typeid}</label>
  <select id="typeid" name="typeid">
  <c:forEach var="opt" items="${constants.options.granttype}"><%--
--%>  <option value="${opt.id}"${rec.typeid==opt.id?' selected="selected"':''}>${opt.name}</option>
  </c:forEach><%--
--%></select>
</span>

<span class="dropdown" title="${constants.tooltips.statusid}">
  <label for="statusid" class="f_required">${constants.labels.statusid}</label>
  <select id="statusid" name="statusid">
    <option value="" selected="selected"></option>
  <c:forEach var="opt" items="${constants.options.grantstatus}"><%--
--%>  <option value="${opt.id}"${rec.statusid==opt.id?' selected="selected"':''}>${opt.name}</option>
  </c:forEach><%--
--%></select>
</span>
</div><!-- formline -->

<!-- Grant/Contract/Gift Title -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.title}">
  <label for="title" class="f_required">${constants.labels.title}</label>
  <input type="text"
         id="title" name="title"
         size="52" maxlength="255"
         value="${rec.title}"
         >
</span>
</div><!-- formline -->

<!-- Funding Agency -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.source}">
  <label for="source">${constants.labels.source}</label>
  <input type="text"
         id="source" name="source"
         size="52" maxlength="100"
         value="${rec.source}"
         >
</span>
</div><!-- formline -->

<!-- Grant Number and Amount -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.number}">
  <label for="number">${constants.labels.number}</label>
  <input type="text"
         id="number" name="number"
         size="10" maxlength="30"
         value="${rec.number}"
         >
</span>
 &nbsp; &nbsp; &nbsp;
<span class="textfield" title="${constants.tooltips.amount}">
  <label for="amount">${constants.labels.amount}</label>
  <input type="text" class="f_currency"
         id="amount" name="amount"
         size="10" maxlength="20"
         value="${rec.amount}"
         >
</span>
</div><!-- formline -->

<fieldset>
    <legend>Either the Start Date or the Submittted/Resubmitted Date must be entered.</legend>

    <div class="formline">
        <div id="grantdatespan">
            <!-- Start Date -->
            <span class="textfield" title="${constants.tooltips.startdate}">
                <label for="startdate" errorlabel="Start Date" class="f_required">${constants.labels.startdate}</label>
                <input type="text" class="datepicker" id="startdate" name="startdate"
                       size="11" value="${rec.startdate}">
            </span> &nbsp; &nbsp; &nbsp;

            <!-- End Date -->
            <span class="textfield" title="${constants.tooltips.enddate}">
                <label for="enddate">${constants.labels.enddate}</label>
                <input type="text" class="datepicker" id="enddate" name="enddate"
                       size="11" value="${rec.enddate}">
            </span>
        </div><!-- grantdatespan -->
    </div>
    <div class="formline">
        <div id="grantsubmitdate">
            <!-- Submitted/Resubmitted Year or Year Obtained(gift) -->
            <span>and/or</span>
            <span class="textfield" title="${constants.tooltips.submitdate}">
                <label for="submitdate" errorlabel="Submittted/Resubmitted Date" class="f_required">${constants.labels.submitdate}</label>
                <input type="text" class="datepicker" id="submitdate" name="submitdate" size="11" value="${rec.submitdate}">
            </span>
        </div><!-- grantsubmitdate -->
    </div>
</fieldset>

<div class="formline">
<!-- Percent Effort -->
<span class="textfield" title="${constants.tooltips.effort}">
  <label for="effort">${constants.labels.effort}</label>
  <input type="text"
         id="effort" name="effort"
         size="6" maxlength="4"
         value="${rec.effort}"
         >%
</span>
</div><!-- formline -->

<%-- Percent Effort -->   Eliminated?  Check in Jira :: it's not in the GrantSummary table.
<div class="formline">
  <label for="effort">Percent Effort:</label>
 <input type="text"
        id="effort"
        >
</div><!-- formline -->
--%>
<!-- Role dropdown -->
<div class="formline">
<span class="dropdown" title="${constants.tooltips.roleid}">
  <label for="roleid" class="f_required">${constants.labels.roleid}</label>
  <select id="roleid" name="roleid">
  <c:forEach var="opt" items="${constants.options.grantrole}"><%--
--%>  <option value="${opt.id}"${rec.roleid==opt.id?' selected="selected"':''}>${opt.name}</option>
  </c:forEach><%--
--%></select>
</span>
</div><!-- formline -->

<!-- PI Name -->
<!--  need javascript to show/hide this field based on roleid selection -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.principalinvestigator}">
  <label for="principalinvestigator">${constants.labels.principalinvestigator}</label>
  <input type="text"
         id="principalinvestigator" name="principalinvestigator"
         size="45" maxlength="255"
         value="${rec.principalinvestigator}"
         >
</span>
<br/>
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<!-- (if you are the CO-PI or Co-Investigator) -->
 (if you are not the Principal Investigator)
</div><!-- formline -->

<!-- Purpose/Goal -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.description}">
  &#8224;&nbsp;
  <label for="description">${constants.labels.description}</label><br>
  <textarea
            id="description" name="description"
            rows="4" cols="52" wrap="soft"
            >${rec.description}</textarea>
</span>
</div><!-- formline -->
<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save">
  <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
