<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>MIV &ndash; Confirmation: Error Message Sent</title>

    <%@ include file="/jsp/metatags.html" %>

  <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
  
  <link rel="stylesheet" type="text/css" title="MIV Style" href="<c:url value='/css/mivEmail.css'/>">

<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; Confirmation: Error Message Sent
  </div><!-- breadcrumbs -->


<!-- MIV Main Div -->
<div id="main">
  <c:if test="${fn:length(sentmessage)>0}">
  <h1>Confirmation: Error Message Sent</h1>
  <p>
  <strong>The following message has been emailed to the MIV Project Team at ${toAddress}:</strong><br />
  Print this page if you would like to keep a copy for your records.
  </p>
  <div class="sent" id="message">
  ${sentmessage}
  </div>
  </c:if>
  <c:if test="${fn:length(failmessage)>0}">
  <h2>Message Was Not Sent!</h2>
  <div class="sendfailed" id="#message">
  <p>${failmessage}</p>
  </div>
  </c:if>
</div><!-- main -->

<jsp:include page="/jsp/mivfooter.jsp" />

</body>
</html>
