<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<div>
    <div id="userheading">
    <span class="standout">${user.targetUserInfo.displayName}</span><br>
    Logged in as ${MIVSESSION.user.loginName}, ID=${user.userID}
    </div>
</div>


<div class="error" style="clear:both; font-size:1.4em; margin:10px;">
An error occured in MyInfoVault.
No form was found to edit a record of type &ldquo;${recordTypeName}&rdquo;
</div>
<div class="mailto" style="font-size:1.4em; margin-top:1em;">
<a href="mailto:miv-help@ucdavis.edu?subject=MIV:Missing Edit Form for '${recordTypeName}' records.&body=I am ${user.userInfo.displayName}
 and was logged in as ${MIVSESSION.user.loginName} with MIV ID number ${user.userId}.
 I was working on ${user.targetUserInfo.displayName}'s data [ID ${user.targetUserInfo.person.userId}]
 when I got a 'no form found' error while trying to edit a '${recordTypeName}'
 record.">Report this Error</a>
<br><br>
</div>

<form class="mivdata" id="mytestform">
<div class="buttonset">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="reset" id="popupformcancel" name="popupformcancel" value="Cancel">
</div>
</form>
