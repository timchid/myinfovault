<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!-- ******************** Footer ******************** -->
<jsp:useBean id="date" scope="page" class="java.util.Date"></jsp:useBean>
<div id="footer">
    Copyright &copy; ${date.year + 1900} The Regents of the University of California, Davis campus. All Rights Reserved.<br>
    <a href="<c:url value='/help/about_this_site.html'/>"
       title="About This Site"
       target="mivhelp">About This Site</a>&nbsp;|&nbsp;MIV Version:&nbsp;${MIVSESSION.information.mivVersion}/${MIVSESSION.information.buildRevision}
    <address><a href="mailto:miv-help@ucdavis.edu" title="Contact the MIV Project Team">Contact the MIV Project Team</a></address>
</div><!-- footer -->

<script type="text/javascript">
/* jQuery('body').addClass("${user.userInfo.person.primaryRoleType}").addClass("${MIVSESSION.config.productionServer ? 'prod' : 'non-prod'}"); */
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("${MIVSESSION.config.gaTrackerCode}");
pageTracker._initData();
pageTracker._trackPageview();
</script>
