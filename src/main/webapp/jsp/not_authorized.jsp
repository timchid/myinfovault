<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ page isErrorPage="true" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; Not Authorized</title>

    <%@ include file="/jsp/metatags.html" %>
    <c:set var="errmsg" value="${requestScope['javax.servlet.error.message']}"/>
    <style>
        div#icon {
            float: left;
        }
        div#message {
            float: left;
            margin-left: 2em;
        }
        div#decoration {
            clear: both;
        }
    </style>
</head>
<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    </div><!-- breadcrumbs -->


    <div id="main">
        <h1>Unauthorized</h1>
        <div id="icon">
            <img src="<c:url value='/images/important.jpg'/>" alt="Important!">
        </div>

        <div id="message">
            <p>
            <strong>We're sorry, you are not authorized to perform this action.</strong><br>
            </p>
            <c:if test="${errmsg != ''}"><p class="customerror"><strong>${errmsg}</strong></p></c:if>
            <p>
            Contact the MIV Project Team at
            <a href="mailto:miv-help@ucdavis.edu" title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>
            if you think this is in error.
            </p>
        </div>

        <div id="decoration">
            <img src="<c:url value='/images/egghead2.jpg'/>"
                 alt="Robert Arneson's Egghead ceramic sculpture located at UC Davis">
        </div>
    </div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vi: se ts=8 sw=4 sr et:
--%>
