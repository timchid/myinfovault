<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>
   
<script type="text/javascript"><!--
    mivFormProfile = {
      required: [ ${constants.config.required} ],
      trim: ["year","content"],
      constraints: {
          year: function(contents) {
              var curYear = new Date().getFullYear();
              
              if(isNaN(contents)){
              	return "INVALID_NUMBER";
              }
              
              var numyear = contents * 1;
              
              if( numyear < 1900 || numyear > (curYear +1) ){
              	return "INVALID_YEAR";
              }
              
              return true;
          }
        },
        errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}" }
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<p class="formhelp">
Enter your special advising responsibilities during the review period
(For example, master advisor, chair of advising committee, etc.).
</p>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="typeid" name="typeid" value="1">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Academic Year -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  <input type="text" id="year" name="year" size="5" maxlength="4"
			value="${rec.year}" title="Year must be from 1900 to ${thisYear+1}">
</span>
</div><!-- formline -->

<!-- Textarea -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.content}">
  &#8224;&nbsp;
  <label for="content" class="f_required">${constants.labels.content}</label><br />
  <textarea
            id="content" name="content"
            rows="4" cols="60" wrap="soft"
            >${rec.content}</textarea>
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
