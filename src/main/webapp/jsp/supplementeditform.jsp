<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>
   
<script type="text/javascript">
/* <![CDATA[ */
    mivFormProfile = {

        required: [ ${constants.config.required} ],
        trim: ["year","hours","title","locationdescription","termtypeid","monthid"],

        constraints: {
        	year: function(contents) {
                var curYear = new Date().getFullYear();
                
                if(isNaN(contents)){
                	return "INVALID_NUMBER";
                }
                
                var numyear = contents * 1;
                
                if( numyear < 1900 || numyear > (curYear +1) ){
                	return "INVALID_YEAR";
                }
                
                return true;
            },
            termtypeid : function(contents) {

                mivFormProfile.flagMonthid = dojo.byId('monthid').value > 0;
                mivFormProfile.flagTerm = dojo.byId('termtypeid').value > 0;
                var valid = mivFormProfile.flagTerm || mivFormProfile.flagMonthid;
                if (!valid)
                {
                	return "INVALID_TERM_TYPE";
                    //alert("Either the TERM or the MONTH needs to be entered.\n Please review the fields and SAVE.");
                }
                /*var valid = contents > 0;
                if (valid && mivFormProfile.flagTerm)
                {
                    //this.flagTerm = true;
                    if (!mivFormProfile.flagMonthid)
                    {
                        var requiredFields = mivFormProfile.required;
                        for (var p=0; p<mivFormProfile.required.length; p++)
                        {
                            if (mivFormProfile.required[p] == "monthid")
                            {
                                mivFormProfile.required[p] = null;
                                break;
                            }
                        }
                    }
                    else
                    {
                        valid = false;
                        alert("Either the TERM or the MONTH needs to be entered.\n Please review the fields and SAVE.");
                    }
                }
                else if (!mivFormProfile.flagTerm)
                {
                    //this.flagTerm = false;
                    if (!mivFormProfile.flagMonthid)
                    {
                        mivFormProfile.required = [${constants.config.required}];
                    }
                    //valid = true;
                }*/
                return valid;
            }/*,
            monthid : function(contents) {
                mivFormProfile.flagMonthid = dojo.byId('monthid').value > 0;
                mivFormProfile.flagTerm = dojo.byId('termtypeid').value > 0;
                var valid = mivFormProfile.flagTerm || mivFormProfile.flagMonthid;
                if (!valid)
                {
                	return "INVALID_MONTH";
                }
                return valid;
            }*/
        },
        errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}",
        		   "INVALID_TERM_TYPE" : "Either the TERM or the MONTH needs to be entered.\n Please review the fields and SAVE.",
        		   "INVALID_MONTH" : "Either the TERM or the MONTH needs to be entered.\n Please review the fields and SAVE."}
    };
/* ]]> */
</script>

<div>
    <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
    <div id="links">
        <%-- <div id="specchar"><a href="#">Add Special Characters</a></div>
             div id="format"><a href="#">Bold / Italic / Underline</a></div --%>
    </div>
</div>
<p class="formhelp">
This data appears in the Teaching Supplement (in the dossier).
</p>
<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Type -->
<div class="formline">
<span class="dropdown" title="${constants.tooltips.typeid}">
    <label for="typeid">${constants.labels.typeid}</label>
    <select id="typeid" name="typeid">
    <c:forEach var="opt" items="${constants.options.teachingtype}"><c:if test="${(opt.id>=2 && opt.id<=4) || opt.id==11}"><%--
--%>  <option value="${opt.id}"${rec.typeid==opt.id?' selected="selected"':''}>${opt.name}</option>
    </c:if><%--
--%></c:forEach><%--
--%></select>
</span>
</div><!-- formline -->

<div class="formline">
<!-- Year -->
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  <input type="text" id="year" name="year" size="5" maxlength="4"
			value="${rec.year}" title="Year must be from 1900 to ${thisYear+1}">
			
</span>

<span class="textfield" title="${constants.tooltips.hours}">
  <label for="hours">${constants.labels.hours}</label>
  <input type="text" class="f_decimal"
         id="hours" name="hours"
         size="6" maxlength="4"
         value="${rec.hours}"
         >
</span>
</div><!-- formline -->


<div class="formline">
<!-- Quarter -->
<span class="dropdown" title="${constants.tooltips.termtypeid}">
    <label for="termtypeid" errorlabel="Term (or Month)" class="f_required">${constants.labels.termtypeid}</label>
    <select id="termtypeid" name="termtypeid">
    <c:forEach var="opt" items="${constants.options.termtype}"><%--
--%>  <option value="${opt.id}"${rec.termtypeid==opt.id?' selected="selected"':''}>${opt.name}</option>
    </c:forEach><%--
--%></select>
</span>

<!-- Month -->
<span class="dropdown" title="${constants.tooltips.monthid}">
<span>or</span>
    <label for="monthid" errorlabel="Month (or Term)" class="f_required">${constants.labels.monthid}</label>
    <select id="monthid" name="monthid">
    <c:forEach var="opt" items="${constants.options.monthname}"><%--
--%>  <option value="${opt.id}"${rec.monthid==opt.id?' selected="selected"':''}>${opt.name}</option>
    </c:forEach><%--
--%></select>
</span>

<span>
Either the TERM or the MONTH needs to be entered.
</span>
</div><!-- formline -->


<!-- Title/Description -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.title}">
  &#8224;&nbsp;
  <label for="title" class="f_required">${constants.labels.title}</label><br />
  <textarea
            id="title" name="title"
            rows="3" cols="56" wrap="soft"
            >${rec.title}</textarea>
</span>
</div><!-- formline -->

<!-- Location -->
<div class="formline">

<span class="textfield" title="${constants.tooltips.locationdescription}">&#8224;&nbsp;
  <label for="locationdescription">${constants.labels.locationdescription}</label><br />
  <textarea
            id="locationdescription" name="locationdescription"
            rows="3" cols="56" wrap="soft"
            >${rec.locationdescription}</textarea>
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save">
  <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
