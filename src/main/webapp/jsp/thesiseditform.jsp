<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: ["startyear","endyear","studentname","position"],
        constraints: {
        	startyear: function(contents) {
                var curYear = new Date().getFullYear();
                
                if(isNaN(contents)){
                	return "INVALID_NUMBER";
                }
                
                var numyear = contents * 1;
                
                if( numyear < 1900 || numyear > curYear ){
                	return "INVALID_YEAR";
                }
                
                return true;
            },
            endyear: function(contents) {
                var curYear = new Date().getFullYear();
                
                if( contents.toLowerCase() != 'present' && isNaN(contents)){
                	return "INVALID_END_YEAR";
                }
                
                /* if( contents.toLowerCase() == 'present' ){
             	   contents = "${thisYear}";            	   
                } */
                
                var numyear = contents * 1;
                
                var startyearObj = dojo.byId('startyear');
                
                if( startyearObj && trim(startyearObj.value).length > 0 && !isNaN(startyearObj.value) 
             		   && numyear < startyearObj.value){
                	return "INVALID_END_YEAR_RANGE";
                }
                
                return true;
            }
          },
          errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear}.",
  	 		/* "INVALID_PERCENT":"Percent must be from 0 to 100.", */
  	 		"INVALID_END_YEAR":"Enter 4 digit year or word &quot;present&quot;.",
  	 		"INVALID_END_YEAR_RANGE":"End Year must be greater than or equal to Start Year."}
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<p class="formhelp">
Name of student(s) of thesis committee(s) on which the candidate has served or
is serving during the review period. Include only formal membership on
committees guiding thesis research. If a Master's candidate is in a department
requiring a field study or research paper in lieu of a thesis, the name of such
student(s) supervised should be indicated.<br/>
DO NOT INCLUDE SERVICE ON ORAL EXAMINATION COMMITTEES.
</p>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">
<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}"><%-- stun MIV-719 changed display value --%>

<div class="formline">
<!-- From Year -->
<span class="textfield" title="${constants.tooltips.startyear}">
  <label for="startyear" class="f_required">${constants.labels.startyear}</label>
  <input type="text"
         id="startyear" name="startyear"
         size="5" maxlength="4"
         value="${rec.startyear}"
         >
</span>

<!-- End Year -->
<span class="textfield" title="${constants.tooltips.endyear}">
  <label for="endyear" class="f_required">${constants.labels.endyear}</label>
  <input type="text"
         id="endyear" name="endyear"
         size="8" maxlength="7"
         value="${rec.endyear}"
         >
</span>
</div><!-- formline -->


<!-- Student Name-->
<div class="formline">
<span class="textfield" title="${constants.tooltips.studentname}">
  <label for="studentname" class="f_required">${constants.labels.studentname}</label>
  <input type="text"
         id="studentname" name="studentname"
         size="50" maxlength="50"
         value="${rec.studentname}"
         >
</span>
</div><!-- formline -->

<!-- Checkboxes goes HERE! -->
<div class="formline">
<span class="dropdown" title="${constants.tooltips.committeeposition}">
  <label for="committeeposition" class="f_required">${constants.labels.committeeposition}</label>
  <select id="committeeposition" name="committeeposition">
    <option value="1"<c:if test="${rec.committeemember=='1'||rec.committeemember=='true'}"> selected="selected"</c:if>>Member</option>
    <option value="2"<c:if test="${rec.committeechair=='1'||rec.committeechair=='true'}"> selected="selected"</c:if>>Chair</option>
    <option value="3"<c:if test="${rec.cochair=='1'||rec.cochair=='true'}"> selected="selected"</c:if>>Co-Chair</option>
  </select>
</span>

<span class="dropdown" title="${constants.tooltips.degreetypeid}">
  <label for="degreetypeid" class="f_required">${constants.labels.degreetypeid}</label>
  <select id="degreetypeid" name="degreetypeid">
    <option value="1"<c:if test="${rec.mastersdegree=='1'||rec.mastersdegree=='true'}"> selected="selected"</c:if>>Masters</option>
    <option value="3"<c:if test="${rec.doctoratedegree=='1'||rec.doctoratedegree=='true'}"> selected="selected"</c:if>>Doctorate</option>
    <option value="2"<c:if test="${rec.doctoraldegree=='1'||rec.doctoraldegree=='true'}"> selected="selected"</c:if>>Ph.D.</option>
  </select>
</span>
</div><!-- formline -->
<div class="formline">
<span class="dropdown" title="${constants.tooltips.degreestatusid}">
  <label for="degreestatusid" class="f_required">${constants.labels.degreestatusid}</label>
  <select id="degreestatusid" name="degreestatusid">
    <option value="1"<c:if test="${rec.degreeawarded=='0'||rec.degreeawarded=='false'}"> selected="selected"</c:if>>In Progress</option>
    <option value="2"<c:if test="${rec.degreeawarded=='1'||rec.degreeawarded=='true'}"> selected="selected"</c:if>>Awarded</option>
  </select>
</span>

</div><!-- formline -->

<!-- Current Position -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.position}">
  <label for="position">${constants.labels.position}</label>
  <input type="text"
         id="position" name="position"
         size="50" maxlength="50"
         value="${rec.position}"
         >
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save">
  <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
