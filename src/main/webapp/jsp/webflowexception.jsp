<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; Error: Alert</title>

    <%@ include file="/jsp/metatags.html" %>

    <style>
        #main > h1 {
            margin-bottom: 0.5em;
        }
        #main > img.icon {
            display: inline-block;
            height: 64px;
            width: 64px;
            vertical-align: top;
        }
        div#message {
            display: inline-block;
            margin-left: 2em;
        }
        div#message > p {
            max-width: 45em;
        }
        div#message > p:first-child {
            margin-top: 0;
        }
        #decoration > img {
            border: none;
        }
    </style>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    </div><!-- breadcrumbs -->


    <div id="main">
        <h1>Error: Alert</h1>

        <img class="icon" src="<c:url value='/images/alert_black-64.png'/>" alt="Important!">

        <div id="message">
            <p>
            <strong>We're sorry, but we've lost track of what you were trying to get done.</strong>
            </p>
            <p>
            If you have left the system idle this error may occur.
            Please make another selection from the top navigation menu to perform another action.
            </p>
            <p>
            Contact the MIV Project Team at
            <a href="mailto:miv-help@ucdavis.edu" title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>
            if you need further assistance.
            </p>
        </div>

        <div id="decoration">
            <img src="<c:url value='/images/egghead2.jpg'/>"
                 alt="Robert Arneson's Egghead ceramic sculpture located at UC Davis">
        </div>
    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vi: se ts=8 sw=4 sr et:
--%>
