<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: ["datespan", "title", "audience", "location", "headcount" ]
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Dates -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.datespan}">
  <label for="datespan" class="f_required">${constants.labels.datespan}</label>
  <input type="text"
         id="datespan" name="datespan"
         size="30" maxlength="50"
         value="${rec.datespan}"
         >
</span>
</div><!-- formline -->

<!-- Topic/Title -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.title}">
  <label for="title" class="f_required">${constants.labels.title}</label>
  <input type="text"
         id="title" name="title"
         size="54" maxlength="255"
         value="${rec.title}"
         >
</span>
</div><!-- formline -->

<!-- Role -->
<div class="formline">
<span class="dropdown" title="${constants.tooltips.gatheringroleid}">
    <label for="gatheringroleid" class="f_required">${constants.labels.gatheringroleid}</label>
    <select id="gatheringroleid" name="gatheringroleid"><%--
    --%><c:forEach var="opt" items="${constants.options.gatheringrole}">
        <option value="${opt.id}"${rec.gatheringroleid==opt.id?' selected="selected"':''}>${opt.name}</option><%--
    --%></c:forEach>
    </select>
</span>
</div><!-- formline -->

<!-- Audience: -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.audience}">
  <label for="audience">${constants.labels.audience}</label>
  <input type="text"
         id="audience" name="audience"
         size="54" maxlength="255"
         value="${rec.audience}"
         >
</span>
</div><!-- formline -->

<!-- Location -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.location}">
  <label for="location">${constants.labels.location}</label>
  <input type="text" class=""
         id="location" name="location"
         size="54" maxlength="255"
         value="${rec.location}"
         >
</span>
</div><!-- formline -->

<!-- Number In Attendence -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.headcount}">
  <label for="headcount">${constants.labels.headcount}</label>
  <input type="text" class="f_numeric"
         id="headcount" name="headcount"
         size="6" maxlength="10"
         value="${rec.headcount}"
         >
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save">
  <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
