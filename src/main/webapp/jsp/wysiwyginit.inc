<script type="text/javascript" src="/miv/js/tiny_mce/tiny_mce.js"></script>

<style>
#main .mivDialog {
    max-width: 80em;
    min-width: 64em;
    width: auto;
}
</style>

<script type="text/javascript">

// get the content from container and set into relative textareas
$(document).ready(function()
{
    $('#mytestform').submit(function()
    {
        // process all wysiwygs and place values into holding textarea
        $('textarea.mceEditor').each(function(index)
        {
            var content = mivCleanup(tinyMCE.get(this.id).getContent());
            if ($('#'+this.id+"_mce").val() == undefined) {
                $('#'+this.id).after("<textarea id='"+this.id+"_mce' name='"+this.id+"_mce' style='display: none;'></textarea>");
            }
            $('#'+this.id+"_mce").val(content);
        });
    });

    $('div#lineBreakNote .description').html('Field supports line breaks (by pressing <em>Shift+Enter<em>), and the data will appear with line breaks in MIV documents.');
});

tinyMCE.init(
{
        // General options
        mode : "textareas",
        theme : "advanced",
        editor_selector : "mceEditor",
        entity_encoding : "raw",

        // Theme options
        plugins : "table,style,lists,safari,inlinepopups,insertdatetime,directionality,preview,contextmenu,noneditable,nonbreaking,xhtmlxtras,paste",

        // theme_advanced_buttons1 :
        // "newdocument,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,sub,sup,|,charmap,iespell,ltr,rtl,|,insertdate,inserttime",
        // theme_advanced_buttons2 : "tablecontrols,|,removeformat,visualaid",
        theme_advanced_buttons1 : "bold,italic,underline,|,hr,removeformat,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,pasteword,|,",

        theme_advanced_buttons2 : "",
        // theme_advanced_buttons2 : "preview",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",

        theme_advanced_buttons1_add : "table,delete_table,|,row_before,row_after,delete_row,|,col_before,col_after,delete_col,|,sub,sup,|,charmap",
        theme_advanced_statusbar_location : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_resizing : false,
        debug : false,
        add_form_submit_trigger : true,
        fix_list_elements : true,
        resizable : false,
        cleanup_on_startup : true,
        onchange_callback : "customOnChangeHandler",
        init_instance_callback : customInit,
        width: "100%",
        height: "15em",
        content_css : "/miv/css/wysiwyg.css",
        tab_focus : "save",
        valid_elements : "" +
          "-strong/-b," +
          "-em/-i," +
          "-hr," +
          "-u," +
          "p[align|style]," +
          "-ol," +
          "-ul," +
          "-li[style|class]," +
          "br," +
          "-sub," +
          "-sup," +
          "-blockquote," +
          "table[border=0|cellpadding=2|width=510|style]," +
          "tbody," +
          "tr[height|style]," +
          "td[width|height|valign|align|style]," +
          "-span[style]" +
          ""
});

/* Custom Init Function to create ContentHolder for wysiwyg editor */
function customInit(inst) {
    if($('#'+inst.id+"_mce").val() == undefined) {
        $('#'+inst.id).after("<textarea id='"+inst.id+"_mce' name='"+inst.id+"_mce' style='display: none;'></textarea>");
    }
    $('#'+inst.id+"_mce").val($('#'+inst.id).text());
}


/* process wysiwyg and place values into holding textarea */
function customOnChangeHandler(inst)
{
    var content = mivCleanup(inst.getBody().innerHTML);
    $('#'+inst.id+"_mce").val(content);
}


// Adding the '#' to "td" as shown below might eliminate the need for the
// "<td>\s*<\/td>" rule in mivCleanup()
// "#td[width|valign|align]," +
function mivCleanup(html)
{

    if (html)
    {
        html = html.replace(/&nbsp;/g,' ');
        html = html.replace(/(.*?) style=("|&quot)(.*?)text-align: (right|left|center|justify);(.*?)("|&quot)(.*?)/ig,'$1 align=\"$4\" style=\"$3 $5\" $7');
        html = html.replace(/<span style=("|&quot;)text-decoration: underline;("|&quot;)>(.*?)<\/span>/ig,'<u>$3</u>');
        html = html.replace(/<[ou]l>\s*<\/[ou]l>/ig, "");
        html = html.replace(/<tbody>\s*<\/tbody>/ig, "");
        html = html.replace(/<hr>/g, "<hr />");
        html = html.replace(/<table[^>]*>\s*<\/table>/ig, "");
        html = html.replace(/<td>\s*<\/td>/g,"<td>&nbsp;</td>");
        // FF has "mce_bogus" br tags because we've forced it into "raw" mode. Drop them.
        html = html.replace(/<br\s+mce_bogus[^>]*>/ig, "");
        html = html.replace(/<br\s+data-mce-bogus[^>]*>/ig, "");
        // html = html.replace(/<sup>\s*/ig, "<sup>");html = html.replace(/\s*<\/sup>/ig, "</sup>");
        // html = html.replace(/<sub>\s*/ig, "<sub>");html = html.replace(/\s*<\/sub>/ig, "</sub>");
        html = html.replace(/<p>\s*<\/p>/ig, "<p><br /></p>");
        // Now if we've removed all the style rules, drop the style attribute too
        html = html.replace(/ +style="\s*"/ig, "");
    }
    return html;
}
</script>
