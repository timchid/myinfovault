<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ tag import="edu.ucdavis.myinfovault.*" %>
<jsp:useBean id="dsm" scope="session" class="edu.ucdavis.myinfovault.DataSetManager"/>
<% UserDataSet ds = DataSetManager.getDataSet(18099); %>
<c:set var="ds1" scope="session" value="<%=DataSetManager.getDataSet(18099)%>"/>
<div class="foreachtag" id="foreach<%=ds.getId()%>" style="border: 1px dotted #800;margin-top:-15px;">
<% out.println("out.println(ds.getId()); ==&gt; " + ds.getId()); %>
 <span style="color:#f03030;">Here begins the stuff from inside the miv:forEach.tag file</span>
 <span style="color:#3030f0">ds1.id==${ds1.id}</span>
 <br>
 Below is the c:forEach loop using ds1.journals:<br>
 <ul>
 <c:forEach var="entry" items="${ds1.journalsTest}">
  <li>
   <span class="year">${entry.year}</span>
   <c:if test="${fn:length(fn:trim(entry.reference)) > 0}" >
   <span class="reference">[${entry.reference}]</span>
   </c:if>
   <span class="author">${entry.authors}</span>
   <span class="title">${entry.title}</span>
   <span class="inpress">${entry.pressState}</span>
   <span class="recid">(record number ${entry.recid})</span>
  </li>
 </c:forEach>
 </ul>
 <jsp:doBody/>
</div><!-- class="foreachtag" -->
