<%@ tag body-content="scriptless" %><%--
--%><%@ attribute name="required" required="false" %><%--
--%><%@ attribute name="matches" required="false" %><%--
--%><%@ attribute name="roles" required="false" %><%--
--%><%@ attribute name="allow" required="false" %><%--
--%><%@ attribute name="usertype" required="false" %><%--
--%><%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ tag import="edu.ucdavis.myinfovault.*" %><%--
--%><%


// Get any parameters provided: required, roles, and usertype
// use 'roles' if both found once roles is implemented
String required = (String)jspContext.findAttribute("required");
String roles    = (String)jspContext.findAttribute("roles");
String user = (String)jspContext.findAttribute("usertype");
String matches = (String)jspContext.findAttribute("matches");

edu.ucdavis.myinfovault.MIVSession mivSession =
    (edu.ucdavis.myinfovault.MIVSession)session.getAttribute("MIVSESSION");

edu.ucdavis.myinfovault.MIVUser mivUser = null;
AccessLvl userLevel = null;

// If there's no session we're definitely not allowed. Return immediately.
if (mivSession == null) return;

// Get the user, return immediately if none.
mivUser = mivSession.getUser();
if (mivUser == null) return;

// Get the current users (old-style) access level
if ("REAL".equalsIgnoreCase(user))
{
    userLevel = mivUser.getAccess();
}
else
{
    userLevel = mivUser.getTargetUserInfo().getAccess();
}

/* Test here for the params set. */
/* For the moment we'll just pretend it's always "required" */

/* 'roles' should allow comma, plus, and parentheses; such as:
      "MIVADMIN,((DEPTADMIN,SCHOOLADMIN)+HELPER)"
    which would mean "allow if the user has the MIVAdmin role,
    and allow if user has either Dept. or School admin along
    with the 'helper' role.  Not a very good example in terms
    of the current meanings of access levels, but you get
    the picture.
*/
if (required == null) {
    AccessLvl matchLevel = AccessLvl.parse(matches);
    int result = userLevel.compareTo(matchLevel);
    if (result == 0)
    {
        %><jsp:doBody/><%
    }
}
else {
    AccessLvl requiredLevel = AccessLvl.parse(required);
int result = userLevel.compareTo(requiredLevel);
if (result >= 0)
{
%><jsp:doBody/><%
}
else
{
    /* In Real-Life we won't print anything here... there'd be no "else" */
/*
    out.println("<div style=\"color:red; font-weight:bold;\">Request Denied!<br/>");
    out.println("You need " + requiredLevel + " to see this, but you only have " + userLevel + "<br/>");
    out.println("You are " + (-result) + " Levels too low.</div>");
    out.write("<!-- Not Allowed: Need level " + requiredLevel +
                ", Have userLevel " + userLevel + " : " + (-result) + " Levels too low. -->");
*/
}
}
%>
