<%@ tag body-content="scriptless" %>
<%@ attribute name="required" required="false" %>
<%@ attribute name="roles" required="false" %>
<%@ attribute name="allow" required="false" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag import="edu.ucdavis.myinfovault.*" %>
<c:set var="taguser" value="${MIVSESSION.user}"/>
<div>
<h4>EL Part of the tag</h4>
"required"=[<strong>${required}</strong>]<br/>
"roles"=[<strong>${roles}</strong>]<br/>
user access level: [<strong>${user.access}</strong>]<br/>
</div>
<div>
<h4>Scriptlet Part of the tag</h4>
<%
 edu.ucdavis.myinfovault.MIVSession s = (edu.ucdavis.myinfovault.MIVSession)session.getAttribute("MIVSESSION");
 out.println("scriptlet says MIVSession is "+s+"<br/>");
 edu.ucdavis.myinfovault.MIVUser u = s.getUser();
 out.println("scriptlet says MIVUser is "+u+"<br/>");
 AccessLvl l = u.getAccess();
 out.println("scriptlet says AccessLvl is "+l+"<br/>");
 if (l.ordinal() > AccessLvl.DEPT_ADMIN.ordinal()) {
     out.println("scriptlet says: \"Wow, you've got some power!\"<br/>");
 }
 int result = (AccessLvl.USER).compareTo(AccessLvl.MIV_ADMIN);
 out.println(" &nbsp; result of compare: "+result+"<br/>");
 out.println("jspContext: "+jspContext+"<br/>");
 String required = (String)jspContext.findAttribute("required");
 String roles = (String)jspContext.findAttribute("roles");
 out.println("findAttribute got ["+required+"] and ["+roles+"]<br/>");

int t = -1;
t = l.compareTo(AccessLvl.MIV_ADMIN);
out.println("compare to MIV_ADMIN gave "+t+"<br/>");
t = l.compareTo(AccessLvl.MIV_ADMIN_HELPER);
out.println("compare to MIV_ADMIN_HELPER gave "+t+"<br/>");
t = l.compareTo(AccessLvl.USER);
out.println("compare to USER gave "+t+"<br/>");

AccessLvl testlvl = AccessLvl.parse(required);
out.println("parsed ("+required+") and got ("+testlvl+")<br/>");
 result = l.compareTo(testlvl);
 if (result >= 0) {
     out.write("It's Allowed");
     if (result > 0) {
         out.write(", with "+result+" levels to spare");
     }
     out.println("!<br/>");
 }
 else {
     out.println("You're NOT Allowed!");
     out.println("You are "+(-result)+" Levels too low.");
 }
%>
</div>
<div style="background-color: #e7ffe7;"><jsp:doBody/></div>
