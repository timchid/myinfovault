<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <!-- Reviews -->
  <xsl:template match="reviews">
    <fo:block space-before="12pt" margin-left="{$content.indent}">
      <fo:block xsl:use-attribute-sets="subheader.format" space-after="12pt" keep-with-next="1">
        <xsl:value-of select="section-header"/>
      </fo:block>
      <xsl:apply-templates select="reviews-record" />
    </fo:block>
  </xsl:template>

  <xsl:template match="reviews-record">
    <fo:block padding-bottom="2pt">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="15%" />
        <fo:table-column column-width="85%" />
        <fo:table-body start-indent="0pt">
          <fo:table-row>
            <xsl:choose>
            <!-- category 1 = print review -->
            <xsl:when test="categoryid = '1'">
              <fo:table-cell>
                <fo:block text-align="left">
                  <xsl:apply-templates select="printreviewyear" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell>
                  <fo:block text-align="left">
                  <xsl:apply-templates select="reviewer" />
                  <xsl:apply-templates select="title" />
                  <xsl:apply-templates select="description" />
                  <xsl:apply-templates select="publication" />
                  <xsl:apply-templates select="volume" />
                  <xsl:apply-templates select="issue" />
                  <xsl:apply-templates select="pages" />
                  <xsl:apply-templates select="city" />
                  <xsl:apply-templates select="province" />
                  </fo:block>
              </fo:table-cell>
            </xsl:when>
            <!-- otherwise digital review -->
            <xsl:otherwise>
              <fo:table-cell>
                <fo:block text-align="left">
                  <xsl:apply-templates select="reviewdate" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell>
                <fo:block text-align="left">
                  <xsl:apply-templates select="reviewer" />
                  <xsl:apply-templates select="venue" />
                  <xsl:apply-templates select="description" />
                  <xsl:apply-templates select="city" />
                  <xsl:apply-templates select="province" />
                </fo:block>
              </fo:table-cell>
            </xsl:otherwise>
            </xsl:choose>    
           </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
               <fo:block></fo:block>
            </fo:table-cell>
            <xsl:choose>
              <xsl:when test="link">
                <fo:table-cell text-align="left">
                  <xsl:apply-templates select="link"> 
                    <xsl:with-param name="urldescription" select="'View Review Information'" />
                    <xsl:with-param name="urlvalue"><xsl:value-of select="link"/></xsl:with-param>
                  </xsl:apply-templates>
                </fo:table-cell>
                </xsl:when>
                <xsl:otherwise>
                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>
                </xsl:otherwise>
            </xsl:choose>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:block>
  </xsl:template>
</xsl:stylesheet>
