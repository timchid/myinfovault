<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY comma ",">
 ]>
 <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="education">
        <fo:block margin-left="{$content.indent}">
            <xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="education-record">
            <fo:table table-layout="fixed" width="100%" space-after="12pt">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>
                <fo:table-body start-indent="0pt">
                    <xsl:apply-templates select="education-record"/>
                </fo:table-body>
            </fo:table>            
            </xsl:if>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="education-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="startdate" mode="education"/>-<xsl:apply-templates select="enddate" mode="education"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="institution" mode="education"/><xsl:apply-templates select="location" mode="education"/>
                    <xsl:apply-templates select="degree" mode="education"/>
                    <xsl:apply-templates select="field" mode="education"/><xsl:apply-templates select="sponsor" mode="education"/>
                    <xsl:apply-templates select="remark" mode="education"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    <xsl:template match="startdate" mode="education">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="enddate" mode="education">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="institution" mode="education">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="location" mode="education">
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="degree" mode="education">
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="field" mode="education" >
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="sponsor" mode="education">
        <fo:inline>&space;(<xsl:apply-templates />)</fo:inline>
    </xsl:template>
    <xsl:template match="remark" mode="education">
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
</xsl:stylesheet>
