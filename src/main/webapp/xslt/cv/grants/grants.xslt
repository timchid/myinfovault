<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY colon ":">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!-- GRANTS -->

    <xsl:template match="grants-active|grants-pending|grants-completed|grants-notawarded|grants-none">
        <xsl:call-template name="grants"/>
    </xsl:template>

    <xsl:template name="grants">
        <fo:block margin-left="{$content.indent}">
            <xsl:call-template name="subsection-header">
                 <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            
            <xsl:if test="grant-record">
                <fo:table  table-layout="fixed" width="100%" space-after="12pt">
                    <fo:table-column column-width="20%"/>
                    <fo:table-column column-width="80%"/>
                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="grant-record"/>
                    </fo:table-body>
                </fo:table>
            </xsl:if>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="grant-record">
        <fo:table-row >
            <fo:table-cell padding-bottom="12pt">
                <fo:block>
                    <xsl:choose>
                        <xsl:when test="child::startdate">
                            <xsl:apply-templates select="startdate" mode="grants"/>

                            <xsl:if test="child::enddate">
                                <xsl:text> - </xsl:text>
                                <xsl:apply-templates select="enddate" mode="grants"/>
                            </xsl:if>
                        </xsl:when>
                        
                        <xsl:otherwise>
                            <xsl:apply-templates select="submitdate" mode="grants"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </fo:block>
            </fo:table-cell>
            
            <fo:table-cell padding-bottom="12pt">
                <fo:block>
                    <xsl:if test="grantnumber">
                        <xsl:apply-templates select="grantnumber"/>
                                   
                        <xsl:if test="grantamount or rolename or granttitle or principalinvestigator or grantsource or effort">
                           <fo:inline>&comma;&space;</fo:inline>
                        </xsl:if>
                    </xsl:if>
                    
                    <xsl:if test="grantamount">
                        <xsl:apply-templates select="grantamount"/>
                        
                        <xsl:if test="rolename or granttitle or principalinvestigator or grantsource or effort">
                           <fo:inline>&comma;&space;</fo:inline>
                        </xsl:if>
                    </xsl:if>
                    
                    <xsl:if test="rolename">
                        <xsl:apply-templates select="rolename" mode="grants"/>
                        
                        <xsl:if test="granttitle or principalinvestigator or grantsource or effort">
                            <fo:inline>&comma;&space;</fo:inline>
                        </xsl:if>
                    </xsl:if>
                    
                    <xsl:if test="granttitle">
                        <xsl:apply-templates select="granttitle"/>
                        
                        <xsl:if test="principalinvestigator or grantsource or effort">
                            <fo:inline>&comma;&space;</fo:inline>
                        </xsl:if>
                    </xsl:if>
                    
                    <xsl:if test="principalinvestigator">
                        <xsl:apply-templates select="principalinvestigator"/>
                        
                        <xsl:if test="grantsource or effort">
                            <fo:inline>&comma;&space;</fo:inline>
                        </xsl:if>
                    </xsl:if>
                    
                    <xsl:if test="grantsource">
                        <xsl:apply-templates select="grantsource"/>
                        
                        <xsl:if test="effort">
                             <fo:inline>&comma;&space;</fo:inline>
                        </xsl:if>
                    </xsl:if>
                    
                    <xsl:if test="effort">
                        <xsl:apply-templates select="effort" mode="grants"/>
                    </xsl:if>
                </fo:block>
                
                <xsl:variable name="value" select="."/>
                <xsl:variable name="lastCharacters" select="substring($value, string-length($value)-1)" />
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <!-- GRANT DATA TAGS -->

    <xsl:template match="granttype|granttitle|grantdescription|grantsource">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>

    <xsl:template match="grantamount">
        <fo:inline>$<xsl:apply-templates/></fo:inline>
    </xsl:template>

    <xsl:template match="submitdate|startdate|enddate" mode="grants">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="principalinvestigator">
        <fo:inline><xsl:apply-templates/>&#160;(Principal Investigator)</fo:inline>
    </xsl:template>

    <xsl:template match="statusname" mode="grants">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="rolename" mode="grants">
        <fo:inline><xsl:apply-templates/></fo:inline>
    </xsl:template>

    <xsl:template match="effort" mode="grants">
        <fo:inline>Percentage Effort=<xsl:apply-templates/>%</fo:inline>
    </xsl:template>
    
    <xsl:template match="grantnumber">
        <fo:inline>Grant #<xsl:apply-templates/></fo:inline>
    </xsl:template>
</xsl:stylesheet>
