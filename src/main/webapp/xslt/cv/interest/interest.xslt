<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="interest-area">    
        <fo:block margin-left="{$content.indent}" space-before="12pt">
        		<xsl:if test="$interest-area='true'">
        		<xsl:call-template name="subsection-header">
                <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
           </xsl:if>
            <xsl:apply-templates select="interest-record"/>
        </fo:block>
    </xsl:template>
    <xsl:template match="interest-record">
        <fo:block font-weight="bold" space-before="24pt" space-after="12pt">
            <xsl:apply-templates select="header"/>
        </fo:block>
        <fo:block space-after="12pt">
            <xsl:apply-templates select="content"/>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>
