<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!-- Extending Knowledge-->
    <xsl:template match="title" mode="knowledge">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    
    <xsl:template match="description" mode="knowledge">
        <fo:inline>, <xsl:apply-templates /></fo:inline>
    </xsl:template>
    
    <xsl:template match="datespan" mode="knowledge">
        <xsl:choose>
            <xsl:when test="parent::knowledge-other-record"><xsl:apply-templates /></xsl:when>
            <xsl:otherwise>, <xsl:apply-templates /></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="publisher" mode="knowledge">
        <fo:inline>, <xsl:apply-templates /></fo:inline>
    </xsl:template>
    
    <xsl:template match="name" mode="knowledge">
        <fo:inline>, <xsl:apply-templates /></fo:inline>
    </xsl:template>
    
    <xsl:template match="audience" mode="knowledge">
        <fo:inline>, <xsl:apply-templates /></fo:inline>
    </xsl:template>
    
    <xsl:template match="location" mode="knowledge">
        <fo:inline>, <xsl:apply-templates /></fo:inline>
    </xsl:template>
    
    <xsl:template match="headcount" mode="knowledge">
        <fo:inline>, <xsl:apply-templates /> Attendees</fo:inline>
    </xsl:template>
    
    <xsl:template match="comment" mode="knowledge">
        <fo:inline>, <xsl:apply-templates /></fo:inline>
    </xsl:template>
    </xsl:stylesheet>