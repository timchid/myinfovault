<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="media-published">
        <fo:block id="media-published" space-before="12pt" margin-left="{$content.indent}">
           <xsl:call-template name="subsection-header">
             <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
          </xsl:call-template>
          <xsl:apply-templates select="media-record"/>
        </fo:block>
    </xsl:template>
    </xsl:stylesheet>