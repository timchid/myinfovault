<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
    <xsl:template match="service-administrative">
        <fo:block id="service-administrative" margin-left="{$content.indent}">
            <xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="administrativeactivity-record">
            <fo:block space-after="12pt">
                <fo:table table-layout="fixed" width="100%">
                    <fo:table-column column-width="20%"/>
                    <fo:table-column column-width="80%"/>
                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="administrativeactivity-record"/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
            </xsl:if>
        </fo:block>
    </xsl:template>

    <xsl:template match="administrativeactivity-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="startyear"/>-<xsl:apply-templates select="endyear"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="title"/>
                    <xsl:if test="percenteffort">, <xsl:apply-templates select="percenteffort"/>% effort</xsl:if>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
</xsl:stylesheet>
