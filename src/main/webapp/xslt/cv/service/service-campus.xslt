<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="service-campus">
        <fo:block id="service-campus" margin-left="{$content.indent}">
            <xsl:call-template name="secsubsection-header">
                <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            
            <xsl:call-template name="committee-records"/>
        </fo:block>                                    
    </xsl:template>
</xsl:stylesheet>