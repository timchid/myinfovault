<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
    <xsl:template match="teaching-additional" mode="teaching-additional">
        
        <!-- ADDITIONAL INFORMATION -->
        <fo:block id="teaching-additional" space-before="12pt" margin-left="{$content.indent}">
        		<xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="additional-record" mode="teaching-additional"/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="additional-record" mode="teaching-additional">
        <fo:block>
            <xsl:apply-templates select="addheader"/>
            <xsl:apply-templates select="addcontent"/>
            <fo:table table-layout="fixed" width="100%">
                <xsl:choose>
                    <xsl:when test="colpercent = 5">
                        <fo:table-column column-width="5%"/>
                        <fo:table-column column-width="95%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 10">
                        <fo:table-column column-width="10%"/>
                        <fo:table-column column-width="90%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 15">
                        <fo:table-column column-width="15%"/>
                        <fo:table-column column-width="85%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 20">
                        <fo:table-column column-width="20%"/>
                        <fo:table-column column-width="80%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 30">
                        <fo:table-column column-width="30%"/>
                        <fo:table-column column-width="70%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 35">
                        <fo:table-column column-width="35%"/>
                        <fo:table-column column-width="65%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 40">
                        <fo:table-column column-width="40%"/>
                        <fo:table-column column-width="60%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 45">
                        <fo:table-column column-width="45%"/>
                        <fo:table-column column-width="55%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 50">
                        <fo:table-column column-width="50%"/>
                        <fo:table-column column-width="50%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 55">
                        <fo:table-column column-width="55%"/>
                        <fo:table-column column-width="45%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 60">
                        <fo:table-column column-width="60%"/>
                        <fo:table-column column-width="40%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 65">
                        <fo:table-column column-width="65%"/>
                        <fo:table-column column-width="35%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 70">
                        <fo:table-column column-width="70%"/>
                        <fo:table-column column-width="30%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 75">
                        <fo:table-column column-width="75%"/>
                        <fo:table-column column-width="25%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 80">
                        <fo:table-column column-width="80%"/>
                        <fo:table-column column-width="20%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 85">
                        <fo:table-column column-width="85%"/>
                        <fo:table-column column-width="15%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 90">
                        <fo:table-column column-width="90%"/>
                        <fo:table-column column-width="10%"/>
                    </xsl:when>
                    <xsl:when test="colpercent = 95">
                        <fo:table-column column-width="95%"/>
                        <fo:table-column column-width="5%"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <fo:table-column column-width="25%"/>
                        <fo:table-column column-width="75%"/>
                    </xsl:otherwise>
                </xsl:choose>
                <fo:table-body start-indent="0pt">
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:apply-templates select="column1"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:apply-templates select="column2"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>
    </xsl:stylesheet>