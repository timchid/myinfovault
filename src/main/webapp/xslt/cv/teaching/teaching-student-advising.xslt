<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="teaching-student-advising">
        <fo:block id="student-advising" space-before="12pt" margin-left="{$content.indent}">
            <xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="student-advising-record">
            <fo:block>
                <fo:table table-layout="fixed" width="100%" space-after="12pt">
                    <fo:table-column column-width="20%"/>
                    <fo:table-column column-width="80%"/>

                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="student-advising-record"/>

                    </fo:table-body>
                </fo:table>
            </fo:block>
            </xsl:if>
        </fo:block>
    </xsl:template>

    <xsl:template match="student-advising-record">

        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="year" mode="teaching-student-advising"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="advise1" mode="teaching"/>
                    <xsl:apply-templates select="ugnum" mode="teaching"/>
                    <xsl:apply-templates select="advise2" mode="teaching"/>
                    <xsl:apply-templates select="gnum" mode="teaching"/>
                    <xsl:if test="a3num != 0">
	                    <xsl:apply-templates select="advise3" mode="teaching"/>
	                    <xsl:apply-templates select="a3num" mode="teaching"/>
                    </xsl:if>
                    <xsl:if test="a4num != 0">
	                    <xsl:apply-templates select="advise4" mode="teaching"/>
	                    <xsl:apply-templates select="a4num" mode="teaching"/>
                    </xsl:if>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    <xsl:template match="year" mode="teaching-student-advising">
        <fo:inline><xsl:value-of select="."/> - <xsl:value-of select=".+1"/></fo:inline>        
    </xsl:template>
    <xsl:template match="advise1" mode="teaching">
        <fo:inline>
           <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="ugnum" mode="teaching">
        <fo:inline> (<xsl:apply-templates/>)</fo:inline>
    </xsl:template>
    <xsl:template match="advise2" mode="teaching">
        <fo:inline>, <xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="gnum" mode="teaching">
        <fo:inline> (<xsl:apply-templates/>)</fo:inline>
    </xsl:template>
    <xsl:template match="advise3" mode="teaching">
        <fo:inline>, <xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="a3num" mode="teaching">
       <fo:inline> (<xsl:apply-templates/>)</fo:inline>
    </xsl:template>
    <xsl:template match="advise4" mode="teaching">
        <fo:inline>, <xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="a4num" mode="teaching">
        <fo:inline> (<xsl:apply-templates/>)</fo:inline>
    </xsl:template>

</xsl:stylesheet>
