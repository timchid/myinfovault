<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY colon ":">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!-- TEACHING DATA TAGS -->
    
    <xsl:template match="coursenumber">
        <fo:inline>
            <xsl:apply-templates/>, 
        </fo:inline>
    </xsl:template>
    <xsl:template match="supplement-record" mode="teaching">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="year" mode="teaching"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <xsl:choose>
                    <xsl:when test="monthname">
                            <fo:block>
                                <xsl:apply-templates select="monthname" mode="teaching"/>
                            </fo:block>
                    </xsl:when>
                    <xsl:otherwise>
                            <fo:block>
                                <xsl:apply-templates select="description" mode="teaching"/>
                            </fo:block>
                    </xsl:otherwise>
                </xsl:choose>            
                <fo:block>
                    <xsl:apply-templates select="teachingtype" mode="teaching"/>: <xsl:apply-templates select="title" mode="teaching"/>
                    <xsl:if test="locationdescription">, <xsl:apply-templates select="locationdescription" mode="teaching"/></xsl:if>
                    <xsl:apply-templates select="hours" mode="teaching"/>
                </fo:block>
            </fo:table-cell>            
        </fo:table-row>
    </xsl:template>
    
    
    <xsl:template match="locationdescription" mode="teaching">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="units" mode="teaching">
        <fo:inline>
            <xsl:apply-templates/>, 
        </fo:inline>
    </xsl:template>
    <xsl:template match="teachingtype" mode="teaching">
        <fo:inline>
            <xsl:apply-templates/>
        </fo:inline>
    </xsl:template>
    <xsl:template match="description" mode="teaching">
            <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="name" mode="teaching">
        <fo:inline>
            <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="degree" mode="teaching">
        <fo:inline>
            , <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="type" mode="teaching">
        <fo:inline>
            , <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="startyear" mode="teaching">
        <fo:inline>
           <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="endyear" mode="teaching">
        <fo:inline>
           <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
    <xsl:template match="coursenumber" mode="teaching">
      <fo:inline>
         <xsl:apply-templates/> 
      </fo:inline>
    </xsl:template>
       <xsl:template match="title" mode="teaching">
        <xsl:apply-templates/>
    </xsl:template>
       <xsl:template match="monthname" mode="teaching">
        <fo:inline>
           <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
      <xsl:template match="hours" mode="teaching">
        <fo:inline>, <xsl:apply-templates/> hours</fo:inline>
    </xsl:template>
      <xsl:template match="termsequence">
        <fo:inline>
           <xsl:apply-templates/> 
        </fo:inline>
    </xsl:template>
 </xsl:stylesheet>