<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" indent="yes"/>

    <xsl:variable name="root">
        <xsl:value-of select="/menu-set/@root"/>
    </xsl:variable>
    
    <xsl:template match="/menu-set">
        <xsl:comment>CSS Horizontal Dropdown Menu</xsl:comment>
        <div id="menu">
            <xsl:apply-templates/>
        </div>
        <xsl:comment>End CSS Horizontal Dropdown Menu</xsl:comment>
    </xsl:template>
    
    <!-- top level menus (i.e. menu with no menu ancestor)-->
    <xsl:template match="menu[not(ancestor::menu)]">
        <ul>
            <!-- continue with regular menu template-->
            <xsl:call-template name="menu"/>
        </ul>
    </xsl:template>

    <xsl:template match="menu" name="menu">
        <!-- get title -->
        <xsl:variable name="title">
            <xsl:choose>
                <!-- use the title element if available -->
                <xsl:when test="title != ''">
                    <xsl:value-of select="title" />
                </xsl:when>
                
                <!-- else, use the name attribute -->
                <xsl:otherwise>
                    <xsl:value-of select="name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
    
        <li>
            <xsl:choose>
                <!-- top menu -->
                <xsl:when test="count(ancestor::menu) = 0">
                    <a class="menuheader" >
                        <xsl:attribute name="href">
                            <xsl:value-of select="$root"/>
                        
                            <xsl:value-of select="@root" /> 
                        </xsl:attribute>                                               
                           <xsl:attribute name="title">
                            <xsl:value-of select="$title"/>
                        </xsl:attribute> 
                           <xsl:value-of select="name"/>                        
                    </a>
                </xsl:when>
                
                <!-- leaf -->
                <xsl:when test="url != ''">
                    <a>
                        <xsl:attribute name="href">
                            <xsl:value-of select="$root"/>
                            <xsl:apply-templates select="url" />
                        </xsl:attribute>
                        <xsl:attribute name="title">
                            <xsl:value-of select="$title"/>
                        </xsl:attribute>
                        <xsl:value-of select="name"/>
                    </a>
                </xsl:when>
                
                <!-- sub menu -->
                <xsl:otherwise>
                    <xsl:attribute name="class">expand</xsl:attribute>
                    
                    <span onclick="void(0)" onfocus="void(0)">
                        <xsl:attribute name="title">
                            <xsl:value-of select="$title"/>
                        </xsl:attribute>
                        <xsl:value-of select="name"/>
                    </span>
                </xsl:otherwise>
            </xsl:choose>
            
            <xsl:if test="count(descendant::menu) > 0">
                <ul>
                    <xsl:apply-templates select="menu|permit"/>
                </ul>
            </xsl:if>
        </li>
    </xsl:template>

    <xsl:template match="permit">    
        <xsl:choose>                
        <xsl:when test="@roles | @action">
            <xsl:text disable-output-escaping="yes"><![CDATA[<miv:permit usertype="]]></xsl:text>
                <xsl:value-of select="@usertype"/>
                
                <xsl:if test="@roles">
                    <xsl:text disable-output-escaping="yes"><![CDATA[" roles="]]></xsl:text>
                    <xsl:value-of select="@roles"/>        
                </xsl:if>
                
                <xsl:if test="@action">
                    <xsl:text disable-output-escaping="yes"><![CDATA[" action="]]></xsl:text>
                    <xsl:value-of select="@action"/>
            </xsl:if>
                
                <xsl:text disable-output-escaping="yes"><![CDATA[">]]></xsl:text>
                <xsl:apply-templates select="menu|permit"/>
                <xsl:text disable-output-escaping="yes"><![CDATA[</miv:permit>]]></xsl:text>
        </xsl:when>
        <xsl:otherwise>
            <xsl:apply-templates select="menu|permit"/>
        </xsl:otherwise>
    </xsl:choose>
    </xsl:template>
</xsl:stylesheet>