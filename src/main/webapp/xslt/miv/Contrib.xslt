<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 <!ENTITY inpress "** IN PRESS **">
 <!ENTITY submitted "** SUBMITTED **">
 ]>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:import href="html-tags.xslt" />
    <xsl:import href="mivCommon.xslt" />

    <!-- Define the document name. -->
    <xsl:variable name="documentName" select="'Contributions to Jointly Authored Works'"/>
    
    <!-- document setup is done in mivCommon.xslt -->
    <xsl:template match="/">
        <xsl:call-template name="documentSetup">
            <xsl:with-param name="documentName" select="$documentName" />
            <xsl:with-param name="hasSections" select="//journals-published | //journals-in-press | //journals-submitted | //book-chapters-published | //book-chapters-in-press | //book-chapters-submitted | //editor-letters-published | //editor-letters-in-press | //editor-letters-submitted | //books-edited-published | //books-edited-in-press | //books-edited-submitted | //reviews-published | //reviews-in-press | //reviews-submitted | //books-authored-published | //books-authored-in-press | //books-authored-submitted | //limited-published | //limited-in-press | //limited-submitted | //media-published | //media-in-press | //media-submitted | //abstracts-published | //abstracts-in-press | //abstracts-submitted | //presentations | //patent-granted | //patent-filed | //disclosure" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="packet">
        <xsl:if test="//journals-published | //journals-in-press | //journals-submitted | //book-chapters-published | //book-chapters-in-press | //book-chapters-submitted | //editor-letters-published | //editor-letters-in-press | //editor-letters-submitted | //books-edited-published | //books-edited-in-press | //books-edited-submitted | //reviews-published | //reviews-in-press | //reviews-submitted | //books-authored-published | //books-authored-in-press | //books-authored-submitted | //limited-published | //limited-in-press | //limited-submitted | //media-published | //media-in-press | //media-submitted | //abstracts-published | //abstracts-in-press | //abstracts-submitted | //presentations | //patent-granted | //patent-filed | //disclosure">
            <xsl:call-template name='maintitle'>
                <xsl:with-param name="documentName" select="$documentName" />
            </xsl:call-template>
            
            <xsl:call-template name="nameblock" />
        </xsl:if>
        
        <fo:block font-size="12pt" font-family="Helvetica">
            <xsl:if test="//*[displaycontribution/text()='true' and (contribution or significance)]">
                <fo:block id="contributions">
                    <xsl:apply-templates select="journals-published" />
                    <xsl:apply-templates select="journals-in-press" />
                    <xsl:apply-templates select="journals-submitted" />
                    <xsl:apply-templates select="book-chapters-published" />
                    <xsl:apply-templates select="book-chapters-in-press" />
                    <xsl:apply-templates select="book-chapters-submitted" />
                    <xsl:apply-templates select="editor-letters-published" />
                    <xsl:apply-templates select="editor-letters-in-press" />
                    <xsl:apply-templates select="editor-letters-submitted" />
                    <xsl:apply-templates select="books-edited-published" />
                    <xsl:apply-templates select="books-edited-in-press" />
                    <xsl:apply-templates select="books-edited-submitted" />
                    <xsl:apply-templates select="reviews-published" />
                    <xsl:apply-templates select="reviews-in-press" />
                    <xsl:apply-templates select="reviews-submitted" />
                    <xsl:apply-templates select="books-authored-published" />
                    <xsl:apply-templates select="books-authored-in-press" />
                    <xsl:apply-templates select="books-authored-submitted" />
                    <xsl:apply-templates select="limited-published" />
                    <xsl:apply-templates select="limited-in-press" />
                    <xsl:apply-templates select="limited-submitted" />
                    <xsl:apply-templates select="media-published" />
                    <xsl:apply-templates select="media-in-press" />
                    <xsl:apply-templates select="media-submitted" />
                    <xsl:apply-templates select="abstracts-published" />
                    <xsl:apply-templates select="abstracts-in-press" />
                    <xsl:apply-templates select="abstracts-submitted" />
                    <xsl:apply-templates select="presentations" />
                    <xsl:apply-templates select="patent-granted" />
                    <xsl:apply-templates select="patent-filed" />
                    <xsl:apply-templates select="disclosure" />
                    <xsl:apply-templates select="publication-additional" />
                </fo:block>
            </xsl:if>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="journals-published|journals-in-press|journals-submitted|book-chapters-published|book-chapters-in-press|book-chapters-submitted|editor-letters-published|editor-letters-in-press|editor-letters-submitted|books-edited-published|books-edited-in-press|books-edited-submitted|reviews-published|reviews-in-press|reviews-submitted|books-authored-published|books-authored-in-press|books-authored-submitted|limited-published|limited-in-press|limited-submitted|media-published|media-in-press|media-submitted|abstracts-published|abstracts-in-press|abstracts-submitted|presentations|patent-granted|patent-filed|disclosure">
        <!-- if have child -->
        <xsl:if test="./*[displaycontribution/text()='true' and (contribution or significance)]">
            <fo:block>
                <xsl:call-template name="printContributions">
                    <xsl:with-param name="inputRecords" select="./*[displaycontribution/text()='true' and (contribution or significance)]" />
                    <xsl:with-param name="sectionHeader" select="section-header" />
                </xsl:call-template>
            </fo:block>
        </xsl:if>
    </xsl:template>

    <xsl:template name="printContributions">
        <xsl:param name="inputRecords" />
        <xsl:param name="sectionHeader" />
        
        <fo:block space-before="24pt" font-weight="bold"  text-decoration="underline" keep-with-previous="always" keep-with-next="always">
            <xsl:apply-templates select="$sectionHeader" />
        </fo:block>
        
        <xsl:for-each select="$inputRecords">
            <xsl:call-template name="contributions" />
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="record">
        <fo:inline>
           <xsl:apply-templates select="author" />
           <xsl:apply-templates select="title" />
           <xsl:apply-templates select="editor" />
           <xsl:apply-templates select="journal" />
           <xsl:apply-templates select="volume" />
           <xsl:apply-templates select="issue" />
           <xsl:apply-templates select="publisher" />
           <xsl:apply-templates select="city" />
           <xsl:apply-templates select="pages" />
           <xsl:apply-templates select="locationdescription" />
           <xsl:if test="editor | volume | issue | publisher | city | pages | locationdescription">&period;</xsl:if>
           <xsl:if test="ancestor::*[contains(name(),'-in-press')]">&space;&inpress;</xsl:if>
           <xsl:if test="ancestor::*[contains(name(),'-submitted')]">&space;&submitted;</xsl:if>
        </fo:inline>
        <xsl:if test="link">
           <fo:block text-decoration="underline" color="#0033ff">
             <fo:basic-link>
               <xsl:attribute name="external-destination">
                   <xsl:value-of select="link" />
               </xsl:attribute>View this Article
             </fo:basic-link>
           </fo:block>
         </xsl:if>
    </xsl:template>

    <xsl:template name="footnotes">
      <xsl:if test="descendant::footnote">
        <fo:block font-size="8pt" font-style="italic"
          space-after="8pt">
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="100%" />
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block font-weight="bold">Footnotes:</fo:block>
                  <xsl:for-each select="*/footnote">
                    <fo:block>#<xsl:value-of select="../number" />&period;
                      <fo:inline space-start="0.5em">
                        <xsl:apply-templates />
                      </fo:inline>&period;
                    </fo:block>
                  </xsl:for-each>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
        </fo:block>
      </xsl:if>
    </xsl:template>

    <xsl:template name="notations">
      <xsl:if test="descendant::notation">
        <fo:block font-size="8pt" font-style="italic">
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="100%" />
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block font-weight="bold">Notations:</fo:block>
                  <fo:block>* = Included in the review period.</fo:block>
                  <fo:block>x = Most significant works.</fo:block>
                  <fo:block>+ = Major mentoring role.</fo:block>
                  <fo:block>@ = Refereed.</fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
        </fo:block>
      </xsl:if>
    </xsl:template>

    <xsl:template name="contributions">
        <xsl:if test="contribution | significance and displaycontribution='true'">
          <!-- Some data in cells is not rendered unless a border is defined. Define as
          white so will be invisible -->
          <xsl:variable name="paperText">
              <xsl:choose>
                  <xsl:when test="name()='patent-record'">
                      <xsl:if test="typeid = '1'">Patent</xsl:if>
                      <xsl:if test="typeid = '2'">Disclosure</xsl:if>
                   </xsl:when><!-- Another way to test is test="self::view:textfield" -->
                  <xsl:otherwise>Paper</xsl:otherwise>
              </xsl:choose>
          </xsl:variable>
          <xsl:variable name="authorText">
              <xsl:choose>
                  <xsl:when test="name()='patent-record'">Title/Inventors</xsl:when>
                  <xsl:otherwise>Title/Authors</xsl:otherwise>
              </xsl:choose>
          </xsl:variable>
          <fo:table table-layout="fixed" width="100%" space-before="10pt"  border="1px white">
            <fo:table-column column-width="15%" />
            <fo:table-column column-width="85%" />
            <fo:table-body>
              <fo:table-row keep-with-previous="1">
                <fo:table-cell>
                  <fo:block font-weight="bold"><xsl:value-of select="$paperText"/> #&colon;&space;</fo:block>
                </fo:table-cell>
                <fo:table-cell>
                
                 <fo:block>
                    <xsl:apply-templates select="number" />
                 </fo:block>
                 
                 <xsl:if test="year">
                   <fo:block>
                    <fo:inline font-weight="bold">
                        Year&colon;&space;
                    </fo:inline>
                    <xsl:apply-templates select="year" />
                </fo:block>
                 </xsl:if>
                 
                 <fo:block >
                     <fo:inline font-weight="bold">
                         Citation&colon;&space;
                     </fo:inline>
                     <fo:inline>
                         <xsl:choose>
                             <xsl:when test="citation">
                                 <xsl:apply-templates select="citation" />
                             </xsl:when>
                             <xsl:otherwise>    
                                 <xsl:call-template name="record" />
                             </xsl:otherwise>
                         </xsl:choose>        
                     </fo:inline>
                 </fo:block>
                 
                 <xsl:if test="contribution">
                   <fo:block >
                         <fo:inline font-weight="bold">
                             Contributions to Jointly Authored Works&colon;
                         </fo:inline>    
                       <fo:inline> 
                             <xsl:apply-templates select="contribution" />
                         </fo:inline>    
                   </fo:block>
                 </xsl:if>
                 
                 <xsl:if test="significance">
                     <fo:block >
                         <fo:inline font-weight="bold">
                             Significance of Research&colon;
                         </fo:inline>    
                       <fo:inline> 
                             <xsl:apply-templates select="significance" />
                         </fo:inline>    
                   </fo:block>
                 </xsl:if>
                  
                    </fo:table-cell>
              </fo:table-row>
              </fo:table-body>
          </fo:table>
      </xsl:if>
    </xsl:template>

    <!-- DATA TAGS -->

    <xsl:template match="number">
      <fo:inline>
        <xsl:apply-templates />
      </fo:inline>
    </xsl:template>

    <xsl:template match="year">
      <fo:inline>
        <xsl:apply-templates />
      </fo:inline>
    </xsl:template>

    <xsl:template match="eengineeringannotation">
        <fo:inline>
            <xsl:apply-templates />
        </fo:inline>
    </xsl:template>

    <xsl:template match="author">
      <xsl:variable name="value" select="." />
      <xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
      <xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
      <xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
      <xsl:variable name="hasPunctuation">
         <xsl:choose>
          <xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
         </xsl:choose>
       </xsl:variable>
      <fo:inline>
        <xsl:choose>
          <xsl:when
            test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record">
            <xsl:apply-templates /><xsl:if test="$hasPunctuation != '1'">&period;</xsl:if><xsl:text>&space;</xsl:text></xsl:when>
          <xsl:when
            test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record | parent::limited-record">
            <xsl:apply-templates />&colon;&space;</xsl:when>
        </xsl:choose>
      </fo:inline>
    </xsl:template>

    <xsl:template match="title | contributor">
        <xsl:variable name="value" select="." />
        <xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
        <xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
        <xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
        <xsl:variable name="hasPunctuation">
        <xsl:choose>
            <xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <fo:inline>
            <xsl:choose>
                <xsl:when test="$hasPunctuation != '1'"><xsl:apply-templates />&period;</xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates />
                </xsl:otherwise>
            </xsl:choose>
        </fo:inline>
    </xsl:template>
  
    <xsl:template match="editor">
      <fo:inline>
        <xsl:text>&space;</xsl:text><xsl:apply-templates />&comma;&space;&leftparen;ed&rightparen;<xsl:if test="../journal or ../publisher or ../volume or ../issue or ../city or ../pages">&comma;</xsl:if>
      </fo:inline>
    </xsl:template>

    <xsl:template match="journal">
        <xsl:variable name="value" select="." />
        <xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
        <xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
        <xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
        <xsl:variable name="hasPunctuation">
            <xsl:choose>
                <xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <fo:inline>
            <xsl:text>&space;</xsl:text>
            <xsl:apply-templates />
            <xsl:choose>
                <xsl:when test="../publisher or ../volume or ../issue or ../city or ../pages">&comma;</xsl:when>
                <xsl:otherwise><xsl:if test="$hasPunctuation != '1'">&period;</xsl:if></xsl:otherwise>
                </xsl:choose>    
        </fo:inline>
    </xsl:template>

    <xsl:template match="publisher">
      <fo:inline><xsl:text>&space;</xsl:text><xsl:apply-templates /><xsl:if test="../city or ../pages">&comma;</xsl:if></fo:inline>
    </xsl:template>
    
    <xsl:template match="city">
      <fo:inline><xsl:text>&space;</xsl:text><xsl:apply-templates /><xsl:if test="../pages">&period;</xsl:if></fo:inline>
    </xsl:template>
    
    <xsl:template match="volume">
      <fo:inline>
        <xsl:choose>
          <xsl:when
            test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record">
            <xsl:text>&space;</xsl:text><xsl:apply-templates /></xsl:when>
          <xsl:when
            test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record">
            &space;Vol&period;&space;<xsl:apply-templates /><xsl:if test="not(../issue) and (../publisher or ../city or ../pages)">&comma;</xsl:if></xsl:when>
          <xsl:otherwise><xsl:text>&space;</xsl:text><xsl:apply-templates /></xsl:otherwise>
        </xsl:choose>
     </fo:inline>
    </xsl:template>
    
    <xsl:template match="issue">
      <xsl:variable name="issuePunctuation">
      <xsl:choose>
        <xsl:when test="(parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record)">
         <xsl:if test="../pages or ../publisher or ../city">&comma;</xsl:if></xsl:when>
        <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
      </xsl:variable>
      <fo:inline>
        <xsl:if test="not(../volume)">
          <xsl:text>&space;</xsl:text></xsl:if>&leftparen;<xsl:apply-templates />&rightparen;<xsl:value-of select="$issuePunctuation"/>
      </fo:inline>
    </xsl:template>
    
    <xsl:template match="pages">
      <xsl:variable name="hasVolIssue">
        <xsl:choose>
          <xsl:when test="../volume or ../issue">1</xsl:when>
          <xsl:when test="../volume">1</xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <fo:inline>
        <xsl:choose>
          <xsl:when
            test="(parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record)">
            <xsl:if test="../volume">&colon;</xsl:if>
            <xsl:text>&space;</xsl:text><xsl:apply-templates /></xsl:when>
          <xsl:when
            test="$hasVolIssue = '1' and (parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record)">
            &space;pp&period;&space;<xsl:apply-templates /></xsl:when>
          <xsl:otherwise><xsl:text>&space;</xsl:text><xsl:apply-templates /></xsl:otherwise>
        </xsl:choose>
      </fo:inline>
    </xsl:template>
    
    <xsl:template name="label">
      <xsl:param name="rightalign" />
      <xsl:param name="displaybefore" />
      <xsl:variable name="align">
        <xsl:choose>
          <xsl:when test="$rightalign='1' or $rightalign='true'">right</xsl:when>
          <xsl:otherwise>left</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
        <xsl:choose>
          <xsl:when test="$displaybefore='1'">
            <fo:block font-weight="bold" text-align="{$align}"
               border-bottom="1px solid black">
              <xsl:apply-templates select="labelbefore"/>
            </fo:block>
          </xsl:when>
          <xsl:otherwise>
            <fo:block font-weight="bold" text-align="{$align}"
             border-bottom="1px solid black">
              <xsl:apply-templates select="labelafter"/>
            </fo:block>
          </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="notation">
      <fo:inline>
        <xsl:apply-templates />
      </fo:inline>
    </xsl:template>
    
    <xsl:template match="citation">
      <xsl:apply-templates />
      <xsl:call-template name="addPunctuation">
        <xsl:with-param name="value" select="." />
        <xsl:with-param name="punctuationValue" select="'&period;'"/>
        <xsl:with-param name="followWith" select="'&space;'"/>
      </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="contribution">
      <xsl:apply-templates />
      <xsl:call-template name="addPunctuation">
        <xsl:with-param name="value" select="." />
        <xsl:with-param name="punctuationValue" select="'&period;'"/>
        <xsl:with-param name="followWith" select="'&space;'"/>
      </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="significance">
      <xsl:apply-templates />
      <xsl:call-template name="addPunctuation">
        <xsl:with-param name="value" select="." />
        <xsl:with-param name="punctuationValue" select="'&period;'"/>
        <xsl:with-param name="followWith" select="'&space;'"/>
      </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="monthname">
        <fo:inline><xsl:apply-templates />&comma;&space;</fo:inline>
    </xsl:template>
    
    <xsl:template match="day">
        <fo:inline><xsl:text>&space;</xsl:text><xsl:apply-templates />&comma;&space;</fo:inline>
    </xsl:template>
    
    <xsl:template match="locationdescription">
      <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
</xsl:stylesheet>
