<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 <!ENTITY inpress "** IN PRESS **">
 <!ENTITY submitted "** SUBMITTED **">
 ]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<xsl:import href="html-tags.xslt" />
	<xsl:import href="mivCommon.xslt" />
	<xsl:import href="annotations.xslt"/>
	<xsl:import href="patent.xslt"/> 
	
	<!-- Define the document name. -->
	<xsl:variable name="documentName" select="'Publications'"/>
	<xsl:variable name="titleAdditional"> 
	    <xsl:apply-templates select="//publication-additional/section-header"/>
	</xsl:variable>
  
	<!-- Global variable to identify if a header page is needed(i.e) if there is data other than additional information. -->
	<xsl:variable name="hasHeaderpage" select="//journals-published | //journals-in-press | //journals-submitted | //book-chapters-published | //book-chapters-in-press | //book-chapters-submitted
				| //editor-letters-published | //editor-letters-in-press | //editor-letters-submitted | //books-edited-published
				| //books-edited-in-press | //books-edited-submitted | //reviews-published | //reviews-in-press | //reviews-submitted
				| //books-authored-published | //books-authored-in-press | //books-authored-submitted | //limited-published
				| //limited-in-press | //limited-submitted | //media-published | //media-in-press | //media-submitted | //abstracts-published
				| //abstracts-in-press | //abstracts-submitted | //presentations | //patent-granted | //patent-filed | //disclosure"/>
	<xsl:variable name="hasAdditional" select="/*/publication-additional"/>
	
	<!-- document setup is done in mivCommon.xslt -->
        <xsl:template match="/">
               <xsl:choose>
                        <xsl:when test="$hasHeaderpage">
		                <xsl:call-template name="documentSetup">
		                     <xsl:with-param name="documentName" select="$documentName" />
		                     <xsl:with-param name="title1" select="''" />
		                     <xsl:with-param name="title2" select="''" />
		                     <xsl:with-param name="titleAdditional" select="$titleAdditional" />
                                     <xsl:with-param name="hasSections" select="$hasHeaderpage" />
		             </xsl:call-template>
		        </xsl:when>        
		        <xsl:when test="$hasAdditional">
		             <xsl:call-template name="documentSetup">
		                     <xsl:with-param name="documentName" select="''" />
		                     <xsl:with-param name="title1" select="''" />
		                     <xsl:with-param name="title2" select="''" />
		                     <xsl:with-param name="titleAdditional" select="$titleAdditional" />
                                     <xsl:with-param name="hasSections" select="$hasAdditional" />
		             </xsl:call-template>
		        </xsl:when>
                </xsl:choose>                               
        </xsl:template>

	<xsl:template match="packet">
		<xsl:if test="$hasHeaderpage">
			<xsl:call-template name='maintitle'>
				<xsl:with-param name="documentName" select="$documentName" />
			</xsl:call-template>
			<xsl:call-template name="nameblock" />
		</xsl:if>
		<xsl:call-template name="main" />
	</xsl:template>

	<xsl:template name="main">
		<fo:block font-size="12pt" font-family="Helvetica">
			<xsl:apply-templates select="journals-published" />
			<xsl:apply-templates select="journals-in-press" />
			<xsl:apply-templates select="journals-submitted" />
			<xsl:apply-templates select="book-chapters-published" />
			<xsl:apply-templates select="book-chapters-in-press" />
			<xsl:apply-templates select="book-chapters-submitted" />
			<xsl:apply-templates select="editor-letters-published" />
			<xsl:apply-templates select="editor-letters-in-press" />
			<xsl:apply-templates select="editor-letters-submitted" />
			<xsl:apply-templates select="books-edited-published" />
			<xsl:apply-templates select="books-edited-in-press" />
			<xsl:apply-templates select="books-edited-submitted" />
			<xsl:apply-templates select="reviews-published" />
			<xsl:apply-templates select="reviews-in-press" />
			<xsl:apply-templates select="reviews-submitted" />
			<xsl:apply-templates select="books-authored-published" />
			<xsl:apply-templates select="books-authored-in-press" />
			<xsl:apply-templates select="books-authored-submitted" />
			<xsl:apply-templates select="limited-published" />
			<xsl:apply-templates select="limited-in-press" />
			<xsl:apply-templates select="limited-submitted" />
			<xsl:apply-templates select="media-published" />
			<xsl:apply-templates select="media-in-press" />
			<xsl:apply-templates select="media-submitted" />
			<xsl:apply-templates select="abstracts-published" />
			<xsl:apply-templates select="abstracts-in-press" />
			<xsl:apply-templates select="abstracts-submitted" />
			<xsl:apply-templates select="presentations" />
			<xsl:apply-templates select="patent-granted" />
			<xsl:apply-templates select="patent-filed" />
			<xsl:apply-templates select="disclosure" />
			<xsl:apply-templates select="publication-additional" />
		</fo:block>
	</xsl:template>

	<xsl:template match="journals-published|journals-in-press|journals-submitted|book-chapters-published|book-chapters-in-press|book-chapters-submitted|editor-letters-published|editor-letters-in-press|editor-letters-submitted|books-edited-published|books-edited-in-press|books-edited-submitted|reviews-published|reviews-in-press|reviews-submitted|books-authored-published|books-authored-in-press|books-authored-submitted|limited-published|limited-in-press|limited-submitted|media-published|media-in-press|media-submitted|abstracts-published|abstracts-in-press|abstracts-submitted|presentations|patent-granted|patent-filed|disclosure">
		<xsl:if test="name(preceding-sibling::*[1]) != 'department'"><fo:block break-before="page"></fo:block></xsl:if>
		<fo:block id="{local-name()}">
			<fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline"
				space-after="12pt">
				<xsl:apply-templates select="section-header" />
			</fo:block>
			<!-- select value equivalent to XSLT v2.0: *[ends-with(name(), '-record'] -->
			<xsl:apply-templates select="*[substring(name(), string-length(name())-6) = '-record']" />
			<xsl:call-template name="footnotes" />
			<xsl:apply-templates select="annotations-legends"/>
		</fo:block>
	</xsl:template>

	<!-- ADDITIONAL INFORMATION -->
	<xsl:template match="publication-additional">
		<!-- ADDITIONAL INFORMATION -->
		<fo:block id="{local-name()}"  break-before="page">
			<fo:block id="title-additional" white-space-collapse="false" font-size="20pt" font-weight="bold"
				space-after="16pt" text-align="center">
				<xsl:apply-templates select="section-header" />
			</fo:block>
			<xsl:call-template name="nameblock" />
			<xsl:apply-templates select="additional-record" />
		</fo:block>
	</xsl:template>

	<xsl:template match="journal-record|book-chapter-record|letter-to-editor-record|book-editor-record|review-record|book-author-record|limited-record|media-record|abstract-record|presentation-record">
		<xsl:call-template name="record" />
	</xsl:template>
	
	<!-- NAMED TEMPLATES -->
	<xsl:template name="record">
		<fo:block padding-bottom="12pt">
			<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="100%" />
				<fo:table-body>
					<xsl:if test="./labelbefore">
					<fo:table-row keep-with-next="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
									<xsl:with-param name="displaybefore" select="1" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
					<fo:table-row>
						<fo:table-cell>
							<fo:table table-layout="fixed" width="100%">
								<fo:table-column column-width="15%" />
								<fo:table-column column-width="7%" />
								<fo:table-column column-width="78%" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="notation" />&space;
												<xsl:if test="descendant::footnote">&hash;</xsl:if>
												<xsl:apply-templates select="number" />&period;&space;
												<fo:block>
													<xsl:apply-templates select="engineeringannotation" />
												</fo:block>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<fo:block>
												<xsl:apply-templates select="year" />
											</fo:block>
										</fo:table-cell>
										<fo:table-cell>
											<xsl:choose>
												<xsl:when test="descendant::citation">
													<fo:block>
														<xsl:apply-templates select="citation" />
													</fo:block>
												</xsl:when>
												<xsl:otherwise>
													<fo:block>
														<xsl:if test="monthname/text() != 'None'">
															<xsl:choose>
																<xsl:when test="day/text() != 0">
																	<fo:inline><xsl:value-of select="monthname" />&space;</fo:inline>
																	<xsl:apply-templates select="day" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:apply-templates select="monthname" />
																</xsl:otherwise>
															</xsl:choose>
														</xsl:if>
														<xsl:apply-templates select="author" />
														<xsl:apply-templates select="title" />
														<xsl:apply-templates select="editor" />
														<xsl:apply-templates select="journal" />
														<xsl:apply-templates select="volume" />
														<xsl:apply-templates select="issue" />
														<xsl:apply-templates select="publisher" />
														<xsl:apply-templates select="city" />
														<xsl:apply-templates select="pages" />
														<xsl:apply-templates select="locationdescription" />
														<xsl:if test="editor | volume | issue | publisher | city | pages">&period;</xsl:if>
														<xsl:if
															test="ancestor::*[contains(name(),'-in-press')]">&space;&inpress;
														</xsl:if>
														<xsl:if
															test="ancestor::*[contains(name(),'-submitted')]">&space;&submitted;
														</xsl:if>
													</fo:block>
												</xsl:otherwise>
											</xsl:choose>
										</fo:table-cell>
									</fo:table-row>
									<xsl:if test="link">
										<fo:table-row>
											<fo:table-cell>
												<fo:block></fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block></fo:block>
											</fo:table-cell>
											<fo:table-cell>
												<fo:block text-decoration="underline" color="#0033ff">
													<fo:basic-link>
														<xsl:attribute name="external-destination">
															<xsl:value-of select="link" />
														</xsl:attribute>View this Article
													</fo:basic-link>
												</fo:block>
											</fo:table-cell>
										</fo:table-row>
									</xsl:if>
								</fo:table-body>
							</fo:table>
						</fo:table-cell>
					</fo:table-row>
					<xsl:if test="./labelafter">
					<fo:table-row keep-with-previous="always">
						<fo:table-cell>
							<fo:block>
								<xsl:call-template name="label">
									<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
									<xsl:with-param name="displaybefore" select="0" />
								</xsl:call-template>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					</xsl:if>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	
	<xsl:template name="contributions">
		<xsl:if test="contribution | significance and displaycontribution='1'">
			<!-- Some data in cells is not rendered unless a border is defined. Define as
			white so will be invisible -->
			<fo:table table-layout="fixed" width="100%" space-before="10pt"  border="1px white">
				<fo:table-column column-width="15%" />
				<fo:table-column column-width="85%" />
				<fo:table-body>
					<fo:table-row keep-with-previous="1">
						<fo:table-cell>
							<fo:block font-weight="bold">Paper #&colon;&space;</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block>
								<xsl:apply-templates select="number" />
							</fo:block>
							<xsl:if test="year">
								<fo:block><fo:inline font-weight="bold">Year&colon;&space;</fo:inline><xsl:apply-templates select="year" /></fo:block>
							</xsl:if>
							<fo:block font-weight="bold">Title/Authors&colon;&space;</fo:block>
							<xsl:choose>
							<xsl:when test="citation">
								<fo:block>
									<xsl:apply-templates select="citation" />
								</fo:block>
							</xsl:when>
							<xsl:otherwise>
								<fo:block>
									<xsl:apply-templates select="title" />
								</fo:block>
								<fo:block>
									<xsl:apply-templates select="contributor" />
								</fo:block>
							</xsl:otherwise>
							</xsl:choose>
							<xsl:if test="contribution">
								<fo:block font-weight="bold">Contributions to Jointly Authored Works&colon;</fo:block>
								<fo:block>
									<xsl:apply-templates select="contribution" />
								</fo:block>
							</xsl:if>
							<xsl:if test="significance">
								<fo:block font-weight="bold">Significance of Research&colon;</fo:block>
								<fo:block>
									<xsl:apply-templates select="significance" />
								</fo:block>
							</xsl:if>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</xsl:if>
	</xsl:template>

	<!-- DATA TAGS -->

	<xsl:template match="number|year|eengineeringannotation|contributor|notation">
		<fo:inline>
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="author">
		<xsl:variable name="value" select="." />
		<xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
		<xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
		<xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
		<xsl:variable name="hasPunctuation">
			<xsl:choose>
			<xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<fo:inline>
			<xsl:choose>
				<xsl:when
					test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record">
					<xsl:apply-templates /><xsl:if test="$hasPunctuation != '1'">&period;</xsl:if><xsl:text>&space;</xsl:text></xsl:when>
				<xsl:when
					test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record">
					<xsl:apply-templates />&colon;&space;</xsl:when>
				<xsl:when test="parent::patent-record">
					<xsl:apply-templates /><xsl:if test="$hasPunctuation != '1'">&period;</xsl:if></xsl:when>	
				<xsl:otherwise>
					<xsl:apply-templates />
				</xsl:otherwise>	
			</xsl:choose>
		</fo:inline>
	</xsl:template>

	<xsl:template match="editor">
		<fo:inline>
			<xsl:text>&space;</xsl:text><xsl:apply-templates />&comma;&space;&leftparen;ed&rightparen;<xsl:if test="../journal or ../publisher or ../volume or ../issue or ../city or ../pages">&comma;</xsl:if>
		</fo:inline>
	</xsl:template>

	<xsl:template match="title">
		<xsl:variable name="value" select="." />
		<xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
		<xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
		<xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
		<xsl:variable name="hasPunctuation">
			<xsl:choose>
				<!-- Original -->
				<!-- xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz?!&quote;','ABCDEFGHIJKLMNOPQRSTUVWXYZ- - -')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..' )">1</xsl:when -->
				<!-- First Look -->
				<!-- xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'','')!='_.') or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz?!&quote;','ABCDEFGHIJKLMNOPQRSTUVWXYZ- - -')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..' )">1</xsl:when -->
				<!-- Match the TitleFormatter logic -->
				<xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<fo:inline>
			<xsl:choose>
				<xsl:when
					test="parent::patent-record | parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record">
					<xsl:apply-templates /><xsl:if test="$hasPunctuation != '1' and $lastCharacter != '&qmark;' and $lastCharacter != '&expoint;' and $lastCharacter != '&quote;'">&period;</xsl:if></xsl:when>
				<xsl:when
					test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record | parent::presentation-record">
					<xsl:apply-templates />
					<xsl:choose>
						<xsl:when test="../editor or ../journal or ../publisher or ../volume or ../issue or ../city or ../pages or ../locationdescription">&comma;</xsl:when>
						<xsl:otherwise><xsl:if test="$hasPunctuation != '1'">&period;</xsl:if></xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates />
					<!-- xsl:if test="not(preceding-sibling::locationdescription or following-sibling::locationdescription)">&period;</xsl:if -->
				</xsl:otherwise>
			</xsl:choose>
		</fo:inline>
	</xsl:template>

	<xsl:template match="journal">
		<xsl:variable name="value" select="." />
		<xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
		<xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
		<xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
		<xsl:variable name="hasPunctuation">
			<xsl:choose>
				<xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<fo:inline>
			<xsl:text>&space;</xsl:text>
			<xsl:apply-templates />
			<xsl:choose>
				<xsl:when test="../publisher or ../volume or ../issue or ../city or ../pages">&comma;</xsl:when>
				<xsl:otherwise><xsl:if test="$hasPunctuation != '1'">&period;</xsl:if></xsl:otherwise>
			</xsl:choose>	
		</fo:inline>
	</xsl:template>

	<xsl:template match="publisher">
		<fo:inline><xsl:text>&space;</xsl:text><xsl:apply-templates /><xsl:if test="../city or ../pages">&comma;</xsl:if></fo:inline>
	</xsl:template>

	<xsl:template match="city">
		<fo:inline><xsl:text>&space;</xsl:text><xsl:apply-templates /><xsl:if test="../pages">&period;</xsl:if></fo:inline>
	</xsl:template>

	<xsl:template match="volume">
		<fo:inline>
			<xsl:choose>
				<xsl:when
					test="parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record">
					<xsl:text>&space;</xsl:text><xsl:apply-templates /></xsl:when>
				<xsl:when
					test="parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record">
					&space;Vol&period;&space;<xsl:apply-templates /><xsl:if test="not(../issue) and (../publisher or ../city or ../pages)">&comma;</xsl:if></xsl:when>
				<xsl:otherwise><xsl:text>&space;</xsl:text><xsl:apply-templates /></xsl:otherwise>
			</xsl:choose>
	   </fo:inline>
	</xsl:template>

	<xsl:template match="issue">
		<xsl:variable name="issuePunctuation">
		<xsl:choose>
			<xsl:when test="(parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record)">
			 <xsl:if test="../pages or ../publisher or ../city">&comma;</xsl:if></xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
		</xsl:variable>
		<fo:inline>
			<xsl:if test="not(../volume)">
				<xsl:text>&space;</xsl:text>
			</xsl:if>&leftparen;<xsl:apply-templates />&rightparen;<xsl:value-of select="$issuePunctuation"/>
		</fo:inline>
	</xsl:template>

	<xsl:template match="pages">
		<xsl:variable name="hasVolIssue">
			<xsl:choose>
				<xsl:when test="../volume or ../issue">1</xsl:when>
				<xsl:when test="../volume">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<fo:inline>
			<xsl:choose>
				<xsl:when
					test="(parent::journal-record | parent::review-record | parent::letter-to-editor-record | parent::abstract-record | parent::limited-record)">
					<xsl:if test="../volume">&colon;</xsl:if>
					<xsl:text>&space;</xsl:text><xsl:apply-templates /></xsl:when>
				<xsl:when
					test="$hasVolIssue = '1' and (parent::book-author-record | parent::book-editor-record | parent::book-chapter-record | parent::media-record)">
					&space;pp&period;&space;<xsl:apply-templates /></xsl:when>
				<xsl:otherwise><xsl:text>&space;</xsl:text><xsl:apply-templates /></xsl:otherwise>
			</xsl:choose>
		</fo:inline>
	</xsl:template>

	<xsl:template match="citation|contribution|significance">
		<xsl:apply-templates />
		<xsl:call-template name="addPunctuation">
			<xsl:with-param name="value" select="." />
			<xsl:with-param name="punctuationValue" select="'&period;'"/>
			<xsl:with-param name="followWith" select="'&space;'"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="monthname">
			<fo:inline><xsl:apply-templates />&comma;&space;</fo:inline>
	</xsl:template>

	<xsl:template match="day">
			<fo:inline><xsl:text>&space;</xsl:text><xsl:apply-templates />&comma;&space;</fo:inline>
	</xsl:template>

	<xsl:template match="locationdescription">
		<xsl:variable name="value" select="." />
		<xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
		<xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
		<xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
		<xsl:variable name="hasPunctuation">
			<xsl:choose>
				<xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<fo:inline>
			<xsl:text>&space;</xsl:text><xsl:apply-templates />
			<xsl:if test="$hasPunctuation != '1' and $lastCharacter != '&qmark;' and $lastCharacter != '&expoint;' and $lastCharacter != '&quote;'">&period;</xsl:if>
		</fo:inline>
	</xsl:template>
</xsl:stylesheet>
