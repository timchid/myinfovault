<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY singlequote "'''">
 ]>
 
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"   
   xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:import href="html-tags.xslt"/>
   <xsl:import href="mivCommon.xslt"/>
  
  <!-- Define the document name. Note: Numeric entity for single quote is used for appostrophe! -->
  <xsl:variable name="documentName" select="concat('Candidate','&#8217;','s Statement')"/>
  
  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
    <xsl:call-template name="documentSetup">
      <xsl:with-param name="documentName" select="$documentName" />
      <xsl:with-param name="hasSections" select="//candidate-statement" />
    </xsl:call-template>
  </xsl:template>
   
   <xsl:template match="packet">   
    <xsl:if test="count(descendant::candidate-statement) &gt; 0">
      <xsl:call-template name='maintitle'>
        <xsl:with-param name="documentName" select="$documentName" />
      </xsl:call-template>
      <xsl:call-template name="nameblock"/>
    <xsl:apply-templates select="candidate-statement"/>
      </xsl:if>
   </xsl:template>
 
   <xsl:template match="candidate-statement">         
      <fo:block id="candidate-statement">
         <xsl:apply-templates select="statement-record" />         
      </fo:block>
   </xsl:template>
   
   <xsl:template match="candidate-upload">         
      <fo:block id="candidate-upload">
        <xsl:apply-templates/>
      </fo:block>
   </xsl:template>

   <!-- CANDIDATE'S STATEMENT -->
   <xsl:template match="statement-record">
      <fo:block padding-bottom="12pt">
          <xsl:apply-templates select="content" />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="content">
      <fo:block>
        <xsl:apply-templates />
      </fo:block>
   </xsl:template>
  
</xsl:stylesheet>