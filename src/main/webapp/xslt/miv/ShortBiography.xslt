<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:import href="html-tags.xslt" />
  <xsl:import href="mivCommon.xslt" />
  	
  <!-- Define the document name. -->
  <xsl:variable name="documentName" select="'Short Biography'" />

    <!-- Global variable to identify if a header page is needed(i.e) if there is data other than additional information. -->
    <xsl:variable name="hasHeaderpage">
        <xsl:choose>
            <xsl:when
                test="//education | //credential | //education-additional | //employment | //employment-additional">
                true
            </xsl:when>
            <xsl:otherwise>
                false
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
    <xsl:call-template name="documentSetup">
      <xsl:with-param name="documentName" select="$documentName" />
      <xsl:with-param name="hasSections" select="$hasHeaderpage" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="packet">
		<xsl:if test="$hasHeaderpage='true'">
			<xsl:call-template name='maintitle'>
				<xsl:with-param name="documentName" select="$documentName" />
			</xsl:call-template>
			<xsl:call-template name="nameblock" />
		</xsl:if>
		<xsl:call-template name="main" />
  </xsl:template>
  
  <xsl:template name="main">
		<fo:block font-size="12pt" font-family="Helvetica">
			<xsl:apply-templates select="education" />
			<xsl:apply-templates select="credential" />
			<xsl:apply-templates select="education-additional" />
			<xsl:apply-templates select="employment" />
			<xsl:apply-templates select="employment-additional" />
		</fo:block>
	</xsl:template>
  
  <!-- <xsl:template match="education|employment">
		<xsl:if test="name(preceding-sibling::*[1]) != 'department'"><fo:block break-before="page"></fo:block></xsl:if>
		<fo:block id="{local-name()}">
			<fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline"
				space-after="12pt">
				<xsl:apply-templates select="section-header" />
			</fo:block>
			select value equivalent to XSLT v2.0: *[ends-with(name(), '-record']
			<xsl:apply-templates select="*[substring(name(), string-length(name())-6) = '-record']" />
		</fo:block>
  </xsl:template> -->
  
  <xsl:template match="employment">
  	 <!-- <xsl:if test="name(preceding-sibling::*[1]) != 'department'"><fo:block break-before="page"></fo:block></xsl:if> -->
  	 <fo:block id="employment" space-after="16pt">
  	 		<fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="5pt">
				<xsl:apply-templates select="section-header" />
			</fo:block>
			
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>
                <fo:table-body start-indent="0pt">
                    <xsl:apply-templates select="employment-record"/>
                </fo:table-body>
            </fo:table>            
      </fo:block>
    </xsl:template>
    
    <xsl:template match="employment-record">
        <fo:table-row keep-together.within-page="1">
            <fo:table-cell padding="2px" text-align="left">
                <fo:block>
                    <xsl:apply-templates select="startdate"/>
                    <xsl:if test="enddate">-<fo:block/><xsl:apply-templates select="enddate"/></xsl:if>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="2px" text-align="left">
                <fo:block>
                    <xsl:apply-templates select="institution"  />
                    <xsl:apply-templates select="location"  />
                    <xsl:apply-templates select="title"  />
					<!-- MIV-1739 removing the salary info from the preview -->                    
					<!-- <xsl:apply-templates select="salary"  /> -->
					<xsl:apply-templates select="remark"  />
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
  
  <xsl:template match="education">
  	 <!-- <xsl:if test="name(preceding-sibling::*[1]) != 'department'"><fo:block break-before="page"></fo:block></xsl:if> -->
  	 <fo:block id="education" space-after="16pt">
  	 		 <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="5pt">
				<xsl:apply-templates select="section-header" />
			</fo:block>
			
	         <fo:table table-layout="fixed" width="100%">
	            <fo:table-column column-width="20%" />
	            <fo:table-column column-width="80%" />
	            <fo:table-body start-indent="0pt">
	               <xsl:apply-templates select="education-record" />
	            </fo:table-body>
	         </fo:table>
      </fo:block>
  </xsl:template>
    
    <xsl:template match="education-record">
        <fo:table-row keep-together.within-page="1">
            <fo:table-cell padding="2px" text-align="left">
                <fo:block>
                    <xsl:apply-templates select="startdate" />
                    <xsl:if test="enddate">-<xsl:apply-templates select="enddate"/></xsl:if>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="2px" text-align="left">
                <fo:block>
                    <xsl:apply-templates select="institution" /><xsl:apply-templates select="location" />
                    <xsl:apply-templates select="degree" />
                    <xsl:apply-templates select="field" /><xsl:apply-templates select="sponsor" />
                    <xsl:apply-templates select="remark" />
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <xsl:template match="credential">
        <fo:block id="credentials" space-after="16pt">
        	<fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="5pt">
				<xsl:apply-templates select="section-header" />
			</fo:block>

            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>
                <fo:table-body start-indent="0pt">
                    <xsl:apply-templates select="credential-record"/>
                </fo:table-body>
            </fo:table>            
        </fo:block>
    </xsl:template>
    
    <xsl:template match="credential-record">
        <fo:table-row keep-together.within-page="1">
            <fo:table-cell padding="2px" text-align="left">
                <fo:block>
                    <xsl:apply-templates select="yearspan"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="2px" text-align="left">
                <fo:block>
                    <xsl:apply-templates select="content" />
                    <xsl:apply-templates select="remark" />
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
	<xsl:template match="education-additional">
        <!-- ADDITIONAL INFORMATION -->
        <fo:block id="education-additional" space-after="16pt">
            <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="5pt">
				<xsl:apply-templates select="section-header" />
			</fo:block>
            
            <xsl:apply-templates select="additional-record"/>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="employment-additional">
        <!-- ADDITIONAL INFORMATION -->
        <fo:block id="employment-additional" space-after="16pt">
            <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="5pt">
				<xsl:apply-templates select="section-header" />
			</fo:block>
            
            <xsl:apply-templates select="additional-record"/>
        </fo:block>
    </xsl:template>
	
    <xsl:template match="startdate" >
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="enddate" >
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="institution" >
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="location" >
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="degree" >
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="field"  >
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="sponsor" >
        <fo:inline>&space;(<xsl:apply-templates />)</fo:inline>
    </xsl:template>
    <xsl:template match="remark" >
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="title" >
        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="yearspan">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
    <xsl:template match="content">
        <fo:inline><xsl:apply-templates /></fo:inline>
    </xsl:template>
<!--    <xsl:template match="salary" >-->
<!--        <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>-->
<!--    </xsl:template>-->    
</xsl:stylesheet>
