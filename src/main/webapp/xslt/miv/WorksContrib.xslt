<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:import href="html-tags.xslt" />
  <xsl:import href="mivCommon.xslt" />
  <xsl:import href="creativeactivitiescommon.xslt" />

  <!-- Define the document name. -->
  <xsl:variable name="documentName" select="'Contributions to Jointly Created Works'" />

  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
    <xsl:call-template name="documentSetup">
      <xsl:with-param name="documentName" select="'Contributions to Jointly Created Works'" />
      <xsl:with-param name="hasSections" select="//works-record" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="packet">
    <xsl:call-template name="main" />
  </xsl:template>
  
  <xsl:template name="main">
    <fo:block font-size="12pt" font-family="Helvetica">

      <!-- Handle all works sections -->
      <xsl:if  test="//*/works-record">
        <xsl:call-template name='maintitle'>
          <xsl:with-param name="documentName">Contributions to Jointly Created Works</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="nameblock" />
        <xsl:apply-templates select="//*[substring(name(), string-length(name())-9) = '-completed']/works-record"/>
        <xsl:apply-templates select="//*[substring(name(), string-length(name())-9) = '-scheduled']/works-record"/>
        <xsl:apply-templates select="//*[substring(name(), string-length(name())-9) = '-submitted']/works-record"/>
      </xsl:if>    

    </fo:block>
  </xsl:template>

 <xsl:template match="//*[substring(name(), string-length(name())-9) = '-submitted']/works-record">
    <xsl:if test="contributors">
      <xsl:call-template name="works">
        <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
      </xsl:call-template>
      <xsl:if test="not(following-sibling::works-record/contributors) and following::*">
        <fo:block break-after="page"/>
      </xsl:if>
    </xsl:if>
 </xsl:template>

 <xsl:template match="//*[substring(name(), string-length(name())-9) = '-scheduled']/works-record">
    <xsl:if test="contributors">
      <xsl:call-template name="works">
         <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
      </xsl:call-template>
      <xsl:if test="not(following-sibling::works-record/contributors) and following::*">
        <fo:block break-after="page"/>
      </xsl:if>
    </xsl:if>
 </xsl:template>

 <xsl:template match="//*[substring(name(), string-length(name())-9) = '-completed']/works-record">
    <xsl:if test="contributors">
      <xsl:call-template name="works">
        <xsl:with-param name="sectionname"><xsl:value-of select="local-name(parent::*)"/></xsl:with-param>
      </xsl:call-template>
      <xsl:if test="not(following-sibling::works-record/contributors) and following::*">
        <fo:block break-after="page"/>
      </xsl:if>
    </xsl:if>    
 </xsl:template>

  <xsl:template name="works">
    <xsl:param name="sectionname"/>
    <fo:block>
      <xsl:if test="not(preceding-sibling::works-record)">
      <fo:block white-space-collapse="false" font-weight="bold" font-size="14pt" text-decoration="underline"
            space-after="12pt">
             <xsl:apply-templates select="../section-header" />
      </fo:block>
      </xsl:if>
      <xsl:call-template name="works-record"/>
    </fo:block>
 </xsl:template>

  <xsl:template match="works-record" name="works-record">

    <fo:block space-after="2pt" keep-together.within-page="always">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="100%" />
        <fo:table-body>
        <fo:table-row>
            <fo:table-cell>
              <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="15%" />
                <fo:table-column column-width="85%" />
                <fo:table-body start-indent="0pt">
                  <fo:table-row>
                    <fo:table-cell>
                       <fo:block font-weight="bold"><xsl:text>Work </xsl:text>#&colon;&space;</fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                      <fo:block text-align="left">
                         <xsl:apply-templates select="number" />
                      </fo:block>
                    
                      <fo:block text-align="left">
                        <fo:inline font-weight="bold">Year&colon;&space;</fo:inline><xsl:apply-templates select="workyear" />
                      </fo:block>

                      <fo:block text-align="left">
                        <fo:inline font-weight="bold">Title/Creators&colon;&space;</fo:inline>
                      </fo:block>
                      <fo:block text-align="left">
                        <fo:inline><xsl:apply-templates select="title" /></fo:inline>
                      </fo:block>
                      <fo:block text-align="left">
                        <fo:inline><xsl:apply-templates select="creators" /></fo:inline>
                      </fo:block>
                      <fo:block text-align="left"><fo:inline font-weight="bold">Contributions to Jointly Created Works&colon;</fo:inline>
                      </fo:block>
                      <fo:block text-align="left">
                        <xsl:apply-templates select="contributors" />
                      </fo:block>
                      <fo:block text-align="left">
                        <xsl:apply-templates select="link"> 
                          <xsl:with-param name="urldescription" select="'View Work Information'" />
                          <xsl:with-param name="urlvalue"><xsl:value-of select="link"/></xsl:with-param>
                        </xsl:apply-templates>
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:block>
  </xsl:template>


</xsl:stylesheet>
