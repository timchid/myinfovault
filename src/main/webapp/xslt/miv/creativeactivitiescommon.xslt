<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  
  <xsl:import href="mivCommon.xslt" />
  
  <!-- DATA TAGS -->
  <xsl:template match="workyear">
    <fo:inline>
      <xsl:apply-templates />
    </fo:inline>
  </xsl:template>

  <xsl:template match="worktypedescription">
    <fo:inline>
      <xsl:apply-templates />&colon;&space;
    </fo:inline>
  </xsl:template>

  <xsl:template match="eventtype">
    <fo:inline>
      <xsl:apply-templates />&colon;&space;
    </fo:inline>
  </xsl:template>

  <xsl:template match="eventdate|eventstartdate|eventenddate">
    <fo:inline>
      <xsl:apply-templates />
    </fo:inline>
  </xsl:template>

  <xsl:template match="reviewdate">
    <fo:inline>
      <xsl:apply-templates />
    </fo:inline>
  </xsl:template>

  <xsl:template match="printreviewyear">
    <fo:inline>
      <xsl:apply-templates />
    </fo:inline>
  </xsl:template>

  <xsl:template match="description">
    	<xsl:variable name="value" select="." />
	<xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
	<xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
	<xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
	<xsl:variable name="hasPunctuation">
		<xsl:choose>
			<xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<fo:inline>
		<xsl:text>&space;</xsl:text>
		<xsl:apply-templates />
		<xsl:if test="$hasPunctuation != '1'">&period;</xsl:if>
	</fo:inline>
  </xsl:template>

  <xsl:template match="venue">
    <xsl:apply-templates />
    <xsl:call-template name="addPunctuation">
      <xsl:with-param name="value" select="." />
      <xsl:with-param name="punctuationValue" select="'&period;'" />
      <xsl:with-param name="followWith" select="'&space;'" />
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="publication">
	<xsl:variable name="value" select="." />
	<xsl:variable name="lastCharacter" select="substring($value, string-length($value))" />
	<xsl:variable name="lastTwoCharacters" select="substring($value, string-length($value)-1)" />
	<xsl:variable name="lastThreeCharacters" select="substring($value, string-length($value)-2)" />
	<xsl:variable name="hasPunctuation">
		<xsl:choose>
			<xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
			<xsl:otherwise>0</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<fo:inline>
		<xsl:text>&space;</xsl:text>
		<xsl:apply-templates />
		<xsl:choose>
			<xsl:when test="../editors or ../publisher or ../volume or ../issue  or ../province or ../city or ../pages">&comma;&space;</xsl:when>
			<xsl:otherwise>&space;</xsl:otherwise>
		</xsl:choose>	
	</fo:inline>
  </xsl:template>
  
  <xsl:template match="creators" >
    <xsl:variable name="value" select="." />
    <xsl:variable name="lastCharacter"
      select="substring($value, string-length($value))" />
    <xsl:variable name="lastTwoCharacters"
      select="substring($value, string-length($value)-1)" />
    <xsl:variable name="lastThreeCharacters"
      select="substring($value, string-length($value)-2)" />
    <xsl:variable name="hasPunctuation">
      <xsl:choose>
        <xsl:when test="$lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <fo:inline>
      <xsl:apply-templates />
      <xsl:if test="$hasPunctuation != '1'">&period;</xsl:if>
      <xsl:text>&space;</xsl:text>
    </fo:inline>
  </xsl:template>

  <xsl:template match="reviewer">
    <fo:inline><xsl:apply-templates />&period;&space;</fo:inline>
  </xsl:template>

  <xsl:template match="publisher">
     <fo:inline><xsl:text>&space;</xsl:text><xsl:apply-templates />
         <xsl:choose>
	        <xsl:when test="../province or ../city">
	           &comma;
	        </xsl:when>
	        <xsl:otherwise>
	          <xsl:text>&period;</xsl:text>
	        </xsl:otherwise>
	     </xsl:choose>   
     </fo:inline>
  </xsl:template>

  <xsl:template match="contributor">
    <fo:inline>
      <xsl:apply-templates />
    </fo:inline>
  </xsl:template>

  <xsl:template match="title">
    <xsl:variable name="value" select="." />
    <xsl:variable name="lastCharacter"
      select="substring($value, string-length($value))" />
    <xsl:variable name="lastTwoCharacters"
      select="substring($value, string-length($value)-1)" />
    <xsl:variable name="lastThreeCharacters"
      select="substring($value, string-length($value)-2)" />
    <xsl:variable name="hasPunctuation">
      <xsl:choose>
        <!-- Match the TitleFormatter logic -->
        <xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <fo:inline>
      <xsl:apply-templates/>
      <xsl:if test="$hasPunctuation != '1'">&period;</xsl:if>
      <xsl:text>&space;</xsl:text>
    </fo:inline>
  </xsl:template>

  <xsl:template match="editors">
    <fo:inline>
      <xsl:text>&space;</xsl:text>
      <xsl:apply-templates />&comma;&space;&leftparen;ed&rightparen;
      <xsl:choose>
        <xsl:when test="../publisher or ../volume or ../issue  or ../province or ../city or ../pages">
           &comma;
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>&period;</xsl:text>
        </xsl:otherwise>
      </xsl:choose>      
    </fo:inline>
  </xsl:template>

  <xsl:template match="city">
    <fo:inline>
      <xsl:text>&space;</xsl:text>
      <xsl:apply-templates />
      <xsl:choose>
        <xsl:when test="../province">&comma;&space;</xsl:when>
        <xsl:otherwise>
              <xsl:if test="../pages">&period;</xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </fo:inline>
  </xsl:template>

  <xsl:template match="province">
    <fo:inline>
      <xsl:apply-templates />
      <xsl:text>&period;&space;</xsl:text>
    </fo:inline>
  </xsl:template>

  <xsl:template match="volume">
    <fo:inline>
       <xsl:text>Vol.&space;</xsl:text>
       <xsl:apply-templates />
    </fo:inline>
  </xsl:template>

  <xsl:template match="issue">
    <fo:inline>
      <xsl:if test="not(../volume)">
        <xsl:text>&space;</xsl:text>
      </xsl:if>&leftparen;<xsl:apply-templates />&rightparen;
    </fo:inline>
  </xsl:template>

  <xsl:template match="pages">
    <xsl:variable name="hasVolIssue">
      <xsl:choose>
        <xsl:when test="../volume or ../issue">1</xsl:when>
        <xsl:when test="../volume">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <fo:inline>
      <xsl:choose>
        <xsl:when test="$hasVolIssue = '1'">
           &space;pp&period;&space;<xsl:apply-templates />&period;&space;
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>&space;</xsl:text>
          <xsl:apply-templates />
        </xsl:otherwise>
      </xsl:choose>
    </fo:inline>
  </xsl:template>

  <xsl:template match="contribution">
    <xsl:apply-templates />
    <xsl:call-template name="addPunctuation">
      <xsl:with-param name="value" select="." />
      <xsl:with-param name="punctuationValue" select="'&period;'" />
      <xsl:with-param name="followWith" select="'&space;'" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="eventdescription">
    <xsl:apply-templates />
    <xsl:call-template name="addPunctuation">
      <xsl:with-param name="value" select="." />
      <xsl:with-param name="punctuationValue" select="'&comma;'" />
      <xsl:with-param name="followWith" select="'&space;'" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="venuedescription">
    <xsl:apply-templates />
    <xsl:call-template name="addPunctuation">
      <xsl:with-param name="value" select="." />
      <xsl:with-param name="punctuationValue" select="'&period;'" />
      <xsl:with-param name="followWith" select="'&space;'" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="monthname">
    <fo:inline>
      <xsl:apply-templates />&comma;&space;
    </fo:inline>
  </xsl:template>

  <xsl:template match="day">
    <fo:inline>
      <xsl:text>&space;</xsl:text>
      <xsl:apply-templates />&comma;&space;
    </fo:inline>
  </xsl:template>

  <xsl:template match="location">
    <xsl:variable name="value" select="." />
    <xsl:variable name="lastCharacter"
      select="substring($value, string-length($value))" />
    <xsl:variable name="lastTwoCharacters"
      select="substring($value, string-length($value)-1)" />
    <xsl:variable name="lastThreeCharacters"
      select="substring($value, string-length($value)-2)" />
    <xsl:variable name="hasPunctuation">
      <xsl:choose>
        <xsl:when test="$lastCharacter='.' or $lastCharacter='!' or $lastCharacter='?' or $lastCharacter='&quote;' or $lastThreeCharacters='...' or (translate($lastTwoCharacters,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')=$lastTwoCharacters and $lastCharacter='.' and $lastTwoCharacters!='..')">1</xsl:when>
        <xsl:otherwise>0</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <fo:inline>&comma;&space;
      <xsl:apply-templates />
      <xsl:if
        test="$hasPunctuation != '1' and $lastCharacter != '&qmark;' and $lastCharacter != '&expoint;' and $lastCharacter != '&quote;'">&period;
      </xsl:if>
    </fo:inline>
  </xsl:template>

  <xsl:template match="link">
    <xsl:param name="urldescription" />
    <xsl:param name="urlvalue" />
    <fo:block text-decoration="underline" color="#0033ff">
      <fo:basic-link>
        <xsl:attribute name="external-destination">
          <xsl:value-of select="$urlvalue"/>
        </xsl:attribute>
        <xsl:value-of select="$urldescription"/>
      </fo:basic-link>
    </fo:block>
  </xsl:template>
  
  <xsl:template name="footnotes">
      <fo:block font-size="8pt" font-style="italic"
        space-after="8pt">
        <fo:table table-layout="fixed" width="100%">
          <fo:table-column column-width="100%" />
          <fo:table-body>
            <fo:table-row>
              <fo:table-cell>
                <fo:block font-weight="bold">Footnotes:</fo:block>
                <xsl:for-each select="../*/footnote">
                  <fo:block>
                    #
                    <xsl:value-of select="../number" />&period;
                    <fo:inline space-start="0.5em">
                      <xsl:apply-templates />
                    </fo:inline>&period;
                  </fo:block>
                </xsl:for-each>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </fo:table>
      </fo:block>
  </xsl:template>
  
</xsl:stylesheet>
