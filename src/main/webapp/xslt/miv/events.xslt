<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:import href="creativeactivitiescommon.xslt" />

  <!-- Events -->
  <xsl:template match="events-record" name="events-record">

    <fo:block padding-bottom="2pt">

<!-- This is turned off for now to not print each work type on a different page -->
<!--     <xsl:variable name="eventtype"> -->
<!--        <xsl:value-of select="preceding-sibling::events-record[1]/eventtype"/> -->
<!--     </xsl:variable> -->

      <!-- If previous eventtype is the same as the current node, print the 
        eventtype -->
<!--       <xsl:variable name="printeventtype"> -->
<!--         <xsl:choose> -->
<!--           <xsl:when -->
<!--             test="eventtype = $eventtype"> -->
<!--             <xsl:value-of select="'false'" /> -->
<!--           </xsl:when> -->
<!--           <xsl:otherwise> -->
<!--             <xsl:value-of select="'true'" /> -->
<!--           </xsl:otherwise> -->
<!--         </xsl:choose> -->
<!--       </xsl:variable> -->

<!--       <xsl:if test="$printeventtype = 'true'"> -->
        <!-- If the preceding sibling is a events-record and we are changing types, we need a page break -->
        <!-- Prior to the page break check to print any footnotes or notations on the page we are finishing up -->
<!--         <xsl:if test="preceding-sibling::events-record"> -->
<!--           <fo:block break-before="page"></fo:block> -->
<!--         </xsl:if> -->
<!--         <fo:block white-space-collapse="false" font-weight="bold" -->
<!--           text-decoration="underline" font-style="italic" space-after="12pt"> -->
<!--           <xsl:value-of select="eventtype" /> -->
<!--         </fo:block> -->
<!--       </xsl:if> -->

    <!--Save the current type to use for numbering.  -->
<!--     <xsl:variable name="currenttype"> -->
<!--       <xsl:value-of select="eventtype" /> -->
<!--     </xsl:variable> -->

    <fo:table table-layout="fixed" width="100%">
    <fo:table-column column-width="100%" />
    <fo:table-body>
      <xsl:if test="./labelbefore">
              <fo:table-row keep-with-next="always">
              <fo:table-cell>
              <fo:block>
              <xsl:call-template name="label">
                      <xsl:with-param name="rightalign"
                      select="./labelbefore/@rightjustify" />
                      <xsl:with-param name="displaybefore"
                      select="1" />
              </xsl:call-template>
              </fo:block>
              </fo:table-cell>
              </fo:table-row>
      </xsl:if>
      <fo:table-row>
        <fo:table-cell>
          <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="17%" />
            <fo:table-column column-width="73%" />
            <fo:table-body start-indent="0pt">
              <fo:table-row>

                <!-- Don't use <number> elements since they are sequential for the record type and -->    
                <!-- we want the numbering to restart for each new type.-->    
                <fo:table-cell>
                  <fo:block text-align="left">
                    <xsl:apply-templates select="notation" />&space;
					<xsl:if test="descendant::footnote">&hash;</xsl:if>
					<xsl:apply-templates select="number" />&period;&space;
                    <!-- Uncomment to handle numbering when sorted by eventtype -->   
<!--                     <xsl:value-of select="count(preceding-sibling::events-record/eventtype[text() = $currenttype])+1"/>&period; -->
                  </fo:block>
                </fo:table-cell>

                <fo:table-cell padding-left="8pt">
                  <fo:block text-align="left">
                    <xsl:apply-templates select="eventstartdate" />
                    <xsl:if test="descendant::eventenddate">
                    	<fo:block>
                    	<fo:inline>
                    		<xsl:text>to&space;</xsl:text>
                    	</fo:inline>
                    	<xsl:apply-templates select="eventenddate" />
                    	</fo:block>
                    </xsl:if>                    
                  </fo:block>
                </fo:table-cell>
                    <fo:table-cell>
                        <fo:block>
                          <xsl:apply-templates select="eventdescription" />
                          <xsl:apply-templates select="venue" />
                          <xsl:apply-templates select="venuedescription" />
                          <xsl:apply-templates select="city" />
                          <xsl:apply-templates select="province" />
                        </fo:block>
                    </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell>
                  <fo:block></fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block></fo:block>
                </fo:table-cell>
                <xsl:choose>
                  <xsl:when test="link">
                    <fo:table-cell text-align="left">
                      <xsl:apply-templates select="link">
                        <xsl:with-param name="urldescription"
                          select="'View Event Information'" />
                        <xsl:with-param name="urlvalue">
                          <xsl:value-of select="link" />
                        </xsl:with-param>
                      </xsl:apply-templates>
                    </fo:table-cell>
                  </xsl:when>
                  <xsl:otherwise>
                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>
                  </xsl:otherwise>
                </xsl:choose>
              </fo:table-row>
            </fo:table-body>
          </fo:table>
        </fo:table-cell>
      </fo:table-row>
    </fo:table-body>
  </fo:table>
    </fo:block>
    <fo:block space-before="10pt" space-after="10pt">
      <xsl:if test="eventsworks-record">
        <xsl:apply-templates select="eventsworks-record" />
      </xsl:if>
    </fo:block>
    <xsl:if test="./labelafter">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="100%" />
         <fo:table-body>
                  <fo:table-row keep-with-next="always">
                  <fo:table-cell>
                  <fo:block>
                  <xsl:call-template name="label">
                          <xsl:with-param name="rightalign"
                          select="./labelafter/@rightjustify" />
                          <xsl:with-param name="displaybefore"
                          select="0" />
                  </xsl:call-template>
                  </fo:block>
                  </fo:table-cell>
                  </fo:table-row>
          </fo:table-body>
        </fo:table>  
    </xsl:if>

  <!-- Variables to check if we are at the end of a type section -->
<!--       <xsl:variable name="currenteventtype"> -->
<!--           <xsl:value-of select="self::events-record/eventtype"/> -->
<!--       </xsl:variable> -->

<!--       <xsl:variable name="nexteventtype"> -->
<!--           <xsl:value-of select="following-sibling::events-record[1]/eventtype"/> -->
<!--       </xsl:variable> -->

  <!-- If we are at the end of a type section, check to print footnotes and 
    notations for the section we are finishing up -->
<!--   <xsl:if test="$currenteventtype != $nexteventtype"> -->
<!--     See if there are any footnotes in this section -->
<!--     <xsl:variable name="footnotepresent"> -->
<!--       <xsl:for-each -->
<!--         select="preceding-sibling::events-record/eventtype[text() = $currenteventtype]"> -->
<!--         <xsl:if test="../footnote"> -->
<!--           <xsl:value-of select="'true'" /> -->
<!--         </xsl:if> -->
<!--       </xsl:for-each> -->
<!--       <xsl:if test="self::events-record/footnote"> -->
<!--         <xsl:value-of select="'true'" /> -->
<!--       </xsl:if> -->
<!--     </xsl:variable> -->
<!--     <xsl:if test="$footnotepresent != ''"> -->
<!--       <fo:block font-size="8pt" font-style="italic" -->
<!--         space-after="1pt" font-weight="bold">Footnotes:</fo:block> -->

      <!-- Get the footnotes for the preceeding entries matching the current work type -->
<!--       <xsl:for-each select="preceding-sibling::events-record/eventtype[text() = $currenteventtype]"> -->
<!--         <xsl:if test="../footnote"> -->
<!--           <fo:block font-size="8pt" font-style="italic" space-after="1pt"># -->
<!--             <xsl:value-of select="position()" />&period; -->
<!--               <fo:inline space-start="0.5em"><xsl:value-of select="../footnote" /></fo:inline>&period; -->
<!--           </fo:block> -->
<!--         </xsl:if> -->
<!--       </xsl:for-each> -->
<!--       Get the footnote for the current entry -->
<!--         <xsl:if test="self::events-record/footnote"> -->
<!--           <fo:block font-size="8pt" font-style="italic" space-after="1pt"># -->
<!--             <xsl:value-of select="count(preceding-sibling::events-record/eventtype[text() = $currenteventtype])+1" />&period; -->
<!--               <fo:inline space-start="0.5em"><xsl:value-of select="self::events-record/footnote" /></fo:inline>&period; -->
<!--           </fo:block> -->
<!--         </xsl:if> -->
<!--     </xsl:if> -->
    
    <!-- See if there are any notations in this section -->
<!--     <xsl:variable name="notations"> -->
<!--       <xsl:for-each -->
<!--         select="preceding-sibling::events-record/eventtype[text() = $currenteventtype]"> -->
<!--         <xsl:value-of select="../notation" /> -->
<!--       </xsl:for-each> -->
<!--       <xsl:value-of select="self::events-record/notation" /> -->
<!--     </xsl:variable> -->
<!--     <xsl:if test="$notations != ''"> -->
<!--       <xsl:call-template name="notations" /> -->
<!--     </xsl:if> -->
<!--   </xsl:if> -->
  </xsl:template>

  <xsl:template match="eventsworks-record" name="eventsworks-record">
    <fo:table table-layout="fixed" width="100%">
      <fo:table-column column-width="100%" />
      <fo:table-body start-indent="25pt" >
        <fo:table-row>
          <fo:table-cell>
            <fo:table table-layout="fixed" width="100%">
              <fo:table-column column-width="20%" />
              <fo:table-column column-width="80%" />
              <fo:table-body start-indent="0pt">
                <fo:table-row>
                  <fo:table-cell padding-right="5pt">
                    <fo:block text-align="right" >
                      <xsl:apply-templates select="workyear" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:apply-templates select="worktypedescription" />
                      <xsl:apply-templates select="creators" />
                      <xsl:apply-templates select="title" />
                      <xsl:apply-templates select="description" />
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>                
              </fo:table-body>
            </fo:table>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template> 


</xsl:stylesheet>
