<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="./mivCommon.xslt"/>
    <xsl:import href="./PkAes.xslt"/>
    <xsl:import href="./PkCom.xslt"/>
    <xsl:import href="./PkEkb.xslt"/>
    <xsl:import href="./PkEva.xslt"/>
    <xsl:import href="./PkGt.xslt"/>
    <xsl:import href="./PkHon.xslt"/>
    <xsl:import href="./PkPos.xslt"/>
    <xsl:import href="./PkPub.xslt"/>
    <xsl:import href="./PkStmt.xslt"/>
    <xsl:import href="./PkTea.xslt"/>
</xsl:stylesheet>
