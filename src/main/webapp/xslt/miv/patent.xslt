<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 <!ENTITY inpress "** IN PRESS **">
 <!ENTITY submitted "** SUBMITTED **">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <!-- PATENTS -->
    <xsl:template match="patent-record" >
	<fo:block padding-bottom="12pt">
	    <fo:table table-layout="fixed" width="100%">
		<fo:table-column column-width="100%" />
		<fo:table-body>
		    <xsl:if test="./labelbefore">
		    <fo:table-row keep-with-next="always">
			<fo:table-cell>
			    <fo:block>
				<xsl:call-template name="label">
				    <xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
				    <xsl:with-param name="displaybefore" select="1" />
				</xsl:call-template>
			    </fo:block>
			</fo:table-cell>
		    </fo:table-row>
		    </xsl:if>
		    <fo:table-row>
			<fo:table-cell>
			    <fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="5%" />
				<fo:table-column column-width="15%" />
				<fo:table-column column-width="2%" />
				<fo:table-column column-width="78%" />
				<fo:table-body start-indent="0pt">
				    <fo:table-row>
					<fo:table-cell>
					    <fo:block>
						<xsl:apply-templates select="notation" />
						<xsl:if test="descendant::footnote">&hash;&space;</xsl:if>
						<xsl:apply-templates select="number" />&period;&space;
						<fo:block>
						    <xsl:apply-templates select="engineeringannotation" />
						</fo:block>
					    </fo:block>
					</fo:table-cell>

					<fo:table-cell>
					    <fo:block text-align="right">
						<xsl:apply-templates select="year" />
					    </fo:block>
					</fo:table-cell>

					<fo:table-cell>
					    <fo:block>
						<fo:inline>
						    <xsl:text>&space;</xsl:text>
						</fo:inline>
					    </fo:block>
					</fo:table-cell>

					<fo:table-cell>
					    <fo:block>
						<fo:block>
						    <xsl:apply-templates select="author" />
						    <xsl:text>&space;</xsl:text>
						    <xsl:apply-templates select="title" />
						</fo:block>
						<fo:block>
						    <xsl:apply-templates select="patentdetail" />
						    <xsl:apply-templates select="patentinfo" />
						</fo:block>

						<xsl:if test="licensing">
						    <fo:block space-before="1em">
							<xsl:apply-templates select="licensing" />
						    </fo:block>
						</xsl:if>
					   </fo:block>
					</fo:table-cell>
				    </fo:table-row>
					<xsl:if test="link">
					    <fo:table-row>
						<fo:table-cell>
						    <fo:block></fo:block>
						</fo:table-cell>
						<fo:table-cell>
						    <fo:block></fo:block>
						</fo:table-cell>
						<fo:table-cell>
						    <fo:block></fo:block>
						</fo:table-cell>
						<fo:table-cell>
						    <fo:block text-decoration="underline" color="#0033ff">
							<fo:basic-link>
							    <xsl:attribute name="external-destination">
								<xsl:value-of select="link" />
							    </xsl:attribute>View Patent Information
							</fo:basic-link>
						    </fo:block>
						</fo:table-cell>
					    </fo:table-row>
					</xsl:if>
				</fo:table-body>
			    </fo:table>
			</fo:table-cell>
		    </fo:table-row>
		    <xsl:if test="./labelafter">
		    <fo:table-row keep-with-previous="always">
			<fo:table-cell>
			    <fo:block>
				<xsl:call-template name="label">
				    <xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
				    <xsl:with-param name="displaybefore" select="0" />
				</xsl:call-template>
			    </fo:block>
			</fo:table-cell>
		    </fo:table-row>
		    </xsl:if>
		</fo:table-body>
	    </fo:table>
	</fo:block>
   </xsl:template>

    <xsl:template match="patentinfo" >
	<fo:inline>
	    <xsl:text>&space;</xsl:text>
	    <xsl:apply-templates/>
	</fo:inline>
    </xsl:template>

    <xsl:template match="patentdetail" >
	<fo:inline>
	    <xsl:apply-templates/>
	</fo:inline>
    </xsl:template>

   <xsl:template match="licensing" >
	<fo:inline>
	    <xsl:apply-templates/>
	</fo:inline>
    </xsl:template>

</xsl:stylesheet>
