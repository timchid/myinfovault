<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:import href="creativeactivitiescommon.xslt" />

  <!-- Events -->
  <xsl:template match="publicationevents-record" name="publicationevents-record">
    <fo:block padding-bottom="2pt">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="100%" />
        <fo:table-body>
          <xsl:if test="./labelbefore">
            <fo:table-row keep-with-next="always">
              <fo:table-cell>
                <fo:block>
                  <xsl:call-template name="label">
                    <xsl:with-param name="rightalign"
                      select="./labelbefore/@rightjustify" />
                    <xsl:with-param name="displaybefore"
                      select="1" />
                  </xsl:call-template>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </xsl:if>
          <fo:table-row>
            <fo:table-cell>
              <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="10%" />
                <fo:table-column column-width="10%" />
                <fo:table-column column-width="80%" />
                <fo:table-body start-indent="0pt">
                  <fo:table-row>
                    <!-- Don't use <number> elements since they are sequential for the record type and -->    
                    <!-- we want the numbering to restart for each new type.-->    
                    <fo:table-cell>
                      <fo:block text-align="left">
                          	<xsl:apply-templates select="notation" />&space;
							<xsl:if test="descendant::footnote">&hash;</xsl:if>
							<xsl:apply-templates select="number" />&period;&space;
                      </fo:block>
                    </fo:table-cell>
    
                    <fo:table-cell padding-left="8pt">
                      <fo:block text-align="left">
                        <xsl:apply-templates select="year" />
                      </fo:block>
                    </fo:table-cell>
                        <fo:table-cell>
                          <fo:block>
                            <xsl:apply-templates select="title" />
                            <xsl:apply-templates select="publication" />
                            <xsl:apply-templates select="editors" />
                            <xsl:apply-templates select="volume" />
                            <xsl:apply-templates select="issue" />
                            <xsl:apply-templates select="pages" />
                            <xsl:apply-templates select="publisher" />
                            <xsl:apply-templates select="city" />
                            <xsl:apply-templates select="province" />
                          </fo:block>
                        </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                      <fo:block></fo:block>
                    </fo:table-cell>
                    <xsl:choose>
                      <xsl:when test="link">
                        <fo:table-cell text-align="left">
                          <xsl:apply-templates select="link">
                            <xsl:with-param name="urldescription"
                              select="'View Publication Information'" />
                            <xsl:with-param name="urlvalue">
                              <xsl:value-of select="link" />
                            </xsl:with-param>
                          </xsl:apply-templates>
                        </fo:table-cell>
                      </xsl:when>
                      <xsl:otherwise>
                        <fo:table-cell>
                          <fo:block></fo:block>
                        </fo:table-cell>
                      </xsl:otherwise>
                    </xsl:choose>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:block>
    <fo:block space-before="10pt" space-after="10pt">
       <xsl:if test="publicationeventsworks-record">
         <xsl:apply-templates select="publicationeventsworks-record" />
       </xsl:if>
    </fo:block>
    <xsl:if test="./labelafter">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="100%" />
         <fo:table-body>
           <fo:table-row keep-with-next="always">
             <fo:table-cell>
               <fo:block>
                 <xsl:call-template name="label">
                   <xsl:with-param name="rightalign"
                   select="./labelafter/@rightjustify" />
                   <xsl:with-param name="displaybefore"
                   select="0" />
                 </xsl:call-template>
               </fo:block>
             </fo:table-cell>
           </fo:table-row>
          </fo:table-body>
        </fo:table>  
    </xsl:if>
  </xsl:template>

  <xsl:template match="publicationeventsworks-record" name="publicationeventsworks-record">
    <fo:table table-layout="fixed" width="100%">
      <fo:table-column column-width="100%" />
      <fo:table-body start-indent="25pt" >
        <fo:table-row>
          <fo:table-cell>
            <fo:table table-layout="fixed" width="100%">
              <fo:table-column column-width="20%" />
              <fo:table-column column-width="80%" />
              <fo:table-body start-indent="0pt">
                <fo:table-row>
                  <fo:table-cell padding-right="5pt">
                    <fo:block text-align="right" >
                      <xsl:apply-templates select="workyear" />
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block>
                      <xsl:apply-templates select="worktypedescription" />
                      <xsl:apply-templates select="editors" />
                      <xsl:apply-templates select="title" />
                      <xsl:apply-templates select="description" />
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>                
              </fo:table-body>
            </fo:table>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
    </fo:table>
  </xsl:template>
</xsl:stylesheet>
