<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="b | B | strong">
      <fo:inline font-weight="bold"><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="i | I | em">
      <fo:inline font-style="italic"><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="u | U">
      <fo:inline text-decoration="underline"><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="sub | SUB">
      <fo:inline vertical-align="sub"><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="sup | SUP">
      <fo:inline vertical-align="super"><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="font | FONT">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="a | A">
      <fo:inline>
          <fo:basic-link>
              <xsl:choose>
              <xsl:when test="@href | @HREF">
              <xsl:attribute name="external-destination">
                  <xsl:value-of select="@href | @HREF"/>
              </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
              <xsl:attribute name="internal-destination">
                  <xsl:value-of select="@id"/>
              </xsl:attribute>
              </xsl:otherwise>
              </xsl:choose>
              
              <xsl:apply-templates />
          </fo:basic-link>
      </fo:inline>
   </xsl:template>
   
   <xsl:template match="p | P">
      <fo:block space-before="12pt"><xsl:apply-templates /></fo:block>
   </xsl:template>
   
   <xsl:template match="br | BR">
      <fo:block>&#160;</fo:block>
   </xsl:template>
   
</xsl:stylesheet>