<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 ]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:include href="../mivMoreCommon.xslt" />

  <!-- common setup for all document types -->
	<xsl:template name="documentSetup">
    <xsl:param name="documentName" />
    <xsl:param name="title1" />
    <xsl:param name="title2" />
    <xsl:param name="titleAdditional" />
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
         <fo:layout-master-set>
            <fo:simple-page-master master-name="myinfovault"
               page-height="11in" page-width="8.5in" margin-top=".5in"
               margin-bottom=".5in" margin-left=".5in"
               margin-right=".5in">
               <fo:region-body margin-bottom="10pt" region-name="xsl-region-body"/>
               <fo:region-before region-name="xsl-region-before" />
               <fo:region-after region-name="xsl-region-after" />
            </fo:simple-page-master>
         </fo:layout-master-set>
         <fo:bookmark-tree>
            <fo:bookmark internal-destination="maintitle">
               <fo:bookmark-title><xsl:value-of select="$documentName"/></fo:bookmark-title>
            </fo:bookmark>
            <xsl:if test="$title1">
              <fo:bookmark internal-destination="title1">
                <fo:bookmark-title>
                  <xsl:value-of select="$title1" />
                </fo:bookmark-title>
              </fo:bookmark>
            </xsl:if>
            <xsl:if test="$title2">
              <fo:bookmark internal-destination="title2">
                <fo:bookmark-title>
                  <xsl:value-of select="$title2" />
                </fo:bookmark-title>
              </fo:bookmark>
            </xsl:if>
            <xsl:if test="$titleAdditional">
              <fo:bookmark internal-destination="title-additional">
                <fo:bookmark-title>
                  <xsl:value-of select="$titleAdditional" />
                </fo:bookmark-title>
              </fo:bookmark>
            </xsl:if>
         </fo:bookmark-tree>
         <fo:page-sequence master-reference="myinfovault">
            <fo:static-content flow-name="xsl-region-after">
               <fo:table table-layout="fixed" width="100%">
                   <fo:table-column column-width="90%" />
                   <fo:table-column column-width="10%" />
                   <fo:table-body>
                       <fo:table-row>
                           <fo:table-cell>
                               <fo:block vertical-align="bottom" text-align="left">
                                 <fo:inline>
                                   <xsl:value-of select="$documentName"/>
                                 </fo:inline>
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                               <fo:block vertical-align="bottom" text-align="right">
                                   <fo:page-number />
                               </fo:block>
                           </fo:table-cell>
                       </fo:table-row>
                   </fo:table-body>
               </fo:table>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
               <xsl:apply-templates />
            </fo:flow>
         </fo:page-sequence>
      </fo:root>
   </xsl:template>
   
   <xsl:template name="maintitle">
      <xsl:param name="documentName" />
      <fo:block id="maintitle"
                font-size="20pt"
                font-weight="bold"
                space-after="16pt"
                text-align="center"><xsl:value-of select="$documentName"/></fo:block>
   </xsl:template>
  
  <xsl:template match="blockquote">
	  <fo:block start-indent="2em" >
	  	<xsl:apply-templates/>
	  </fo:block>
  </xsl:template>

<!-- Used for UL/OL Indenting -->
<xsl:template match="li[@class='indentoutdent']">
		<fo:list-item xsl:use-attribute-sets="list.item">
			<!-- Generate list label.-->
			<!-- The end position of the label is calculated by label-end()function. -->
			<!-- The characters for label of line are specified in the type attribute.
				Initial value is “&#x2022;” -->
			<fo:list-item-label end-indent="label-end()">
				<fo:block text-align="end" start-indent="2em">
					<xsl:text>  </xsl:text>
				</fo:block>
			</fo:list-item-label>
			<!-- Generate the list body.-->
			<!-- The starting position of the label is calculated by
				the body-start() function -->
			<fo:list-item-body start-indent="body-start()" text-align="justify">
				<fo:block>
					<xsl:apply-templates />
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>
	</xsl:template>
 
<!-- Additional Information Two Column layout -->

  <xsl:template match="additional-record">
       <fo:block padding-bottom="10pt">
        <!-- xsl:apply-templates select="number" />.
        <xsl:apply-templates select="content" / -->
           <xsl:apply-templates select="addheader" /> 
           <xsl:apply-templates select="addcontent" />   
    </fo:block>
    </xsl:template>
   
  <!-- Code to handle Table data --> 
  <!-- when table-and-caption is supported, that will be the
   wrapper for this template -->
   <xsl:template match="table">
     <!-- Check to make sure there is a tbody element -->
     <xsl:choose>
       <xsl:when test="name(child::node())='tbody'">
           <xsl:apply-templates/>
       </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="tbody"/>
        </xsl:otherwise>
     </xsl:choose>
   </xsl:template>  
    
    <!--
    find the width= attribute of all the <th> and <td>
    elements in the first <tr> of this table. They are
    in pixels, so divide by 72 to get inches
  -->
   <xsl:template name='tbody' match="tbody">
   <fo:table>
      <xsl:attribute name="space-after">10pt</xsl:attribute>
      <xsl:for-each select="tr[1]/th|tr[1]/td">
        <fo:table-column>
         <xsl:attribute name="width">
           <xsl:choose>
             <xsl:when test="ancestor::table/@width">
               <xsl:value-of select="floor(ancestor::table/@width div 72)"/>in         
             </xsl:when>
             <xsl:otherwise>
               <!-- Provide a default -->
               <xsl:value-of select="510"/>in         
             </xsl:otherwise>
           </xsl:choose>
         </xsl:attribute>      
         <xsl:attribute name="column-width">    
              <xsl:value-of select="@width"/>
        </xsl:attribute> 
      </fo:table-column>  
      </xsl:for-each>       
    <fo:table-body start-indent="0pt">
      <xsl:apply-templates />
    </fo:table-body>  
   </fo:table>   
   </xsl:template>
   
   <xsl:template match="addheader">
    <fo:block white-space-collapse="false" font-size="12pt" font-weight="bold" text-decoration="underline"
      padding-top="12pt">
      <xsl:apply-templates />
    </fo:block>
  </xsl:template>

  <xsl:template match="addcontent">
    <fo:block font-size="12pt">
      <xsl:apply-templates />
    </fo:block>
  </xsl:template>

	<xsl:template match="span[@class='styleB']">
		<fo:inline font-weight="bold">
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="span[@class='styleU']">
		<fo:inline text-decoration="underline">
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="span[@class='styleI']">
		<fo:inline font-style="italic">
			<xsl:apply-templates />
		</fo:inline>
        </xsl:template>
	<xsl:template match="span[@class='styleBI']">
		<fo:inline font-weight="bold" font-style="italic">
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="span[@class='styleBU']">
		<fo:inline font-weight="bold" text-decoration="underline">
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="span[@class='styleIU']">
		<fo:inline font-style="italic" text-decoration="underline">
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>

	<xsl:template match="span[@class='styleBIU']">
		<fo:inline font-weight="bold" font-style="italic"
			text-decoration="underline">
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>   
	
	<xsl:template match="symbol">
		<fo:inline font-family="Symbol" font-size="9pt">
          		<xsl:apply-templates/>
		</fo:inline>
	</xsl:template>
</xsl:stylesheet>