<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!-- PERSONAL -->

    <xsl:template match="personal">
        <fo:block id="personal">
            <fo:table table-layout="fixed" width="100%">
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="80%"/>
                <fo:table-body start-indent="0pt">
                    <xsl:apply-templates select="name-record"/>
                    <xsl:apply-templates select="phone-record"/>
                    <xsl:apply-templates select="email-record"/>
                    <xsl:apply-templates select="link-record"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>


    <xsl:template match="name-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>Name:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="fname"/>
                    <xsl:apply-templates select="lname"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="personal-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>Birth Date:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="birthdate"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="address-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block><xsl:apply-templates select="addresstype"/>:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="street1"/>
                    <xsl:apply-templates select="street2"/>
                    <xsl:apply-templates select="city"/>
                    <xsl:apply-templates select="state"/>
                    <xsl:apply-templates select="zipcode"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="phone-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block><xsl:apply-templates select="phonetype"/>:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="email-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>E-mail:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="link-record">
        <fo:table-row>
            <fo:table-cell>
                <fo:block>Web Link:</fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

</xsl:stylesheet>
