<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="service-board">
      <xsl:if test="advisoryboard-record">
            <fo:block id="service-board">
                <fo:table table-layout="fixed" width="100%">
                    <fo:table-column column-width="20%" />
                    <fo:table-column column-width="80%" />
                    <fo:table-body>                
                        <xsl:apply-templates select="advisoryboard-record"/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
      </xsl:if>
    </xsl:template>
    <xsl:template match="advisoryboard-record">            
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates select="years" />
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
                <fo:block>
                    <xsl:apply-templates
                        select="description" mode="serviceboard"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <xsl:template match="years" mode="serviceboard">
        <fo:inline><xsl:apply-templates/></fo:inline>
    </xsl:template>
    <xsl:template match="description" mode="serviceboard">
        <fo:inline><xsl:apply-templates/></fo:inline>
    </xsl:template>    
    </xsl:stylesheet>