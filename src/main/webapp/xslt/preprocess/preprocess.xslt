<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/>

<!-- This stylesheet makes a copy of the input XML precessing selected elements as needed -->
<!-- prior to FO transformation -->


<xsl:template match="node() | @*">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<!-- Process symbol elements changing to highlight-->
<xsl:template match="symbol">
  <xsl:element name="highlight">
    <xsl:copy-of select="@*"/>
    <xsl:value-of select="."/>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
