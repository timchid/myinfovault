<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.1">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-system="${dtd.publications}"/>

    <!-- DON'T Strip space nodes within all elements -->
    <!--<xsl:strip-space elements="*"/>-->

    <xsl:template match="/">
        <document>
            <records>
                <xsl:apply-templates select="xml/records/record"/>
            </records>
        </document>
    </xsl:template>

    <xsl:template match="record">
        <record>
            <!-- external ID -->
            <xsl:choose>
                <!-- use the electronic-resource-num if not empty -->
                <xsl:when test="electronic-resource-num[normalize-space(text())]">
                    <xsl:apply-templates select="electronic-resource-num"/>
                </xsl:when>
                
                <!-- else, use the accession-num if not empty -->
                <xsl:when test="accession-num[normalize-space(text())]">
                    <xsl:apply-templates select="accession-num"/>
                </xsl:when>
                
                <!-- otherwise, use the concatenation of the foreign key database ID and record number -->
                <xsl:otherwise>
                    <external-id>
                        <xsl:value-of select="foreign-keys/key[1]/@db-id"/>
                        <xsl:value-of select="foreign-keys/key[1]"/>
                    </external-id>
                </xsl:otherwise>
            </xsl:choose>
        
            <xsl:apply-templates select="ref-type"/>
            <xsl:apply-templates select="source-app"/>
            <xsl:apply-templates select="dates"/>
            <xsl:apply-templates select="contributors"/>
            <xsl:apply-templates select="titles"/>
            <xsl:apply-templates select="publisher"/>
            <xsl:apply-templates select="pub-location"/>
            <xsl:apply-templates select="periodical"/>
            <xsl:apply-templates select="volume"/>
            <xsl:apply-templates select="issue"/>
            <xsl:apply-templates select="number"/>
            <xsl:apply-templates select="pages"/>
            <xsl:apply-templates select="isbn"/>
            <xsl:apply-templates select="editor"/>
            <xsl:apply-templates select="urls"/>
        </record>
    </xsl:template>
    
    <xsl:template match="electronic-resource-num|accession-num">
        <external-id><xsl:apply-templates /></external-id>
    </xsl:template>
    
    <xsl:template match="ref-type">
        <xsl:variable name="typeid" select="." />
        <ref-type>
            <xsl:choose>
                <xsl:when test="$typeid=3">9</xsl:when>
                <xsl:when test="$typeid=6">3</xsl:when>
                <xsl:when test="$typeid=17">2</xsl:when>
                <xsl:when test="$typeid=28">4</xsl:when>
                <xsl:otherwise>2</xsl:otherwise>
            </xsl:choose>
        </ref-type>
        <ref-status>1</ref-status>
    </xsl:template>

    <xsl:template match="source-app">
        <external-source>
            <xsl:attribute name="sourceid">
                <xsl:choose>
                    <xsl:when test="@name='PubMed'">1</xsl:when>
                    <xsl:when test="@name='EndNote'">2</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </external-source>
    </xsl:template>

    <xsl:template match="dates">
        <date>
            <year><xsl:value-of select="year"/></year>
            <xsl:apply-templates select="day" />
            <xsl:apply-templates select="month" />
        </date>
    </xsl:template>
    <xsl:template match="day">
        <day><xsl:apply-templates /></day>
    </xsl:template>
    <xsl:template match="month">
        <month><xsl:apply-templates /></month>
    </xsl:template>

    <xsl:template match="contributors">
        <authors><xsl:apply-templates select="authors/author" /></authors>
        <editors><xsl:apply-templates select="secondary-authors/author" /></editors>
    </xsl:template>
    <xsl:template match="author">
        <author><xsl:apply-templates /></author>
    </xsl:template>

    <xsl:template match="titles">
        <title><xsl:apply-templates select="title"/></title>
        <periodical><xsl:apply-templates select="secondary-title"/></periodical>
    </xsl:template>

    <xsl:template match="publisher">
        <publisher><xsl:apply-templates /></publisher>
    </xsl:template>
    <xsl:template match="pub-location">
        <location><xsl:apply-templates /></location>
    </xsl:template>

<!-- Moved up, now using titles/secondary-title
    <xsl:template match="periodical/full-title">
        <periodical>
            <xsl:apply-templates/>
        </periodical>
    </xsl:template> -->

    <xsl:template match="volume">
        <volume><xsl:apply-templates /></volume>
    </xsl:template>

    <xsl:template match="issue">
        <issue><xsl:apply-templates /></issue>
    </xsl:template>

    <xsl:template match="number">
        <issue><xsl:apply-templates /></issue>
    </xsl:template>

    <xsl:template match="pages">
        <pages><xsl:apply-templates /></pages>
    </xsl:template>

    <xsl:template match="isbn">
        <isbn><xsl:apply-templates /></isbn>
    </xsl:template>

    <xsl:template match="editor">
        <editor><xsl:apply-templates /></editor>
    </xsl:template>

    <xsl:template match="urls">
        <url><xsl:value-of select="related-urls/url"/></url>
        <!-- <url><xsl:apply-templates /></url> -->
    </xsl:template>
<!-- <xsl:template match="related-url">
        <xsl:apply-templates />
    </xsl:template>
    <xsl:template match="url">
        <xsl:apply-templates />
    </xsl:template> -->

    <xsl:template match="style[contains(@face,'superscript')]">&lt;sup&gt;<xsl:apply-templates/>&lt;/sup&gt;</xsl:template>
    <xsl:template match="style[contains(@face,'subscript')]">&lt;sub&gt;<xsl:apply-templates/>&lt;/sub&gt;</xsl:template>

</xsl:stylesheet>
