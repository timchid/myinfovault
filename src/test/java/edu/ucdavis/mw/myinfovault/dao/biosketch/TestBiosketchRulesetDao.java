package edu.ucdavis.mw.myinfovault.dao.biosketch;

import java.sql.SQLException;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;

/**
 * @author dreddy
 * @since MIV 2.1
 */
@Ignore
public class TestBiosketchRulesetDao extends TestCase
{
    private BiosketchRulesetDao biosketchRulesetDao;

    @Override
    protected void setUp() throws Exception
    {
        ClassPathXmlApplicationContext util = new ClassPathXmlApplicationContext(
                new String[] { "/edu/ucdavis/mw/myinfovault/dao/biosketch/test-myinfovault-data.xml" });

        biosketchRulesetDao = (BiosketchRulesetDao) util.getBean("biosketchRulesetDao");
    }

    public void testFindRulesetListWithCriteria()
    {
        try
        {
            List<Ruleset> biosketchRulesetList = biosketchRulesetDao.findRulesetListWithCriteria(134, 18807);

            for (int i = 0; i < biosketchRulesetList.size(); i++)
            {
                Ruleset ruleset = biosketchRulesetList.get(i);
                System.out.println("RULESET - ID is " + ruleset.getId());
                System.out.println("CRITERIA IS - ID is " + ruleset.toString());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }


    public void testDeleteBiosketchCriteria()
    {
        try
        {
            biosketchRulesetDao.deleteBiosketchCriteria(84);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }
}
