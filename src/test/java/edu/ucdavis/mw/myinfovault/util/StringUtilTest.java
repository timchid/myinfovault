/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: StringUtilTest.java
 */

package edu.ucdavis.mw.myinfovault.util;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.mock.web.MockServletContext;

import edu.ucdavis.myinfovault.MIVConfig;

/**
 * Tests {@link StringUtil}.
 *
 * @author Craig Gilmore
 * @since v4.6.1
 */
public class StringUtilTest
{
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        MIVConfig.getConfig(new MockServletContext());
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.util.StringUtil#getSizeInBytes(java.lang.String)}.
     */
    @Test
    public final void testGetSizeInBytes()
    {
        assertEquals(13, StringUtil.getSizeInBytes("I'll be back."));
    }


    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.util.StringUtil#toJson(java.lang.Iterable)}.
     */
    @Test
    public final void testToJson()
    {
        List<Map<String,String>> testList = new ArrayList<Map<String,String>>();

        Map<String,String> testMap1 = new HashMap<String,String>();
        testMap1.put("key1a", "value1a");
        testMap1.put("key1b", "value1b");

        Map<String,String> testMap2 = new HashMap<String,String>();
        testMap2.put("key2", "value2");

        testList.add(testMap1);
        testList.add(testMap2);

        assertEquals("[ { \"key1b\" : \"value1b\", \"key1a\" : \"value1a\" },\n{ \"key2\" : \"value2\" } ]\n",
                     StringUtil.toJson(testList));
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.util.StringUtil#capitalizeFully(java.lang.String)}.
     */
    @Test
    public final void testCapitalizeFully()
    {
        // skipList - "in,on,and"
        assertEquals("I Swam in A Lake on Tuesday and Wednesday.",
                     StringUtil.capitalizeFully("I SWAM in A LAKE on TUESDAY and WEDNESDAY."));
    }
}
