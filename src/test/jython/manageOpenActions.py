'''
This script exercises the MIV Manage Open Actions functionality.
Dossiers are searched, selected and routed where possible. Dossiers
are routed forward (route) or backward (return). If routing forward
to the next workflow node is not possible, the dossier is routed to
the previous workflow node and visa versa.   

Note that routing  may be throttled to only allow a specified number of 
documents to be routed during a specific time interval. To configure a maximum
number of routing requests per time interval, set the controlMinRouteInterval 
value in the grinder.properties file.   


Created June 2010
@author: rhendric
'''

import mivCommon
from net.grinder.script import Test
from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPluginControl, HTTPRequest
from HTTPClient import NVPair
connectionDefaults = HTTPPluginControl.getConnectionDefaults()
httpUtilities = HTTPPluginControl.getHTTPUtilities()

from java.util.regex import Matcher
from java.util.regex import Pattern

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

# These definitions at the top level of the file are evaluated once,
# when the worker process is started.

connectionDefaults.setUseCookies(1)
connectionDefaults.useContentEncoding = 1
connectionDefaults.setFollowRedirects(0)

# defaults
nextTestNumber = 0
testIncrement = 100
testSuccess = 1
scriptName = 'manageOpenActions'    
serverURL = mivCommon.getServerUrl()


connectionDefaults.defaultHeaders = \
  ( NVPair('Accept-Language', 'en-us,en;q=0.5'),
    NVPair('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.7'),
    NVPair('Accept-Encoding', 'gzip,deflate'),
    NVPair('User-Agent', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.2) Gecko/2008092418 CentOS/3.0.2-3.el5.centos Firefox/3.0.2'), )

headers0= \
  ( NVPair('Accept', '*/*'),
    NVPair('Referer', 'https://'+serverURL+'/miv/MIVMain'), )

url0 = 'https://'+serverURL+':443'


class TestRunner:
  """A TestRunner instance is created for each worker thread."""
  
  def getScriptName(self):
      return scriptName

  def openActions_page(self):
    """GET OpenActions"""
    
    result = self.request101.GET('/miv/OpenActions?_flowId=openactions-flow')

    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
        mivCommon.setTestSuccess(self,0)

    # Expecting 302 'Moved Temporarily'
    if result.getStatusCode() == 302:
        self.token__flowExecutionKey = \
        httpUtilities.valueFromLocationURI('_flowExecutionKey') 
        redirectURL = mivCommon.getRedirectURL(result)
        mivCommon.logMessage("Redirecting to: %s" % redirectURL)
        result1 = self.request102.GET(redirectURL)
        if result1.getStatusCode() >= 400 and result1.getStatusCode() <= 599:
            grinder.getStatistics().getForLastTest().setSuccess(0)
        
        
        # set the departments/schools allowed to select from for search
        mivCommon.setSearchCriteria(self, result1.getText())  

    mivCommon.writeToFile(self.fileHandle,"\nOpenActions_page\n"+result1.toString()+result1.getText())
    
    return result

  def openActionsSearch_page(self):
    """GET OpenActions Search"""
    
    logMessage = "OpenActions Searching by "+self.searchBy+": "+self.searchValue
    mivCommon.logAndPrint(logMessage)

    # Expecting 302 'Moved Temporarily'
    request = '/miv/OpenActions'+ \
      '?_flowExecutionKey='+ \
      self.token__flowExecutionKey+ \
      '&inputName='+self.token_inputName+ \
      '&lname='+ \
      self.token_lname+ \
      '&'+self.token_eventId+'=Search+Now!' + \
      '&department='+ \
      self.token_department
      
    if self.token_school != '':
       request = request+'&school=' + self.token_school
    
    result = self.request103.GET(request)

    mivCommon.writeToFile(self.fileHandle,"\nOpenActionsSearch_page\n"+result.toString()+result.getText())

    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
        mivCommon.setTestSuccess(self,0)

    if result.getStatusCode() == 302:
        self.token__flowExecutionKey = \
          httpUtilities.valueFromLocationURI('_flowExecutionKey') 
        redirectURL = mivCommon.getRedirectURL(result)
        result1 = self.request104.GET(redirectURL)
        mivCommon.writeToFile(self.fileHandle,"\nOpenActionsSearchResults_page\n"+result1.toString()+result1.getText())
    
    return result1

  def viewDossierStatus_page(self):
    if len(self.dossierSelected) > 0:

       pattern = Pattern.compile(">.*?</a>");
       result = pattern.matcher(self.dossierSelected)
       self.userName = ''
       if result.find():
           self.userName = result.group().lstrip(">").rstrip("</a>")

       mivCommon.parseTokens(self, self.dossierSelected)    

       logMessage = 'Selected Dossier '+self.token_dossierId+' for '+self.userName
       mivCommon.logAndPrint(logMessage)

       result = self.request105.GET('/miv/OpenActions' +
         '?userId=' +
         self.token_userId +
         '&dossierId=' +
         self.token_dossierId +
         '&_eventId=' +
         self.token__eventId +
         '&_flowExecutionKey=' +
         self.token__flowExecutionKey)
 
       if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
         mivCommon.setTestSuccess(self,0)

       if result.getStatusCode() == 302:
         redirectURL = mivCommon.getRedirectURL(result)
         result1 = self.request106.GET(redirectURL)
         mivCommon.writeToFile(self.fileHandle,"\nViewDossierStatusResults_page\n"+result1.toString()+result1.getText())
   
         # Parse the routing event from the page for this dossier
         pattern = Pattern.compile("<div id=\"routeit\".*?>.*?</div>")
         resultText = ' '.join(result1.getText().split());          # remove all white space
         routeItResult = pattern.matcher(resultText)
         if routeItResult.find():
             mivCommon.parseTokens(self,routeItResult.group())
             if self.token__eventId != 'route':
                 pattern = Pattern.compile("<div id=\"returnit\".*?>.*?</div>")
                 returnItResult = pattern.matcher(resultText)
                 if returnItResult.find():
                     mivCommon.parseTokens(self, returnItResult.group())
    
    return result1
      
      
  def routeDossierResults_page(self):
    
    logMessage = 'Routing ('+self.token__eventId+') Dossier '+self.token_dossierId+' for '+self.userName
    mivCommon.logAndPrint(logMessage)
    
    result = self.request107.GET('/miv/OpenActions' +
      '?_flowExecutionKey=' +
      self.token__flowExecutionKey +
      '&_eventId=' +
      self.token__eventId +
      '&dossierId=' +
      self.token_dossierId)

    if result.getStatusCode() == 302:
        redirectURL = mivCommon.getRedirectURL(result)
        result1 = self.request108.GET(redirectURL)
        mivCommon.writeToFile(self.fileHandle,"\nRouteDossierResults_page\n"+result1.toString()+result1.getText())

    return result1
        
  def viewDossierAsOnePDF_page(self):
    
    logMessage = 'Viewing Dossier '+self.token_dossierId+' for '+self.userName
    mivCommon.logAndPrint(logMessage)
    result = self.request109.GET('/miv/documents/' +
      self.token_userId +
      '/dossier/' +
      self.token_dossierId +
      '/dossier.pdf')
      
    return result;
      
  
  def __call__(self):
    """This method is called for every run performed by the worker thread."""
    
    # open a test file
    self.fileHandle = mivCommon.openTestFile(self.getScriptName())

    # defaults
    if self.nextTestNumber is None:
        self.nextTestNumber = nextTestNumber
    
    if self.testIncrement is None:
        self.testIncrement = testIncrement

    self. testSuccess = 1
   
    # If the response back from the server is not one that we except,
    # we want to mark the test as unsuccessful and not include the statistics
    # in the test times. To do this, the delayReports variable can be set to 1.
    # Doing so will delay the reporting back of the statistics until after
    # the test has completed and we have had chance to check its operation.
    # The default is to report back when the test returns control back to
    # the script, i.e. immediately after a test has executed.
    grinder.getStatistics().setDelayReports(1)
    
    # Create an HTTPRequest for each request, then replace the
    # reference to the HTTPRequest with an instrumented version.
    # You can access the unadorned instance using request101.__target__.
    self.nextTestNumber = self.nextTestNumber+self.testIncrement   
    self.request101 = HTTPRequest(url=url0, headers=headers0)
    self.request101 = Test(self.nextTestNumber+1, 'GET OpenActions').wrap(self.request101)
    self.request102 = HTTPRequest(url=url0, headers=headers0)
    self.request102 = Test(self.nextTestNumber+2, 'OpenActions').wrap(self.request102)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'openActions_page'), 'openActions_page')

    self.nextTestNumber += self.testIncrement
    self.request103 = HTTPRequest(url=url0, headers=headers0)
    self.request103 = Test(self.nextTestNumber+1, 'GET OpenActionsSearchCriteria').wrap(self.request103)
    self.request104 = HTTPRequest(url=url0, headers=headers0)
    self.request104 = Test(self.nextTestNumber+2, 'OpenActionsResults').wrap(self.request104)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'openActionsSearch_page'), 'openActionsSearch_page')

    self.nextTestNumber += self.testIncrement
    self.request105 = HTTPRequest(url=url0, headers=headers0)
    self.request105 = Test(self.nextTestNumber+1, 'GET Dossier').wrap(self.request105)
    self.request106 = HTTPRequest(url=url0, headers=headers0)
    self.request106 = Test(self.nextTestNumber+2, 'DossierResults').wrap(self.request106)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'viewDossierStatus_page'), 'viewDossierStatus_page')
    
    self.nextTestNumber += self.testIncrement
    self.request107 = HTTPRequest(url=url0, headers=headers0)
    self.request107 = Test(self.nextTestNumber+1, 'Route Dossier').wrap(self.request107)
    self.request108 = HTTPRequest(url=url0, headers=headers0)
    self.request108 = Test(self.nextTestNumber+2, 'Route DossierResults').wrap(self.request108)
    self.request109 = HTTPRequest(url=url0, headers=headers0)
    self.request109 = Test(self.nextTestNumber+3, 'View DossierPDF').wrap(self.request109)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'routeDossierResults_page'), 'routeDossierResults_page')
    
    mivCommon.sleep()  
    self.openActions_page()      # GET (requests 101)

    mivCommon.sleep()  
    result = self.openActionsSearch_page()      # GET (requests 102-103)

    searchResult = mivCommon.getSearchResult(self,result.getText())
    mivCommon.logAndPrint(searchResult)

    # If a Dossier is selected, view the status and route it
    if len(self.dossierSelected) > 0:
       self.viewDossierStatus_page()    
       # If there is a route or return event, route the dossier
       if (self.token__eventId == 'route' or self.token__eventId == 'return') and mivCommon.validateRouteLimit():
           self.routeDossierResults_page()
       else: 
          self.viewDossierAsOnePDF_page()     

    scriptResult = "Success"
    if not mivCommon.getTestSuccess(self) :
       scriptResult = "Failed"
    mivCommon.printMessage(self.getScriptName()+' Script Complete: '+ scriptResult)

    mivCommon.closeTestFile(self.fileHandle)    

def instrumentMethod(test, method_name, c=TestRunner):
    """Instrument a method with the given Test."""
    unadorned = getattr(c, method_name)
    import new
    method = new.instancemethod(test.wrap(unadorned), None, c)
    setattr(c, method_name, method)

