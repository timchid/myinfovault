'''
This script randomly logs in an MIV User.

Created on June 2010
@author: rhendric
'''

import mivCommon
from net.grinder.script import Test
from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPluginControl, HTTPRequest
from HTTPClient import NVPair


connectionDefaults = HTTPPluginControl.getConnectionDefaults()
connectionDefaults.setUseCookies(1)
connectionDefaults.useContentEncoding = 1
connectionDefaults.setFollowRedirects(0)


httpUtilities = HTTPPluginControl.getHTTPUtilities()
serverURL = mivCommon.getServerUrl()

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

# These definitions at the top level of the file are evaluated once,
# when the worker process is started.

# defaults
nextTestNumber = 0
testIncrement = 100
testSuccess = 1

scriptName = 'mivLogin'

connectionDefaults.defaultHeaders = \
  ( NVPair('Accept-Language', 'en-us,en;q=0.5'),
    NVPair('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.7'),
    NVPair('Accept-Encoding', 'gzip,deflate'),
    NVPair('User-Agent', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.2) Gecko/2008092418 CentOS/3.0.2-3.el5.centos Firefox/3.0.2'), )

headers0= \
  ( NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    NVPair('Referer', 'http://'+mivCommon.getServerUrl()+'/'), )

headers1= \
  ( NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    NVPair('Referer', 'https://cas-dev.ucdavis.edu/impersonate/login?service=https%3a%2f%2f'+serverURL+'%2fmiv%2fMIVLogin'), )


url0 = 'https://'+mivCommon.getServerUrl()+':443'
url1 = 'https://cas-dev.ucdavis.edu:443'    

class TestRunner:
  """A TestRunner instance is created for each worker thread."""

  def getScriptName(self):
      return scriptName

  # A method for each recorded page.
  def MIVLogin_initial_page(self):
  
    # Expecting 302 'Found'
    result = self.request101.GET('/miv/MIVLogin')
    
    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
       mivCommon.setTestSuccess(self,0)
    
    self.token_service = \
      httpUtilities.valueFromLocationURI('service') # 'https://psl-115.ucdavis.edu/miv/MIVLogin'
    mivCommon.writeToFile(self.fileHandle,"\nMIVLogin_initial_page\n"+result.toString()+result.getText())

    return result

  def Get_CASLogin_page(self):

    result = self.request201.GET('/impersonate/login' +
      '?service=' +
      self.token_service)
    self.token_lt = \
      httpUtilities.valueFromHiddenInput('lt') # '_cABFC2967-649F-6095-83EE-AB51E245F328_k...'
    self.token__eventId = \
      httpUtilities.valueFromHiddenInput('_eventId') # 'submit'

    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
       mivCommon.setTestSuccess(self,0);

    mivCommon.writeToFile(self.fileHandle,"\nGet_CASLogin\n"+result.toString()+result.getText())

    return result

  def Post_CASLogin_page(self):

    self.token_lt = \
      httpUtilities.valueFromHiddenInput('lt') 
    
    # Expecting 302 'Moved Temporarily'
    result = self.request301.POST('/impersonate/login?service=' +
      self.token_service,
      ( NVPair('username', self.username),
        NVPair('password', self.password),
        NVPair('lt', self.token_lt),
        NVPair('_eventId', self.token__eventId), ),
      ( NVPair('Content-Type', 'application/x-www-form-urlencoded'), ))
    self.token_ticket = \
      httpUtilities.valueFromLocationURI('ticket') # 'ST-1890225-GG2eWYSuPPaBjZStWykiMSpXxUdWB...'
      
    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
       mivCommon.setTestSuccess(self,0);

   # If there is no ticket, login failed  
    if self.token_ticket == '':
       mivCommon.setTestSuccess(self,0);
    
    mivCommon.writeToFile(self.fileHandle,"\nPost_CASLogin\n"+result.toString()+result.getText())

    return result

  def MIVLogin_page(self):

      result = self.request401.GET('/miv/MIVLogin?ticket='+self.token_ticket)

      mivCommon.writeToFile(self.fileHandle,"\nMIVLogin_page\n"+result.toString()+result.getText())

      if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
         mivCommon.setTestSuccess(self,0);
            
      # Redirect to MIVLogin
      if result.getStatusCode() == 302:
         redirectURL = mivCommon.getRedirectURL(result)
         mivCommon.logMessage("Redirecting to: %s" % redirectURL)
         result1 = self.request501.GET(redirectURL)
         if result1.getStatusCode() == 302:
             if (mivCommon.getRedirectURL(result1).__contains__('unauthorized')):
                 mivCommon.logAndPrint("Unable to login %s (%s / %s): unauthorized" % (self.fullname, self.username, self.principalId))
                 mivCommon.setTestSuccess(self,0);

                
         mivCommon.writeToFile(self.fileHandle,"\nMIVLogin\n"+result1.toString()+result1.getText())
    
    
      if mivCommon.getTestSuccess(self):
         message = "Logged in %s (%s / %s)" % (self.fullname, self.username, self.principalId)    
         self.token_jsessionid = mivCommon.getSessionId()
         mivCommon.logMessage("JSESSIONID: %s" % self.token_jsessionid)
      else:    
         message = "Log in failed for %s (%s / %s)" % (self.fullname, self.username, self.principalId)    

      mivCommon.logAndPrint(message)
    
      return result

  def MIVMain_page(self):
      result = self.request601.GET('/miv/MIVMain')

      if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
         mivCommon.setTestSuccess(self,0);

      mivCommon.writeToFile(self.fileHandle,"\nMIVMain_page\n"+result.toString()+result.getText())
      return result  
  
  def __call__(self):
    """This method is called for every run performed by the worker thread."""

    # open a test file
    self.fileHandle = mivCommon.openTestFile(self.getScriptName())

    # defaults
    if self.nextTestNumber is None:
        self.setNextTestNumber(nextTestNumber)
    
    if self.testIncrement is None:
        self.setNextTestNumber(testIncrement)
    self. testSuccess = 1
    
    # If the response back from the server is not one that we except,
    # we want to mark the test as unsuccessful and not include the statistics
    # in the test times. To do this, the delayReports variable can be set to 1.
    # Doing so will delay the reporting back of the statistics until after
    # the test has completed and we have had chance to check its operation.
    # The default is to report back when the test returns control back to
    # the script, i.e. immediately after a test has executed.
    grinder.getStatistics().setDelayReports(1)
    
    # Create an HTTPRequest for each request, then replace the
    # reference to the HTTPRequest with an instrumented version.
    # You can access the unadorned instance using request101.__target__.
    self.nextTestNumber = self.nextTestNumber+self.testIncrement   
    self.request101 = HTTPRequest(url=url0, headers=headers0)
    self.request101 = Test(self.nextTestNumber+1, 'Get Login').wrap(self.request101)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'MIVLogin_initial_page'), 'MIVLogin_initial_page')

    self.nextTestNumber += self.testIncrement
    self.request201 = HTTPRequest(url=url1, headers=headers1)
    self.request201 = Test(self.nextTestNumber+1, 'GET login').wrap(self.request201)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'Get_CASLogin_page'), 'Get_CASLogin_page')

    self.nextTestNumber += self.testIncrement
    self.request301 = HTTPRequest(url=url1, headers=headers1)
    self.request301 = Test(self.nextTestNumber+1, 'POST login').wrap(self.request301)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'Post_CASLogin_page'), 'Post_CASLogin_page')

    self.nextTestNumber += self.testIncrement
    self.request401 = HTTPRequest(url=url0, headers=headers0)
    self.request401 = Test(self.nextTestNumber+1, 'GET MIVLogin').wrap(self.request401)
    self.request501 = HTTPRequest(url=url0, headers=headers0)
    self.request501 = Test(self.nextTestNumber+2, 'MivLogin').wrap(self.request501)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'MIVLogin_page'), 'MIVLogin_page')
    
    self.nextTestNumber += self.testIncrement
    self.request601 = HTTPRequest(url=url0, headers=headers0)
    self.request601 = Test(self.nextTestNumber+1, 'GET MIVMain').wrap(self.request601)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'MIVMain_page'), 'MIVMain_page')

    self.MIVLogin_initial_page()      # GET MIVLogin (request 101)

    self.Get_CASLogin_page()      # GET login (requests 201)

    mivCommon.sleep()
    self.Post_CASLogin_page()      # POST login (request 301)

    self.MIVLogin_page()      # GET MIVLogin (requests 401-501)

    self.MIVMain_page()      # GET MIVMain (requests 601)
    
    scriptResult = "Success"
    if not  mivCommon.getTestSuccess(self):
       scriptResult = "Failed"
    mivCommon.printMessage(self.getScriptName()+' Script Complete: '+ scriptResult)

    mivCommon.closeTestFile(self.fileHandle)    

def instrumentMethod(test, method_name, c=TestRunner):
  """Instrument a method with the given Test."""
  unadorned = getattr(c, method_name)
  import new
  method = new.instancemethod(test.wrap(unadorned), None, c)
  setattr(c, method_name, method)
